<?php
class Document {
	private $title;
	private $description;
	private $keywords;	
	private $links = array();		
	private $styles = array();
	private $scripts = array();
	private $cook_title;
	private $og_image;
	private $address_type;
	private $page_image;
	
	public function setTitle($title) {
		$this->title = $title;
	}
	
	public function getTitle() {
		return $this->title;
	}
	
	public function setDescription($description) {
		$this->description = $description;
	}
		
	public function getDescription() {
		return $this->description;
	}
	
	public function setOgTitle($cook_title) {
		$this->cook_title = $cook_title;
	}

	public function getOgTitle() {
		return $this->cook_title;
	}
	
	public function setOgImage($og_image) {
		$this->og_image = $og_image;
	}

	public function getOgImage() {
		return $this->og_image;
	}
	
	public function setAddressType($address) {
		$this->address_type = $address;
	}

	public function getAddressType() {
		return $this->address_type;
	}
	
	public function setKeywords($keywords) {
		$this->keywords = $keywords;
	}
	
	public function getKeywords() {
		return $this->keywords;
	}
	
	public function addLink($href, $rel) {
		$this->links[md5($href)] = array(
			'href' => $href,
			'rel'  => $rel
		);			
	}
	
	public function getLinks() {
		return $this->links;
	}	
	
	public function addStyle($href, $rel = 'stylesheet', $media = 'screen') {
		$this->styles[md5($href)] = array(
			'href'  => $href,
			'rel'   => $rel,
			'media' => $media
		);
	}
	
	public function getStyles() {
		return $this->styles;
	}	
	
	public function addScript($script) {
		$this->scripts[md5($script)] = $script;			
	}
	
	public function getScripts() {
		return $this->scripts;
	}
	
	public function setPageImage($page_image) {
		$this->page_image = $page_image;
	}
	
	public function getPageImage() {
		return $this->page_image;
	}
}
?>