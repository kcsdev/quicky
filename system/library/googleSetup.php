<?php

// Includes
set_include_path(dirname(__FILE__));

require_once 'googleSetup/google-api-php-client/apiClient.php';
require_once 'googleSetup/google-api-php-client/contrib/apiOauth2Service.php';
require_once 'googleSetup/idiorm.php';
require_once 'googleSetup/relativeTime.php';

// Session. Pass your own name if you wish.

session_name('tzine_demo');

