<?php

function mb_truncate($string, $length = 80, $etc = '...', $charset='UTF-8') {
        
    if ($length == 0)
        return '';

    if (mb_strlen($string, $charset) > $length) {
        //$length -= min($length, mb_strlen($etc, $charset));
        
        $string = preg_replace('/\s+?(\S+)?$/', '', mb_substr($string, 0, $length+1, $charset));
        return mb_substr($string, 0, $length, $charset) . $etc;
    
    } else {
        return $string;
    }
}

function truncate($str, $length, $breakWords = TRUE, $append = '�') {

    mb_internal_encoding("UTF-8");    
    
    $strLength = mb_strlen($str);
    
    if ($strLength <= $length) {
       return $str;
    }
    
    if ( ! $breakWords) {
        while ($length < $strLength AND preg_match('/^\pL$/', mb_substr($str, $length, 1, $charset))) {
            $length++;
        }
    }
    
    return mb_substr($str, 0, $length) . $append;
}


?>