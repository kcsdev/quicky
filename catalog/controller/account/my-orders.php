<?php

class ControllerAccountMyOrders extends Controller {
	
	public function getMyOrders($data = array()) {
		$this->language->load('account/myorders');
		
		$data_get = $this->request->get;
		
		$date = isset($data['stat_date']) ? date("Y-m-d",strtotime($data['stat_date'])) : date("Y-m-d",strtotime("yesterday"));
		
		$last_date = isset($data['last_date']) ? date("Y-m-d",strtotime($data['last_date'])) : date("Y-m-d",strtotime("today +30 days"));
		$order_status = isset($data['order_status']) ? $data['order_status'] : "1";
		
		$order = isset($data['order']) ? $data['order'] : "ASC";
		
		$customer_id = $this->customer->getId();
		
		$json['data'] = array();
		
			$result = $this->db->query("SELECT * FROM " . DB_PREFIX . "order WHERE delivery_date >= '".$this->db->escape($date)."' AND delivery_date <= '". $this->db->escape($last_date) ."' AND parent_order_id = '0' AND order_status_id IN (". $order_status .") AND customer_id = '". (int)$customer_id ."' ORDER BY delivery_date ". $this->db->escape($order) ." , delivery_time");
		
		foreach ($result->rows as $order_info){
			

				$order_history_query =  $this->db->query("SELECT * FROM oc_order_history h LEFT JOIN oc_order_status s ON s.order_status_id = h.order_status_id WHERE h.order_id = ".(int)$order_info['order_id']." AND s.language_id = 2");
				$order_history_info = $order_history_query->rows;
				
				$num_of_meals_query = $this->db->query("SELECT SUM(quantity) AS meal_num FROM " . DB_PREFIX . "order_product WHERE order_id = ".(int)$order_info['order_id']."");
				$num_of_meals = $num_of_meals_query->row;
				
				$meals_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "order_product WHERE order_id = ".(int)$order_info['order_id']."");
				$meals_info_rows = $meals_query->rows;	
			
			$meals_info = array();
			$total_profit = 0;
			
			foreach ($meals_info_rows as $meals_info_row){
				
				$sides = explode(',', $meals_info_row['sides']);
				
				$sides_info = array();
				
				if(!(empty($sides[0]))) {
					foreach ($sides as $side){
						$side_result_query = $this->db->query("SELECT name , image FROM " . DB_PREFIX . "product p LEFT JOIN " . DB_PREFIX . "product_description  pd ON p.product_id = pd.product_id  WHERE p.product_id = ".(int)$side." AND language_id = ".(int)$this->config->get('config_language_id')."");
						
						$side_result = $side_result_query->row;
						
						$sides_info[] = array(
								'name' => $side_result['name'],
								'image' => $side_result['image']
								);
					}
				}
				
				$total_profit += $meals_info_row['total'];
				
				$meals_info[] = array(
						'product_id' => $meals_info_row['product_id'],
						'profit' => $meals_info_row['total'],
						'name' => mb_truncate($meals_info_row['name'],40),
						'full_name' => $meals_info_row['name'],
						'comment' => $meals_info_row['comment'],
						'quantity' => $meals_info_row['quantity'],
						'sides' => empty($sides_info) ? "" : $sides_info
						);
					
			}
			
			$order_history = array();
			
			foreach ($order_history_info as $history_info){
				$order_history_name = $history_info['name'];
			}
			
			$this->load->model('account/address');
			$this->load->model('account/customer');
			$this->load->model('tool/image');
			
			$customer_info = $this->model_account_customer->getCustomer($order_info['customer_id']);
						
			if(!empty($customer_info['image'])){
				if (strpos($customer_info['image'], 'http') === 0) {
					$author_pic = $customer_info['pic_square'];
				}else{
					$author_pic = $this->model_tool_image->resize($customer_info['image'], 40, 40);
				}
			} else {
				$author_pic = $this->model_tool_image->resize("no-avatar-women.png", 40, 40);;
			}
			
			$cooks_s = json_decode($order_info['cook_id']);
			
			$cook = 0;
			$cooks_i = 0;
			if(is_object($cooks_s)) {
				foreach($cooks_s as $cooks_s){
					$cooks_i++;
					if($cooks_i == 1){
						$cook = $cooks_s;
					}
					
				}
			}
			
			$cook_info = $this->model_account_customer->getCustomer($cook);
						
			if(!empty($cook_info['image'])){
				if (strpos($cook_info['image'], 'http') === 0) {
					$cook_pic = $cook_info['pic_square'];
				}else{
					$cook_pic = $this->model_tool_image->resize($cook_info['image'], 40, 40);
				}
			} else {
				$cook_pic = $this->model_tool_image->resize("no-avatar-women.png", 40, 40);
			}
			
			if($cooks_i == 1){
				$cook_name = $cook_info['firstname'];
			} else {
				$cook_name = "בשלנים שונים";
				$cook_pic = $this->model_tool_image->resize("no-avatar-cook.jpg", 40, 40);
			}
			
			$cook_addresses = $this->model_account_address->getAddressesById($order_info['cook_id']);
			$cook_address = reset($cook_addresses);
			$cook_city = $cook_address['city'];
			
			$customer_info = $this->model_account_customer->getCustomer($order_info['customer_id']);
						
			if(!empty($customer_info['image'])){
				if (strpos($customer_info['image'], 'http') === 0) {
					$author_pic = $customer_info['pic_square'];
				}else{
					$author_pic = $this->model_tool_image->resize($customer_info['image'], 40, 40);
				}
			} else {
				$author_pic = $this->model_tool_image->resize("no-avatar-women.png", 40, 40);;
			}
			
			$order_time = date("H:i",strtotime($order_info['delivery_time']." -1 hour"))." - ".date("H:i",strtotime($order_info['delivery_time'])) ;
			$cook_order_time = date("H:i",strtotime($order_info['delivery_time']." -2 hour"))." - ".date("H:i",strtotime($order_info['delivery_time']."-1 hour")) ;
			$order_date_time = strftime("%A, %e ב%B",strtotime($order_info['delivery_date']));
			
			
			
			
			//// prep code for the right way to show status data
			//// maybe we first check if all child orders are approved
			//$pre_result = $this->db->query("SELECT order_id, order_status_id FROM " . DB_PREFIX . "order WHERE parent_order_id = '".(int)$order_info['parent_order_id']."'");

			//if(isset($_GET['debug'])){
			//	print_r($pre_result);
			//	die;
			//}
			
			//// a check to see if all sub orders are confirmed
			////print_r($pre_result);
			//
			//$num_approved = 0;
			//
			//foreach($pre_result->rows as $result){
			//	if($result['order_status_id'] >= 5){
			//		$num_approved++;
			//	}
			//}
			//
			//if($num_approved == 0){
			//	$this->data['aproved_status'] = " מאושר ";
			//} else if ($num_approved < $pre_result->num_rows){
			//	$this->data['aproved_status'] = "מאושר חלקית";
			//} else if ($num_approved == $pre_result->num_rows){
			//	$this->data['aproved_status'] = "מאושר";
			//}
			//
			//$status_name = $this->db->query("SELECT * FROM " . DB_PREFIX . "order_status WHERE order_status_id = '".(int)$pre_result->row['order_status_id']."' AND language_id = '".(int)$order_info['language_id']."'");
			//
			//
			//if($status_name->row['name'] == "מאושר"){
			//	$status = $this->data['aproved_status'];
			//} else {
			//	$status = $status_name->row['name'];
			//}
			
			
			// maybe we first check if all child orders are approved
			$pre_result = $this->db->query("SELECT o.order_id, o.order_status_id ,os.name FROM " . DB_PREFIX . "order as o LEFT JOIN " . DB_PREFIX . "order_status as os ON o.order_status_id = os.order_status_id  WHERE parent_order_id = '".(int)$order_info['order_id']."' AND os.language_id = 2");
			
			$status = isset($pre_result->row['name']) ? $pre_result->row['name'] : "";
			
			$json['data'][] = array(
				"DT_RowId" => $order_info['order_id'],
				"order_id" => $order_info['order_id'],
				"cook_address_1" => $cook_address['address_1'],
				"cook_address_2" => $cook_address['address_2'],
				"cook_company" => $cook_address['company'],
				"cook_name" => $cook_name,
				"cook_id" => $order_info['cook_id'],
				"cook_comment" => $cook_address['address_comment'],
				"customer_id" => $order_info['customer_id'],
				"cook_avatar" => $cook_pic,
				"customer_avatar" => $author_pic,
				"customer_name" => $order_info['firstname'],
				"customer_contact" => $order_info['payment_firstname']." ".$order_info['payment_lastname'],
				"customer_address_1" => $order_info['payment_address_1'],
				"customer_address_2" => $order_info['payment_address_2'],
				"customer_company" => $order_info['payment_company'],
				"customer_phone" => $order_info['payment_postcode'],
				"order_comment" => $order_info['comment'],
				"order_time" => $order_time,
				"cook_order_time" => $cook_order_time,
				"order_details_link" => $this->url->link('account/order/info', '&order_id='.$order_info['order_id'], 'SSL'),
				"order_date" => strftime("יום %a, %e ב%B",strtotime($order_info['delivery_date'])),
				"order_date_time" => $order_date_time,
				"status" => $order_info['order_status_id'],
				"status_text" => isset($status) ? $status : "",
				"date_modified" => date("H:i",strtotime($order_info['date_modified'])),
				"delivery_man" => $order_info['driver_deliver'],
				"meals" => $meals_info,
				"number_of_meals" => $num_of_meals['meal_num'],
				"aprove_link" => $this->url->link('seller/account-order/approveOrder', '&oid='.$order_info['order_id'], 'SSL'),
				"total_profit" => $this->currency->format($order_info['total'], $order_info['currency_value']),
				"currency_value" => $order_info['currency_value'],
				"int_total_profit" => $total_profit
				);
		}
		
		return $json['data'];
	}
	
	public function index() {
		
		if (!$this->customer->isLogged()) {
			$this->session->data['redirect'] = $this->url->link('account/my-orders', '', 'SSL');
	  		$this->redirect($this->url->link('product/category', '&path=0&open_login', 'SSL'));
		}
		
		$this->language->load('account/myorders');
		
		$this->data['link_back'] = $this->url->link('account/account', '', 'SSL');
		
		$this->document->setTitle($this->language->get('your_orders_page_title'));
		
		$this->data['text_my_orders'] = $this->language->get('text_my_orders');
		$this->data['text_recived_orders'] = $this->language->get('text_recived_orders');
		$this->data['text_history_orders'] = $this->language->get('text_history_orders');
		$this->data['text_wating_orders'] = $this->language->get('text_wating_orders');
		
		$this->data['text_order_details_link'] = $this->language->get('text_order_details_link');
		
		$this->data['cul_num'] = $this->language->get('cul_num');
		$this->data['cul_who'] = $this->language->get('cul_who');
		$this->data['cul_who_cook'] = $this->language->get('cul_who_cook');
		$this->data['cul_meals'] = $this->language->get('cul_meals');
		$this->data['cul_time'] = $this->language->get('cul_time');
		$this->data['cul_time_delivery'] = $this->language->get('cul_time_delivery');
		$this->data['cul_total'] = $this->language->get('cul_total');
		$this->data['cul_status'] = $this->language->get('cul_status');
		
		
		$showtime = isset($this->request->get['showtime']) ? $this->request->get['showtime'] : 0;
		
		if ($showtime == 1){
			$last_date = "tomorrow";
			$stat_date = "tomorrow";
		} elseif($showtime > 0){
			$stat_date = "today";
			$last_date = "today +".$showtime." days";
		} elseif($showtime < 0) {
			$last_date = "today";
			$stat_date = "today ".$showtime." days";
		} elseif( $showtime == 0) {
			$stat_date = "today";
			$last_date = "today +365 days";
		}
		
		$my_orders = $this->getMyOrders(array('stat_date' => $stat_date,'last_date' => $last_date,'order_status' => "1,5,17,18,19,20,21,22"));
		
		$orders_by_day = array();
		
		$total_meals = 0;
		
		foreach($my_orders as $order){
			$orders_by_day[0][] = $order;
			
			$total_meals += $order['number_of_meals'];
		}
		
		$this->data['orders'] = $orders_by_day;
		
		$showhistory = isset($this->request->get['sh']) ? $this->request->get['sh'] : 0;
		
		if($showhistory > 0){
			$stat_date = "today";
			$last_date = "today +".$showhistory." days";
		} elseif($showhistory < 0) {
			$last_date = "today";
			$stat_date = "today ".$showhistory." days";
		} else {
			$last_date = "today";
			$stat_date = "today -7 days";
		}
		
		
		$start = isset($this->request->get['shs']) ? $this->request->get['shs'] : 0;
		$end = isset($this->request->get['she']) ? $this->request->get['she'] : 0;
		
		$or_stat = "1,5,17,18,19,20,21,22";
		
		if($start && $end){
			$last_date = $end;
			$stat_date = $start;
			$or_stat = "18";
		}
		
		$my_history_orders = $this->getMyOrders(array('stat_date' => $stat_date,'last_date' => $last_date,'order' => 'DESC', 'order_status' => $or_stat));

		$history_orders_by_day = array();
		$total_meals = 0;
		$total_cost = 0;
		$currency_value = 1;
		
		foreach($my_history_orders as $key => $order){
			$history_orders_by_day[0][] = $order;
			$total_meals += $order['number_of_meals'];
			$total_cost += $order['int_total_profit'];
			$currency_value = $order['currency_value'];
		}
		
		$this->load->model('tool/image');
		
		$sql = "SELECT *  FROM " . DB_PREFIX . "customer WHERE customer_group_id = 2";
		$query = $this->db->query($sql);
		
		$cooks_info = $query->rows;
		
		$all_cooks = array();
		
		foreach($cooks_info as $cook_info){
			if(!empty($cook_info['image'])){
				if (strpos($cook_info['image'], 'http') === 0) {
					$author_pic = $cook_info['pic_square'];
				}else{
					$author_pic = $this->model_tool_image->resize($cook_info['image'], 40, 40);
				}
			} else {
				$author_pic = $this->model_tool_image->resize("no-avatar-women.png", 40, 40);;
			}
			
			$all_cooks[] = array(
				"id" => $cook_info['customer_id'],
				"name" => $cook_info['firstname'],
				"img" => "$author_pic",
			);
			
		}
		
		$this->data['all_cooks'] = $all_cooks;
		
		$this->data['total_meals'] = $total_meals;
		$this->data['total_cost'] = $this->currency->format($total_cost, $currency_value);
		
		$this->data['history_orders'] = $history_orders_by_day;
		
		$this->data['waiting_orders'] = $this->getMyOrders(array('order_status' => '1'));
		$this->data['waiting_orders_num'] = count($this->data['waiting_orders']);
		
		list($this->template, $this->children) = $this->MsLoader->MsHelper->loadTemplate('my-orders');
		$this->response->setOutput($this->render());
	}
}

?>
