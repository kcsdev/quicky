<?php 
class ControllerAccountThankyou extends Controller {  
	public function index() {
    	$this->language->load('account/success');
  
    	$this->document->setTitle($this->language->get('heading_title'));

		$this->data['breadcrumbs'] = array();

      	$this->data['breadcrumbs'][] = array(
        	'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home'),       	
        	'separator' => false
      	); 

      	$this->data['breadcrumbs'][] = array(
        	'text'      => $this->language->get('text_account'),
			'href'      => $this->url->link('account/account', '', 'SSL'),
        	'separator' => $this->language->get('text_separator')
      	);

      	$this->data['breadcrumbs'][] = array(
        	'text'      => $this->language->get('text_success'),
			'href'      => $this->url->link('account/success'),
        	'separator' => $this->language->get('text_separator')
      	);

    	$this->data['heading_title'] = $this->language->get('heading_title');

		$this->load->model('account/customer_group');
		
		$customer_group = $this->model_account_customer_group->getCustomerGroup($this->customer->getCustomerGroupId());

		if ($customer_group && !$customer_group['approval']) {
    		$this->data['text_message'] = sprintf($this->language->get('text_message'), $this->url->link('information/contact'));
		} else {
			$this->data['text_message'] = sprintf($this->language->get('text_approval'), $this->config->get('config_name'), $this->url->link('information/contact'));
		}
		
    	$this->data['button_continue'] = $this->language->get('button_continue');
		
		if ($this->cart->hasProducts()) {
			$this->data['continue'] = $this->url->link('checkout/cart', '', 'SSL');
		} else {
			$this->data['continue'] = $this->url->link('account/account', '', 'SSL');
		}

		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/common/thankyou.tpl')) {
			$this->template = $this->config->get('config_template') . '/template/common/thankyou.tpl';
		} else {
			$this->template = 'default/template/common/success.tpl';
		}
		
		$this->children = array(
			'common/column_left',
			'common/column_right',
			'common/content_top',
			'common/content_bottom',
			'common/footer',
			'common/header'	
		);
						
		$this->response->setOutput($this->render());				
  	}
	
	
	
	public function addThisCoupon() {
		
	    // if customer just sgined up or is logged in
		if(isset($this->request->post['coupon_code']) && $this->customer->isLogged() && !isset($this->request->post['email'])){
			$this->session->data['coupon_new'] = true;
			
			$code = $this->request->post['coupon_code'];
			// add coustomer to coupon coustomer list
			$this->load->model('checkout/coupon');
			$coupon = $this->model_checkout_coupon->couponCheck($code);
			
			if($coupon){
				
				$worked = $this->model_checkout_coupon->addToCCListDirect($coupon['coupon_id'],$this->customer->getId());
				
				if($worked == "was before"){
					unset($this->session->data['coupon_new']);
					$message = array( 'success' => 'coupon already exists' );
				} else if($worked == "added") {
					unset($this->session->data['coupon_new']);
					$message = array( 'success' => 'coupon was added successfuly' );
				} else {
					unset($this->session->data['coupon_new']);
					$message = array( 'error' => 'error somthing went wrong' );
				}
				
			} else {
				$message = array( 'error' => 'coupon code error loggedin');
			}
		}
		
		// if email all ready exists
		if(isset($this->request->post['email']) && isset($this->request->post['coupon_code'])){
			$this->session->data['coupon_new'] = true;
			
			$code = $this->request->post['coupon_code'];
			// add coustomer to coupon coustomer list
			$this->load->model('checkout/coupon');
			$coupon = $this->model_checkout_coupon->couponCheck($code);
			
			if($coupon){
				
				$this->load->model('account/customer');
				
				$customer_info = $this->model_account_customer->getCustomerByEmail($this->request->post['email']);
				
				if($customer_info){
					$worked = $this->model_checkout_coupon->addToCCListDirect($coupon['coupon_id'],$customer_info['customer_id']);
					if($worked == "was before"){
						unset($this->session->data['coupon_new']);
						$message = array( 'success' => 'coupon already exists' );
					} else if($worked == "added") {
						unset($this->session->data['coupon_new']);
						$message = array( 'success' => 'coupon was added successfuly' );
					} else {
						unset($this->session->data['coupon_new']);
						$message = array( 'error' => 'error somthing went wrong' );
					}
					
				} else {
					$message = array( 'error' => 'customer dose not exists');
				}
				
				
			} else {
				$message = array( 'error' => 'coupon code error email only');
			}
			
		}
		
		if(!isset($this->request->post['coupon_code'])) {
			$message = array( 'error' => 'no coupon code was sent');
		}
		
		$this->response->setOutput(json_encode($message));
	    
	}
}
?>