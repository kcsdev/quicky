<?php

class ControllerAccountMSMessage extends Controller {
	public function __construct($registry) {
		parent::__construct($registry);
		
		if (!$this->customer->isLogged()) {
			$this->session->data['redirect'] = $this->url->link('account/msconversation', '', 'SSL');
			return $this->redirect($this->url->link('product/category', '&path=0&open_login', 'SSL'));
		}
		
		if ($this->config->get('msconf_enable_private_messaging') != 1) return $this->redirect($this->url->link('account/account', '', 'SSL'));
	}
	
	public function jxSendMessage() {
		$this->data = array_merge($this->data, $this->load->language('multiseller/multiseller'));
		$customer_id = $this->customer->getId();
		$customer_name = $this->customer->getFirstname() . ' ' . $this->customer->getLastname();
		$conversation_id = $this->request->post['conversation_id'];
		if (!$conversation_id) return;
		
		$conversation = $this->MsLoader->MsConversation->getConversations(array(
			'conversation_id' => $conversation_id,
			'single' => 1
		));
		
		if (!$conversation) return;
		
		$message_to = $this->MsLoader->MsConversation->getWith($conversation_id, array('participant_id' => $customer_id));
		$message_text = trim($this->request->post['ms-message-text']);
		
		$conversation_with = $this->MsLoader->MsConversation->getWith($conversation_id, array('participant_id' => $customer_id));
		
		$this->load->model('account/customer');
		$customer = $this->model_account_customer->getCustomer($conversation_with);
		$addressee_name = $customer['firstname'] . ' ' . $customer['lastname'];
		
		$recepient_email = $customer['email'];
		
		$json = array();
		
		if (empty($message_text)) {
			$json['errors'][] = $this->language->get('ms_error_empty_message');
			$this->response->setOutput(json_encode($json));
			return;
		}
		
		if (mb_strlen($message_text) > 2000) {
			$json['errors'][] = $this->language->get('ms_error_contact_text');
		}
		
		if (!isset($json['errors'])) {
			$this->MsLoader->MsMessage->createMessage(
				array(
					'conversation_id' => $conversation_id,
					'from' => $this->customer->getId(),
					'to' => $message_to,
					'message' => $message_text
				)
			);
			
			//$mails[] = array(
			//	'type' => MsMail::SMT_PRIVATE_MESSAGE,
			//	'data' => array(
			//		'recipients' => $recepient_email,
			//		'customer_name' => $customer_name,
			//		'customer_message' => $message_text,
			//		'title' => $conversation['title'],
			//		'product_id' => $conversation['product_id'],
			//		'addressee' => $addressee_name
			//	)
			//);
			//
			//$this->MsLoader->MsMail->sendMails($mails);
			
			/**********************************************************************************
			 *	homeals mail change
			 **********************************************************************************/
			$subject = sprintf($this->language->get('text_subject_pm'),$customer_name);
			
			$template = new Template();
			
			$template->data['title'] = $subject;
			$template->data['store_name'] = 'Homeals';
			$template->data['store_url'] = $this->config->get('config_url');
			$template->data['conv_url'] = $this->url->link('account/msmessage', '&conversation_id='.$conversation_id, 'SSL');
			$template->data['logo'] = $this->config->get('config_url') . 'image/' . $this->config->get('config_logo');
			
			$template->data['text_greeting'] = sprintf($this->language->get('text_greeting_pm'),$addressee_name);
			$template->data['text_pre_msg'] = sprintf($this->language->get('text_pre_msg'),$customer_name);
			
			$template->data['message'] = '"'.mb_truncate($message_text,140).'"';
			$template->data['email'] = $recepient_email;
			
			// fotter
			$template->data['text_follow_us'] = $this->language->get('text_follow_us');			
			
			if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/mail/pm.tpl')) {
				$html = $template->fetch($this->config->get('config_template') . '/template/mail/pm.tpl');
			} else {
				$html = $template->fetch('default/template/mail/pm.tpl');
			}
			
			$mail = new Mail(); 
			$mail->protocol = $this->config->get('config_mail_protocol');
			$mail->parameter = $this->config->get('config_mail_parameter');
			$mail->hostname = $this->config->get('config_smtp_host');
			$mail->username = $this->config->get('config_smtp_username');
			$mail->password = $this->config->get('config_smtp_password');
			$mail->port = $this->config->get('config_smtp_port');
			$mail->timeout = $this->config->get('config_smtp_timeout');
			
			$mail->setTo($recepient_email);
			$mail->setFrom($this->config->get('config_email'));
			$mail->setSender('Homeals');
			$mail->setSubject(html_entity_decode($subject, ENT_QUOTES, 'UTF-8'));
			$mail->setHtml($html);
			$mail->send();
			
			/**********************************************************************************
			 *	homeals mail end
			 **********************************************************************************/
			
			$json['success'] = $this->language->get('ms_sellercontact_success');
			$json['msg_text'] = $message_text;
			$json['redirect'] = $this->url->link('account/msmessage&conversation_id=' . $conversation_id, '', 'SSL');
		}
		$this->response->setOutput(json_encode($json));
	}

	public function index() {
		$this->document->addScript('catalog/view/javascript/multimerch/account-message.js');
		$this->data = array_merge($this->data, $this->load->language('multiseller/multiseller'));
		$this->language->load('account/account');
		$customer_id = $this->customer->getId();
		
		$this->load->model('account/customer');
		$this->load->model('tool/image');
		$customer_info = $this->model_account_customer->getCustomer($customer_id);
		
		if(!empty($customer_info['image'])){
			if (strpos($customer_info['image'], 'http') === 0) {
				$customer_image = $customer_info['pic_square'];
			}else{
				$customer_image = $this->model_tool_image->resize($customer_info['image'], 50, 50);
			}
		} else {
			$customer_image = $this->model_tool_image->resize('no-avatar-women.png', 50, 50);
		}
				
		$this->data['customer_image'] = $customer_image;
		$this->data['customer_name'] = $customer_info['firstname'];
		
		$this->data['msg_placeholder'] = $this->language->get('msg_placeholder');
		$this->data['inbox_url'] = $this->url->link('account/msconversation', '', 'SSL');
		
		
		$conversation_id = isset($this->request->get['conversation_id']) ? $this->request->get['conversation_id'] : false;
		if (!$conversation_id || !$this->MsLoader->MsConversation->isParticipant($conversation_id, array('participant_id' => $customer_id)))
			return $this->redirect($this->url->link('account/msconversation', '', 'SSL'));
		
		$his_id = $this->MsLoader->MsConversation->getWith($conversation_id, array ('participant_id' => $customer_id));
		$his_info = $this->model_account_customer->getCustomer($his_id);
		
		$this->data['his_name'] = sprintf($this->language->get('msg_title'),$his_info['firstname']);
		
		$messages = $this->MsLoader->MsMessage->getMessages(
			array(
				'conversation_id' => $conversation_id
			),
			array(
				'order_by'  => 'date_created',
				'order_way' => 'DESC',
			)
		);
		
		$this->data['date_format'] = $this->language->get('date_strf_message');
		
		$this->data['messages'] = array();
		
		foreach($messages as $massage){
			
			$customer_info = $this->model_account_customer->getCustomer($massage['from']);
			
			if(!empty($customer_info['image'])){
				if (strpos($customer_info['image'], 'http') === 0) {
					$customer_image = $customer_info['pic_square'];
				}else{
					$customer_image = $this->model_tool_image->resize($customer_info['image'], 50, 50);
				}
			} else {
				$customer_image = $this->model_tool_image->resize('no-avatar-women.png', 50, 50);
			}
			
			$massage['image'] = $customer_image;
			
			$this->data['messages'][] = $massage;
		}
		
		$this->MsLoader->MsConversation->markRead(
			$conversation_id,
			array(
				'participant_id' => $customer_id
			)
		);
		
		$this->data['conversation'] = $this->MsLoader->MsConversation->getConversations(array(
			'conversation_id' => $conversation_id,
			'single' => 1
		));
		
		
		$this->document->setTitle( sprintf($this->language->get('msg_title'),$his_info['firstname']) );
		
		// Breadcrumbs
		$breadcrumbs = array(
			array(
				'text' => $this->language->get('text_account'),
				'href' => $this->url->link('account/account', '', 'SSL'),
			),
			array(
				'text' => $this->language->get('ms_account_dashboard_breadcrumbs'),
				'href' => $this->url->link('seller/account-dashboard', '', 'SSL'),
			),
			array(
				'text' => $this->language->get('ms_account_conversations_breadcrumbs'),
				'href' => $this->url->link('account/msconversation', '', 'SSL'),
			),
			array(
				'text' => $this->data['conversation']['title'],
				'href' => $this->url->link('account/msmessage', '&conversation_id=' . $conversation_id, 'SSL'),
			)
		); 
		
		if (!$this->MsLoader->MsSeller->isCustomerSeller($customer_id)) {
			unset($breadcrumbs[1]);
		}
		
		$this->data['breadcrumbs'] = $this->MsLoader->MsHelper->setBreadcrumbs($breadcrumbs);
		list($this->template, $this->children) = $this->MsLoader->MsHelper->loadTemplate('account-message');
		$this->response->setOutput($this->render());
	}
}

?>
