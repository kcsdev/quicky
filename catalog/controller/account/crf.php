<?php  
class ControllerAccountCRF extends Controller {
	public function dependentoption() {
		if (isset($this->request->get['value'])) {
			$option_value_id = (int)$this->request->get['value'];
		} else {
			$option_value_id = 0;
		}
		
		if (isset($this->request->get['parent_id'])) {
			$option_id = (int)$this->request->get['parent_id'];
		} else {
			$option_id = 0;
		}
		
		$json = array();
		
		$fields = $this->config->get('crf_fields');
		
		$json['option'] = array();
		
		foreach ($fields as $field) {
			if ($field['parent_id'] == $option_id) {
				$query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "option` WHERE option_id = '" . (int)$field['option_id'] . "'");
				
				if ($query->row) {
					if ($query->row['type'] == 'select' || $query->row['type'] == 'radio' || $query->row['type'] == 'checkbox') {
						$query1 = $this->db->query("SELECT * FROM " . DB_PREFIX . "option_value ov LEFT JOIN " . DB_PREFIX . "registration_option_value_to_option_value rov2ov ON rov2ov.registration_option_value_id = ov.option_value_id LEFT JOIN " . DB_PREFIX . "option_value_description ovd ON ovd.option_value_id = ov.option_value_id WHERE ovd.language_id = '" . (int)$this->config->get('config_language_id') . "' AND rov2ov.option_value_id = '" . (int)$option_value_id . "' AND rov2ov.option_id = '" . (int)$field['option_id'] . "' ORDER BY ov.sort_order ASC");
					
						$option_value_data = array();
						
						if ($query1->num_rows) {
							foreach ($query1->rows as $option_value) {
								if (isset($this->session->data['crf'][$field['option_id']]) && ($query->row['type'] == 'select' || $query->row['type'] == 'radio')) {
									$selected = $this->session->data['crf'][$field['option_id']];
								} elseif (isset($this->session->data['crf'][$field['option_id']]) && $query->row['type'] == 'checkbox') {
									$selected = (is_array($this->session->data['crf'][$field['option_id']]) && in_array($option_value['option_value_id'], $this->session->data['crf'][$field['option_id']])) ? $option_value['option_value_id'] : 0;
								} else {
									$selected = '';
								}
								
								$option_value_data[] = array(
									'option_value_id'         => $option_value['option_value_id'],
									'name'                    => $option_value['name'],
									'selected'				  => $selected
								);
							}
						}
					} else {
						$query1 = $this->db->query("SELECT * FROM " . DB_PREFIX . "registration_option_value_to_option_value WHERE option_id = '" . (int)$field['option_id'] . "' AND option_value_id = '" . (int)$option_value_id . "'");
					
						if ($query1->num_rows) {
							$option_value_data = 1;
						} else {
							$option_value_data = 0;
						}
					}
					
					$json['option'][] = array(
						'option_id'         => $field['option_id'],
						'type'              => $query->row['type'],
						'option_value'		=> $option_value_data
					);
				}
			}
		}
		
		$this->response->setOutput(json_encode($json));
	}
				
	public function upload() {
		$this->language->load('product/product');
		
		$json = array();
		
		if (!empty($this->request->files['file']['name'])) {
			$filename = basename(preg_replace('/[^a-zA-Z0-9\.\-\s+]/', '', html_entity_decode($this->request->files['file']['name'], ENT_QUOTES, 'UTF-8')));
			
			if ((utf8_strlen($filename) < 3) || (utf8_strlen($filename) > 64)) {
        		$json['error'] = $this->language->get('error_filename');
	  		}	  	
			
			$allowed = array();
			
			if (!$this->config->get('config_upload_allowed')) {
				$filetypes = explode("\n", $this->config->get('config_file_extension_allowed'));
			} else {
				$filetypes = explode(',', $this->config->get('config_upload_allowed'));
			}
			
			foreach ($filetypes as $filetype) {
				$allowed[] = trim($filetype);
			}
			
			if (!in_array(substr(strrchr($filename, '.'), 1), $allowed)) {
				$json['error'] = $this->language->get('error_filetype');
       		}	
						
			if ($this->request->files['file']['error'] != UPLOAD_ERR_OK) {
				$json['error'] = $this->language->get('error_upload_' . $this->request->files['file']['error']);
			}
		} else {
			$json['error'] = $this->language->get('error_upload');
		}
		
		if (!$json) {
			if (is_uploaded_file($this->request->files['file']['tmp_name']) && file_exists($this->request->files['file']['tmp_name'])) {
				$file = basename($filename) . '.' . md5(mt_rand());
				
				// Hide the uploaded file name so people can not link to it directly.
				$json['file'] = $file;
				
				move_uploaded_file($this->request->files['file']['tmp_name'], DIR_DOWNLOAD . $file);
			}
						
			$json['success'] = $this->language->get('text_upload');
		}	
		
		$this->response->setOutput(json_encode($json));		
	}
}
?>