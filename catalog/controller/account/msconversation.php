<?php

class ControllerAccountMSConversation extends Controller {
	public function __construct($registry) {
		parent::__construct($registry);
		
		if (!$this->customer->isLogged()) {
			$this->session->data['redirect'] = $this->url->link('account/msconversation', '', 'SSL');
			return $this->redirect($this->url->link('product/category', '&path=0&open_login', 'SSL'));
		}
		
		if ($this->config->get('msconf_enable_private_messaging') != 1) return $this->redirect($this->url->link('account/account', '', 'SSL'));		
	}

	public function getTableData() {
		$this->data = array_merge($this->data, $this->load->language('multiseller/multiseller'));
		
		$colMap = array();
		
		$customer_id = $this->customer->getId();
		
		$sorts = array('last_message_date', 'title');
		$filters = array('');
		
		list($sortCol, $sortDir) = $this->MsLoader->MsHelper->getSortParams($sorts, $colMap);
		//$filterParams = $this->MsLoader->MsHelper->getFilterParams($filters, $colMap);

		$conversations = $this->MsLoader->MsConversation->getConversations(
			array(
				'participant_id' => $customer_id,
			),
			array(
				'order_by'  => $sortCol,
				'order_way' => $sortDir,
				'offset' => $this->request->get['iDisplayStart'],
				'limit' => $this->request->get['iDisplayLength']
			)
		);
		
		$total = isset($conversations[0]) ? $conversations[0]['total_rows'] : 0;
		
		$columns = array();
		foreach ($conversations as $conversation) {
			// Actions
			$actions = "";
			$actions .= "<a href='" . $this->url->link('account/msmessage', 'conversation_id=' . $conversation['conversation_id'], 'SSL') ."' class='ms-button ms-button-view' title='" . $this->language->get('ms_view') . "'>view</a>";
			
			// Conversation Status
			$read = "";
			if ($this->MsLoader->MsConversation->isRead($conversation['conversation_id'], array('participant_id' => $customer_id))) {
				$status = "read";
			} else {
				$status = "unread";
			}
			
			// Get customer name
			$conversation_with = $this->MsLoader->MsConversation->getWith($conversation['conversation_id'], array('participant_id' => $customer_id));
			$this->load->model('account/customer');
			$customer = $this->model_account_customer->getCustomer($conversation_with);
			$customer_name = $customer['firstname'] . ' ' . $customer['lastname'];
			
			$columns[] = array_merge(
				$conversation,
				array(
					'icon' => $status,
					//'last_message_date' => date($this->language->get('date_format_long'), strtotime($conversation['last_message_date'])),
					'with' => $customer_name,
					'title' => (mb_strlen($conversation['title']) > 80 ? mb_substr($conversation['title'], 0, 80) . '...' : $conversation['title']),
					'actions' => $actions
				)
			);
		}
		
		$this->response->setOutput(json_encode(array(
			'iTotalRecords' => $total,
			'iTotalDisplayRecords' => $total,
			'aaData' => $columns
		)));
	}
	
	public function index() {
		$this->document->addStyle('catalog/view/javascript/multimerch/datatables/css/jquery.dataTables.css');
		$this->document->addScript('catalog/view/javascript/multimerch/datatables/js/jquery.dataTables.min.js');
		$this->document->addScript('catalog/view/javascript/multimerch/common.js');
		$this->data = array_merge($this->data, $this->load->language('multiseller/multiseller'));
		$this->language->load('account/account');
		
		$this->data['link_back'] = $this->url->link('account/account', '', 'SSL');
		$this->document->setTitle($this->language->get('conversations_heading'));
		$customer_id = $this->customer->getId();
		
		// Breadcrumbs
		$breadcrumbs = array(
			array(
				'text' => $this->language->get('text_account'),
				'href' => $this->url->link('account/account', '', 'SSL'),
			),
			array(
				'text' => $this->language->get('ms_account_dashboard_breadcrumbs'),
				'href' => $this->url->link('seller/account-dashboard', '', 'SSL'),
			),
			array(
				'text' => $this->language->get('ms_account_conversations_breadcrumbs'),
				'href' => $this->url->link('account/msconversation', '', 'SSL'),
			)
		);
		
		if (!$this->MsLoader->MsSeller->isCustomerSeller($customer_id)) {
			unset($breadcrumbs[1]);
		}
		
		$this->data['breadcrumbs'] = $this->MsLoader->MsHelper->setBreadcrumbs($breadcrumbs);
		
		$colMap = array();
		
		$customer_id = $this->customer->getId();
		
		$sorts = array('last_message_date', 'title');
		$filters = array('');
		
		list($sortCol, $sortDir) = $this->MsLoader->MsHelper->getSortParams($sorts, $colMap);

		$conversations = $this->MsLoader->MsConversation->getConversations(
			array(
				'participant_id' => $customer_id,
			),
			array(
				'order_by' => 'last_message_date',
				'order_way' => 'DESC'
			)
		);
		
		$this->data['total'] = isset($conversations[0]) ? $conversations[0]['total_rows'] : 0;
		
		$unread_num = 0;
		
		$columns = array();
		foreach ($conversations as $conversation) {

			// Conversation Status
			$read = "";
			if ($this->MsLoader->MsConversation->isRead($conversation['conversation_id'], array('participant_id' => $customer_id))) {
				$status = "read";
			} else {
				$status = "unread";
				$unread_num++;
			}
			
			// Get customer name
			$conversation_with = $this->MsLoader->MsConversation->getWith($conversation['conversation_id'], array('participant_id' => $customer_id));
			$this->load->model('account/customer');
			$this->load->model('tool/image');
			$customer = $this->model_account_customer->getCustomer($conversation_with);
			
			if(!empty($customer['image'])){
				if (strpos($customer['image'], 'http') === 0) {
					$customer_image = $customer['pic_square'];
				}else{
					$customer_image = $this->model_tool_image->resize($customer['image'], 50, 50);
				}
			} else {
				$customer_image = $this->model_tool_image->resize('no-avatar-women.png', 50, 50);
			}
			
			$customer_name = $customer['firstname'] ;//. ' ' . $customer['lastname'];
			
			setlocale(LC_ALL, 'he_IL.UTF-8');
			
			$columns[] = array_merge(
				$conversation,
				array(
					'status' => $status,
					'image' => $customer_image,
					'last_message_date' => strftime($this->language->get('date_strf_message'), strtotime($conversation['last_message_date'])),
					'with' => $customer_name,
					'title' => mb_truncate($conversation['last_message'],80),
					'conv_link' => $this->url->link('account/msmessage', 'conversation_id=' . $conversation['conversation_id'], 'SSL')
				)
			);
		}
		
		$this->data['conversations'] = $columns;
		$this->data['unread_num'] = $unread_num;
		
		list($this->template, $this->children) = $this->MsLoader->MsHelper->loadTemplate('account-conversation');
		$this->response->setOutput($this->render());
	}
}
?>
