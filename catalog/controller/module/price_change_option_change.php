<?php
class ControllerModulePriceChangeOptionChange extends Controller {

	public function getLatestPriceprice() {

		
            
            $this->load->model('catalog/product');
            $quantity = $this->request->post['quantity'];

		

		$opt_data = array();

		if (!empty($this->request->post['option'])) {
			foreach($this->request->post['option'] as $option) {

				if (!empty($option)) {
					if (is_array($option)) {
						foreach($option as $o) {
							$opt_data[] = $this->optionValue($o);
						}
					} else {
						$opt_data[] = $this->optionValue($option);
					}
				}
			}
		}

		$product = $this->model_catalog_product->getProduct($this->request->post['product_id']);
		$discountValue = $this->discountValues($this->request->post['product_id'],$quantity);

		$price = $discountValue ? $discountValue : $product['price'];
		$special = $product['special'];
		$points = $product['points'];
		$weight = $product['weight'];


		if (!empty($opt_data)) {
			foreach($opt_data as $option) {
				//price
				if ($option['price_prefix'] == '+') {
					if (!empty($special)) {
						$special += $option['price'];
					}

					$price += $option['price'];
				} else {
					if (!empty($special)) {
						$special -= $option['price'];
					}

					$price -= $option['price'];
				}

				//reward
				if ($option['points_prefix'] == '+') {
					$points += $option['points'];
				} else {
					$points -= $option['points'];
				}

				//weight
				if ($option['weight_prefix'] == '+') {
					$weight += $option['weight'];
				} else {
					$weight -= $option['weight'];
				}
			}
		}

		$json = array(
			'price' => $this->currency->format($this->tax->calculate($price, $product['tax_class_id'], $this->config->get('config_tax')) * $quantity),
			'special' => !empty($special) ? $this->currency->format($this->tax->calculate($special, $product['tax_class_id'], $this->config->get('config_tax')) * $quantity) : false,
			'ex_tax' => $this->currency->format((float)$special ? $special * $quantity : $price * $quantity),
			'points' => $points * $quantity,
			'weight' => $this->weight->format($weight * $quantity, $product['weight_class_id'], $this->language->get('decimal_point'), $this->language->get('thousand_point')),
		);

		$this->response->setOutput(json_encode($json));

	}

	protected function optionValue($option_value_id) {

		$query = "SELECT points,price,price_prefix,points_prefix,weight,weight_prefix FROM " . DB_PREFIX . "product_option_value WHERE product_option_value_id = " . (int)$option_value_id;

		$result = $this->db->query($query);

		return $result->row;

	}

	protected function discountValues($product_id,$quantity) {

		$group_id = 1;

		$query = "SELECT price FROM " . DB_PREFIX . "product_discount WHERE product_id = '" . (int)$product_id . "' AND customer_group_id = '" . (int)$group_id . "' AND quantity <= '" . (int)$quantity . "' AND ((date_start = '0000-00-00' OR date_start < NOW()) AND (date_end = '0000-00-00' OR date_end > NOW())) ORDER BY quantity DESC, priority ASC, price ASC LIMIT 1";

		$result = $this->db->query($query);

		if ($result->rows) {
			return $result->row['price'];
		} else {
			return false;
		}

	}
}