<?php  
class ControllerModuleKeywords extends Controller {
	protected function index($setting) {
		$this->language->load('module/keywords');
		
    	$this->data['heading_title'] = $this->language->get('heading_title');
	
	$this->data['text_morning'] = $this->language->get('text_morning');
	$this->data['text_afternoon'] = $this->language->get('text_afternoon');
	$this->data['text_evning'] = $this->language->get('text_evning');
		
		if (isset($this->request->get['filter_tag'])) {
			$parts = explode(',', urldecode((string)$this->request->get['filter_tag']));
		} else {
			$parts = array();
		}
		
		/*if (isset($parts[0])) {
			$this->data['category_id'] = $parts[0];
		} else {
			$this->data['category_id'] = 0;
		}
		
		if (isset($parts[1])) {
			$this->data['child_id'] = $parts[1];
		} else {
			$this->data['child_id'] = 0;
		}*/
							
		$this->load->model('catalog/category');

		$this->load->model('catalog/product');

		$this->data['tags'] = array();

		//$categories = $this->model_catalog_category->getCategories(0);
        
        $tags = $parts;
        
        foreach ($tags as $key => $tag) {
            $product_total = $this->model_catalog_product->getTotalProducts(array('filter_tag' => $tag));
            
            $current_tag = $tag;
            unset($parts[$key]);
            $remove = implode(",",$parts);
            $parts[] = $current_tag;
            
            $this->data['tags'][] = array(				
				'name'        => $tag,
                'remove'      => $this->url->link('product/search', 'filter_tag=' . $remove),
				'href'        => $this->url->link('product/search', 'filter_tag=' . $tag)
			);
        }
        
		/*foreach ($categories as $category) {
			$total = $this->model_catalog_product->getTotalProducts(array('filter_category_id' => $category['category_id']));

			$children_data = array();

			$children = $this->model_catalog_category->getCategories($category['category_id']);

			foreach ($children as $child) {
				$data = array(
					'filter_category_id'  => $child['category_id'],
					'filter_sub_category' => true
				);

				$product_total = $this->model_catalog_product->getTotalProducts($data);

				$total += $product_total;

				$children_data[] = array(
					'category_id' => $child['category_id'],
					'name'        => $child['name'] . ($this->config->get('config_product_count') ? ' (' . $product_total . ')' : ''),
					'href'        => $this->url->link('product/category', 'path=' . $category['category_id'] . '_' . $child['category_id'])	
				);		
			}

			$this->data['categories'][] = array(
				'category_id' => $category['category_id'],
				'name'        => $category['name'] . ($this->config->get('config_product_count') ? ' (' . $total . ')' : ''),
				'children'    => $children_data,
				'href'        => $this->url->link('product/category', 'path=' . $category['category_id'])
			);	
		}*/
		
		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/module/keywords.tpl')) {
			$this->template = $this->config->get('config_template') . '/template/module/keywords.tpl';
		} else {
			$this->template = 'default/template/module/category.tpl';
		}
		
		$this->render();
  	}
}
?>