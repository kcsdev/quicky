<?php  
class ControllerModuleRefine extends Controller {
   protected function index($setting) {
      $this->language->load('module/category');
      
       $this->data['heading_title'] = $this->language->get('heading_title');
      
      if (isset($this->request->get['path'])) {
         $parts = explode('_', (string)$this->request->get['path']);
          $curent_category_id = (int)array_pop($parts);
         $this->data['curent_category_id'] = true;
      } else {
         $parts = array();
         $curent_category_id = false;
         $this->data['curent_category_id'] = false;
      }
      
      if (isset($parts[0])) {
         $this->data['category_id'] = $parts[0];
      } else {
         $this->data['category_id'] = 0;
      }
      
      if (isset($parts[1])) {
         $this->data['child_id'] = $parts[1];
      } else {
         $this->data['child_id'] = 0;
      }
                     
      $this->load->model('catalog/category');

      $this->load->model('catalog/product');
      
      if ($curent_category_id) {
       $curent_category_info = $this->model_catalog_category->getCategory($curent_category_id);
       if ($curent_category_info) {
       
         $this->data['heading_title'] = $curent_category_info['name'];
                        
         $this->data['categories'] = array();
         
         $categories = $this->model_catalog_category->getCategories($curent_category_id);
         
         foreach ($categories as $category) {
         
         $this->data['categories'][] = array(
            'category_id' => $category['category_id'],
            'name'        => $category['name'],
            'children'    => '',
            'href'        => $this->url->link('product/category', 'path=' . $curent_category_info['category_id'] . '_' . $category['category_id'])
         );   
          }
       }
      } else {

      $this->data['categories'] = array();

      $categories = $this->model_catalog_category->getCategories(0);

      foreach ($categories as $category) {
         $total = $this->model_catalog_product->getTotalProducts(array('filter_category_id' => $category['category_id']));

         $children_data = array();

         $children = $this->model_catalog_category->getCategories($category['category_id']);

         foreach ($children as $child) {
            $data = array(
               'filter_category_id'  => $child['category_id'],
               'filter_sub_category' => true
            );

            $product_total = $this->model_catalog_product->getTotalProducts($data);

            $total += $product_total;

            $children_data[] = array(
               'category_id' => $child['category_id'],
               'name'        => $child['name'] . ($this->config->get('config_product_count') ? ' (' . $product_total . ')' : ''),
               'href'        => $this->url->link('product/category', 'path=' . $category['category_id'] . '_' . $child['category_id'])   
            );      
         }

         $this->data['categories'][] = array(
            'category_id' => $category['category_id'],
            'name'        => $category['name'] . ($this->config->get('config_product_count') ? ' (' . $total . ')' : ''),
            'children'    => $children_data,
            'href'        => $this->url->link('product/category', 'path=' . $category['category_id'])
         );   
      }
      }
      if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/module/category.tpl')) {
         $this->template = $this->config->get('config_template') . '/template/module/category.tpl';
      } else {
         $this->template = 'default/template/module/category.tpl';
      }
      
      $this->render();
     }
}
?>