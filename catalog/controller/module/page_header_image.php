<?php  
class ControllerModulePageHeaderImage extends Controller {
	protected function index($setting) {
		$this->language->load('module/page_header_image');
		
		//getting the page image
		$page_image = $this->document->getPageImage();
		
		if(!empty($page_image)){
			$this->data['main_image'] = $page_image;
		} else {
			$this->data['main_image'] ="/image/data/homepage/homepagestake.png";
		}
		
		$this->data['heading_title'] = $this->language->get('heading_title');
		$this->data['text_filter'] = $this->language->get('text_filter');
		
		$this->data['filter_kitchen'] = $this->language->get('filter_kitchen');
		$this->data['filter_kitchen_placeholder'] = $this->language->get('filter_kitchen_placeholder');
		
		$this->data['filter_by_cost'] = $this->language->get('filter_by_cost');
		$this->data['filter_by_cost_placeholder'] = $this->language->get('filter_by_cost_placeholder');
		
		$this->data['filter_by_kosher'] = $this->language->get('filter_by_kosher');
		$this->data['filter_by_kosher_placeholder'] = $this->language->get('filter_by_kosher_placeholder');
		
		$this->data['find_order'] = $this->language->get('find_order');
		
		if (isset($this->request->get['path'])) {
			$parts = explode('_', (string)$this->request->get['path']);
		} else {
			$parts = array();
		}
		
		if (isset($parts[0])) {
			$this->data['category_id'] = $parts[0];
		} else {
			$this->data['category_id'] = 0;
		}
		
		if (isset($parts[1])) {
			$this->data['child_id'] = $parts[1];
		} else {
			$this->data['child_id'] = 0;
		}
		
		$this->load->model('catalog/category');

		$this->load->model('catalog/product');

		$this->data['categories'] = array();

		$categories = $this->model_catalog_category->getCategories(0);

		foreach ($categories as $category) {
			$total = $this->model_catalog_product->getTotalProducts(array('filter_category_id' => $category['category_id']));
			
			$children_data = array();
			
			$children = $this->model_catalog_category->getCategories($category['category_id']);
			
			foreach ($children as $child) {
				$data = array(
					'filter_category_id'  => $child['category_id'],
					'filter_sub_category' => true
				);
				
				$product_total = $this->model_catalog_product->getTotalProducts($data);
				
				$total += $product_total;
				
				$children_data[] = array(
					'category_id' => $child['category_id'],
					'name'        => $child['name'] . ($this->config->get('config_product_count') ? ' (' . $product_total . ')' : ''),
					'href'        => $this->url->link('product/category', 'path=' . $category['category_id'] . '_' . $child['category_id'])	
				);		
			}
			
			$this->data['categories'][] = array(
				'category_id' => $category['category_id'],
				'name'        => $category['name'] . ($this->config->get('config_product_count') ? ' (' . $total . ')' : ''),
				'children'    => $children_data,
				'href'        => $this->url->link('product/category', 'path=' . $category['category_id'])
			);	
		}
		
		if (isset($this->request->get['route'])){
			if (($this->request->get['route'] == "seller/catalog-seller/profile") && isset($this->request->get['seller_id']) ){
				// loading models
				$this->language->load('product/category');
				$this->load->model('catalog/category');
				$this->load->model('catalog/product');
				$this->load->model('tool/image');
				
				$seller = $this->MsLoader->MsSeller->getSeller($this->request->get['seller_id']);
				$customer = $this->model_account_customer->getCustomerByEmail($seller['c.email']);
				
				if ($customer['image'] && file_exists(DIR_IMAGE . $customer['image'])) {//($seller['ms.avatar'] && file_exists(DIR_IMAGE . $seller['ms.avatar'])) {
					$cimage = $this->model_tool_image->resize($customer['image'], 200, 200);
					//$cimage = $this->MsLoader->MsFile->resizeImage($customer['image'], 200,200);
					//$image = $this->MsLoader->MsFile->resizeImage($seller['ms.avatar'], $this->config->get('msconf_seller_avatar_seller_profile_image_width'), $this->config->get('msconf_seller_avatar_seller_profile_image_height'));
				} else {
					$cimage = $this->model_tool_image->resize('no-avatar-women.png', 200, 200);
					//$cimage = $this->MsLoader->MsFile->resizeImage('ms_no_image.jpg', $this->config->get('msconf_seller_avatar_seller_profile_image_width'), $this->config->get('msconf_seller_avatar_seller_profile_image_height'));
					//$image = $this->MsLoader->MsFile->resizeImage('ms_no_image.jpg', $this->config->get('msconf_seller_avatar_seller_profile_image_width'), $this->config->get('msconf_seller_avatar_seller_profile_image_height'));
				}
				
				$this->load->model('account/address');
				
				$cook_addresses = $this->model_account_address->getAddressesById($customer['customer_id']);
				$cook_address = reset($cook_addresses);
				$cook_city = $cook_address['city'];
				
				/*
				$this->data['seller']['sex'] = $customer['sex'];
				$this->data['seller']['birth_day'] = $customer['birth_day'];
				$this->data['seller']['profile_description'] = $customer['profile_description'];
				$this->data['seller']['image'] = $cimage;
				$this->data['seller']['sp'] = $customer['sp'];
				$this->data['seller']['telephone'] = $customer['telephone'];
				$this->data['seller']['name'] = $customer['firstname'];
				
				$this->data['seller']['info_line_1'] = sprintf($this->language->get('info_line_1'),$cook_city,$customer['sp']);
				$this->data['seller']['info_line_2'] = sprintf($this->language->get('info_line_2'),$cook_address['address_1'],$cook_city);;
				$this->data['seller']['info_line_3'] = sprintf($this->language->get('info_line_3'),$customer['telephone']);;
				
				$this->data['seller']['nickname'] = $seller['ms.nickname'];
				$this->data['seller']['seller_id'] = $seller['seller_id'];
				$this->data['seller']['description'] = html_entity_decode($seller['ms.description'], ENT_QUOTES, 'UTF-8');
				$this->data['seller']['thumb'] = "";//$image;
				$this->data['seller']['href'] = $this->url->link('seller/catalog-seller/products', 'seller_id=' . $seller['seller_id']);
				$this->data['seller_id'] = $this->request->get['seller_id'];
				*/
			}
			
			if ($this->request->get['route'] == "product/category"){
				
			}
		}
		
		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/module/page_header_image.tpl')) {
			$this->template = $this->config->get('config_template') . '/template/module/page_header_image.tpl';
		} else {
			$this->template = 'default/template/module/page_header_image.tpl';
		}
		
		$this->render();
  	}
}
?>