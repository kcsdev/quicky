<?php 
class ControllerProductRoshhashana extends Controller {
	
	public function index() {
		$this->language->load('product/product');
		
		$this->load->model('account/customer');
		$this->load->model('tool/image');
		$this->load->model('catalog/review');
		$this->load->model('account/address');
		
		$this->document->setTitle('תפריט מיוחד לראש השנה');
		$this->document->setOgTitle('תפריט מיוחד לראש השנה');
		$this->document->setDescription('תפריט מיוחד לראש השנה');
		
		
		$roshhashana_meals = $this->db->query("SELECT * FROM roshhashana_meals");
		
		$all_meals = array();
		if($roshhashana_meals->rows){
			foreach($roshhashana_meals->rows as $roshhashana_meal){
				
				$all_meals[$roshhashana_meal['cook_id']][] = $roshhashana_meal;
				
			}	
		}
		
		
		//foreach($all_meals as $seller_id => $meal_info){
		//	print_r($seller_id."<br>");
		//	print_r($meal_info);
		//	print_r("<br><br><br>");
		//}
		//
		//die;
		
		$sellers = array();
		
		foreach($all_meals as $seller_id => $meal_info){
			$seller = $this->MsLoader->MsSeller->getSeller($seller_id);
			$rate = $this->MsLoader->MsSeller->getRate(array('seller_id' => $seller_id, true));
			$customer = $this->model_account_customer->getCustomerByEmail($seller['c.email']);
			
			if ($customer['image'] && file_exists(DIR_IMAGE . $customer['image'])) {
				$cimage = $this->model_tool_image->resize($customer['image'], 200, 200);
			} else {
				$cimage = $this->model_tool_image->resize('no-avatar-women.png', 200, 200);
			}
			
			$cook_addresses = $this->model_account_address->getAddressesById($customer['customer_id']);
			$cook_address = reset($cook_addresses);
			$cook_city = $cook_address['city'];
			
			$review_rating = $this->model_catalog_review->getRatingBySellerId($seller_id);
			$review_total = $this->model_catalog_review->getTotalReviewsBySellerId($seller_id);
			
			$positive_reviews = 0;
			foreach($review_rating as $rating){
				if ($rating['rating'] > 3){
					$positive_reviews++;
				}
			}
			if($positive_reviews){
				$positive_rating = (round($positive_reviews / count($review_rating),2)*100);
			} else {
				$positive_rating = 0;
			}
			
			$meals = array();
			foreach($meal_info as $meal){
				$meals[$meal['cat']][] = $meal;
			}
			
			$sellers[] = array(
							"seller_id" 		=> $seller_id, //$this->data['seller_id'] = ;
							"sex" 				=> $customer['sex'], //$this->data['customer']['sex'] = $customer['sex'];
							"birth_day"			=> $customer['birth_day'], //$this->data['customer']['birth_day'] = $customer['birth_day'];
							"image" 			=> $cimage, //$this->data['customer']['image'] = $cimage;
							"sp" 				=> $customer['sp'], // $this->data['customer']['sp'] = $customer['sp'];
							"telephone" 		=> $customer['telephone'],//$this->data['customer']['telephone'] = $customer['telephone'];
							"name" 				=> $customer['firstname'],
							"city" 				=> $cook_city,
							"nickname" 			=> $seller['ms.nickname'],
							"href" 				=> $this->url->link('seller/catalog-seller/products', 'seller_id=' . $seller_id),//this->data['seller']['href'] = $this->url->link('seller/catalog-seller/products', 'seller_id=' . $seller_id);
							"review_rating" 	=> $positive_rating, //$this->data['review_rating'] = $positive_rating;
							"review_total" 		=> $review_total, //$this->data['review_total'] = $review_total;
							"meals_cat_1" 		=> isset($meals['ראשונות']) ? $meals['ראשונות'] : false, //$this->data['review_total'] = $review_total;
							"meals_cat_2" 		=> isset($meals['מרקים']) ? $meals['מרקים'] : false, //$this->data['review_total'] = $review_total;
							"meals_cat_3" 		=> isset($meals['עיקריות']) ? $meals['עיקריות'] : false, //$this->data['review_total'] = $review_total;
							"meals_cat_4" 		=> isset($meals['תוספות']) ? $meals['תוספות'] : false, //$this->data['review_total'] = $review_total;
							"meals_cat_5" 		=> isset($meals['עוגות']) ? $meals['עוגות'] : false, //$this->data['review_total'] = $review_total;
							);
		}
		
		function cmp($a, $b)
		{
			if ($a['review_rating'] == $b['review_rating']) {
				return 0;
			}
			return ($a['review_rating'] < $b['review_rating']) ? 1 : -1;
		}
		
		usort($sellers, "cmp");
		
		$this->data['sellers'] = $sellers;
		
		$this->data['heading_title'] = 'תפריט מיוחד לראש השנה';
		
		$this->data['text_sp'] = $this->language->get('text_sp');
		$this->data['text_contactme'] = $this->language->get('text_contactme');
		$this->data['text_share'] = $this->language->get('text_share');
		$this->data['text_aboutme'] = $this->language->get('text_aboutme');
		$this->data['text_mymeals'] = $this->language->get('text_mymeals');
		$this->data['text_cookreviews'] = $this->language->get('text_cookreviews');
		$this->data['text_rating'] = $this->language->get('text_rating');
		$this->data['text_recommended'] = $this->language->get('text_recommended');
		$this->data['text_reviews_text'] = $this->language->get('text_reviews_text');
		$this->data['text_per_meal'] = $this->language->get('text_per_meal');
		
		$this->data['text_empty'] = $this->language->get('text_empty');
		$this->data['text_quantity'] = $this->language->get('text_quantity');
		$this->data['text_manufacturer'] = $this->language->get('text_manufacturer');
		$this->data['text_model'] = $this->language->get('text_model');
		$this->data['text_price'] = $this->language->get('text_price');
		$this->data['text_tax'] = $this->language->get('text_tax');
		$this->data['text_points'] = $this->language->get('text_points');
		$this->data['text_compare'] = sprintf($this->language->get('text_compare'), (isset($this->session->data['compare']) ? count($this->session->data['compare']) : 0));
		$this->data['text_display'] = $this->language->get('text_display');
		$this->data['text_list'] = $this->language->get('text_list');
		$this->data['text_grid'] = $this->language->get('text_grid');		
		$this->data['text_sort'] = $this->language->get('text_sort');
		$this->data['text_limit'] = $this->language->get('text_limit');
		
		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/product/roshhashana.tpl')) {
			$this->template = $this->config->get('config_template') . '/template/product/roshhashana.tpl';
		} else {
			$this->template = 'default/template/product/special.tpl';
		}
		
		$this->children = array(
			'common/column_left',
			'common/column_right',
			'common/content_top',
			'common/content_bottom',
			'common/footer',
			'common/header'
		);
		
		$this->response->setOutput($this->render());		
  	}
}
?>