<?php 
class ControllerProductCategory extends Controller {  
	public function index() {
		
		if(!isset($this->request->get['path'])){
			$this->request->get['path'] = 0;
		}
		
		/*
		// time set for showing the oppsite products
		if(!isset($this->session->data['cut_off_time'])){
			$cut_off_time = strtotime("today 15:00");
			$this->session->data['cut_off_time'] = strtotime("today 15:00");
			$this->data['cut_off_time'] = $cut_off_time;
		} else {
			$cut_off_time = $this->session->data['cut_off_time'];
			$this->data['cut_off_time'] = $cut_off_time;
		}
		*/
		
		/*
		// saving address - dont need it now
		if(isset($this->request->request['city'])){
			$this->data['city'] = $this->request->request['city'];
			$this->session->data['city'] = $this->request->request['city'];
			
			$this->data['street_name'] = $this->request->request['street_name'];
			$this->session->data['street_name'] = $this->request->request['street_name'];
			
			$this->data['street_number'] = $this->request->request['street_number'];
			$this->session->data['street_number'] = $this->request->request['street_number'];
			
		} elseif (isset($this->session->data['city'])) {
			$this->data['city'] = $this->session->data['city'];
			$this->data['street_name'] = $this->session->data['street_name'];
			$this->data['street_number'] = $this->session->data['street_number'];
		} else {
			$this->data['city'] = "";
			$this->data['street_name'] = "";
			$this->data['street_number'] = "";
		}
		*/
		
		// loading models
		$this->language->load('product/category');
		$this->load->model('catalog/category');
		$this->load->model('catalog/product');
		$this->load->model('tool/image'); 
		
		if (isset($this->request->get['sort'])) {
			$sort = $this->request->get['sort'];
		} else {
			$sort = 'pd.name';
		}

		if (isset($this->request->get['order'])) {
			$order = $this->request->get['order'];
		} else {
			$order = 'ASC';
		}
		
		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else { 
			$page = 1;
		}	
		
		if (isset($this->request->get['limit'])) {
			$limit = $this->request->get['limit'];
		} else {
			$limit = $this->config->get('config_catalog_limit');
		}
		
		$this->data['breadcrumbs'] = array();
		
   		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home'),
			'separator' => false
   		);
		
		if (isset($this->session->data['success'])) {
			$this->data['success'] = $this->session->data['success'];
			unset($this->session->data['success']);
		} else {
			$this->data['success'] = '';
		}
		
		
		/***************************
		 * For sorting by cook
		 *
		 *************************/
		$cart_items = $this->cart->getProducts();
		
		if (!empty($cart_items)){
			$cart_item = reset($cart_items);
			
			$this->data['cart_delivery_day'] 	= strftime($this->language->get('date_time_format_strf'),strtotime($cart_item['date']));
			$this->data['cart_delivery_time'] 	= $cart_item['time'].":30 - ".($cart_item['time'] - 1).":30";
			$this->data['cart_delivery_text'] 	= $this->language->get('cart_delivery_text');
		}else{
			$cart_item = array();
			$cart_item['cook'] = 0;
		}
		
		$this->data['cook'] = array();
		
		if (true){//isset($this->request->get['cook']) && $this->request->get['cook'] == 'false') {
			$curr_cook = 0;
		} else if($cart_item['cook'] != 0) {
			$curr_cook = $cart_item['cook'];
			
			$this->session->data['cook'] = $curr_cook;
			$this->load->model('account/customer');
			$cook_info = $this->model_account_customer->getCustomer($curr_cook);
			
			if(!empty($cook_info['image'])){
				if (strpos($cook_info['image'], 'http') === 0) {
					$author_pic = $cook_info['pic_square'];
				}else{
					$author_pic = $this->model_tool_image->resize($cook_info['image'], 100, 101);
				}
			} else {
				$author_pic = "";
			}
			
			$this->load->model('account/address');
			
			$customer_address = $this->model_account_address->getAddressesById((int)$cook_info["customer_id"]);
			$customer_address = reset($customer_address);
			
			$cook_city = $customer_address['city'];
			
			setlocale(LC_ALL, 'he_IL.UTF-8');
			$this->data['cook'] = array(
				'cook_id'   => $cook_info["customer_id"],
				'firstname' => $cook_info["firstname"],
				'cook_name' => sprintf($this->language->get('cook_name'),$cook_info['firstname'],$cook_info['lastname']),
				'sp' => $cook_info["sp"],
				'delivery_day' => strftime($this->language->get('date_time_format_strf'),strtotime($cart_item['date'])),
				'delivery_time' => ($cart_item['time']).":00 - ".($cart_item['time'] - 1).":00",
				'image' => $author_pic,
				'city'	=> $cook_city
			);
			
		} else {
			$curr_cook = 0;
		}
		
		if (isset($this->request->get['path'])) {
			$path = '';
		
			$parts = explode('_', (string)$this->request->get['path']);
		
			foreach ($parts as $path_id) {
				if (!$path) {
					$path = (int)$path_id;
				} else {
					$path .= '_' . (int)$path_id;
				}
									
				$category_info = $this->model_catalog_category->getCategory($path_id);
				
				if ($category_info) {
	       			$this->data['breadcrumbs'][] = array(
   	    				'text'      => $category_info['name'],
						'href'      => $this->url->link('product/category', 'path=' . $path),
        				'separator' => $this->language->get('text_separator')
        			);
				}
			}		
		
			$category_id = (int)array_pop($parts);
		} else {
			$category_id = 0;
		}
		
		// for more power over catagories
		if (isset($this->request->get['exclude'])) {
			$exclude = explode('_', (string)$this->request->get['exclude']);
			
			$allcatagorys = $this->model_catalog_category->getCategories();
		
			$wanted_cats = array();
			
			foreach($allcatagorys as $cata_info){
				if($cata_info['top'] == 1 && !in_array($cata_info['category_id'],$exclude) ){
					$wanted_cats[] = $cata_info['category_id'];
				}
			}
			
			$include = $wanted_cats;
			
		} else {
			$exclude = 0;
			$include = 0;
		}
		
		if (isset($this->request->get['sort'])) $sort = $this->request->get['sort']; else $sort = 'p.date_added';
		if (isset($this->request->get['order'])) $order = $this->request->get['order']; else $order = 'DESC';
		
		$this->data['special_block'] = $module = $this->getChild('module/latest', array(
		    'limit' => 20,
		    'image_width' => 185,
		    'category' => $category_id,
		    'start' => 0,
		    'sort' => $sort,
		    'order' => $order
		));
		
		$category_info = $this->model_catalog_category->getCategory($category_id);
		
		if (($category_info) OR ($category_id==0)) {
			
			if($this->customer->isLogged() && $this->customer->getAddressId()){
				$this->load->model('account/address');
				$address_id = $this->customer->getAddressId();
				$def_address = $this->model_account_address->getAddress($address_id);
			} else {
				$def_address = false;
			}
			
			if ($category_id==0) {
				$this->data['heading_title'] = $this->document->getTitle();
				
				$category_info['description']='';
				$category_info['image']='';
			} else {
				$this->document->setDescription($category_info['meta_description']);
				$this->document->setKeywords($category_info['meta_keyword']);
				$this->data['heading_title'] = $category_info['name'];
			}
			
			$this->data['text_refine'] = $this->language->get('text_refine');
			$this->data['text_empty'] = $this->language->get('text_empty');
			$this->data['text_no_pros'] = $this->language->get('text_no_pros'); // homeals added
			$this->data['text_quantity'] = $this->language->get('text_quantity');
			$this->data['text_manufacturer'] = $this->language->get('text_manufacturer');
			$this->data['text_model'] = $this->language->get('text_model');
			$this->data['text_price'] = $this->language->get('text_price');
			$this->data['text_tax'] = $this->language->get('text_tax');
			$this->data['text_points'] = $this->language->get('text_points');
			$this->data['text_compare'] = sprintf($this->language->get('text_compare'), (isset($this->session->data['compare']) ? count($this->session->data['compare']) : 0));
			$this->data['text_display'] = $this->language->get('text_display');
			$this->data['text_list'] = $this->language->get('text_list');
			$this->data['text_grid'] = $this->language->get('text_grid');
			$this->data['text_sort'] = $this->language->get('text_sort');
			$this->data['text_limit'] = $this->language->get('text_limit');
			$this->data['text_sp'] = $this->language->get('text_sp');
			$this->data['text_cook_page_heading'] = $this->language->get('text_cook_page_heading');
			
			$this->data['text_delivery'] = $this->language->get('text_delivery');
			$this->data['text_cook'] = $this->language->get('text_cook');
			$this->data['text_next_day'] = $this->language->get('text_next_day');
			
			$this->data['text_permeal'] = $this->language->get('text_permeal');
			$this->data['text_reviewsmin'] = $this->language->get('text_reviewsmin');
			$this->data['button_cart'] = $this->language->get('button_cart');
			$this->data['button_wishlist'] = $this->language->get('button_wishlist');
			$this->data['button_compare'] = $this->language->get('button_compare');
			$this->data['button_continue'] = $this->language->get('button_continue');
			
			if ($category_info['image']) {
				$this->data['thumb'] = $this->model_tool_image->resize($category_info['image'], $this->config->get('config_image_category_width'), $this->config->get('config_image_category_height'));
			} else {
				$this->data['thumb'] = '';
			}
			
			$this->data['description'] = html_entity_decode($category_info['description'], ENT_QUOTES, 'UTF-8');
			$this->data['compare'] = $this->url->link('product/compare');
			
			$url = '';
			
			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}	

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}	
			
			if (isset($this->request->get['limit'])) {
				$url .= '&limit=' . $this->request->get['limit'];
			}
			
			$this->data['categories'] = array();
			
			$results = $this->model_catalog_category->getCategories($category_id);
			
			// cat fillter tags
			foreach ($results as $result) {
				$data = array(
					'filter_category_id'  => $result['category_id'],
					'filter_sub_category' => true
				);
				
				$product_total = $this->model_catalog_product->getTotalProducts($data);
				
				if($exclude){
					
					$exclude_this = false;
					
					foreach($exclude as $cat_id){
						if($cat_id == $result['category_id']) $exclude_this = true;
					}
					
					if(!$exclude_this){
						$this->data['categories'][] = array(
							'cat_id' => $result['category_id'],
							'name'  => $result['name'] . ($this->config->get('config_product_count') ? ' (' . $product_total . ')' : ''),
							'href'  => $this->url->link('product/category', 'path=' . $this->request->get['path'] . '&exclude=' .$this->request->get['exclude']. '_' . $result['category_id'] . $url)
						);
					}
					
				}else{
					
					$this->data['categories'][] = array(
						'cat_id' => $result['category_id'],
						'name'  => $result['name'] . ($this->config->get('config_product_count') ? ' (' . $product_total . ')' : ''),
						'href'  => $this->url->link('product/category', 'path=' . $this->request->get['path'] . '&exclude=' . $result['category_id'] . $url)
					);
					
				}
				
			}
			
			if(empty($this->data['categories']) && isset($this->request->get['exclude'])){
				$url = "index.php?route=product/category&path=0";
				
				?>
				<script>
					window.location = "<?php echo $url; ?>"
				</script>
				<?php
				
				die;
			}
			
			// cat filter tags end
			$meal_type = 0;
			 
			/*******************************************************
			 *
			 *	adding a good day if this day has no orders
			 *
			 *******************************************************/
			
			/*
			 
			$i = 0;
			$j = 0;
			
			$days = array();
			$days_more = array();
			
			while($i < 1 && $j < 40){
				
				$week_day = date("m/d/y", strtotime('+'.$j.' day'));
				
				$data = array(
					'filter_category_id' 	=> $category_id,
					'filter_exclude' 	=> $exclude,
					'filter_meal_type'   	=> $meal_type,
					'filter_customer_id' 	=> $curr_cook,
					'filter_day_available'  => $week_day
				);
				
				$results = $this->model_catalog_product->getProducts($data);
				
				$products_numbers = count($results);
				
				$op_data = array(
					'filter_category_id' 	=> $category_id,
					'filter_exclude' 	=> $exclude,
					'filter_meal_type'   	=> $meal_type,
					'show_opposite'   	=> "true",
					'filter_customer_id' 	=> $curr_cook,
					'filter_day_available'  => $week_day
				);
				
				$results_op = $this->model_catalog_product->getProducts($op_data);
				
				$op_products_numbers = count($results_op);
				
				if( ($products_numbers == 0) && ($op_products_numbers > 0) && ( strtotime("now") < $cut_off_time) ) {
					$days[0] = date('D', strtotime('today'));
					$days_more[0] = 0;
					$i++;
				}
				
				
				if($products_numbers > 0) {
					$days[$i] = date('D', strtotime('+'.$j.' day'));
					$days_more[$i] = $j;
					$i++;
				}
				
				$j++;
			}
			// fixing day date end

			$this->data['products'] = array();
			
			if (isset($cart_item['date'])) {
				$week_day = $cart_item['date'];
				$this->session->data['date'] = date("m/d/y",strtotime($cart_item['date']));
			} else if (isset($this->request->get['day'])) {
				$week_day = date("m/d/y",strtotime($this->request->get['day']));
				$this->session->data['date'] = $week_day;
			} else if (isset($this->session->data['date']) && (strtotime($this->session->data['date']) > strtotime($days[0]))) {
				$week_day = date("m/d/y",strtotime($this->session->data['date']));
				$this->session->data['date'] = $week_day;
			} else {
				//$week_day = date("m/d/y");
				$week_day = date("m/d/y",strtotime($days[0]));
				$this->session->data['date'] = $week_day;
			}
			
			*/
			
			$data = array(
				'filter_category_id' 	=> $category_id,
				'filter_meal_type'   	=> $meal_type,
				'filter_customer_id' 	=> $curr_cook,
				'filter_day_available'  => 'all',
				'sort'               	=> $sort,
				'order'              	=> $order,
				'start'              	=> ($page - 1) * $limit,
				'limit'              	=> $limit
			);
			
			$product_total = $this->model_catalog_product->getTotalProducts($data);
			
			$results = $this->model_catalog_product->getProducts($data);
			
			$this->data['product_number'] = count($results);
			
			date_default_timezone_set("Asia/Jerusalem");
			
			$this->data['only_cook_title'] = $this->language->get('only_cook_title');
			$this->data['only_cook_text1'] = $this->language->get('only_cook_text1');
			$this->data['only_cook_text2'] = $this->language->get('only_cook_text2');
			
			$this->data['text_close'] = $this->language->get('text_close');
			
			foreach ($results as $result) {
				if ($result['image']) {
					$image = $this->model_tool_image->resize($result['image'], $this->config->get('config_image_product_width'), $this->config->get('config_image_product_height'));
				} else {
					$image = $this->model_tool_image->resize("no_image.jpg", $this->config->get('config_image_product_width'), $this->config->get('config_image_product_height'));
				}
				
				if (($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) {
					$price = $this->currency->format($this->tax->calculate($result['price'], $result['tax_class_id'], $this->config->get('config_tax')));
				} else {
					$price = false;
				}
				
				if ((float)$result['special']) {
					$special = $this->currency->format($this->tax->calculate($result['special'], $result['tax_class_id'], $this->config->get('config_tax')));
				} else {
					$special = false;
				}	
				
				if ($this->config->get('config_tax')) {
					$tax = $this->currency->format((float)$result['special'] ? $result['special'] : $result['price']);
				} else {
					$tax = false;
				}				
				
				if ($this->config->get('config_review_status')) {
					$rating = (int)$result['rating'];
				} else {
					$rating = false;
				}
				
				if ($result['user_image']) {
					
					$user_image = $this->model_tool_image->resize($result['user_image'], 90, 90);
					
					
				} else {
					$user_image = 'image/no-avatar-women.png';
				}
				
				$wordchop = mb_truncate($result['name'],50);
				
				$side_results = $this->model_catalog_product->getProductRelated($result['product_id']);
				$side_data = array();
				
				foreach ($side_results as $side_result) {
					if ($side_result['image']) {
						$side_image = $this->model_tool_image->resize($side_result['image'], $this->config->get('config_image_related_width'), $this->config->get('config_image_related_height'));
						$review_image = $this->model_tool_image->resize($side_result['image'], 212, 145);
					} else {
						$side_image = $this->model_tool_image->resize('no_image.jpg', $this->config->get('config_image_related_width'), $this->config->get('config_image_related_height'));
						$review_image = $this->model_tool_image->resize('no_image.jpg', 212, 145);
					}
					
					if (($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) {
						$side_price = $this->currency->format($this->tax->calculate($side_result['price'], $side_result['tax_class_id'], $this->config->get('config_tax')));
						$side_price_int = (int)$side_result['price'];
					} else {
						$side_price = false;
						$side_price_int = false;
					}
					
					
					$side_data[] = array(
						'product_id' 	 => $side_result['product_id'],
						'thumb'   	 => $side_image,
						'image'   	 => $review_image,
						'name'    	 => $side_result['name'],
						'price'   	 => $side_price,
						'price_int'   	 => $side_price_int,
						'text_extra' 	 => sprintf($this->language->get('text_extra'), $price),
						'href'    	 => $this->url->link('product/product', 'product_id=' . $side_result['product_id']),
					);
				}
				
				if($result['number_of_sides'] == 2){
					$total_sides = sprintf($this->language->get('total_sides_cat'), $result['number_of_sides']);
				}elseif($result['number_of_sides'] == 1){
					$total_sides = sprintf($this->language->get('total_sides_cat'), $result['number_of_sides']);
				}elseif($result['number_of_sides'] == 0 && count($side_data) > 0 ){
					$total_sides = sprintf($this->language->get('total_sides_cat'), count($side_data));
				}elseif($result['number_of_sides'] == 0 ){
					$total_sides = "";
				}
				
				$this->data['products'][] = array(
					'product_id'  => $result['product_id'],
					'thumb'       => $image,
					'name'        => $wordchop,
					'user_id'     => $result['user_id'],
					'username'    => $result['username'],
					'totalsides'  => $total_sides,
					'user_image'  => $user_image,
					'pro_cat'     => $result['pro_cat'],
					'user_href'   => $this->url->link('seller/catalog-seller/profile', 'seller_id='. $result['user_id']),
					'sp'	      => $result['sp'],
					'is_verified'	      => $result['is_verified'],
					'description' => utf8_substr(strip_tags(html_entity_decode($result['description'], ENT_QUOTES, 'UTF-8')), 0, 100) . '..',
					'price'       => $price,
					'special'     => $special,
					'tax'         => $tax,
					'sides'	      => $side_data,
					'rating'      => $result['rating'],
					'reviews'     => (int)$result['reviews'],
					'reviews_text'     => (int)$result['reviews_text'],
					'sort_rating'     => $result['sort_rating'],
					'last_order_time' => sprintf($this->language->get('last_order_time_catpage'),$result['last_order_time']),
					'href'        => $this->url->link('product/product', 'path=' . $this->request->get['path'] . '&product_id=' . $result['product_id'])
				);
			}
			
			$this->data['text_order_time_passed'] = $this->language->get('text_order_time_passed');
			
			/*
			if($results){
				$this->data['text_ot_title'] = sprintf($this->language->get('text_ot_title'),strftime('%A',strtotime($week_day)));
			} else {
				$this->data['text_ot_title'] = sprintf($this->language->get('text_ot_title_no_orders'),strftime('%A',strtotime($week_day)));
			}
			*/
			
			$this->data['text_ot_subtitle'] = $this->language->get('text_ot_subtitle');
			
			/*
			if ( $week_day == date("m/d/y",strtotime("today")) ) {
				$data = array(
					'filter_category_id' 	=> $category_id,
					'filter_exclude' 	=> $include,
					'filter_meal_type'   	=> $meal_type,
					'show_opposite'   	=> "true",
					'filter_customer_id' 	=> $curr_cook,
					'filter_day_available'  => $week_day,
					'sort'               	=> $sort,
					'order'              	=> $order
				);
				
				$results_op = $this->model_catalog_product->getProducts($data);
					
				foreach ($results_op as $result) {
					if ($result['image']) {
						$image = $this->model_tool_image->resize($result['image'], $this->config->get('config_image_product_width'), $this->config->get('config_image_product_height'));
					} else {
						$image = $this->model_tool_image->resize("no_image.jpg", $this->config->get('config_image_product_width'), $this->config->get('config_image_product_height'));
					}
					
					if (($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) {
						$price = $this->currency->format($this->tax->calculate($result['price'], $result['tax_class_id'], $this->config->get('config_tax')));
					} else {
						$price = false;
					}
					
					if ((float)$result['special']) {
						$special = $this->currency->format($this->tax->calculate($result['special'], $result['tax_class_id'], $this->config->get('config_tax')));
					} else {
						$special = false;
					}	
					
					if ($this->config->get('config_tax')) {
						$tax = $this->currency->format((float)$result['special'] ? $result['special'] : $result['price']);
					} else {
						$tax = false;
					}				
					
					if ($this->config->get('config_review_status')) {
						$rating = (int)$result['rating'];
					} else {
						$rating = false;
					}
					
					if ($result['user_image']) {
						
						$user_image = $this->model_tool_image->resize($result['user_image'], 90, 90);
						
						
					} else {
						$user_image = 'image/no-avatar-women.png';
					}
					
					$wordchop = mb_truncate($result['name'],60);
					
					
					
					$side_results = $this->model_catalog_product->getProductRelated($result['product_id']);
					$side_data = array();
					
					foreach ($side_results as $side_result) {
						if ($side_result['image']) {
							$side_image = $this->model_tool_image->resize($side_result['image'], $this->config->get('config_image_related_width'), $this->config->get('config_image_related_height'));
							$review_image = $this->model_tool_image->resize($side_result['image'], 212, 145);
						} else {
							$side_image = $this->model_tool_image->resize('no_image.jpg', $this->config->get('config_image_related_width'), $this->config->get('config_image_related_height'));
							$review_image = $this->model_tool_image->resize('no_image.jpg', 212, 145);
						}
						
						if (($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) {
							$side_price = $this->currency->format($this->tax->calculate($side_result['price'], $side_result['tax_class_id'], $this->config->get('config_tax')));
							$side_price_int = (int)$side_result['price'];
						} else {
							$side_price = false;
							$side_price_int = false;
						}
						
						
						$side_data[] = array(
							'product_id' 	 => $side_result['product_id'],
							'thumb'   	 => $side_image,
							'image'   	 => $review_image,
							'name'    	 => $side_result['name'],
							'price'   	 => $side_price,
							'price_int'   	 => $side_price_int,
							'text_extra' 	 => sprintf($this->language->get('text_extra'), $price),
							'href'    	 => $this->url->link('product/product', 'product_id=' . $side_result['product_id']),
						);
					}
					
					if($result['number_of_sides'] == 2){
						$total_sides = sprintf($this->language->get('total_sides_cat'), $result['number_of_sides']);
					}elseif($result['number_of_sides'] == 1){
						$total_sides = sprintf($this->language->get('total_sides_cat'), $result['number_of_sides']);
					}elseif($result['number_of_sides'] == 0 && count($side_data) > 0 ){
						$total_sides = sprintf($this->language->get('total_sides_cat'), count($side_data));
					}elseif($result['number_of_sides'] == 0 ){
						$total_sides = "";
					}
					
					if($result['last_order_time']){
						$last_order_text = sprintf($this->language->get('last_order_time_catpage'),$result['last_order_time']);
					} else {
						$last_order_text = "אזל המלאי";
					}
					
					$this->data['opposite_products'][] = array(
						'product_id'  => $result['product_id'],
						'thumb'       => $image,
						'name'        => $wordchop,
						'user_id'     => $result['user_id'],
						'username'    => $result['username'],
						'totalsides'  => $total_sides,
						'user_image'  => $user_image,
						'pro_cat'     => $result['pro_cat'],
						'user_href'   => $this->url->link('seller/catalog-seller/profile', 'seller_id='. $result['user_id']),
						'sp'	      => $result['sp'],
						'is_verified'	      => $result['is_verified'],
						'description' => utf8_substr(strip_tags(html_entity_decode($result['description'], ENT_QUOTES, 'UTF-8')), 0, 100) . '..',
						'price'       => $price,
						'special'     => $special,
						'tax'         => $tax,
						'sides'	      => $side_data,
						'rating'      => $result['rating'],
						'reviews'     => (int)$result['reviews'],
						'reviews_text'     => (int)$result['reviews_text'],
						'sort_rating'     => $result['sort_rating'],
						'last_order_time' => $last_order_text,
						'href'        => $this->url->link('product/product', 'path=' . $this->request->get['path'] . '&product_id=' . $result['product_id'])
					);
				}
			}
			*/
			
			$url = '';
			
			if (isset($this->request->get['limit'])) {
				$url .= '&limit=' . $this->request->get['limit'];
			}
							
			$this->data['sorts'] = array();
			
			$this->data['sorts'][] = array(
				'text'  => $this->language->get('text_default'),
				'value' => 'p.sort_order-ASC',
				'href'  => $this->url->link('product/category', 'path=' . $this->request->get['path'] . '&sort=p.sort_order&order=ASC' . $url)
			);
			
			$this->data['sorts'][] = array(
				'text'  => $this->language->get('text_name_asc'),
				'value' => 'pd.name-ASC',
				'href'  => $this->url->link('product/category', 'path=' . $this->request->get['path'] . '&sort=pd.name&order=ASC' . $url)
			);
			
			$this->data['sorts'][] = array(
				'text'  => $this->language->get('text_name_desc'),
				'value' => 'pd.name-DESC',
				'href'  => $this->url->link('product/category', 'path=' . $this->request->get['path'] . '&sort=pd.name&order=DESC' . $url)
			);
			
			$this->data['sorts'][] = array(
				'text'  => $this->language->get('text_price_asc'),
				'value' => 'p.price-ASC',
				'href'  => $this->url->link('product/category', 'path=' . $this->request->get['path'] . '&sort=p.price&order=ASC' . $url)
			); 
			
			$this->data['sorts'][] = array(
				'text'  => $this->language->get('text_price_desc'),
				'value' => 'p.price-DESC',
				'href'  => $this->url->link('product/category', 'path=' . $this->request->get['path'] . '&sort=p.price&order=DESC' . $url)
			); 
			
			if ($this->config->get('config_review_status')) {
				$this->data['sorts'][] = array(
					'text'  => $this->language->get('text_rating_desc'),
					'value' => 'rating-DESC',
					'href'  => $this->url->link('product/category', 'path=' . $this->request->get['path'] . '&sort=rating&order=DESC' . $url)
				); 
				
				$this->data['sorts'][] = array(
					'text'  => $this->language->get('text_rating_asc'),
					'value' => 'rating-ASC',
					'href'  => $this->url->link('product/category', 'path=' . $this->request->get['path'] . '&sort=rating&order=ASC' . $url)
				);
			}
			
			$this->data['sorts'][] = array(
				'text'  => $this->language->get('text_model_asc'),
				'value' => 'p.model-ASC',
				'href'  => $this->url->link('product/category', 'path=' . $this->request->get['path'] . '&sort=p.model&order=ASC' . $url)
			);

			$this->data['sorts'][] = array(
				'text'  => $this->language->get('text_model_desc'),
				'value' => 'p.model-DESC',
				'href'  => $this->url->link('product/category', 'path=' . $this->request->get['path'] . '&sort=p.model&order=DESC' . $url)
			);
			
			$url = '';
	
			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}	

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}
			
			$this->data['limits'] = array();
			
			$this->data['limits'][] = array(
				'text'  => $this->config->get('config_catalog_limit'),
				'value' => $this->config->get('config_catalog_limit'),
				'href'  => $this->url->link('product/category', 'path=' . $this->request->get['path'] . $url . '&limit=' . $this->config->get('config_catalog_limit'))
			);
						
			$this->data['limits'][] = array(
				'text'  => 25,
				'value' => 25,
				'href'  => $this->url->link('product/category', 'path=' . $this->request->get['path'] . $url . '&limit=25')
			);
			
			$this->data['limits'][] = array(
				'text'  => 50,
				'value' => 50,
				'href'  => $this->url->link('product/category', 'path=' . $this->request->get['path'] . $url . '&limit=50')
			);

			$this->data['limits'][] = array(
				'text'  => 75,
				'value' => 75,
				'href'  => $this->url->link('product/category', 'path=' . $this->request->get['path'] . $url . '&limit=75')
			);
			
			$this->data['limits'][] = array(
				'text'  => 100,
				'value' => 100,
				'href'  => $this->url->link('product/category', 'path=' . $this->request->get['path'] . $url . '&limit=100')
			);
						
			$url = '';
	
			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}	

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}
	
			if (isset($this->request->get['limit'])) {
				$url .= '&limit=' . $this->request->get['limit'];
			}
					
			$pagination = new Pagination();
			$pagination->total = $product_total;
			$pagination->page = $page;
			$pagination->limit = $limit;
			$pagination->text = $this->language->get('text_pagination');
			$pagination->url = $this->url->link('product/category', 'path=' . $this->request->get['path'] . $url . '&page={page}');
			
			$this->data['pagination'] = $pagination->render();
			
			$this->data['sort'] = $sort;
			$this->data['order'] = $order;
			$this->data['limit'] = $limit;
			
			$this->data['continue'] = $this->url->link('common/home');
			
			if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/product/category.tpl')) {
				$this->template = $this->config->get('config_template') . '/template/product/category.tpl';
			} else {
				$this->template = 'default/template/product/category.tpl';
			}
			
			$this->children = array(
				'common/column_left',
				'common/column_right',
				'common/content_top',
				'common/content_bottom',
				'common/footer',
				'common/header'
			);
				
			$this->response->setOutput($this->render());										
    	} else {
			$url = '';
			
			if (isset($this->request->get['path'])) {
				$url .= '&path=' . $this->request->get['path'];
			}
									
			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}	

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}
				
			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}
						
			if (isset($this->request->get['limit'])) {
				$url .= '&limit=' . $this->request->get['limit'];
			}
						
			$this->data['breadcrumbs'][] = array(
				'text'      => $this->language->get('text_error'),
				'href'      => $this->url->link('product/category', $url),
				'separator' => $this->language->get('text_separator')
			);
				
			$this->document->setTitle($this->language->get('text_error'));

      		$this->data['heading_title'] = $this->language->get('text_error');

      		$this->data['text_error'] = $this->language->get('text_error');

      		$this->data['button_continue'] = $this->language->get('button_continue');

      		$this->data['continue'] = $this->url->link('common/home');

			if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/error/not_found.tpl')) {
				$this->template = $this->config->get('config_template') . '/template/error/not_found.tpl';
			} else {
				$this->template = 'default/template/error/not_found.tpl';
			}
			
			$this->children = array(
				'common/column_left',
				'common/column_right',
				'common/content_top',
				'common/content_bottom',
				'common/footer',
				'common/header'
			);
					
			$this->response->setOutput($this->render());
		}
  	}
}
?>