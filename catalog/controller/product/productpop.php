<?php
class ControllerProductProductpop extends Controller {

   public function index(){
   
     $product_id = $_GET['product_id'];
      $this->load->model('catalog/product');
		
     $product_info = $this->model_catalog_product->getProduct($product_id);
       
     $this->template = $this->config->get('config_template') . '/template/product/productpop.tpl';
       
     $this->response->setOutput($this->render());
    }

 public function countProducts() {
        $total = 0;
        
        foreach ($this->session->data['cart'] as $value) {
            $total += $value;
        }
        
        return $total;
      }


}