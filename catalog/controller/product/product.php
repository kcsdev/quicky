<?php  
class ControllerProductProduct extends Controller {
	private $error = array();
	
	public function index() {
		$this->language->load('product/product');
		
		$this->data['breadcrumbs'] = array();
		
		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home'),			
			'separator' => false
		);
		
		$this->load->model('catalog/category');	
		
		if (isset($this->request->get['path'])) {
			$path = '';
				
			foreach (explode('_', $this->request->get['path']) as $path_id) {
				if (!$path) {
					$path = $path_id;
				} else {
					$path .= '_' . $path_id;
				}
				
				$category_info = $this->model_catalog_category->getCategory($path_id);
				
				if ($category_info) {
					$this->data['breadcrumbs'][] = array(
						'text'      => $category_info['name'],
						'href'      => $this->url->link('product/category', 'path=' . $path),
						'separator' => $this->language->get('text_separator')
					);
				}
			}
		}
		
		
		$this->load->model('catalog/manufacturer');	
		
		if (isset($this->request->get['manufacturer_id'])) {
			$this->data['breadcrumbs'][] = array( 
				'text'      => $this->language->get('text_brand'),
				'href'      => $this->url->link('product/manufacturer'),
				'separator' => $this->language->get('text_separator')
			);	
				
			$manufacturer_info = $this->model_catalog_manufacturer->getManufacturer($this->request->get['manufacturer_id']);

			if ($manufacturer_info) {	
				$this->data['breadcrumbs'][] = array(
					'text'	    => $manufacturer_info['name'],
					'href'	    => $this->url->link('product/manufacturer/info', 'manufacturer_id=' . $this->request->get['manufacturer_id']),					
					'separator' => $this->language->get('text_separator')
				);
			}
		}
		
		if (isset($this->request->get['filter_name']) || isset($this->request->get['filter_tag'])) {
			$url = '';
			
			if (isset($this->request->get['filter_name'])) {
				$url .= '&filter_name=' . $this->request->get['filter_name'];
			}
						
			if (isset($this->request->get['filter_tag'])) {
				$url .= '&filter_tag=' . $this->request->get['filter_tag'];
			}
						
			if (isset($this->request->get['filter_description'])) {
				$url .= '&filter_description=' . $this->request->get['filter_description'];
			}
			
			if (isset($this->request->get['filter_category_id'])) {
				$url .= '&filter_category_id=' . $this->request->get['filter_category_id'];
			}	
						
			$this->data['breadcrumbs'][] = array(
				'text'      => $this->language->get('text_search'),
				'href'      => $this->url->link('product/search', $url),
				'separator' => $this->language->get('text_separator')
			); 	
		}
		
		if (isset($this->request->get['product_id'])) {
			$product_id = (int)$this->request->get['product_id'];
		} else {
			$product_id = 0;
		}
		
		$this->load->model('catalog/product');
		
		$product_info = $this->model_catalog_product->getProduct($product_id);
		
		if ($product_info && !$product_info['meal_type']) {
			$url = '';
			
			if (isset($this->request->get['path'])) {
				$url .= '&path=' . $this->request->get['path'];
			}
			
			if (isset($this->request->get['manufacturer_id'])) {
				$url .= '&manufacturer_id=' . $this->request->get['manufacturer_id'];
			}			

			if (isset($this->request->get['filter_name'])) {
				$url .= '&filter_name=' . $this->request->get['filter_name'];
			}
						
			if (isset($this->request->get['filter_tag'])) {
				$url .= '&filter_tag=' . $this->request->get['filter_tag'];
			}
			
			if (isset($this->request->get['filter_description'])) {
				$url .= '&filter_description=' . $this->request->get['filter_description'];
			}	
						
			if (isset($this->request->get['filter_category_id'])) {
				$url .= '&filter_category_id=' . $this->request->get['filter_category_id'];
			}
												
			$this->data['breadcrumbs'][] = array(
				'text'      => $product_info['name'],
				'href'      => $this->url->link('product/product', $url . '&product_id=' . $this->request->get['product_id']),
				'separator' => $this->language->get('text_separator')
			);			
			
			$this->document->setTitle($product_info['name']);
			$this->document->setDescription($product_info['meta_description']);
			$this->document->setKeywords($product_info['meta_keyword']);
			$this->document->addLink($this->url->link('product/product', 'product_id=' . $this->request->get['product_id']), 'canonical');
			
			$this->document->setOgTitle($product_info['name']);
			$this->document->setDescription(strip_tags(html_entity_decode($product_info['description'], ENT_QUOTES, 'UTF-8')));
			
			$this->data['heading_title'] = $product_info['name'];
			
			$this->data['user_id'] = $product_info['user_id'];
			$this->data['username'] = $product_info['username'];
			$this->data['sp'] = $product_info['sp'];
			$this->data['cook_image'] = $product_info['cook_image'];
			
			$this->data['seller_id'] = $product_info['user_id'];
			
			$this->data['pro_cat'] = $product_info['pro_cat'];
			$this->data['priceint'] = $this->tax->calculate($product_info['price'], $product_info['tax_class_id'], $this->config->get('config_tax'));
			
			
			if (isset($cart_item['date'])) {
				$week_day = $cart_item['date'];
				$this->session->data['date'] = date("m/d/y",strtotime($cart_item['date']));
			} else if (isset($this->request->get['day'])) {
				$week_day = date("m/d/y",strtotime($this->request->get['day']));
				$this->session->data['date'] = $week_day;
			} else if (isset($this->session->data['date'])) {
				$week_day = date("m/d/y",strtotime($this->session->data['date']));
				$this->session->data['date'] = $week_day;
			} else {
				$week_day = date("m/d/y");
				$this->session->data['date'] = $week_day;
			}
			
			
			$seller_products = $this->model_catalog_product->getProducts(array('filter_exclude_product_id' => $product_id,'filter_customer_id' => $this->data['user_id'], 'filter_day_available' => $week_day , 'start' => 0,'limit' => 3));//menny
			
			
			if (empty($seller_products)) {
				$seller_products = $this->model_catalog_product->getProducts(array('filter_exclude_product_id' => $product_id,'filter_customer_id' => $this->data['user_id'], 'filter_day_available' => 'all' , 'start' => 0,'limit' => 2));//menny
			}
			
			if (!empty($seller_products)) {
				foreach ($seller_products as $product) {
					$product_data = $this->model_catalog_product->getProduct($product['product_id']);
					if ($product_data['image'] && file_exists(DIR_IMAGE . $product_data['image'])) {
						$image = $this->model_tool_image->resize($product_data['image'], 128, 85);
						//$this->MsLoader->MsFile->resizeImage($product_data['image'], $this->config->get('msconf_product_seller_profile_image_width'), $this->config->get('msconf_product_seller_profile_image_height'));
					}
					
					if (($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) {
						$price = $this->currency->format($this->tax->calculate($product_data['price'], $product_data['tax_class_id'], $this->config->get('config_tax')));
					} else {
						$price = false;
					}
							
					if ((float)$product_data['special']) {
						$special = $this->currency->format($this->tax->calculate($product_data['special'], $product_data['tax_class_id'], $this->config->get('config_tax')));
					} else {
						$special = false;
					}
					
					if ($this->config->get('config_review_status')) {
						$rating = $product_data['rating'];
					} else {
						$rating = false;
					}
								
					$this->data['seller']['products'][] = array(
						'product_id' => $product['product_id'],
						'pro_cat' => $product['pro_cat'],
						'tax' => false,
						//'thumb' => $image,
						'name' => $product_data['name'],
						'price' => $price,
						'is_verified' => $product_data['is_verified'],
						'text_extra' => sprintf($this->language->get('text_extra'), $price),
						'special' => $special,
						'rating' => $rating,
						'reviews'    => sprintf($this->language->get('text_reviews'), (int)$product_data['reviews']),
						'href'     => $this->url->link('product/product', 'product_id=' . $product_data['product_id']),						
					);				
				}
			} else {
				$this->data['seller']['products'] = NULL;
			}
			
			$this->load->model('account/address');
			
			$customer_address = $this->model_account_address->getAddressesById((int)$product_info['user_id']);
			$customer_address = reset($customer_address);
			
			$this->data['city'] = $customer_address['city'];
			
			$this->data['text_contact'] = $this->language->get('text_contact');
			
			$this->data['text_select'] = $this->language->get('text_select');
			$this->data['text_manufacturer'] = $this->language->get('text_manufacturer');
			$this->data['text_model'] = $this->language->get('text_model');
			$this->data['text_reward'] = $this->language->get('text_reward');
			$this->data['text_points'] = $this->language->get('text_points');	
			$this->data['text_discount'] = $this->language->get('text_discount');
			$this->data['text_stock'] = $this->language->get('text_stock');
			$this->data['text_price'] = $this->language->get('text_price');
			$this->data['text_tax'] = $this->language->get('text_tax');
			$this->data['text_discount'] = $this->language->get('text_discount');
			$this->data['text_option'] = $this->language->get('text_option');
			$this->data['add_comment'] = $this->language->get('add_comment');
			$this->data['text_qty'] = sprintf($this->language->get('text_qty'), $product_info['quantity']);
			$this->data['text_minimum'] = sprintf($this->language->get('text_minimum'), $product_info['minimum']);
			$this->data['text_or'] = $this->language->get('text_or');
			$this->data['text_write'] = $this->language->get('text_write');
			$this->data['text_note'] = $this->language->get('text_note');
			$this->data['text_share'] = $this->language->get('text_share');
			$this->data['text_wait'] = $this->language->get('text_wait');
			$this->data['text_tags'] = $this->language->get('text_tags');
			$this->data['text_cook'] = $this->language->get('text_cook');
			$this->data['text_ingredients'] = $this->language->get('text_ingredients');
			$this->data['text_per_meal'] = $this->language->get('text_per_meal');
			$this->data['ils'] = $this->language->get('ils');
			
			$this->data['text_sp'] = $this->language->get('text_sp');
			$this->data['text_more'] = sprintf($this->language->get('text_more'),$product_info['username']);
			$this->data['text_view_all'] = $this->language->get('text_view_all');
			$this->data['text_add_review'] = $this->language->get('text_add_review');
			$this->data['text_read_more'] = $this->language->get('text_read_more');
			$this->data['text_read_less'] = $this->language->get('text_read_less');
			
			$this->data['text_items_in_bag'] = $this->language->get('text_items_in_bag');
			$this->data['text_recent_meal_added'] = $this->language->get('text_recent_meal_added');
			$this->data['text_add_meal'] = $this->language->get('text_add_meal');
			$this->data['text_checkout'] = $this->language->get('text_checkout');
			$this->data['text_close'] = $this->language->get('text_close');
			$this->data['text_empty'] = $this->language->get('text_empty');
			$this->data['text_view_cart'] = $this->language->get('text_view_cart');
			$this->data['text_address_time'] = $this->language->get('text_address_time');
			$this->data['text_address_time_demo'] = $this->language->get('text_address_time_demo');
			
			$this->data['text_deliver_to'] = $this->language->get('text_deliver_to');
			
			if(isset($this->session->data['address_type']) && $this->session->data['address_type'] == "new"){
				$this->data['delivery_address'] = $this->session->data['filter_place'];
			} else if(isset($this->session->data['address_type']) &&  $this->session->data['address_type'] == "def" ){
				$this->data['delivery_address']	= $this->session->data['filter_place_def'];
			} else {
				$this->data['delivery_address']	= "";
			}
			
			$this->data['text_deliver'] = $this->language->get('text_deliver');
			$this->data['text_total'] = $this->language->get('text_total');
			$this->data['text_addmsg'] = $this->language->get('text_addmsg');
			$this->data['text_addfav'] = $this->language->get('text_addfav');
			
			$this->data['entry_name'] = $this->language->get('entry_name');
			$this->data['entry_review'] = $this->language->get('entry_review');
			$this->data['entry_rating'] = $this->language->get('entry_rating');
			$this->data['entry_good'] = $this->language->get('entry_good');
			$this->data['entry_bad'] = $this->language->get('entry_bad');
			$this->data['entry_captcha'] = $this->language->get('entry_captcha');
			
			$this->data['button_order_now'] = $this->language->get('button_order_now');
			$this->data['button_wishlist'] = $this->language->get('button_wishlist');
			$this->data['button_compare'] = $this->language->get('button_compare');			
			$this->data['button_upload'] = $this->language->get('button_upload');
			$this->data['button_continue'] = $this->language->get('button_continue');
			$this->data['button_time_passed'] = $this->language->get('button_time_passed');
			$this->data['button_out_of_stock'] = $this->language->get('button_out_of_stock');
			
			$this->data['button_not_avilable'] = $this->language->get('button_order_now');
			$this->data['button_add_avilable'] = $this->language->get('button_add_avilable');
			
			$this->data['limitbox_text_1'] = $this->language->get('limitbox_text_1');
			$this->data['limitbox_text_2'] = $this->language->get('limitbox_text_2');
			
			$this->data['text_favorite'] = $this->language->get('text_favorite');
			$this->data['text_add_comment'] = $this->language->get('text_add_comment');
			
			$this->data['text_otp_title'] = $this->language->get('text_otp_title');
			$this->data['text_otp_subtitle'] = $this->language->get('text_otp_subtitle');
			$this->data['text_otp_exit'] = $this->language->get('text_otp_exit');
			
			//$this->data['items'] = $this->cart->getProducts();
			
			$this->data['text_of'] = $this->language->get('text_of');
			
			$this->load->model('catalog/review');
			
			$this->data['tab_description'] = $this->language->get('tab_description');
			$this->data['tab_attribute'] = $this->language->get('tab_attribute');
			$this->data['tab_review'] = sprintf($this->language->get('tab_review'), $this->model_catalog_review->getTotalReviewsByProductId($this->request->get['product_id']));
			$this->data['tab_review_count'] = (int)$product_info['reviews_text'];
			$this->data['lable_reviews'] = $this->language->get('lable_reviews');
			$this->data['tab_related'] = $this->language->get('tab_related');
			
			$this->data['product_id'] = $this->request->get['product_id'];
			$this->data['manufacturer'] = $product_info['manufacturer'];
			$this->data['manufacturers'] = $this->url->link('product/manufacturer/info', 'manufacturer_id=' . $product_info['manufacturer_id']);
			$this->data['model'] = $product_info['model'];
			$this->data['reward'] = $product_info['reward'];
			$this->data['sku'] = $product_info['sku'];
			$this->data['isbn'] = $product_info['isbn'];
			$this->data['ean'] = $product_info['ean'];
			$this->data['upc'] = $product_info['upc'];
			$this->data['mpn'] = $product_info['mpn'];
			$this->data['location'] = $product_info['location'];
			$this->data['is_verified'] = $product_info['is_verified'];
			
			$this->data['number_of_sides'] = $product_info['number_of_sides'];
			
			$this->data['points'] = $product_info['points'];
			
			if ($product_info['quantity'] <= 0) {
				$this->data['stock'] = $product_info['stock_status'];
			} elseif ($this->config->get('config_stock_display')) {
				$this->data['stock'] = $product_info['quantity'];
			} else {
				$this->data['stock'] = $this->language->get('text_instock');
			}
			
			$this->load->model('tool/image');

			if ($product_info['image']) {
				$this->data['popup'] = $this->model_tool_image->resize($product_info['image'], $this->config->get('config_image_popup_width'), $this->config->get('config_image_popup_height'));
			} else {
				$this->data['popup'] = '';
			}
			
			if($this->cart->hasProducts()){
				$cart_items = $this->cart->getProducts();
				$cart_item = reset($cart_items);
				$this->data['cart_item'] = $cart_item;
				$this->data['cart_cook_id'] = $cart_item['cook']; 
			} else {
				$this->data['cart_cook_id'] = 0;
			}
			
			
			
			if ($product_info['image']) {
            
                $pageURL = "image/";
                if (file_exists($pageURL.$product_info['image'])) {
                    $imgsize = (getimagesize($pageURL.$product_info['image']));
                    if ($this->config->get('config_image_thumb_width') != 228) $width = $this->config->get('config_image_thumb_width'); else $width  = 2000;
    			    $this->data['thumb'] = $this->model_tool_image->resize($product_info['image'], 532, 400);
			} else $image= false;
				//$this->data['thumb'] = $this->model_tool_image->resize($product_info['image'], $width, $height);
			} else {
				$this->data['thumb'] = '';
			}
			
			$this->document->setOgImage($this->data['thumb']);
			
			$this->data['images'] = array();
			
			$results = $this->model_catalog_product->getProductImages($this->request->get['product_id']);
			
			foreach ($results as $result) {
				$this->data['images'][] = array(
					'popup' => $this->model_tool_image->resize($result['image'], $this->config->get('config_image_popup_width'), $this->config->get('config_image_popup_height')),
					'thumb' => $this->model_tool_image->resize($result['image'], $this->config->get('config_image_additional_width'), $this->config->get('config_image_additional_height'))
				);
			}	
						
			if (($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) {
				$this->data['price'] = $this->currency->format($this->tax->calculate($product_info['price'], $product_info['tax_class_id'], $this->config->get('config_tax')));
			} else {
				$this->data['price'] = false;
			}
						
			if ((float)$product_info['special']) {
				$this->data['special'] = $this->currency->format($this->tax->calculate($product_info['special'], $product_info['tax_class_id'], $this->config->get('config_tax')));
			} else {
				$this->data['special'] = false;
			}
			
			if ($this->config->get('config_tax')) {
				$this->data['tax'] = $this->currency->format((float)$product_info['special'] ? $product_info['special'] : $product_info['price']);
			} else {
				$this->data['tax'] = false;
			}
			
			$discounts = $this->model_catalog_product->getProductDiscounts($this->request->get['product_id']);
			
			$this->data['discounts'] = array(); 
			
			foreach ($discounts as $discount) {
				$this->data['discounts'][] = array(
					'quantity' => $discount['quantity'],
					'price'    => $this->currency->format($this->tax->calculate($discount['price'], $product_info['tax_class_id'], $this->config->get('config_tax')))
				);
			}
			//options create
			$this->data['options'] = array();
			
			foreach ($this->model_catalog_product->getProductOptions($this->request->get['product_id']) as $option) { 
				//if ($option['type'] == 'select' || $option['type'] == 'radio' || $option['type'] == 'checkbox' || $option['type'] == 'image') { 
				//Modified for option quantity=============================================================================================
				if ($option['type'] == 'select' || $option['type'] == 'radio' || $option['type'] == 'checkbox' || $option['type'] == 'checkboxQuantity' || $option['type'] == 'image') { 
				//=========================================================================================================================
					$option_value_data = array();
					
					foreach ($option['option_value'] as $option_value) {
						if (!$option_value['subtract'] || ($option_value['quantity'] > 0)) {
							if ((($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) && (float)$option_value['price']) {
								$price = $this->currency->format($this->tax->calculate($option_value['price'], $product_info['tax_class_id'], $this->config->get('config_tax')));
							} else {
								$price = false;
							}
							
							$option_value_data[] = array(
								'product_option_value_id' => $option_value['product_option_value_id'],
								'option_value_id'         => $option_value['option_value_id'],
								'name'                    => $option_value['name'],
								'image'                   => $this->model_tool_image->resize($option_value['image'], 150, 150),
								'price'                   => $price,
								'price_prefix'            => $option_value['price_prefix']
							);
						}
					}
					
					$this->data['options'][] = array(
						'product_option_id' => $option['product_option_id'],
						'option_id'         => $option['option_id'],
						'name'              => $option['name'],
						'type'              => $option['type'],
						'option_value'      => $option_value_data,
						'required'          => $option['required']
					);					
				} elseif ($option['type'] == 'text' || $option['type'] == 'textarea' || $option['type'] == 'file' || $option['type'] == 'date' || $option['type'] == 'datetime' || $option['type'] == 'time') {
					$this->data['options'][] = array(
						'product_option_id' => $option['product_option_id'],
						'option_id'         => $option['option_id'],
						'name'              => $option['name'],
						'type'              => $option['type'],
						'option_value'      => $option['option_value'],
						'required'          => $option['required']
					);						
				}
			}
         
							
			if ($product_info['minimum']) {
				$this->data['minimum'] = $product_info['minimum'];
			} else {
				$this->data['minimum'] = 1;
			}
			
			$this->data['review_status'] = $this->config->get('config_review_status');
			$this->data['reviews'] = sprintf($this->language->get('text_reviews'), (int)$product_info['reviews']);
			$this->data['review_count'] = (int)$product_info['reviews'];
			$this->data['reviews_text'] = (int)$product_info['reviews_text'];
			
			$this->data['text_meal_box'] = $this->language->get('text_meal_box');
			$this->data['text_meals'] = $this->language->get('text_meals');
			
			$this->data['has_item'] = false;
			
			if($this->cart->getProducts()){
				$this->data['has_item'] = true;
			}
			
			$days_more = array();
			
			$i = 0;
			$j = 0;
			$exit = 0;
			
			while($i < 5 && $j < 40) {
				
				$check_day = date('Y-m-d', strtotime('+'.$j.' day'));
				
				$result = $this->model_catalog_product->getProduct($product_id,$check_day);
				
				//unset($_SESSION['avail']);
				
				/***********************************
				*	DEBUGING
				***********************************/
				if(isset($_GET['debug']) && isset($_GET['controller_product'])){
					print_r($check_day." - ".$product_id." - ".$i);
					print_r(" - ".$j." - ".$result);
					print_r("<br>");
				}
				/***********************************
				*	DEBUGING END
				***********************************/
				
				if($result) {
					$days_more[$i] = $j;
					$i++;
				}
				
				$j++;
			}
			
			$this->data['days_more'] = $days_more;
			
			/***********************************
			*	DEBUGING
			***********************************/
			if(isset($_GET['debug']) && isset($_GET['controller_product'])){
				print_r($days_more);
				die;
			}
			
			///***********************************
			//*	DEBUGING END
			//***********************************/
			
			setlocale(LC_ALL, 'he_IL.UTF-8');
			
			$this->data['text_day_1'] = $this->language->get('text_day_1')." ".strftime("%d/%m",strtotime("today"));
			$this->data['text_day_2'] = $this->language->get('text_day_2')." ".strftime("%d/%m",strtotime("+1 day"));
			$this->data['text_day_3'] = strftime("%A %d/%m",strtotime("+2 day"));
			$this->data['text_day_4'] = strftime("%A %d/%m",strtotime("+3 day"));
			$this->data['text_day_5'] = strftime("%A %d/%m",strtotime("+4 day"));
			
			$this->load->model('catalog/availability');
			// need to get the date we want is some form of rewust or we pick today as the date
			if(isset($_REQUEST['date'])){
				$date = date('Y-m-d', strtotime($_REQUEST['date']));//print_r($_REQUEST['date']);
			}else{
				$date = date("Y-m-d");
			}
			//$last_order_time = $this->model_catalog_availability->getTimeAvailability($product_id,$date);
			//
			//$last_order_time = 10;
			//
			//$times_of_day = array();
			//$order_times = array();
			//
			//
			//
			//foreach($order_times as $key => $order_time){ 
			//	if( ( strtotime($date." ".$key.":00") - 60*60*$last_order_time ) > time() ){
			//		$times_of_day[$key] = $order_time;
			//	}
			//}
			//
			//if(empty($times_of_day)){
			//	$times_of_day[-1] = "11:00 - 12:00";
			//	// we need to change the date!
			//}
			//
			//if(isset($_REQUEST['debug'])){
			////	print_r(date("Y-m-d l ")); die;
			//}
			
			if(empty($product_info['order_times'])){
				$times_of_day[-1] = "11:00 - 12:00";
			} else {
				$times_of_day = $product_info['order_times'];
			}
			
			
			$this->data['times_of_day'] = $times_of_day;
			
			$this->data['rating'] = (int)$product_info['rating'];
			$this->data['description'] = strip_tags(html_entity_decode($product_info['description'], ENT_QUOTES, 'UTF-8'));
			//$this->document->serDescription($this->data['description']);
			$this->data['attribute_groups'] = $this->model_catalog_product->getProductAttributes($this->request->get['product_id']);
			
			
			$this->data['logged'] = $this->customer->isLogged();
			
			$this->data['products'] = array();
			
			$results = $this->model_catalog_product->getProductRelated($this->request->get['product_id']);
			
			$this->data['total_sides_int'] = $product_info['number_of_sides'];
			
			$this->data['rate_word'][0] = $this->language->get('rate_word_0');
			$this->data['rate_word'][1] = $this->language->get('rate_word_1');
			$this->data['rate_word'][2] = $this->language->get('rate_word_2');
			$this->data['rate_word'][3] = $this->language->get('rate_word_3');
			$this->data['rate_word'][4] = $this->language->get('rate_word_4');
			$this->data['rate_word'][5] = $this->language->get('rate_word_5');
			
			$this->data['text_choose_side'] = $this->language->get('text_choose_side');
			$this->data['placeholder_meal'] = $this->language->get('placeholder_meal');
			$this->data['placeholder_delivery'] = $this->language->get('placeholder_delivery');
			$this->data['add_privet_review'] = $this->language->get('add_privet_review');
			$this->data['text_review_start'] = $this->language->get('text_review_start');
			
			$this->data['sface'] = $this->language->get('sface');
			$this->data['stwit'] = $this->language->get('stwit');
			
			$this->data['stwit_text'] = urlencode($this->data['heading_title'].": ");
			
			$this->data['smail'] = $this->language->get('smail');
			$this->data['smail_sub'] = $this->data['heading_title'];
			$this->data['smail_bud'] = sprintf($this->language->get('smail_bud_meal'),urlencode($this->url->link('product/product', 'product_id=' . $this->request->get['product_id'])));
			
			$this->data['text_cancel'] = $this->language->get('text_cancel');
			$this->data['text_submit'] = $this->language->get('text_submit');
			
			$review_image = $this->model_tool_image->resize($product_info['image'], 212, 145);
			
			$review = array(
				'name' 		=> $product_info['name'],
				'image' 	=> $review_image,
			);
			
			$this->data['review'] = $review;
			
			//print_r($results); die;
			
			foreach ($results as $result) {
				if ($result['image']) {
					$image = $this->model_tool_image->resize($result['image'], $this->config->get('config_image_related_width'), $this->config->get('config_image_related_height'));
					$review_image = $this->model_tool_image->resize($result['image'], 212, 145);
				} else {
					$image = $this->model_tool_image->resize('no_image.jpg', $this->config->get('config_image_related_width'), $this->config->get('config_image_related_height'));
					$review_image = $this->model_tool_image->resize('no_image.jpg', 212, 145);
				}
				
				if (($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) {
					$price = $this->currency->format($this->tax->calculate($result['price'], $result['tax_class_id'], $this->config->get('config_tax')));
					$price_int = (int)$result['price'];
				} else {
					$price = false;
					$price_int = false;
				}
				
				if ((float)$result['special']) {
					$special = $this->currency->format($this->tax->calculate($result['special'], $result['tax_class_id'], $this->config->get('config_tax')));
				} else {
					$special = false;
				}
				
				if ($this->config->get('config_review_status')) {
					$rating = (int)$result['rating'];
				} else {
					$rating = false;
				}
				
				$this->data['products'][] = array(
					'product_id' 	 => $result['product_id'],
					'tag'		 => $result['tag'],
					'thumb'   	 => $image,
					'image'   	 => $review_image,
					'name'    	 => $result['name'],
					'price'   	 => $price,
					'price_int'   	 => $price_int,
					'is_verified' 	 => $result['is_verified'],
					'text_extra' 	 => sprintf($this->language->get('text_extra'), $price),
					'special' 	 => $special,
					'rating'    	 => $rating,
					'reviews'    	 => sprintf($this->language->get('text_reviews'), (int)$result['reviews']),
					'href'    	 => $this->url->link('product/product', 'product_id=' . $result['product_id']),
				);
			}
			
						
			if($product_info['number_of_sides'] == 2){
				$this->data['text_sides'] = sprintf($this->language->get('total_sides'), 2 );
			}elseif($product_info['number_of_sides'] == 1 && count($this->data['products']) > 1 ){
				$this->data['text_sides'] = sprintf($this->language->get('total_sides_one'), 1 );
			}elseif($product_info['number_of_sides'] == 1 ){
				$this->data['text_sides'] = $this->language->get('total_sides_one_nc');
			}elseif($product_info['number_of_sides'] == 0 ){
				$this->data['text_sides'] = $this->language->get('text_sides');
			}
			
			
			
			$this->data['tags'] = array();
					
			$tags = explode(',', $product_info['tag']);
			
			foreach ($tags as $tag) {
				$this->data['tags'][] = array(
					'tag'  => trim($tag),
					'href' => $this->url->link('product/search', 'filter_tag=' . trim($tag))
				);
			}
			
			$this->model_catalog_product->updateViewed($this->request->get['product_id']);
			
			if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/product/product.tpl')) {
				$this->template = $this->config->get('config_template') . '/template/product/product.tpl';
			} else {
				$this->template = 'default/template/product/product.tpl';
			}
			
			$this->children = array(
				'common/column_left',
				'common/column_right',
				'common/content_top',
				'common/content_bottom',
				'common/footer',
				'common/header'
			);
			
			$this->response->setOutput($this->render());
		} else {
			$url = '';
			
			if (isset($this->request->get['path'])) {
				$url .= '&path=' . $this->request->get['path'];
			}
			
			if (isset($this->request->get['manufacturer_id'])) {
				$url .= '&manufacturer_id=' . $this->request->get['manufacturer_id'];
			}			
			
			if (isset($this->request->get['filter_name'])) {
				$url .= '&filter_name=' . $this->request->get['filter_name'];
			}	
			
			if (isset($this->request->get['filter_tag'])) {
				$url .= '&filter_tag=' . $this->request->get['filter_tag'];
			}
			
			if (isset($this->request->get['filter_description'])) {
				$url .= '&filter_description=' . $this->request->get['filter_description'];
			}
			
			if (isset($this->request->get['filter_category_id'])) {
				$url .= '&filter_category_id=' . $this->request->get['filter_category_id'];
			}
			
			$this->data['breadcrumbs'][] = array(
				'text'      => $this->language->get('text_error'),
				'href'      => $this->url->link('product/product', $url . '&product_id=' . $product_id),
				'separator' => $this->language->get('text_separator')
			);			
			
			$this->document->setTitle($this->language->get('text_error'));
			
			$this->data['heading_title'] = $this->language->get('text_error');
			
			$this->data['text_error'] = $this->language->get('text_error');
			
			$this->data['button_continue'] = $this->language->get('button_continue');
			
			$this->data['continue'] = $this->url->link('common/home');
			
				if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/error/not_found.tpl')) {
					$this->template = $this->config->get('config_template') . '/template/error/not_found.tpl';
				} else {
					$this->template = 'default/template/error/not_found.tpl';
				}
				
				$this->children = array(
					'common/column_left',
					'common/column_right',
					'common/content_top',
					'common/content_bottom',
					//'supercheckout/supercheckout',
					'common/footer',
					'common/header'
				);			
				$this->response->setOutput($this->render());
		}
  	}
	
	
	public function ajax() {
		$this->language->load('product/product');
		
		$this->data['breadcrumbs'] = array();
		
		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home'),			
			'separator' => false
		);
		
		$this->load->model('catalog/category');	
		
		if (isset($this->request->get['path'])) {
			$path = '';
				
			foreach (explode('_', $this->request->get['path']) as $path_id) {
				if (!$path) {
					$path = $path_id;
				} else {
					$path .= '_' . $path_id;
				}
				
				$category_info = $this->model_catalog_category->getCategory($path_id);
				
				if ($category_info) {
					$this->data['breadcrumbs'][] = array(
						'text'      => $category_info['name'],
						'href'      => $this->url->link('product/category', 'path=' . $path),
						'separator' => $this->language->get('text_separator')
					);
				}
			}
		}
		
		
		$this->load->model('catalog/manufacturer');	
		
		if (isset($this->request->get['manufacturer_id'])) {
			$this->data['breadcrumbs'][] = array( 
				'text'      => $this->language->get('text_brand'),
				'href'      => $this->url->link('product/manufacturer'),
				'separator' => $this->language->get('text_separator')
			);	
				
			$manufacturer_info = $this->model_catalog_manufacturer->getManufacturer($this->request->get['manufacturer_id']);

			if ($manufacturer_info) {	
				$this->data['breadcrumbs'][] = array(
					'text'	    => $manufacturer_info['name'],
					'href'	    => $this->url->link('product/manufacturer/info', 'manufacturer_id=' . $this->request->get['manufacturer_id']),					
					'separator' => $this->language->get('text_separator')
				);
			}
		}
		
		if (isset($this->request->get['filter_name']) || isset($this->request->get['filter_tag'])) {
			$url = '';
			
			if (isset($this->request->get['filter_name'])) {
				$url .= '&filter_name=' . $this->request->get['filter_name'];
			}
						
			if (isset($this->request->get['filter_tag'])) {
				$url .= '&filter_tag=' . $this->request->get['filter_tag'];
			}
						
			if (isset($this->request->get['filter_description'])) {
				$url .= '&filter_description=' . $this->request->get['filter_description'];
			}
			
			if (isset($this->request->get['filter_category_id'])) {
				$url .= '&filter_category_id=' . $this->request->get['filter_category_id'];
			}	
						
			$this->data['breadcrumbs'][] = array(
				'text'      => $this->language->get('text_search'),
				'href'      => $this->url->link('product/search', $url),
				'separator' => $this->language->get('text_separator')
			); 	
		}
		
		if (isset($this->request->get['product_id'])) {
			$product_id = (int)$this->request->get['product_id'];
		} else {
			$product_id = 0;
		}
		
		$this->load->model('catalog/product');
		
		$product_info = $this->model_catalog_product->getProduct($product_id);
		
		if ($product_info && !$product_info['meal_type']) {
			$url = '';
			
			if (isset($this->request->get['path'])) {
				$url .= '&path=' . $this->request->get['path'];
			}
			
			if (isset($this->request->get['manufacturer_id'])) {
				$url .= '&manufacturer_id=' . $this->request->get['manufacturer_id'];
			}			

			if (isset($this->request->get['filter_name'])) {
				$url .= '&filter_name=' . $this->request->get['filter_name'];
			}
						
			if (isset($this->request->get['filter_tag'])) {
				$url .= '&filter_tag=' . $this->request->get['filter_tag'];
			}
			
			if (isset($this->request->get['filter_description'])) {
				$url .= '&filter_description=' . $this->request->get['filter_description'];
			}	
						
			if (isset($this->request->get['filter_category_id'])) {
				$url .= '&filter_category_id=' . $this->request->get['filter_category_id'];
			}
												
			$this->data['breadcrumbs'][] = array(
				'text'      => $product_info['name'],
				'href'      => $this->url->link('product/product', $url . '&product_id=' . $this->request->get['product_id']),
				'separator' => $this->language->get('text_separator')
			);			
			
			$this->document->setTitle($product_info['name']);
			$this->document->setDescription($product_info['meta_description']);
			$this->document->setKeywords($product_info['meta_keyword']);
			$this->document->addLink($this->url->link('product/product', 'product_id=' . $this->request->get['product_id']), 'canonical');
			
			$this->document->setOgTitle($product_info['name']);
			$this->document->setDescription(strip_tags(html_entity_decode($product_info['description'], ENT_QUOTES, 'UTF-8')));
			
			$this->data['heading_title'] = $product_info['name'];
			
			$this->data['user_id'] = $product_info['user_id'];
			$this->data['username'] = $product_info['username'];
			$this->data['sp'] = $product_info['sp'];
			$this->data['cook_image'] = $product_info['cook_image'];
			
			$this->data['seller_id'] = $product_info['user_id'];
			
			$this->data['pro_cat'] = $product_info['pro_cat'];
			$this->data['priceint'] = $this->tax->calculate($product_info['price'], $product_info['tax_class_id'], $this->config->get('config_tax'));
			
			
			if (isset($cart_item['date'])) {
				$week_day = $cart_item['date'];
				$this->session->data['date'] = date("m/d/y",strtotime($cart_item['date']));
			} else if (isset($this->request->get['day'])) {
				$week_day = date("m/d/y",strtotime($this->request->get['day']));
				$this->session->data['date'] = $week_day;
			} else if (isset($this->session->data['date'])) {
				$week_day = date("m/d/y",strtotime($this->session->data['date']));
				$this->session->data['date'] = $week_day;
			} else {
				$week_day = date("m/d/y");
				$this->session->data['date'] = $week_day;
			}
			
			
			$seller_products = $this->model_catalog_product->getProducts(array('filter_exclude_product_id' => $product_id,'filter_customer_id' => $this->data['user_id'], 'filter_day_available' => $week_day , 'start' => 0,'limit' => 3));//menny
			
			
			if (empty($seller_products)) {
				$seller_products = $this->model_catalog_product->getProducts(array('filter_exclude_product_id' => $product_id,'filter_customer_id' => $this->data['user_id'], 'filter_day_available' => 'all' , 'start' => 0,'limit' => 2));//menny
			}
			
			if (!empty($seller_products)) {
				foreach ($seller_products as $product) {
					$product_data = $this->model_catalog_product->getProduct($product['product_id']);
					if ($product_data['image'] && file_exists(DIR_IMAGE . $product_data['image'])) {
						$image = $this->model_tool_image->resize($product_data['image'], 128, 85);
						//$this->MsLoader->MsFile->resizeImage($product_data['image'], $this->config->get('msconf_product_seller_profile_image_width'), $this->config->get('msconf_product_seller_profile_image_height'));
					}
					
					if (($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) {
						$price = $this->currency->format($this->tax->calculate($product_data['price'], $product_data['tax_class_id'], $this->config->get('config_tax')));
					} else {
						$price = false;
					}
							
					if ((float)$product_data['special']) {
						$special = $this->currency->format($this->tax->calculate($product_data['special'], $product_data['tax_class_id'], $this->config->get('config_tax')));
					} else {
						$special = false;
					}
					
					if ($this->config->get('config_review_status')) {
						$rating = $product_data['rating'];
					} else {
						$rating = false;
					}
								
					$this->data['seller']['products'][] = array(
						'product_id' => $product['product_id'],
						'pro_cat' => $product['pro_cat'],
						'tax' => false,
						'thumb' => $image,
						'name' => $product_data['name'],
						'price' => $price,
						'is_verified' => $product_data['is_verified'],
						'text_extra' => sprintf($this->language->get('text_extra'), $price),
						'special' => $special,
						'rating' => $rating,
						'reviews'    => sprintf($this->language->get('text_reviews'), (int)$product_data['reviews']),
						'href'     => $this->url->link('product/product', 'product_id=' . $product_data['product_id']),						
					);				
				}
			} else {
				$this->data['seller']['products'] = NULL;
			}
			
			$this->load->model('account/address');
			
			$customer_address = $this->model_account_address->getAddressesById((int)$product_info['user_id']);
			$customer_address = reset($customer_address);
			
			$this->data['city'] = $customer_address['city'];
			
			$this->data['text_contact'] = $this->language->get('text_contact');
			
			$this->data['text_select'] = $this->language->get('text_select');
			$this->data['text_manufacturer'] = $this->language->get('text_manufacturer');
			$this->data['text_model'] = $this->language->get('text_model');
			$this->data['text_reward'] = $this->language->get('text_reward');
			$this->data['text_points'] = $this->language->get('text_points');	
			$this->data['text_discount'] = $this->language->get('text_discount');
			$this->data['text_stock'] = $this->language->get('text_stock');
			$this->data['text_price'] = $this->language->get('text_price');
			$this->data['text_tax'] = $this->language->get('text_tax');
			$this->data['text_discount'] = $this->language->get('text_discount');
			$this->data['text_option'] = $this->language->get('text_option');
			$this->data['add_comment'] = $this->language->get('add_comment');
			$this->data['text_qty'] = sprintf($this->language->get('text_qty'), $product_info['quantity']);
			$this->data['text_minimum'] = sprintf($this->language->get('text_minimum'), $product_info['minimum']);
			$this->data['text_or'] = $this->language->get('text_or');
			$this->data['text_write'] = $this->language->get('text_write');
			$this->data['text_note'] = $this->language->get('text_note');
			$this->data['text_share'] = $this->language->get('text_share');
			$this->data['text_wait'] = $this->language->get('text_wait');
			$this->data['text_tags'] = $this->language->get('text_tags');
			$this->data['text_cook'] = $this->language->get('text_cook');
			$this->data['text_ingredients'] = $this->language->get('text_ingredients');
			$this->data['text_per_meal'] = $this->language->get('text_per_meal');
			$this->data['ils'] = $this->language->get('ils');
			
			$this->data['text_sp'] = $this->language->get('text_sp');
			$this->data['text_more'] = sprintf($this->language->get('text_more'),$product_info['username']);
			$this->data['text_view_all'] = $this->language->get('text_view_all');
			$this->data['text_add_review'] = $this->language->get('text_add_review');
			$this->data['text_read_more'] = $this->language->get('text_read_more');
			$this->data['text_read_less'] = $this->language->get('text_read_less');
			
			$this->data['text_items_in_bag'] = $this->language->get('text_items_in_bag');
			$this->data['text_recent_meal_added'] = $this->language->get('text_recent_meal_added');
			$this->data['text_add_meal'] = $this->language->get('text_add_meal');
			$this->data['text_checkout'] = $this->language->get('text_checkout');
			$this->data['text_close'] = $this->language->get('text_close');
			$this->data['text_empty'] = $this->language->get('text_empty');
			$this->data['text_view_cart'] = $this->language->get('text_view_cart');
			$this->data['text_address_time'] = $this->language->get('text_address_time');
			$this->data['text_address_time_demo'] = $this->language->get('text_address_time_demo');
			
			$this->data['text_deliver_to'] = $this->language->get('text_deliver_to');
			
			if(isset($this->session->data['address_type']) && $this->session->data['address_type'] == "new"){
				$this->data['delivery_address'] = $this->session->data['filter_place'];
			} else if(isset($this->session->data['address_type']) &&  $this->session->data['address_type'] == "def" ){
				$this->data['delivery_address']	= $this->session->data['filter_place_def'];
			} else {
				$this->data['delivery_address']	= "";
			}
			
			$this->data['text_deliver'] = $this->language->get('text_deliver');
			$this->data['text_total'] = $this->language->get('text_total');
			$this->data['text_addmsg'] = $this->language->get('text_addmsg');
			$this->data['text_addfav'] = $this->language->get('text_addfav');
			
			$this->data['entry_name'] = $this->language->get('entry_name');
			$this->data['entry_review'] = $this->language->get('entry_review');
			$this->data['entry_rating'] = $this->language->get('entry_rating');
			$this->data['entry_good'] = $this->language->get('entry_good');
			$this->data['entry_bad'] = $this->language->get('entry_bad');
			$this->data['entry_captcha'] = $this->language->get('entry_captcha');
			
			$this->data['button_order_now'] = $this->language->get('button_order_now');
			$this->data['button_wishlist'] = $this->language->get('button_wishlist');
			$this->data['button_compare'] = $this->language->get('button_compare');			
			$this->data['button_upload'] = $this->language->get('button_upload');
			$this->data['button_continue'] = $this->language->get('button_continue');
			$this->data['button_time_passed'] = $this->language->get('button_time_passed');
			$this->data['button_out_of_stock'] = $this->language->get('button_out_of_stock');
			
			$this->data['button_not_avilable'] = $this->language->get('button_order_now');
			$this->data['button_add_avilable'] = $this->language->get('button_add_avilable');
			
			$this->data['limitbox_text_1'] = $this->language->get('limitbox_text_1');
			$this->data['limitbox_text_2'] = $this->language->get('limitbox_text_2');
			
			$this->data['text_favorite'] = $this->language->get('text_favorite');
			$this->data['text_add_comment'] = $this->language->get('text_add_comment');
			
			$this->data['text_otp_title'] = $this->language->get('text_otp_title');
			$this->data['text_otp_subtitle'] = $this->language->get('text_otp_subtitle');
			$this->data['text_otp_exit'] = $this->language->get('text_otp_exit');
			
			//$this->data['items'] = $this->cart->getProducts();
			
			$this->data['text_of'] = $this->language->get('text_of');
			
			$this->load->model('catalog/review');
			
			$this->data['tab_description'] = $this->language->get('tab_description');
			$this->data['tab_attribute'] = $this->language->get('tab_attribute');
			$this->data['tab_review'] = sprintf($this->language->get('tab_review'), $this->model_catalog_review->getTotalReviewsByProductId($this->request->get['product_id']));
			$this->data['tab_review_count'] = (int)$product_info['reviews_text'];
			$this->data['lable_reviews'] = $this->language->get('lable_reviews');
			$this->data['tab_related'] = $this->language->get('tab_related');
			
			$this->data['product_id'] = $this->request->get['product_id'];
			$this->data['manufacturer'] = $product_info['manufacturer'];
			$this->data['manufacturers'] = $this->url->link('product/manufacturer/info', 'manufacturer_id=' . $product_info['manufacturer_id']);
			$this->data['model'] = $product_info['model'];
			$this->data['reward'] = $product_info['reward'];
			$this->data['sku'] = $product_info['sku'];
			$this->data['isbn'] = $product_info['isbn'];
			$this->data['ean'] = $product_info['ean'];
			$this->data['upc'] = $product_info['upc'];
			$this->data['mpn'] = $product_info['mpn'];
			$this->data['location'] = $product_info['location'];
			$this->data['is_verified'] = $product_info['is_verified'];
			
			$this->data['number_of_sides'] = $product_info['number_of_sides'];
			
			$this->data['points'] = $product_info['points'];
			
			if ($product_info['quantity'] <= 0) {
				$this->data['stock'] = $product_info['stock_status'];
			} elseif ($this->config->get('config_stock_display')) {
				$this->data['stock'] = $product_info['quantity'];
			} else {
				$this->data['stock'] = $this->language->get('text_instock');
			}
			
			$this->load->model('tool/image');

			if ($product_info['image']) {
				$this->data['popup'] = $this->model_tool_image->resize($product_info['image'], $this->config->get('config_image_popup_width'), $this->config->get('config_image_popup_height'));
			} else {
				$this->data['popup'] = '';
			}
						
			if($this->cart->hasProducts()){
				$this->data['cart_items']['num'] = $this->cart->countProducts();
				$this->data['cart_items']['info'] = $this->cart->getProducts();
			} else {
				$this->data['cart_items'] = false;
			}
			
			//debug($this->data['cart_items']);
			
			if ($product_info['image']) {
				$pageURL = "image/";
				if (file_exists($pageURL.$product_info['image'])) {
					
					$imgsize = (getimagesize($pageURL.$product_info['image']));
					
					if ($this->config->get('config_image_thumb_width') != 228) $width = $this->config->get('config_image_thumb_width');
					else $width  = 228;
					
					$this->data['thumb'] = $this->model_tool_image->resize($product_info['image'], 255, 180);
				} else $image= false;
				//$this->data['thumb'] = $this->model_tool_image->resize($product_info['image'], $width, $height);
			} else {
				$this->data['thumb'] = '';
			}
			
			$this->document->setOgImage($this->data['thumb']);
			
			$this->data['images'] = array();
			
			$results = $this->model_catalog_product->getProductImages($this->request->get['product_id']);
			
			foreach ($results as $result) {
				$this->data['images'][] = array(
					'popup' => $this->model_tool_image->resize($result['image'], $this->config->get('config_image_popup_width'), $this->config->get('config_image_popup_height')),
					'thumb' => $this->model_tool_image->resize($result['image'], $this->config->get('config_image_additional_width'), $this->config->get('config_image_additional_height'))
				);
			}	
						
			if (($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) {
				$this->data['price'] = $this->currency->format($this->tax->calculate($product_info['price'], $product_info['tax_class_id'], $this->config->get('config_tax')));
			} else {
				$this->data['price'] = false;
			}
						
			if ((float)$product_info['special']) {
				$this->data['special'] = $this->currency->format($this->tax->calculate($product_info['special'], $product_info['tax_class_id'], $this->config->get('config_tax')));
			} else {
				$this->data['special'] = false;
			}
			
			if ($this->config->get('config_tax')) {
				$this->data['tax'] = $this->currency->format((float)$product_info['special'] ? $product_info['special'] : $product_info['price']);
			} else {
				$this->data['tax'] = false;
			}
			
			$discounts = $this->model_catalog_product->getProductDiscounts($this->request->get['product_id']);
			
			$this->data['discounts'] = array(); 
			
			foreach ($discounts as $discount) {
				$this->data['discounts'][] = array(
					'quantity' => $discount['quantity'],
					'price'    => $this->currency->format($this->tax->calculate($discount['price'], $product_info['tax_class_id'], $this->config->get('config_tax')))
				);
			}
			
			$this->data['options'] = array();
			
			foreach ($this->model_catalog_product->getProductOptions($this->request->get['product_id']) as $option) { 
				if ($option['type'] == 'select' || $option['type'] == 'radio' || $option['type'] == 'checkbox' || $option['type'] == 'image') { 
					$option_value_data = array();
					
					foreach ($option['option_value'] as $option_value) {
						if (!$option_value['subtract'] || ($option_value['quantity'] > 0)) {
							if ((($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) && (float)$option_value['price']) {
								$price = $this->currency->format($this->tax->calculate($option_value['price'], $product_info['tax_class_id'], $this->config->get('config_tax')));
							} else {
								$price = false;
							}
							
							$option_value_data[] = array(
								'product_option_value_id' => $option_value['product_option_value_id'],
								'option_value_id'         => $option_value['option_value_id'],
								'name'                    => $option_value['name'],
								'image'                   => $this->model_tool_image->resize($option_value['image'], 150, 150),
								'price'                   => $price,
								'price_prefix'            => $option_value['price_prefix']
							);
						}
					}
					
					$this->data['options'][] = array(
						'product_option_id' => $option['product_option_id'],
						'option_id'         => $option['option_id'],
						'name'              => $option['name'],
						'type'              => $option['type'],
						'option_value'      => $option_value_data,
						'required'          => $option['required']
					);					
				} elseif ($option['type'] == 'text' || $option['type'] == 'textarea' || $option['type'] == 'file' || $option['type'] == 'date' || $option['type'] == 'datetime' || $option['type'] == 'time') {
					$this->data['options'][] = array(
						'product_option_id' => $option['product_option_id'],
						'option_id'         => $option['option_id'],
						'name'              => $option['name'],
						'type'              => $option['type'],
						'option_value'      => $option['option_value'],
						'required'          => $option['required']
					);						
				}
			}
							
			if ($product_info['minimum']) {
				$this->data['minimum'] = $product_info['minimum'];
			} else {
				$this->data['minimum'] = 1;
			}
			
			$this->data['review_status'] = $this->config->get('config_review_status');
			$this->data['reviews'] = sprintf($this->language->get('text_reviews'), (int)$product_info['reviews']);
			$this->data['review_count'] = (int)$product_info['reviews'];
			$this->data['reviews_text'] = (int)$product_info['reviews_text'];
			
			$this->data['text_meal_box'] = $this->language->get('text_meal_box');
			$this->data['text_meals'] = $this->language->get('text_meals');
			
			$this->data['has_item'] = false;
			
			if($this->cart->getProducts()){
				$this->data['has_item'] = true;
			}
			
			$days_more = array();
			
			$i = 0;
			$j = 0;
			$exit = 0;
			
			while($i < 5 && $j < 35) {
				
				$check_day = date('Y-m-d', strtotime('+'.$j.' day'));
				
				$result = true;//check_avilable($product_id,$check_day);
				
				if($result) {
					$days_more[$i] = $j;
					$i++;
				}
				
				$j++;
			}
			
			$this->data['days_more'] = $days_more;
			
			// need to get the date we want is some form of rewust or we pick today as the date
			if(isset($_REQUEST['date'])){
				$date = date('Y-m-d', strtotime($_REQUEST['date']));//print_r($_REQUEST['date']);
			}else{
				$date = date("Y-m-d");
			}
			
			if(empty($product_info['order_times'])){
				$times_of_day[-1] = "11:00 - 12:00";
			} else {
				$times_of_day = $product_info['order_times'];
			}
			
			$this->data['times_of_day'] = $times_of_day;
			
			$this->data['rating'] = (int)$product_info['rating'];
			$this->data['description'] = strip_tags(html_entity_decode($product_info['description'], ENT_QUOTES, 'UTF-8'));
			//$this->document->serDescription($this->data['description']);
			$this->data['attribute_groups'] = $this->model_catalog_product->getProductAttributes($this->request->get['product_id']);
			
			
			$this->data['logged'] = $this->customer->isLogged();
			
			$this->data['total_sides_int'] = $product_info['number_of_sides'];
			
			$this->data['text_choose_side'] = $this->language->get('text_choose_side');
			$this->data['placeholder_meal'] = $this->language->get('placeholder_meal');
			$this->data['placeholder_delivery'] = $this->language->get('placeholder_delivery');
			$this->data['add_privet_review'] = $this->language->get('add_privet_review');
			$this->data['text_review_start'] = $this->language->get('text_review_start');
			
			$this->data['sface'] = $this->language->get('sface');
			$this->data['stwit'] = $this->language->get('stwit');
			
			$this->data['stwit_text'] = urlencode($this->data['heading_title'].": ");
			
			$this->data['smail'] = $this->language->get('smail');
			$this->data['smail_sub'] = $this->data['heading_title'];
			$this->data['smail_bud'] = sprintf($this->language->get('smail_bud_meal'),urlencode($this->url->link('product/product', 'product_id=' . $this->request->get['product_id'])));
			
			$this->data['text_cancel'] = $this->language->get('text_cancel');
			$this->data['text_submit'] = $this->language->get('text_submit');
			
			$review_image = $this->model_tool_image->resize($product_info['image'], 212, 145);
			
			$review = array(
				'name' 		=> $product_info['name'],
				'image' 	=> $review_image,
			);
			
			$this->data['review'] = $review;
			
			$this->data['products'] = array();
			
			$results = $this->model_catalog_product->getProductRelated($product_id);
			
			foreach ($results as $result) {
				if ($result['image']) {
					$image = $this->model_tool_image->resize($result['image'], $this->config->get('config_image_related_width'), $this->config->get('config_image_related_height'));
					$review_image = $this->model_tool_image->resize($result['image'], 212, 145);
				} else {
					$image = $this->model_tool_image->resize('no_image.jpg', $this->config->get('config_image_related_width'), $this->config->get('config_image_related_height'));
					$review_image = $this->model_tool_image->resize('no_image.jpg', 212, 145);
				}
				
				if (($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) {
					$price = $this->currency->format($this->tax->calculate($result['price'], $result['tax_class_id'], $this->config->get('config_tax')));
					$price_int = (int)$result['price'];
				} else {
					$price = false;
					$price_int = false;
				}
				
				if ((float)$result['special']) {
					$special = $this->currency->format($this->tax->calculate($result['special'], $result['tax_class_id'], $this->config->get('config_tax')));
				} else {
					$special = false;
				}
				
				if ($this->config->get('config_review_status')) {
					$rating = (int)$result['rating'];
				} else {
					$rating = false;
				}
				
				$this->data['products'][] = array(
					'product_id' 	 => $result['product_id'],
					'tag'		 => $result['tag'],
					'thumb'   	 => $image,
					'image'   	 => $review_image,
					'name'    	 => $result['name'],
					'price'   	 => $price,
					'price_int'   	 => $price_int,
					'is_verified' 	 => $result['is_verified'],
					'text_extra' 	 => sprintf($this->language->get('text_extra'), $price),
					'special' 	 => $special,
					'rating'    	 => $rating,
					'reviews'    	 => sprintf($this->language->get('text_reviews'), (int)$result['reviews']),
					'href'    	 => $this->url->link('product/product', 'product_id=' . $result['product_id']),
				);
			}
			
			if($product_info['number_of_sides'] == 2){
				$this->data['text_sides'] = sprintf($this->language->get('total_sides'), 2 );
			}elseif($product_info['number_of_sides'] == 1 && count($this->data['products']) > 1 ){
				$this->data['text_sides'] = sprintf($this->language->get('total_sides_one'), 1 );
			}elseif($product_info['number_of_sides'] == 1 ){
				$this->data['text_sides'] = $this->language->get('total_sides_one_nc');
			}elseif($product_info['number_of_sides'] == 0 ){
				$this->data['text_sides'] = $this->language->get('text_sides');
			}
			
			$this->data['tags'] = array();
			
			$tags = explode(',', $product_info['tag']);
			
			foreach ($tags as $tag) {
				$this->data['tags'][] = array(
					'tag'  => trim($tag),
					'href' => $this->url->link('product/search', 'filter_tag=' . trim($tag))
				);
			}
			
			$this->model_catalog_product->updateViewed($product_id);
			
			if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/product/product_ajax.tpl')) {
				$this->template = $this->config->get('config_template') . '/template/product/product_ajax.tpl';
			} else {
				$this->template = 'default/template/product/product_ajax.tpl';
			}
			
			$this->children = array(
				'common/column_left',
				'common/column_right',
				'common/content_top',
				'common/content_bottom',
				'common/footer',
				'common/header'
			);
			
			$this->response->setOutput($this->render());
		} else {
			$url = '';
			
			if (isset($this->request->get['path'])) {
				$url .= '&path=' . $this->request->get['path'];
			}
			
			if (isset($this->request->get['manufacturer_id'])) {
				$url .= '&manufacturer_id=' . $this->request->get['manufacturer_id'];
			}			
			
			if (isset($this->request->get['filter_name'])) {
				$url .= '&filter_name=' . $this->request->get['filter_name'];
			}	
			
			if (isset($this->request->get['filter_tag'])) {
				$url .= '&filter_tag=' . $this->request->get['filter_tag'];
			}
			
			if (isset($this->request->get['filter_description'])) {
				$url .= '&filter_description=' . $this->request->get['filter_description'];
			}
			
			if (isset($this->request->get['filter_category_id'])) {
				$url .= '&filter_category_id=' . $this->request->get['filter_category_id'];
			}
			
			$this->data['breadcrumbs'][] = array(
				'text'      => $this->language->get('text_error'),
				'href'      => $this->url->link('product/product', $url . '&product_id=' . $product_id),
				'separator' => $this->language->get('text_separator')
			);			
			
			$this->document->setTitle($this->language->get('text_error'));
			
			$this->data['heading_title'] = $this->language->get('text_error');
			
			$this->data['text_error'] = $this->language->get('text_error');
			
			$this->data['button_continue'] = $this->language->get('button_continue');
			
			$this->data['continue'] = $this->url->link('common/home');
			
				if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/error/not_found.tpl')) {
					$this->template = $this->config->get('config_template') . '/template/error/not_found.tpl';
				} else {
					$this->template = 'default/template/error/not_found.tpl';
				}
				
				$this->children = array(
					'common/column_left',
					'common/column_right',
					'common/content_top',
					'common/content_bottom',
					//'supercheckout/supercheckout',
					'common/footer',
					'common/header'
				);			
				$this->response->setOutput($this->render());
		}
  	}
	
	public function review() {
		$this->language->load('product/product');
		
		$this->load->model('catalog/review');
		
		$this->data['text_on'] = $this->language->get('text_on');
		$this->data['text_no_reviews'] = $this->language->get('text_no_reviews');
		
		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}
		
		$this->data['reviews'] = array();
		
		$review_total = $this->model_catalog_review->getTotalReviewsByProductId($this->request->get['product_id']);
			
		$results = $this->model_catalog_review->getReviewsByProductId($this->request->get['product_id']);
      		
		foreach ($results as $result) {
			
		$this->load->model('account/customer');
		$this->load->model('tool/image');
		
			$customer_info = $this->model_account_customer->getCustomer($result['customer_id']);
			
			if(!empty($customer_info['image'])){
				if (strpos($customer_info['image'], 'http') === 0) {
					$author_pic = $customer_info['pic_square'];
				}else{
					$author_pic = $this->model_tool_image->resize($customer_info['image'], 50, 50);
				}
			} else {
				$author_pic = $this->model_tool_image->resize('no-avatar-women.png', 50, 50);
			}
			
			
		setlocale(LC_ALL, 'he_IL.UTF-8');
			
        	$this->data['reviews'][] = array(
        		'author'     => isset($customer_info['firstname']) ? $customer_info['firstname'] : "ללא שם" ,
			'author_pic' => $author_pic,
			'text'       => $result['text'],
			'rating'     => (int)$result['rating'],
        		'reviews'    => sprintf($this->language->get('text_reviews'), (int)$review_total),
        		'date_added' => strftime($this->language->get('date_format_long_strf'), strtotime($result['date_added']))
        	);
      	}			
			
		$pagination = new Pagination();
		$pagination->total = $review_total;
		$pagination->page = $page;
		$pagination->limit = 5; 
		$pagination->text = $this->language->get('text_pagination');
		$pagination->url = $this->url->link('product/product/review', 'product_id=' . $this->request->get['product_id'] . '&page={page}');
			
		$this->data['pagination'] = $pagination->render();
		
		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/product/review.tpl')) {
			$this->template = $this->config->get('config_template') . '/template/product/review.tpl';
		} else {
			$this->template = 'default/template/product/review.tpl';
		}
		
		$this->response->setOutput($this->render());
	}
	
	public function review_seller() {
		
		if (!headers_sent()) {
			header('Content-Type: text/html; charset=utf-8');
		}
		
		$this->language->load('product/product');
		
		$this->load->model('catalog/review');

		$this->data['text_on'] = $this->language->get('text_on');
		$this->data['text_no_reviews'] = $this->language->get('text_no_reviews');

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}
		
		
		$this->data['reviews'] = array();
		
		$review_total = $this->model_catalog_review->getTotalReviewsBySellerId($this->request->get['seller_id']);
		
		$results = $this->model_catalog_review->getReviewsBySellerId($this->request->get['seller_id'], 0, 100);
      		
		foreach ($results as $result) {
		
		$this->load->model('account/customer');
		$this->load->model('tool/image');
		
			$customer_info = $this->model_account_customer->getCustomer($result['customer_id']);
			if(!empty($customer_info['image'])){
				if (strpos($customer_info['image'], 'http') === 0) {
					$author_pic = $customer_info['pic_square'];
				}else{
					$author_pic = $this->model_tool_image->resize($customer_info['image'], 38, 38);
				}
			} else {
				$author_pic = $this->model_tool_image->resize('no-avatar-women.png', 38, 38);
			}
			
			$product_image = $this->model_tool_image->resize($result['image'], 70, 50);
			
			if($result['number_of_sides'] == 2){
				$total_sides = sprintf($this->language->get('total_sides_clear'), $result['number_of_sides']);
			}elseif($result['number_of_sides'] == 1){
				$total_sides = sprintf($this->language->get('total_sides_clear_one'), $result['number_of_sides']);
			}elseif($result['number_of_sides'] == 0){
				$total_sides = "";
			}
			
			setlocale(LC_ALL, 'he_IL.UTF-8');
			
			$this->data['reviews'][] = array(
				'author'     	=> $customer_info['firstname'],
				'author_pic' 	=> $author_pic,
				'text'       	=> $result['text'],
				'product_name'  => $result['name'],
				'product_href'  => $this->url->link('product/product', 'product_id=' . $result['product_id']),
				'total_sides'   => $total_sides,
				'image' 	=> $product_image,
				'rating'     	=> (int)$result['rating'],
				'reviews'    	=> sprintf($this->language->get('text_reviews'), (int)$review_total),
				'date_added' => strftime($this->language->get('date_format_long_strf'), strtotime($result['date_added']))
        	);
      	}			
			
		$pagination = new Pagination();
		$pagination->total = $review_total;
		$pagination->page = $page;
		$pagination->limit = 5; 
		$pagination->text = $this->language->get('text_pagination');
		$pagination->url = $this->url->link('product/product/review', 'seller_id=' . $this->request->get['seller_id'] . '&page={page}');
			
		$this->data['pagination'] = $pagination->render();
		
		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/product/review_seller.tpl')) {
			$this->template = $this->config->get('config_template') . '/template/product/review_seller.tpl';
		} else {
			$this->template = 'default/template/product/review.tpl';
		}
		
		$this->response->setOutput($this->render());
	}
	
	public function write() {
		$this->language->load('product/product');
		
		$this->load->model('catalog/review');
		
		$json = array();
		
		if ($this->request->server['REQUEST_METHOD'] == 'POST') {
			if ((utf8_strlen($this->request->post['name']) < 3) || (utf8_strlen($this->request->post['name']) > 25)) {
				$json['error'] = $this->language->get('error_name');
			}
			
			if ((utf8_strlen($this->request->post['text']) < 25) || (utf8_strlen($this->request->post['text']) > 1000)) {
				$json['error'] = $this->language->get('error_text');
			}
	
			if (empty($this->request->post['rating'])) {
				$json['error'] = $this->language->get('error_rating');
			}
	
			if (empty($this->session->data['captcha']) || ($this->session->data['captcha'] != $this->request->post['captcha'])) {
				$json['error'] = $this->language->get('error_captcha');
			}
				
			if (!isset($json['error'])) {
				$this->model_catalog_review->addReview($this->request->get['product_id'], $this->request->post);
				
				$json['success'] = $this->language->get('text_success');
			}
		}
		
		$this->response->setOutput(json_encode($json));
	}
	
	public function alargicRemember() {
		setcookie("alargic",1, time()+3600*24*356);	
		$this->response->setOutput(json_encode("success"));
	} 
	
	public function alargicForget() {
		setcookie("alargic",0, time()+3600*24*356);	
		$this->response->setOutput(json_encode("success"));
	}
	
	public function SubscribeToNewsletter() {
		
		$json = array();
		
		if(!isset($this->request->post['email']) || $this->request->post['email'] == ""){
			$json['error_email'] = 'no email was sent';
			$this->response->setOutput(json_encode($json));
			return;
		}
		
		if(!isset($this->request->post['city']) || $this->request->post['city']  == ""){
			$json['error_city'] = $this->request->post['city'];
			$this->response->setOutput(json_encode($json));
			return;
		}
		
		if(!isset($this->request->post['method']) || $this->request->post['method']  == ""){
			$json['error_method'] = 'no method was sent';
			$this->response->setOutput(json_encode($json));
			return;
		}
		
		$MailChimp = new \Drewm\MailChimp('3d058d8d6da676724f993faafba9c34c-us8');
		
		$data = array (
				'email' 	=> $this->request->post['email'],
				'city' 		=> isset($this->request->post['city']) ? $this->request->post['city'] : "",
				'firstname' 	=> isset($this->request->post['first']) ? $this->request->post['first'] : "",
				'lastname' 	=> isset($this->request->post['last']) ? $this->request->post['last'] : "",
				'method' 	=> isset($this->request->post['method']) ? $this->request->post['method'] : "",
			);
		
		$homeals_tlv = '43b8806f12';// 43b8806f12 Homeals Tel Aviv
		$homeals_il = 'b284333344';// b284333344 Homeals IL Users
		
		$list_id = $homeals_tlv;
		
		$result = $MailChimp->call('lists/subscribe', array(
				'id'                => $list_id,
				'email'             => array('email' => $data['email']),
				'merge_vars'        => array('FNAME' => $data['firstname'],
							     'LNAME' => $data['lastname'],
							     'CITY' => $data['city'],
							     'METHOD' => $data['method'],
							     'optin_ip' => $_SERVER['REMOTE_ADDR'],
							     'optin_time' => date("Y-m-d H:i:s")),
				'double_optin'      => false,
				'update_existing'   => true,
				'replace_interests' => false,
				'send_welcome'      => false,
			    ));
		
		$json['success'] = $result;
		
		$this->response->setOutput(json_encode($json));
	}
	
	public function writeMulti() {
		$this->language->load('product/product');
		
		$this->load->model('catalog/review');
		$this->load->model('catalog/product');
		
		$json = array();
		
		if ($this->request->server['REQUEST_METHOD'] == 'POST') {
			
			$delivery_info = (isset($this->request->post['delivery_info'])) ? $this->request->post['delivery_info'] : 0;
			
			if(isset($delivery_info['q1']) || isset($delivery_info['q2']) || isset($delivery_info['q3']) || isset($delivery_info['text'])){
				
				$q1 = (isset($delivery_info['q1'])) ? $delivery_info['q1'] : "no answer";
				$q2 = (isset($delivery_info['q2'])) ? $delivery_info['q2'] : "no answer";
				$q3 = (isset($delivery_info['q3'])) ? $delivery_info['q3'] : "no answer";
				$delivery_text = (isset($delivery_info['text'])) ? $delivery_info['text'] : "";
				$order_id = (isset($delivery_info['order_id'])) ? $delivery_info['order_id'] : 0;
				
				$query = $this->db->query("INSERT INTO " . DB_PREFIX . "review_delivery (`order_id`, `customer_id`, `q1`, `q2`, `q3`, `text`) VALUES ('".(int)$order_id."', '".$this->customer->getId()."', '".$this->db->escape($q1)."', '".$this->db->escape($q2)."', '".$this->db->escape($q3)."', '".$this->db->escape($delivery_text)."');");
				
			}
			
			$stars = $this->request->post['stars'];
			$texts = $this->request->post['texts'];
			
			$review_objs = array();
			
			foreach( $stars as $key => $value){
				if(!isset($review_objs[$key])){
					if($value){
						$review_objs[$key]["rating"] = $value;
						$review_objs[$key]["text"] = "";
						$review_objs[$key]["name"] = $this->customer->getFirstName()." ".$this->customer->getLastName();
					}
				}
			}
			
			foreach($texts as $key => $value){
				if($key == "order_id"){
					// nothing here
				} else {
					$review_objs[$key]["text"] = $value;
					$product_id = $key;
				}
				
				$product_id = $key;
			}
			
			$this->model_catalog_review->addReviewsMulti($review_objs);
			
			if (isset($this->request->post['cook_id'])){
				$cook_pm = $this->request->post['cook_pm'];
				$cook_id = $this->request->post['cook_id'];
			} else {
				$pro_info = $this->model_catalog_product->getProduct($product_id);
				$cook_id = $pro_info['user_id'];
			}
			
			
			$this->load->model('account/customer');
			
			$seller_info = $this->model_account_customer->getCustomer($cook_id);
			$seller_name = 	$seller_info['firstname'];
			$customer_name = $this->customer->getFirstName();
			$cook_email = 	$seller_info['email'];
			
			if(isset($this->request->post['cooks_data'])){
				$cooks_data = $this->request->post['cooks_data'];
				
				$pm_data = array();
				$data = array();
				
				foreach($cooks_data as $cook){
					$pm_cook_id = $cook[0];
					$pm_product_id = $cook[1];
					$pm_cook_pm_text = $cook[2];
					
					$pm_data[$pm_cook_id][$pm_product_id] = $pm_cook_pm_text;
				}
				
				//$json['cooks'] = $pm_data;
				
				$send_pm = array();
				
				foreach($pm_data as $cook_id => $pm) {
					
					$cook_info = $this->model_account_customer->getCustomer($cook_id);
					$cook_name = $seller_info['firstname'];
					$cook_email  = $seller_info['email'];
					
					foreach($pm as $product_id => $message ){
						$data = array(
								"cook_id"		=>	$cook_id,
								"product_id"		=>	$product_id,
								"message"		=>	$message,
								"customer_name" 	=> 	$customer_name,
								"cook_name" 		=>  	$cook_name,
								"cook_email" 		=>	$cook_email
							     );
						
						if($message != ""){
							$send_pm[] = $this->sendPMandMail($data);
						}
					}
				}
			}
			
			//$json['worked'] = $send_pm;
			$json['success'] = true;
			
		}
		
		$this->response->setOutput(json_encode($json));
	}
	
	
	/*******************************************************
	 *	sends PM and mail to cook after review
	 *
	 *	data: message,cook_id,product_id,customer_name,cook_name,cook_email
	 *******************************************************/		
	private function sendPMandMail($data) {
		
		$this->language->load('product/product');
		
		$this->load->model('catalog/product');
		
		$cook_pm = $data['message'];
		$cook_id = $data['cook_id'];
		$product_id = $data['product_id'];
		$customer_name = $data['customer_name'];
		$seller_name =  $data['cook_name'];
		$cook_email = $data['cook_email'];
		
		/*************************************
		*
		*	Sending PM For Review
		*
		*************************************/
		
		$message_text = trim($cook_pm);
		
		$conversation = $this->MsLoader->MsConversation->getConversations(
			array(
				'participant_id' => $cook_id,
				'other_participant_id' => $this->customer->getId(),
				'single' => true
			)
		);
		
		if(empty($conversation)){
			$conversation_id = $this->MsLoader->MsConversation->createConversation(
				array(
					'product_id' => $product_id,
					'title' => "new pm",
				)
			);
		} else {
			$conversation_id = $conversation['conversation_id'];
		}
		
		
		$this->MsLoader->MsMessage->createMessage(
			array(
				'conversation_id' => $conversation_id,
				'from' => $this->customer->getId(),
				'to' => $cook_id,
				'message' => $message_text
			)
		);
		
		/**********************************************************************************
		 *	homeals mail change
		 **********************************************************************************/
		$subject = sprintf($this->language->get('text_subject_pm'),$customer_name);
		
		$template = new Template();
		
		$template->data['title'] = $subject;
		$template->data['store_name'] = 'Homeals';
		$template->data['store_url'] = $this->config->get('config_url');
		$template->data['conv_url'] = $this->url->link('account/msmessage', '&conversation_id='.$conversation_id, 'SSL');
		$template->data['logo'] = $this->config->get('config_url') . 'image/' . $this->config->get('config_logo');
		
		$template->data['text_greeting'] = sprintf($this->language->get('text_greeting_pm'),$seller_name);
		$template->data['text_pre_msg'] = sprintf($this->language->get('text_pre_msg'),$customer_name);
		
		$template->data['message'] = '"'.mb_truncate($message_text,140).'"';
		
		// fotter
		$template->data['text_follow_us'] = $this->language->get('text_follow_us');			
		
		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/mail/pm.tpl')) {
			$html = $template->fetch($this->config->get('config_template') . '/template/mail/pm.tpl');
		} else {
			$html = $template->fetch('default/template/mail/pm.tpl');
		}
		
		$mail = new Mail(); 
		$mail->protocol = $this->config->get('config_mail_protocol');
		$mail->parameter = $this->config->get('config_mail_parameter');
		$mail->hostname = $this->config->get('config_smtp_host');
		$mail->username = $this->config->get('config_smtp_username');
		$mail->password = $this->config->get('config_smtp_password');
		$mail->port = $this->config->get('config_smtp_port');
		$mail->timeout = $this->config->get('config_smtp_timeout');
		
		$mail->setTo($cook_email);
		$mail->setFrom($this->config->get('config_email'));
		$mail->setSender('Homeals');
		$mail->setSubject(html_entity_decode($subject, ENT_QUOTES, 'UTF-8'));
		$mail->setHtml($html);
		$mail->send();
		
		/**********************************************************************************
		 *	homeals mail end
		 **********************************************************************************/
		
		return $cook_pm." sent.";
		
	}
	
	public function captcha() {
		$this->load->library('captcha');
		
		$captcha = new Captcha();
		
		$this->session->data['captcha'] = $captcha->getCode();
		
		$captcha->showImage();
	}
	
	public function review_ajax() {
		if($this->customer->isLogged()){
			$this->language->load('product/product');
			
			if (isset($this->request->get['product_id'])) {
				$product_id = (int)$this->request->get['product_id'];
			} else {
				$product_id = 0;
			}
			
			$this->load->model('catalog/product');
			
			// make sure we only fire once
			$this->session->data['review_flag'] = true;
			
			$product_info = $this->model_catalog_product->getProduct($product_id);
			
			$this->data['heading_title'] = $product_info['name'];
			
			$this->data['text_of'] = $this->language->get('text_of');
			
			$this->data['user_id'] = $product_info['user_id'];
			$this->data['username'] = $product_info['username'];
			$this->data['sp'] = $product_info['sp'];
			$this->data['cook_image'] = $product_info['cook_image'];
			
			$this->data['seller_id'] = $product_info['user_id'];
			
			$this->data['products'] = array();
			
			$results = $this->model_catalog_product->getProductRelated($this->request->get['product_id']);
			
			$this->data['total_sides_int'] = $product_info['number_of_sides'];
			
			$this->data['rate_word'][0] = $this->language->get('rate_word_0');
			$this->data['rate_word'][1] = $this->language->get('rate_word_1');
			$this->data['rate_word'][2] = $this->language->get('rate_word_2');
			$this->data['rate_word'][3] = $this->language->get('rate_word_3');
			$this->data['rate_word'][4] = $this->language->get('rate_word_4');
			$this->data['rate_word'][5] = $this->language->get('rate_word_5');
			
			$this->data['text_choose_side'] = $this->language->get('text_choose_side');
			$this->data['placeholder_meal'] = $this->language->get('placeholder_meal');
			$this->data['placeholder_delivery'] = $this->language->get('placeholder_delivery');
			$this->data['add_privet_review'] = $this->language->get('add_privet_review');
			$this->data['text_review_start'] = $this->language->get('text_review_start');
			
			$this->data['sface'] = $this->language->get('sface');
			$this->data['stwit'] = $this->language->get('stwit');
			
			$this->data['stwit_text'] = urlencode($this->data['heading_title'].": ");
			
			$this->data['smail'] = $this->language->get('smail');
			$this->data['smail_sub'] = $this->data['heading_title'];
			$this->data['smail_bud'] = sprintf($this->language->get('smail_bud_meal'),urlencode($this->url->link('product/product', 'product_id=' . $this->request->get['product_id'])));
			
			$this->data['text_cancel'] = 'לא מעוניין לדרג';
			$this->data['text_submit'] = $this->language->get('text_submit');
			
			$review_image = $this->model_tool_image->resize($product_info['image'], 212, 145);
			
			$review = array(
				'name' 		=> $product_info['name'],
				'image' 	=> $review_image,
			);
			
			$this->data['review'] = $review;
			
			foreach ($results as $result) {
				if ($result['image']) {
					$image = $this->model_tool_image->resize($result['image'], $this->config->get('config_image_related_width'), $this->config->get('config_image_related_height'));
					$review_image = $this->model_tool_image->resize($result['image'], 212, 145);
				} else {
					$image = $this->model_tool_image->resize('no_image.jpg', $this->config->get('config_image_related_width'), $this->config->get('config_image_related_height'));
					$review_image = $this->model_tool_image->resize('no_image.jpg', 212, 145);
				}
				
				if (($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) {
					$price = $this->currency->format($this->tax->calculate($result['price'], $result['tax_class_id'], $this->config->get('config_tax')));
					$price_int = (int)$result['price'];
				} else {
					$price = false;
					$price_int = false;
				}
				
				if ((float)$result['special']) {
					$special = $this->currency->format($this->tax->calculate($result['special'], $result['tax_class_id'], $this->config->get('config_tax')));
				} else {
					$special = false;
				}
				
				if ($this->config->get('config_review_status')) {
					$rating = (int)$result['rating'];
				} else {
					$rating = false;
				}
				
				$this->data['products'][] = array(
					'product_id' 	 => $result['product_id'],
					'tag'		 => $result['tag'],
					'thumb'   	 => $image,
					'image'   	 => $review_image,
					'name'    	 => $result['name'],
					'price'   	 => $price,
					'price_int'   	 => $price_int,
					'is_verified' 	 => $result['is_verified'],
					'text_extra' 	 => sprintf($this->language->get('text_extra'), $price),
					'special' 	 => $special,
					'rating'    	 => $rating,
					'reviews'    	 => sprintf($this->language->get('text_reviews'), (int)$result['reviews']),
					'href'    	 => $this->url->link('product/product', 'product_id=' . $result['product_id']),
				);
			}
			
			if($product_info['number_of_sides'] == 2){
				$this->data['text_sides'] = sprintf($this->language->get('total_sides'), 2 );
			}elseif($product_info['number_of_sides'] == 1 && count($this->data['products']) > 1 ){
				$this->data['text_sides'] = sprintf($this->language->get('total_sides_one'), 1 );
			}elseif($product_info['number_of_sides'] == 1 ){
				$this->data['text_sides'] = $this->language->get('total_sides_one_nc');
			}elseif($product_info['number_of_sides'] == 0 ){
				$this->data['text_sides'] = $this->language->get('text_sides');
			}
			
			$this->data['logged'] = $this->customer->isLogged();
			
			if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/product/product_review.tpl')) {
				$this->template = $this->config->get('config_template') . '/template/product/product_review.tpl';
			} else {
				$this->template = 'default/template/product/product_review.tpl';
			}
			
			$this->response->setOutput($this->render());	
		} else {
			return false;
		}
	}
	
	public function ajax_update() {
		$this->language->load('product/product');
		
		$json = array();
		
		if (isset($this->request->post['product_id'])) {
			$product_id = (int)$this->request->post['product_id'];
		} else {
			$product_id = 0;
		}
		
		// need to get the date we want is some form of rewust or we pick today as the date
		if(isset($this->request->post['date'])){
			$date = date('Y-m-d', strtotime($_REQUEST['date']));//print_r($_REQUEST['date']);
		}else{
			$date = date('Y-m-d');
		}
		
		$this->load->model('catalog/product');
		
		$product_info = $this->model_catalog_product->getProduct($product_id,$date);
		
		if($this->cart->hasProducts()){
			$cart_quantity = 0;
			$cart_items = $this->cart->getProducts();
			foreach($cart_items as $cart_item){
				if($cart_item['product_id'] == $product_id){
					$cart_quantity += $cart_item['quantity'];
				}
			}
		} else {
			$cart_quantity = 0;
		}
		
		if ($product_info) {
			
			$intqunt = $product_info['quantity'] - $cart_quantity;
			
			// if from edit cart we add the side dish names and id's
			if(isset($this->request->post['ec'])){
				
				$sides = array();
				
				$results = $this->model_catalog_product->getProductRelated($product_id);
				
				foreach ($results as $result) {
					if (($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) {
						$price = $this->currency->format($this->tax->calculate($result['price'], $result['tax_class_id'], $this->config->get('config_tax')));
						$price_int = (int)$result['price'];
					} else {
						$price = false;
						$price_int = false;
					}
							
					if ((float)$result['special']) {
						$special = $this->currency->format($this->tax->calculate($result['special'], $result['tax_class_id'], $this->config->get('config_tax')));
					} else {
						$special = false;
					}
					
					if ($this->config->get('config_review_status')) {
						$rating = (int)$result['rating'];
					} else {
						$rating = false;
					}
					
					$sides[] = array(
						'product_id' 	 => $result['product_id'],
						'name'    	 => $result['name'],
						'price'   	 => $price,
						'price_int'   	 => $price_int,
						'text_extra' 	 => sprintf($this->language->get('text_extra'), $price),
					);
				}
				
				$json['side_num'] = $product_info['number_of_sides'];
				$json['sides'] = $sides;
			}
			
			
			$json['intqunt'] = $intqunt;
			$json['quantity'] = sprintf($this->language->get('text_qty'), $intqunt);
			
			$times = array();
			
			$orders_by_time = $this->getFullTimes($date);
			$json['full'] = $orders_by_time;
			
			$end_times = $product_info['all_end_times'];
			
			foreach($product_info['all_order_times'] as $key => $value){
				
				//print_r($orders_by_time);
				//die;
				
				if(in_array($key,$orders_by_time)){
					$times[$key] = array(
							 'value' => $value,
							 'status' => 'time over booked',
							 'end_time' => $end_times[$key],
							 );	
				} else if(in_array($key,$product_info['order_times'])){
					$times[$key] = array(
							 'value' => $value,
							 'status' => 'ok',
							 'end_time' => $end_times[$key],
							 );
				} else {
					$times[$key] = array(
							 'value' => $value,
							 'status' => 'time passed',
							 'end_time' => $end_times[$key],
							 );
				}
			}
			
			//$json['times'] = $product_info['order_times'];
			$json['all_times'] = $times;
			
		} else {
			$json['failed'] = "no product";
		}
		
		$this->response->setOutput(json_encode($json));
	}
	
	public function getFullTimes($date) {
		// get todays defualt or custom settings for the day
		$date_today_name = date("l",strtotime($date));
		$todays_drivers_sql = "SELECT * FROM  ". DB_PREFIX ."driver_availability WHERE day LIKE  '" . $date_today_name  . "' OR custom_date =  '" . $date  . "'";
		$todays_drivers = $this->db->query($todays_drivers_sql);
		
		if($todays_drivers->num_rows > 1){
			foreach($todays_drivers->rows as $today_drivers_avail){
				if($today_drivers_avail['type'] == 'custom'){
					$driver_avail = $today_drivers_avail; 
				}
			}
		} else {
			$driver_avail = $todays_drivers->row;
		}
		
		// calculate the max number
		$max_number = $driver_avail['per_window']*$driver_avail['drivers'];
		
		// getting all todays orders
		$todays_orders_sql = "SELECT delivery_time,delivery_date,COUNT(delivery_date) as amount FROM ". DB_PREFIX ."order WHERE order_status_id IN (22) AND delivery_date = '" . $date  . "' GROUP BY delivery_time";
		$todays_orders = $this->db->query($todays_orders_sql);
		
		// merge orders by times and count how many per time
		
		$times_to_kill = array();
		
		foreach($todays_orders->rows as $todays_orders_by_time){
			if($todays_orders_by_time['amount'] >= $max_number){
				$times_to_kill[] = date("H",strtotime($todays_orders_by_time['delivery_time']));
			}
		}
		
		return $times_to_kill;
	}
	
	public function upload() {
		$this->language->load('product/product');
		
		$json = array();
		
		if (!empty($this->request->files['file']['name'])) {
			$filename = basename(preg_replace('/[^a-zA-Z0-9\.\-\s+]/', '', html_entity_decode($this->request->files['file']['name'], ENT_QUOTES, 'UTF-8')));
			
			if ((utf8_strlen($filename) < 3) || (utf8_strlen($filename) > 64)) {
        		$json['error'] = $this->language->get('error_filename');
	  		}	  	
			
			$allowed = array();
			
			$filetypes = explode(',', $this->config->get('config_upload_allowed'));
			
			foreach ($filetypes as $filetype) {
				$allowed[] = trim($filetype);
			}
			
			if (!in_array(substr(strrchr($filename, '.'), 1), $allowed)) {
				$json['error'] = $this->language->get('error_filetype');
       		}	
						
			if ($this->request->files['file']['error'] != UPLOAD_ERR_OK) {
				$json['error'] = $this->language->get('error_upload_' . $this->request->files['file']['error']);
			}
		} else {
			$json['error'] = $this->language->get('error_upload');
		}
		
		if (!$json) {
			if (is_uploaded_file($this->request->files['file']['tmp_name']) && file_exists($this->request->files['file']['tmp_name'])) {
				$file = basename($filename) . '.' . md5(mt_rand());
				
				// Hide the uploaded file name so people can not link to it directly.
				$json['file'] = $this->encryption->encrypt($file);
				
				move_uploaded_file($this->request->files['file']['tmp_name'], DIR_DOWNLOAD . $file);
			}
						
			$json['success'] = $this->language->get('text_upload');
		}	
		
		$this->response->setOutput(json_encode($json));		
	}
}
?>