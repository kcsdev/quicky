<?php 
class ControllerProductQuickyCategory extends Controller {

 public function index(){
     
     //get the lenauage
      $this->language->load('product/quickyCategory');

      $this->data['text_kitchen'] = $this->language->get('text_kitchen');
     $this->data['min_order'] = $this->language->get('min_order');
     $this->data['text_kosher'] = $this->language->get('text_kosher');
     $this->data['text_find'] = $this->language->get('text_find');
      $this->data['text_city'] = $this->language->get('text_city');
     
     //get the menu categories
     $this->load->model('account/customer');
     $this->data['category'] = $this->model_account_customer->getMenuCat();
     
     
     
     //get the restaurants
      $this->load->model('catalog/restaurants');
     $this->load->model('tool/image');
     $this->load->model('account/address');
     $this->load->model('account/customer');
     
     //get all the cities
     $this->data['cities'] = $this->model_catalog_restaurants->getCities();
     
     //get all the kosher values
     $this->data['kosher'] = $this->model_catalog_restaurants->getkosher();
     
     //get kosher option_id array
     $kosher_arr = $this->model_catalog_restaurants->getkosherarray();
     
     //get categories id array
     $cat_arr = $this->model_catalog_restaurants->getCatId();

     //get information
     if(isset($_POST['filter-kitchen']) && isset($_POST['filter-cost']) && isset($_POST['filter-kosher'])){

      $this->session->data['city'] = $_POST['filter-cost'];
      $restaurants = $this->model_catalog_restaurants->getRestFilter($_POST);
     
     
     }elseif(isset($_SESSION['city'])){
     
       $restaurants = $this->model_catalog_restaurants->getRestCity($_SESSION['city']);
         
     }elseif(isset($_POST['city'])){
       $this->session->data['city'] = $_POST['city'];
       $restaurants = $this->model_catalog_restaurants->getRestCity($_SESSION['city']);
         
     }elseif(isset($_GET['cat'])){
         
         $restaurants = $this->model_catalog_restaurants->getRestCat($_GET['cat']);
         $this->data['rest_cat'] = $this->model_catalog_restaurants->getCat($_GET['cat']);
     
     }else{
         
     $restaurants = $this->model_catalog_restaurants->getRest();
         
    }
     
     
     $cat = 	$this->model_account_customer->getMenuCat();
     

    foreach ($restaurants as $restaurant) {
             
                
			$rest_id = $restaurant['customer_id'];

             //get kosher id
                foreach($kosher_arr as $kos){
            
                  $kostype = $this->model_catalog_restaurants->getkoshertype($kos['option_value_id'], $rest_id);
                  if(!empty($kostype)){

                     $kos_id = $kos['option_value_id'];
                      break;
                  }
                }
        
          // get categories id
         $the_cat = '';
            foreach($cat_arr as $cat){
            
                $cattype = $this->model_catalog_restaurants->getCatType($cat['option_value_id'], $rest_id);
                
                if(!empty($cattype)){
                
                   $the_cat .= $cat['option_value_id'] . ',';
                 }
            
            }
        
        //get rest list
        $rest_list_city = '';
        $cities_list = $this->model_catalog_restaurants->getRestlist($rest_id);
        $rest_city_list = json_decode($cities_list[0]['data']);
        foreach($rest_city_list as $list){
        $rest_list_city .= $list->city . ',';
        
        }
        
			if ($restaurant['image'] && file_exists(DIR_IMAGE . $restaurant['image'])) {
				$cimage = $this->model_tool_image->resize($restaurant['image'], 305, 163);
			} else {
				$cimage = $this->model_tool_image->resize('ms_no_image.jpg', 305, 163);
			}
            
            //get the days
            $days = $this->model_account_customer->getDays($rest_id);
            $day_arr = json_decode($days[0]['data']);
	        $day =  idate("w") + 1;
            $current_day = $day_arr->$day;
           
 
			$restaurant_address = $this->model_account_address->getAddressesById($rest_id);
			// getting only the first address
			$restaurant_address = reset($restaurant_address);
			$this->data['restaruants'][] = array(
				'restaruant_id'  	=> $rest_id,
				'name'        		=> $restaurant['firstname'] . ' ' .  $restaurant['lastname'],
				'image' 		=> $cimage,
				'sp' 			=> $restaurant['sp'],
				'description' 		=> utf8_substr(strip_tags(html_entity_decode($restaurant['profile_description'], ENT_QUOTES, 'UTF-8')), 0, 135) . '...',
				'address' 		=> $restaurant_address,
                'start' => $current_day->start,
               'close' => $current_day->close,
                'kos_id' => $kos_id,
                'cat' => $the_cat,
                'city_list' => $rest_list_city,
				'href'  => $this->url->link('seller/catalog-seller/profile', '&seller_id=' . $rest_id)
			);
                
        //get min order
            if(isset($_SESSION['city'])){
            $order_city = json_decode($restaurant['data']);
              foreach($order_city as $order){
                  if($_SESSION['city'] == $order->city){
                     $this->data['restaruants'][count($this->data['restaruants']) - 1]['min_order'] = $order->min_order;
                     $this->data['restaruants'][count($this->data['restaruants']) - 1]['pay'] = $order->pay;
                      $this->data['restaruants'][count($this->data['restaruants']) - 1]['timeof'] = $order->time;
                    }
                  
                }
            }
		}
     
     
     
        $this->template = $this->config->get('config_template') . '/template/product/quicky_category.tpl';
		$this->children = array(
			'common/header',
			'common/footer'
		);
		
		$this->response->setOutput($this->render());
 
 
 }
    
    public function createSess(){
    
     $this->session->data['city'] = $_POST['city'];
    
    }




}