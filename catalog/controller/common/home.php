<?php  
class ControllerCommonHome extends Controller {
	public function index() {
		
		// moves loged in customers to the category page
		/*
		if($this->customer->isLogged() && !isset($this->request->get['allow'])){
			if($this->customer->getCustomerGroupId() == 2){
				$this->redirect($this->url->link('seller/account-my-orders','','SSL'));
			} else {
				$this->redirect($this->url->link('product/category','&path=0','SSL'));
			}
		}
		*/
        
        //unset session city
        if(isset($this->session->data['city'])){
          $this->redirect($this->url->link('product/quickyCategory', '', 'SSL')); 
        }
		
		// add for slider
		//$this->document->addScript('catalog/view/javascript/homeals/jssor.slider.mini.js');
		
		$this->document->setTitle($this->config->get('config_title'));
		$this->document->setDescription($this->config->get('config_meta_description'));
		
		$this->data['heading_title'] = $this->config->get('config_title');
		
		/*****************************************************************
		 *			HEADER START
		 ****************************************************************/
		
		if (isset($this->request->server['HTTPS']) && (($this->request->server['HTTPS'] == 'on') || ($this->request->server['HTTPS'] == '1'))) {
			$server = $this->config->get('config_ssl');
		} else {
			$server = $this->config->get('config_url');
		}
		
		// logo change to somthing more dynamic
		$this->data['logo'] = $server . 'image/data/logo.png';
		
		$this->data['logged'] = $this->customer->isLogged();
		
		/*****************************************************************
		 *			SEARCH START
		 ****************************************************************/
		
		$this->load->model('account/customer');
		$this->load->model('account/address');
		$this->load->model('tool/image');
		$this->load->model('catalog/category');
		
		/*****************************************************************
		 *			CATEGORY MENU START
		 ****************************************************************/
		
		$this->data['categories'] = array();
		
		$categories = $this->model_catalog_category->getCategories(0);
		
		foreach ($categories as $category) {
			
			//fix image
			if ($category['image'] && file_exists(DIR_IMAGE . $category['image'])) {
				$cat_image = $this->model_tool_image->resize( $category['image'] , $this->config->get('config_image_popup_width'), $this->config->get('config_image_popup_height'));
			} else {
				$cat_image = $this->model_tool_image->resize( 'ms_no_image.jpg' , $this->config->get('config_image_popup_width'), $this->config->get('config_image_popup_height'));
			}
			
			$this->data['categories'][] = array(
				'category_id' 	=> $category['category_id'],
				'image'       	=> $cat_image,
				'name'        	=> $category['name'],
				'href'        	=> $this->url->link('product/category', 'path=' . $category['category_id'])
			);	
		}
			
		/*****************************************************************
		 *			INFO START
		 ****************************************************************/
		
		$this->data['img_dir'] = "catalog/view/theme/" . $this->config->get('config_template') . "/image/" ;
		
		$this->data['cat_link'] = $this->url->link('product/category','&path=0');
		
		/*****************************************************************
		 *			RESTAURANTS START
		 ****************************************************************/
        
        $this->language->load('module/page_header_image');
        
        //leanguage 
        
        $this->data['heading_title'] = $this->language->get('heading_title');
		$this->data['text_filter'] = $this->language->get('text_filter');
		
		$this->data['filter_kitchen'] = $this->language->get('filter_kitchen');
		$this->data['filter_kitchen_placeholder'] = $this->language->get('filter_kitchen_placeholder');
		
		$this->data['filter_by_cost'] = $this->language->get('filter_by_cost');
		$this->data['filter_by_cost_placeholder'] = $this->language->get('filter_by_cost_placeholder');
		
		$this->data['filter_by_kosher'] = $this->language->get('filter_by_kosher');
		$this->data['filter_by_kosher_placeholder'] = $this->language->get('filter_by_kosher_placeholder');
		
		$this->data['find_order'] = $this->language->get('find_order');
        
        
        
        
		
		$results = array();
		
		$rest_data = array( 	"filter_customer_group_id" => "2" ,
					"start" => "0" ,
					"limit" => "9" ,
				);
		
		$rest_info = $this->model_account_customer->getCustomers($rest_data);
		$cat = 	$this->model_account_customer->getMenuCat();
        
        $this->data['cat'] = $cat;
		foreach ($rest_info as $restaurant) {
			$rest_id = $restaurant['customer_id'];
			
			if ($restaurant['image'] && file_exists(DIR_IMAGE . $restaurant['image'])) {
				$cimage = $this->model_tool_image->resize($restaurant['image'], 305, 163);
			} else {
				$cimage = $this->model_tool_image->resize('ms_no_image.jpg', 305, 163);
			}
            
            //get the days
            
            $days = $this->model_account_customer->getDays($rest_id);
            $day_arr = json_decode($days[0]['data']);
	        $day =  idate("w") + 1;
            $current_day = $day_arr->$day;
           
 
			$restaurant_address = $this->model_account_address->getAddressesById($rest_id);
			// getting only the first address
			$restaurant_address = reset($restaurant_address);
			
			$this->data['restaruants'][] = array(
				'restaruant_id'  	=> $rest_id,
				'name'        		=> $restaurant['firstname'],
				'image' 		=> $cimage,
				'sp' 			=> $restaurant['sp'],
				'description' 		=> utf8_substr(strip_tags(html_entity_decode($restaurant['profile_description'], ENT_QUOTES, 'UTF-8')), 0, 135) . '...',
				'address' 		=> $restaurant_address,
                'start' => $current_day->start,
                'close' => $current_day->close,
				/*
				// not working as it should
				'total_sales'	 	=> $this->MsLoader->MsSeller->getSalesForSeller($rest_id),
				//not working
				'total_products' 	=> $this->MsLoader->MsProduct->getTotalProducts(array(
														'seller_id' 		=> $rest_id,
														'product_status' 	=> array(MsProduct::STATUS_ACTIVE)
													)),
				*/
				'href'  => $this->url->link('seller/catalog-seller/profile', '&seller_id=' . $rest_id)
			);
		}
        

		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/common/home.tpl')) {
			$this->template = $this->config->get('config_template') . '/template/common/home.tpl';
		} else {
			$this->template = 'default/template/common/home.tpl';
		}
		
		$this->children = array(
			'common/footer',
			'common/header'
		);
		
		$this->response->setOutput($this->render());
	}
}
?>