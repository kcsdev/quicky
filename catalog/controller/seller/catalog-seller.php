<?php 
class ControllerSellerCatalogSeller extends ControllerSellerCatalog {
	public function __construct($registry) {
		parent::__construct($registry);
		
		$this->language->load('product/category');
		$this->load->model('localisation/country');
		$this->load->model('catalog/category');
		$this->load->model('catalog/product');
		$this->load->model('tool/image');
	}
	
	public function index() {

		$this->document->addScript('catalog/view/javascript/jquery/jquery.total-storage.min.js');
		
		$this->data['text_display'] = $this->language->get('text_display');
		$this->data['text_list'] = $this->language->get('text_list');
		$this->data['text_grid'] = $this->language->get('text_grid');
		$this->data['text_sort'] = $this->language->get('text_sort');
		$this->data['text_limit'] = $this->language->get('text_limit');
		
		if (isset($this->request->get['sort'])) {
			$order_by = $this->request->get['sort'];
		} else {
			$order_by = 'ms.nickname';
		}
		
		if (isset($this->request->get['order'])) {
			$order_way = $this->request->get['order'];
		} else {
			$order_way = 'ASC';
		}
		
		$page = isset($this->request->get['page']) ? $this->request->get['page'] : 1;
		
		if (isset($this->request->get['limit'])) {
			$limit = $this->request->get['limit'];
		} else {
			$limit = $this->config->get('config_catalog_limit');
		}
		
		$this->data['products'] = array();
		
		$total_sellers = $this->MsLoader->MsSeller->getTotalSellers(array(
			'seller_status' => array(MsSeller::STATUS_ACTIVE) 
		));
		
		$results = $this->MsLoader->MsSeller->getSellers(
			array(
				'seller_status' => array(MsSeller::STATUS_ACTIVE)
			),
			array(
				'order_by'	=> $order_by,
				'order_way'	=> $order_way,
				'offset'	=> ($page - 1) * $limit,
				'limit'		=> $limit
			)
		);
		
		foreach ($results as $result) {
			if ($result['ms.avatar'] && file_exists(DIR_IMAGE . $result['ms.avatar'])) {
				$image = $this->MsLoader->MsFile->resizeImage($result['ms.avatar'], $this->config->get('msconf_seller_avatar_seller_list_image_width'), $this->config->get('msconf_seller_avatar_seller_list_image_height'));
			} else {
				$image = $this->MsLoader->MsFile->resizeImage('ms_no_image.jpg', $this->config->get('msconf_seller_avatar_seller_list_image_width'), $this->config->get('msconf_seller_avatar_seller_list_image_height'));
			}


			$rate = $this->MsLoader->MsSeller->getRate(array('seller_id' => $result['seller_id']), true);

			$country = $this->model_localisation_country->getCountry($result['ms.country_id']);
			$this->data['sellers'][] = array(
				'seller_id'  => $result['seller_id'],
				'thumb'       => $image,
				'nickname'        => $result['ms.nickname'],
				'description' => utf8_substr(strip_tags(html_entity_decode($result['ms.description'], ENT_QUOTES, 'UTF-8')), 0, 200) . '..',
				/*'rating'      => $result['rating'],*/
				'country' => ($country ? $country['name'] : NULL),
				'company' => ($result['ms.company'] ? $result['ms.company'] : NULL),
				'website' => ($result['ms.website'] ? $result['ms.website'] : NULL),
				'country_flag' => ($country ? 'image/flags/' . strtolower($country['iso_code_2']) . '.png' : NULL),
				'total_sales' => $this->MsLoader->MsSeller->getSalesForSeller($result['seller_id']),
				'total_products' => $this->MsLoader->MsProduct->getTotalProducts(array(
					'seller_id' => $result['seller_id'],
					'product_status' => array(MsProduct::STATUS_ACTIVE)
				)),
				'href'        => $this->url->link('seller/catalog-seller/profile', '&seller_id=' . $result['seller_id']),
				'avg' => round((float)$rate['avg_overall'], 2),
				'total_votes' => count($rate['rows'])
			);
		}
		
		$url = '';

		if (isset($this->request->get['limit'])) {
			$url .= '&limit=' . $this->request->get['limit'];
		}
						
		$this->data['sorts'] = array();
		
		$this->data['sorts'][] = array(
			'text'  => $this->language->get('ms_sort_nickname_asc'),
			'value' => 'ms.nickname-ASC',
			'href'  => $this->url->link('seller/catalog-seller', '&sort=ms.nickname&order=ASC' . $url)
		);

		$this->data['sorts'][] = array(
			'text'  => $this->language->get('ms_sort_nickname_desc'),
			'value' => 'ms.nickname-DESC',
			'href'  => $this->url->link('seller/catalog-seller', '&sort=ms.nickname&order=DESC' . $url)
		);

		$this->data['sorts'][] = array(
			'text'  => $this->language->get('ms_sort_country_asc'),
			'value' => 'ms.country_id-ASC',
			'href'  => $this->url->link('seller/catalog-seller', '&sort=ms.country_id&order=ASC' . $url)
		); 

		$this->data['sorts'][] = array(
			'text'  => $this->language->get('ms_sort_country_desc'),
			'value' => 'ms.country_id-DESC',
			'href'  => $this->url->link('seller/catalog-seller', '&sort=ms.country_id&order=DESC' . $url)
		); 
		
		$url = '';

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}	

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}
		
		
		$this->data['limits'] = array();
		
		$this->data['limits'][] = array(
			'text'  => $this->config->get('config_catalog_limit'),
			'value' => $this->config->get('config_catalog_limit'),
			'href'  => $this->url->link('seller/catalog-seller', $url . '&limit=' . $this->config->get('config_catalog_limit'))
		);
					
		$this->data['limits'][] = array(
			'text'  => 25,
			'value' => 25,
			'href'  => $this->url->link('seller/catalog-seller', $url . '&limit=25')
		);
		
		$this->data['limits'][] = array(
			'text'  => 50,
			'value' => 50,
			'href'  => $this->url->link('seller/catalog-seller', $url . '&limit=50')
		);

		$this->data['limits'][] = array(
			'text'  => 75,
			'value' => 75,
			'href'  => $this->url->link('seller/catalog-seller', $url . '&limit=75')
		);
		
		$this->data['limits'][] = array(
			'text'  => 100,
			'value' => 100,
			'href'  => $this->url->link('seller/catalog-seller', $url . '&limit=100')
		);
		
		$url = '';

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}	
		
		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}
		
		if (isset($this->request->get['limit'])) {
			$url .= '&limit=' . $this->request->get['limit'];
		}
		
		$pagination = new Pagination();
		$pagination->total = $total_sellers;
		$pagination->page = $page;
		$pagination->limit = $limit;
		$pagination->text = $this->language->get('text_pagination');
		$pagination->url = $this->url->link('seller/catalog-seller', $url . '&page={page}');
		
		$this->data['pagination'] = $pagination->render();
		
		$this->data['sort'] = $order_by;
		$this->data['order'] = $order_way;
		$this->data['limit'] = $limit;		
		
		$this->data['continue'] = $this->url->link('common/home');
		
		$this->document->setTitle($this->language->get('ms_catalog_sellers_heading'));
		
		$this->data['breadcrumbs'] = $this->MsLoader->MsHelper->setBreadcrumbs(array(
			array(
				'text' => $this->language->get('ms_catalog_sellers'),
				'href' => $this->url->link('seller/catalog-seller', '', 'SSL'), 
			)
		));
		
		list($this->template, $this->children) = $this->MsLoader->MsHelper->loadTemplate('catalog-seller');
		$this->response->setOutput($this->render());
	}
		
	public function profile() {
         
		//$this->document->addScript('catalog/view/javascript/jquery/tabs.js');
		$this->load->model('account/customer');
        
        //get the cities
        $this->load->model('catalog/restaurants');
         $rest_cities = $this->model_catalog_restaurants->getProfileCities($_GET['seller_id']);
        if(!empty($rest_cities)){
        $this->data['cities'] = json_decode($rest_cities[0]['data']);

        if(isset($_POST['city'])){
         $this->session->data['city'] = $_POST['city'];
 
        }
        if(isset($this->session->data['city'])){
        
        $cities = $this->model_catalog_restaurants->getProfileRest($_GET['seller_id'], $this->session->data['city']);
        $cities_list = json_decode($cities[0]['data']);
           foreach($cities_list as $list){
               if($list->city == $this->session->data['city']){
                  
                  $this->data['order_val'] = $list;
                  $this->session->data['pay'] = $list->pay;
               }
        
            }
        }
        }
		if (isset($this->request->get['seller_id'])) {
			$seller = $this->MsLoader->MsSeller->getSeller($this->request->get['seller_id']);
            //get mail
            $mail = $this->model_account_customer->getMail($this->request->get['seller_id']);
			$customer = $this->model_account_customer->getCustomerByEmail($mail[0]['email']);
            $menu_cat = $this->model_account_customer->getMenu($this->request->get['seller_id']);
			// useless for now
			// $rate = $this->MsLoader->MsSeller->getRate(array('seller_id' => (int)$this->request->get['seller_id']), true);
		}
	//didn't bring new restaurant check this	
	/*	if (!isset($seller) || empty($seller) || $seller['ms.seller_status'] != MsSeller::STATUS_ACTIVE) {

			$this->redirect($this->url->link('product/category', '&path=0', 'SSL'));
            
			return;
		}*/

        if(!empty($menu_cat)){
         $menu = json_decode($menu_cat['data']);
         $this->data['menu_cat'] = $menu;
        }else{
        
        $this->data['menu_cat'] = '';
        
        }
		
		/*
		if ($this->config->get('msconf_seller_comments_enable')) {
			$this->document->addScript('catalog/view/javascript/ms-comments.js');
			
			if(file_exists('catalog/view/theme/'.$this->config->get('config_template').'/stylesheet/ms-comments.css')){
				$this->document->addStyle("catalog/view/theme/" . $this->config->get('config_template') . "/stylesheet/ms-comments.css");
			} else {
				$this->document->addStyle("catalog/view/theme/default/stylesheet/ms-comments.css");
			}
		}
		*/
		
		$this->document->addScript('catalog/view/javascript/dialog-sellercontact.js');
		
		if ($customer['image'] && file_exists(DIR_IMAGE . $customer['image'])) {//($seller['ms.avatar'] && file_exists(DIR_IMAGE . $seller['ms.avatar'])) {
			$cimage = "image/".$customer['image'];//$this->model_tool_image->resize($customer['image'], 200, 200);
			//$cimage = $this->MsLoader->MsFile->resizeImage($customer['image'], 200,200);
			//$image = $this->MsLoader->MsFile->resizeImage($seller['ms.avatar'], $this->config->get('msconf_seller_avatar_seller_profile_image_width'), $this->config->get('msconf_seller_avatar_seller_profile_image_height'));
		} else {
			$cimage = "image/".$this->model_tool_image->resize('no-avatar-women.png', 200, 200);
			//$cimage = $this->MsLoader->MsFile->resizeImage('ms_no_image.jpg', $this->config->get('msconf_seller_avatar_seller_profile_image_width'), $this->config->get('msconf_seller_avatar_seller_profile_image_height'));
			//$image = $this->MsLoader->MsFile->resizeImage('ms_no_image.jpg', $this->config->get('msconf_seller_avatar_seller_profile_image_width'), $this->config->get('msconf_seller_avatar_seller_profile_image_height'));
		}
		
		$this->load->model('account/address');
		
		$cook_addresses = $this->model_account_address->getAddressesById($customer['customer_id']);
		$cook_address = reset($cook_addresses);
		$cook_city = $cook_address['city'];
		
		$this->document->setOgTitle($customer['firstname'].", ".$cook_city);
		$this->document->setOgImage($cimage);
		$this->document->setDescription($customer['sp']);
		
		$this->data['add_to_order'] = "הוספה להזמנה";
		$this->data['rest_comments_text'] = "הערות כלליות:";
		$this->data['rest_legend_text'] = "מקרא:";
		
		$this->data['seller']['sex'] = $customer['sex'];
		$this->data['seller']['birth_day'] = $customer['birth_day'];
		$this->data['seller']['profile_description'] = $customer['profile_description'];
		$this->data['seller']['image'] = $cimage;
		$this->data['seller']['sp'] = $customer['sp'];
		$this->data['seller']['telephone'] = $customer['telephone'];
		$this->data['seller']['name'] = $customer['firstname'];
		
		$this->data['seller']['info_line_1'] = sprintf($this->language->get('info_line_1'),$cook_city,$customer['sp']);
		$this->data['seller']['info_line_2'] = sprintf($this->language->get('info_line_2'),$cook_address['address_1'],$cook_city);;
		$this->data['seller']['info_line_3'] = sprintf($this->language->get('info_line_3'),$customer['telephone']);;
		
		$this->data['seller']['nickname'] = $seller['ms.nickname'];
		$this->data['seller']['seller_id'] = $seller['seller_id'];
		$this->data['seller']['description'] = html_entity_decode($seller['ms.description'], ENT_QUOTES, 'UTF-8');
		$this->data['seller']['thumb'] = "";//$image;
		$this->data['seller']['href'] = $this->url->link('seller/catalog-seller/products', 'seller_id=' . $seller['seller_id']);
		$this->data['seller_id'] = $this->request->get['seller_id'];
		/*
		$this->data['avg_overall'] = round((float)$rate['avg_overall'], 2);
		$this->data['avg_communication'] = round((float)$rate['avg_communication'], 2);
		$this->data['avg_honesty'] = round((float)$rate['avg_honesty'], 2);
		$this->data['total_votes'] =  count($rate['rows']);
		*/
		
		$this->data['text_totalsides'] = sprintf($this->language->get('text_totalsides'), (2));
		
		$this->data['text_read_more'] =$this->language->get('text_read_more');
		$this->data['text_read_less'] =$this->language->get('text_read_less');
		$this->data['text_permeal'] = $this->language->get('text_permeal');
		$this->data['text_reviewsmin'] = $this->language->get('text_reviewsmin');
		$this->data['button_cart'] = $this->language->get('button_cart');
		$this->data['button_wishlist'] = $this->language->get('button_wishlist');
		$this->data['button_compare'] = $this->language->get('button_compare');
		$this->data['button_continue'] = $this->language->get('button_continue');
		
		$this->data['text_sp'] = $this->language->get('text_sp');
		$this->data['text_contactme'] = $this->language->get('text_contactme');
		$this->data['text_share'] = $this->language->get('text_share');
		$this->data['text_aboutme'] = $this->language->get('text_aboutme');
		
		$this->data['text_mymeals'] = $this->language->get('text_mymeals');
		$this->data['text_cookreviews'] = $this->language->get('text_cookreviews');
		$this->data['text_rating'] = $this->language->get('text_rating');
		$this->data['text_recommended'] = $this->language->get('text_recommended');
		$this->data['text_reviews_text'] = $this->language->get('text_reviews_text');
		$this->data['text_per_meal'] = $this->language->get('text_per_meal');
		
		$this->data['sface'] = $this->language->get('sface');
		$this->data['stwit'] = $this->language->get('stwit');
		$this->data['smail'] = $this->language->get('smail');
		
		$this->data['title_bulk_order'] = sprintf($this->language->get('title_bulk_order'),$customer['firstname']);
		$this->data['text_bulk_order'] = sprintf($this->language->get('text_bulk_order'),$customer['firstname']);
		
		$this->data['smail_sub'] = sprintf($this->language->get('smail_sub'),$customer['firstname']);
		$this->data['smail_bud'] = sprintf($this->language->get('smail_bud'),$customer['sp'],urlencode($this->url->link('seller/catalog-seller/profile').'&seller_id='.$customer['customer_id']));
		
		$this->load->model('catalog/review');
		
		$review_rating = $this->model_catalog_review->getRatingBySellerId($this->request->get['seller_id']);
		$review_total = $this->model_catalog_review->getTotalReviewsBySellerId($this->request->get['seller_id']);
		
		$positive_reviews = 0;
		foreach($review_rating as $rating){
			if ($rating['rating'] > 3){
				$positive_reviews++;
			}
		}
		if($positive_reviews){
			$positive_rating = (round($positive_reviews / count($review_rating),2)*100);
		} else {
			$positive_rating = 0;
		}
		
		$this->data['description_max_size'] = 400; // make more dynamic
		
		$this->data['review_rating'] = $positive_rating;
		$this->data['review_total'] = $review_total;
		
		$country = $this->model_localisation_country->getCountry($seller['ms.country_id']);
		
		if (!empty($country)) {			
			$this->data['seller']['country'] = $country['name'];
		} else {
			$this->data['seller']['country'] = NULL;
		}
		
		if (!empty($seller['ms.company'])) {
			$this->data['seller']['company'] = $seller['ms.company'];
		} else {
			$this->data['seller']['company'] = NULL;
		}
		
		if (!empty($seller['ms.website'])) {
			$this->data['seller']['website'] = $seller['ms.website'];
		} else {
			$this->data['seller']['website'] = NULL;
		}
		
		// not working
		/*
		$this->data['seller']['total_sales'] = $this->MsLoader->MsSeller->getSalesForSeller($seller['seller_id']);
		$this->data['seller']['total_products'] = $this->MsLoader->MsProduct->getTotalProducts(array(
			'seller_id' => $seller['seller_id'],
			'product_status' => array(MsProduct::STATUS_ACTIVE)
		));
		*/
				
		$this->data['images'] = array();
			
		$results = $this->model_account_customer->getCustomerImages($this->request->get['seller_id']);
		
		foreach ($results as $result) {
			$this->data['images'][] = array(
				'popup' => $this->model_tool_image->resize($result['image'], $this->config->get('config_image_popup_width'), $this->config->get('config_image_popup_height')),
				'thumb' => $this->model_tool_image->resize($result['image'], $this->config->get('config_image_additional_width'), $this->config->get('config_image_additional_height'))
			);
		}
		$img = $this->model_account_customer->getManiImage($this->request->get['seller_id']);
        $this->data['img'] = $img[0]['image'];
		// badges - useless
		/*
		$badges = array_unique(array_merge(
			$this->MsLoader->MsBadge->getSellerGroupBadges(array('seller_id' => $seller['seller_id'], 'language_id' => $this->config->get('config_language_id'))),
			$this->MsLoader->MsBadge->getSellerGroupBadges(array('seller_group_id' => $seller['ms.seller_group'], 'language_id' => $this->config->get('config_language_id'))),
			$this->MsLoader->MsBadge->getSellerGroupBadges(array('seller_group_id' => $this->config->get('msconf_default_seller_group_id'), 'language_id' => $this->config->get('config_language_id')))
		), SORT_REGULAR);
		
		foreach ($badges as &$badge) {
			$badge['image'] = $this->model_tool_image->resize($badge['image'], $this->config->get('msconf_badge_width'), $this->config->get('msconf_badge_height'));
		}
		$this->data['seller']['badges'] = $badges;
		*/
		
		$this->document->setPageImage('image/' . $img[0]['image']);
		
		//$this->load->model('tool/image');
		
		//
		
		$product_data = array('filter_customer_id' => $customer['customer_id']);

		$products = $this->model_catalog_product->getProducts($product_data);
 
		if (!empty($products)) {
			foreach ($products as $product) {
                
                if($product['product_id']){
                     $category = $this->model_catalog_product->getProductCat($product['product_id']);  
                    if(!empty($category)){
                    $cat = json_decode($category['data']);  
                    }else{
                    
                      $cat = '';
                    }
                   }
                
                
				if ($product['image'] && file_exists(DIR_IMAGE . $product['image'])) {
					$image_big = $this->model_tool_image->resize($product['image'], 580, 400);
					$image = $this->model_tool_image->resize($product['image'], 50, 35);
				} else {
					$image_big = $this->model_tool_image->resize('no_image.jpg', 580, 400);
					$image = $this->MsLoader->MsFile->resizeImage('no_image.jpg', 50, 35);
				}
				
				if (($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) {
					$price = $this->currency->format($this->tax->calculate($product['price'], $product['tax_class_id'], $this->config->get('config_tax')));
				} else {
					$price = false;
				}
				
				if ((float)$product['special']) {
					$special = $this->currency->format($this->tax->calculate($product['special'], $product['tax_class_id'], $this->config->get('config_tax')));
				} else {
					$special = false;
				}
				
				if ($this->config->get('config_review_status')) {
					$rating = $product['rating'];
				} else {
					$rating = false;
				}
				
				$side_results = $this->model_catalog_product->getProductRelated($product['product_id']);
				$side_data = array();
				
				foreach ($side_results as $side_result) {
					if ($side_result['image']) {
						$side_image = $this->model_tool_image->resize($side_result['image'], $this->config->get('config_image_related_width'), $this->config->get('config_image_related_height'));
						$review_image = $this->model_tool_image->resize($side_result['image'], 212, 145);
					} else {
						$side_image = $this->model_tool_image->resize('no_image.jpg', $this->config->get('config_image_related_width'), $this->config->get('config_image_related_height'));
						$review_image = $this->model_tool_image->resize('no_image.jpg', 212, 145);
					}
					
					if (($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) {
						$side_price = $this->currency->format($this->tax->calculate($side_result['price'], $side_result['tax_class_id'], $this->config->get('config_tax')));
						$side_price_int = (int)$side_result['price'];
					} else {
						$side_price = false;
						$side_price_int = false;
					}
					
					$side_data[] = array(
						'product_id' 	 => $side_result['product_id'],
						'thumb'   	 => $side_image,
						'image'   	 => $review_image,
						'name'    	 => $side_result['name'],
						'price'   	 => $side_price,
						'price_int'   	 => $side_price_int,
						'text_extra' 	 => sprintf($this->language->get('text_extra'), $side_price),
						'href'    	 => $this->url->link('product/product', 'product_id=' . $side_result['product_id']),
					);
				}
				
				if($product['number_of_sides'] == 2){
					$total_sides = sprintf($this->language->get('total_sides_cat'), $product['number_of_sides']);
				}elseif($product['number_of_sides'] == 1){
					$total_sides = sprintf($this->language->get('total_sides_cat'), $product['number_of_sides']);
				}elseif($product['number_of_sides'] == 0){
					$total_sides = "";
				}
				
				// menny
				/*
				$this->load->model('catalog/availability');
				$avaiabilityDays = $this->model_catalog_availability->getDaysAvailability($product['product_id']);
				
				ksort($avaiabilityDays);
				
				$avaiabilityToday = false;
				$days_text = "זמין בימים: ";
				$i = 0;
				foreach($avaiabilityDays as $day){
					if($i){
						$days_text.= ", ";
					} else {
						$i = 1;
					}
					
					$days_text.= $day;
					
					if($day == strftime("%a",strtotime("today"))) {
						$avaiabilityToday = true;
					}
				}
				*/
				//create the products
				$this->data['seller']['products'][] = array(
					'product_id'  	=> $product['product_id'],
					'description' 	=> strip_tags(html_entity_decode($product['description'], ENT_QUOTES, 'UTF-8')),
					'is_verified' 	=> $product['is_verified'],
					'sides'	      	=> $side_data,
					'pro_cat'	=> $product['pro_cat'],
					'tax' 		=> false,
					'avaiabilityToday' 	=> "", //json_encode($avaiabilityToday),
					'days_available' 	=> "", //$days_text,
					'thumb' 	=> $image,
					'image_big' 	=> $image_big,
					'name' 		=> $product['name'],
					'total_sides' 	=> $total_sides,
					'price' 	=> $price,
					'special' 	=> $special,
					'rating'	=> $rating,
					'reviews'    	=> sprintf($this->language->get('text_reviews'), (int)$product['reviews']),
					'reviews_text' 	=> (int)$product['reviews_text'],
					'href'    	=> $this->url->link('product/product', 'product_id=' . $product['product_id']),	
                    'cat' => $cat,
				);				
			}
		} else {
			$this->data['seller']['products'] = NULL;
		}
		
		$this->document->setTitle($customer['firstname']);
		
		$this->data['breadcrumbs'] = $this->MsLoader->MsHelper->setBreadcrumbs(array(
			array(
				'text' => $this->language->get('ms_catalog_sellers'),
				'href' => $this->url->link('seller/catalog-seller', '', 'SSL'), 
			),
			array(
				'text' => sprintf($this->language->get('ms_catalog_seller_profile_breadcrumbs'), $this->data['seller']['nickname']),
				'href' => $this->url->link('seller/catalog-seller/profile', '&seller_id='.$seller['seller_id'], 'SSL'),
			)
		));
		
		list($this->template, $this->children) = $this->MsLoader->MsHelper->loadTemplate('catalog-seller-profile');
		$this->response->setOutput($this->render());
  	}
  	
	public function products() {
		if (isset($this->request->get['seller_id'])) {
			$seller = $this->MsLoader->MsSeller->getSeller($this->request->get['seller_id']);
		}
			
		if (!isset($seller) || empty($seller) || $seller['ms.seller_status'] != MsSeller::STATUS_ACTIVE) {
			$this->redirect($this->url->link('seller/catalog-seller', '', 'SSL'));
			return;
		}

		$this->document->addScript('catalog/view/javascript/jquery/jquery.total-storage.min.js');
		
		/* seller info part */	
		if ($seller['ms.avatar'] && file_exists(DIR_IMAGE . $seller['ms.avatar'])) {
			$image = $this->MsLoader->MsFile->resizeImage($seller['ms.avatar'], $this->config->get('msconf_product_seller_products_image_width'), $this->config->get('msconf_product_seller_products_image_height'));
		} else {
			$image = $this->MsLoader->MsFile->resizeImage('ms_no_image.jpg', $this->config->get('msconf_product_seller_products_image_width'), $this->config->get('msconf_product_seller_products_image_height'));
		}
		
		$this->data['seller']['nickname'] = $seller['ms.nickname'];
		$this->data['seller']['description'] = $seller['ms.description'];
		$this->data['seller']['thumb'] = $image;
		$this->data['seller']['href'] = $this->url->link('seller/catalog-seller/profile', 'seller_id=' . $seller['seller_id']);
		
		$country = $this->model_localisation_country->getCountry($seller['ms.country_id']);
		
		if (!empty($country)) {			
			$this->data['seller']['country'] = $country['name'];
		} else {
			$this->data['seller']['country'] = NULL;
		}
		
		if (!empty($seller['ms.company'])) {
			$this->data['seller']['company'] = $seller['ms.company'];
		} else {
			$this->data['seller']['company'] = NULL;
		}
		
		if (!empty($seller['ms.website'])) {
			$this->data['seller']['website'] = $seller['ms.website'];
		} else {
			$this->data['seller']['website'] = NULL;
		}
		
		// badges
		$badges = array_unique(array_merge(
			$this->MsLoader->MsBadge->getSellerGroupBadges(array('seller_id' => $seller['seller_id'], 'language_id' => $this->config->get('config_language_id'))),
			$this->MsLoader->MsBadge->getSellerGroupBadges(array('seller_group_id' => $seller['ms.seller_group'], 'language_id' => $this->config->get('config_language_id'))),
			$this->MsLoader->MsBadge->getSellerGroupBadges(array('seller_group_id' => $this->config->get('msconf_default_seller_group_id'), 'language_id' => $this->config->get('config_language_id')))
		), SORT_REGULAR);		
		
		$this->data['seller']['total_sales'] = $this->MsLoader->MsSeller->getSalesForSeller($seller['seller_id']);
		$this->data['seller']['total_products'] = $this->MsLoader->MsProduct->getTotalProducts(array(
			'seller_id' => $seller['seller_id'],
			'product_status' => array(MsProduct::STATUS_ACTIVE)
		));

		foreach ($badges as &$badge) {
			$badge['image'] = $this->model_tool_image->resize($badge['image'], $this->config->get('msconf_badge_width'), $this->config->get('msconf_badge_height'));
		}
		$this->data['seller']['badges'] = $badges;

		/* seller products part */
		$this->data['text_display'] = $this->language->get('text_display');
		$this->data['text_list'] = $this->language->get('text_list');
		$this->data['text_grid'] = $this->language->get('text_grid');
		$this->data['text_sort'] = $this->language->get('text_sort');
		$this->data['text_limit'] = $this->language->get('text_limit');
		
		$available_sorts = array('pd.name-ASC', 'pd.name-DESC', 'ms.country_id-ASC', 'ms.country_id-DESC', 'pd.name', 'ms.country_id');
		if (isset($this->request->get['sort'])) {
			$order_by = $this->request->get['sort'];
			if (!in_array($order_by, $available_sorts)) {
				$order_by = 'pd.name';
			}
		} else {
			$order_by = 'pd.name';
		}

		if (isset($this->request->get['order'])) {
			$order_way = $this->request->get['order'];
		} else {
			$order_way = 'ASC';
		}
		
		$page = isset($this->request->get['page']) ? $this->request->get['page'] : 1;	
							
		if (isset($this->request->get['limit'])) {
			$limit = $this->request->get['limit'];
		} else {
			$limit = $this->config->get('config_catalog_limit');
		}
		
		$this->data['products'] = array();
		
		$sort = array(
			//'filter_category_id' => $category_id, 
			'order_by'               => $order_by,
			'order_way'              => $order_way,
			'offset'              => ($page - 1) * $limit,
			'limit'              => $limit
		);
		
		$total_products = $this->MsLoader->MsProduct->getTotalProducts(array(
			'seller_id' => $seller['seller_id'],
			'product_status' => array(MsProduct::STATUS_ACTIVE)
		));
		
		$products = $this->MsLoader->MsProduct->getProducts(
			array(
				'seller_id' => $seller['seller_id'],
				'product_status' => array(MsProduct::STATUS_ACTIVE)
			),
			$sort
		);
		
		if (!empty($products)) {
			foreach ($products as $product) {
				$product_data = $this->model_catalog_product->getProduct($product['product_id']);
				if ($product_data['image'] && file_exists(DIR_IMAGE . $product_data['image'])) {
					$image = $this->model_tool_image->resize($product_data['image'], 200, 200);
					//$image = $this->MsLoader->MsFile->resizeImage($product_data['image'], $this->config->get('msconf_product_seller_products_image_width'), $this->config->get('msconf_product_seller_products_image_height'));
				} else {
					$image = $this->MsLoader->MsFile->resizeImage('no_image.jpg', $this->config->get('msconf_product_seller_products_image_width'), $this->config->get('msconf_product_seller_products_image_height'));
				}

				if (($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) {
					$price = $this->currency->format($this->tax->calculate($product_data['price'], $product_data['tax_class_id'], $this->config->get('config_tax')));
				} else {
					$price = false;
				}
						
				if ((float)$product_data['special']) {
					$special = $this->currency->format($this->tax->calculate($product_data['special'], $product_data['tax_class_id'], $this->config->get('config_tax')));
				} else {
					$special = false;
				}
				
				if ($this->config->get('config_review_status')) {
					$rating = $product_data['rating'];
				} else {
					$rating = false;
				}
				
				if ($this->config->get('config_tax')) {
					$tax = $this->currency->format((float)$product_data['special'] ? $product_data['special'] : $product_data['price']);
				} else {
					$tax = false;
				}
				
				
				
				$this->data['seller']['products'][] = array(
					'product_id' => $product['product_id'],
					'pro_cat' => $product['pro_cat'],
					'tax' => $product['tax'],
					'thumb' => $image,
					'name' => $product_data['name'],
					'price' => $price,
					'tax' => $tax,
					'special' => $special,
					'rating' => $rating,
					'description' => utf8_substr(strip_tags(html_entity_decode($product_data['description'], ENT_QUOTES, 'UTF-8')), 0, 100) . '..',					
					'reviews'    => sprintf($this->language->get('text_reviews'), (int)$product_data['reviews']),
					'href'    => $this->url->link('product/product', 'product_id=' . $product_data['product_id']),						
				);				
			}
		} else {
			$this->data['seller']['products'] = NULL;
		}
		
		
		$url = '';

		if (isset($this->request->get['limit'])) {
			$url .= '&limit=' . $this->request->get['limit'];
		}
						
		$this->data['sorts'] = array();
		
		$this->data['sorts'][] = array(
			'text'  => $this->language->get('ms_sort_nickname_asc'),
			'value' => 'pd.name-ASC',
			'href'  => $this->url->link('seller/catalog-seller/products', '&sort=pd.name&order=ASC&seller_id=' . $seller['seller_id'] . $url)
		);

		$this->data['sorts'][] = array(
			'text'  => $this->language->get('ms_sort_nickname_desc'),
			'value' => 'pd.name-DESC',
			'href'  => $this->url->link('seller/catalog-seller/products', '&sort=pd.name&order=DESC&seller_id=' . $seller['seller_id'] . $url)
		);

		/*
		$this->data['sorts'][] = array(
			'text'  => $this->language->get('ms_sort_country_asc'),
			'value' => 'ms.country_id-ASC',
			'href'  => $this->url->link('seller/catalog-seller/products', '&sort=ms.country_id&order=ASC' . $url)
		); 

		$this->data['sorts'][] = array(
			'text'  => $this->language->get('ms_sort_country_desc'),
			'value' => 'ms.country_id-DESC',
			'href'  => $this->url->link('seller/catalog-seller/products', '&sort=ms.country_id&order=DESC' . $url)
		); 
		*/
		$url = '';

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}	

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}
		
		
		$this->data['limits'] = array();
		
		$this->data['limits'][] = array(
			'text'  => $this->config->get('config_catalog_limit'),
			'value' => $this->config->get('config_catalog_limit'),
			'href'  => $this->url->link('seller/catalog-seller/products', $url . '&limit=' . $this->config->get('config_catalog_limit') . '&seller_id=' . $seller['seller_id'])
		);
					
		$this->data['limits'][] = array(
			'text'  => 25,
			'value' => 25,
			'href'  => $this->url->link('seller/catalog-seller/products', $url . '&limit=25&seller_id=' . $seller['seller_id'])
		);
		
		$this->data['limits'][] = array(
			'text'  => 50,
			'value' => 50,
			'href'  => $this->url->link('seller/catalog-seller/products', $url . '&limit=50&seller_id=' . $seller['seller_id'])
		);

		$this->data['limits'][] = array(
			'text'  => 75,
			'value' => 75,
			'href'  => $this->url->link('seller/catalog-seller/products', $url . '&limit=75&seller_id=' . $seller['seller_id'])
		);
		
		$this->data['limits'][] = array(
			'text'  => 100,
			'value' => 100,
			'href'  => $this->url->link('seller/catalog-seller/products', $url . '&limit=100&seller_id=' . $seller['seller_id'])
		);
		
		$url = '';

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}	

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['limit'])) {
			$url .= '&limit=' . $this->request->get['limit'];
		}
		
		$pagination = new Pagination();
		$pagination->total = $total_products;
		$pagination->page = $page;
		$pagination->limit = $limit;
		$pagination->text = $this->language->get('text_pagination');
		$pagination->url = $this->url->link('seller/catalog-seller/products', $url . '&page={page}&seller_id=' . $seller['seller_id']);
	
		$this->data['pagination'] = $pagination->render();
		
		$this->data['sort'] = $order_by;
		$this->data['order'] = $order_way;
		$this->data['limit'] = $limit;		
		
		$this->data['ms_catalog_seller_products'] = sprintf($this->language->get('ms_catalog_seller_products_heading'), $seller['ms.nickname']);
		$this->document->setTitle(sprintf($this->language->get('ms_catalog_seller_products_heading'), $seller['ms.nickname']));
		
		$this->data['breadcrumbs'] = $this->MsLoader->MsHelper->setBreadcrumbs(array(
			array(
				'text' => $this->language->get('ms_catalog_sellers'),
				'href' => $this->url->link('seller/catalog-seller', '', 'SSL'), 
			),
			array(
				'text' => sprintf($this->language->get('ms_catalog_seller_products_breadcrumbs'), $seller['ms.nickname']),
				'href' => $this->url->link('seller/catalog-seller/profile', '&seller_id='.$seller['seller_id'], 'SSL'),
			)
		));
		
		list($this->template, $this->children) = $this->MsLoader->MsHelper->loadTemplate('catalog-seller-products');
		$this->response->setOutput($this->render());
	}

	public function jxSubmitContactDialog() {
		if ($this->config->get('msconf_enable_private_messaging') == 2) {
			return $this->_submitEmailDialog();
		} else if ($this->config->get('msconf_enable_private_messaging') == 1 && $this->customer->getId()) {
			return $this->_submitPmDialog();
		} else {
			return false;
		}
	}
	
	private function _submitEmailDialog() {
		$seller_id = $this->request->post['seller_id'];
		$product_id = $this->request->post['product_id'];
		$seller_email = $this->MsLoader->MsSeller->getSellerEmail($seller_id);
		$seller_name = $this->MsLoader->MsSeller->getSellerName($seller_id);
		$message_text = trim($this->request->post['ms-sellercontact-text']);
		$customer_name = mb_substr(trim($this->request->post['ms-sellercontact-name']), 0, 50);
		$customer_email = $this->request->post['ms-sellercontact-email'];

		$json = array();

		if (empty($message_text) || empty($customer_name) || empty($customer_email) || empty($this->request->post['ms-sellercontact-captcha'])) {
			$json['errors'][] = $this->language->get('ms_error_contact_allfields');
			$this->response->setOutput(json_encode($json));
			return;
		}

		if (!isset($this->session->data['captcha']) || ($this->session->data['captcha'] != $this->request->post['ms-sellercontact-captcha'])) {
			$json['errors'][] = $this->language->get('ms_error_contact_captcha');
		}

		if (!filter_var($customer_email, FILTER_VALIDATE_EMAIL)) {
			$json['errors'][] = $this->language->get('ms_error_contact_email');			
		}

		if (mb_strlen($message_text) > 2000) {
			$json['errors'][] = $this->language->get('ms_error_contact_text');
		}

		if (!isset($json['errors'])) {	
			$mails[] = array(
				'type' => MsMail::SMT_SELLER_CONTACT,
				'data' => array(
					'recipients' => $seller_email,
					'customer_name' => $customer_name,
					'customer_email' => $customer_email,
					'customer_message' => $message_text,
					'product_id' => $product_id,
					'addressee' => $seller_name
				)
			);			
			$this->MsLoader->MsMail->sendMails($mails);
			$json['success'] = $this->language->get('ms_sellercontact_success');
		}
		$this->response->setOutput(json_encode($json));
	}

	private function _submitPmDialog() {
		$seller_id = $this->request->post['seller_id'];
		$product_id = $this->request->post['product_id'];
		$seller_name = $this->MsLoader->MsSeller->getSellerName($seller_id);
		$message_text = trim($this->request->post['ms-sellercontact-text']);
		$seller_email = $this->MsLoader->MsSeller->getSellerEmail($seller_id);
		$customer_name = $this->customer->getFirstname() . ' ' . $this->customer->getLastname();
		
		$json = array();
		
		if (empty($message_text)) {
			$json['errors'][] = $this->language->get('ms_error_contact_allfields');
			$this->response->setOutput(json_encode($json));
			return;
		}
	
		if (mb_strlen($message_text) > 2000) {
			$json['errors'][] = $this->language->get('ms_error_contact_text');
		}
		
		$product = $this->MsLoader->MsProduct->getProduct($product_id);
		$product_name = $product['languages'][$this->MsLoader->MsHelper->getLanguageId($this->config->get('config_language'))]['name'];
		
		$title = $product_id ? sprintf($this->language->get('ms_conversation_title_product'), $product_name) : sprintf($this->language->get('ms_conversation_title'), $customer_name);
		
		if (!isset($json['errors'])) {
			
			$conversation = $this->MsLoader->MsConversation->getConversations(
				array(
					'participant_id' => $seller_id,
					'other_participant_id' => $this->customer->getId(),
					'single' => true
				)
			);
			
			if(empty($conversation)){
				$conversation_id = $this->MsLoader->MsConversation->createConversation(
					array(
						'product_id' => $product_id,
						'title' => $title,
					)
				);
			} else {
				$conversation_id = $conversation['conversation_id'];
			}
			
			
			$this->MsLoader->MsMessage->createMessage(
				array(
					'conversation_id' => $conversation_id,
					'from' => $this->customer->getId(),
					'to' => $seller_id,
					'message' => $message_text
				)
			);
			
			//$mails[] = array(
			//	'type' => MsMail::SMT_PRIVATE_MESSAGE,
			//	'data' => array(
			//		'recipients' => $seller_email,
			//		'customer_name' => $customer_name,
			//		'customer_message' => $message_text,
			//		'title' => $title,
			//		'product_id' => $product_id,
			//		'addressee' => $seller_name
			//	)
			//);			
			//$this->MsLoader->MsMail->sendMails($mails);
			
			/**********************************************************************************
			 *	homeals mail change
			 **********************************************************************************/
			$subject = sprintf($this->language->get('text_subject_pm'),$customer_name);
			
			$template = new Template();
			
			$template->data['title'] = $subject;
			$template->data['store_name'] = 'Homeals';
			$template->data['store_url'] = $this->config->get('config_url');
			$template->data['conv_url'] = $this->url->link('account/msmessage', '&conversation_id='.$conversation_id, 'SSL');
			$template->data['logo'] = $this->config->get('config_url') . 'image/' . $this->config->get('config_logo');
			
			$template->data['text_greeting'] = sprintf($this->language->get('text_greeting_pm'),$seller_name);
			$template->data['text_pre_msg'] = sprintf($this->language->get('text_pre_msg'),$customer_name);
			
			$template->data['message'] = '"'.mb_truncate($message_text,140).'"';
			$template->data['message'] = $seller_email;
			
			// fotter
			$template->data['text_follow_us'] = $this->language->get('text_follow_us');			
			
			if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/mail/pm.tpl')) {
				$html = $template->fetch($this->config->get('config_template') . '/template/mail/pm.tpl');
			} else {
				$html = $template->fetch('default/template/mail/pm.tpl');
			}
			
			$mail = new Mail(); 
			$mail->protocol = $this->config->get('config_mail_protocol');
			$mail->parameter = $this->config->get('config_mail_parameter');
			$mail->hostname = $this->config->get('config_smtp_host');
			$mail->username = $this->config->get('config_smtp_username');
			$mail->password = $this->config->get('config_smtp_password');
			$mail->port = $this->config->get('config_smtp_port');
			$mail->timeout = $this->config->get('config_smtp_timeout');
			
			$mail->setTo($seller_email);
			$mail->setFrom($this->config->get('config_email'));
			$mail->setSender('Homeals');
			$mail->setSubject(html_entity_decode($subject, ENT_QUOTES, 'UTF-8'));
			$mail->setHtml($html);
			$mail->send();
			
			/**********************************************************************************
			 *	homeals mail end
			 **********************************************************************************/
			
			$json['success'] = $this->language->get('ms_sellercontact_success');
		}
		$this->response->setOutput(json_encode($json));
	}
	
	public function jxRenderContactDialog() {
		if ($this->config->get('msconf_enable_private_messaging') == 2) {
			return $this->_renderEmailDialog();
		} else if ($this->config->get('msconf_enable_private_messaging') == 1 && $this->customer->getId()) {
			return $this->_renderPmDialog(); 
		} else {
			return false;
		}
	}
	
	private function _renderEmailDialog() {
		if (isset($this->request->get['product_id'])) {
			$seller_id = $this->MsLoader->MsProduct->getSellerId($this->request->get['product_id']);
			$this->data['product_id'] = (int)$this->request->get['product_id'];
		} else {
			$seller_id = $this->request->get['seller_id'];
			$this->data['product_id'] = 0;
		}
		$seller = $this->MsLoader->MsSeller->getSeller($seller_id);
		
		if (empty($seller))
			return false;

		$this->data['seller_id'] = $seller_id;
		
		$this->data['customer_email'] = $this->customer->getEmail();
		$this->data['customer_name'] = $this->customer->getFirstname() . ' ' . $this->customer->getLastname();
		
		if (!empty($seller['ms.avatar'])) {
			$this->data['seller_thumb'] = $this->MsLoader->MsFile->resizeImage($seller['ms.avatar'], $this->config->get('msconf_seller_avatar_product_page_image_width'), $this->config->get('msconf_seller_avatar_product_page_image_height'));
		} else {
			$this->data['seller_thumb'] = $this->MsLoader->MsFile->resizeImage('ms_no_image.jpg', $this->config->get('msconf_seller_avatar_product_page_image_width'), $this->config->get('msconf_seller_avatar_product_page_image_height'));
		}
			
		$this->data['seller_href'] = $this->url->link('seller/catalog-seller/profile', 'seller_id=' . $seller['seller_id']);
		$this->data['ms_sellercontact_sendmessage'] = sprintf($this->language->get('ms_sellercontact_sendmessage'), $seller['ms.nickname']);
		
		list($this->template, $this->children) = $this->MsLoader->MsHelper->loadTemplate('dialog-sellercontact');
		return $this->response->setOutput($this->render());
	}

	private function _renderPmDialog() {
		if (isset($this->request->get['product_id'])) {
			$seller_id = $this->MsLoader->MsProduct->getSellerId($this->request->get['product_id']);
			$this->data['product_id'] = (int)$this->request->get['product_id'];
		} else {
			$seller_id = $this->request->get['seller_id'];
			$this->data['product_id'] = 0;
		}
		$seller = $this->MsLoader->MsSeller->getSeller($seller_id);
		
		$seller_name = $this->MsLoader->MsSeller->getSellerName($seller_id);
		$this->data['msg_placeholder'] = $this->language->get('msg_placeholder');
		$this->data['msg_send'] = $this->language->get('msg_send');
		
		$this->data['msg_new_headline'] = sprintf($this->language->get('msg_new_headline'),$seller_name);
		
		if (empty($seller)){
			$this->load->model('account/customer'); // needed for cook
			
			$customer_info =  $this->model_account_customer->getCustomer($seller_id);
			
			$seller_name = $customer_info['firstname'];
			$seller = array(
					'ms.avatar' => '',
					'seller_id' => $seller_id,
					);
			
		}
		
		
		$this->data['seller_id'] = $seller_id;
		
		if (!empty($seller['ms.avatar'])) {
			$this->data['seller_thumb'] = $this->MsLoader->MsFile->resizeImage($seller['ms.avatar'], $this->config->get('msconf_seller_avatar_product_page_image_width'), $this->config->get('msconf_seller_avatar_product_page_image_height'));
		} else {
			$this->data['seller_thumb'] = $this->MsLoader->MsFile->resizeImage('ms_no_image.jpg', $this->config->get('msconf_seller_avatar_product_page_image_width'), $this->config->get('msconf_seller_avatar_product_page_image_height'));
		}
			
		$this->data['seller_href'] = $this->url->link('seller/catalog-seller/profile', 'seller_id=' . $seller['seller_id']);
		
		$this->data['ms_sellercontact_sendmessage'] = sprintf($this->language->get('ms_sellercontact_sendmessage'), $seller_name);
		
		list($this->template, $this->children) = $this->MsLoader->MsHelper->loadTemplate('dialog-sellercontact');
		return $this->response->setOutput($this->render());
	}
	
	public function jxSubmitRateDialog() {
		$data = $this->request->post;
		
		$json = array();
		
		foreach ($data['seller_id'] as $seller_id) {
			if (mb_strlen($data['ms-seller-rate-text'][$seller_id]) > 300) {
				$json['errors'][] = $this->language->get('ms_error_rate_comment_length');
			} else if (!isset($data['ms-seller-rate-text'][$seller_id]) || $data['ms-seller-rate-text'][$seller_id] == "") {
				$json['errors'][] = $this->language->get('ms_error_rate_no_comment');
			} else if ( !isset($data['rating_overall'][$seller_id]) || $data['rating_overall'][$seller_id] == "" || !isset($data['rating_communication'][$seller_id]) || $data['rating_communication'][$seller_id] == "" || !isset($data['rating_honesty'][$seller_id]) || $data['rating_honesty'][$seller_id] == "" ) {
				$json['errors'][] = $this->language->get('ms_error_rate_no_rating');
			}
		}
		
		if (!isset($json['errors'])) {
			foreach ($data['seller_id'] as $seller_id) {
				$seller_rating = array(
					'order_id' => $data['order_id'],
					'evaluator_id' => $this->customer->getId(),
					'rated_user_id' => $seller_id,
					'comment' => $data['ms-seller-rate-text'][$seller_id],
					'rating_overall' => $data['rating_overall'][$seller_id],
					'rating_communication' => $data['rating_communication'][$seller_id],
					'rating_honesty' => $data['rating_honesty'][$seller_id]
				);
				
				$this->MsLoader->MsSeller->rate($seller_rating);
			}
			
			$json['success'] = $this->language->get('ms_seller_rate_success');
			$json['redirect'] = $this->url->link('account/order');
		}
		$this->response->setOutput(json_encode($json));
	}
	
	public function ajaxDeliverySave() {
		
		if($_SERVER['REQUEST_METHOD'] == 'POST'){
			
			$data = $this->request->post;
			
			$date = date("Y-m-d",strtotime($data['date']));
			$deliver_map = htmlspecialchars_decode($data['deliver_map']);
			
			$json['data'] = array();
			
			$request_sql = "INSERT INTO hm_deliver (deliver_date, deliver_map) VALUES ('".$this->db->escape($date)."', '".$this->db->escape($deliver_map)."') ON DUPLICATE KEY UPDATE deliver_map = VALUES(deliver_map)";
				
			$json['data'] = $this->db->query($request_sql);
			
			$this->response->setOutput(json_encode($json));
		} else {
			$this->response->setOutput(json_encode("error : no data was sent"));
		}
	}
	
	public function ajaxDeliveryLoad() {
		
		if($_SERVER['REQUEST_METHOD'] == 'POST'){
			$data = $this->request->post;
			
			$date = date("Y-m-d",strtotime($data['date']));
			
			$json['deliver_map'] = array();
			
			$request_sql = "SELECT * FROM hm_deliver WHERE deliver_date =  '". $this->db->escape($date) ."'";
			
			$request = $this->db->query($request_sql);
			
			if($request->num_rows){
				$json['deliver_map'] = $request->row['deliver_map'];
			} else {
				$json['error'] = "no data";	
			}
			
			$this->response->setOutput(json_encode($json));
		} else {
			$json['error'] = "no data was sent";
			$this->response->setOutput(json_encode($json));
		}
		
	}
	
	public function ajaxDelivery() {
		$data = $this->request->get;
		
		$date = date("Y-m-d",strtotime($data['date']));
		
		$json['data'] = array();
		
		$order_status = "1,5,7,17,18,19,20,21";
		
		$result = $this->db->query("SELECT * FROM " . DB_PREFIX . "order WHERE order_status_id IN (". $order_status .") AND delivery_date = '".$this->db->escape($date)."'");
		
		foreach ($result->rows as $order_info){
			$order_history_query =  $this->db->query("SELECT * FROM oc_order_history h LEFT JOIN oc_order_status s ON s.order_status_id = h.order_status_id WHERE h.order_id = ".(int)$order_info['order_id']." AND s.language_id = 2");
			$order_history_info = $order_history_query->rows;
			
			
			if($order_info['parent_order_id']){
				$num_of_meals_query = $this->db->query("SELECT SUM(quantity) AS meal_num FROM " . DB_PREFIX . "order_product WHERE order_id = ".(int)$order_info['parent_order_id']." AND cook_id = ".(int)$order_info['cook_id']."");
				$num_of_meals = $num_of_meals_query->row;
			} else {
				$num_of_meals_query = $this->db->query("SELECT SUM(quantity) AS meal_num FROM " . DB_PREFIX . "order_product WHERE order_id = ".(int)$order_info['order_id']."");
				$num_of_meals = $num_of_meals_query->row;
			}
			
			$order_history = array();
			
			foreach ($order_history_info as $history_info){
				$order_history[] = array( "name" => $history_info['name'],
										 "date" => date("H:i:s",strtotime($history_info['date_added']))
										);
			}
			
			$this->load->model('account/address');
			$this->load->model('account/customer'); // needed for cook
			
			$cook_info = $this->model_account_customer->getCustomer($order_info['cook_id']);
			
			$cook_addresses = $this->model_account_address->getAddressesById($order_info['cook_id']);
			$cook_address = reset($cook_addresses);
			$cook_city = $cook_address['city'];
			
			$order_time = date("H:i",strtotime($order_info['delivery_time']." -1 hour"))." - ".date("H:i",strtotime($order_info['delivery_time'])) ;
			$cook_order_time = date("H:i",strtotime($order_info['delivery_time']." -2 hour"))." - ".date("H:i",strtotime($order_info['delivery_time']."-1 hour")) ;
			
			$json['data'][] = array(
				"DT_RowId" => $order_info['order_id'],
				"order_id" => $order_info['order_id'],
				"cook_address_1" => $cook_address['address_1'],
				"cook_address_2" => $cook_address['address_2'],
				"cook_company" => $cook_address['company'],
				"cook_name" => $cook_address['firstname']." ".$cook_address['lastname'],
				"cook_phone" => $cook_info['telephone'],
				"cook_comment" => $cook_address['address_comment'],
				"customer_name" => $order_info['firstname']." ".$order_info['lastname'],
				"customer_contact" => $order_info['payment_firstname']." ".$order_info['payment_lastname'],
				"customer_address_1" => $order_info['payment_address_1'],
				"customer_address_2" => $order_info['payment_address_2'],
				"customer_company" => $order_info['payment_company'],
				"customer_phone" => $order_info['payment_postcode'],
				"order_comment" => $order_info['comment'],
				"order_time" => $order_time,
				"cook_order_time" => $cook_order_time,
				"order_date" => $order_info['delivery_date'],
				"status" => $order_info['order_status_id'],
				"status_text" => $order_info['order_status_id'],
				"date_modified" => date("H:i",strtotime($order_info['date_modified'])),
				"driver_pickup" => $order_info['driver_pickup'],
				"driver_deliver" => $order_info['driver_deliver'],
				"number_of_meals" => $num_of_meals['meal_num'],
				"history" => $order_history
				);
			
		}
		
		$this->response->setOutput(json_encode($json));
	}
	
	
	public function updateDeliverStatus() {
		if(isset($this->request->post['order_id']) && isset($this->request->post['status_id'])){
			
			$order_id = $this->request->post['order_id'];
			$status_id = $this->request->post['status_id'];
			
			$this->load->model('checkout/order');
			
			$this->model_checkout_order->update($order_id, $status_id, 'deliver page update', false);
			
			$order_info = $this->model_checkout_order->getOrder($order_id);
			
			$json = "";
			
			if($order_info['order_status_id'] == $status_id) {
				$json = "success";
				$this->response->setOutput(json_encode($json));
			} else {
				$json = "error";
				$this->response->setOutput(json_encode($json));
			}
		}
		
		if(isset($this->request->post['order_id']) && isset($this->request->post['driver']) && isset($this->request->post['type'])){
			
			$order_id = $this->request->post['order_id'];
			$driver = $this->request->post['driver'];
			$type = $this->request->post['type'];
			
			$this->load->model('checkout/order');
			
			if($type){
				$driver_type = 'driver_pickup';
			} else {
				$driver_type = 'driver_deliver';
			}
			
			$this->db->query("UPDATE `" . DB_PREFIX . "order` SET ".$driver_type." = '" . $this->db->escape($driver) . "' WHERE order_id = '" . (int)$order_id . "'");
			
			$order_info = $this->model_checkout_order->getOrder($order_id);
			
			$json = "";
			
			if($order_info['driver'] == $this->db->escape($driver)) {
				$json = "success";
				$this->response->setOutput(json_encode($json));
			} else {
				$json = "error";
				$this->response->setOutput(json_encode($json));
			}
		}
		
		
	}
	
	// approving order from sms HOMEALS
	
	public function approveOrderSms(){
		
		if(isset($this->request->get['oid']) && isset($this->request->get['from'])){
			$order_id = $this->request->get['oid'];
			$cook_telephone = $this->request->get['from'];
			
			$this->load->model('checkout/order');
			
			$order_info = $this->model_checkout_order->getOrder($order_id);
			
			$this->load->model('account/customer'); // needed for cook
			
			if($order_info['cook_id']){
				$cook_info = $this->model_account_customer->getCustomer($order_info['cook_id']);
			} else {
				return false;
			}
			
			if($cook_info['telephone'] == $cook_telephone){
				
				$comment = "SMS approved";
			
				if($order_info['order_status_id'] == 1){
					
					$this->model_checkout_order->update($order_id, 5 , $comment );
					
					return true;
					
				} else {
					return false;
				}
				
				list($this->template, $this->children) = $this->MsLoader->MsHelper->loadTemplate('order-approved');
				$this->response->setOutput($this->render());
				
			} else {
				return false;
			}
			
		} else {
			return false;
		}
		
	}
}
?>
