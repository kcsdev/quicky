<?php

class ControllerSellerAccountOrder extends ControllerSellerAccount {
	public function getTableData() {
		$colMap = array(
			'customer_name' => 'firstname',
			'date_created' => 'o.date_added',
		);
		
		$sorts = array('order_id', 'customer_name', 'date_created', 'total_amount');
		$filters = array_merge($sorts, array('products'));
		
		list($sortCol, $sortDir) = $this->MsLoader->MsHelper->getSortParams($sorts, $colMap);
		$filterParams = $this->MsLoader->MsHelper->getFilterParams($filters, $colMap);
		
		$seller_id = $this->customer->getId();
		$orders = $this->MsLoader->MsOrderData->getOrders(
			array(
				'seller_id' => $seller_id,
			),
			array(
				'order_by'  => $sortCol,
				'order_way' => $sortDir,
				'offset' => $this->request->get['iDisplayStart'],
				'limit' => $this->request->get['iDisplayLength'],
				'filters' => $filterParams
			),
			array(
				'total_amount' => 1,
				'products' => 1,
			)
		);
		
		$total = isset($orders[0]) ? $orders[0]['total_rows'] : 0;

		$columns = array();
		foreach ($orders as $order) {
			$order_products = $this->MsLoader->MsOrderData->getOrderProducts(array('order_id' => $order['order_id'], 'seller_id' => $seller_id));
			
			if ($this->config->get('msconf_hide_customer_email')) {
				$customer_name = "{$order['firstname']} {$order['lastname']}";
			} else {
				$customer_name = "{$order['firstname']} {$order['lastname']} ({$order['email']})";
			}
			
			$products = "";
			foreach ($order_products as $p) {
				$products .= "<p>";
					$products .= "<span class='name'>" . ($p['quantity'] > 1 ? "{$p['quantity']} x " : "") . "<a href='" . $this->url->link('product/product', 'product_id=' . $p['product_id'], 'SSL') . "'>{$p['name']}</a></span>";
					$products .= "<span class='total'>" . $this->currency->format($p['seller_net_amt'], $this->config->get('config_currency')) . "</span>";
				$products .= "</p>";
			}
			
			$columns[] = array_merge(
				$order,
				array(
					'order_id' => $order['order_id'],
					'customer_name' => $customer_name,
					'products' => $products,
					'date_created' => date($this->language->get('date_format_short'), strtotime($order['date_added'])),
					'total_amount' => $this->currency->format($order['total_amount'], $this->config->get('config_currency'))
				)
			);
		}
		
		$this->response->setOutput(json_encode(array(
			'iTotalRecords' => $total,
			'iTotalDisplayRecords' => $total,
			'aaData' => $columns
		)));
	}
	
	// approving order from mail HOMEALS
	
	public function approveOrder(){
		
		$this->session->data['redirect'] = "index.php?route=seller/account-order/approveOrder&oid=".$this->request->get['oid'];
		
		if(isset($this->request->get['oid'])){
			$order_id = $this->request->get['oid'];
		} else {
			$this->redirect($this->url->link('product/category', '&path=0', 'SSL'));
		}
		
		$cook_id = $this->customer->getId();
		
		// dirty hack
		if($cook_id == 229){
			$result = $this->db->query("SELECT * FROM `" . DB_PREFIX . "order` WHERE order_id = '" . (int)$order_id . "'");
		} else {
			$result = $this->db->query("SELECT * FROM `" . DB_PREFIX . "order` WHERE order_id = '" . (int)$order_id . "' AND cook_id = '". (int)$cook_id ."'");
		}
		
		
		if ($result->num_rows == 1){
			
			$this->load->model('checkout/order');
			
			if($cook_id == 229){
				$comment = "Order@homeals Approved";
			} else {
				$comment = "Site Approved";
			}
			
			if($result->row['order_status_id'] == 1){
				$this->model_checkout_order->update($order_id, 5 , $comment );
				
				$this->data['approved_text'] = $this->language->get('text_approved');
				$this->data['approved_text_more'] = sprintf($this->language->get('text_approved_more'),$order_id,$result->row['firstname'],$result->row['lastname'],$result->row['delivery_time']);
				
			} else {
				
				$this->data['approved_text'] = sprintf($this->language->get('text_not_pendding'),$order_id);
				$this->data['approved_text_more'] = "";
				
			}
			
			list($this->template, $this->children) = $this->MsLoader->MsHelper->loadTemplate('order-approved');
			$this->response->setOutput($this->render());
			
		} else {
			
			if(!$this->customer->isLogged()) {
				$this->data['approved_text'] = "יש להתחבר כדי לאשר הזמנה";
				$this->data['approved_text_more'] = "";
			} else {
				$this->data['approved_text'] = "לא קיימת הזמנה";
				$this->data['approved_text_more'] = "בדוק שהינך מחובר כבשלן שצריך לאשר את ההזמנה";
			}
				
			list($this->template, $this->children) = $this->MsLoader->MsHelper->loadTemplate('order-approved');
			$this->response->setOutput($this->render());
		}
	}
	
	public function index() {
		$this->data['link_back'] = $this->url->link('account/account', '', 'SSL');
		
		$this->document->setTitle($this->language->get('ms_account_orders_heading'));
		
		$this->data['breadcrumbs'] = $this->MsLoader->MsHelper->setBreadcrumbs(array(
			array(
				'text' => $this->language->get('text_account'),
				'href' => $this->url->link('account/account', '', 'SSL'),
			),
			array(
				'text' => $this->language->get('ms_account_dashboard_breadcrumbs'),
				'href' => $this->url->link('seller/account-dashboard', '', 'SSL'),
			),			
			array(
				'text' => $this->language->get('ms_account_orders_breadcrumbs'),
				'href' => $this->url->link('seller/account-order', '', 'SSL'),
			)
		));
		
		list($this->template, $this->children) = $this->MsLoader->MsHelper->loadTemplate('account-order');
		$this->response->setOutput($this->render());
	}
}

?>
