<?php

class ControllerSellerAccountMyOrders extends ControllerSellerAccount {
	
	public function getMyOrders($data = array()) {
		$this->language->load('account/myorders');
		
		$data_get = $this->request->get;
		
		$date = isset($data['stat_date']) ? date("Y-m-d",strtotime($data['stat_date'])) : date("Y-m-d",strtotime("yesterday"));
		
		$last_date = isset($data['last_date']) ? date("Y-m-d",strtotime($data['last_date'])) : date("Y-m-d",strtotime("today +30 days"));
		$order_status = isset($data['order_status']) ? $data['order_status'] : "1";
		
		$order = isset($data['order']) ? $data['order'] : "ASC";
		
		$seller_id = $this->customer->getId();
		
		$json['data'] = array();
		
		if($seller_id == 229){
			
			
			$ckid = isset($this->request->get['ckid']) ? $this->request->get['ckid'] : 0;
			
			if($ckid){
				$and_cook_id = " AND cook_id = '". (int)$ckid ."'";
			} else {
				$and_cook_id = "";
			}
			
			
			$result = $this->db->query("SELECT * FROM " . DB_PREFIX . "order WHERE delivery_date >= '".$this->db->escape($date)."' AND delivery_date <= '". $this->db->escape($last_date) ."' AND order_status_id IN (". $order_status .") ".$and_cook_id." ORDER BY delivery_date ". $this->db->escape($order) ." , delivery_time");
			
			
			
		} else {
			$result = $this->db->query("SELECT * FROM " . DB_PREFIX . "order WHERE delivery_date >= '".$this->db->escape($date)."' AND delivery_date <= '". $this->db->escape($last_date) ."' AND order_status_id IN (". $order_status .") AND cook_id = '". (int)$seller_id ."' ORDER BY delivery_date ". $this->db->escape($order) ." , delivery_time");	
		}
		
		
		foreach ($result->rows as $order_info){
			
			if($order_info['parent_order_id']){
				$order_history_query =  $this->db->query("SELECT * FROM oc_order_history h LEFT JOIN oc_order_status s ON s.order_status_id = h.order_status_id WHERE h.order_id = ".(int)$order_info['order_id']." AND s.language_id = 2");
				$order_history_info = $order_history_query->rows;
				
				$num_of_meals_query = $this->db->query("SELECT SUM(quantity) AS meal_num FROM " . DB_PREFIX . "order_product WHERE order_id = ".(int)$order_info['parent_order_id']." AND cook_id = ".(int)$order_info['cook_id']."");
				$num_of_meals = $num_of_meals_query->row;
				
				$meals_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "order_product WHERE order_id = ".(int)$order_info['parent_order_id']." AND cook_id = ".(int)$order_info['cook_id']."");
				$meals_info_rows = $meals_query->rows;	
			} else {
				$order_history_query =  $this->db->query("SELECT * FROM oc_order_history h LEFT JOIN oc_order_status s ON s.order_status_id = h.order_status_id WHERE h.order_id = ".(int)$order_info['order_id']." AND s.language_id = 2");
				$order_history_info = $order_history_query->rows;
				
				$num_of_meals_query = $this->db->query("SELECT SUM(quantity) AS meal_num FROM " . DB_PREFIX . "order_product WHERE order_id = ".(int)$order_info['order_id']."");
				$num_of_meals = $num_of_meals_query->row;
				
				$meals_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "order_product WHERE order_id = ".(int)$order_info['order_id']."");
				$meals_info_rows = $meals_query->rows;	
			}
			
			$meals_info = array();
			$total_profit = 0;
			
			foreach ($meals_info_rows as $meals_info_row){
				
				$sides = explode(',', $meals_info_row['sides']);
				
				$sides_info = array();
				
				if(!(empty($sides[0]))) {
					foreach ($sides as $side){
						$side_result_query = $this->db->query("SELECT name , image FROM " . DB_PREFIX . "product p LEFT JOIN " . DB_PREFIX . "product_description  pd ON p.product_id = pd.product_id  WHERE p.product_id = ".(int)$side." AND language_id = ".(int)$this->config->get('config_language_id')."");
						
						$side_result = $side_result_query->row;
						
						$sides_info[] = array(
								'name' => $side_result['name'],
								'image' => $side_result['image']
								);
								
					}
				}
				
				$total_profit += $meals_info_row['total'];
				
				$meals_info[] = array(
						'product_id' => $meals_info_row['product_id'],
						'profit' => $meals_info_row['total'],
						'name' => mb_truncate($meals_info_row['name'],40),
						'full_name' => $meals_info_row['name'],
						'comment' => $meals_info_row['comment'],
						'quantity' => $meals_info_row['quantity'],
						'sides' => empty($sides_info) ? "" : $sides_info
						);
					
			}
			
			$order_history = array();
			
			foreach ($order_history_info as $history_info){
				$order_history_name = $history_info['name'];
			}
			
			$this->load->model('account/address');
			$this->load->model('account/customer');
			$this->load->model('tool/image');
			
			$customer_info = $this->model_account_customer->getCustomer($order_info['customer_id']);
			
			if(!empty($customer_info['image'])){
				if (strpos($customer_info['image'], 'http') === 0) {
					$author_pic = $customer_info['pic_square'];
				}else{
					$author_pic = $this->model_tool_image->resize($customer_info['image'], 40, 40);
				}
			} else {
				$author_pic = $this->model_tool_image->resize("no-avatar-women.png", 40, 40);;
			}
			
			$cook_addresses = $this->model_account_address->getAddressesById($order_info['cook_id']);
			$cook_address = reset($cook_addresses);
			$cook_city = $cook_address['city'];
			
			$order_time = date("H:i",strtotime($order_info['delivery_time']." -1 hour"))." - ".date("H:i",strtotime($order_info['delivery_time'])) ;
			$cook_order_time = date("H:i",strtotime($order_info['delivery_time']." -2 hour"))." - ".date("H:i",strtotime($order_info['delivery_time']."-1 hour")) ;
			$order_date_time = strftime("%A, %e ב%B",strtotime($order_info['delivery_date']));
			
			$json['data'][] = array(
				"DT_RowId" => $order_info['order_id'],
				"order_id" => $order_info['order_id'],
				"cook_address_1" => $cook_address['address_1'],
				"cook_address_2" => $cook_address['address_2'],
				"cook_company" => $cook_address['company'],
				"cook_name" => $cook_address['firstname']." ".$cook_address['lastname'],
				"cook_id" => $order_info['cook_id'],
				"cook_comment" => $cook_address['address_comment'],
				"customer_id" => $order_info['customer_id'],
				"customer_avatar" => $author_pic,
				"customer_name" => $order_info['firstname'],
				"customer_contact" => $order_info['payment_firstname']." ".$order_info['payment_lastname'],
				"customer_address_1" => $order_info['payment_address_1'],
				"customer_address_2" => $order_info['payment_address_2'],
				"customer_company" => $order_info['payment_company'],
				"customer_phone" => $order_info['payment_postcode'],
				"order_comment" => $order_info['comment'],
				"order_details_link" => $this->url->link('account/order/info_cook', '&order_id='.$order_info['order_id'], 'SSL'),
				"order_time" => $order_time,
				"cook_order_time" => $cook_order_time,
				"order_date" => $order_info['delivery_date'],
				"order_date_time" => $order_date_time,
				"status" => $order_info['order_status_id'],
				"status_text" => isset($order_history_name) ? $order_history_name : "",
				"date_modified" => date("H:i",strtotime($order_info['date_modified'])),
				"delivery_man" => $order_info['driver_pickup'],
				"meals" => $meals_info,
				"number_of_meals" => $num_of_meals['meal_num'],
				"aprove_link" => $this->url->link('seller/account-order/approveOrder', '&oid='.$order_info['order_id'], 'SSL'),
				"total_profit" => $this->currency->format($total_profit, $order_info['currency_value']),
				"currency_value" => $order_info['currency_value'],
				"int_total_profit" => $total_profit
				);
		}
		
		return $json['data'];
	}
	
	public function index() {
		$this->language->load('account/myorders');
		
		$this->data['link_back'] = $this->url->link('account/account', '', 'SSL');
		
		$this->document->setTitle($this->language->get('page_title'));
		
		$this->data['text_your_orders'] = $this->language->get('text_your_orders');
		
		$this->data['text_recived_orders'] = $this->language->get('text_recived_orders');
		$this->data['text_history_orders'] = $this->language->get('text_history_orders');
		$this->data['text_wating_orders'] = $this->language->get('text_wating_orders');
		
		$this->data['text_order_details_link'] = $this->language->get('text_order_details_link');
		
		$this->data['cul_num'] = $this->language->get('cul_num');
		$this->data['cul_who'] = $this->language->get('cul_who');
		$this->data['cul_meals'] = $this->language->get('cul_meals');
		$this->data['cul_time'] = $this->language->get('cul_time');
		$this->data['cul_total'] = $this->language->get('cul_total');
		$this->data['cul_status'] = $this->language->get('cul_status');
		
		
		
		$showtime = isset($this->request->get['showtime']) ? $this->request->get['showtime'] : 0;
		
		if ($showtime == 1){
			$last_date = "tomorrow";
			$stat_date = "tomorrow";
		} elseif($showtime > 0){
			$stat_date = "today";
			$last_date = "today +".$showtime." days";
		} elseif($showtime < 0) {
			$last_date = "today";
			$stat_date = "today ".$showtime." days";
		} elseif( $showtime == 0) {
			$last_date = "today";
			$stat_date = "today";
		}
		
		$my_orders = $this->getMyOrders(array('stat_date' => $stat_date,'last_date' => $last_date,'order_status' => "2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21"));
		
		$orders_by_day = array();
		
		$total_meals = 0;
		
		foreach($my_orders as $order){
			$orders_by_day[$order['order_date']][] = $order;
			
			$total_meals += $order['number_of_meals'];
		}
		
		$this->data['orders'] = $orders_by_day;
		
		$showhistory = isset($this->request->get['sh']) ? $this->request->get['sh'] : 0;
		
		if($showhistory > 0){
			$stat_date = "today";
			$last_date = "today +".$showhistory." days";
		} elseif($showhistory < 0) {
			$last_date = "today";
			$stat_date = "today ".$showhistory." days";
		} else {
			$last_date = "today";
			$stat_date = "today -7 days";
		}
		
		$start = isset($this->request->get['shs']) ? $this->request->get['shs'] : 0;
		$end = isset($this->request->get['she']) ? $this->request->get['she'] : 0;
		
		$or_stat = "2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21"; // was "5,17,18,19,20,21";
		
		if($start && $end){
			$last_date = $end;
			$stat_date = $start;
			$or_stat = "17,18,20";
		}
		
		$my_history_orders = $this->getMyOrders(array('stat_date' => $stat_date,'last_date' => $last_date,'order' => 'DESC', 'order_status' => $or_stat));

		
		$history_orders_by_day = array();
		$total_meals = 0;
		$total_cost = 0;
		
		foreach($my_history_orders as $key => $order){
			
			$history_orders_by_day[$order['order_date']][] = $order;
			
			$total_meals += $order['number_of_meals'];
			$total_cost += $order['int_total_profit'];
			$currency_value = $order['currency_value'];
		}
		
		$this->load->model('tool/image');
		
		$sql = "SELECT *  FROM " . DB_PREFIX . "customer WHERE customer_group_id = 2";
		$query = $this->db->query($sql);
		
		$cooks_info = $query->rows;
		
		$all_cooks = array();
		
		foreach($cooks_info as $cook_info){
			if(!empty($cook_info['image'])){
				if (strpos($cook_info['image'], 'http') === 0) {
					$author_pic = $cook_info['pic_square'];
				}else{
					$author_pic = $this->model_tool_image->resize($cook_info['image'], 40, 40);
				}
			} else {
				$author_pic = $this->model_tool_image->resize("no-avatar-women.png", 40, 40);;
			}
			
			$all_cooks[] = array(
				"id" => $cook_info['customer_id'],
				"name" => $cook_info['firstname'],
				"img" => "$author_pic",
			);
			
		}
		
		$this->data['all_cooks'] = $all_cooks;
		
		$this->data['total_meals'] = $total_meals;
		$this->data['total_cost'] = $this->currency->format($total_cost, $currency_value);
		
		$this->data['history_orders'] = $history_orders_by_day;
		
		$this->data['waiting_orders'] = $this->getMyOrders(array('order_status' => '1'));
		$this->data['waiting_orders_num'] = count($this->data['waiting_orders']);
		
		list($this->template, $this->children) = $this->MsLoader->MsHelper->loadTemplate('order-my-orders');
		$this->response->setOutput($this->render());
	}
}

?>
