<?php 
#####################################################################################
#  API for Opencart 1.5.x From HostJars http://opencart.hostjars.com    			#
#####################################################################################
class ControllerFeedApi extends Controller {
	public function index() {
		if ($this->config->get('api_status')) {
			
			$output = '';
			
			if($this->request->server['REQUEST_METHOD'] == 'GET') {
				if (isset($this->request->get['func']) && is_callable(array($this, $this->request->get['func']))) {
					$output = json_encode(call_user_func(array($this, $this->request->get['func'])));
				}
			}
			elseif ($this->request->server['REQUEST_METHOD'] == 'POST')	{
				if (isset($this->request->get['func']) && is_callable(array($this, $this->request->get['func']))) {
					$output = json_encode(call_user_func(array($this, $this->request->get['func'])));
				}
			}
			elseif($this->request->server['REQUEST_METHOD'] == 'PUT') {
				if (isset($this->request->get['func']) && is_callable(array($this, $this->request->get['func']))) {
					$output = json_encode(call_user_func(array($this, $this->request->get['func'])));
				}
			}
			elseif($this->request->server['REQUEST_METHOD'] == 'DELETE') {
				if (isset($this->request->get['func']) && is_callable(array($this, $this->request->get['func']))) {
					if (!isset($this->request->get['param1'])) {
						$output = "DELETE functions require \"param1\"";
					}
					else {
						$output = json_encode(call_user_func(array($this, $this->request->get['func'])));
					}
				}
			}
			$output=str_replace("null","",$output);
			
			
			$this->response->setOutput($output);
		}
	}

	/**
	 *  WRITE YOUR DESIRED API FUNCTIONS HERE:
	 *  
	 *  example calling function getProducts() as restful webservice:
	 *  http://www.example.com/index.php?route=feed/api&func=getProducts
	 * 
	 */


	//"GET" FUNCTIONS:

	// example function - getProducts loads the products model and calls its getProducts function.
	private function getProducts() {
		if ($this->request->server['REQUEST_METHOD'] != 'GET') {
			return '';
		}
		$this->load->model('catalog/product');
		return $this->model_catalog_product->getProducts_api();
	}
	
	private function getProductsByCategory() {
		if ($this->request->server['REQUEST_METHOD'] != 'GET') {
			return '';
		}
		$this->load->model('catalog/product');
		return $this->model_catalog_product->getProducts_byCategory($this->request->get['category_id']);
	}
	
	private function getProduct() {
		if ($this->request->server['REQUEST_METHOD'] != 'GET') {
			return '';
		}
		$this->load->model('catalog/product');
		return $this->model_catalog_product->getProduct_with_images($this->request->get['product_id']);
	}
	
	private function getCategories() {
		if ($this->request->server['REQUEST_METHOD'] != 'GET') {
			return '';
		}
		$this->load->model('catalog/category');
		return $this->model_catalog_category->getCategories();
	}
	
	// example function - getCustomers from the database
	private function getCustomers() {
		if ($this->request->server['REQUEST_METHOD'] != 'GET') {
			return '';
		}
		$this->load->model('feed/api');
		return $this->model_feed_api->getCustomers();
	}
	
	private function newCustomer() {
		if ($this->request->server['REQUEST_METHOD'] != 'GET') {
			return '';
		}
		$this->load->model('feed/api');
		$this->load->model('account/customer');
		$this->model_feed_api->newCustomer();
	}
	
	
	private function CustomerLogin() {
		if ($this->request->server['REQUEST_METHOD'] != 'GET') {
			return '';
		}
		$this->load->model('feed/api');
		$this->load->model('account/customer');
		$this->model_feed_api->CustomerLogin();
	}
	
	
	private function forgotPassword() {
		if ($this->request->server['REQUEST_METHOD'] != 'GET') {
			return '';
		}
		$this->load->model('feed/api');

		$this->model_feed_api->forgotPassword();
	}
	
	
	
	private function get_sellers() {
		if ($this->request->server['REQUEST_METHOD'] != 'GET') {
			return '';
		}
		
		return $this->MsLoader->MsSeller->getSellers();
	}
	private function get_seller() {
		if ($this->request->server['REQUEST_METHOD'] != 'GET') {
			return '';
		}
		
		return $this->MsLoader->MsSeller->getSellers(array("seller_id" => $this->request->get['seller_id']));
	}
	
	private function get_seller_cancelation_policy() {
		if ($this->request->server['REQUEST_METHOD'] != 'GET') {
			return '';
		}
		return $this->MsLoader->MsSeller->getSeller_cancelation_policy($this->request->get['seller_id']);
	}
	private function update_seller_cancelation_policy() {
		if ($this->request->server['REQUEST_METHOD'] != 'POST') {
			return '';
		}
		return $this->MsLoader->MsSeller->updateSeller_cancelation_policy($this->request->post['seller_id'],$this->request->post['value']);
	}
	
	
	private function get_seller_privacy_policy() {
		if ($this->request->server['REQUEST_METHOD'] != 'GET') {
			return '';
		}
		
		return $this->MsLoader->MsSeller->getSeller_privacy_policy($this->request->get['seller_id']);
	}
	private function update_seller_privacy_policy() {
		if ($this->request->server['REQUEST_METHOD'] != 'POST') {
			return '';
		}
		
		return $this->MsLoader->MsSeller->updateSeller_privacy_policy($this->request->post['seller_id'],$this->request->post['value']);
	}
	
	
	private function get_seller_terms_of_service() {
		if ($this->request->server['REQUEST_METHOD'] != 'GET') {
			return '';
		}
		
		return $this->MsLoader->MsSeller->getSeller_terms_of_service($this->request->get['seller_id']);
	}
	private function update_seller_terms_of_service() {
		if ($this->request->server['REQUEST_METHOD'] != 'POST') {
			return '';
		}
		
		return $this->MsLoader->MsSeller->updateSeller_terms_of_service($this->request->post['seller_id'],$this->request->post['value']);
	}
	
	
	
	private function get_seller_products() {
		if ($this->request->server['REQUEST_METHOD'] != 'GET') {
			return '';
		}
		
		return $this->MsLoader->MsProduct->getProducts(array("seller_id" => $this->request->get['seller_id']));
	}
	
	
	
	// example function - getOrders from the database
	private function getOrders() {
		if ($this->request->server['REQUEST_METHOD'] != 'GET') {
			return '';
		}
		$this->load->model('feed/api');
		return $this->model_feed_api->getOrders();
	}
	
	//"DELETE" FUNCTIONS:

	// example function - deleteProduct loads the model and calls its deleteProduct function.
	private function deleteProduct($prodid) {
//		if ($this->request->server['REQUEST_METHOD'] != 'DELETE') {
//			return '';
//		}
		$this->load->model('feed/api');
		return $this->model_feed_api->deleteProduct($this->request->get['param1']);
	}
	
	
	
	
###########################  Messages functions ################################
	private function sendMessage() {
		$this->load->model('account/messages');
		
		//POST method
		$from=isset($_POST['from']) ? (int)$_POST['from'] : "";
		$to=isset($_POST['to']) ? (int) $_POST['to'] : "";
		$subject=isset($_POST['subject']) ? $_POST['subject'] : "";
		$message=isset($_POST['message']) ? $_POST['message'] : "";
		
		
		return $this->model_account_messages->send_message($from,$to,$subject,$message);
	}

	private function getUserInbox()	{
		$this->load->model('account/messages');
		
		$uid = isset($_GET['uid']) ? (int) $_GET['uid'] : '';
		return $this->model_account_messages->get_user_inbox($uid);
	}
	
	private function getUserOutbox()	{
		$this->load->model('account/messages');
		
		$uid = isset($_GET['uid']) ? (int) $_GET['uid'] : '';
		return $this->model_account_messages->get_user_outbox($uid);
	}
	
	private function markAsRead() {
		$this->load->model('account/messages');
		
		$msg = isset($_GET['msg']) ? (int) $_GET['msg'] : '';
		return $this->model_account_messages->mark_as_read($msg);
	}
	

################################################################################
	
	
	
	
}
?>