<?php
class ControllerCheckoutSuccess extends Controller { 
	public function index() { 	
		if (isset($this->session->data['order_id']) || isset($this->request->get['order_id'])) {
			
			if (isset($this->request->get['order_id'])){
				$this_order_id = $this->request->get['order_id'];
			} else {
				$this_order_id = $this->session->data['order_id'];
			}
			
			$this->load->model('checkout/order');
			$this->load->model('account/address');
			$this->load->model('account/order');
			$this->load->model('account/customer');
			$this->load->model('catalog/product');
			$this->load->model('tool/image');
			
			$order_info = $this->model_checkout_order->getOrder($this_order_id);
			
			if(!$order_info['order_status_id']){
				$this->redirect($this->url->link('common/home', '&allow', 'SSL'));
			}

			// clearing all of last order info
			$this->cart->clear();
			unset($this->session->data['shipping_method']);
			unset($this->session->data['shipping_methods']);
			unset($this->session->data['payment_method']);
			unset($this->session->data['payment_methods']);
			unset($this->session->data['guest']);
			unset($this->session->data['comment']);
			unset($this->session->data['order_id']);	
			unset($this->session->data['coupon']);
			unset($this->session->data['reward']);
			unset($this->session->data['voucher']);
			unset($this->session->data['vouchers']);
			// Add Unset all Homeals stuff (date,day,side,ect..)
			unset($this->session->data['filter_place']);
			unset($this->session->data['date']);
			unset($this->session->data['time']);
			// clearing order cache end
			
			/**********************************************
			*	Homeals script to update address
			**********************************************/
			
			
			$address_info = $this->db->query("SELECT * FROM " . DB_PREFIX . "address WHERE customer_id = '".(int)$order_info['customer_id']."' AND address_1 = '".$this->db->escape($order_info['payment_address_1'])."' AND city = '".$this->db->escape($order_info['payment_city'])."' ");
			
			if($address_info->num_rows > 1){
				
				$max_id = 0;
				
				foreach($address_info->rows as $address){
					if($address['address_id'] > $max_id) {
						$max_id = $address['address_id'];
					}
				}
				
				foreach($address_info->rows as $address){
					if($address['address_id'] != $max_id){
						$this->model_account_address->deleteAddress($address['address_id']);
					}
				}
				
				$this->db->query("UPDATE `" . DB_PREFIX . "customer` SET address_id = '" . (int)$max_id . "' WHERE customer_id = '" . (int)$order_info['customer_id'] . "'");
				
				$this->session->data['address_type'] = "def";
			} else {
				if(isset($address_info->row['address_id'])){
					$this->customer->setDefAddress($address_info->row['address_id']);
					$this->session->data['address_type'] = "def";
				}
			}
			/********************************** end **************************/
			
			// getting order products
			$order_products_sql = "SELECT * FROM " . DB_PREFIX . "order_product WHERE order_id = '".(int)$this_order_id."'";
			
			$order_info_products = $this->db->query($order_products_sql);
			
			// an array of products_id from the order	
			$products = array();
			$products_ids = array();
			$top_cats = array();
			$total_quantity = 0;
			
			foreach( $order_info_products->rows as $product_info){
				
				$product_cat_info_sql = "SELECT category_id FROM " . DB_PREFIX . "product_to_category WHERE product_id = ".$product_info['product_id']."";
				$product_cat_info = $this->db->query($product_cat_info_sql);
				
				// getting number of cats and suming them up
				foreach($product_cat_info->rows as $cat_id){
					if(isset($top_cats[$cat_id['category_id']])){
						$top_cats[$cat_id['category_id']] += 1;
					} else {
						$top_cats[$cat_id['category_id']] = 1;
					}
				}
				
				$products[] = array(
							'product_id' => $product_info['product_id'],
							'cook_id' => $product_info['cook_id'],
							'product_quant' => $product_info['quantity']
						);
				
				$products_ids[] = $product_info['product_id'];
				
				$total_quantity += $product_info['quantity'];
			}
			
			$this->data['total_quantity'] = $total_quantity;
			
			// getting top cook and his products
			$cook_rating = array();
			foreach($products as $product){
				if(isset($cook_rating[$product['cook_id']])){
					$cook_rating[$product['cook_id']] += 1;
				} else {
					$cook_rating[$product['cook_id']] = 1;
				}
			}
			
			$top_cook = $this->doublemax($cook_rating);
			$top_cook = reset($top_cook);
			
			$this->data['share_meal_id'] = reset($products_ids);
			
			$data_top_cook = array(
				      'filter_exclude_products' => $products_ids,
				      'filter_customer_id' => $top_cook,
				      'filter_day_available' => $order_info['delivery_date'],
				      'start' => 0,
				      'limit' => 2
				);
				
			// top seller products
			$seller_products = $this->model_catalog_product->getProducts($data_top_cook);
			
			if(empty($seller_products)){
				$data_top_cook = array(
				      'filter_exclude_products' => $products_ids,
				      'filter_customer_id' => $top_cook,
				      'filter_day_available' => 'all',
				      'start' => 0,
				      'limit' => 2
				);
				
				$seller_products = $this->model_catalog_product->getProducts($data_top_cook);
			}
			
			$this->data['seller_products'] = $this->get_products_for_controller($seller_products);
			$this->data['top_cook'] = $top_cook;
			// adding the top cook prodycuts to exclude them from category search
			foreach( $seller_products as $product_info){
				$products_ids[] = $product_info['product_id'];
			}
			
			$cats = array();
			foreach($top_cats as $cat_id => $nothing){
				$cats[] = $cat_id;
			}
			
			// top category products cats
			$top_cat = $this->doublemax($top_cats);
			$top_cat = reset($top_cat);
			
			$cats = array_intersect(array(59,60,61,62,63),$cats);	
			$top_cat = reset($cats);
			
			$data_top_cat = array(
				      'filter_exclude_products' => $products_ids,
				      'filter_category_id' => $top_cat,
				      'filter_day_available' => $order_info['delivery_date'],
				      'start' => 0,
				      'limit' => 3
				      );
			// top category products			
			$category_products = $this->model_catalog_product->getProducts($data_top_cat);
			
			if(empty($category_products)){
				$data_top_cat = array(
				      'filter_exclude_products' => $products_ids,
				      'filter_category_id' => $top_cat,
				      'filter_day_available' => 'all',
				      'start' => 0,
				      'limit' => 3
				      );
				$category_products = $this->model_catalog_product->getProducts($data_top_cat);
			}
			
			$this->data['category_products'] = $this->get_products_for_controller($category_products);	
			$this->data['top_cat'] = $top_cat;
			
			// getting top cook and his products end
			$cat_name_sql = "SELECT name  FROM " . DB_PREFIX . "category_description WHERE category_id = " . $top_cat . " AND language_id = 2";
			$cat_name_info = $this->db->query($cat_name_sql);
			$this->data['top_cat_name'] = $cat_name_info->row['name'];
			
			$cooks_ids = json_decode($order_info['cook_id']);
			
			foreach($cooks_ids as $cook_id){
				$cook_info = $this->model_account_customer->getCustomer($cook_id);
				$customer_address = $this->model_account_address->getAddressesById($cook_id);
				$customer_address = reset($customer_address);
				
				if(!empty($cook_info['image'])){
					if (strpos($cook_info['image'], 'http') === 0) {
						$author_pic = $cook_info['pic_square'];
					}else{
						$author_pic = $this->model_tool_image->resize($cook_info['image'], 100, 101);
					}
				} else {
					$author_pic = "";
				}
				
				$this->data['cooks'][$cook_id] = array(
									'cook_name'		=>	sprintf($this->language->get('cook_name'),$cook_info['firstname'],$cook_info['lastname']),
									'cook_id'		=>	$cook_id,
									'image'			=>	$author_pic,
									'firstname'		=>	$cook_info['firstname'],
									'sp'			=>	$cook_info['sp'],
									'email'			=>	$cook_info['email'],
									'customer_id'		=>	$cook_info['customer_id'],
									'cook_city'		=>	$customer_address['city'],
									'meals_this_order'	=>	$customer_address['city'],
									'cook_info'		=>	$cook_info
								       );
			}
			
			$this->data['totals'] = $this->model_account_order->getOrderTotals($this_order_id);
			$this->data['order_id'] = $this_order_id;
			
			$this->data['order_time'] = strftime("%A, %e %B, ",strtotime($order_info['delivery_date']))." ".strftime("%H:%M",strtotime($order_info['delivery_time']))." - ".strftime("%H:%M",strtotime($order_info['delivery_time']." +1 hour"));
			
			$company = ($order_info['payment_company'] != "" ) ? ", ".$order_info['payment_company'] : "";
			$this->data['order_address'] = $order_info['payment_address_1'].$company;
			
			$this->language->load('checkout/success');
			
			$this->document->setTitle('ההזמנה נקלטה');
			
			$this->data['heading_title'] = $this->language->get('heading_title');
			
			if ($this->customer->isLogged()) {
				$this->data['text_message'] = sprintf($this->language->get('text_customer'), $this->url->link('account/account', '', 'SSL'), $this->url->link('account/order', '', 'SSL'), $this->url->link('account/download', '', 'SSL'), $this->url->link('information/contact'));
			} else {
				$this->data['text_message'] = sprintf($this->language->get('text_guest'), $this->url->link('information/contact'));
			}
			
			// homeals success!!
			
			$this->data['heading_success'] = $this->language->get('heading_success');
			$this->data['soon_aproved'] = $this->language->get('soon_aproved');
			$this->data['heading_deal'] = $this->language->get('heading_deal');
			$this->data['deal_text'] = $this->language->get('deal_text');
			
			$this->data['text_thank_you'] = $this->language->get('text_thank_you');
			$this->data['text_order_summary'] = $this->language->get('text_order_summary');
			$total_cooks = count((array)json_decode($order_info['cook_id']));
			$this->data['text_order_orderd'] = sprintf('%s ארוחות מ-%s בשלנים',$total_quantity,$total_cooks);
			$this->data['text_order_total'] = $this->language->get('text_order_total');
			$this->data['text_order_info_link'] = $this->language->get('text_order_info_link');
			$this->data['text_order_details'] = $this->language->get('text_order_details');
			
			
			$this->data['order_now'] = $this->language->get('order_now');
			
			$this->load->model('catalog/product');
			
			$i = 0;
			$j = 0;
			
			$days = array();
			$days_more = array();
			
			while($i < 5){
				$week_day = date("m/d/y", strtotime('+'.$j.' day'));
				
				$data = array(
					'filter_category_id' 	=> 0,
					'filter_meal_type'   	=> 0,
					'filter_customer_id' 	=> 0,
					'filter_day_available'  => $week_day
				);
				
				$results = $this->model_catalog_product->getProducts($data);
				
				$products_numbers = count($results);
				
				if($products_numbers > 0) {
					$days[$i] = date('D', strtotime('+'.$j.' day'));
					$days_more[$i] = $j;
					$i++;
				}
				
				$j++;
			}
			
			$this->data['tomorrow'] = $days_more;
			
			$this->data['button_continue'] = $this->language->get('button_continue');
			$this->data['continue'] = $this->url->link('common/home');
			
			if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/common/success.tpl')) {
				$this->template = $this->config->get('config_template') . '/template/common/success.tpl';
			} else {
				$this->template = 'default/template/common/success.tpl';
			}
			
			$this->children = array(
				'common/column_left',
				'common/column_right',
				'common/content_top',
				'common/content_bottom',
				'common/footer',
				'common/header'	
			);
			
			$this->response->setOutput($this->render());
			
		} else {
			
			$this->redirect($this->url->link('common/home', '&allow', 'SSL'));
			
			die;
			
			if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/common/success.tpl')) {
				$this->template = $this->config->get('config_template') . '/template/common/success.tpl';
			} else {
				$this->template = 'default/template/common/success.tpl';
			}
			
			$this->children = array(
				'common/column_left',
				'common/column_right',
				'common/content_top',
				'common/content_bottom',
				'common/footer',
				'common/header'	
			);
			
			$this->response->setOutput($this->render());
		}
  	}
	
	
	/*****************************************
	*
	*	Will Send mail to all orders with status delivered for that given day
	*
	****************************************/
	public function notifyAllDelivered() {
		
		$this->load->model('checkout/order');
		
		$today = date("Y-m-d",strtotime("today"));
		
		$todays_orders = $this->db->query("SELECT * FROM " . DB_PREFIX . "order WHERE order_status_id = '22' AND delivery_date = '". $today ."'");
		
		foreach ($todays_orders->rows as $order){
			print_r($order['order_id']);
			$this->model_checkout_order->sendReviewMail($order['order_id']);
			print_r(" - review mail sent successfuly \n\r");
		}
		
		die();
		
	}
	
	private function doublemax($mylist){
		$maxvalue=max($mylist); 
		while(list($key,$value)=each($mylist)){ 
		  if($value==$maxvalue)$maxindex=$key; 
		}
		return array($maxvalue => $maxindex); 
	}
	
	// a function that will return the products info in a form that will fir ower product controller
	private function get_products_for_controller($products){
		
		$controller_ready_products = array();
		
		if (!empty($products)) {
			foreach ($products as $product) {
				$product_data = $this->model_catalog_product->getProduct($product['product_id']);
				if ($product_data['image'] && file_exists(DIR_IMAGE . $product_data['image'])) {
					$image = $this->model_tool_image->resize($product_data['image'], 333, 230);
					//$this->MsLoader->MsFile->resizeImage($product_data['image'], $this->config->get('msconf_product_seller_profile_image_width'), $this->config->get('msconf_product_seller_profile_image_height'));
				} else {
					$image = $this->MsLoader->MsFile->resizeImage('no_image.jpg', $this->config->get('msconf_product_seller_profile_image_width'), $this->config->get('msconf_product_seller_profile_image_height'));
				}

				if (($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) {
					$price = $this->currency->format($this->tax->calculate($product_data['price'], $product_data['tax_class_id'], $this->config->get('config_tax')));
				} else {
					$price = false;
				}
						
				if ((float)$product_data['special']) {
					$special = $this->currency->format($this->tax->calculate($product_data['special'], $product_data['tax_class_id'], $this->config->get('config_tax')));
				} else {
					$special = false;
				}
				
				if ($this->config->get('config_review_status')) {
					$rating = $product_data['rating'];
				} else {
					$rating = false;
				}
				
				
				$side_results = $this->model_catalog_product->getProductRelated($product['product_id']);
				$side_data = array();
				
				foreach ($side_results as $side_result) {
					if ($side_result['image']) {
						$side_image = $this->model_tool_image->resize($side_result['image'], $this->config->get('config_image_related_width'), $this->config->get('config_image_related_height'));
						$review_image = $this->model_tool_image->resize($side_result['image'], 212, 145);
					} else {
						$side_image = $this->model_tool_image->resize('no_image.jpg', $this->config->get('config_image_related_width'), $this->config->get('config_image_related_height'));
						$review_image = $this->model_tool_image->resize('no_image.jpg', 212, 145);
					}
					
					if (($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) {
						$side_price = $this->currency->format($this->tax->calculate($side_result['price'], $side_result['tax_class_id'], $this->config->get('config_tax')));
						$side_price_int = (int)$side_result['price'];
					} else {
						$side_price = false;
						$side_price_int = false;
					}
					
					
					$side_data[] = array(
						'product_id' 	 => $side_result['product_id'],
						'thumb'   	 => $side_image,
						'image'   	 => $review_image,
						'name'    	 => $side_result['name'],
						'price'   	 => $side_price,
						'price_int'   	 => $side_price_int,
						'text_extra' 	 => sprintf($this->language->get('text_extra'), $side_price),
						'href'    	 => $this->url->link('product/product', 'product_id=' . $side_result['product_id']),
					);
				}
				
				
				if($product['number_of_sides'] == 2){
					$total_sides = sprintf($this->language->get('total_sides_cat'), $product['number_of_sides']);
				}elseif($product['number_of_sides'] == 1){
					$total_sides = sprintf($this->language->get('total_sides_cat'), $product['number_of_sides']);
				}elseif($product['number_of_sides'] == 0){
					$total_sides = "";
				}
				
				//menny
				$this->load->model('catalog/availability');
				$avaiabilityDays = $this->model_catalog_availability->getDaysAvailability($product['product_id']);
				
				ksort($avaiabilityDays);
				
				
				$avaiabilityToday = false;
				$days_text = "זמין בימים: ";
				$i = 0;
				foreach($avaiabilityDays as $day){
					if($i){
						$days_text.= ", ";
					} else {
						$i = 1;
					}
					
					$days_text.= $day;
					
					if($day == strftime("%a",strtotime("today"))) {
						$avaiabilityToday = true;
					}
				}
				
				$controller_ready_products[] = array(
					'product_id' => $product['product_id'],
					'is_verified' => $product['is_verified'],
					'sides'	      => $side_data,
					'pro_cat' => $product['pro_cat'],
					'tax' => false,
					'avaiabilityToday' => json_encode($avaiabilityToday),
					'days_available' => $days_text,
					'thumb' => $image,
					'name' => $product_data['name'],
					'total_sides' => $total_sides,
					'price' => $price,
					'special' => $special,
					'rating' => $rating,
					'reviews'    => sprintf($this->language->get('text_reviews'), (int)$product_data['reviews']),
					'reviews_text' => (int)$product_data['reviews_text'],
					'href'    	 => $this->url->link('product/product', 'product_id=' . $product_data['product_id']),						
				);				
			}
			
			return $controller_ready_products;
			
			if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/common/meal_controller.tpl')) {
				$this->template = $this->config->get('config_template') . '/template/common/meal_controller.tpl';
			} else {
				$this->template = 'default/template/common/meal_controller.tpl';
			}
			
			//$this->children = array(
			//	'common/column_left',
			//	'common/column_right',
			//	'common/content_top',
			//	'common/content_bottom',
			//	'common/footer',
			//	'common/header'	
			//);
			
			$this->response->setOutput($this->render());
			
		} else {
			return NULL;
		}
	}
    
     
	
}
?>