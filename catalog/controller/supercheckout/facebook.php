<?php
class ControllerSupercheckoutFacebook extends Controller {

    public function index() {

        $browser = ($this->request->server['HTTP_USER_AGENT']);

        //flag for IE 1-7
        if (preg_match('/(?i)msie [1-7]/', $browser)) {

            $this->data['IE7'] = true;

        } else {

            $this->data['IE7'] = false;

        }
        //adding script to the page<!-- Gritter Notifications Plugin -->

        $this->document->addScript('catalog/view/javascript/supercheckout/tinysort/jquery.tinysort.min.js');

        $this->document->addScript('catalog/view/javascript/supercheckout/common.js');

        $this->document->addScript('catalog/view/javascript/supercheckout/bootstrap.js');
        
        $this->document->addScript('catalog/view/javascript/supercheckout/jquery.colorbox.js');
        
        $this->document->addScript('catalog/view/javascript/supercheckout/theme/plugins/notifications/Gritter/js/jquery.gritter.min.js');
        $this->document->addScript('catalog/view/javascript/supercheckout/theme/plugins/notifications/notyfy/jquery.notyfy.js');
        $this->document->addScript('catalog/view/javascript/supercheckout/theme/demo/notifications.js');
        

        $this->load->model('setting/setting');

        $result = $this->model_setting_setting->getSetting('velocity_supercheckout', $this->config->get('config_store_id'));
        if(!empty($result)) {
            $this->settings = $result['supercheckout'];

            $this->data['settings'] = $result['supercheckout'];
        }
        if (!isset($this->data['settings'])) {
            $settings = $this->model_setting_setting->getSetting('default_supercheckout', 0);
            $this->data['settings'] = $settings['default_supercheckout'];

        }
        if(empty($this->data['settings']) || !$this->data['settings']['general']['enable']){
          //  $this->redirect($this->url->link('checkout/checkout','','SSL'));
        }

        if (isset($this->data['settings']['general']['default_option'])) { //for setting default value for guest or login
            $this->data['account'] = $this->data['settings']['general']['default_option'];
        } else {
            $this->data['account'] = 'guest';
        }
        
        foreach ($this->data['settings']['step'] as $key => $step) {
            $sort_block[$key] = $step;
        }
        $redirect = "";
        //unsetting methods
        unset($this->session->data['payment_method']);
        unset($this->session->data['shipping_method']);
        unset($this->session->data['shipping_country_id']);
        unset($this->session->data['shipping_zone_id']);
        unset($this->session->data['payment_country_id']);
        unset($this->session->data['payment_zone_id']);
        unset($this->session->data['shipping_address_id']);
        unset($this->session->data['payment_address_id']);
        unset($this->session->data['payment']);
        unset($this->session->data['shipping']);
        unset($this->session->data['set_add_shipping_address_check']);
        unset($this->session->data['set_add_payment_address_check']);
        unset($this->session->data['guestAccount_customer_id']);

        $this->data['sort_block'] = $sort_block;

        $this->data['payment_address_sort_order'] = $this->settings['step']['payment_address'];
        $this->data['shipping_address_sort_order'] = $this->settings['step']['shipping_address'];
        $this->data['address_id'] = 0;
        //zone code fix
            $this->load->model('localisation/country');

            $country_info_guest = $this->model_localisation_country->getCountry($this->config->get('config_country_id'));            
            $this->session->data['country_info_guest']=$country_info_guest;
            $this->data['country_info_guest']=$country_info_guest;
        //zone code fix
        if (!$this->customer->isLogged()) {
            
            $this->session->data['guest']['shipping']['country_id'] = $this->config->get('config_country_id');
            $this->session->data['guest']['shipping']['zone_id'] = $this->config->get('config_zone_id');
            $this->session->data['guest']['payment']['country_id'] = $this->config->get('config_country_id');
            $this->session->data['guest']['payment']['zone_id'] = $this->config->get('config_zone_id');
            $this->session->data['guest']['payment']['firstname'] = "";
            $this->session->data['guest']['payment']['lastname'] = "";
            $this->session->data['guest']['payment']['company'] = "";
            $this->session->data['guest']['payment']['company_id'] = "";
            $this->session->data['guest']['payment']['tax_id'] = "";
            $this->session->data['guest']['payment']['address_1'] = "";
            $this->session->data['guest']['payment']['address_2'] = "";
            $this->session->data['guest']['payment']['city'] = "";
            $this->session->data['guest']['payment']['postcode'] = "";
            $this->session->data['guest']['payment']['zone'] = "";
            $this->session->data['guest']['payment']['country'] = "";
            $this->session->data['guest']['payment']['iso_code_2'] = $country_info_guest['iso_code_2'];
            $this->session->data['guest']['payment']['iso_code_3'] = $country_info_guest['iso_code_3'];
            $this->session->data['guest']['payment']['address_format'] = $country_info_guest['address_format'];

            $this->session->data['guest']['shipping']['firstname'] = "";
            $this->session->data['guest']['shipping']['lastname'] = "";
            $this->session->data['guest']['shipping']['company'] = "";
            $this->session->data['guest']['shipping']['company_id'] = "";
            $this->session->data['guest']['shipping']['tax_id'] = "";
            $this->session->data['guest']['shipping']['address_1'] = "";
            $this->session->data['guest']['shipping']['address_2'] = "";
            $this->session->data['guest']['shipping']['city'] = "";
            $this->session->data['guest']['shipping']['postcode'] = "";
            $this->session->data['guest']['shipping']['zone'] = "";
            $this->session->data['guest']['shipping']['country'] = "";
            $this->session->data['guest']['shipping']['iso_code_2'] =$country_info_guest['iso_code_2'];;
            $this->session->data['guest']['shipping']['iso_code_3'] = $country_info_guest['iso_code_3'];			
            $this->session->data['guest']['shipping']['address_format'] = $country_info_guest['address_format'];

            $this->session->data['guest']['customer_group_id'] = "";
            $this->session->data['guest']['firstname'] = "";
            $this->session->data['guest']['lastname'] = "";
            $this->session->data['guest']['email'] = "";
            $this->session->data['guest']['telephone'] = "";
            $this->session->data['guest']['fax'] = "";
        } else {
            $this->data['address_id'] = $this->customer->getAddressId();
        }

        /*** Settting default values to county and zone stored in database */
        $this->session->data['shipping_country_id'] = $this->config->get('config_country_id');
        $this->session->data['payment_country_id'] = $this->config->get('config_country_id');
        $this->session->data['shipping_zone_id'] = $this->config->get('config_zone_id');
        $this->session->data['payment_zone_id'] = $this->config->get('config_zone_id');
        //iso fix
        $this->session->data['shipping_iso_code_2'] = $this->session->data['country_info_guest']['iso_code_2'];
        $this->session->data['shipping_iso_code_3'] = $this->session->data['country_info_guest']['iso_code_3'];
        $this->session->data['payment_iso_code_2'] = $this->session->data['country_info_guest']['iso_code_2'];
        $this->session->data['payment_iso_code_3'] = $this->session->data['country_info_guest']['iso_code_3'];

        // Validate cart has products and has stock.
        if ((!$this->cart->hasProducts() && empty($this->session->data['vouchers'])) || (!$this->cart->hasStock() && !$this->config->get('config_stock_checkout'))) {

            //$this->redirect($this->url->link('checkout/cart'));

        }

        //validate login
        if ($this->customer->isLogged()) {            
            $this->data['firstName'] = $this->customer->getFirstName();
            if(!isset($this->data['firstName'])|| $this->data['firstName']==""){
                $this->data['firstName']="";
            }
            $this->data['lastName'] = $this->customer->getLastName();
            if(!isset($this->data['lastName'])|| $this->data['lastName']==""){
                $this->data['lastName']="";
            }
            $this->data['logoutLink'] = $this->url->link('account/logout', '', 'SSL');
            if(!isset($this->data['logoutLink'])|| $this->data['logoutLink']==""){
                $this->data['logoutLink']="";
            }
            $this->data['myAccount'] = $this->url->link('account/account', '', 'SSL');
            if(!isset($this->data['myAccount'])|| $this->data['myAccount']==""){
                $this->data['myAccount']="";
            }
            $this->data['myOrder'] = $this->url->link('account/order', '', 'SSL');
            if(!isset($this->data['myOrder'])|| $this->data['myOrder']==""){
                $this->data['myOrder']="";
            }
        }

        
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        ////////////////////////////////////////////////////////////////  LOGIN PART  //////////////////////////////////////////////////////////////////////////////
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        $this->data['login_options'] = $this->language->get('login_options');
        $this->data['text_login_option'] = $this->language->get('text_login_option');
        $this->data['text_my_account'] = $this->language->get('text_my_account');
        $this->data['text_my_orders'] = $this->language->get('text_my_orders');
        $this->data['text_logout'] = $this->language->get('text_logout');
        $this->data['text_new_customer'] = $this->language->get('text_new_customer');
        $this->data['text_returning_customer'] = $this->language->get('text_returning_customer');
        $this->data['text_checkout'] = $this->language->get('text_checkout');
        $this->data['text_register'] = $this->language->get('text_register');
        $this->data['text_guest'] = $this->language->get('text_guest');
        $this->data['text_i_am_returning_customer'] = $this->language->get('text_i_am_returning_customer');
        $this->data['text_register_account'] = $this->language->get('text_register_account');
        $this->data['text_forgotten'] = $this->language->get('text_forgotten');
        $this->data['text_register_account'] = $this->language->get('text_register_account');
        $this->data['text_create_account'] = $this->language->get('text_create_account');
        $this->data['entry_confirm'] = $this->language->get('entry_confirm');
        $this->data['text_OR_separator'] = $this->language->get('text_OR_separator');
        
        $this->data['entry_email'] = $this->language->get('entry_email');
        $this->data['entry_password'] = $this->language->get('entry_password');
        $this->data['button_continue'] = $this->language->get('button_continue');
        $this->data['button_login'] = $this->language->get('button_login');
        //bug fix
        $this->data['button_place_order'] = $this->language->get('button_place_order');
        $this->data['button_update_link'] = $this->language->get('button_update_link');
        $this->data['text_coupon_code'] = $this->language->get('text_coupon_code');
        $this->data['text_voucher_code'] = $this->language->get('text_voucher_code');
        $this->data['text_billing_address'] = $this->language->get('text_billing_address');
        $this->data['text_sign_in_with'] = $this->language->get('text_sign_in_with');
        $this->data['column_action'] = $this->language->get('column_action');
        $this->data['registered_user'] = $this->language->get('registered_user');
        $this->data['text_guest_checkout'] = $this->language->get('text_guest_checkout');
        $this->data['social_login'] = $this->language->get('social_login');
        
        $this->data['guest_checkout'] = ($this->config->get('config_guest_checkout') && !$this->config->get('config_customer_price') && !$this->cart->hasDownload());

        $this->data['entry_firstname'] = $this->language->get('entry_firstname');
        $this->data['error_login_require'] = $this->language->get('error_login_require');
        $this->data['entry_lastname'] = $this->language->get('entry_lastname');
        $this->data['entry_email'] = $this->language->get('entry_email');
        $this->data['entry_telephone'] = $this->language->get('entry_telephone');
        $this->data['forgotten'] = $this->url->link('account/forgotten', '', 'SSL');

        //guest
        if (isset($this->session->data['guest']['firstname'])) {
            $this->data['firstname'] = $this->session->data['guest']['firstname'];
        } else {
            $this->data['firstname'] = '';
        }

        if (isset($this->session->data['guest']['lastname'])) {
            $this->data['lastname'] = $this->session->data['guest']['lastname'];
        } else {
            $this->data['lastname'] = '';
        }

        if (isset($this->session->data['guest']['email'])) {
            $this->data['email'] = $this->session->data['guest']['email'];
        } else {
            $this->data['email'] = '';
        }

        //admin control
        if ($this->settings['step']['facebook_login']['display']) {
            $this->data['facebook_enable'] = $this->settings['step']['facebook_login']['display'];
        } else {
            $this->data['facebook_enable'] = $this->settings['step']['facebook_login']['display'];
        }
        if ($this->settings['step']['google_login']['display']) {
            $this->data['google_enable'] = $this->settings['step']['google_login']['display'];
        } else {
            $this->data['google_enable'] = $this->settings['step']['google_login']['display'];
        }
        
        /**************************************************
         *  FACEBOOK LOGIN STARTS HERE
         *      FACEBOOK LOGIN STARTS HERE
         *          FACEBOOK LOGIN STARTS HERE
         *              FACEBOOK LOGIN STARTS HERE
         ***************************************************/

        //facebook login settings
        $appId = $this->settings['step']['facebook_login']['app_id'];
        $secret = $this->settings['step']['facebook_login']['app_secret'];
        $this->data['appId'] = $appId;
        $this->data['secret'] = $secret;

        //google login settings
        $this->load->library('googleSetup');

        $client = new apiClient();

        $redirect_url = $this->url->link('supercheckout/facebook','','SSL');

        $client->setClientId($this->settings['step']['google_login']['client_id']);
        $client->setClientSecret($this->settings['step']['google_login']['app_secret']);
        $client->setDeveloperKey($this->settings['step']['google_login']['app_id']);
        $client->setRedirectUri($redirect_url);
        $client->setApprovalPrompt(false);

        $oauth2 = new apiOauth2Service($client);

        $this->data['client'] = $client;
        $url = ($client->createAuthUrl());
        $this->data['url'] = $url;

        if (isset($this->request->get['code'])) {

            $client->authenticate();
            $info = $oauth2->userinfo->get();
            if (isset($info['given_name']) && $info['given_name'] != "") {

                $name = $info['given_name'];

            } else {

                $name = $info['name'];

            }

            $user_table = array(
                    'firstname' => $name,
                    'lastname' => $info['family_name'],
                    'email' => $info['email'],
                    'telephone' => '',
                    'fax' => '',
                    'password' => substr(md5(uniqid(rand(), true)), 0, 9),
                    'company' => '',
                    'company_id' => '',
                    'tax_id' => '',
                    'address_1' => '',
                    'address_2' => '',
                    'city' => '',
                    'postcode' => '',
                    'country_id' => '',
                    'zone_id' => '',
                    'customer_group_id' => 1,
                    'status' => 1,
                    'approved' => 1
            );

            $this->load->model('account/customer');
            $this->load->model('supercheckout/customer');

            //getting customer info if already exists
            $users_check = $this->model_account_customer->getCustomerByEmail($info['email']);

            //adding customer if new
            if (empty($users_check)) {

                $this->model_supercheckout_customer->addFacebookGoogleCustomer($user_table);

            }

            $users_check = $this->model_account_customer->getCustomerByEmail($info['email']);

            //loging in the customer
            $users_pass = $this->customer->login($info['email'], '', true);
            
            $this->session->data['customer_id'] = $users_check['customer_id'];
            
            if ($users_pass == true) {
                
                echo'<script>window.opener.location.href ="' . $redirect_url . '"; window.close();</script>';

            } else {

                echo'<script>window.opener.location.href ="' . $redirect_url . '"; window.close();</script>';

            }
        }
        
        
         /**************************************************
         *  FACEBOOK LOGIN ENDS HERE
         *      FACEBOOK LOGIN ENDS HERE
         *          FACEBOOK LOGIN ENDS HERE
         *              FACEBOOK LOGIN ENDS HERE
         ***************************************************/

        $this->data['logged'] = $this->customer->isLogged();
        $this->data['shipping_required'] = $this->cart->hasShipping();

        if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/supercheckout/facebook.tpl')) {

            $this->template = $this->config->get('config_template') . '/template/supercheckout/facebook.tpl';

        } else {

            $this->template = 'default/template/supercheckout/facebook.tpl';

        }

        $this->response->setOutput($this->render());

    }

}
?>