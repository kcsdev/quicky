<?php echo $header; ?>


</div><!-- content-inner -->
</div><!-- container -->

<div id="homepage-con">
    <div id="search">
        <?php if(false) { // hiding slider ?>
        <div id="slider-con">
            <div id="slider1_container" style="position: relative; margin: 0 auto; top: 0px; left: 0px; width: 1600px; height: 600px; overflow: hidden;">
                    <!-- Slides Container -->
                    <div class="slides" u="slides" style="position: absolute; left: 0px; top: 0px; width: 1600px; height: 600px; overflow: hidden;">
                        
                        <?
                        $i = 0;
                        foreach($meals as $meal) {
                            $i++;
                        ?>
                        <div>
                            <img u="image" src="<?php echo $meal['image']; ?>" />
                            
                            <div class="shadow" style="height: 100%; width: 100%;">
                                <div id="procook-con" style="width:1024px; margin: auto; height: 100%; position: relative;">
                                    <a href="<?php echo $meal['href']; ?>" >
                                    <div style="height: 180px; width: 220px; position: absolute; bottom: 0; left:30px;">
                                        <div class="s-cook-avatar">
                                            <img class="s-image-avatar" src="<?php echo $meal['cook_image']; ?>">
                                        </div>
                                        <div class="s-cook-name"><?php echo $meal['cook_name'].", ".$meal['cook_city']; ?></div>
                                        <div class="s-meal-name"><?php echo $meal['name']; ?></div>
                                        <div class="s-meal-price"><?php echo $meal['price']; ?></div>
                                    </div>
                                    </a>
                                </div>
                            </div>
                            
                        </div>
                        <?php } ?>
                        
                    </div>
                    
                    <!-- Bullets Navigator -->
                    <div u="navigator" class="jssorb21" style="position: absolute; bottom: 26px; left: 6px;">
                        <div u="prototype" style="POSITION: absolute; WIDTH: 19px; HEIGHT: 19px; text-align:center; line-height:19px; color:White; font-size:12px;"></div>
                    </div>
                    <!-- Arrow Navigator -->
                    <span u="arrowleft" class="jssora21l" style="width: 55px; height: 55px; top: 350px; left: 8px;">
                    </span>
                    <span u="arrowright" class="jssora21r" style="width: 55px; height: 55px; top: 350px; right: 8px">
                    </span>
                    <!-- Arrow Navigator Skin End -->
                    <a style="display: none" href="http://www.jssor.com">jQuery Slider</a>
                </div>
        </div>
        
        <div class="back-imgae">
            <img src="/image/data/homepage/homepagestake.png">
        </div>
        
        <div id="search-controller-con">
            <div id="search-controller">
                
                <ul class="search-tabs">
                    <li id="order-in" class="active">
                        <a href="#">
                        הזמנת משלוחים
                        </a>
                    </li>
                    <li id="take-away">
                        <a href="#">
                        איסוף עצמי
                        </a>
                    </li>
                </ul>
                
                <div class="order-in-tab search-tab active">
                    <span>
                        לאן תרצו להזמין משלוח?
                    </span>
                    
                    <div id="controller">
                        <input type="text" name="search" id="hp-search-input" placeholder="למשל : תל אביב"  onfocus="this.placeholder = ''" onblur="this.placeholder = $('#input-search-homeals').attr('placeholder') " />
                        <button id="hp-search-btn">מצא</button>
                    </div>
                </div>
                
                <div class="take-out-tab search-tab">
                    <span>
                        לאן תרצו להזמין משלוח?
                    </span>
                    
                    <div id="controller">
<div class="under-image-panel">
    <div class="concenter1024">
		<div class="table height100">
			<form action="#" class="rest-filter-form" id="rest-filter-form" >
				
				<div class="rest-filter" >
				<span class="filter-name"><?php echo $filter_kitchen ?></span>
				<select name="filter-kitchen" id="filter-kitchen" class="chosen" >
					<option value="1"><?php echo $filter_kitchen_placeholder ?> 1 </option>
					<option value="2"><?php echo $filter_kitchen_placeholder ?> 2 </option>
					<option value="3"><?php echo $filter_kitchen_placeholder ?> 3 </option>
					<option value="4"><?php echo $filter_kitchen_placeholder ?> 4 </option>
					<option value="5"><?php echo $filter_kitchen_placeholder ?> 5 </option>
					<option value="6"><?php echo $filter_kitchen_placeholder ?> 6 </option>
				</select>
				</div>
				<div class="rest-filter" >
				<span class="filter-name"><?php echo $filter_by_cost ?></span>
				<select name="filter-cost" id="filter-cost" class="chosen" >
					<option value="1"><?php echo $filter_by_cost_placeholder ?> 1 </option>
					<option value="2"><?php echo $filter_by_cost_placeholder ?> 2 </option>
					<option value="3"><?php echo $filter_by_cost_placeholder ?> 3 </option>
					<option value="4"><?php echo $filter_by_cost_placeholder ?> 4 </option>
					<option value="5"><?php echo $filter_by_cost_placeholder ?> 5 </option>
					<option value="6"><?php echo $filter_by_cost_placeholder ?> 6 </option>
				</select>
				</div>
				<div class="rest-filter" >
				<span class="filter-name"><?php echo $filter_by_kosher ?></span>
				<select name="filter-kosher" id="filter-kosher" class="chosen" >
					<option value="1"><?php echo $filter_by_kosher_placeholder ?> 1 </option>
					<option value="2"><?php echo $filter_by_kosher_placeholder ?> 2 </option>
					<option value="3"><?php echo $filter_by_kosher_placeholder ?> 3 </option>
					<option value="4"><?php echo $filter_by_kosher_placeholder ?> 4 </option>
					<option value="5"><?php echo $filter_by_kosher_placeholder ?> 5 </option>
					<option value="6"><?php echo $filter_by_kosher_placeholder ?> 6 </option>
				</select>
				</div>
				<div class="rest-filter-button" >
				<input type="submit" id="rest-filter-submit" value="<?php echo $find_order ?>" >
				</div>
				
			</form>
		</div>
	
    </div>
</div>
                    </div>
                </div>
                
            </div><!-- search-controller end -->
        </div><!-- search-controller-con end -->
    </div>
    
    <div id="home-content">
        <div class="concenter1024">
            <h1 class="restaurant-list-title">
                רשימת מסעדות נבחרות
            </h1>
            
            <div class="food-style-list">
                <ul class="food-cat-menu" >
                
                <?php foreach($cat as $category){ ?>
                    <li>
                        <a href="<?php echo 'index.php?route=product/quickyCategory&cat=' . $category['option_value_id'] ?>" class="home-cat-link"><?php echo $category['name']; ?></a>
                    </li>
                <?php } ?>
                    <li>
                       <a href="index.php?route=product/quickyCategory"  class="all-cat-link"><?php echo 'לרשימה המלאה'; ?></a>
                       
                    </li>
                </ul>
            </div>
            
            <div class="restaurant-list clear" >
                <?php foreach($restaruants as $rest) { ?>
                <!-- get the restaurent -->
                <a href="<?php echo $rest['href'] ?>" alt="<?php echo $rest['name'] ?>" data-start="<?php echo $rest['start'] ?>" data-close="<?php echo $rest['close'] ?>">
                <div class="rest-col">
                    <div class="rest-image">
                        <!--<span class="helper"></span>-->
                        <img src="<?php echo $rest['image'] ?>" >
                    </div>
                    <div class="rest-info">
                        <ul class="food-cat-menu short-rest" >
                            <li><?php echo $rest['name'] ?></li>
                            <li>|</li>
                            <li><?php echo $rest['sp'] ?></li>
                        </ul>
                    </div>
                </div>
                </a>
                <?php } ?>
                
            </div>
            
            <div class="food-style-list">
                <ul class="food-cat-menu" >
                
                <?php foreach($cat as $category){ ?>
                    <li>
                        <a href="<?php// echo $cat['href'] ?>" class="home-cat-link"><?php echo $category['name']; ?></a>
                    </li>
                <?php } ?>
                    <li>
                        <a href="index.php?route=product/category&path=0"  class="all-cat-link"><?php echo 'לרשימה המלאה'; ?></a>
                    </li>
                </ul>
            </div>
            
        </div>
    </div>
    
    
</div>

<script>
    $(document).ready(function ($) {
        
        // auto-complete
        var ajx_options = { serviceUrl:'system/helper/autocomplete.php',
			// callback function:
			onSelect: function(data){
				/*
				if (data.value != $("#input-search-homeals").val()) {
                                        $(".loading-screen").show();
					window.location = "index.php?route=product/category&path=0&filter_place="+data.value;
				}
                                */
			}
		};
                
	$("#hp-search-input").autocomplete(ajx_options);
        
        // slider code
        
        /*
        var options = {
                $FillMode: 2,                                       //[Optional] The way to fill image in slide, 0 stretch, 1 contain (keep aspect ratio and put all inside slide), 2 cover (keep aspect ratio and cover whole slide), 4 actuall size, default value is 0
                $AutoPlay: true,                                    //[Optional] Whether to auto play, to enable slideshow, this option must be set to true, default value is false
                $AutoPlayInterval: 4000,                            //[Optional] Interval (in milliseconds) to go for next slide since the previous stopped if the slider is auto playing, default value is 3000
                $PauseOnHover: 3,                                   //[Optional] Whether to pause when mouse over if a slider is auto playing, 0 no pause, 1 pause for desktop, 2 pause for touch device, 3 pause for desktop and touch device, default value is 3

                $ArrowKeyNavigation: true,   			    //[Optional] Allows keyboard (arrow key) navigation or not, default value is false
                $SlideEasing: $JssorEasing$.$EaseOutQuart,          //[Optional] Specifies easing for right to left animation, default value is $JssorEasing$.$EaseOutQuad
                $SlideDuration: 1200,                               //[Optional] Specifies default duration (swipe) for slide in milliseconds, default value is 500
                $MinDragOffsetToSlide: 20,                          //[Optional] Minimum drag offset to trigger slide , default value is 20
                $SlideSpacing: 0, 			            //[Optional] Space between each slide in pixels, default value is 0
                $DisplayPieces: 1,                                  //[Optional] Number of pieces to display (the slideshow would be disabled if the value is set to greater than 1), the default value is 1
                $ParkingPosition: 0,                                //[Optional] The offset position to park slide (this options applys only when slideshow disabled), default value is 0.
                $UISearchMode: 1,                                   //[Optional] The way (0 parellel, 1 recursive, default value is 1) to search UI components (slides container, loading screen, navigator container, arrow navigator container, thumbnail navigator container etc).
                $PlayOrientation: 1,                                //[Optional] Orientation to play slide (for auto play, navigation), 1 horizental, 2 vertical, default value is 1
                $DragOrientation: 1,                                //[Optional] Orientation to drag slide, 0 no drag, 1 horizental, 2 vertical, 3 either, default value is 1 (Note that the $DragOrientation should be the same as $PlayOrientation when $DisplayPieces is greater than 1, or parking position is not 0)


                $BulletNavigatorOptions: {                          //[Optional] Options to specify and enable navigator or not
                    $Class: $JssorBulletNavigator$,                 //[Required] Class to create navigator instance
                    $ChanceToShow: 2,                               //[Required] 0 Never, 1 Mouse Over, 2 Always
                    $AutoCenter: 1,                                 //[Optional] Auto center navigator in parent container, 0 None, 1 Horizontal, 2 Vertical, 3 Both, default value is 0
                    $SpacingX: 8,                                   //[Optional] Horizontal space between each item in pixel, default value is 0
                    $SpacingY: 8,                                   //[Optional] Vertical space between each item in pixel, default value is 0
                    $Orientation: 1                                 //[Optional] The orientation of the navigator, 1 horizontal, 2 vertical, default value is 1
                },

                $ArrowNavigatorOptions: {                           //[Optional] Options to specify and enable arrow navigator or not
                    $Class: $JssorArrowNavigator$,                  //[Requried] Class to create arrow navigator instance
                    $ChanceToShow: 1,                               //[Required] 0 Never, 1 Mouse Over, 2 Always
                    $AutoCenter: 2,                                 //[Optional] Auto center arrows in parent container, 0 No, 1 Horizontal, 2 Vertical, 3 Both, default value is 0
                    $Steps: 1                                       //[Optional] Steps to go for each navigation request, default value is 1
                }
            };
            var jssor_slider1 = new $JssorSlider$("slider1_container", options);

            ////responsive code begin
            ////you can remove responsive code if you don't want the slider scales while window resizes
            function ScaleSlider() {
                var bodyWidth = document.body.clientWidth;
                if (bodyWidth)
                    jssor_slider1.$SetScaleWidth(Math.min(bodyWidth, 1920));
                else
                    window.setTimeout(ScaleSlider, 30);
            }
            
            ScaleSlider();
            
            if (!navigator.userAgent.match(/(iPhone|iPod|iPad|BlackBerry|IEMobile)/)) {
                $(window).bind('resize', ScaleSlider);
            }
            ////responsive code end
            */
            
    });
    
    $('#order-in , #take-away').on( "click" , function(e) {
        e.preventDefault()
        
        $(this).addClass("active");
        $(this).siblings().removeClass("active");
    });

    $(window).load(function() {
        $(".loading-screen").fadeOut("slow");
        $(".go-back").hide(); // from responsive mobile view
    });
    
    /********************************
    *	FOR RESPONSIVE DESIGN - footer (there is also in common)
    *********************************/
    
    if(/Android|webOS|iPhone|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
        //
    }
    
   /********************************
    *	FOR RESPONSIVE DESIGN END
    *********************************/
    
</script>

<!-- calculat if the restaurant is open -->
<script>
var time = new Date();
var hour = time.getHours();
var minuts = time.getMinutes();

var rest = $('div.restaurant-list').children();
$.each( rest, function( key, value ) {
    
    start = $(value).attr("data-start");
    close = $(value).attr("data-close");

    if((!close) && (!start)){
    $(value).removeAttr( "href" );
    $(value).css('cursor', 'default');
     $(value).children().prepend('<div class="time-note">המסעדה איננה פתוחה היום</div>');
    }else{
        
        new_start = start.split(":");
        new_close = close.split(":");
        start_hour =  parseInt(new_start[0]);
        start_minuts = parseInt(new_start[1]);
        close_hour = parseInt(new_close[0]);
        close_minuts = parseInt(new_close[1]);
        if(start_hour > hour){
            
            if(start_minuts < 10){
                 start_minuts = '0' + start_minuts;
                }
            
          $(value).children().prepend('<div class="time-note">ניתן להזמין מהשעה ' + start_hour + ':' + start_minuts +'</div>');
            
        }else if((start_hour == hour) && ( start_minuts < minuts)){
            
                 if(start_minuts < 10){
                 start_minuts = '0' + start_minuts;
                }
        
         $(value).children().prepend('<div class="time-note">ניתן להזמין מהשעה ' + start_hour + ':' + start_minuts +'</div>');
        }else if(close_hour < hour){
            
             if(start_minuts < 10){
                 start_minuts = '0' + start_minuts;
                }
             $(value).children().prepend('<div class="time-note">ניתן להזמין מהשעה ' + start_hour + ':' + start_minuts +'</div>');
        
        }else if((close_hour == hour) && (minuts > close_minuts)){
            
            if(start_minuts < 10){
                 start_minuts = '0' + start_minuts;
                }
         $(value).children().prepend('<div class="time-note">ניתן להזמין מהשעה ' + start_hour + ':' + start_minuts +'</div>');
        }
    }

});
</script>

<?php echo $footer; ?>