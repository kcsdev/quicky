<?php //echo $homealscheckout; ?>

</div><!-- content inner div end -->
</section>
<!-- section code end -->

<!-- קוד Google לתג שיווק מחדש -->
<!--------------------------------------------------
אין לשייך תגי שיווק מחדש עם מידע המאפשר זיהוי אישי ואין להציב אותם בדפים הקשורים לקטגוריות רגישות. ראה מידע נוסף והוראות על התקנת התג ב: http://google.com/ads/remarketingsetup
--------------------------------------------------->
<script type="text/javascript">
/* <![CDATA[ */
var google_conversion_id = 960498678;
var google_custom_params = window.google_tag_params;
var google_remarketing_only = true;
/* ]]> */
</script>
<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
</script>
<noscript>
<div style="display:inline;">
<img height="1" width="1" style="border-style:none;" alt="" src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/960498678/?value=0&amp;guid=ON&amp;script=0"/>
</div>
</noscript>
<!--
<div style="display: none;">
	
	<div id="join-newsletter">
		<div class="close-btn" onclick="$.colorbox.close();"></div>
		<div id="homepage-footer">
			<div id="footer-newsletter">
				<div class="black-filter">
				<div id="newsletter-title">הירשמו וקבלו הצעות לארוחות</div>
				<div id="newsletter-text">הצטרפו לרשימת התפוצה שלנו וקבלו עידכונים והצעות <span class="mobilebr"><br></span>לארוחות טעימות מדי יום.</div>
				<div id="newsletter-con">
					<input id="newsletter-firstname" name="firstname" type="text" placeholder="שם פרטי" />
					<input id="newsletter-lastname" name="lastname" type="text" placeholder="שם משפחה" />
					<input id="newsletter-email" name="email" type="text" placeholder="דואר אלקטרוני (חובה)" />
					<input id="newsletter-city" name="city" type="text" placeholder="עיר למשלוחים" />
					<input id="newsletter-method" name="method" type="hidden" value="popup box signup" />
					<button id="newsletter-send">הירשם</button>
				</div>
				</div>
				<div class="real-filter">
				</div>
			</div>
			
		</div>
	</div>
	
</div>-->

<footer id="footer-container">
	<div id="homepage-footer">
		
        <div class="concenter1024">
			<div class="footer-inner">
				<div id="footer">
					<?php if ($informations) { ?>
					<div class="column">
						<h3><?php echo $text_information; ?></h3>
						<ul>
						<?php foreach ($informations as $information) { ?>
							<li><a href="<?php echo $information['href']; ?>"><?php echo $information['title']; ?></a></li>
						<?php } ?>
						</ul>
					</div>
					<?php } ?>
					<div class="custom_footer_main">
						  <div class="custom_footer_inner">
							  <?php echo $content_footer; ?>
						  </div>
					</div>
					<div class="column">
						<h3><?php echo $text_service; ?></h3>
						<ul>
							<li><a href="<?php echo $contact; ?>"><?php echo $text_contact; ?></a></li>
							<li><a href="<?php echo $return; ?>"><?php echo $text_return; ?></a></li>
							<li><a href="<?php echo $sitemap; ?>"><?php echo $text_sitemap; ?></a></li>
						</ul>
					</div>
					<div class="column">
						<h3><?php echo $text_extra; ?></h3>
						<ul>
							<li><a href="<?php echo $manufacturer; ?>"><?php echo $text_manufacturer; ?></a></li>
							<li><a href="<?php echo $voucher; ?>"><?php echo $text_voucher; ?></a></li>
							<li><a href="<?php echo $affiliate; ?>"><?php echo $text_affiliate; ?></a></li>
							<li><a href="<?php echo $special; ?>"><?php echo $text_special; ?></a></li>
						</ul>
					</div>
					<div class="column">
						<h3><?php echo $text_account; ?></h3>
						<ul>
							<li><a href="<?php echo $account; ?>"><?php echo $text_account; ?></a></li>
							<li><a href="<?php echo $order; ?>"><?php echo $text_order; ?></a></li>
							<li><a href="<?php echo $wishlist; ?>"><?php echo $text_wishlist; ?></a></li>
							<li><a href="<?php echo $newsletter; ?>"><?php echo $text_newsletter; ?></a></li>
						</ul>
					</div>
				</div>
			</div><!-- footer-inner end -->
			
			<!--
            <div id="footer-main">
                <div id="footer-main-logo">
                    <img src="image/data/greylogo.png" />
                </div>
				<div style="clear: both"></div>
                <div id="footer-main-links">
                    <ul>
                        <?php foreach ($informations as $information) { ?>
						<li><a href="<?php echo $information['href']; ?>" target="_blank"><?php echo $information['title']; ?></a></li>
						<?php } ?>
						<?php if (false) { ?>
						<li><a href="index.php?route=product/roshhashana" target="_blank">לתפריט ראש השנה</a></li>
						<?php } ?>
                    </ul>
					<div style="clear: both"></div>
				</div>
				<div style="clear: both"></div>
				<div id="footer-main-followus">
					<div id="followus-title">עקבו אחרינו</div>
					<div id="followus-body">
						<a href="https://www.facebook.com/pages/Homeals-Israel/608776639200255" target="blank" >
							<div id="facebook-follow"></div>
						</a>
						<a href="http://instagram.com/homealsisrael" target="blank" >
							<div id="insta-fllow"></div>
						</a>
						<a href="http://saloona.co.il/homeals/" target="blank" >
							<div id="saloona-fllow"></div>
						</a>
						<div style="clear: both"></div>
					</div>
					<div class="fb-like" data-href="https://www.facebook.com/HOMEALS" data-width="300px" data-layout="standard" data-action="like" data-show-faces="true" data-share="true"></div>
				</div>
				<div style="clear: both"></div>
			</div><!-- footer-main end -->
			
			
			<div style="clear: both"></div>
			
		</div><!-- concenter1024 end -->
		
        <div id="footer-epilog">
            <div class="concenter1024">
                <div class="right">
                    <span>
		 כל הזכויות שמורות ל- Quiky
					</span>
					<span style="display: inline-block;">
		 © 2014
					</span>   
                    
                </div>
                <div class="center">&nbsp;|&nbsp;</div>
                <div class="left">
					<span id="epilog-links">
						<a href="index.php?route=information/information&information_id=4"  style="display: none" target="_blank">אודות</a>
						<a href="index.php?route=information/information&information_id=5"  target="_blank">תנאי שימוש</a>
						<a href="index.php?route=information/information&information_id=3"  target="_blank">מדיניות פרטיות</a>
					</span>   
                </div>
            </div>
        </div><!-- footer-epilog end -->
		
    </div><!-- homepage-footer end -->
</footer>


<div id="fb-root"></div>

</div>

<span class="grid_default_width" style="display: none; visibility: hidden;" ></span>
<span class="home_products_default_width" style="display:none; visibility:hidden"></span>
<span class="module_default_width" style="display:none; visibility:hidden"></span>

<!-- place holder -->
<div id="button-show-cart"></div>

<script>
	
<?php if ($this->cart->hasProducts())  { ?>
    var addtocart_error = false;
<?php } else { ?>
    var addtocart_error = true;
<?php } ?>
	
	var addtocart_day_error = false;
	var add_to_cart = false;
	var open_review_box = false;
	var open_pm = false;
	var added_now = false;
	
$(document).ready(function() {
	
	//$("#signinpass , #loginpass").placeholder();
	
	
	/********************************
	 *	FOR INTERNET EXPLORER
	 *********************************/
	if( $.browser.msie ) {
		
		$('#loginpass').show();
		$('#loginpass').on("blur", function() {
			$('#loginpass').show();
		});
		
	} 
	
	/********************************
	 *	FOR RESPONSIVE DESIGN - footer (there is also in common)
	 *********************************/
	
	if(/Android|webOS|iPhone|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
		
		$("#logo-homeals img,#hp-header-logo img").attr("src","catalog/view/theme/<?php echo $this->config->get('config_template') ?>/image/homeals/mobile_logo.png");
		$("#logo-homeals img,#hp-header-logo img").show();
		
		$("#slider-con").remove();
		
		$("#supercheckout-columnleft").append($("#columnleft-2"));
		$("#supercheckout-columnleft").append($("#columnleft-1"));
		
		$("#hp-header-logo").append("<div class='open-menu menu-left push-body' ><div></div></div>");
		
		$(".header-homeals>div:first-child").append("<div class='open-menu menu-left push-body' ><div></div></div>");
		
		$('.open-menu').jPushMenu();
		
		$(".header-homeals>div:first-child , #hp-header-logo").append("<div class='go-back' onclick='showLoad();goBack()'><i class='fa fa-angle-right'></i></div>");
		
		$('#container').on("click", function () {
			$('.jPushMenuBtn,body,.cbp-spmenu , #hp-header-con').removeClass('disabled active cbp-spmenu-open cbp-spmenu-push-toleft cbp-spmenu-push-toright');
		});
		
		$("#nav_open_login").on("click", function(e){
			$('.jPushMenuBtn,body,.cbp-spmenu , #hp-header-con').removeClass('disabled active cbp-spmenu-open cbp-spmenu-push-toleft cbp-spmenu-push-toright');
			$("#open_login").colorbox({inline:true});
			$("#open_login").trigger("click");
		});
		
		$("#nav_open_signin").on("click", function(e){
			$('.jPushMenuBtn,body,.cbp-spmenu , #hp-header-con').removeClass('disabled active cbp-spmenu-open cbp-spmenu-push-toleft cbp-spmenu-push-toright');
			$("#open_signin").colorbox({inline:true});
			$("#open_signin").trigger("click");
		});
		
		$(".side-swiper-container").show();
		
		$("#epilog-links").find($("a")).show();
		$("#footer-main-links").html($("#epilog-links"));
		$("#epilog-links").show();
		
		$(".a_menu_needhelp").on("click", function(e){
			$("#menu-needhelp").slideToggle(200);
		});
		
		$("#menu-user").on("click", function(e){
			$("#menu-user-menu").slideToggle(200);
		});
		
		$("#nav-log-me-out").on("click", function(e){
			$("#log-me-out").trigger("click");
		});
		
		var mySwiper = $('.swiper-container').swiper({
					slidesPerView: 'auto',
					mode:'horizontal',
					loop: true
				      });
		
		var sideSwiper = $('.side-swiper-container').swiper({
					slidesPerView: 'auto',
					mode:'horizontal',
					loop: false
				      });
		
		$("div.left.product-image span.image").after('<div class="add-meal-con"><div id="add-meal-btn">הוסף להזמנה</div></div>');
		
		//no facebook like for mobile
		$(".fb-like").remove();
		
		$("iframe[name=\"google_conversion_frame\"]").css("display","none");
		
		$( ".more-meals-cook" ).after( $( "#success-menu-span-cook" ) );
		$( ".more-meals-cat" ).after( $( "#success-menu-span-cat" ) );
		
	} else {
		// only on non mobile browser
		<?php
		if (isset($_GET['route']) && ( $_GET['route'] == 'product/product' || $_GET['route'] == 'seller/catalog-seller/profile' ) ) {
			if(isset($this->session->data['nl_seen_page'])){
				$this->session->data['nl_seen_page'] = $this->session->data['nl_seen_page'] + 1;
			} else {
				$this->session->data['nl_seen_page'] = 1;
			}
		
		if(!$this->customer->isLogged() && $this->session->data['nl_seen_page'] ==  2) {
		?>
		/*$(window).load(function() {
				
			setTimeout(function(){
				if ($("#colorbox").css("display") == "none") {
					$.colorbox({href:"#join-newsletter",inline:true, open: true
						,onOpen:function(){
							$("body").css("height",$(window).height());
							$("body").css("overflow","hidden");
						}
						,onComplete:function(){
							
						}
						,onClosed:function(){
							$("body").css("height","100%");
							$("body").css("overflow","visible");
						}
					});
				}	
				},10000);
		});*/
		<?php } } ?>
		

	<?php
	if(false){
		if(isset($this->session->data['roshhashana_seen_page'])){
			$this->session->data['roshhashana_seen_page'] = 2;
		} else {
			$this->session->data['roshhashana_seen_page'] = 1;
		}
		
		if(false && $this->session->data['roshhashana_seen_page'] ==  1) {
		?>
		$(window).load(function() {
				
			setTimeout(function(){
			
				if ($("#colorbox").css("display") == "none") {
					$.colorbox({href:"#roshhashana",inline:true, open: true
						,onOpen:function(){
							$("body").css("height",$(window).height());
							$("body").css("overflow","hidden");
						}
						,onComplete:function(){
							
						}
						,onClosed:function(){
							$("body").css("height","100%");
							$("body").css("overflow","visible");
						}
					});
				}
				
				},500);
		});
		<?php } ?>
	<?php } ?>
		
	}
	
	/********************************
	 *	FOR RESPONSIVE DESIGN END
	 *********************************/
	
	var towpointtwo = false;
	
	<?php if($review_this_meal_id){ ?>
	// need to change the button from this #open_checkout to open this colorbox and the button there to open checkout
	
	<?php if(isset($_GET['opencart'])){ ?>
		towpointtwo = true;
	<?php } ?>
	
	if ($("#colorbox").css("display") != "none") {
		towpointtwo = true;
	}
	
	if (!towpointtwo) {
		// after 7 seconds
		setTimeout(function(){openReviewMeal()},7000);
	} else {
		$("#open_checkout_review").live("click", function() {
			openReviewMeal();
		});
	}
	
	function openReviewMeal(){
		// ajax add cookie once
		var review_href = 'index.php?route=product/product/review_ajax&product_id=<?php echo $review_this_meal_id; ?>';
		$.colorbox({href:review_href, open: true
					,onOpen:function(){
						$("body").css("height",$(window).height());
						$("body").css("overflow","hidden");
					}
					,onComplete:function(){
						
						if (towpointtwo) {
							$("#cancel-review").html("המשך לתשלום ללא דירוג");
							$("#submit-review").html("אשר והמשך לתשלום");
							$("#submit-review").addClass("homeals-button-continue");
							
						}
						
						$(".star-rating").barrating('show', {
							showSelectedRating:false,
							showValues:false,
							showSelectedRating:true
						});
						
						$(".side-select-ajax").on("change",function(){
							var side_id = $(this).val();
							$(this).parent().parent().css("display","none");
							$("#ajax_side_"+side_id).css("display","block");
						});
						
						$("#cancel-review").on("click",function(){
							$.colorbox.close();
							
							if (towpointtwo) {
								setTimeout(function(){$("#open_checkout").trigger("click");},500);
							}
							
						});
							
						$("#submit-review").on("click",function(){
							
							if (!$("#review-box-ajax").find("select").first().val()) {
								alert("יש להזין דירוג");
								return false;
							}
							
							var star = {};
							var text = {};
							
							var i = 0;
							
							$("#review-box-ajax").find("select").each(function() {
								if ($(this).val()) {
									star[$(this).attr("id")] = $(this).val();
								}
							});
							
							$("#review-box-ajax").find("textarea").each(function() {
								text[$(this).attr("id")] = $(this).val();
								text_data = $(this).val();
							});
							
							var data = {
								"stars" : star,
								"texts" : text
							}
							
							$.ajax({
								url: 'index.php?route=product/product/writemulti',
								type: 'post',
								dataType: 'json',
								data: data,
								beforeSend: function() {
									$('#submit-review').attr('disabled', true);
									showLoadBox();
								},
								complete: function() {
									$('#submit-review').attr('disabled', false);
									hideLoadBox();
								},
								success: function(data) {
									if (data['error']) {
										//$('#review-title').after('<div class="warning">' + data['error'] + '</div>');
									}
									
									if (data['success']) {
										//showLoadPage();
										$.colorbox.close();
										
										if (towpointtwo) {
											setTimeout(function(){$("#open_checkout").trigger("click");},500);
										}
										
									}
								}
							});
						});
					}
					,onClosed:function(){
						$("body").css("height","100%");
						$("body").css("overflow","visible");
					}
			});
	}
	<?php } ?>
	
	
	$('#time-of-day').on("change", function() {
		addtocart_error = false;
		$(this).parent().css("border","1px solid #e7e3d9");
	});
	
	$('#day').on("change", function() {
		addtocart_day_error = false;
		addtocart_error = true;
		$(this).parent().css("border","1px solid #e7e3d9");
	});

	$('#button-show-cart').live('click', function() {
		$.ajax({
			url: 'index.php?route=checkout/cart/show',
			type: 'post',
			dataType: 'json',
			success: function(json) {
				if (json['success']) {
					var recent_items = json['recent_items'];
					
					if( $.browser.msie ) {
						//for IE
						var item_one = recent_items[0];
						if (typeof recent_items[1] != "undefined") {
							var item_two = recent_items[1];
						} else {
							var item_two = "";
						}
						
					} else {
						var item_one = recent_items[ Object.keys(recent_items).sort().pop() ];
						
						if (typeof recent_items[ Object.keys(recent_items).sort().pop() -1 ] != "undefined") {
							var item_two = recent_items[ Object.keys(recent_items).sort().pop() -1 ];
						} else {
							var item_two = "";
						}
					}
					
					$('#notification').addClass("isDown");
					
					var open_checkout = '<a href="#mealcheckout" id="open_checkout"><?php echo $text_checkout ?></a>';
					
					if (towpointtwo) {
						open_checkout = '<a id="open_checkout_review"><?php echo $text_checkout ?></a><a href="#mealcheckout" id="open_checkout" style="display:none;">open</a>';
					}
                    <?php if(isset($this->session->data['pay'])){ ?>
					var price = "<?php echo $this->session->data['pay'] ?>";
                      <?php } ?>
                    var price_int = parseFloat(price) + json['total_cash'];
                    var qty_class = "quant";
                    var span_class = 'style="display:none !important"';
					var html_src ='	<div class="cart-con">';
                    html_src += '<div id="close_not">סגור</div>';
					html_src +='		<div id="cart-header">';
					html_src +='			<span>';
					html_src +='				יש לך	';
					html_src +='				<span class="latge-number">' + json['total_number'] + '</span>';
					html_src +='				מנות בצלחת	';
					html_src +='			</span>';
                    html_src +='     <img class="cart-image" src="images/cart-image.png">';
					html_src +='		</div>';
					
					html_src +='		<div class="cart-body">';
                    <?php if(isset($rest)){ ?>
                    html_src +="         <div class=" + "side-rest-name" +"> הזמנה מ-<?=$rest?></div>"
                     <?php } ?>
					html_src +='			<table class="cart-table">';
					html_src +='				<thead>';
					html_src +='					<tr>';
					html_src +='						<td>שם המנה</td>';
					html_src +='						<td>כמות</td>';
					html_src +='						<td>מחיר</td>';
					html_src +='					</tr>';
					html_src +='				</thead>';
					html_src +='				<tbody>';
                                                    <?php foreach($this->cart->getProducts() as $key =>$value){ ?>
					html_src +='					<tr>';
					html_src +="						<td><?= $value['name'] ?><span " + span_class +"><?= $key ?></span></td>";
					html_src +='						<td>';
					html_src +='							<div class="quant-controller">';
					html_src +='								<div class="plus">+</div>';
					html_src +="								<div class=" + qty_class +"><?= $value['quantity'] ?></div>";
					html_src +='								<div class="minus">-</div>';
					html_src +='							</div>';
					html_src +='						</td>';
					html_src +="						<td><?= $value['total'] ?> שח</td>";
					html_src +='					</tr>';
                                                     <?php } ?>                                                           
					html_src +='				</tbody>';
					html_src +='			</table>';
					html_src +='		</div>';
					
					html_src +='		<div class="cart-summary">';
					html_src +='			<div class="total shipping">';
					html_src +='				<span>דמי משלוח</span>';
                    <?php if(isset($this->session->data['pay'])){ ?>
					html_src +="				<span><?php echo  $ils . $this->session->data['pay'] ?></span>";
                        <?php } ?>
					html_src +='			</div>';
					html_src +='			<div class="total overall">';
					html_src +='				<span>סה״כ לתשלום</span>';
					html_src +='				<span>' + price_int + ' <?php echo $ils ?></span>';
					html_src +='			</div>';
					html_src +='		</div>';
					
					html_src +='		<div class="cart-more">';
					html_src +='			<input type="checkbox" id="pick-up" name="pick-up">';
					html_src +='			<span>איסוף עצמי</span>';
					html_src +='		</div>';
					
					html_src +='		<div class="cart-button">';
					html_src +='			<button id="go-to-checkout">קדימה אוכל!</button>';
					html_src +='		</div>';
					html_src +='	</div><!-- cart-con end -->';
					
					$('#notification').html(html_src);
					
					<?php if(isset($_GET['opencart'])) { ?>
					
					added_now = true;
					
					<?php } ?>
					
					if (added_now) {
						//$('.closedcart').fadeIn('slow');
						$('.success').fadeIn('slow');
						
						$('#notification').fadeIn(10, function() {
							$('.closedcart').trigger("click");
						});
					} else {
						/*
						$('.closedcart').fadeIn('slow');
						*/
						$('.success').fadeIn('slow');
						$('#notification').fadeIn('slow');
					}
					
					$('#cart-total').html(json['total']);
					
					$('input[type="checkbox"]').tmMark();
				}	
			}
		});	
	});
	
	$('#button-cart').live('click', function(e) {
		e.preventDefault();
		
		if (!addtocart_error && !addtocart_day_error) {
			$.ajax({
				url: 'index.php?route=checkout/cart/add',
				type: 'post',
				data: $('.product-info input[type=\'text\'], .product-info input[type=\'hidden\'], .product-info input[type=\'radio\']:checked, .product-info input[type=\'checkbox\']:checked, .product-info select, .product-info textarea'),
				dataType: 'json',
				beforeSend: function() {
					//show loading
					$(".product_under-img-con").prepend('<div class="loading-box" id="meal-box-load"></div>');
					$('#button-cart').attr("disabled", true);
				},
				complete: function() {
					//hide loading
					$("#meal-box-load").remove();
					$('#button-cart').removeAttr("disabled");
				},
				success: function(json) {
					$('.success, .warning, .attention, information, .error').remove();
					
					if (json['error']) {
						if (json['error']['option']) {
							for (i in json['error']['option']) {
								$('#option-' + i).after('<span class="error">' + json['error']['option'][i] + '</span>');
							}
						}
						
						if (json['error']['profile']) {
							$('select[name="profile_id"]').after('<span class="error">' + json['error']['profile'] + '</span>');
						}
						
						// blocks crazy requests
						if (json['error']['no_meal']) {
							alert("אין באפשרותך להוסיף את מוצר זה זמנו עבר או אזל המלאי");
							$('#button-show-cart').trigger("click");
						}
						
						if (json['error']['wrong_date']) {
							alert("תאריך המוצר שהינך מנסה להוסיף אינו תואם את תאריך המוצרים שכבר בעגלה, כדי להוסיף אנא בחר בתאריך המתאים.");
							$('#button-show-cart').trigger("click");
						}
						
						if (json['error']['wrong_time']) {
							alert("זמן המוצר שהינך מנסה להוסיף אינו תואם את זמן המוצרים שכבר בעגלה, כדי להוסיף אנא בחר בזמן המתאים.");
							$('#button-show-cart').trigger("click");
						}
					}
					
					if (json['success']) {
						
						if (add_to_cart) {
							location.reload();
						} else {
							added_now = true;
							$('#button-show-cart').trigger("click");
							//refreshAll();
						}
						
						$("#day").trigger("change");
						
						$("#quantity-input").val(1);
						
						$("#day").attr("disabled", true);
						$("#time-of-day").attr("disabled", true);
						
						$(".edit-box input[name='option[time-of-day]']").val($("#time-of-day").val());
						
						/* $('html, body').animate({ scrollTop: 0 }, 'slow'); */
					}	
				}
			});
		}
		
		
		var alert_msg = "";
		
		if (addtocart_day_error) {
			$("#day").parent().css("border","1px solid #e7604a");
			alert_msg = "בחר יום לביצוע ההזמנה";
		}
		
		if (addtocart_error) {
			$("#time-of-day").parent().css("border","1px solid #e7604a");
			
			if (addtocart_day_error) {
				alert_msg = "בחר יום ושעה לביצוע ההזמנה";
			} else {
				alert_msg = "בחר שעה לביצוע ההזמנה";
			}
		}
		
		if (alert_msg != "") {
			alert(alert_msg);
		}
	});
	
	$('#button-show-cart').trigger("click");
	
	$(".close-btn").on("click", function(){
		$.colorbox.close();
	});
	
	$("#loginbutton").live("click", function(e){
		
		var _email = $("#loginemail").val();
		var _pass = $("#loginpass").val();
		var _remmber_me = $('#rememberme').is(":checked");
		
		if (_email == "" || _pass == "") {
			$(".logbox-warning").css("visibility","visible");
			$("#loginemail").css("border","1px solid #F00");
			$("#loginpass").css("border","1px solid #F00");
            
		}else{
			$(".logbox-warning").css("visibility","hidden");
			$("#loginemail").css("border","1px solid #DDDDDD");
			$("#loginpass").css("border","1px solid #DDDDDD");
			
			$.ajax({
				type: "POST",
				url: "index.php?route=account/login/ajax_validate/",
				data: { email: _email, password: _pass , remmber_me: _remmber_me},
				cache: false,
			       beforeSend: function () {
						$(".loading-screen").show();
					}
				}).done(function( msg ) {
                   
					if (msg == "logedin") {
						console.log(msg);
                        
						var redirect = "<?php echo (isset($this->session->data['redirect']) ? $this->session->data['redirect'] : "" ); ?>";
						var redirect_decoded = redirect.replace(/&amp;/g, '&');
						
						$('#button-login-fb').attr("id","button-cart");
							
						//$('.emptybtn').trigger("click");
						
						if (add_to_cart) {
							
							$.ajax({
							    url: 'index.php?route=checkout/cart/clear',
							    success:function(){
									if ($("#time-of-day").val() != "") {
										$('#button-cart').trigger("click");
									}
								location = location.href + "&opencart";
							    }
							});
							
						} else if (open_review_box) {
							location = location.href + "&review";
						} else  if (open_pm) {
						       location = location.href + "&openpm";
						} else {
							if (!redirect) {
								location.reload(true);
							} else {
								location = redirect_decoded;
							}
						}
						
					} else if(msg == "error_login") {
						$(".loading-screen").hide();
						//console.log('asdasdsa');
						$(".logbox-warning").css("visibility","visible");
						$("#loginemail").css("border","1px solid #F00");
						$("#loginpass").css("border","1px solid #F00");
					}
				});	
		}
		
	});
	
	/* HOME PAGE END */
	
	<?php if(isset($_GET['openpm']) && $this->customer->isLogged()) { ?>
	
	$('#button-pm').trigger("click");
	
	<?php } ?>
	
	<?php if(isset($_GET['open_login']) && !$this->customer->isLogged()) { ?>
	$(window).load(function() {
		$("#open_login").colorbox({inline:true});
		$("#open_login").trigger("click");
	});
	<?php } ?>
	
	<?php if(isset($_GET['open_signin']) && !$this->customer->isLogged()) { ?>
	$(window).load(function() {
		$("#open_signin").colorbox({inline:true});
		$("#open_signin").trigger("click");
	});
	<?php } ?>
	
	function ajax_newsletter(event) {
		
		$("#newsletter-send").attr("id","newsletter-send-off");
		
		event.stopPropagation();
		event.preventDefault();
		
		var _email = $("#newsletter-email").val(),
		    _first = $("#newsletter-firstname").val(),
		    _last = $("#newsletter-lastname").val(),
			_city = $("#newsletter-city").val(),
			_method = $("#newsletter-method").val();
		
		if (!(_email == "")) {
		    $.ajax({
			type: "POST",
			url: "index.php?route=product/product/SubscribeToNewsletter",
			data: { email: _email, first: _first, last: _last ,city: _city, method: _method},
			dataType: 'json',
			beforeSend: function () {
				// show loading
			}
			  }).done(function( msg ) {
				
			    if (msg.success.status != "error") {
					$("#newsletter-con").fadeOut(function() {
						$(this).html("<h1 style='color: #FFFFFF;'>תודה שהצטרפתם לרשימת התפוצה שלנו. הצעות לארוחות טעימות עושות דרכן לתיבת המייל שלכם. בתיאבון.</h1>").fadeIn();
						
						setTimeout(function(){
							$.colorbox.close();
						},2000);
						
					});
			    } else {
					alert(msg.success.error);
					$("#newsletter-send-off").attr("id","newsletter-send");
			    }
		    });
		} else {
			$("#newsletter-send-off").attr("id","newsletter-send");
			$("#newsletter-email").addClass("error");
		}
	}
	
	$("#newsletter-send").live("click", ajax_newsletter);
});
</script>


<script>
$('div#notification').on('click', 'button#go-to-checkout' , function(){

window.location.href='http://54.77.118.39/index.php?route=checkout/cart';


});


</script>

<script>
var old_form = '';

$('a#signinnow').on('click', function(){
old_form = $('div.logbox-logincon form').html();
console.log(old_form);
$('div.fbButton').css('display', 'none');
var form_output = '<br><form action="http://54.77.118.39/index.php?route=account/register" method="post" enctype="multipart/form-data">';
 form_output += '<div>';
 form_output += '<input type="text" name="firstname" placeholder="שם פרטי" class="logboxinput">';
 form_output += '</div>';
 form_output += '<div>';
 form_output += '<input type="text" name="lastname" placeholder="שם משפחה" class="logboxinput">';
 form_output += '</div>';
 form_output += '<div>';
 form_output += '<input type="text" name="email" placeholder="כתובת אימייל" class="logboxinput">';
 form_output += '</div>';
 form_output += '<div>';
 form_output += '<input type="text" name="telephone" placeholder="טלפון" class="logboxinput">';
 form_output += '<input type="hidden" name="fax" value="231123213">';
 form_output += '<input type="hidden" name="language" value="2">';
 form_output += '</div>';
 form_output += '<div>';
 form_output += '<select class="logboxinput" name="city">';
     <?php foreach($city as $value): ?>
     form_output += '<option value="' + "<?php echo  $value['name'];?>" +'">';
                                           
      form_output += "<?php echo  $value['name'];?>";                                     
     form_output += '</option>';                                      
    
    
         <?php endforeach; ?>
 form_output += '</select>'; 
   form_output += '</div>';
 form_output += '<div>';
 form_output += '<input type="text" name="password" placeholder="סיסמא" class="logboxinput">';
 form_output += '</div>';
 form_output += '<div>';
 form_output += '<input type="submit" class="logboxinput" name="loginbutton" id="loginbutton" value="התחבר">';
 form_output += '</div>';
 form_output += '</form>';
 $('div.logbox-logincon').html(form_output);
});
    
/*$('body').on('click','div#cboxOverlay',function(){
$('div.fbButton').css('display', 'block');
 $('div.logbox-logincon').html(old_form);



});*/

    
   
</script>

<script>
$('body').on('click','div#close_not', function(){

var w = $('div#notification').width(); 
var mar = $('div#notification').css('margin-left');
  if(mar == '0px'){

           $('div#notification').css('margin-left', -w);
           $('div#close_not').html('פתח');
      }else{
      
         $('div#notification').css('margin-left', '0px');
           $('div#close_not').html('סגור');
      }
});


</script>

<script>
function refreshurl(){
  if(localStorage['where'] == 'pop'){
  localStorage.removeItem('where');
  location.reload();
  }
}
setInterval(refreshurl, 1000);

</script>
<script>
$('#notification').on('click', 'div.quant-controller div', function(){
var product = $(this).parent().parent().prev().children('span').text();
   

var elem_class = $(this).attr('class');
if(elem_class == 'plus'){
var num_qty = parseInt($(this).parent().children('div.quant').html());

    $(this).parent().children('div.quant').html(num_qty + 1);
    editCart(product, num_qty + 1, elem_class, $(this).parent().parent());
}else if(elem_class == 'minus'){
var num_qty = parseInt($(this).parent().children('div.quant').html());
    
if(num_qty !=0) { 
  $(this).parent().children('div.quant').html(num_qty - 1);
   editCart(product, num_qty - 1, elem_class, $(this).parent().parent());
  }
}

});


//makeFrame();
</script>

</body></html>