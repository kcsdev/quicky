<?php echo $header; ?><?php echo $column_left; ?><?php echo $column_right; ?>

<div id="content" class="order-approved">
	<?php echo $content_top; ?>
	
	<h1><?php echo $approved_text; ?></h1>
	
    <p><?php echo $approved_text_more; ?></p>
    
	<?php echo $content_bottom; ?>
</div>

<?php echo $footer; ?>