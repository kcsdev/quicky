<?php echo $header; ?>

<?php echo $content_top; ?>

<div id="content" class="ms-catalog-seller-profile">
	
	<div class="fourty">
		<div class="rest-info" style="display:none">
			<ul class="rest-info-list">
				<li class="rest-name"><?php echo $seller['name']; ?></li>
				<li class="info_line_1"><?php echo $seller['info_line_1']; ?></li>
				<li class="info_line_2"><?php echo $seller['info_line_2']; ?></li>
				<li class="info_line_3"><?php echo $seller['info_line_3']; ?></li>
			</ul>
		</div>
		<div class="rest-logo">
			<img src="<?php echo $seller['image']; ?>" alt="<?php echo $seller['nickname']; ?>" >
		</div>
	</div>
	
	<div class="sixty">
        <h1><?php echo $seller['name']; ?></h1>
		<div class="rest-description">
			<?php echo $seller['profile_description']; ?>
		</div>
        <?php if(isset($order_val)){ ?>
		<div class="rest-costs">
			<ul>
				<li>
				מינימום הזמנה:
				<?php echo $order_val->min_order ?>
				</li>
				<li>|</li>
				<li>
				דמי משלוח:
				<?php echo $order_val->pay ?> ש״ח
				</li>
				<li>|</li>
				<li>
				זמן הגעה:
				<?php echo ($order_val->time + 20) ?> דק׳
				</li>
			</ul>
		</div>
        <?php } ?>
       <br><br> <div class="phone-header"><?php echo $seller['info_line_3']; ?></div>
	</div>
	
	<hr>
	
	<div class="onethireds">
		<?php echo $column_left; ?>
		<?php echo $column_right; ?>
	</div>
	
	<div class="twothireds">
	
	<div class="pre-menu-info">
		<div class="rest-comments">
			<span class="header">
				<?php echo $rest_comments_text; ?>
			</span>
			<?php echo "חסר רוטב שום."; ?>
		</div>
		<div class="legend">
			<span class="header">
				<?php echo $rest_legend_text; ?>
			</span>
			<?php echo "ללא גלוטן, צמחוני, טבעוני"; ?>
		</div>

	</div>
        
    	<div id="tab-products" >
      <?php if ($seller['products']) { ?>
   <div class="product-grid-list">
  <ul class="product-list" id="product-list-grid">  
    
    <?php if(!empty($menu_cat)){ ?>    
    <?php foreach($menu_cat as $key=>$value){ ?>
        
      <div class="cat-name" id="<?php echo $value->day ?>"><div class="cat-title"><?php echo $value->day ?></div>
        


				<?php foreach ($seller['products'] as $product) { ?>
          
               <?php if(in_array($value->day, $product['cat'])){ ?>
				<li id="pid_<?php echo $product['product_id']; ?>" class="<?php
    if(!empty($product['pro_cat'])){
    foreach($product['pro_cat'] as $cat){
		echo $cat['category_id']." ";
        }
    }
    ?>">

<div class="product-block">
	<div class="product-block-inner">
		
		<div class="image" style="display:none">
		<?php if ($product['thumb']) { ?>
			<img src="image/no_image.jpg" data-src="<?php echo $product['thumb']; ?>" title="<?php echo $product['name']; ?>" alt="<?php echo $product['name']; ?>" />
		<?php } ?>
		</div>
		
		<div class="saleblock">
			<?php if (!$product['special']) { ?>
			<?php } else { ?>
			<span class="saleicon sale">Sale</span>         
			<?php } ?>
		</div>
		
		<div class="under-img-con">
			
			<div class="probox">	
				<div class="namepro">
					<span class="namecon">
					<!--	<a href="<?php echo $product['href']; ?>">-->
                       <a class="aproduct" onclick="addToCart('<?php echo $product['product_id']; ?>');"> 
							<?php echo mb_truncate($product['name'],50); ?>
						</a>
						<br>
						<div class="meal-description">
							<?php echo mb_truncate($product['description'],72); ?>
						</div>
					</span>
					
					<!--<button class="add-to-order" data-product-id="<?php echo $product['product_id']; ?>" >
					<?php echo $add_to_order; ?>
					</button>-->
                    <input type="button" value="הוספה להזמנה" onclick="addToCart('<?php echo $product['product_id']; ?>');" class="button new_order">
					
					<span class="pricecon">
					<?php if (!$product['special']) { ?>
					<?php echo $product['price']; ?>
					<?php } else { ?>
					<!-- <span class="price-old"><?php echo $product['price']; ?></span> --> <span class="price-new"><?php echo $product['special']; ?></span>
					<?php } ?>
					<?php if ($product['tax']) { ?>
					<br />
					<span class="price-tax"><?php echo $text_per_meal; ?> <?php //echo $product['tax']; ?></span>
					<?php } ?>
					</span>
					
					<div class="image">
					<?php if ($product['thumb']) { ?>
						<img src="image/no_image.jpg" data-src="<?php echo $product['thumb']; ?>" class="thumb" title="<?php echo $product['name']; ?>" alt="<?php echo $product['name']; ?>" />
					<?php } ?>
					
					<?php if ($product['thumb']) { ?>
						<img src="<?php echo $product['image_big']; ?>" class="zoom-image" title="<?php echo $product['name']; ?>" alt="<?php echo $product['name']; ?>" />
					<?php } ?>
					</div>
					
				</div>
				
				<div class="rating" style="display:none">
					<?php if($product['reviews'] != 0){ ?>
					<div class="rating-scope" itemscope itemtype="http://schema.org/AggregateRating">
						<img itemprop="ratingValue" src="catalog/view/theme/<?php echo $this->config->get('config_template') ?>/image/stars-<?php echo $product['rating']; ?>.png" />&nbsp;&nbsp;<span itemprop="reviewCount" onclick="$('a[href=\'#tab-review\']').trigger('click');" ><?php echo $product['reviews']; ?> <?php echo $text_reviewsmin; ?></span>&nbsp;<?php echo (($product['reviews_text'] == 0) ? "" : "|"); ?>&nbsp;<a href="<?php echo $product['href']."#reviews_start"; ?>" class="review-write" <?php echo (($product['reviews_text'] == 0) ? "style='display:none'" : ""); ?>><img itemprop="ratingValue" src="catalog/view/theme/<?php echo $this->config->get('config_template') ?>/image/homeals/speachbubble.png" />&nbsp;<?php echo $product['reviews_text']; ?></a>
					</div>
					<?php } else { ?>
					
					<?php } ?>
					
					<div class="last-order-time">
						<?php echo $product['days_available']; ?>
					</div>
				</div>
				
				<div style="clear: both"></div>
			</div>
				
		</div><!-- under-img-con - End -->
	</div>
</div>

	</li>
            
            
     <?php } ?>       
    <?php } ?><!-- end foreach -->
            </div>
    <?php } ?>
        <?php } ?>        
</ul>
			</div>
		<?php } ?>
	</div>
	
	</div>
	<?php echo $content_bottom; ?>
</div>

<!-- choose city pop-up -->
<script>
$(document).ready(function(){
var getUrlParameter = function getUrlParameter(sParam) {
    var sPageURL = decodeURIComponent(window.location.search.substring(1)),
        sURLVariables = sPageURL.split('&'),
        sParameterName,
        i;

    for (i = 0; i < sURLVariables.length; i++) {
        sParameterName = sURLVariables[i].split('=');

        if (sParameterName[0] === sParam) {
            return sParameterName[1] === undefined ? true : sParameterName[1];
        }
    }
};    
    
var profile_id = getUrlParameter('seller_id');    
    
var city = "<?php echo isset($_SESSION['city']) ? $_SESSION['city'] : 0 ?>";
var email = "<?php echo null !== $this->customer->getEmail() ? $this->customer->getEmail() : 0 ?>";
if(city === "0" && email === "0"){
var output = '<h1 style="margin-right: 4%;">עלייך לבחור עיר:</h1>';
output += '<form action="index.php?route=seller/catalog-seller/profile&seller_id=' + profile_id + '" method="post" class="city_form">';
output+= '<label for"city">עיר:</label>&nbsp&nbsp';
output += '<select name="city">';
<?php foreach($cities as $city){ 
$op_city = $city->city;
?>
output += '<option value="<?php echo $op_city ?>"><?php echo $op_city ?></option>';
<?php } ?>
output += '</select><br><br>';
output += '<input type="submit" class="city_button" value="בחר">';
output += '</form>';
$('body').append('<div id="cboxOverlay" style="opacity: 0.9; cursor: pointer; visibility: visible;"></div');
$('body').append('<div id="city-choose">' + output + '</div>');
 }
  
    });
</script>



<script type="text/javascript">
	var image_num = <?php echo isset($image_num) ? $image_num : "0"; ?>;
	
	function nextImage(image_id) {
		if (image_id == 0) {
			next_image =  image_num;
		} else if (image_id > image_num) {
			next_image = 1;
		} else {
			next_image = image_id;
		}
		
		return next_image;
	}
	
	function nextImage2(image_id,image_number) {
	
		if (image_id == 0) {
			next_image = image_number;
		} else if (image_id > image_number) {
			next_image = 1;
		} else {
			next_image = image_id;
		}
		
		return next_image;
	}
	
	$(function(){
		
		/**************************************************
		*
		*	new meal block with moving pictures
		*
		*************************************************/
		$(".side-arrow-right , .side-arrow-left").on("click",function() {
		
		$(this).parent().parent().find('.side-img-con').show();
		$(this).parent().parent().find('.side-text-con').hide();
		
		var image_num =  parseInt($(this).parent().parent().find(".side-img-con").data("num"),10);
		var curr_image_id = parseInt($(this).parent().parent().find(".side-img-con").find(".selected").data("side-id"),10);
		
		if($(this).hasClass("side-arrow-left")){
			var next_image_id = nextImage2(curr_image_id + 1,image_num);
		} else {
			var next_image_id = nextImage2(curr_image_id - 1,image_num);
		}
		
		$(this).parent().parent().find(".side-img.selected").removeClass("selected");
		$(this).parent().parent().find("[data-side-id='"+next_image_id+"']").addClass("selected");
		
		});
		
		// Cycle Side images
		$(".product-block").hover(function(){
		$(this).find('.side-arrows').show();
		
		}, function(){
		$(this).find('.side-arrows').hide();
		$(this).find('.side-img-con').hide();
		$(this).find('.side-text-con').show();
		});
		/*************************************************/
		
		var image_num = <?php echo isset($image_num) ? $image_num : "0"; ?>;
		
		if (image_num == 1) {
			$('.next-image').hide();
			$('.prev-image').hide();
		}
		
		$('#seller-tabs a').tabs();
		
		$('.next-image').bind("click",function(){
			next_image_id = parseInt($(this).parent().attr("id"),10);
			$(this).parent().removeClass("selected");
			next_image = nextImage(next_image_id + 1);
			$(".images").find("#"+next_image).addClass("selected");
		});
		
		$('.prev-image').bind("click",function(){
			next_image_id = parseInt($(this).parent().attr("id"),10);
			$(this).parent().removeClass("selected");
			next_image = nextImage(next_image_id - 1);
			$(".images").find("#"+next_image).addClass("selected");
		});
		
		
		$('.read-more').bind("click",function(e){
			e.preventDefault();			
			$(".next-image").css("display","none");
			$(".images").animate({
				opacity: 1,
				width: "50"
			  }, 300,"swing", function() {
				$('.hide-next-image').show();
			  });
			
			$(".long").show();
			$(".short").hide();
			
			$(".description").animate({
				opacity: 1,
				width: "600"
			  }, 300,"swing", function() {
				$(".description").css("text-align","right");
				$(".description_under").css("padding-left","10px");
				$(".read-more").hide();
				$(".read-less").show();
			  });
		});
		
		$('.read-less').bind("click",function(e){
			e.preventDefault();
			$(".next-image").css("display","block");
			$(".images").animate({
				opacity: 1,
				width: "435"
			  }, 300,"swing", function() {
				$('.hide-next-image').hide();
			  });
			$(".description").animate({
				opacity: 1,
				width: "213"
			  }, 300,"swing", function() {
				$(".description").css("text-align","justify");
				$(".description_under").css("padding-left","0");
				$(".read-less").hide();
				$(".read-more").show();
				$(".short").show();
				$(".long").hide();
			  });
		});
		
		
		
		// pic zoom
		$(".namepro .image").mouseenter(function() {
			$(this).find(".zoom-image").show();
			
			$(document).on('mousemove', function(e){
				$(this).find(".zoom-image").offset({left: e.pageX + 10, top: e.pageY});
			});
		}).mouseleave(function() {
			$(document).off('mousemove');
			$(this).find(".zoom-image").hide();
		});
		

	});
</script>
<!--filter -->
<script>
        
$('li.filter-cat span').on('click',function(){
var cat_id = $(this).parent().data('id');
$('li').removeClass('active');
$(this).parent().addClass('active');    
if(cat_id == 'all'){

$('div.cat-name').css('display', 'block');

}else{

$('div.cat-name').css('display', 'none');
$('div#' + cat_id).css('display', 'block');

}



});
    
 $('.add-to-order').on("click", function() {
     
  			var product_id = $(this).attr("data-product-id");
			var product_href = "index.php?route=product/product/ajax&product_id=";
			
			$.colorbox({ href: product_href+product_id });

 })   

</script>

<script>
var elem = $('ul#product-list-grid').children();
$.each(elem, function(index, value){

  var child = $(value).children('li').length;
  if(child == 0){
       var the_id = $(value).attr('id');
       $.each($('li.filter-cat'), function(){
         var data = $(this).data('id');
         if(data == the_id){
             $(this).addClass( "not_show" );
            }
         });
      $(value).addClass( "not_show" );
     }

});
</script>
<script>
  $(function() {
    var availableTag = [

         <?php foreach ($seller['products'] as $product) { ?>
        <?php echo '"' .  mb_truncate($product['name'],50) . '",' ?>
            <?php } ?>
    ];
    $( 'input[name="quik-search"]' ).autocomplete({
      source: availableTag
    });
  });

</script>

<?php echo $footer; ?>
