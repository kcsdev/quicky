<?php echo $header; ?><?php echo $column_left; ?><?php echo $column_right; ?>

<div id="content" class="ms-account-conversation-view">
	<?php echo $content_top; ?>
	
	<h1 id="conversation-text"><a href="<?php echo $inbox_url; ?>" id="inbox-bread"><?php echo "תיבת דואר"; ?></a> <span id="conversation-title"> > <?php echo $his_name; ?></span></h1>
	
	<div id="conversation-con">
	
	<div id="error_text" class="error"></div>
	
	<?php if (isset($success) && ($success)) { ?>
		<div class="success"><?php echo $success; ?></div>
	<?php } ?>
	
	<form id="ms-message-form" class="ms-form">
		<div class="messages-header" id="ms-message-div"><div class="review"><div class="review-list">
			
			<div class="review-header">
				<div class="avatarcon"><img class="avatar" src="image/no-avatar-women.png" data-src="<?php echo $customer_image; ?>" class="lazy"></div>
				<div class="author"><?php echo $customer_name; ?></div>
			</div>
			<div id="chatzig"></div>
			<div id="message-textarea-con">
				<textarea rows="4" cols="50" name="ms-message-text" id="ms-message-text" placeholder="<?php echo $msg_placeholder."..."; ?>"></textarea>
			</div>
			
			<input type="hidden" name="conversation_id" value="<?php echo $conversation['conversation_id']; ?>" />
			
			<div class="right">
				<div class="action-buttons-con">
					<div class="button msg-submit" id="ms-message-reply" >שלח</div>
				</div>
				<div style="clear: both"></div>
			</div>	
		</div></div></div>
	</form>	
	
	<div class="ms-messages">
		<div class="messages-info"><div class="review">
		<?php if (isset($messages)) { ?>
			<?php foreach ($messages as $message) {
				
				$date_created = strftime($date_format, strtotime($message['date_created']));
				
				?>
			<?php if($message['from'] == $this->customer->getId()){ ?>
			<div class="review-list me">
			<?php } else { ?>
			
			<div class="review-list him">
			<?php } ?>
					
					<div class="review_date"><?php echo $date_created; ?></div>
					<div class="review-header">
                                                <div class="avatarcon"><img class="avatar" src="image/no-avatar-women.png" data-src="<?php echo $message['image']; ?>" class="lazy"></div>
						<div class="author"><?php echo ucwords($message['sender']); ?></div>
					</div>
					<div id="chatzig"></div>
					<div class="text">
					<?php echo nl2br($message['message']); ?>
					</div>
				</div>
				
			<?php } ?>
		<?php } ?>
		</div></div>
	</div>
	</div>
	<?php echo $content_bottom; ?>
</div>

<script>
$(document).ready(function() {
	$("#ms-message-text").autosize();
});
</script>

<?php echo $footer; ?>