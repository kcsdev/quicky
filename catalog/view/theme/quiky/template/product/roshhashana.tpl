<?php echo $header; ?>
<div id="content"><?php echo $content_top; ?>
	
	
	<div class="roshhashana-top">
		<div class="roshhashana-pic"></div>
	</div>
	
	<div class="more-info">
		<div class="more-info-right">
			<h2>הזמנות</h2>
			<span class="red">הזמנות עד:</span><span>יום שישי 19/9 בטלפון 03-3741886</span>
			<br>
			<span class="red">מינימום הזמנה מבשלן בודד:</span><span>150 ש״ח</span>
		</div>
		<div class="more-info-left">
			<h2>משלוחים</h2>
			<span class="red">משלוחים עד הבית:</span><span>ביום שלישי 23/9</span>
			<br>
			<span class="red">דמי משלוח:</span><span>תל אביב  25 ש״ח <span class="grey">|</span> גוש דן 35 ש״ח <span class="grey">|</span> השרון 40 ש״ח</span>
		</div>
		<div style="clear: both"></div>
	</div>
	
	<?php foreach($sellers as $seller) { ?> 
	<div class="menu-con">
		<div class="ms-sellerprofile roshhashana-left">
			<div class="seller-data">
				<div class="avatar-box">
					<img src="image/no-avatar-women.png" data-src="<?php echo $seller['image']; ?>" />
					<h2><?php echo $seller['name']; ?></h2>
					<p><?php echo $seller['city']; ?></p>
					 <div class="speciallities">
						<h3><?php echo $text_sp ?></h3>
						<p><?php echo $seller['sp'] ?></p>
					</div>
				
				<?php if ((!$this->customer->getId() || ($this->customer->getId() && $this->customer->getId() != $seller['seller_id'])) && ($this->config->get('msconf_enable_private_messaging') == 2 || ($this->config->get('msconf_enable_private_messaging') == 1 && $this->customer->getId()))) { ?>
					<a id="button-pm" href="index.php?route=seller/catalog-seller/jxRenderContactDialog&seller_id=<?php echo $seller_id; ?>" class="ajax_msg"><?php echo $text_contactme ?></a>
				<?php } else { ?>
					<a onclick="$('#open_login').trigger('click')"><?php echo $text_contactme ?></a>
				<?php } ?>
				</div>
				
				<div class="info-box">
					<div class="tab">
						<p><?php echo $seller['review_rating']; ?>%<img src="catalog/view/theme/<?php echo $this->config->get('config_template') ?>/image/homeals/positive.png" align="absmiddle" /></p>
						<p><?php echo $text_rating ?></p>
					</div>
					
					<div class="tab-split"></div>
					
					<div class="tab last">
						<p><?php echo $seller['review_total']; ?><img src="catalog/view/theme/<?php echo $this->config->get('config_template') ?>/image/homeals/cookcomment.png" align="absmiddle" /></p>
						<p><?php echo $text_cookreviews ?></p>
					</div>
					<div class="clear"></div>
				</div>
				
			</div>
		</div>
		
		<h1 class="title"><?php echo 'המנות של '.$seller['name']; ?></h1>
		
		<div class="roshhashana-right">
			
			<?php
			
			$cat_1_live = false;
			$cat_2_live = false;
			$cat_3_live = false;
			$cat_4_live = false;
			$cat_5_live = false;
			
			if($seller['meals_cat_1']){
				$cat_1_live = true;
			} else if ($seller['meals_cat_2']){
				$cat_2_live = true;
			} else if ($seller['meals_cat_3']){
				$cat_3_live = true;
			} else if ($seller['meals_cat_4']){
				$cat_4_live = true;
			} else if ($seller['meals_cat_5']){
				$cat_5_live = true;
			} 
			
			?>
			
			
			<div class="tab-link-con">
				<div class="tab-links">
					<a data-id="cat-1" class="menu-cat-btn <?php echo ($cat_1_live) ?'selected' : '';?> "><?php echo 'ראשונות'; ?></a>
					<a data-id="cat-2" class="menu-cat-btn <?php echo ($cat_2_live) ?'selected' : '';?> "><?php echo 'מרקים'; ?></a>
					<a data-id="cat-3" class="menu-cat-btn <?php echo ($cat_3_live) ?'selected' : '';?> "><?php echo 'עיקריות'; ?></a>
					<a data-id="cat-4" class="menu-cat-btn <?php echo ($cat_4_live) ?'selected' : '';?> "><?php echo 'תוספות'; ?></a>
					<a data-id="cat-5" class="menu-cat-btn <?php echo ($cat_5_live) ?'selected' : '';?> "><?php echo 'עוגות'; ?></a>
				</div>
			</div>
			
			<div class="meal-con" id="cat-1" <?php echo ($cat_1_live) ? '' : 'style="display: none"';?>>
				<?php if($seller['meals_cat_1']){ ?> 
				<?php foreach($seller['meals_cat_1'] as $meal) { ?>
				<table class="meal-table">
					<tbody class="meal-table-body">
						<tr>
							<td class="menu-item-right meal-name"><?php echo $meal['name']; ?></td>
							<td class="menu-item-left meal-price">₪<?php echo round($meal['price']*1.2); ?></td>
						</tr>
						<tr>
							<td class="menu-item-right meal-description"><?php echo $meal['description']; ?></td>
							<td class="menu-item-left meal-quant"><?php echo $meal['quant']; ?>
							</td>
						</tr>
					</tbody>
				</table>
				<?php } ?>
				<?php } else { ?>
				<h1>אין מנות להציג לקטגוריה זו</h1>
				<?php } ?>
			</div>
			
			<div class="meal-con" id="cat-2" <?php echo ($cat_2_live) ? '' : 'style="display: none"';?>>
				<?php if($seller['meals_cat_2']){ ?> 
				<?php foreach($seller['meals_cat_2'] as $meal) { ?>
				<table class="meal-table">
					<tbody class="meal-table-body">
						<tr>
							<td class="menu-item-right meal-name"><?php echo $meal['name']; ?></td>
							<td class="menu-item-left meal-price">₪<?php echo round($meal['price']*1.2); ?></td>
						</tr>
						<tr>
							<td class="menu-item-right meal-description"><?php echo $meal['description']; ?></td>
							<td class="menu-item-left meal-quant"><?php echo $meal['quant']; ?>
							</td>
						</tr>
					</tbody>
				</table>
				<?php } ?>
				<?php } else { ?>
				<h1>אין מנות להציג לקטגוריה זו</h1>
				<?php } ?>
			</div>
			
			<div class="meal-con" id="cat-3" <?php echo ($cat_3_live) ? '' : 'style="display: none"';?>>
				<?php if($seller['meals_cat_3']){ ?> 
				<?php foreach($seller['meals_cat_3'] as $meal) { ?>
				<table class="meal-table">
					<tbody class="meal-table-body">
						<tr>
							<td class="menu-item-right meal-name"><?php echo $meal['name']; ?></td>
							<td class="menu-item-left meal-price">₪<?php echo round($meal['price']*1.2); ?></td>
						</tr>
						<tr>
							<td class="menu-item-right meal-description"><?php echo $meal['description']; ?></td>
							<td class="menu-item-left meal-quant"><?php echo $meal['quant']; ?>
							</td>
						</tr>
					</tbody>
				</table>
				<?php } ?>
				<?php } else { ?>
				<h1>אין מנות להציג לקטגוריה זו</h1>
				<?php } ?>
			</div>
			
			<div class="meal-con" id="cat-4" <?php echo ($cat_4_live) ? '' : 'style="display: none"';?>>
				<?php if($seller['meals_cat_4']){ ?> 
				<?php foreach($seller['meals_cat_4'] as $meal) { ?>
				<table class="meal-table">
					<tbody class="meal-table-body">
						<tr>
							<td class="menu-item-right meal-name"><?php echo $meal['name']; ?></td>
							<td class="menu-item-left meal-price">₪<?php echo round($meal['price']*1.2); ?></td>
						</tr>
						<tr>
							<td class="menu-item-right meal-description"><?php echo $meal['description']; ?></td>
							<td class="menu-item-left meal-quant"><?php echo $meal['quant']; ?>
							</td>
						</tr>
					</tbody>
				</table>
				<?php } ?>
				<?php } else { ?>
				<h1>אין מנות להציג לקטגוריה זו</h1>
				<?php } ?>
			</div>
			
			<div class="meal-con" id="cat-5" <?php echo ($cat_5_live) ? '' : 'style="display: none"';?>>
				<?php if($seller['meals_cat_5']){ ?> 
				<?php foreach($seller['meals_cat_5'] as $meal) { ?>
				<table class="meal-table">
					<tbody class="meal-table-body">
						<tr>
							<td class="menu-item-right meal-name"><?php echo $meal['name']; ?></td>
							<td class="menu-item-left meal-price">₪<?php echo round($meal['price']*1.2); ?></td>
						</tr>
						<tr>
							<td class="menu-item-right meal-description"><?php echo $meal['description']; ?></td>
							<td class="menu-item-left meal-quant"><?php echo $meal['quant']; ?>
							</td>
						</tr>
					</tbody>
				</table>
				<?php } ?>
				<?php } else { ?>
				<h1>אין מנות להציג לקטגוריה זו</h1>
				<?php } ?>
			</div>
			
		</div>
	</div>
	<?php } ?>
	
	<div class="more-info">
		<div class="more-info-right">
			<h2>הזמנות</h2>
			<span class="red">הזמנות עד:</span><span>יום שישי 19/9 בטלפון 03-3741886</span>
			<br>
			<span class="red">מינימום הזמנה מבשלן בודד:</span><span>150 ש״ח</span>
		</div>
		<div class="more-info-left">
			<h2>משלוחים</h2>
			<span class="red">משלוחים עד הבית:</span><span>ביום שלישי 23/9</span>
			<br>
			<span class="red">דמי משלוח:</span><span>תל אביב  25 ש״ח <span class="grey">|</span> גוש דן 35 ש״ח <span class="grey">|</span> השרון 40 ש״ח</span>
		</div>
		<div style="clear: both"></div>
	</div>
	
<?php echo $content_bottom; ?>
</div>

<script>
    $(document).ready(function ($) {
		$(".menu-cat-btn").on("click", function(e) {
			e.preventDefault;
			if($(this).hasClass("selected")){
				// do nothing
			} else {
				$(this).parent().find(".menu-cat-btn").removeClass("selected");
				$(this).parent().parent().parent().find(".meal-con").css("display","none");
				$(this).addClass("selected");
				$(this).parent().parent().parent().find("#"+$(this).attr("data-id")).css("display","block");
			}
		});
	});
</script>

<?php echo $footer; ?>