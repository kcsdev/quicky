<?php echo $header ?>

<div class="image-placeholder">
</div>

<div class="image-con page-image-header">
    <img src="/image/data/homepage/homepagestake.png" >
</div>

<div id="filter-wrapper">
    <div class="concenter1024">
		<div class="table height100">
			<form action="index.php?route=product/quickyCategory" class="rest-filter-form" id="rest-filter-form"  method="post">
				
				<div class="rest-filter" >
				<span class="filter-name"><?php echo $text_kitchen ?></span>
                    <select name="filter-kitchen" id="filter-kitchen" class="chosen" >
                <?php foreach($category as $cat){ ?>
					<option value="<?php echo $cat['option_value_id'] ?>"><?php echo $cat['name'] ?> </option>
                    <?php } ?>
                    </select>
				</div>
				<div class="rest-filter">
				<span class="filter-name"><?php echo $text_city ?></span>
				<select name="filter-cost" id="filter-cost" class="chosen" >
                    <?php foreach ($cities as $the_city){ ?>
                    <?php if(isset($_SESSION['city']) && $_SESSION['city'] == $the_city['name']){ ?>
					<option value="<?= $the_city['name'] ?>" selected><?= $the_city['name'] ?></option>
                    <?php }else{ ?>
                     <option value="<?= $the_city['name'] ?>"><?= $the_city['name'] ?></option>
                    <?php } ?>
                    <?php } ?>
				</select>
				</div>
				<div class="rest-filter" >
				<span class="filter-name"><?php echo $text_kosher ?></span>
				<select name="filter-kosher" id="filter-kosher" class="chosen" >
                <?php foreach($kosher as $kos){ ?>
					<option value="<?php echo $kos['option_value_id'] ?>"><?php echo $kos['name'] ?></option>
                <?php } ?>
				</select>
				</div>
				<div class="rest-filter-button" >
				<input type="submit" id="rest-filter-submit" value="<?php echo $text_find ?>" >
				</div>
				
			</form>
		</div>
	
    </div>

</div>

<div id="home-content">
<div class="concenter1024">
<h1 class="restaurant-list-title cat-title">רשימת מסעדות<br><br>
<?php    
if(isset($_SESSION['city'])){ 
echo ' ב'  . $_SESSION['city'];
 }elseif(isset($rest_cat)){ 
echo ' בסגנון '  . $rest_cat[0]['name'];
 } ?> 
</h1>
 
<!-- restaurant list -->    
 
            <div class="restaurant-list clear" >
                <?php if(!empty($restaruants)){ ?>
                <?php foreach($restaruants as $rest) { ?>
                <!-- get the restaurent -->
                <?php if(isset($_SESSION['city'])){ ?>
                                <a href="<?php echo $rest['href'] ?>" alt="<?php echo $rest['name'] ?>" data-start="<?php echo $rest['start'] ?>" data-close="<?php echo $rest['close'] ?>" data-id="<?php echo $rest['restaruant_id'] ?>" data-min="<?php echo $rest['min_order'] ?>" data-kosher="<?php echo $rest['kos_id'] ?>" data-cat="<?php echo $rest['cat'] ?>" data-citylist ="<?php echo $rest['city_list'] ?>">
                    
                 <?php }else{ ?>
                <a href="<?php echo $rest['href'] ?>" alt="<?php echo $rest['name'] ?>" data-start="<?php echo $rest['start'] ?>" data-close="<?php echo $rest['close'] ?>" data-id="<?php echo $rest['restaruant_id'] ?>">
                 <?php } ?>
                <div class="rest-col">
                    <div class="rest-image">
                        <!--<span class="helper"></span>-->
                        <img src="<?php echo $rest['image'] ?>" >
                    </div>
                    <?php if(isset($_SESSION['city']) && isset($rest['min_order']) && isset($rest['pay']) && isset($rest['timeof'])){ ?>
                    <div class="rest-info times">
                        <ul class="food-cat-menu short-rest" >
                            <li><?php echo 'מינימום הזמנה: ₪' . $rest['min_order']  ?></li>
                            <li>|</li>
                            <li><?php echo 'משלוח: ₪' . $rest['pay']  ?></li>
                            <li>|</li>
                           <li><?php echo 'זמן הגעה עד כ-' . $rest['timeof'] . ' דקות' ?></li>
                        </ul>
                    </div>
                    <?php } ?>
                    <div class="rest-info rest-data">
                        <ul class="food-cat-menu short-rest" >
                            <li><?php echo $rest['name'] ?></li>
                            <li>|</li>
                            <li><?php echo $rest['sp'] ?></li>
                        </ul>
                    </div>
                </div>
                </a>
                <?php } ?>
                <?php }else{ ?>
                <div class="no-prod" align="center">
                 אין מוצרים בקטגוריה זו
                </div>>
                <?php } ?>
            </div>    
    
 <!-- end of restaurant list -->    

</div>
</div>




<!-- calculat if the restaurant is open -->
<script>
var time = new Date();
var hour = time.getHours();
var minuts = time.getMinutes();

var rest = $('div.restaurant-list').children();
$.each( rest, function( key, value ) {
    
    start = $(value).attr("data-start");
    close = $(value).attr("data-close");

    if((!close) && (!start)){
    $(value).removeAttr( "href" );
    $(value).css('cursor', 'default');
     $(value).children().prepend('<div class="time-note">המסעדה איננה פתוחה היום</div>');
    }else{
        
        new_start = start.split(":");
        new_close = close.split(":");
        start_hour =  parseInt(new_start[0]);
        start_minuts = parseInt(new_start[1]);
        close_hour = parseInt(new_close[0]);
        close_minuts = parseInt(new_close[1]);
        if(start_hour > hour){
            
            if(start_minuts < 10){
                 start_minuts = '0' + start_minuts;
                }
            
          $(value).children().prepend('<div class="time-note">ניתן להזמין מהשעה ' + start_hour + ':' + start_minuts +'</div>');
            
        }else if((start_hour == hour) && ( start_minuts < minuts)){
            
                 if(start_minuts < 10){
                 start_minuts = '0' + start_minuts;
                }
        
         $(value).children().prepend('<div class="time-note">ניתן להזמין מהשעה ' + start_hour + ':' + start_minuts +'</div>');
        }else if(close_hour < hour){
            
             if(start_minuts < 10){
                 start_minuts = '0' + start_minuts;
                }
             $(value).children().prepend('<div class="time-note">ניתן להזמין מהשעה ' + start_hour + ':' + start_minuts +'</div>');
        
        }else if((close_hour == hour) && (minuts > close_minuts)){
            
            if(start_minuts < 10){
                 start_minuts = '0' + start_minuts;
                }
         $(value).children().prepend('<div class="time-note">ניתן להזמין מהשעה ' + start_hour + ':' + start_minuts +'</div>');
        }
    }

});
    
</script>

<script>
var city = "<?php echo isset($_SESSION['city']) ? $_SESSION['city'] : 0 ?>";
var email = "<?php echo null !== $this->customer->getEmail() ? $this->customer->getEmail() : 0 ?>";

if(city === "0" && email === "0"){
var output = '<h1 style="margin-right: 4%;">עלייך לבחור עיר:</h1>';
output += '<form action="index.php?route=product/quickyCategory" method="post" class="city_form">';
output+= '<label for"city">עיר:</label>&nbsp&nbsp';
output += '<select name="city">';
<?php foreach($cities as $city){ 
$op_city = $city['name'];
?>
output += '<option value="<?php echo $op_city ?>"><?php echo $op_city ?></option>';
<?php } ?>
output += '</select><br><br>';
output += '<input type="submit" class="city_button" value="בחר">';
output += '</form>';
$('body').append('<div id="cboxOverlay" style="opacity: 0.9; cursor: pointer; visibility: visible;"></div');
$('body').append('<div id="city-choose">' + output + '</div>');
 }

</script>
    
    
<script>
/*$('input#rest-filter-submit').on('click', function(){
 $('div.restaurant-list').children().css('display', 'none');
var kitchen = $('select#filter-kitchen option:selected').val();  
var city = $('select#filter-cost option:selected').val();
var kosher = $('select#filter-kosher option:selected').val();
var rest_list = $('div.restaurant-list').children();
$.ajax({
			  url: "index.php?route=product/quickyCategory/createSess",
			  type: "POST",
			  dataType: "html",
			  async: "false", 
			  data: {city:city},
			  success: function(response) {
              
                $.each( rest_list, function( key, value ) {
                
                 var city_list = $(value).attr("data-citylist");
                 var rest_kosher = $(value).attr("data-kosher");
                var rest_cat = $(value).attr("data-cat");
                 var city_list_arr = city_list.split(",");
                 var rest_cat_arr = rest_cat.split(",");
                 if($.inArray( city, city_list_arr) != -1 && $.inArray( kitchen, rest_cat_arr) != -1 && rest_kosher == kosher){
                 
                       $(value).css('display', 'block');
                   }
                
                })
              }

});

});
    
</script>

<?php echo $footer ?>