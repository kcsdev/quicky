<?php //echo $header; ?><?php //echo $column_left; ?><?php //echo $column_right; ?>

<div id="ajax-product">
    <div class="product-info" itemscope itemtype="http://schema.org/Product">
	<?php //echo $content_top; ?>
	
	<div class="product-header clear">
	    <div class="left product-title">
		<h1 class="item name fn" itemprop="name" id="meal-title-pro"><?php echo $heading_title; ?>
		<?php if( false ) {  ?><span class="side-total"><?php echo $total_sides; ?></span><?php } ?>
		</h1>
		
		<?php if($cart_items){ ?>
		<span class="cart-num">
		    סך הכל מוצרים בסל 
		    <span class="num-in-cart"><?php echo $cart_items['num']; ?></span>
		</span>
		<?php } ?>
	    </div>
	</div>
	
	<div id="tab-description">
	    <?php
	    $description_max_size = 250;
	    $short_description = mb_truncate($description,$description_max_size);
	    
	    if (mb_strlen($description,"UTF-8") > $description_max_size){
	    ?>
	    <div class="short-description">
		<p><?php echo $short_description; ?></p>
		<a id="more"><?php echo $text_read_more; ?></a></div>
	    <div class="full-description" style="display: none"><?php echo $description; ?><a id="less"><?php echo $text_read_less; ?></a></div>
	    <?php } else { ?>
	    <div class="full-description"><?php echo nl2br($description); ?></div>
	    <?php } ?>
	</div>
	<!-- start of product table -->
   
	<div class="product-table">
     <!--
	    <div class="row 1-row">
		<div class="right-cell">
		    <h1>כמות</h1>
		</div>
		<div class="left-cell">
		    <div class="quant-controller">';
			    <div class="plus">+</div>
			    <div class="quant">1</div>
			    <div class="minus">-</div>
		    </div>
		</div>
	    </div>
	    <div class="row 2-row product-options">
		<div class="right-cell">
		    <h1>מידת עשייה</h1>
		</div>
		<div class="left-cell">
		    <label for="raw" >נא</label>
		    <input type="radio" id="raw" name="prep-level" value="raw" checked>
			
		    <label for="medium" >בינוני</label>
		    <input type="radio" id="medium" name="prep-level" value="medium" >
			
		    <label for="well" >שרוף</label>
		    <input type="radio" id="well" name="prep-level" value="well" >
		</div>
	    </div>
	    <div class="row 3-row extras">
		<div class="right-cell">
		    <h1>תוספות לבחירה</h1>
		    <h3>ניתן לבחור עד 3 תוספות</h3>
		</div>
		<div class="left-cell">
		    <label for="one" value="hasa">חסה</label>
		    <input type="checkbox" name="extras" id="one" value="one" >
		    
		    <label for="two" >עגבניה</label>
		    <input type="checkbox" name="extras" id="two" value="two" >
			
		    <label for="three" >בצל</label>
		    <input type="checkbox" name="extras" id="three" value="three" >
			
		    <label for="four" >מלפפון חמוץ</label>
		    <input type="checkbox" name="extras" id="four" value="four" >
			
		    <label for="five" >פטריות</label>
		    <input type="checkbox" name="extras" id="five" value="five" >
			
		    <label for="six" >אננס</label>
		    <input type="checkbox" name="extras" id="six" value="six" >
			
		    <label for="seven" >ביצת עין</label>
		    <input type="checkbox" name="extras" id="seven" value="seven" >
		</div>
	    </div>
	    <div class="row 4-row">
		<div class="right-cell">
		    <h1 class="h1-sauce">רטבים</h1>
		    
		    <h1>הערות מיוחדות</h1>
		</div>
		<div class="left-cell">
		    <div class="more-product-table">
			
			<div class="more">
			    <select class="sauce" name="the-sauce">
				<option value="1">רוטב שום</option>
				<option value="2">קטשופ</option>
				<option value="3">רוטב צ׳ילי</option>
				<option value="4">רוטב טריאקי</option>
				<option value="5">רוטב סוס</option>
			    </select>
			    <textarea class="" name="meal-comment"></textarea>
			</div>
          -->
        
     
            <?php if ($options) { ?>
              <div class="options">
        		        
                <?php foreach ($options as $option) { ?>
                <?php if ($option['type'] == 'select') { ?>
                <div id="option-<?php echo $option['product_option_id']; ?>" class="option">
                  <?php if ($option['required']) { ?>
                  <span class="required">*</span>
                  <?php } ?>
                  <b><?php echo $option['name']; ?>:</b><br />
                  <select name="option[<?php echo $option['product_option_id']; ?>]">
                    <option value=""><?php echo $text_select; ?></option>
                    <?php foreach ($option['option_value'] as $option_value) { ?>
                    <option value="<?php echo $option_value['product_option_value_id']; ?>"><?php echo $option_value['name']; ?>
                    <?php if ($option_value['price']) { ?>
                    (<?php echo $option_value['price_prefix']; ?><?php echo $option_value['price']; ?>)
                    <?php } ?>
                    </option>
                    <?php } ?>
                  </select>
                </div>
                <br />
                <?php } ?>
                <?php if ($option['type'] == 'radio') { ?>
                <div id="option-<?php echo $option['product_option_id']; ?>" class="option">
                  <?php if ($option['required']) { ?>
                  <span class="required">*</span>
                  <?php } ?>
                  <b><?php echo $option['name']; ?>:</b><br />
                  <?php foreach ($option['option_value'] as $option_value) { ?>
                  <input type="radio" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option_value['product_option_value_id']; ?>" id="option-value-<?php echo $option_value['product_option_value_id']; ?>" />
                  <label for="option-value-<?php echo $option_value['product_option_value_id']; ?>"><?php echo $option_value['name']; ?>
                    <?php if ($option_value['price']) { ?>
                    (<?php echo $option_value['price_prefix']; ?><?php echo $option_value['price']; ?>)
                    <?php } ?>
                  </label>
                  <br />
                  <?php } ?>
                </div>
                <br />
                <?php } ?>
                <?php if ($option['type'] == 'checkbox') { ?>
                <div id="option-<?php echo $option['product_option_id']; ?>" class="option">
                 <div class="row">
                   <div class="right-cell">
                  <?php if ($option['required']) { ?>
                  <span class="required">*</span>
                  <?php } ?>
                  <b><?php echo $option['name']; ?>:</b>
                    </div>
                    <div class="left-cell">
                  <?php foreach ($option['option_value'] as $option_value) { ?>
                  <input type="checkbox" name="option[<?php echo $option['product_option_id']; ?>][]" value="<?php echo $option_value['product_option_value_id']; ?>" id="option-value-<?php echo $option_value['product_option_value_id']; ?>" />
                  <label for="option-value-<?php echo $option_value['product_option_value_id']; ?>"><?php echo $option_value['name']; ?>
                    <?php if ($option_value['price']) { ?>
                    (<?php echo $option_value['price_prefix']; ?><?php echo $option_value['price']; ?>)
                    <?php } ?>
                  </label>
                  
                  <?php } ?>
                    </div>
                  </div>
                </div>
                <br />
                <?php } ?>
                <?php if ($option['type'] == 'image') { ?>
                <div id="option-<?php echo $option['product_option_id']; ?>" class="option">
                  <?php if ($option['required']) { ?>
                  <span class="required">*</span>
                  <?php } ?>
                  <b><?php echo $option['name']; ?>:</b><br />
                  <table class="option-image">
                    <?php foreach ($option['option_value'] as $option_value) { ?>
                    <tr>
                      <td style="width: 1px;"><input type="radio" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option_value['product_option_value_id']; ?>" id="option-value-<?php echo $option_value['product_option_value_id']; ?>" /></td>
                      <td><label for="option-value-<?php echo $option_value['product_option_value_id']; ?>"><img src="<?php echo $option_value['image']; ?>" alt="<?php echo $option_value['name'] . ($option_value['price'] ? ' ' . $option_value['price_prefix'] . $option_value['price'] : ''); ?>" /></label></td>
                      <td><label for="option-value-<?php echo $option_value['product_option_value_id']; ?>"><?php echo $option_value['name']; ?>
                          <?php if ($option_value['price']) { ?>
                          (<?php echo $option_value['price_prefix']; ?><?php echo $option_value['price']; ?>)
                          <?php } ?>
                        </label></td>
                    </tr>
                    <?php } ?>
                  </table>
                </div>
                <br />
                <?php } ?>
                <?php if ($option['type'] == 'text') { ?>
                <div id="option-<?php echo $option['product_option_id']; ?>" class="option">
                  <?php if ($option['required']) { ?>
                  <span class="required">*</span>
                  <?php } ?>
                  <b><?php echo $option['name']; ?>:</b><br />
                  <input type="text" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option['option_value']; ?>" />
                </div>
                <br />
                <?php } ?>
                <?php if ($option['type'] == 'textarea') { ?>
                <div id="option-<?php echo $option['product_option_id']; ?>" class="option">
                  <?php if ($option['required']) { ?>
                  <span class="required">*</span>
                  <?php } ?>
                  <b><?php echo $option['name']; ?>:</b><br />
                  <textarea name="option[<?php echo $option['product_option_id']; ?>]" cols="40" rows="5"><?php echo $option['option_value']; ?></textarea>
                </div>
                <br />
                <?php } ?>
                <?php if ($option['type'] == 'file') { ?>
                <div id="option-<?php echo $option['product_option_id']; ?>" class="option">
                  <?php if ($option['required']) { ?>
                  <span class="required">*</span>
                  <?php } ?>
                  <b><?php echo $option['name']; ?>:</b><br />
                  <input type="button" value="<?php echo $button_upload; ?>" id="button-option-<?php echo $option['product_option_id']; ?>" class="button">
                  <input type="hidden" name="option[<?php echo $option['product_option_id']; ?>]" value="" />
                </div>
                <br />
                <?php } ?>
                <?php if ($option['type'] == 'date') { ?>
                <div id="option-<?php echo $option['product_option_id']; ?>" class="option">
                  <?php if ($option['required']) { ?>
                  <span class="required">*</span>
                  <?php } ?>
                  <b><?php echo $option['name']; ?>:</b><br />
                  <input type="text" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option['option_value']; ?>" class="date" />
                </div>
                <br />
                <?php } ?>
                <?php if ($option['type'] == 'datetime') { ?>
                <div id="option-<?php echo $option['product_option_id']; ?>" class="option">
                  <?php if ($option['required']) { ?>
                  <span class="required">*</span>
                  <?php } ?>
                  <b><?php echo $option['name']; ?>:</b><br />
                  <input type="text" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option['option_value']; ?>" class="datetime" />
                </div>
                <br />
                <?php } ?>
                <?php if ($option['type'] == 'time') { ?>
                <div id="option-<?php echo $option['product_option_id']; ?>" class="option">
                  <?php if ($option['required']) { ?>
                  <span class="required">*</span>
                  <?php } ?>
                  <b><?php echo $option['name']; ?>:</b><br />
                  <input type="text" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option['option_value']; ?>" class="time" />
                </div>
                <br />
                <?php } ?>
                <?php } ?>
              
              <?php } ?>
        <!-- end of options real -->
       
			<div class="image">
			    <?php if ($thumb) { ?>      
			    <span class="image">
			      <?php if ($is_verified) { ?>
			      <div class="homaels-verified"></div>
			      <?php } ?>
			      <img id="cloud-zoom"  src="<?php echo $thumb; ?>"   title="<?php echo $heading_title; ?>" alt="<?php echo $heading_title; ?>" data-zoom-image="<?php echo $popup; ?>"/>
			    </span>
			    <?php } ?>
			</div>
			</div>
		    </div><!-- more-product-table end -->
		</div>
	    


	
	<!-- end of product table -->
	<div class="order-line">
	    <span class="sum-price">
	סה״כ:
	<?php echo $price; ?>
	    </span>
       <!-- <input type="button" value="הוסף להזמנה" id="button-login-fb" class="button quikbtn" onclick="openCart();">
	   <input type="button" id="addToPlate" class="quikbtn" onclick="openCart();" value="הוסף לצלחת">
	    <button id="button-cart" class="quikbtn" onclick="openCart();">
		הוסף לצלחת
	    </button>-->
       <input type="button" value="הוסף להזמנה" id="button-cart" class="button">

	</div>
  </div>
	<?php // echo $content_bottom; ?>
    </div><!-- product-info end -->
</div><!-- ajax-product end -->


<script type="text/javascript">
$(document).ready(function() {
    
    $('.product-filter .sort select').customSelect();
    $('.product-filter .limit select').customSelect();
    $('.product-info .options select').customSelect();
    $("#cboxClose").text("");
    $("#cboxClose").css("text-indent","0");
    $('input[type="checkbox"]').tmMark(); 
    $('input[type="radio"]').tmMark();
        
    if(/Android|webOS|iPhone|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
       // responsive for page
    }
    
    $("#addToPlate").on("click", function() {
	$.colorbox.close();
	addToCart("<?php echo $product_id ?>");
    });
    
    $("#add-meal-btn").live("click",function(){
	$("html, body").animate({ scrollTop: ($('.product_under-img-con').offset().top - 81 )}, 1000);
    });
    
    $('#quantity-input').on('change', function() {
	var multi = $(this).val();
	var price = parseInt($("#int-price").val());
	
	var side_1 = $('.product_under-img-con select[name="option[side-1]"]');
	
	if(side_1.length) {
	    var side_1_price = parseInt(side_1.find('option[value="'+side_1.val()+'"]').attr("data-price"));
	} else {
	    var side_1_price = 0;
	}
	
	var side_2 = $('.product_under-img-con select[name="option[side-2]"]');
	
	if(side_2.length) {
	    var side_2_price = parseInt(side_2.find('option[value="'+side_2.val()+'"]').attr("data-price"));
	} else {
	    var side_2_price = 0;
	}
	
	var extra = side_1_price + side_2_price; // $("#extra-price").val();
	
	var total = price + extra;
	
	$("#total-price").html("<?php echo $ils ?>"+parseFloat(Math.round(multi*total* 100) / 100));
    });
    
    $(".plus-button").on("click",function () {
	if (!(parseInt($(this).parent().find("input").val()) == max_qunt )) {
	   $(this).parent().find("input").val( parseInt($(this).parent().find("input").val())+1);
	   $('#quantity-input').trigger("change");
	}
    });
    
    $(".minus-button").on("click",function () {
	if (!(parseInt($(this).parent().find("input").val()) <= 1 )) {
	   $(this).parent().find("input").val( parseInt($(this).parent().find("input").val())-1);
	   $('#quantity-input').trigger("change");
	}
    });
    
});
</script>

<!-- copy add product -->
<script>
function openCart() {
    add_to_cart = true;
    $("#open_login").trigger("click");
}
</script>


<?php if ($options) { ?>
<script type="text/javascript" src="catalog/view/javascript/jquery/ajaxupload.js"></script>
<?php foreach ($options as $option) { ?>
<?php if ($option['type'] == 'file') { ?>
<script type="text/javascript">
new AjaxUpload('#button-option-<?php echo $option['product_option_id']; ?>', {
	action: 'index.php?route=product/product/upload',
	name: 'file',
	autoSubmit: true,
	responseType: 'json',
	onSubmit: function(file, extension) {
		$('#button-option-<?php echo $option['product_option_id']; ?>').after('<img src="catalog/view/theme/default/image/loading.gif" class="loading" style="padding-left: 5px;" />');
		$('#button-option-<?php echo $option['product_option_id']; ?>').attr('disabled', true);
	},
	onComplete: function(file, json) {
		$('#button-option-<?php echo $option['product_option_id']; ?>').attr('disabled', false);
		
		$('.error').remove();
		
		if (json['success']) {
			//alert(json['success']);
			$('input[name=\'option[<?php echo $option['product_option_id']; ?>]\']').attr('value', json['file']);
		}
		
		if (json['error']) {
			$('#option-<?php echo $option['product_option_id']; ?>').after('<span class="error">' + json['error'] + '</span>');
		}
		
		$('.loading').remove();	
	}
});
</script>
<?php } ?>
<?php } ?>
<?php } ?>
<!-- add to cart btn -->
<script>

$('#button-cart').live('click', function(e) {
		e.preventDefault();
		
		if ( 1 == 1) {
			$.ajax({
				url: 'index.php?route=checkout/cart/add',
				type: 'post',
				data: $('.product-info input[type=\'text\'], .product-info input[type=\'hidden\'], .product-info input[type=\'radio\']:checked, .product-info input[type=\'checkbox\']:checked, .product-info select, .product-info textarea'),
				dataType: 'json',
				beforeSend: function() {
					//show loading
					$(".product_under-img-con").prepend('<div class="loading-box" id="meal-box-load"></div>');
					$('#button-cart').attr("disabled", true);
				},
				complete: function() {
					//hide loading
					$("#meal-box-load").remove();
					$('#button-cart').removeAttr("disabled");
				},
				success: function(json) {
					$('.success, .warning, .attention, information, .error').remove();
					
					if (json['error']) {
						if (json['error']['option']) {
							for (i in json['error']['option']) {
								$('#option-' + i).after('<span class="error">' + json['error']['option'][i] + '</span>');
							}
						}
						
						if (json['error']['profile']) {
							$('select[name="profile_id"]').after('<span class="error">' + json['error']['profile'] + '</span>');
						}
						
						// blocks crazy requests
						if (json['error']['no_meal']) {
							alert("אין באפשרותך להוסיף את מוצר זה זמנו עבר או אזל המלאי");
							$('#button-show-cart').trigger("click");
						}
						
						if (json['error']['wrong_date']) {
							alert("תאריך המוצר שהינך מנסה להוסיף אינו תואם את תאריך המוצרים שכבר בעגלה, כדי להוסיף אנא בחר בתאריך המתאים.");
							$('#button-show-cart').trigger("click");
						}
						
						if (json['error']['wrong_time']) {
							alert("זמן המוצר שהינך מנסה להוסיף אינו תואם את זמן המוצרים שכבר בעגלה, כדי להוסיף אנא בחר בזמן המתאים.");
							$('#button-show-cart').trigger("click");
						}
					}
					
					if (json['success']) {
						console.log('asdsada');
						if (add_to_cart) {
							location.reload();
						} else {
							added_now = true;
							$('#button-show-cart').trigger("click");
							//refreshAll();
						}
						
						$("#day").trigger("change");
						
						$("#quantity-input").val(1);
						
						$("#day").attr("disabled", true);
						$("#time-of-day").attr("disabled", true);
						
						$(".edit-box input[name='option[time-of-day]']").val($("#time-of-day").val());
						
						/* $('html, body').animate({ scrollTop: 0 }, 'slow'); */
					}	
				}
			});
		}
		
		
		var alert_msg = "";
		
		if (addtocart_day_error) {
			$("#day").parent().css("border","1px solid #e7604a");
			alert_msg = "בחר יום לביצוע ההזמנה";
		}
		
		if (addtocart_error) {
			$("#time-of-day").parent().css("border","1px solid #e7604a");
			
			if (addtocart_day_error) {
				alert_msg = "בחר יום ושעה לביצוע ההזמנה";
			} else {
				alert_msg = "בחר שעה לביצוע ההזמנה";
			}
		}
		
		if (alert_msg != "") {
			alert(alert_msg);
		}
	});
	

</script>

<?php //echo $footer; ?>
