<?php echo $header; ?><?php echo $column_left; ?><?php echo $column_right; ?>
<div id="content"><?php echo $content_top; ?>
	<div class="contact" >
		<div class="sixty">
			<div class="contact-header">
				<h2 id="contact-phone" >
					<span class="quikyred">טלפון</span>
					מכל טלפון נייד - 
					<?php echo $telephone; ?>
				</h2>
				<span class="quikyred">|</span>
				<h2  id="contact-mikud" >
					<span class="quikyred">מיקוד</span>
					56304
				</h2>
				<span class="quikyred">|</span>
				<h2  id="contact-msg" >
					השיארו לנו הודעות, בקשות... וגם מילה טובה :)
				</h2>	
			</div>
			
			<div class="contact-description">
				קוויקי - שרות משלוחי אוכל ממסעדות באזור בקעת אונו, בעיר פתח תקווה, באיזור שוהם ומתחם איירפורט סיטי. הוקמה בשנת 2005 ע״י תושבי האזור. השרות הוקם מתוך הכרה ברצון הלקוחות - להרחיב את מגוון המסעדות באזור, מהן ניתן להזמין משלוחים, ומתוך הכרה בצורך של מסעדות לספק ללקוחותיהן משלוחי אוכל תוך מתן שרות איכותי, יציב ומהיר.
			</div>
			
			<div class="contact-image">
				<span class="helper"></span>
				<img src="<?php echo $contact_image; ?>" alt="תקשרו אלינו!" />
			</div>
			
		</div>
		<div class="fourty">
			<form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data">
			
				<table class="form">
					<tr>
						<td class="quikyred">מי אתם</td>
					</tr>
					<tr>
						<td>
							<select name="youare">
								<option value="<?php echo $youare; ?>">בעלי מסעדות</option>
								<option value="costumer">לקוחות</option>
							</select>
							<br />
							<?php if ($error_youare) { ?>
							<span class="error"><?php echo $error_youare; ?></span>
							<?php } ?>
						</td>	
					</tr>
					
					<tr>
						<td class="quikyred">שם מלא</td>
					</tr>
					<tr>
						
						<td><input type="text" name="name" value="<?php echo $name; ?>" />
							<br />
							<?php if ($error_name) { ?>
							<span class="error"><?php echo $error_name; ?></span>
							<?php } ?>
						</td>	
					</tr>
					
					<tr>
						<td class="quikyred">כתובת אימייל</td>
					</tr>
					<tr>
						
						<td><input type="text" name="email" value="<?php echo $email; ?>" />
							<br />
							<?php if ($error_email) { ?>
							<span class="error"><?php echo $error_email; ?></span>
							<?php } ?>
						</td>
					</tr>
					
					<tr>
						<td class="quikyred">טלפון</td>
					</tr>
					<tr>
						
						<td><input type="text" name="telephone" value="<?php echo $telephone; ?>" />
							<br />
							<?php if ($error_telephone) { ?>
							<span class="error"><?php echo $error_telephone; ?></span>
							<?php } ?>
						</td>
					</tr>
					
					<tr>
						<td class="quikyred">הודעה</td>
					</tr>
					<tr>
						<td><textarea name="enquiry" cols="40" rows="10" style="width: 100%;"><?php echo $enquiry; ?></textarea>
							<br />
							<?php if ($error_enquiry) { ?>
							<span class="error"><?php echo $error_enquiry; ?></span>
							<?php } ?>
						</td>
					</tr>
					
					<tr>
						<td>
							<input type="submit" value="<?php echo "שלח/י הודעה"; ?>" class="quiky-button" />
						</td>
					</tr>
					
				</table>
			</form>
		</div>
	</div>
<?php echo $content_bottom; ?>
</div>
<?php echo $footer; ?>