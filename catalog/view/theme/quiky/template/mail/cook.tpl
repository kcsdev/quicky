<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/1999/REC-html401-19991224/strict.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title><?php echo $title; ?></title>

<style>
  @import url(http://fonts.googleapis.com/earlyaccess/alefhebrew.css);
  @import url(http://fonts.googleapis.com/css?family=Ubuntu:400,700);
</style>

</head>
<body style="font-family: 'Alef Hebrew','Ubuntu', Arial, Helvetica, sans-serif; font-size: 12px; color: #000000;">
<div style="font-family: 'Alef Hebrew','Ubuntu', Arial, Helvetica, sans-serif; width: 100%; margin: auto; background-color: #FAF7EF; background-image: url('<?php echo $store_url; ?>catalog/view/theme/OPC040088/image/homeals/bg.png');">
  <a href="<?php echo $store_url; ?>" title="<?php echo $store_name; ?>" style="width: 100%; display: block; text-align: center; height: 110px;">
  <img src="<?php echo $logo; ?>" alt="<?php echo $store_name; ?>" style="margin-top: 10px; border: none;" />
  </a>
<div style="width:696px;margin:auto;direction: rtl;color: #372c2a;">
    <table style="border-collapse:collapse;width:100%;margin-bottom: 3px;background-color: #ffffff;">
      <tbody>
        <tr>
          <td style="font-size:12px;text-align:right;padding:35px 27px 10px 27px;vertical-align: top;">
              <div style="float:right;font-size:24px;display: block;padding-bottom: 10px;width: 100%;">
                <span><?php echo $text_greeting; ?></span>
              </div>
              
              <table>
              <tbody>
              <tr>
                <td style="font-size:14px;padding-bottom: 10px;">
                  <span><?php echo $cook_text_greeting; ?></span>
                </td>
              </tr>
              <tr>
                <td style="font-size: 14px;">
                  
                  <span style="color: #a2a1a1;"><?php echo $cook_text_delivery_time.":"; ?>
                  </span><span><?php echo $order_when; ?></span><br>
                  
                  <span style="color: #a2a1a1;"><?php echo $text_order_tools; ?></span>
                  <span><?php echo $order_tools; ?></span>
                
                </td>
              </tr></tbody>
              </table>
              
              
          </td>
          <td style="width:237px;font-size:12px;text-align:right;padding: 35px 27px 20px 27px;">
              <div style="float: right; width: 210px;">
                <div style="float: right;margin-bottom: 15px;width: 210px;text-align: center;">
                <span style="color: #a2a1a1;"><?php echo $text_order_status; ?></span>
                <span style="color: #DBD42B;"><?php echo $order_status; ?></span>
                <span style="color: #a2a1a1;"><?php echo $text_order_id; ?></span>
                <span><?php echo $order_id; ?></span>
              </div>
              <table style="border-collapse:collapse;width:100%;margin-bottom:3px;background-color:#ffffff;border: 2px solid #f5f2ea;">
              <tbody style="text-align: center;">
                <tr style="height: 22px;background: #f5f2ea;color: #78706c;">
                  <td><?php echo $cook_text_meals; ?></td>
                  <td><?php echo $cook_text_payment; ?></td>
                </tr>
                <tr style="height: 54px;color: #372c2a;font-size: 42px;">
                  <td style="border-left: 1px solid #f5f2ea;"><?php echo $cook_total_numofmeals; ?></td>
                  <td><?php print_r ($cook_total_pay); ?></td>
                </tr>
                </tbody>
              </table>
              
              <div>
                <a href="<?php echo $cook_link_aprove; ?>" style="background-image: url('<?php echo $store_url; ?>catalog/view/theme/OPC040088/image/homeals/mailbtn.png');height: 40px;line-height: 40px;width: 211px;display: block;text-align: center;text-decoration: none;color: #175a58;font-size: 18px;">
                <?php echo $cook_text_aprove; ?>
                </a>
              </div>
              
              </div>
          </td>
        </tr>
      </tbody>
    </table>
    
    <table style="border-collapse:collapse;width:100%;background-color: #ffffff;margin-bottom: 3px;">
      <tbody>
        <tr>
          <td style="line-height: 32px;font-size:18px;font-weight:bold;padding:14px 27px 5px 27px;color:#e7604a;width: 144px;vertical-align: top;">
            <img style="margin-left: 10px;" src="<?php echo $store_url; ?>catalog/view/theme/OPC040088/image/homeals/mailman.png" /><?php echo $cook_text_details; ?>
          </td>
          <td style="font-size:14px;padding: 14px 27px 20px 0;">
              <div style="height: 30px;  line-height: 30px;">
                <span style="color: #a2a1a1;"><?php echo $cook_text_username.":"; ?></span>
                <?php echo $cook_order_username; ?>
              </div>
              <div style="height: 30px;  line-height: 30px;">
                <span style="color: #a2a1a1;"><?php echo $cook_text_place.":"; ?></span>
                <?php echo $cook_order_city_name; ?>
              </div>
              <div style="height: 30px;  line-height: 30px;">
                <span style="color: #a2a1a1;"><?php echo $cook_text_phone.":"; ?></span>
                <?php echo $telephone; ?>
              </div>
              <div style="height: 30px;  line-height: 30px;">
                <span style="color: #a2a1a1;"><?php echo $cook_text_mail.":"; ?></span>
                <?php echo $email; ?>
              </div>
              
          </td>
        </tr>
      </tbody>
    </table>
    
    <table style="border-collapse:collapse;width:100%;background-color: #ffffff;margin-bottom: 3px;font-size: 14px;">
      <thead>
        <tr>
          <td style="font-size: 18px;font-weight: bold;padding: 14px 27px 0px 27px;  color: #e7604a;  width: 50%;" colspan="2">
            <?php echo $text_meal_details; ?>
          </td>
        </tr>
      </thead>
      <?php foreach ($products as $product) { ?>
      <tbody>
        <tr>
          <td style="padding: 25px 27px 25px  20px;width: 1%;"><img src="<?php echo $product['image']; ?>" /></td>
          <td style="padding: 25px 27px 25px 20px;">
            
            <div style="font-size: 16px; margin-bottom: 20px;">
              <?php echo $product['name'];
                if(isset($product['sides'])){
                  foreach ($product['sides'] as $side) {
                    if(isset($side['name'])){
                      echo " + ".$side['name'];
                    }
                  }
                }
              ?>
            </div>
            
            <?php if(!empty($product['comment'])) { ?>
            <div style="height: 20px;margin-bottom: 7px;">
              <span style="color: #a2a1a1;"><?php echo $text_meal_coment; ?></span>
              <span style="font-style: italic;"><?php echo '"'.$product['comment'].'"'; ?></span>
            </div>
            <?php } ?>
            
            
            <table>
              <tbody>
                <tr>
                  <td style="width: 135px;">
                    <div style="height: 20px;">
                      <span style="color: #a2a1a1;">
                        <?php echo $text_per_price; ?>
                      </span>
                      <span>
                        <?php echo $product['price']; ?>
                      </span>
                    </div>
                  </td>
                  <td>
                    <div style="height: 20px;">
                      <span style="color: #a2a1a1;">
                        <?php echo $text_quantity; ?>
                      </span>
                      <span>
                        <?php
                          if($product['quantity'] == 1){
                           echo "ארוחה אחת";
                          } else {
                           echo $product['quantity']." ארוחות";
                          }
                        ?>
                      </span>
                    </div>
                  </td>
                </tr>
              </tbody>
            </table>
            
            <div style="height: 37px;font-size: 30px;color: #e7604a;">
              <span><?php echo $product['total']; ?></span>
            </div>
            
          </td>
        </tr>
        <?php } ?>
      </tbody>
    </table>
    
    <table style="border-collapse:collapse;width:100%;background-color: #ffffff;margin-bottom: 30px;">
      <thead>
        <tr>
          <td colspan=2 style="line-height: 32px;font-size: 18px;font-weight: bold;padding: 14px 20px 5px 27px;color: #e7604a;  width: 50%;">
            <img style="margin-left: 10px;" src="<?php echo $store_url; ?>catalog/view/theme/OPC040088/image/homeals/mailbike.png" /><?php echo $text_delivery_details; ?>
          </td>
        </tr>
      </thead>
      <tbody>
        <tr>
          <td style="font-size:14px;border-right:3px solid #f6f3eb;padding:0 20px 20px 27px;width: 50%;">
              <div style="height: 30px;  line-height: 30px;">
                <span style="color: #a2a1a1;"><?php echo $text_order_address; ?></span>
                <?php echo $cook_order_address_1; ?>
              </div>
              <div style="height: 30px;  line-height: 30px;">
                <span style="color: #a2a1a1; width: 39px;"></span>
                <?php echo $cook_order_address_2; ?>
              </div>
              <div style="height: 30px;  line-height: 30px;">
                <span style="color: #a2a1a1;"><?php echo $text_order_compony; ?></span>
                <?php echo $order_compony; ?>
              </div>
          </td>
          <td style="font-size:14px;padding:0 20px 20px 27px">
              <div style="height: 30px;  line-height: 30px;">
                <span style="color: #a2a1a1;"><?php echo $text_order_contact; ?></span>
                <?php echo $order_contact; ?>
              </div>
              <div style="height: 30px;  line-height: 30px;">
                <span style="color: #a2a1a1;"><?php echo $cook_text_phone; ?>:</span>
                <?php echo $telephone; ?>
              </div>
              <?php if($comment){ ?>   
              <div style="height: 30px;  line-height: 30px;">
                <span style="color: #a2a1a1;"><?php echo $text_order_comment; ?>:</span>
                <?php echo $comment; ?>
              </div>
              <?php } ?>
          </td>
        </tr>
      </tbody>
    </table>
    
        <table style="border-collapse:collapse;width:100%;margin-bottom: 3px;background-color: #ffffff; color: #a2a1a1;">
      <tbody>
        <tr>
          <td style="font-size:12px; text-align: right; padding: 5px 27px 20px 27px;">
            <div class="payment-block">
              <h1 style="font-weight: normal">תשלום</h1>
              <div class="text">
                מייד עם אישור ההזמנה הסועד ייחויב באמצעות PayPal בסכום מלא הכולל את דמי המשלוח עבור ההזמנה. התשלום המגיע לך בגין הארוחות יועבר אלייך עד 24 שעות לאחר שההזמנה תימסר לסועד.
              </div>
            </div>
          </td>
        </tr>
      </tbody>
    </table>
      
          <table style="border-collapse:collapse;width:100%;margin-bottom: 3px;background-color: #ffffff; color: #a2a1a1;">
      <tbody>
        <tr>
          <td style="font-size:12px; text-align: right; padding: 5px 27px 20px 27px;">
      <div class="payment-block">
        <h1 style="font-weight: normal">סיוע ותמיכה</h1>
        <div class="text">
זוהי הודעת דואר אלקטרוני אוטומטית. לא ניתן להשיב למייל זה. במידה ונתקלת באיזושהי בעיה בהזמנתך, צוות התמיכה שלנו ישמח לסייע לך עד שתהיה שבע רצון, בטלפון:03-3741886 או במייל: support@homeals.com.        </div>
      </div>
</td>
        </tr>
      </tbody>
    </table>
    
    <p style="margin-top:40px;margin-bottom:20px;color: #a2a1a1;text-align: center;">
    <?php echo $text_follow_us; ?><br><br>
    <a href="https://www.facebook.com/pages/Homeals-Israel/608776639200255"><img src="<?php echo $store_url; ?>catalog/view/theme/OPC040088/image/homeals/fbmail.png" /></a>
    <a href="http://instagram.com/homealsisrael"><img src="<?php echo $store_url; ?>catalog/view/theme/OPC040088/image/homeals/instamail.png" /></a>
    </p>
    <p style="margin-top:0px;color: #a2a1a1;text-align: center;">
      הודעה זו נשלחה ל-<?php echo $cook_email; ?> ע״י Homeals Media Ltd, משה דיין 16, ת.ד. 10073 קרית אריה, פתח תקווה.
    </p>
    <p style="padding-bottom:40px;color: #a2a1a1;text-align: center;">
      <span style="display: inline-block;">
      2014 © 		</span>
      <span>
      כל הזכויות שמורות ל- Homeals		</span>
    </p>
    </div>
</div>
</body>
</html>