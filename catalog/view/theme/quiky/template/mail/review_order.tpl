<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/1999/REC-html401-19991224/strict.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title><?php echo $title; ?></title>

<style>
  @import url(http://fonts.googleapis.com/earlyaccess/alefhebrew.css);
  @import url(http://fonts.googleapis.com/css?family=Ubuntu:400,700);
</style>

</head>
<body style="font-family: 'Alef Hebrew','Ubuntu', Arial, Helvetica, sans-serif; font-size: 12px; color: #000000;">
<div style="font-family: 'Alef Hebrew','Ubuntu', Arial, Helvetica, sans-serif; width: 100%; margin: auto; background-color: #FAF7EF; background-image: url('<?php echo $store_url; ?>catalog/view/theme/OPC040088/image/homeals/bg.png');">
  <a href="<?php echo $store_url; ?>" title="<?php echo $store_name; ?>" style="width: 100%; display: block; text-align: center; height: 110px;">
  <img src="<?php echo $logo; ?>" alt="<?php echo $store_name; ?>" style="margin-top: 10px; border: none;" />
  </a>
<div style="width:696px;margin:auto;background-color: #faf7ef;direction: rtl;color: #372c2a;">
  
  
    <table style="border-collapse:collapse;width:100%;margin-bottom: 3px;background-color: #ffffff;">
      <tbody>
        <tr>
          <td style="font-size:12px; text-align: right; padding: 35px 27px 10px 27px;">
              <div style="float: right;font-size: 24px;">
                <span><?php echo $text_review_greeting; ?></span>
              </div>
          </td>
        </tr>
        
        
        <tr>
          <td style="font-size: 14px; padding: 0 27px 15px 27px;">
            <p><?php echo $text_review_text_prolog; ?></p>
            <p><?php echo $text_review_text_epilog; ?></p>
          </td>
        </tr>
        
        
        <tr>
          <td style="font-size: 14px; padding: 0 27px 25px 27px;">
            
            <div>
              <a href="<?php echo $rate_order_url; ?>" style="float: right; background-image: url('<?php echo $store_url; ?>catalog/view/theme/OPC040088/image/homeals/mailbtn.png');height: 40px;line-height: 40px;width: 211px;display: block;text-align: center;text-decoration: none;color: #175a58;font-size: 18px;">
              <?php echo $text_rate_order; ?>
              </a>
            </div>
            
          </td>
        </tr>
      </tbody>
    </table>
    
    <p style="margin-top:20px;margin-bottom:20px;color: #a2a1a1;text-align: center;">
    <?php echo $text_follow_us; ?><br><br>
    <a href="https://www.facebook.com/pages/Homeals-Israel/608776639200255"><img src="<?php echo $store_url; ?>catalog/view/theme/OPC040088/image/homeals/fbmail.png" /></a>
    <a href="http://instagram.com/homealsisrael"><img src="<?php echo $store_url; ?>catalog/view/theme/OPC040088/image/homeals/instamail.png" /></a>
    </p>
    <p style="margin-top:0px;padding-bottom:40px;color: #a2a1a1;text-align: center;"><?php echo $text_homeals_help; ?></p>
      
</div>
</div>
</body>
</html>
