<div class="image-placeholder">
</div>
<?php if(!empty($main_image)){ ?>
<div class="image-con page-image-header">
    <img src="<?php echo $main_image ?>" >
</div>
<?php }?>
<?php if($_GET['route'] == "product/category" ) { ?>
<div class="under-image-panel">
    <div class="concenter1024">
		<div class="table height100">
			<form action="#" class="rest-filter-form" id="rest-filter-form" >
				
				<div class="rest-filter" >
				<span class="filter-name"><?php echo $filter_kitchen ?></span>
				<select name="filter-kitchen" id="filter-kitchen" class="chosen" >
					<option value="1"><?php echo $filter_kitchen_placeholder ?> 1 </option>
					<option value="2"><?php echo $filter_kitchen_placeholder ?> 2 </option>
					<option value="3"><?php echo $filter_kitchen_placeholder ?> 3 </option>
					<option value="4"><?php echo $filter_kitchen_placeholder ?> 4 </option>
					<option value="5"><?php echo $filter_kitchen_placeholder ?> 5 </option>
					<option value="6"><?php echo $filter_kitchen_placeholder ?> 6 </option>
				</select>
				</div>
				<div class="rest-filter" >
				<span class="filter-name"><?php echo $filter_by_cost ?></span>
				<select name="filter-cost" id="filter-cost" class="chosen" >
					<option value="1"><?php echo $filter_by_cost_placeholder ?> 1 </option>
					<option value="2"><?php echo $filter_by_cost_placeholder ?> 2 </option>
					<option value="3"><?php echo $filter_by_cost_placeholder ?> 3 </option>
					<option value="4"><?php echo $filter_by_cost_placeholder ?> 4 </option>
					<option value="5"><?php echo $filter_by_cost_placeholder ?> 5 </option>
					<option value="6"><?php echo $filter_by_cost_placeholder ?> 6 </option>
				</select>
				</div>
				<div class="rest-filter" >
				<span class="filter-name"><?php echo $filter_by_kosher ?></span>
				<select name="filter-kosher" id="filter-kosher" class="chosen" >
					<option value="1"><?php echo $filter_by_kosher_placeholder ?> 1 </option>
					<option value="2"><?php echo $filter_by_kosher_placeholder ?> 2 </option>
					<option value="3"><?php echo $filter_by_kosher_placeholder ?> 3 </option>
					<option value="4"><?php echo $filter_by_kosher_placeholder ?> 4 </option>
					<option value="5"><?php echo $filter_by_kosher_placeholder ?> 5 </option>
					<option value="6"><?php echo $filter_by_kosher_placeholder ?> 6 </option>
				</select>
				</div>
				<div class="rest-filter-button" >
				<input type="submit" id="rest-filter-submit" value="<?php echo $find_order ?>" >
				</div>
				
			</form>
		</div>
	
    </div>
</div>

<div class="under-image-panel-placeholder">
</div>
<?php } else if($_GET['route'] == "ssseller/catalog-seller/profile" ) { ?>
<div class="under-image-panel">
    <div class="concenter1024">
	<div class="onethireds">
	    <div class="rest-info">
			<ul class="rest-info-list">
				<li class="rest-name"><?php echo $seller['name']; ?></li>
				<li class="info_line_1"><?php echo $seller['info_line_1']; ?></li>
				<li class="info_line_2"><?php echo $seller['info_line_2']; ?></li>
				<li class="info_line_3"><?php echo $seller['info_line_3']; ?></li>
			</ul>
	    </div>
	    <div class="rest-logo">
			<img src="<?php echo $seller['image']; ?>" alt="<?php echo $seller['nickname']; ?>" >
	    </div>
	</div>
	<div class="twothireds">
	    <div class="rest-description">
			<?php echo $seller['profile_description']; ?>
	    </div>
	    <div class="rest-costs">
			<ul>
				<li>
				מינימום הזמנה:
				50 ש״ח
				</li>
				<li>|</li>
				<li>
				דמי משלוח:
				15 ש״ח
				</li>
				<li>|</li>
				<li>
				זמן הגעה:
				45 דק׳
				</li>
			</ul>
	    </div>
	</div>
    </div>
</div>

<div class="under-image-panel-placeholder">
</div>

<?php } else { ?>
<script>    
$(document).ready(function() {
    $("#notification").before($(".image-placeholder"));
})
</script>

<?php } ?>
