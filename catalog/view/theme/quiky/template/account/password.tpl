<?php echo $header; ?><?php echo $column_left; ?><?php echo $column_right; ?>
<div id="content"><?php echo $content_top; ?>
    <h1 id="inbox-text"><?php echo $heading_title; ?></h1>
    
    <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data">
        <div class="password-content">
            <table class="form">
                
                <!--
                <tr>
                    <td><span class="required">*</span> <?php echo $entry_my_password; ?></td>
                    <td><input class="homeals-input" type="password" name="my_password" value="<?php echo $my_password; ?>" />
                    <?php if ($error_my_password) { ?>
                    <span class="error"><?php echo $error_my_password; ?></span>
                    <?php } ?></td>
                </tr>
                
                <tr>
                    <td></td>
                    <td>
                        <a href="<?php echo $forgot_my_password_url ?>" >
                            <span class="forgot-it"><?php echo $entry_forgot_my_password; ?></span>
                        </a>
                    </td>
                </tr>
                -->
                
                <tr>
                    <td><span class="required">*</span> <?php echo $entry_password; ?></td>
                    <td><input class="homeals-input" type="password" name="password" value="<?php echo $password; ?>" />
                    <?php if ($error_password) { ?>
                    <span class="error"><?php echo $error_password; ?></span>
                    <?php } ?></td>
                </tr>
                <tr>
                    <td><span class="required">*</span> <?php echo $entry_confirm; ?></td>
                    <td><input class="homeals-input" type="password" name="confirm" value="<?php echo $confirm; ?>" />
                    <?php if ($error_confirm) { ?>
                    <span class="error"><?php echo $error_confirm; ?></span>
                    <?php } ?></td>
                </tr>
            </table>
        </div>
        <div class="moveitleft">
            <div class="right"><input type="submit" value="<?php echo $button_continue; ?>" class="homeals-button" /></div>
        </div>
    </form>
    
    <?php echo $content_bottom; ?>
</div>
<?php echo $footer; ?>