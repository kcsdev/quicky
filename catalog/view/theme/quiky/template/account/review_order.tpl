<?php echo $header; /* ?><?php echo $column_left; ?><?php echo $column_right; */?>
<div id="content"><?php echo $content_top; ?>
    
    
    
    <div class="twothireds" style="margin-right: 0;">
        
        <h1><?php echo $heading_title; ?></h1>
        <div id="cook-time">
            <p>
                <?php echo $text_delivered; ?> <span class="bold"><?php echo $delivery_time; ?></span>
            </p>
        </div>
        
        <div id="seller-tabs" class="htabs">
        <a href="#tab-comments" class="selected" style="display: inline;">
        <?php echo $text_meals; ?>	<img src="catalog/view/theme/OPC040088/image/homeals/spitz.png">
        </a>
        </div>
        
        <?php foreach ($productsByCook as $cook_id => $products) { ?>
        
        
        
        <?php
        /***************************************************************************************************
        *
        *   cook controller start
        *
        */
        
        $last_cook_id = 0;
        
        ?>
        <?php foreach( $cooks as $cook ){
            if($cook['cook_id'] == $cook_id) {
                $last_cook_id = $cook['cook_id'];
            ?>
        
        <div class="side-con">
            
            <div id="cook-badge">
                <div id="cook-image">
                    <img src="<?php echo $cook['image'] ?>" />
                </div>
                <div id="cook-name"><?php echo $cook["firstname"]; ?></div>
                <div id="cook-city"><?php echo $cook["cook_city"]; ?></div>
                <div id="cook-sp-title"><?php echo $text_sp; ?></div>
                <div id="cook-sp"><?php echo $cook["sp"]; ?></div>
                <input type="hidden" id="cook-mail" value="<?php echo $cook["email"]; ?>" />
                <input type="hidden" id="cook-id" value="<?php echo $cook["customer_id"]; ?>" />
            </div>
            
        </div>
        
        <?php } ?>
        <?php } ?>
        <?php
        /*
        *
        *   cook controller end
        *
        ***************************************************************************************************/
        ?>
        
        
        
            <?php foreach ($products as $product) { ?>
            
        <div class="review-meal-con">
            <div class="review-meal-name"><?php echo $product['name']; ?></div>
            
            <div class="review-meal-controler" id="meal-<?php echo $product['product_id']; ?>">
                <div class="main-dish dish-con" >
                    <div class="meal-image">
                        <img src="<?php echo $product['image']; ?>" />
                    </div>
                    <div class="meal-name"><?php echo $product['name']; ?></div>
                    <div class="meal-star-rating">
                        
                        <select class="star-rating" name="star" id="<?php echo $product['product_id']; ?>">
                            <option value=""><?php echo$rate_word[0]; ?></option>
                            <option value="1"><?php echo$rate_word[1]; ?></option>
                            <option value="2"><?php echo$rate_word[2]; ?></option>
                            <option value="3"><?php echo$rate_word[3]; ?></option>
                            <option value="4"><?php echo$rate_word[4]; ?></option>
                            <option value="5"><?php echo$rate_word[5]; ?></option>
                        </select>
                        
                    </div>
                    
                </div>
                
                
                <?php foreach ($product['sides'] as $side) { ?>
                <div class="side-dish dish-con" id="side_<?php echo $side['product_id']; ?>">
                    <div class="meal-image">
                        <img src="<?php echo $side['image']; ?>" />
                    </div>
                    <div class="meal-name"><?php echo $side['name']; ?></div>
                    <div class="meal-star-rating">
                        
                        <select class="star-rating" name="star" id="<?php echo $side['product_id']; ?>">
                            <option value=""><?php echo $rate_word[0]; ?></option>
                            <option value="1"><?php echo $rate_word[1]; ?></option>
                            <option value="2"><?php echo $rate_word[2]; ?></option>
                            <option value="3"><?php echo $rate_word[3]; ?></option>
                            <option value="4"><?php echo $rate_word[4]; ?></option>
                            <option value="5"><?php echo $rate_word[5]; ?></option>
                        </select>
                        
                    </div>
                    
                </div>
                <?php } ?>
                
                <?php
                if(false){
                $i = 0;
                for ($i;$i < $product['number_of_sides'];$i++) {
                ?>
                
                <div class="side-dish dish-con">
                    <div class="select-side-dish">
                        <select class="side-select">
                            <option value="0"><?php echo $text_choose_side; ?></option>
                            <?php foreach ($product['sides'] as $side) { ?>
                            <option value="<?php echo $side['product_id']; ?>"><?php echo $side['name']; ?></option>
                            <?php } ?>
                        </select>
                    </div>
                </div>
                
                <?php   }  } ?>
                
            </div>
            
            <div class="review-meal-text">
                <textarea class="review-textarea" name="text" id="<?php echo $product['product_id']; ?>" placeholder="<?php echo $placeholder_meal; ?>" onfocus="this.placeholder = ''" onblur="this.placeholder = '<?php echo $placeholder_meal; ?>'"></textarea>    
            </div>
            
            <a class="add-privet-review"><?php echo $add_privet_review ?></a>
            <div class="privet-review-meal-text" style="display: none; margin-bottom: 30px;">
                <textarea class="privet-review-textarea" name="cook_pm[<?php echo $last_cook_id; ?>]" data-cook-id="<?php echo $last_cook_id; ?>" data-product-id="<?php echo $product['product_id']; ?>"></textarea>
            </div>
            
        </div>
        <?php } /*  products  */ ?>
        
        <?php } /*  productsByCooks */ ?> 
        
        
        
        <div id="seller-tabs" class="htabs">
        <a href="#tab-comments" class="selected" style="display: inline;">
        <?php echo $text_delivery; ?>
        <img src="catalog/view/theme/OPC040088/image/homeals/spitz.png">
        </a>
        </div>
        
        <div id="delivery-review-con">
            <div id="delivery-review-header"><?php echo $delivery_header; ?></div>
             <div id="delivery-review-controler">
                <div class="yes-no-question">
                    <div class="question"><?php echo $delivery_question[1]; ?></div>
                    <div class="answer" ><input type="radio" name="delivery_answer[1]" value="yes"> <?php echo $text_yes; ?> </div>
                    <div class="answer" ><input type="radio" name="delivery_answer[1]" value="no"> <?php echo $text_no; ?> </div>
                </div>
                <div class="yes-no-question">
                    <div class="question"><?php echo $delivery_question[2]; ?></div>
                    <div class="answer" ><input type="radio" name="delivery_answer[2]" value="yes"> <?php echo $text_yes; ?> </div>
                    <div class="answer" ><input type="radio" name="delivery_answer[2]" value="no"> <?php echo $text_no; ?> </div>
                </div>
                <div class="yes-no-question">
                    <div class="question"><?php echo $delivery_question[3]; ?></div>
                    <div class="answer" ><input type="radio" name="delivery_answer[3]" value="yes"> <?php echo $text_yes; ?> </div>
                    <div class="answer" ><input type="radio" name="delivery_answer[3]" value="no"> <?php echo $text_no; ?> </div>
                </div>
                
                <div style="clear: both"></div>
             </div>
             
             <div class="review-meal-text">
                <textarea class="review-textarea" name="delivery" id="order_id" placeholder="<?php echo $placeholder_delivery; ?>" onfocus="this.placeholder = ''" onblur="this.placeholder = '<?php echo $placeholder_delivery; ?>'"></textarea>    
            </div>
        </div>
        
        <div class="action-buttons-con">
            <button id="submit"><?php echo $text_submit; ?></button>
            <button id="cancel"><?php echo $text_cancel; ?></button>
        </div>
        
    </div>
    
<div id="lightbox-con" style="display: none">   
    <div id="submit-success">
        <div class="success-text">
        
        ביקורת ההזמנה נקלטה במערכת. 
        <br>
        אנחנו מודים לך על תרומתך להעשרת הקהילה שלנו.
        
        </div>
        <button class="goon-bottom">המשך</button>
        <div style="clear: both"></div>
    </div>
    
</div>
    
  <?php echo $content_bottom; ?>
</div>


  
<script type="text/javascript"><!--
$(document).ready(function() {
    
    $("#cancel").on("click",function(){
        location.reload();
    });
    
    $(".goon-bottom").on("click",function(){
        location = "index.php?route=product/category&path=0";
    });
    
    $(".add-privet-review").on("click", function(){
       
        $(this).next().slideToggle("fast");
        $(this).next().focus();
        
    });
    
    $("#submit").on("click",function(){
        var star = {};
        var text = {};
        var cooks_data = new Array();
        
        var cook_pm = $(".privet-review-textarea").val();
        var cook_mail = $("#cook-mail").val();
        var cook_id = $("#cook-id").val();
        
        var i = 0;
        $(".twothireds").find("select").each(function() {
            star[$(this).attr("id")] = $(this).val();
        });
        
        $(".twothireds").find("textarea").each(function() {
            text[$(this).attr("id")] = $(this).val();
        });
        
        $(".privet-review-textarea").each(function() {
            
            pm_cook_id = $(this).attr("data-cook-id");
            product_id = $(this).attr("data-product-id");
            cook_pm_text = $(this).val();
            
            cooks_data.push([pm_cook_id,product_id,cook_pm_text]);
            
        });
        
        var delivery_info = {
            "q1"         : $(".tm-selected input[name=\"delivery_answer[1]\"]").val(),
            "q2"         : $(".tm-selected input[name=\"delivery_answer[2]\"]").val(),
            "q3"         : $(".tm-selected input[name=\"delivery_answer[3]\"]").val(),
            "text"       : $("textarea[name=\"delivery\"]").val(),
            "order_id"   : '<?php echo $_GET['order_id']; ?>',
        }
        
        var data = {
            "stars"         : star,
            "texts"         : text,
            "cooks_data"    : cooks_data,
            "cook_pm"       : cook_pm,
            "cook_mail"     : cook_mail,
            "cook_id"       : cook_id,
            "delivery_info" : delivery_info
        }           
        
        $.ajax({
            url: 'index.php?route=product/product/writemulti',
            type: 'post',
            dataType: 'json',
            data: data,
            beforeSend: function() {
                $('.success, .warning').remove();
                $('#submit').attr('disabled', true);
                $('#submit').html('נשלח');
                showLoad();
                //$('#review-title').after('<div class="attention"><img src="catalog/view/theme/default/image/loading.gif" alt="" /> <?php echo $text_wait; ?></div>');
            },
            complete: function() {
                hideLoad();
                //$('#submit').attr('disabled', false);
                //$('.attention').remove();
            },
            success: function(data) {
                
                console.log(data);
                
                if (data['error']) {
                    //$('#review-title').after('<div class="warning">' + data['error'] + '</div>');
                }
                
                if (data['success']) {
                    //$('#review-title').after('<div class="success">' + data['success'] + '</div>');
                    //$(".twothireds").html('<h1>תודה על הביקורת!</h1>');
                    $.colorbox({href:"#submit-success", inline: true, open: true});
                    
                }
            }
        });
    });
    
    $(".side-select").on("change",function(){
        var side_id = $(this).val();
        $(this).parent().parent().html($("#side_"+side_id).html());
    });
    
    $(".star-rating").barrating('show', {
        showSelectedRating:false,
        showValues:false,
        showSelectedRating:true
    });
    
    
});
//--></script>
  
  
<?php echo $footer; ?>