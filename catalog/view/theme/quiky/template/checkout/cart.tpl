<?php echo $header; ?>

<?php if ($attention) { ?>
<div class="attention"><?php echo $attention; ?><img src="catalog/view/theme/default/image/close.png" alt="" class="close" /></div>
<?php } ?>
<?php if ($success) { ?>
<div class="success"><?php echo $success; ?><img src="catalog/view/theme/default/image/close.png" alt="" class="close" /></div>
<?php } ?>
<?php if ($error_warning) { ?>
<div class="warning"><?php echo $error_warning; ?><img src="catalog/view/theme/default/image/close.png" alt="" class="close" /></div>
<?php } ?>
<?php //echo $column_left; ?><?php echo $column_right; ?>
<div id="content"><?php echo $content_top; ?>
  <div class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
    <?php } ?>
  </div>
  <h1 class="cart-header"><?php// echo $heading_title; ?>
    <?php if ($weight) { ?>
    &nbsp;<?php //echo $weight; ?>
    <?php } ?>
     סיכום הזמנה
  </h1>
  <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data">
    <div class="cart-info">
      <table id="cart-table">
        <thead>
          <tr id="first">
            <td class="image" style="display:none"><?php echo $column_image; ?></td>
              <td class="name">הזמנה מ<span id="rest"><?php echo  $rest ?></span><span id="rest_id" style="display:none"><?php echo $rest_id ?></span></td>
              <td class="price"><?php echo $column_price; ?></td>
            <td class="model" style="display:none"><?php echo $column_model; ?></td>
            <td class="quantity"><?php echo $column_quantity; ?></td>
            <td class="total"><?php echo $column_total; ?></td>
              <td><div class="what">?</div> ערוך את המנה</td>
          </tr>
        </thead>
        <tbody>
          <?php foreach ($products as $product) { ?>
	  
            <?php if(false): //$product['recurring']?>
              <tr>
                  <td colspan="6" style="border:none;"><image src="catalog/view/theme/default/image/reorder.png" alt="" title="" style="float:left;" /><span style="float:left;line-height:18px; margin-left:10px;"> 
                      <strong><?php echo $text_recurring_item ?></strong>
                      <?php echo $product['profile_description'] ?>
                  </td>
                </tr>
            <?php endif; ?>
	    
          <tr>
            <td class="image" style="display:none"><?php if ($product['thumb']) { ?>
              <a href="<?php echo $product['href']; ?>"><img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>" /></a>
              <?php } ?></td>
            <td class="name"><a href="<?php echo $product['remove']; ?>"><img class="img_remove" src="catalog/view/theme/default/image/remove.png" alt="<?php echo $button_remove; ?>" title="<?php echo $button_remove; ?>" /></a>&nbsp;<a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a>
              <span class="key" style="display:none"><?php echo $product['key']; ?></span>
              <?php if (!$product['stock']) { ?>
              <span class="stock">***</span>
              <?php } ?>
              <div>
                <?php foreach ($product['option'] as $option) { ?>
                - <small><?php echo $option['name']; ?>: <?php echo $option['value']; ?></small><br />
                <?php } ?>
                <?php if(false): //$product['recurring'] ?>
                - <small><?php echo $text_payment_profile ?>: <?php echo $product['profile_name'] ?></small>
                <?php endif; ?>
              </div>
              <?php if ($product['reward']) { ?>
              <small><?php echo $product['reward']; ?></small>
              <?php } ?></td>
            <td class="model" style="display:none"><?php echo $product['model']; ?></td>
             <td class="price"><?php echo $product['price']; ?></td>
            <td class="quantity"><input type="button" class="plus edit_qty" value="+"><input type="text" name="quantity[<?php echo $product['key']; ?>]" value="<?php echo $product['quantity']; ?>" size="1" class="qty"  readonly/><input type="button" class="minus edit_qty" value="-">
              &nbsp;
              &nbsp;</td>
           
            <td class="total"><?php echo $product['total']; ?></td>
            <td><!--<input type="image" src="catalog/view/theme/default/image/update.png" alt="<?php echo $button_update; ?>" title="<?php echo $button_update; ?>" />--> עריכה</td>
          </tr>
          <?php } ?>
          <?php foreach ($vouchers as $vouchers) { ?>
          <tr>
            <td class="image"></td>
            <td class="name"><?php echo $vouchers['description']; ?></td>
            <td class="model"></td>
            <td class="quantity"><input type="text" name="" value="1" size="1" disabled="disabled" />
              &nbsp;<a href="<?php echo $vouchers['remove']; ?>"><img src="catalog/view/theme/default/image/remove.png" alt="<?php echo $button_remove; ?>" title="<?php echo $button_remove; ?>" /></a></td>
            <td class="price"><?php echo $vouchers['amount']; ?></td>
            <td class="total"><?php echo $vouchers['amount']; ?></td>
          </tr>
          <?php } ?>
        </tbody>
      </table>
    </div>
  </form>
  
  <?php if (false && ($coupon_status || $voucher_status || $reward_status || $shipping_status)) { ?>
  <div class="cart-left-area"> <!-- Magento extra Div start -->
		<h2><?php echo $text_next; ?></h2>
		<div class="content">
    <p><?php echo $text_next_choice; ?></p>
    <table class="radio">
      <?php if ($coupon_status) { ?>
      <tr class="highlight">
        <td><?php if ($next == 'coupon') { ?>
          <input type="radio" name="next" value="coupon" id="use_coupon" checked="checked" />
          <?php } else { ?>
          <input type="radio" name="next" value="coupon" id="use_coupon" />
          <?php } ?></td>
        <td><label for="use_coupon"><?php echo $text_use_coupon; ?></label></td>
      </tr>
      <?php } ?>
      <?php if ($voucher_status) { ?>
      <tr class="highlight">
        <td><?php if ($next == 'voucher') { ?>
          <input type="radio" name="next" value="voucher" id="use_voucher" checked="checked" />
          <?php } else { ?>
          <input type="radio" name="next" value="voucher" id="use_voucher" />
          <?php } ?></td>
        <td><label for="use_voucher"><?php echo $text_use_voucher; ?></label></td>
      </tr>
      <?php } ?>
      <?php if ($reward_status) { ?>
      <tr class="highlight">
        <td><?php if ($next == 'reward') { ?>
          <input type="radio" name="next" value="reward" id="use_reward" checked="checked" />
          <?php } else { ?>
          <input type="radio" name="next" value="reward" id="use_reward" />
          <?php } ?></td>
        <td><label for="use_reward"><?php echo $text_use_reward; ?></label></td>
      </tr>
      <?php } ?>
      <?php if ($shipping_status) { ?>
      <tr class="highlight">
        <td><?php if ($next == 'shipping') { ?>
          <input type="radio" name="next" value="shipping" id="shipping_estimate" checked="checked" />
          <?php } else { ?>
          <input type="radio" name="next" value="shipping" id="shipping_estimate" />
          <?php } ?></td>
        <td><label for="shipping_estimate"><?php echo $text_shipping_estimate; ?></label></td>
      </tr>
      <?php } ?>
    </table>
  </div>
		<div class="cart-module">
    <div id="coupon" class="content" style="display: <?php echo ($next == 'coupon' ? 'block' : 'none'); ?>;">
      <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data">
        <?php echo $entry_coupon; ?>&nbsp;<br />
        <input type="text" name="coupon" value="<?php echo $coupon; ?>" />
        <input type="hidden" name="next" value="coupon" />
        &nbsp;
        <input type="submit" value="<?php echo $button_coupon; ?>" class="button" />
      </form>
    </div>
    <div id="voucher" class="content" style="display: <?php echo ($next == 'voucher' ? 'block' : 'none'); ?>;">
      <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data">
        <?php echo $entry_voucher; ?>&nbsp;
        <input type="text" name="voucher" value="<?php echo $voucher; ?>" />
        <input type="hidden" name="next" value="voucher" />
        &nbsp;
        <input type="submit" value="<?php echo $button_voucher; ?>" class="button" />
      </form>
    </div>
    <div id="reward" class="content" style="display: <?php echo ($next == 'reward' ? 'block' : 'none'); ?>;">
      <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data">
        <?php echo $entry_reward; ?>&nbsp;
        <input type="text" name="reward" value="<?php echo $reward; ?>" />
        <input type="hidden" name="next" value="reward" />
        &nbsp;
        <input type="submit" value="<?php echo $button_reward; ?>" class="button" />
      </form>
    </div>
    <div id="shipping" class="content" style="display: <?php echo ($next == 'shipping' ? 'block' : 'none'); ?>;">
      <p><?php echo $text_shipping_detail; ?></p>
      <table>
        <tr><td><span class="required">*</span> <?php echo $entry_country; ?></td></tr>
		<tr><td><select name="country_id">
              <option value=""><?php echo $text_select; ?></option>
              <?php foreach ($countries as $country) { ?>
              <?php if ($country['country_id'] == $country_id) { ?>
              <option value="<?php echo $country['country_id']; ?>" selected="selected"><?php echo $country['name']; ?></option>
              <?php } else { ?>
              <option value="<?php echo $country['country_id']; ?>"><?php echo $country['name']; ?></option>
              <?php } ?>
              <?php } ?>
            </select></td></tr>
        <tr><td><span class="required">*</span> <?php echo $entry_zone; ?></td></tr>
        <tr><td><select name="zone_id"></select></td></tr>
        <tr><td><span id="postcode-required" class="required">*</span> <?php echo $entry_postcode; ?></td></tr>
		<tr><td><input type="text" name="postcode" value="<?php echo $postcode; ?>" /></td></tr>
      </table>
      <input type="button" value="<?php echo $button_quote; ?>" id="button-quote" class="button" />
    </div>
  </div>
  </div> <!-- Magento extra Div End -->
  <?php } ?>
  <div class="cart-right-area"> <!-- Magento extra Div start -->
		<div class="cart-total">
    <table id="total" class="new_total">
      <tr>
          <td> <b>סיכום ביניים-</b></td> 
          <td class="second"><span id="fix_price"><?= $new_price ?></span> ש״ח</td>
          <td><b> דמי משלוח-</b></td>
          <?php if(!empty($this->session->data['pay'])){ ?>
          <td class="second"><span id="delivery"><?= $this->session->data['pay']?></span> ש״ח</td>
          <?php }else{ ?>
            <td class="second"><span id="delivery">0</span>ש״ח</td>
          <?php } ?>
          <?php if((isset($cost)) && (!empty($cost))){ ?>
          <td><b>סה״כ לתשלום- </b><br><span class="small-check">(מינימום הזמנה <?= $cost->min_order ?> ש״ח)</span></td>
           <?php }else{ ?>
           <td><b>סה״כ לתשלום- </b><br><span class="small-check">(מינימום הזמנה 0 ש״ח)</span></td>
           <?php } ?>
          <?php if(!empty($this->session->data['pay'])){ ?>
          <td class="second"><span id="final"><?php echo ($this->session->data['pay'] + $new_price) ?></span> ש״ח</td>
          <?php }else{ ?>
          <td class="second"><span id="final"><?php echo ($new_price) ?></span>ש״ח</td>
            <?php } ?>
          <td id="text-td"><b>הערות נוספות למסעדה</b><textarea rows="4" cols="8" placeholder="כתוב הערה..."></textarea></td>
          <td><span style="color:#ad2121">אתם לא אוכלים מספיק!</span><br>הוסיפו מנות,שתיה,קינוחים להזמנה זו<br><a href="<?php echo 'index.php?route=seller/catalog-seller/profile&seller_id=' . $rest_id; ?>" class="button"><?php echo $button_shopping; ?></a></td>
      <?php foreach ($totals as $total) { ?>
    <!--  <tr> -->
      <!--  <td class="right"><b><?php echo $total['title']; ?>:</b></td>-->
      <!--  <td class="right"><?php echo $total['text']; ?></td>-->
     <!-- </tr>-->
      <?php } ?>
    </tr>
    </table>	
  </div>
<?php if(!empty($specail)){ ?>  
<div class="row">
    <div class="col-md-12">
        <h1 class="cart-header">תוספות להזמנה</h1>
        <h3>מדיניות הרטבים של המסעדה</h3>
        <p>
        האחריות הבלעדית על אספקת המוצרים המוצעים באתר ולטיבם ואיכותם חלה לפי העניין של היצרנים.<br>
        האחריות הבלעדית לאספקת המוצרים המוצעים באתר ולטיבם.
        </p>
    </div>
<div class="col-md-12">
 <?php foreach($specail as $op){ ?>
<input type="checkbox" name="qty" class="special_add">&nbsp;&nbsp;<label><?php echo $op['name'] ?></label>&nbsp;&nbsp;<input type="button" class="up  edit_special" value="+"><input type="text" name="quantity_special" value="1" size="1" class="qty"  readonly/><input type="button" class="down  edit_special" value="-"><br><br>
<?php } ?>
</div>
</div>
  <hr>
  <?php } ?>
<div class="row form-row">
 <div class="col-form"> 
 <div class="checkout-header">
 פרטים אישיים
</div>
<div class="form-details">
<label>שם מלא:</label>     
    <input type="text" name="name" value="<?php echo $this->customer->getFirstName() . ' ' . $this->customer->getLastName() ?>">
    <span class="required">*</span>
</div>  
<div class="form-details">
<label>טלפון:</label>     
<input type="text" name="phone" value="<?php echo $this->customer->getTelephone() ?>"> 
<span class="required">*</span>
</div>  
<div class="form-details">
<label>אימייל:</label>     
<input type="email" name="mail" value="<?php echo $this->customer->getEmail() ?>"> 
<span class="required">*</span>
</div>
<div class="form-details">
<input type="checkbox" class="mychbox" checked>
<label class="label-check">מאשר לשלוח לי מידע על מבצעים מיוחדים<br>קוויקי לעולם לא תמסור את כתובת המייל לגורם שלישי</label>
</div>

 <div class="checkout-header" style="width:88%">
חברה | בית עסק | מוסד ציבורי
</div> 
<div class="what no-margin take-top">?</div>
<div class="form-details">
<label>שם חברה/מוסד:</label>     
<input type="text" name="company">     
</div>  
<div class="form-details">
<label>מספר טלפון נוסף:</label>     
<input type="text" name="company_phone">     
</div>
<br><br> 
<div class="form-details">
<span class="required">שימו לב:</span><br>
    אנו עשויים להתקשר ע״מ לקבל הבהרות בקשר להזמנה.<br>
    אנא וודאו שהמספר נכון<br><br>
    <span class="required">*</span> = שדה חובה
    
</div>

</div>      
 
<div class="col-form">
 <div class="checkout-header">
 כתובת למשלוח
</div>
<div class="form-details">
<label>עיר:</label>     
<input type="text" name="city" value="<?php echo $this->session->data['city'] ?>" readonly>  
<span class="required">*</span>
</div>     
<div class="form-details">
<label>רחוב/יעד:</label>     
<input type="text" name="street"> 
<span class="required">*</span>
</div>     
<div class="form-details">
<label>מספר בית:</label>     
<input type="text" name="entrence" class="small"> 
<label class="small">כניסה:</label>     
<input type="text" name="house_number" class="small">
<span class="required">*</span>
</div>  
<div class="form-details">
<label>קומה:</label>     
<input type="text" name="apartment_number" class="small"> 
<label class="small">מספר דירה:</label>     
<input type="text" name="floor" class="small"> 
</div>  
<div class="form-details">
<label>הערות לכתובת:</label>  
<div class="what no-margin text-comm">?</div> 
<textarea name="comments" placeholder="הכניסה מסביב, דלת אדומה וכדומה." style="width:66%;"></textarea>

</div>   
</div>  
    
<div class="col-form">
 <div class="checkout-header" style="width:88%">
כדאי להירשם:
</div>
<div class="what no-margin take-top">?</div>   
<div class="form-details">
<label>סיסמא מבוקשת:</label>     
<input type="password" name="regis_password">  
<span class="required">*</span>
</div> 
<div class="form-details">
<label>חזור על הסיסמא:</label>     
<input type="password" name="re_password">     
</div>
 <div class="checkout-header">
 מתי להגיע?
     
<select class="time time-first">
    <option value="00">00</option>
    <option value="15">15</option> 
    <option value="30">30</option> 
    <option value="45">45</option> 
</select>     
     <span class="time-seperete">:</span> 
<select class="time hour">
    <option value="12">12</option>
    <option value="13">13</option> 
    <option value="14">14</option> 
    <option value="15">15</option>
    <option value="16">16</option>
    <option value="17">17</option> 
    <option value="18">18</option> 
    <option value="19">19</option>
</select>
</div>
<div class="form-details"> 
    <input type="button" class="button" id="add_order" value="קדימה, אוכל!">   
 
</div>    
    

</div>
</div>     
      
  <div class="checkout-button" style="display:none"><a href="<?php echo $checkout; ?>" class="button"><?php echo $button_checkout; ?></a></div>	
  </div> <!-- Magento extra Div End -->
  <div class="cart-clear">&nbsp;</div> <!-- Magento extra Div -->
  <div class="buttons cart-buttons" style="display:none">    
    <div class="center"><a href="<?php echo $continue; ?>" class="button"><?php echo $button_shopping; ?></a></div>
  </div>
  <?php echo $content_bottom; ?></div>
<script type="text/javascript"><!--
$('input[name=\'next\']').bind('change', function() {
	$('.cart-module > div').hide();
	
	$('#' + this.value).show();
});
//--></script>
<?php if ($shipping_status) { ?>
<script type="text/javascript"><!--
$('#button-quote').live('click', function() {
	$.ajax({
		url: 'index.php?route=checkout/cart/quote',
		type: 'post',
		data: 'country_id=' + $('select[name=\'country_id\']').val() + '&zone_id=' + $('select[name=\'zone_id\']').val() + '&postcode=' + encodeURIComponent($('input[name=\'postcode\']').val()),
		dataType: 'json',		
		beforeSend: function() {
			$('#button-quote').attr('disabled', true);
			$('#button-quote').after('<span class="wait">&nbsp;<img src="catalog/view/theme/default/image/loading.gif" alt="" /></span>');
		},
		complete: function() {
			$('#button-quote').attr('disabled', false);
			$('.wait').remove();
		},		
		success: function(json) {
			$('.success, .warning, .attention, .error').remove();			
						
			if (json['error']) {
				if (json['error']['warning']) {
					$('#notification').html('<div class="warning" style="display: none;">' + json['error']['warning'] + '<img src="catalog/view/theme/default/image/close.png" alt="" class="close" /></div>');
					
					$('.warning').fadeIn('slow');
					
					$('html, body').animate({ scrollTop: 0 }, 'slow'); 
				}	
							
				if (json['error']['country']) {
					$('select[name=\'country_id\']').after('<span class="error">' + json['error']['country'] + '</span>');
				}	
				
				if (json['error']['zone']) {
					$('select[name=\'zone_id\']').after('<span class="error">' + json['error']['zone'] + '</span>');
				}
				
				if (json['error']['postcode']) {
					$('input[name=\'postcode\']').after('<span class="error">' + json['error']['postcode'] + '</span>');
				}					
			}
			
			if (json['shipping_method']) {
				html  = '<h2><?php echo $text_shipping_method; ?></h2>';
				html += '<form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data">';
				html += '  <table class="radio">';
				
				for (i in json['shipping_method']) {
					html += '<tr>';
					html += '  <td colspan="3"><b>' + json['shipping_method'][i]['title'] + '</b></td>';
					html += '</tr>';
				
					if (!json['shipping_method'][i]['error']) {
						for (j in json['shipping_method'][i]['quote']) {
							html += '<tr class="highlight">';
							
							if (json['shipping_method'][i]['quote'][j]['code'] == '<?php echo $shipping_method; ?>') {
								html += '<td><input type="radio" name="shipping_method" value="' + json['shipping_method'][i]['quote'][j]['code'] + '" id="' + json['shipping_method'][i]['quote'][j]['code'] + '" checked="checked" /></td>';
							} else {
								html += '<td><input type="radio" name="shipping_method" value="' + json['shipping_method'][i]['quote'][j]['code'] + '" id="' + json['shipping_method'][i]['quote'][j]['code'] + '" /></td>';
							}
								
							html += '  <td><label for="' + json['shipping_method'][i]['quote'][j]['code'] + '">' + json['shipping_method'][i]['quote'][j]['title'] + '</label></td>';
							html += '  <td style="text-align: right;"><label for="' + json['shipping_method'][i]['quote'][j]['code'] + '">' + json['shipping_method'][i]['quote'][j]['text'] + '</label></td>';
							html += '</tr>';
						}		
					} else {
						html += '<tr>';
						html += '  <td colspan="3"><div class="error">' + json['shipping_method'][i]['error'] + '</div></td>';
						html += '</tr>';						
					}
				}
				
				html += '  </table>';
				html += '  <br />';
				html += '  <input type="hidden" name="next" value="shipping" />';
				
				<?php if ($shipping_method) { ?>
				html += '  <input type="submit" value="<?php echo $button_shipping; ?>" id="button-shipping" class="button" />';	
				<?php } else { ?>
				html += '  <input type="submit" value="<?php echo $button_shipping; ?>" id="button-shipping" class="button" disabled="disabled" />';	
				<?php } ?>
							
				html += '</form>';
				
				$.colorbox({
					overlayClose: true,
					opacity: 0.5,
					width: '600px',
					height: '400px',
					href: false,
					html: html
				});
				
				$('input[name=\'shipping_method\']').bind('change', function() {
					$('#button-shipping').attr('disabled', false);
				});
			}
		}
	});
});
//--></script> 
<script type="text/javascript"><!--
$('select[name=\'country_id\']').bind('change', function() {
	$.ajax({
		url: 'index.php?route=checkout/cart/country&country_id=' + this.value,
		dataType: 'json',
		beforeSend: function() {
			$('select[name=\'country_id\']').after('<span class="wait">&nbsp;<img src="catalog/view/theme/default/image/loading.gif" alt="" /></span>');
		},
		complete: function() {
			$('.wait').remove();
		},			
		success: function(json) {
			if (json['postcode_required'] == '1') {
				$('#postcode-required').show();
			} else {
				$('#postcode-required').hide();
			}
			
			html = '<option value=""><?php echo $text_select; ?></option>';
			
			if (json['zone'] != '') {
				for (i = 0; i < json['zone'].length; i++) {
        			html += '<option value="' + json['zone'][i]['zone_id'] + '"';
	    			
					if (json['zone'][i]['zone_id'] == '<?php echo $zone_id; ?>') {
	      				html += ' selected="selected"';
	    			}
	
	    			html += '>' + json['zone'][i]['name'] + '</option>';
				}
			} else {
				html += '<option value="0" selected="selected"><?php echo $text_none; ?></option>';
			}
			
			$('select[name=\'zone_id\']').html(html);
		},
		error: function(xhr, ajaxOptions, thrownError) {
			alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
		}
	});
});

$('select[name=\'country_id\']').trigger('change');
//--></script>
<?php } ?>

<script>
$('input.edit_qty').on('click', function(){
var deliver = parseFloat($('span#final').text());
var qty_id = $(this).attr('class');
var qty_arr = qty_id.split(" ");
var qty_class=qty_arr[0];
if(qty_class == 'plus'){
  var val = parseInt($(this).next().val());
  if(val >= 1){
    $(this).next().val(val + 1);
    var qty_send =  val + 1;
     var key = $(this).parent().prev().prev().prev().children('span.key').html();
     editfinelCart(key, qty_send, $(this).parent().next(), deliver, qty_class);
     
  }
}else{

var val = parseInt($(this).prev().val());
      if(val > 1){
    $(this).prev().val(val - 1)
    var qty_send =  val - 1;
    var key = $(this).parent().prev().prev().prev().children('span.key').html();
    editfinelCart(key, qty_send, $(this).parent().next(), deliver, qty_class);
     
  }

}


});
    
$('input.edit_special').on('click', function(){
var qty_id = $(this).attr('class');
var qty_arr = qty_id.split(" ");
var qty_class=qty_arr[0];
if(qty_class == 'up'){
  var val = parseInt($(this).next().val());
  if(val >= 1){
     $(this).next().val(val + 1)
    
  }
}else{

var val = parseInt($(this).prev().val());
      if(val > 1){
     $(this).prev().val(val - 1)
    
  }

}


});

</script>
<?php echo $footer; ?>
