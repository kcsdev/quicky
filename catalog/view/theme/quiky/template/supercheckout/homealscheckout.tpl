<?php if(($settings['step']['login']['option']['guest']['display']) && ($facebook_enable || $google_enable) ){ 
    $login_boxes_width=31.1;
}else{
    $login_boxes_width=47.7;
}
?>
<style>
    body{margin: 0 !important;}
.supercheckout_top_boxes{width:<?php echo $login_boxes_width; ?>%;}
.supercheckout_login_option_box{
    background-color:rgba(7, 8, 5, 0.02) !important
}
</style>


<div style="display: none" >
<div id="open-checkout" class="checkout-con">
    
<div id="edit-cart">
    <div class="edit-box">
        <div class="loading-edit" ></div>
        
        <input type="hidden" name="product_id" value="" />
        <input type="hidden" name="option[date]" value="" />
        <input type="hidden" name="option[cook_id]" value="" />
        <input type="hidden" name="option[time-of-day]" value="<?php echo $time_key; ?>" />
        
        <div class="title">
            <h1><?php echo $text_order_summary_part_1; ?></h1>
        </div>
        
        <div class="edit-left-box">
            <!-- left side for future -->
        </div>
        
        <div class="white-box" id="qty-box">
        
            <div>
                <span class="edit-title">תוספות:</span>
                
                <div class="select-con">
                    <select name="option[side-1]" data-name="side-1">
                    </select>
                </div>
                
                <div class="select-con">
                    <select name="option[side-2]" data-name="side-2">
                    </select>
                </div>
            </div>
            
            <div style="height: 90px;">
                <span class="edit-title" style="margin-bottom: 0px;">כמות:</span>
                <div class="plus-minus-controller">
                    <div class="edit-minus-button">-</div>
                    <div class="input"><span>ארוחות</span>
                    <input type="text" id="edit-quantity-input" name="quantity" value="1" readonly="readonly"></div>
                    <div class="edit-plus-button">+</div>
                </div>
                
                <span class="text_qty">0 נותרו</span>
            </div>
            
            <div class="edit-title">
                הערה לבשלן:
            </div>
            
            <div id="comment" style="display: block;">
                <textarea name="option[comment]" style="resize: none;"></textarea>
                <div style="clear: both"></div>
            </div>
            
        </div>
        
        <div class="cart" id="total-box">
        
            <span>סה"כ:</span>
                <input id="edit-int-price" type="hidden" value="0">
                <input id="edit-extra-price" type="hidden" value="0">
            <span id="edit-total-price" style="float: left">₪0</span>
        
        </div>
        
        <div style="clear: both"></div>
        <div class="action-buttons-con" style="width: 256px;">
            <button id="button-edit-cart" >עדכן</button>
            <button id="button-edit-cancel">בטל</button>
	</div>
        <div style="clear: both"></div>
        
    </div>
</div>
    
    
<fieldset class="group-select" id="supercheckout-fieldset">
    
    <div class="close-btn">X</div>
    
    <div class="supercheckout-threecolumns supercheckout-container supercheckout-skin-generic " id="supercheckout-columnleft">
        <?php 
        if($settings['general']['layout']=='3-Column'){
            $layout_name='three-column';
            $multiplier=0.895;
        }elseif($settings['general']['layout']=='2-Column'){
            $layout_name='two-column';
            $multiplier=0.947;
        }elseif($settings['general']['layout']=='1-Column'){
            $layout_name='one-column';
            $multiplier=1;
        }
        
        ?>
        <div class="supercheckout-column-left columnleftsort" id="columnleft-1" style="width:<?php $i= $settings['general']['column_width'][$layout_name][1]*$multiplier; echo $i; ?>%"> 
        <div class="title">
            <h2><?php echo $text_order_summary_part_1; ?></h2>
            <h1><?php echo $text_order_summary_part_2; ?></h1>
        </div>
        
        <div class="homeals-summary" >
            <div class="number-of-meals" >
                <span class="text" ><?php echo $text_number_of_meals; ?></span>
                <span class="price" id="total-pro">1</span>
            </div>
            <div style="clear: both"></div>
            <div class="shipping-cost" >
                <span class="text" ><?php echo $text_shipping_cost; ?></span>
                <span class="price" id="total-shipping-cost">15</span>
            </div>
            <div style="clear: both"></div>
            <hr class="total-coupon total-credit" style="display: none">
            <div class="total-coupon total-credit" style="display: none">
                <span class="text"><?php echo $text_total_meals; ?></span>
                <span class="price" id="homeals-subtotal-price">10</span>
            </div>
            <div style="clear: both"></div>
            <div class="total-credit" style="display: none">
                <span class="text"><?php echo $text_total_coupon; ?></span>
                <span class="price" id="homeals-credit-price" style="direction: ltr;">10</span>
            </div>
            <div style="clear: both"></div>
            <div class="total-coupon" style="display: none">
                <span class="text"><?php echo $text_total_coupon; ?></span>
                <span class="price" id="homeals-coupon-price" style="direction: ltr;">10</span>
            </div>
            <div style="clear: both"></div>
                
                
            <hr>
            <div class="total-price" >
                <span class="total"><?php echo $text_total_meals; ?></span>
                <span class="price" id="homeals-total-price">10</span>
            </div>
            <div style="clear: both"></div>
        </div>
	
        <?php if($hasCoupon) { ?> 
        <div class="homeals-coupon-input">
            <table>
            <tr style="display:block;">
		
                <td class="title"><b><?php echo "קוד קופון"; ?></b></td>
                <td class="value"><input  id="coupon_code" name="coupon" type="text" class="voucherText">
                    <input type="hidden" value="coupon" name="next">
                    <input id="button-coupon" type="button" onClick="if(window.couponBlur==true){ callCoupon(); }" class="orangebuttonapply" value="הוסף">
                </td>
            </tr>
            <tr id="homeals-coupon-warn" style="display:none;">
                <td>קוד לא תקין</td>
            </tr>
            </table>
        </div>
        <?php } ?>
	
	<div class="homeals-environment">
	    
	    <h1>תשלום</h1>
	    <h2><?php echo $text_help_save ?></h2>
	    
	    <div id="need-tools">
		<input type="checkbox" name="tools" id="tools" />
		<?php echo $text_need_tools ?>
	    </div>
	    
	</div>
	
        <div class="homeals-payment">
            
            <div class="pay-with-credit" id="placeorderButton"><?php echo $text_pay_credit; ?></div>
	    <div id="paypalicons"></div>
	    <div id="or">או</div>
	    <div class="pay-with-paypal" id="placeorderButton"><?php echo "";//$text_pay_pal; ?></div>
            
        </div>
        
            <div  class="supercheckout-blocks" data-column="<?php echo $sort_block['login'][$layout_name]['column']; ?>" data-row="<?php echo $sort_block['login'][$layout_name]['row']; ?>" data-column-inside="<?php echo $sort_block['login'][$layout_name]['column-inside']; ?>"  >
                <ul class="headingCheckout">
                    <li>
                        <p class="supercheckout-numbers supercheckout-numbers-1"><?php if(!$logged){ echo $text_login_option;}else{echo 'Welcome '.$firstName. ' '.$lastName;} ?></p>

                    </li>
                </ul>
                <div id="checkoutLogin">
                    <div class="supercheckout-checkout-content">

                    </div>
                    <?php if(!$logged){  ?>
                    <div class="supercheckout-extra-wrap">
                        <b><?php echo $entry_email; ?><span class="supercheckout-required">*</span></b><br />
                        <input type="text" id="email" name="email" value="" class="supercheckout-large-field" />
                        <br/>
                    </div>    
                    <div id="loginDetails" style="display:<?php if($settings['step']['login']['option']['guest']['display'] ){ echo 'block'; }else{ echo 'none';} ?>;">
                        
                        <div class="supercheckout-extra-wrap">
                            <label for="guest">
                                <?php if ($account == 'guest') { ?>
                                <input type="radio" name="account" value="guest" id="guest" checked="checked" />
                                <?php } else { ?>
                                <input type="radio" name="account" value="guest" id="guest" />
                                <?php } ?>
                                <b><?php echo $text_guest; ?></b></label>
                            <br />
                        </div>
                        
                        <div class="supercheckout-extra-wrap">
                            <label for="register">
                                <?php if ($account == 'register') { ?>
                                <input type="radio" name="account" value="register" id="register" checked="checked" />
                                <?php } else { ?>
                                <input type="radio" name="account" value="register" id="register" />
                                <?php } ?>
                                <b><?php echo $text_register; ?></b></label>
                            <br />
                        </div>                    
                    </div>
                    <div id="supercheckout-login">
                        <div class="supercheckout-extra-wrap">
                            <b><?php echo $entry_password; ?><span class="supercheckout-required">*</span></b><br />
                            <input type="password" id="password" name="password" value="" class="supercheckout-large-field" />
                            <br />
                            <div id="forgotpasswordlink"><a href="<?php echo $forgotten; ?>"><?php echo $text_forgotten; ?></a></div>
                            <br />
                            <input type="button" value="<?php echo $button_login; ?>" id="button-login" class="orangebuttonsmall" /><br />
                        </div>
                    </div> 
                    <?php if($facebook_enable || $google_enable) {   ?>
                    <div class="orSeparator"><span><?php echo $text_OR_separator; ?></span></div>
                    <h3><?php echo $text_sign_in_with; ?></h3>
                    <div class="socialNetwork">
                        <?php if($facebook_enable){ ?>
                        <!-- <div class="fbButton" id="fb-auth" ></div> -->
                        <?php }if($google_enable){ ?>
                        <div class="googleButton" onclick="window.open('<?php echo $url; ?>', 'name','resizable=1,scrollbars=no,width=500,height=400')"></div>
                        <?php } ?>
                        <div class="supercheckout-clear"></div>
                    </div>
                    <?php } ?>
                    <?php }else{ ?>
                    <div class="myaccount">
                        <ol class="rectangle-list">                            
                            <li>
                                <a href="<?php echo $myAccount; ?>"><?php echo $text_my_account; ?></a>
                            </li>
<!--                            <li style="float:left;">
                                <a href="<?php echo $myOrder; ?>"><?php echo $text_my_orders; ?></a>
                            </li>-->
                            <li>
                                <a href="<?php echo $logoutLink; ?>"><?php echo $text_logout; ?></a>
                            </li>
                            <div class="supercheckout-clear"></div>

                        </ol>
                    </div>

                    <?php } ?>
                </div>
            </div>
            
            <div  class="supercheckout-blocks"  data-column="<?php echo $sort_block['payment_address'][$layout_name]['column']; ?>" data-row="<?php echo $sort_block['payment_address'][$layout_name]['row']; ?>" data-column-inside="<?php echo $sort_block['payment_address'][$layout_name]['column-inside']; ?>">
                <ul>
                    <li>
                        <p class="supercheckout-numbers supercheckout-numbers-2"><?php echo $text_billing_address; ?></p>
                    </li>
                </ul>
                <div id="checkoutBillingAddress">
                    <?php
                    
                        // checking for address type
                        $address_type = (isset($this->session->data['address_type']) ? $this->session->data['address_type'] : '');
                        
                        if ($addresses) {
                        
                        
                        $def_address_string = $def_address['address_1'].",".$def_address['city'] ;
                        
                        ?>
                    
                    <div class="supercheckout-checkout-content" >
                    </div>
                    
                    <div class="supercheckout-extra-wrap" style="display: none">
                        <input type="radio" name="payment_address" value="existing" id="payment-address-existing" <?php echo ( $address_type == "def" ? 'checked="checked"' : '' ); ?>  />
                        <label for="payment-address-existing"><?php echo $text_address_existing; ?></label>
                    </div>    
                    <div id="payment-existing" style="display: none">
                        <select id="payment-existing-address-id" name="address_id" style="width: 92%; margin-bottom: 15px;">
                            <?php foreach ($addresses as $address) { ?>
                            <?php if ($address['address_id'] == $address_id) { ?>
                            <option value="<?php echo $address['address_id']; ?>" selected="selected"><?php echo $address['firstname']; ?> <?php echo $address['lastname']; ?>, <?php echo $address['address_1']; ?>, <?php echo $address['city']; ?>, <?php echo $address['zone']; ?>, <?php echo $address['country']; ?></option>
                            <?php } else { ?>
                            <option value="<?php echo $address['address_id']; ?>"><?php echo $address['firstname']; ?> <?php echo $address['lastname']; ?>, <?php echo $address['address_1']; ?>, <?php echo $address['city']; ?>, <?php echo $address['zone']; ?>, <?php echo $address['country']; ?></option>
                            <?php } ?>
                            <?php } ?>
                        </select>
                    </div>
                    
                    
                    <div class="supercheckout-extra-wrap" style="display: none">
                        <p>
                            <input type="radio" name="payment_address" value="new" id="payment-address-new" <?php echo ( $address_type == "new" ? 'checked="checked"' : '' ); ?> />
                            <label for="payment-address-new"><?php echo $text_address_new; ?></label>
                        </p>
                    </div>
                    
                    
                    <?php } ?>
                    
                    <div id="payment-new" style="display: <?php echo ($addresses ? 'block' : 'block'); ?>;">
                        
                        <div id="homealscheckout-time" class="homealscheckout">
                            <span class="lable"><?php echo $text_time; ?></span>
                            <span id="homealscheckout-time-text" class="text"><?php echo $date.", ".$time; ?></span>
                            <input type="hidden" name="delivery-time" value="<?php echo $date.", ".$time; ?>" />
                        </div>
                        
                        <div class="homealscheckout">
                            <a style="float:left; z-index: 2;" class="address-book-btn" >בחר כתובת</a>
                            <span class="lable">כתובת בתל אביב:</span>
                        </div>
                        <div id="homealscheckout-delivery-address" class="homealscheckout">
                            <span class="lable"><?php echo $text_delivery_address; ?><span class="asterix">*</span></span> 
                            
                            <?php $street_num = ($def_address ? explode("#",$def_address['address_1']) : ""); ?>
                            
                            <input type="text" class="homeals-delivery-address" name="homeals-delivery-address" value="<?php echo ( $address_type == "def" && $street_num ? trim($street_num['0']) : $delivery_address); ?>" />
                            
                            <input type="hidden" name="delivery-address" value="<?php echo ($address_type == "def" && $street_num ? $street_num['0'] : $delivery_address); ?>" />
                            
                            <div class="address-box-con">
                                <div id="spitz"></div>
                                <?php if(empty($addresses)){ ?>
                                    <div class="address-div">
                                    <?php if ($logged) { ?> 
                                        <?php echo $text_no_saved_addresses; ?>
                                    <?php } else { ?>
                                        <?php echo $text_log_in_addresses; ?>
                                    <?php }?>
                                    </div>
                                <?php } else {
                                    foreach($addresses as $address) {
                                ?> 
                                    <a id="<?php echo $address['address_id'] ?>" class="set-address-con" >
                                        <div class="address-div">
                                        <?php
                                            if(empty($address['company'])){
                                                echo mb_truncate(str_replace(array("#"), "",$address['address_1']),25);
                                            } else {
                                                echo mb_truncate(str_replace(array("#"), "",$address['address_1']).", (".$address['company'].")",25);
                                            }
                                            
                                            
                                        ?>
                                        
                                        <i class="fa fa-chevron-left left-arrow"></i>
                                        </div>
                                        <?php
                                            $pieces = explode(" ",$address['address_2']);
                                            $add_street_num = explode("#",$address['address_1']);
                                        ?>
                                        <div style="display: none">
                                            <input type="hidden" class="street_name" value="<?php echo trim($add_street_num['0']); ?>" />
                                            <input type="hidden" class="street_num" value="<?php echo $add_street_num['1']; ?>" />
                                            <input type="hidden" class="a_floor" value="<?php echo $pieces['1']; ?>" />
                                            <input type="hidden" class="a_apart" value="<?php echo $pieces['3']; ?>" />
                                            <input type="hidden" class="a_entrnce" value="<?php echo $pieces['5']; ?>" />
                                            <input type="hidden" class="a_compony" value="<?php echo $address['company']; ?>" />
                                            <input type="hidden" class="a_contact" value="<?php echo $address['firstname']." ".$address['lastname']; ?>" />
                                            <input type="hidden" class="a_phone" value="<?php echo $address['phone']; ?>" />
                                            <input type="hidden" class="a_comment" value="<?php echo $address['address_comment']; ?>" />
                                        </div>
                                    </a>
                                    <?php } ?>
                                <?php } ?>
                            </div>
                            
                        </div>
                        
                        
                        <div id="homealscheckout-street_num" class="homealscheckout-half">
                            <span class="lable"><?php echo $text_street_num; ?><span class="asterix">*</span></span> 
                            <input type="text" class="street_num" name="street_num" value="<?php echo ($address_type == 'def'  && $street_num ? trim($street_num['1']) : ''); ?>" />
                        </div>
                        
                        <div style="height: 13px; clear: both;">
                        <span class="red" id="wh-street_name"><?php echo $warn_street_name; ?></span>
                        <span class="red" id="wh-street_num"><?php echo $warn_street_num; ?></span>
                        </div>
                        
                        <?php  $pieces = ($def_address ? explode(" ",$def_address['address_2']) : ""); ?>
                        
                        <div id="homealscheckout-floor" class="homealscheckout-half">
                            <span class="lable"><?php echo $text_floor; ?></span> 
                            <input type="text" name="floor" value="<?php echo ($address_type == "def" && $pieces ? $pieces['1'] : ""); ?>" />
                        </div>
                        
                        <div id="homealscheckout-apartment" class="homealscheckout-half">
                            <span class="lable"><?php echo $text_apartment; ?></span> 
                            <input type="text" name="apartment" value="<?php echo ($address_type == "def" && $pieces ? $pieces['3'] : ""); ?>" />
                        </div>
                        
                        <div id="homealscheckout-entrance" class="homealscheckout-half">
                            <span class="lable"><?php echo $text_entrance; ?></span> 
                            <input type="text" name="entrance" value="<?php echo ($address_type == "def" && $pieces ? $pieces['5'] : ""); ?>" />
                        </div>
                        
                        <div style="clear:both"></div>
                        
                        <div id="supercheckout-comments" style="display:<?php if($logged){ if($settings['option']['logged']['confirm']['fields']['comment']['display']){ echo'block';}else{ echo'none';} }else{ if($settings['option']['guest']['confirm']['fields']['comment']['display']){ echo'block';}else{ echo'none';}} ?>;">
                        <span class="lable"><?php echo $text_delivery_comments; ?></span> 
                        <textarea id="supercheckout-comment_order" name="comment" rows="5" ><?php echo ($address_type == "def" ? $def_address['address_comment'] : ""); ?></textarea>
                        </div>
                            
                        <div id="homealscheckout-company" class="homealscheckout">
                            <span class="lable"><?php echo $text_company; ?></span> 
                            <input type="text" name="homeals-company" value="<?php echo ($address_type == "def"  ? $def_address['company'] : ""); ?>" />
                        </div>
                            
                        <div id="homealscheckout-contact" class="homealscheckout">
                            <span id="wh-contact" style="float:left; z-index: 2;" class="red" ><?php echo $warn_contact; ?></span>
                            <span class="lable"><?php echo $text_delivery_contact; ?><span class="asterix">*</span></span>
                            <input type="text" name="contact" value="<?php echo ($address_type == "def" ? $def_address['firstname']." ".$def_address['lastname'] : $user_name); ?>" />
                        </div>
                            
                        <div id="homealscheckout-mobile" class="homealscheckout">
                            <span id="wh-phone" style="float:left; z-index: 2;" class="red" ><?php echo $warn_phone; ?></span>
                            <span class="lable"><?php echo $text_delivery_mobile; ?><span class="asterix">*</span></span> 
                            <input type="text" name="mobile" value="<?php echo ($address_type == "def" ? $def_address['phone'] : $mobile ); ?>" />
                        </div>
                        
                        <!-- <a href="" id="save-address" ><?php echo $text_save_address; ?></a> -->
                        
                        <?php if(true){ ?>
                        <table id="payment_address_table" class="supercheckout-form" style="display: none">
                            
                            <tr class="sort_data" style="display:block" data-percentage="<?php echo $payment_address_sort_order['fields']['firstname']['sort_order'] ?>" >
                                <td> <?php echo $entry_firstname; ?><span style="display:'inline'" class="supercheckout-required"><span class="asterix">*</span>:</span>
                                    <input type="text" name="firstname" value="<?php if(isset($order_details['payment_firstname'])){ echo $order_details['payment_firstname']; } ?>" class="supercheckout-large-field" /></td>
                            </tr>
                            <tr class="sort_data" style="display:<?php if($logged){ if($settings['option']['logged']['payment_address']['fields']['lastname']['display']){ echo'block';}else{ echo'none';} }else{ if($settings['option']['guest']['payment_address']['fields']['lastname']['display']){ echo'block';}else{ echo'none';}} ?>;" data-percentage="<?php echo $payment_address_sort_order['fields']['lastname']['sort_order'] ?>">
                                <td> <?php echo $entry_lastname; ?><span style="display:<?php if($logged){ if($settings['option']['logged']['payment_address']['fields']['lastname']['require']){ echo'inline';}else{ echo'none';} }else{ if($settings['option']['guest']['payment_address']['fields']['lastname']['require']){ echo'inline';}else{ echo'none';}} ?>;" class="supercheckout-required">*</span>
                                <input type="text" name="lastname" value="<?php if(isset($order_details['payment_lastname'])){ echo $order_details['payment_lastname']; } ?>" class="supercheckout-large-field" /></td>
                            </tr>
                            <tr class="sort_data" style="display:<?php if($logged){ if($settings['option']['logged']['payment_address']['fields']['telephone']['display']){ echo'block';}else{ echo'none';} }else{ if($settings['option']['guest']['payment_address']['fields']['telephone']['display']){ echo'block';}else{ echo'none';}} ?>;" data-percentage="<?php echo $payment_address_sort_order['fields']['telephone']['sort_order'] ?>">
                                <td> <?php echo $entry_telephone; ?><span style="display:<?php if($logged){ if($settings['option']['logged']['payment_address']['fields']['telephone']['require']){ echo'inline';}else{ echo'none';} }else{ if($settings['option']['guest']['payment_address']['fields']['telephone']['require']){ echo'inline';}else{ echo'none';}} ?>;" class="supercheckout-required">*</span>
                                <input type="text" name="telephone" value="<?php if(isset($order_details['telephone'])){ echo $order_details['telephone']; } ?>" class="supercheckout-large-field" /></td>
                            </tr>
                            <tr class="sort_data" style="display:<?php if($logged){ if($settings['option']['logged']['payment_address']['fields']['company']['display']){ echo'block';}else{ echo'none';} }else{ if($settings['option']['guest']['payment_address']['fields']['company']['display']){ echo'block';}else{ echo'none';}} ?>;" data-percentage="<?php echo $payment_address_sort_order['fields']['company']['sort_order'] ?>" >
                                <td><?php echo $entry_company; ?><span style="display:<?php if($logged){ if($settings['option']['logged']['payment_address']['fields']['company']['require']){ echo'inline';}else{ echo'none';} }else{ if($settings['option']['guest']['payment_address']['fields']['company']['require']){ echo'inline';}else{ echo'none';}} ?>;" class="supercheckout-required">*</span>
                                <input type="text" name="company" value="<?php if(isset($order_details['payment_company'])){ echo $order_details['payment_company']; } ?>" class="supercheckout-large-field" /></td>
                            </tr>
                            
                            <tr class="sort_data" style="display:<?php if($logged){ if($settings['option']['logged']['payment_address']['fields']['company_id']['display']){ echo'block';}else{ echo'none';} }else{ if($settings['option']['guest']['payment_address']['fields']['company_id']['display']){ echo'block';}else{ echo'none';}} ?>;" data-percentage="<?php echo $payment_address_sort_order['fields']['company_id']['sort_order'] ?>" >
                                <td><?php echo $entry_company_id; ?><span class="supercheckout-required" style="display:<?php if($logged){ if($settings['option']['logged']['payment_address']['fields']['company_id']['require']){ echo'inline';}else{ echo'none';} }else{ if($settings['option']['guest']['payment_address']['fields']['company_id']['require']){ echo'inline';}else{ echo'none';}} ?>;">*</span>
                                <input type="text" name="company_id" value="" class="supercheckout-large-field" /></td>
                            </tr>                            
                            
                            <tr class="sort_data" style="display:<?php if($logged){ if($settings['option']['logged']['payment_address']['fields']['tax_id']['display']){ echo'block';}else{ echo'none';} }else{ if($settings['option']['guest']['payment_address']['fields']['tax_id']['display']){ echo'block';}else{ echo'none';}} ?>;" data-percentage="<?php echo $payment_address_sort_order['fields']['tax_id']['sort_order'] ?>" >
                                <td><?php echo $entry_tax_id; ?><span style="display:<?php if($logged){ if($settings['option']['logged']['payment_address']['fields']['tax_id']['require']){ echo'inline';}else{ echo'none';} }else{ if($settings['option']['guest']['payment_address']['fields']['tax_id']['require']){ echo'inline';}else{ echo'none';}} ?>;" class="supercheckout-required">*</span>
                                    
                                    <input type="text" name="tax_id" value="<?php if(isset($order_details['payment_firstname'])){ echo $order_details['payment_firstname']; } ?>" class="supercheckout-large-field" /></td>
                            </tr>
                            
                            <tr class="sort_data" style="display:<?php if($logged){ if($settings['option']['logged']['payment_address']['fields']['address_1']['display']){ echo'block';}else{ echo'none';} }else{ if($settings['option']['guest']['payment_address']['fields']['address_1']['display']){ echo'block';}else{ echo'none';}} ?>;" data-percentage="<?php echo $payment_address_sort_order['fields']['address_1']['sort_order'] ?>" >
                                <td> <?php echo $entry_address_1; ?><span style="display:<?php if($logged){ if($settings['option']['logged']['payment_address']['fields']['address_1']['require']){ echo'inline';}else{ echo'none';} }else{ if($settings['option']['guest']['payment_address']['fields']['address_1']['require']){ echo'inline';}else{ echo'none';}} ?>;" class="supercheckout-required">*</span>
                                <input type="text" name="address_1" value="<?php if(isset($order_details['payment_address_1'])){ echo $order_details['payment_address_1']; } ?>" class="supercheckout-large-field" /></td>
                            </tr>
                            <tr class="sort_data" style="display:<?php if($logged){ if($settings['option']['logged']['payment_address']['fields']['address_2']['display']){ echo'block';}else{ echo'none';} }else{ if($settings['option']['guest']['payment_address']['fields']['address_2']['display']){ echo'block';}else{ echo'none';}} ?>;" data-percentage="<?php echo $payment_address_sort_order['fields']['address_2']['sort_order'] ?>" >
                                <td><?php echo $entry_address_2; ?><span style="display:<?php if($logged){ if($settings['option']['logged']['payment_address']['fields']['address_2']['require']){ echo'inline';}else{ echo'none';} }else{ if($settings['option']['guest']['payment_address']['fields']['address_2']['require']){ echo'inline';}else{ echo'none';}} ?>;" class="supercheckout-required">*</span>
                                <input type="text" name="address_2" value="<?php if(isset($order_details['payment_address_2'])){ echo $order_details['payment_address_2']; } ?>" class="supercheckout-large-field" /></td>
                            </tr>
                            <tr class="sort_data" style="display:<?php if($logged){ if($settings['option']['logged']['payment_address']['fields']['city']['display']){ echo'block';}else{ echo'none';} }else{ if($settings['option']['guest']['payment_address']['fields']['city']['display']){ echo'block';}else{ echo'none';}} ?>;" data-percentage="<?php echo $payment_address_sort_order['fields']['city']['sort_order'] ?>" >
                                <td><?php echo $entry_city; ?><span style="display:<?php if($logged){ if($settings['option']['logged']['payment_address']['fields']['city']['require']){ echo'inline';}else{ echo'none';} }else{ if($settings['option']['guest']['payment_address']['fields']['city']['require']){ echo'inline';}else{ echo'none';}} ?>;" class="supercheckout-required">*</span>
                                <input type="text" name="city" value="<?php if(isset($order_details['payment_city'])){ echo $order_details['payment_city']; } ?>" class="supercheckout-large-field" /></td>
                            </tr>
                            <tr class="sort_data" style="display:<?php if($logged){ if($settings['option']['logged']['payment_address']['fields']['postcode']['display']){ echo'block';}else{ echo'none';} }else{ if($settings['option']['guest']['payment_address']['fields']['postcode']['display']){ echo'block';}else{ echo'none';}} ?>;" data-percentage="<?php echo $payment_address_sort_order['fields']['postcode']['sort_order'] ?>" >
                                <td><?php echo $entry_postcode; ?><span style="display:<?php if($logged){ if($settings['option']['logged']['payment_address']['fields']['postcode']['require'] && $country_info_guest['postcode_required']){ echo'inline';}else{ echo'none';} }else{ if($settings['option']['guest']['payment_address']['fields']['postcode']['require'] && $country_info_guest['postcode_required']){ echo'inline';}else{ echo'none';}} ?>;" id="payment-postcode-required" class="supercheckout-required">*</span>
                                <input type="text" name="postcode" value="" class="supercheckout-large-field" /></td>
                            </tr>
                            <tr class="sort_data" style="display:<?php if($logged){ if($settings['option']['logged']['payment_address']['fields']['country_id']['display']){ echo'block';}else{ echo'none';} }else{ if($settings['option']['guest']['payment_address']['fields']['country_id']['display']){ echo'block';}else{ echo'none';}} ?>;" data-percentage="<?php echo $payment_address_sort_order['fields']['country_id']['sort_order'] ?>" >
                                <td> <?php echo $entry_country; ?><span style="display:<?php if($logged){ if($settings['option']['logged']['payment_address']['fields']['country_id']['require']){ echo'inline';}else{ echo'none';} }else{ if($settings['option']['guest']['payment_address']['fields']['country_id']['require']){ echo'inline';}else{ echo'none';}} ?>;" class="supercheckout-required">*</span>
                                <select name="country_id" class="supercheckout-large-field">
                                        <option value=""><?php echo $text_select; ?></option>
                                        <?php foreach ($countries as $country) { ?>                                        
                                        <?php if (($country['country_id'] == $country_id)) { ?>
                                        <option value="<?php echo $country['country_id']; ?>" selected="selected"><?php echo $country['name']; ?></option>
                                        <?php } else { ?>
                                        <option value="<?php echo $country['country_id']; ?>"><?php echo $country['name']; ?></option>
                                        <?php } ?>
                                        <?php } ?>
                                    </select></td>
                            </tr>
                            <tr class="sort_data" style="display:<?php if($logged){ if($settings['option']['logged']['payment_address']['fields']['zone_id']['display']){ echo'block';}else{ echo'none';} }else{ if($settings['option']['guest']['payment_address']['fields']['zone_id']['display']){ echo'block';}else{ echo'none';}} ?>;" data-percentage="<?php echo $payment_address_sort_order['fields']['zone_id']['sort_order'] ?>" >
                                <td> <?php echo $entry_zone; ?><span style="display:<?php if($logged){ if($settings['option']['logged']['payment_address']['fields']['zone_id']['require']){ echo'inline';}else{ echo'none';} }else{ if($settings['option']['guest']['payment_address']['fields']['zone_id']['require']){ echo'inline';}else{ echo'none';}} ?>;" class="supercheckout-required">*</span>
                                <select name="zone_id" class="supercheckout-large-field">
                                        <?php echo $zones_default; ?>
                                    </select></td>
                            </tr>
                            
                            <tr class="sort_data" style="display:<?php if($logged){ if($settings['option']['logged']['payment_address']['fields']['address_1']['display']){ echo'block';}else{ echo'none';} }else{ if($settings['option']['guest']['payment_address']['fields']['address_1']['display']){ echo'block';}else{ echo'none';}} ?>;" data-percentage="<?php echo $payment_address_sort_order['fields']['address_1']['sort_order'] ?>" >
                                <td> <?php echo $entry_address_1; ?><span style="display:<?php if($logged){ if($settings['option']['logged']['payment_address']['fields']['address_1']['require']){ echo'inline';}else{ echo'none';} }else{ if($settings['option']['guest']['payment_address']['fields']['address_1']['require']){ echo'inline';}else{ echo'none';}} ?>;" class="supercheckout-required">*</span>
                                <input type="text" name="phone" value="<?php if(isset($order_details['phone'])){ echo $order_details['phone']; } ?>" class="supercheckout-large-field" /></td>
                            </tr>
                            <tr class="sort_data" style="display:<?php if($logged){ if($settings['option']['logged']['payment_address']['fields']['address_1']['display']){ echo'block';}else{ echo'none';} }else{ if($settings['option']['guest']['payment_address']['fields']['address_1']['display']){ echo'block';}else{ echo'none';}} ?>;" data-percentage="<?php echo $payment_address_sort_order['fields']['address_1']['sort_order'] ?>" >
                                <td> <?php echo $entry_address_1; ?><span style="display:<?php if($logged){ if($settings['option']['logged']['payment_address']['fields']['address_1']['require']){ echo'inline';}else{ echo'none';} }else{ if($settings['option']['guest']['payment_address']['fields']['address_1']['require']){ echo'inline';}else{ echo'none';}} ?>;" class="supercheckout-required">*</span>
                                <textarea name="address_comment" value="<?php if(isset($order_details['address_comment'])){ echo $order_details['address_comment']; } ?>" class="supercheckout-large-field"></textarea></td>
                            </tr>

                        </table>
                        <?php } ?>
                        
                        <?php if(false){ ?>
                        <table id="payment_address_table" class="supercheckout-form">
                            <tr class="sort_data" style="display:<?php if($logged){ if($settings['option']['logged']['payment_address']['fields']['firstname']['display']){ echo'block';}else{ echo'none';} }else{ if($settings['option']['guest']['payment_address']['fields']['firstname']['display']){ echo'block';}else{ echo'none';}} ?>;"  data-percentage="<?php echo $payment_address_sort_order['fields']['firstname']['sort_order'] ?>" >
                                <td> <?php echo $entry_firstname; ?><span style="display:<?php if($logged){ if($settings['option']['logged']['payment_address']['fields']['firstname']['require']){ echo'inline';}else{ echo'none';} }else{ if($settings['option']['guest']['payment_address']['fields']['firstname']['require']){ echo'inline';}else{ echo'none';}} ?>;" class="supercheckout-required">*</span>
                                    <input type="text" name="firstname" value="<?php if(isset($order_details['payment_firstname'])){ echo $order_details['payment_firstname']; } ?>" class="supercheckout-large-field" /></td>
                            </tr>
                            <tr class="sort_data" style="display:<?php if($logged){ if($settings['option']['logged']['payment_address']['fields']['lastname']['display']){ echo'block';}else{ echo'none';} }else{ if($settings['option']['guest']['payment_address']['fields']['lastname']['display']){ echo'block';}else{ echo'none';}} ?>;" data-percentage="<?php echo $payment_address_sort_order['fields']['lastname']['sort_order'] ?>">
                                <td> <?php echo $entry_lastname; ?><span style="display:<?php if($logged){ if($settings['option']['logged']['payment_address']['fields']['lastname']['require']){ echo'inline';}else{ echo'none';} }else{ if($settings['option']['guest']['payment_address']['fields']['lastname']['require']){ echo'inline';}else{ echo'none';}} ?>;" class="supercheckout-required">*</span>
                                <input type="text" name="lastname" value="<?php if(isset($order_details['payment_lastname'])){ echo $order_details['payment_lastname']; } ?>" class="supercheckout-large-field" /></td>
                            </tr>
                            <tr class="sort_data" style="display:<?php if($logged){ if($settings['option']['logged']['payment_address']['fields']['telephone']['display']){ echo'block';}else{ echo'none';} }else{ if($settings['option']['guest']['payment_address']['fields']['telephone']['display']){ echo'block';}else{ echo'none';}} ?>;" data-percentage="<?php echo $payment_address_sort_order['fields']['telephone']['sort_order'] ?>">
                                <td> <?php echo $entry_telephone; ?><span style="display:<?php if($logged){ if($settings['option']['logged']['payment_address']['fields']['telephone']['require']){ echo'inline';}else{ echo'none';} }else{ if($settings['option']['guest']['payment_address']['fields']['telephone']['require']){ echo'inline';}else{ echo'none';}} ?>;" class="supercheckout-required">*</span>
                                <input type="text" name="telephone" value="<?php if(isset($order_details['telephone'])){ echo $order_details['telephone']; } ?>" class="supercheckout-large-field" /></td>
                            </tr>
                            <tr class="sort_data" style="display:<?php if($logged){ if($settings['option']['logged']['payment_address']['fields']['company']['display']){ echo'block';}else{ echo'none';} }else{ if($settings['option']['guest']['payment_address']['fields']['company']['display']){ echo'block';}else{ echo'none';}} ?>;" data-percentage="<?php echo $payment_address_sort_order['fields']['company']['sort_order'] ?>" >
                                <td><?php echo $entry_company; ?><span style="display:<?php if($logged){ if($settings['option']['logged']['payment_address']['fields']['company']['require']){ echo'inline';}else{ echo'none';} }else{ if($settings['option']['guest']['payment_address']['fields']['company']['require']){ echo'inline';}else{ echo'none';}} ?>;" class="supercheckout-required">*</span>
                                <input type="text" name="company" value="<?php if(isset($order_details['payment_company'])){ echo $order_details['payment_company']; } ?>" class="supercheckout-large-field" /></td>
                            </tr>
                            
                            <tr class="sort_data" style="display:<?php if($logged){ if($settings['option']['logged']['payment_address']['fields']['company_id']['display']){ echo'block';}else{ echo'none';} }else{ if($settings['option']['guest']['payment_address']['fields']['company_id']['display']){ echo'block';}else{ echo'none';}} ?>;" data-percentage="<?php echo $payment_address_sort_order['fields']['company_id']['sort_order'] ?>" >
                                <td><?php echo $entry_company_id; ?><span class="supercheckout-required" style="display:<?php if($logged){ if($settings['option']['logged']['payment_address']['fields']['company_id']['require']){ echo'inline';}else{ echo'none';} }else{ if($settings['option']['guest']['payment_address']['fields']['company_id']['require']){ echo'inline';}else{ echo'none';}} ?>;">*</span>
                                <input type="text" name="company_id" value="" class="supercheckout-large-field" /></td>
                            </tr>                            
                            
                            <tr class="sort_data" style="display:<?php if($logged){ if($settings['option']['logged']['payment_address']['fields']['tax_id']['display']){ echo'block';}else{ echo'none';} }else{ if($settings['option']['guest']['payment_address']['fields']['tax_id']['display']){ echo'block';}else{ echo'none';}} ?>;" data-percentage="<?php echo $payment_address_sort_order['fields']['tax_id']['sort_order'] ?>" >
                                <td><?php echo $entry_tax_id; ?><span style="display:<?php if($logged){ if($settings['option']['logged']['payment_address']['fields']['tax_id']['require']){ echo'inline';}else{ echo'none';} }else{ if($settings['option']['guest']['payment_address']['fields']['tax_id']['require']){ echo'inline';}else{ echo'none';}} ?>;" class="supercheckout-required">*</span>
                                    
                                    <input type="text" name="tax_id" value="<?php if(isset($order_details['payment_firstname'])){ echo $order_details['payment_firstname']; } ?>" class="supercheckout-large-field" /></td>
                            </tr>
                            
                            <tr class="sort_data" style="display:<?php if($logged){ if($settings['option']['logged']['payment_address']['fields']['address_1']['display']){ echo'block';}else{ echo'none';} }else{ if($settings['option']['guest']['payment_address']['fields']['address_1']['display']){ echo'block';}else{ echo'none';}} ?>;" data-percentage="<?php echo $payment_address_sort_order['fields']['address_1']['sort_order'] ?>" >
                                <td> <?php echo $entry_address_1; ?><span style="display:<?php if($logged){ if($settings['option']['logged']['payment_address']['fields']['address_1']['require']){ echo'inline';}else{ echo'none';} }else{ if($settings['option']['guest']['payment_address']['fields']['address_1']['require']){ echo'inline';}else{ echo'none';}} ?>;" class="supercheckout-required">*</span>
                                <input type="text" name="address_1" value="<?php if(isset($order_details['payment_address_1'])){ echo $order_details['payment_address_1']; } ?>" class="supercheckout-large-field" /></td>
                            </tr>
                            <tr class="sort_data" style="display:<?php if($logged){ if($settings['option']['logged']['payment_address']['fields']['address_2']['display']){ echo'block';}else{ echo'none';} }else{ if($settings['option']['guest']['payment_address']['fields']['address_2']['display']){ echo'block';}else{ echo'none';}} ?>;" data-percentage="<?php echo $payment_address_sort_order['fields']['address_2']['sort_order'] ?>" >
                                <td><?php echo $entry_address_2; ?><span style="display:<?php if($logged){ if($settings['option']['logged']['payment_address']['fields']['address_2']['require']){ echo'inline';}else{ echo'none';} }else{ if($settings['option']['guest']['payment_address']['fields']['address_2']['require']){ echo'inline';}else{ echo'none';}} ?>;" class="supercheckout-required">*</span>
                                <input type="text" name="address_2" value="<?php if(isset($order_details['payment_address_2'])){ echo $order_details['payment_address_2']; } ?>" class="supercheckout-large-field" /></td>
                            </tr>
                            <tr class="sort_data" style="display:<?php if($logged){ if($settings['option']['logged']['payment_address']['fields']['city']['display']){ echo'block';}else{ echo'none';} }else{ if($settings['option']['guest']['payment_address']['fields']['city']['display']){ echo'block';}else{ echo'none';}} ?>;" data-percentage="<?php echo $payment_address_sort_order['fields']['city']['sort_order'] ?>" >
                                <td><?php echo $entry_city; ?><span style="display:<?php if($logged){ if($settings['option']['logged']['payment_address']['fields']['city']['require']){ echo'inline';}else{ echo'none';} }else{ if($settings['option']['guest']['payment_address']['fields']['city']['require']){ echo'inline';}else{ echo'none';}} ?>;" class="supercheckout-required">*</span>
                                <input type="text" name="city" value="<?php if(isset($order_details['payment_city'])){ echo $order_details['payment_city']; } ?>" class="supercheckout-large-field" /></td>
                            </tr>
                            <tr class="sort_data" style="display:<?php if($logged){ if($settings['option']['logged']['payment_address']['fields']['postcode']['display']){ echo'block';}else{ echo'none';} }else{ if($settings['option']['guest']['payment_address']['fields']['postcode']['display']){ echo'block';}else{ echo'none';}} ?>;" data-percentage="<?php echo $payment_address_sort_order['fields']['postcode']['sort_order'] ?>" >
                                <td><?php echo $entry_postcode; ?><span style="display:<?php if($logged){ if($settings['option']['logged']['payment_address']['fields']['postcode']['require'] && $country_info_guest['postcode_required']){ echo'inline';}else{ echo'none';} }else{ if($settings['option']['guest']['payment_address']['fields']['postcode']['require'] && $country_info_guest['postcode_required']){ echo'inline';}else{ echo'none';}} ?>;" id="payment-postcode-required" class="supercheckout-required">*</span>
                                <input type="text" name="postcode" value="" class="supercheckout-large-field" /></td>
                            </tr>
                            <tr class="sort_data" style="display:<?php if($logged){ if($settings['option']['logged']['payment_address']['fields']['country_id']['display']){ echo'block';}else{ echo'none';} }else{ if($settings['option']['guest']['payment_address']['fields']['country_id']['display']){ echo'block';}else{ echo'none';}} ?>;" data-percentage="<?php echo $payment_address_sort_order['fields']['country_id']['sort_order'] ?>" >
                                <td> <?php echo $entry_country; ?><span style="display:<?php if($logged){ if($settings['option']['logged']['payment_address']['fields']['country_id']['require']){ echo'inline';}else{ echo'none';} }else{ if($settings['option']['guest']['payment_address']['fields']['country_id']['require']){ echo'inline';}else{ echo'none';}} ?>;" class="supercheckout-required">*</span>
                                <select name="country_id" class="supercheckout-large-field">
                                        <option value=""><?php echo $text_select; ?></option>
                                        <?php foreach ($countries as $country) { ?>                                        
                                        <?php if (($country['country_id'] == $country_id)) { ?>
                                        <option value="<?php echo $country['country_id']; ?>" selected="selected"><?php echo $country['name']; ?></option>
                                        <?php } else { ?>
                                        <option value="<?php echo $country['country_id']; ?>"><?php echo $country['name']; ?></option>
                                        <?php } ?>
                                        <?php } ?>
                                    </select></td>
                            </tr>
                            <tr class="sort_data" style="display:<?php if($logged){ if($settings['option']['logged']['payment_address']['fields']['zone_id']['display']){ echo'block';}else{ echo'none';} }else{ if($settings['option']['guest']['payment_address']['fields']['zone_id']['display']){ echo'block';}else{ echo'none';}} ?>;" data-percentage="<?php echo $payment_address_sort_order['fields']['zone_id']['sort_order'] ?>" >
                                <td> <?php echo $entry_zone; ?><span style="display:<?php if($logged){ if($settings['option']['logged']['payment_address']['fields']['zone_id']['require']){ echo'inline';}else{ echo'none';} }else{ if($settings['option']['guest']['payment_address']['fields']['zone_id']['require']){ echo'inline';}else{ echo'none';}} ?>;" class="supercheckout-required">*</span>
                                <select name="zone_id" class="supercheckout-large-field">
                                        <?php echo $zones_default; ?>
                                    </select></td>
                            </tr>
                            
                            <tr class="sort_data" style="display:<?php if($logged){ if($settings['option']['logged']['payment_address']['fields']['address_1']['display']){ echo'block';}else{ echo'none';} }else{ if($settings['option']['guest']['payment_address']['fields']['address_1']['display']){ echo'block';}else{ echo'none';}} ?>;" data-percentage="<?php echo $payment_address_sort_order['fields']['address_1']['sort_order'] ?>" >
                                <td> <?php echo $entry_address_1; ?><span style="display:<?php if($logged){ if($settings['option']['logged']['payment_address']['fields']['address_1']['require']){ echo'inline';}else{ echo'none';} }else{ if($settings['option']['guest']['payment_address']['fields']['address_1']['require']){ echo'inline';}else{ echo'none';}} ?>;" class="supercheckout-required">*</span>
                                <input type="text" name="phone" value="<?php if(isset($order_details['phone'])){ echo $order_details['phone']; } ?>" class="supercheckout-large-field" /></td>
                            </tr>
                            
                            <tr class="sort_data" style="display:'block';" data-percentage="<?php echo $payment_address_sort_order['fields']['address_1']['sort_order'] ?>" >
                                <td> <?php echo $entry_address_1; ?><span style="display:<?php if($logged){ if($settings['option']['logged']['payment_address']['fields']['address_1']['require']){ echo'inline';}else{ echo'none';} }else{ if($settings['option']['guest']['payment_address']['fields']['address_1']['require']){ echo'inline';}else{ echo'none';}} ?>;" class="supercheckout-required">*</span>
                                <textarea name="address_comment" value="<?php if(isset($order_details['address_comment'])){ echo $order_details['address_comment']; } ?>" class="supercheckout-large-field" ></textarea></td>
                            </tr>
                            
                        </table>
                        <?php } ?>
                    </div>
                    <ul>
                        <li>
                            <div class="input-box input-different-shipping" style="display:<?php if(!$logged){  if($settings['option']['guest']['payment_address']['fields']['shipping']['display']){echo 'block';}else{echo 'none';}}?>;">
                                <input name="use_for_shipping" id="shipping_use"  checked="checked" type="checkbox">
                                <label for="shipping_use"><b><?php echo $text_ship_same_address; ?></b></label>
                            </div>
                        </li>
                    </ul>
                </div>
                <br/>
            </div>      
            <div class="supercheckout-blocks" data-column="<?php echo $sort_block['shipping_address'][$layout_name]['column']; ?>" data-row="<?php echo $sort_block['shipping_address'][$layout_name]['row']; ?>" data-column-inside="<?php echo $sort_block['shipping_address'][$layout_name]['column-inside']; ?>">
            <div id="checkoutShippingAddress">
                <div class="supercheckout-checkout-content">

                </div>
                
                    <ul>
                        <li>
                            <p class="supercheckout-numbers supercheckout-numbers-ship"><?php echo $text_shipping_address; ?></p>
                        </li>
                    </ul>
                    <?php if ($addresses) { ?>
                    <div class="supercheckout-extra-wrap" style="display: none">
                        <input type="radio" name="shipping_address" value="existing" id="shipping-address-existing" checked="checked" />
                        <label for="shipping-address-existing"><?php echo $text_address_existing; ?></label>
                    </div>
                    <div id="shipping-existing" class="styled-select" style="display: none">
                        <select name="address_id" style="width: 92%; margin-bottom: 15px;">
                            <?php foreach ($addresses as $address) { ?>
                            <?php if ($address['address_id'] == $address_id) { ?>
                            <option value="<?php echo $address['address_id']; ?>" selected="selected"><?php echo $address['firstname']; ?> <?php echo $address['lastname']; ?>, <?php echo $address['address_1']; ?>, <?php echo $address['city']; ?>, <?php echo $address['zone']; ?>, <?php echo $address['country']; ?></option>
                            <?php } else { ?>
                            <option value="<?php echo $address['address_id']; ?>"><?php echo $address['firstname']; ?> <?php echo $address['lastname']; ?>, <?php echo $address['address_1']; ?>, <?php echo $address['city']; ?>, <?php echo $address['zone']; ?>, <?php echo $address['country']; ?></option>
                            <?php } ?>
                            <?php } ?>
                        </select>
                    </div>
                    <div class="supercheckout-extra-wrap">
                        <p>
                            <input type="radio" name="shipping_address" value="new" id="shipping-address-new" />
                            <label for="shipping-address-new"><?php echo $text_address_new; ?></label>
                        </p>
                    </div>
                    <?php } ?>
                    
                    <div id="shipping-new" style="display: <?php echo ($addresses ? 'none' : 'block'); ?>;">
                        <table class="supercheckout-form" id="shipping_address_table">
                            <tr class="sort_data" style="display:<?php if($logged){ if($settings['option']['logged']['shipping_address']['fields']['firstname']['display']){ echo'block';}else{ echo'none';} }else{ if($settings['option']['guest']['shipping_address']['fields']['firstname']['display']){ echo'block';}else{ echo'none';}} ?>;"   data-percentage="<?php echo $shipping_address_sort_order['fields']['firstname']['sort_order'] ?>" >
                                <td> <?php echo $entry_firstname; ?><span style="display:<?php if($logged){ if($settings['option']['logged']['shipping_address']['fields']['firstname']['require']){ echo'inline';}else{ echo'none';} }else{ if($settings['option']['guest']['shipping_address']['fields']['firstname']['require']){ echo'inline';}else{ echo'none';}} ?>;" class="supercheckout-required">*</span>
                                <input type="text" name="firstname" value="<?php if(isset($order_details['shipping_firstname'])){ echo $order_details['shipping_firstname']; } ?>" class="supercheckout-large-field" /></td>
                            </tr>
                            <tr class="sort_data" style="display:<?php if($logged){ if($settings['option']['logged']['shipping_address']['fields']['lastname']['display']){ echo'block';}else{ echo'none';} }else{ if($settings['option']['guest']['shipping_address']['fields']['lastname']['display']){ echo'block';}else{ echo'none';}} ?>;"  data-percentage="<?php echo $shipping_address_sort_order['fields']['lastname']['sort_order'] ?>">
                                <td> <?php echo $entry_lastname; ?><span style="display:<?php if($logged){ if($settings['option']['logged']['shipping_address']['fields']['lastname']['require']){ echo'inline';}else{ echo'none';} }else{ if($settings['option']['guest']['shipping_address']['fields']['lastname']['require']){ echo'inline';}else{ echo'none';}} ?>;" class="supercheckout-required">*</span>
                                <input type="text" name="lastname" value="<?php if(isset($order_details['shipping_lastname'])){ echo $order_details['shipping_lastname']; } ?>" class="supercheckout-large-field" /></td>
                            </tr>                           
                            <tr class="sort_data" style="display:<?php if($logged){ if($settings['option']['logged']['shipping_address']['fields']['address_1']['display']){ echo'block';}else{ echo'none';} }else{ if($settings['option']['guest']['shipping_address']['fields']['address_1']['display']){ echo'block';}else{ echo'none';}} ?>;" data-percentage="<?php echo $shipping_address_sort_order['fields']['address_1']['sort_order'] ?>">
                                <td><?php echo $entry_address_1; ?><span style="display:<?php if($logged){ if($settings['option']['logged']['shipping_address']['fields']['address_1']['require']){ echo'inline';}else{ echo'none';} }else{ if($settings['option']['guest']['shipping_address']['fields']['address_1']['require']){ echo'inline';}else{ echo'none';}} ?>;" class="supercheckout-required">*</span> 
                                <input type="text" name="address_1" value="<?php if(isset($order_details['shipping_address_1'])){ echo $order_details['shipping_address_1']; } ?>" class="supercheckout-large-field" /></td>
                            </tr>
                            <tr class="sort_data" style="display:<?php if($logged){ if($settings['option']['logged']['shipping_address']['fields']['address_2']['display']){ echo'block';}else{ echo'none';} }else{ if($settings['option']['guest']['shipping_address']['fields']['address_2']['display']){ echo'block';}else{ echo'none';}} ?>;" data-percentage="<?php echo $shipping_address_sort_order['fields']['address_2']['sort_order'] ?>">
                                <td><?php echo $entry_address_2; ?><span style="display:<?php if($logged){ if($settings['option']['logged']['shipping_address']['fields']['address_2']['require']){ echo'inline';}else{ echo'none';} }else{ if($settings['option']['guest']['shipping_address']['fields']['address_2']['require']){ echo'inline';}else{ echo'none';}} ?>;" class="supercheckout-required">*</span>
                                <input type="text" name="address_2" value="<?php if(isset($order_details['shipping_address_2'])){ echo $order_details['shipping_address_2']; } ?>" class="supercheckout-large-field" /></td>
                            </tr>
                            <tr class="sort_data" style="display:<?php if($logged){ if($settings['option']['logged']['shipping_address']['fields']['city']['display']){ echo'block';}else{ echo'none';} }else{ if($settings['option']['guest']['shipping_address']['fields']['city']['display']){ echo'block';}else{ echo'none';}} ?>;"  data-percentage="<?php echo $shipping_address_sort_order['fields']['city']['sort_order'] ?>">
                                <td><?php echo $entry_city; ?><span style="display:<?php if($logged){ if($settings['option']['logged']['shipping_address']['fields']['city']['require']){ echo'inline';}else{ echo'none';} }else{ if($settings['option']['guest']['shipping_address']['fields']['city']['require']){ echo'inline';}else{ echo'none';}} ?>;" class="supercheckout-required">*</span>
                                <input type="text" name="city" value="<?php if(isset($order_details['shipping_city'])){ echo $order_details['shipping_city']; } ?>" class="supercheckout-large-field" /></td>
                            </tr>
                            <tr class="sort_data" style="display:<?php if($logged){ if($settings['option']['logged']['shipping_address']['fields']['postcode']['display']){ echo'block';}else{ echo'none';} }else{ if($settings['option']['guest']['shipping_address']['fields']['postcode']['display']){ echo'block';}else{ echo'none';}} ?>;"  data-percentage="<?php echo $shipping_address_sort_order['fields']['postcode']['sort_order'] ?>">
                                <td><?php echo $entry_postcode; ?><span style="display:<?php if($logged){ if($settings['option']['logged']['shipping_address']['fields']['postcode']['require']&& $country_info_guest['postcode_required']){ echo'inline';}else{ echo'none';} }else{ if($settings['option']['guest']['shipping_address']['fields']['postcode']['require']&& $country_info_guest['postcode_required']){ echo'inline';}else{ echo'none';}} ?>;" id="shipping-postcode-required" class="supercheckout-required">*</span>
                                <input type="text" name="postcode" value="" class="supercheckout-large-field" /></td>
                            </tr>
                            <tr class="sort_data" style="display:<?php if($logged){ if($settings['option']['logged']['shipping_address']['fields']['country_id']['display']){ echo'block';}else{ echo'none';} }else{ if($settings['option']['guest']['shipping_address']['fields']['country_id']['display']){ echo'block';}else{ echo'none';}} ?>;" data-percentage="<?php echo $shipping_address_sort_order['fields']['country_id']['sort_order'] ?>">
                                <td><?php echo $entry_country; ?><span style="display:<?php if($logged){ if($settings['option']['logged']['shipping_address']['fields']['country_id']['require']){ echo'inline';}else{ echo'none';} }else{ if($settings['option']['guest']['shipping_address']['fields']['country_id']['require']){ echo'inline';}else{ echo'none';}} ?>;" class="supercheckout-required">*</span>
                                <select name="country_id" class="supercheckout-large-field">
                                        <option value=""><?php echo $text_select; ?></option>
                                        <?php foreach ($countries as $country) { ?>
                                        <?php if (($country['country_id'] == $country_id)) { ?>
                                        <option value="<?php echo $country['country_id']; ?>" selected="selected"><?php echo $country['name']; ?></option>
                                        <?php } else { ?>
                                        <option value="<?php echo $country['country_id']; ?>"><?php echo $country['name']; ?></option>
                                        <?php } ?>
                                        <?php } ?>
                                    </select></td>
                            </tr>
                            <tr class="sort_data" style="display:<?php if($logged){ if($settings['option']['logged']['shipping_address']['fields']['zone_id']['display']){ echo'block';}else{ echo'none';} }else{ if($settings['option']['guest']['shipping_address']['fields']['zone_id']['display']){ echo'block';}else{ echo'none';}} ?>;" data-percentage="<?php echo $shipping_address_sort_order['fields']['zone_id']['sort_order'] ?>">
                                <td><?php echo $entry_zone; ?><span style="display:<?php if($logged){ if($settings['option']['logged']['shipping_address']['fields']['zone_id']['require']){ echo'inline';}else{ echo'none';} }else{ if($settings['option']['guest']['shipping_address']['fields']['zone_id']['require']){ echo'inline';}else{ echo'none';}} ?>;" class="supercheckout-required">*</span>
                                <select name="zone_id" class="supercheckout-large-field">
                                        <?php echo $zones_default; ?>
                                    </select></td>
                            </tr>
                            
                            <tr class="sort_data" style="display:<?php if($logged){ if($settings['option']['logged']['shipping_address']['fields']['address_1']['display']){ echo'block';}else{ echo'none';} }else{ if($settings['option']['guest']['shipping_address']['fields']['address_1']['display']){ echo'block';}else{ echo'none';}} ?>;" data-percentage="<?php echo $shipping_address_sort_order['fields']['address_1']['sort_order'] ?>">
                                <td><?php echo $entry_address_1; ?><span style="display:<?php if($logged){ if($settings['option']['logged']['shipping_address']['fields']['address_1']['require']){ echo'inline';}else{ echo'none';} }else{ if($settings['option']['guest']['shipping_address']['fields']['address_1']['require']){ echo'inline';}else{ echo'none';}} ?>;" class="supercheckout-required">*</span> 
                                <input type="text" name="phone" value="<?php if(isset($order_details['phone'])){ echo $order_details['phone']; } ?>" class="supercheckout-large-field" /></td>
                            </tr>
                            <tr class="sort_data" style="display:<?php if($logged){ if($settings['option']['logged']['shipping_address']['fields']['address_1']['display']){ echo'block';}else{ echo'none';} }else{ if($settings['option']['guest']['shipping_address']['fields']['address_1']['display']){ echo'block';}else{ echo'none';}} ?>;" data-percentage="<?php echo $shipping_address_sort_order['fields']['address_1']['sort_order'] ?>">
                                <td><?php echo $entry_address_1; ?><span style="display:<?php if($logged){ if($settings['option']['logged']['shipping_address']['fields']['address_1']['require']){ echo'inline';}else{ echo'none';} }else{ if($settings['option']['guest']['shipping_address']['fields']['address_1']['require']){ echo'inline';}else{ echo'none';}} ?>;" class="supercheckout-required">*</span> 
                                <textarea name="address_comment" value="<?php if(isset($order_details['address_comment'])){ echo $order_details['address_comment']; } ?>" class="supercheckout-large-field" ></textarea></td>
                            </tr>
                        </table>
                    </div>
                </div>                    

            </div>
            <?php if($settings['step']['shipping_method']['display_options']){ ?>
            <div  class="supercheckout-blocks" data-column="<?php echo $sort_block['shipping_method'][$layout_name]['column']; ?>" data-row="<?php echo $sort_block['shipping_method'][$layout_name]['row']; ?>" data-column-inside="<?php echo $sort_block['shipping_method'][$layout_name]['column-inside']; ?>" >
                
                <ul>
                    <li style="display:inline;">
                        <p class="supercheckout-numbers supercheckout-numbers-3"><?php echo $text_shipping_method; ?></p>
                        <div class="loader" id="shippingMethodLoader"></div>
                    </li>                
                </ul>
                
                               
                <div id="shipping-method">
                    <?php if(!$shipping_required){ ?>
                    <div class="supercheckout-checkout-content" style="display:block">
                        <div class="permanent-warning" style="display: block;">No shipping required with these product(s).<img src="catalog/view/theme/default/image/close.png" alt="" class="close" /></div>
                    </div>
                <?php } ?> 
                    <?php if($error_warning_shipping){ ?>
                    <div class="supercheckout-checkout-content" style="display:block">
                        <div class="warning" style="display: block;"><?php echo $error_warning_shipping; ?><img src="catalog/view/theme/default/image/close.png" alt="" class="close" /></div>
                    </div>
                    <?php } ?>
                    <div class="supercheckout-checkout-content">

                    </div>
                    <?php if ($error_warning) { ?>
                    <div class="warning"><?php echo $error_warning; ?></div>
                    <?php } ?>
                    <?php if ($shipping_methods && $shipping_required) { ?>

                    <table class="radio">
                        <?php foreach ($shipping_methods as $shipping_method) { ?>
                        <?php if($settings['step']['shipping_method']['display_title']){ ?>
                        <tr>
                            <td colspan="3"><b><?php echo $shipping_method['title']; ?></b></td>
                        </tr>
                        <?php } ?>
                        <?php if (!$shipping_method['error']) { ?>
                        <?php foreach ($shipping_method['quote'] as $quote) { ?>
                        <tr class="highlight">
                            <td><?php if(!isset($shipping_code)){ $shipping_code = ''; }if(($quote['code'] == $shipping_code) || !(isset($shipping_code))) { ?>
                                <?php $codeShipping = $quote['code']; ?>
                                <input type="radio" name="shipping_method" value="<?php echo $quote['code']; ?>" id="<?php echo $quote['code']; ?>" checked="checked" />
                                <?php } else { ?>
                                <input type="radio" name="shipping_method" value="<?php echo $quote['code']; ?>" id="<?php echo $quote['code']; ?>" />
                                <?php } ?></td>
                            <td><label for="<?php echo $quote['code']; ?>"><?php echo $quote['title']; ?></label></td>
                            <td style="text-align: right;" class="price"><label for="<?php echo $quote['code']; ?>"><?php echo $quote['text']; ?></label></td>
                        </tr>
                        <?php } ?>
                        <?php } else { ?>
                        <tr>
                            <td colspan="3"><div class="error"><?php echo $shipping_method['error']; ?></div></td>
                        </tr>
                        <?php } ?>
                        <?php } ?>
                    </table>
                    <br />
                    <?php } ?>
                </div>
                
            </div>
            <?php } ?>
            <?php if($settings['step']['payment_method']['display_options']){ ?>
            <div  style="display: none" class="supercheckout-blocks" data-column="<?php echo $sort_block['payment_method'][$layout_name]['column']; ?>" data-row="<?php echo $sort_block['payment_method'][$layout_name]['row']; ?>" data-column-inside="<?php echo $sort_block['payment_method'][$layout_name]['column-inside']; ?>">
                <ul>
                    <li>
                        <p class="supercheckout-numbers supercheckout-numbers-4"><?php echo $text_payment_method; ?></p>
                        <div class="loader" id="paymentMethodLoader"></div>
                    </li>                
                </ul>
                
                <div id="payment-method">
                    <div class="supercheckout-checkout-content">

                    </div>
                    <?php if ($error_warning) { ?>
                    <div class="warning"><?php echo $error_warning; ?></div>
                    <?php } ?>
                    <?php if ($payment_methods) { ?>                 
                    <table class="radio">
                        <?php foreach ($payment_methods as $payment_method) { ?>
                        <tr class="highlight">
                            <td><?php if ($payment_method['code'] == $payment_code || !$payment_code) { ?>
                                <?php $code = $payment_method['code']; ?>
                                <input type="radio" name="payment_method" value="<?php echo $payment_method['code']; ?>" id="<?php echo $payment_method['code']; ?>"  checked="checked" />
                                <?php } else { ?>
                                <input type="radio" name="payment_method" value="<?php echo $payment_method['code']; ?>" id="<?php echo $payment_method['code']; ?>"  />
                                <?php } ?></td>
                            <td><label for="<?php echo $payment_method['code']; ?>"><?php echo $payment_method['title']; ?></label></td>
                        </tr>
                        <?php } ?>
                    </table>                 
                    <?php } ?>
                </div>
                
            </div>
            <?php } ?>
            <div class="supercheckout-blocks confirmCheckoutBack" data-column="<?php echo $sort_block['cart'][$layout_name]['column']; ?>" data-row="<?php echo $sort_block['cart'][$layout_name]['row']; ?>" data-column-inside="<?php echo $sort_block['cart'][$layout_name]['column-inside']; ?>" style="display:<?php if($logged){ if($settings['option']['logged']['cart']['display']){ echo' ';}else{ echo'none';} }else{ if($settings['option']['guest']['cart']['display']){ echo' ';}else{ echo'none';}} ?>;">
                <ul>
                    <li>
                        <p class="supercheckout-numbers supercheckout-check"><?php echo $text_confirm_order; ?></p>
                        <div class="loader"></div>
                    </li>
                </ul>
                <div id="confirmCheckout">
                    <div class="supercheckout-checkout-content">

                    </div>
                    <?php if (!isset($redirect)) { ?>
                    
                    <table class="supercheckout-summary">
                        <thead>
                            <tr>
                                
                                <th style="display:<?php if($logged){ if($settings['option']['logged']['cart']['columns']['name']){ echo' ';}else{ echo'none';} }else{ if($settings['option']['guest']['cart']['columns']['name']){ echo' ';}else{ echo'none';}} ?>;" class="supercheckout-name"><?php echo $column_name; ?></th>
                                <th style="display:<?php if($logged){ if($settings['option']['logged']['cart']['columns']['model']){ echo' ';}else{ echo'none';} }else{ if($settings['option']['guest']['cart']['columns']['model']){ echo' ';}else{ echo'none';}} ?>;" class="supercheckout-qty"><?php echo $column_model; ?></th>
                                <th style="display:<?php if($logged){ if($settings['option']['logged']['cart']['columns']['quantity']){ echo' ';}else{ echo'none';} }else{ if($settings['option']['guest']['cart']['columns']['quantity']){ echo' ';}else{ echo'none';}} ?>;" class="supercheckout-qty"><?php echo $column_quantity; ?></th>
                                <th style="display:<?php if($logged){ if($settings['option']['logged']['cart']['columns']['price']){ echo' ';}else{ echo'none';} }else{ if($settings['option']['guest']['cart']['columns']['price']){ echo' ';}else{ echo'none';}} ?>;" class="supercheckout-total" style="text-align:center;"><?php echo $column_price; ?></th>
                                <th style="display:<?php if($logged){ if($settings['option']['logged']['cart']['columns']['total']){ echo' ';}else{ echo'none';} }else{ if($settings['option']['guest']['cart']['columns']['total']){ echo' ';}else{ echo'none';}} ?>;" class="supercheckout-total"><?php echo $column_total; ?></th>
                                <th class="supercheckout-qty"><?php echo $column_action; ?></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($products as $product) { ?>
                            <tr>
                                <td style="display:<?php if($logged){ if($settings['option']['logged']['cart']['columns']['name']){ echo' ';}else{ echo'none';} }else{ if($settings['option']['guest']['cart']['columns']['name']){ echo' ';}else{ echo'none';}} ?>;" class="supercheckout-name">
                                    
                                    <div ><a <?php if($logged){ if($settings['option']['logged']['cart']['columns']['image']){ echo'data-toggle="popover"';}else{ echo'';} }else{ if($settings['option']['guest']['cart']['columns']['image']){ echo'data-toggle="popover"';}else{ echo'';}} ?> data-title="<?php echo $product['name']; ?>" data-content="<img src='<?php echo $product['thumb']; ?>' />" data-placement="right" href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a>
                                    <?php foreach ($product['option'] as $option) { ?>
                                    <br />
                                    &nbsp;<small> - <?php echo $option['name']; ?>: <?php echo $option['value']; ?></small>
                                    <?php } ?></div>
                                </td>
                                <td style="display:<?php if($logged){ if($settings['option']['logged']['cart']['columns']['model']){ echo' ';}else{ echo'none';} }else{ if($settings['option']['guest']['cart']['columns']['model']){ echo' ';}else{ echo'none';}} ?>;" class="supercheckout-qty"><?php echo $product['model']; ?></td>
                                <td style="display:<?php if($logged){ if($settings['option']['logged']['cart']['columns']['quantity']){ echo' ';}else{ echo'none';} }else{ if($settings['option']['guest']['cart']['columns']['quantity']){ echo' ';}else{ echo'none';}} ?>;" class="supercheckout-qty">
                                    <input class="quantitybox" type="text" name="quantity[<?php echo $product['key']; ?>]" value="<?php echo $product['quantity']; ?>" ><br>
                                    <a href="javascript://" id="<?php echo $product['key']; ?>" onclick="updateQuantity(this.id);" ><small><?php echo $button_update_link; ?></small></a>
                                </td>
                                <td style="display:<?php if($logged){ if($settings['option']['logged']['cart']['columns']['price']){ echo' ';}else{ echo'none';} }else{ if($settings['option']['guest']['cart']['columns']['price']){ echo' ';}else{ echo'none';}} ?>;" class="supercheckout-total"><?php echo $product['price']; ?></td>
                                <td style="display:<?php if($logged){ if($settings['option']['logged']['cart']['columns']['total']){ echo' ';}else{ echo'none';} }else{ if($settings['option']['guest']['cart']['columns']['total']){ echo' ';}else{ echo'none';}} ?>;" class="supercheckout-total"><?php echo $product['total']; ?></td>
                                <td class="supercheckout-qty"><a href="javascript://" id="<?php echo $product['key']; ?>" onclick="removeProduct(this.id);" class="removeProduct"><div id="<?php echo $product['key']; ?>"  title="Delete"></div></a></td>
                            </tr>
                            <?php } ?>
                            <?php foreach ($vouchers as $voucher) { ?>
                            <tr>
                                <td class="supercheckout-name"><?php echo $voucher['description']; ?></td>
                                <td class="supercheckout-qty">1</td>
                                <td class="supercheckout-total"><?php echo $voucher['amount']; ?></td>
                                <td class="supercheckout-total"><?php echo $voucher['amount']; ?></td>
                            </tr>
                            <?php } ?>
                        </tbody>                    
                    </table>
                    <table class="supercheckout-totals">
                        <tbody>
                                <?php foreach ($totals as $total) { ?>
                            <tr>
                                <td class="title"><b><?php echo $total['title']; if($total['code']=='voucher' || $total['code']=='coupon'){echo'<a href="javascript://" id="'.$total['code'].'"  onclick="redeem(this.id);" ><div title="Redeem" class="removeProduct"></div></a></td>';} ?></b></td>
                                <td class="value"><span class="price"><?php echo $total['text']; ?></span> </td>                                
                            </tr>
                            <?php } ?>
                            <tr style="display:<?php if($logged){ if($settings['option']['logged']['cart']['option']['coupon']['display']){ echo' ';}else{ echo'none';} }else{ if($settings['option']['guest']['cart']['option']['coupon']['display']){ echo' ';}else{ echo'none';}} ?>;">
                                <td class="title"><b><?php echo $text_coupon_code; ?></b></td>
                                <td class="value"><input  id="coupon_code" name="coupon" type="text" class="voucherText">
                                    <input type="hidden" value="coupon" name="next">
                                    <input id="button-coupon" type="button" onClick="if(window.couponBlur==true){ callCoupon(); }" class="orangebuttonapply" value="Apply">
                                </td>
                            </tr>
                            <tr style="display:<?php if($logged){ if($settings['option']['logged']['cart']['option']['voucher']['display']){ echo' ';}else{ echo'none';} }else{ if($settings['option']['guest']['cart']['option']['voucher']['display']){ echo' ';}else{ echo'none';}} ?>;">
                                <td class="title"><b><?php echo $text_voucher_code; ?></b></td>
                                <td class="value"><input  id="voucher_code" name="voucher" type="text" class="voucherText">
                                    <input type="hidden" value="voucher" name="next">
                                    <input id="button-voucher" type="button" onClick="if(window.voucherBlur==true){ callVoucher(); }" class="orangebuttonapply" value="Apply">
                                </td>
                            </tr>
                        </tbody>
                    </table>            
                    <?php } ?>
                </div>

            </div>
            <div style="display:none" id="payment_display_block"  class="supercheckout-blocks" data-column="<?php echo $sort_block['confirm'][$layout_name]['column']; ?>" data-row="<?php echo $sort_block['confirm'][$layout_name]['row']; ?>" data-column-inside="<?php echo $sort_block['confirm'][$layout_name]['column-inside']; ?>" >
                <div class="supercheckout-checkout-content"> </div>
                <div id="display_payment">
                    
                    <?php echo $payment_display; ?>
                </div>
                
                <?php if ($text_agree) { ?>                
                <div id="supercheckout-agree" style="display:<?php if($logged){ if($settings['option']['logged']['confirm']['fields']['agree']['display']){ echo'block';}else{ echo'none';} }else{ if($settings['option']['guest']['confirm']['fields']['agree']['display']){ echo'block';}else{ echo'none';}} ?>;">
                    
                    <?php if ($agree) { ?>
                    <input type="checkbox" name="agree" value="1" checked="checked" />
                    <?php } else { ?>
                    <input type="checkbox" name="agree" value="1" />
                    <?php } ?><?php echo " ".$text_agree; ?>
                </div>                
                <?php } ?>
                <div id="placeorderButton-old">
                    <div id="buttonWithProgres" style="width:206px;">
                        <div  id="confirm_order" class="orangebutton" >
                            <?php echo $button_place_order; ?>
                            <div id="progressbar" style="text-align:center;margin-top: 0px;"></div>
                        </div>
                    
                    </div>
                </div>
            </div>
            
        </div>

        <div class="supercheckout-column-middle columnleftsort" id="columnleft-2"  style="width:<?php $i= $settings['general']['column_width'][$layout_name][2]*$multiplier; echo $i; ?>%;margin-right:0px;">
            
        <div class="title">
            <h2><?php echo $text_delivery_details_part_1; ?></h2>
            <h1><?php echo $text_delivery_details_part_2; ?></h1>
        </div>
            
            <div class="supercheckout-column-left columnleftsort" id="column-2-upper" style="width:100%;height:auto;"> 
            </div>
            <div class="supercheckout-column-left columnleftsort" id="column-1-inside" style="width:<?php $i= $settings['general']['column_width'][$layout_name]['inside'][1]*.914; echo $i; ?>%"> 
            </div>
            <div class="supercheckout-column-left columnleftsort" id="column-2-inside"  style="width:<?php $i= $settings['general']['column_width'][$layout_name]['inside'][2]*.914; echo $i; ?>%">
            
            </div>
            <div class="supercheckout-column-left columnleftsort" id="column-2-lower"  style="width:100%;height:auto;">
            
            </div>
        </div>
        <div class="supercheckout-column-right columnleftsort" id="columnleft-3" style="width:<?php $i= $settings['general']['column_width'][$layout_name][3]*$multiplier; echo $i; ?>%">
        
        <div class="title">
            <h2><?php echo $text_your_order_part_1; ?></h2>
            <h1><?php echo $text_your_order_part_2; ?></h1>
        </div>        
        
        </div>   
        
    </div>
    
</fieldset>
</div>
</div>

<script type="text/javascript">
    
    /************************************
     *  edit cart functions
     ************************************/
    
        var edit_max_qunt = 1;
    
   
    
    $(".edit-cart").live("click", function() {
	
        //$(".choosen-for-edit").removeClass("choosen-for-edit");
	$(".choosen-for-edit").addClass("edit-cart");
	$(".choosen-for-edit").removeClass("choosen-for-edit");
	
        $(this).addClass("choosen-for-edit");
	$(".choosen-for-edit").removeClass("edit-cart");
        
        // disabling button
        //$(".edit-cart").addClass("edit-remove");
        //$(".edit-remove").removeClass("edit-cart");
        
        var price = $(this).find("#pro-total").attr("data-price");
        var title = $(this).find("a").attr("data-title");
        
        $("#edit-int-price").val(price);
        
        var init_qty = $(this).find("#pro-qty").attr("data-qty");
        var side_1_id = $(this).find("#side-1").attr("data-pid");
        var side_2_id = $(this).find("#side-2").attr("data-pid");
        var comment_text = $(this).find("#comment-checkout").text();
        comment_text = $.trim(comment_text);
        comment_text = comment_text.substr(1, comment_text.length -2);
        
	var date = "<?php echo $this->session->data['date']; ?>";
        var time = parseInt("<?php echo $this->session->data['time']; ?>");
        var cook = "<?php echo $this->session->data['cook']; ?>";
        var product_id = $(this).attr("data-pid");
        var cart_key = $(this).attr("data-key");
        
        $(".edit-box input[name='product_id']").val(product_id);
        $(".edit-box input[name='option[date]']").val(date);
        $(".edit-box input[name='option[cook_id]']").val(cook);
        $(".edit-box textarea[name='option[comment]']").val(comment_text);
        $(".edit-box .title h1").text(title);
        
        //removing options
        $(".edit-box option").remove();
        
	$.ajax({
		url: 'index.php?route=product/product/ajax_update',
		type: 'post',
		dataType: 'json',
		data: 'date=' + encodeURIComponent(date) + '&ec=true&product_id=' + product_id,
		beforeSend: function() {
			//show loading
                        $("#edit-cart .loading-edit").show();
                        // opening edit box
			if ($("#edit-cart").css("display") == "none" ) {
			    $("#edit-cart").toggle( "slide" , { direction: "right" } );
			} else {
			    
			}
		},
		complete: function() {
			//hide loading
                        $("#edit-cart .loading-edit").fadeOut();
		},
		success: function(data) {
			
			var html = "";
			var passed = "";
			
			if (data.failed) {
                            
                            $(".choosen-for-edit").addClass("edit-cart");
			    $(".choosen-for-edit").removeClass("choosen-for-edit");
                            
			    return;
			
			} else {
                            
                            //$("#edit-int-price").val()
                            $("#button-edit-cart").attr("data-key",cart_key);
                            
                            var side_option = '';
                            
                            // adding side dish options
                            if (data.side_num != 0) {
                                data.sides.forEach(function(entry) {
                                    
                                    var side_1_price = "";
                                    if (entry.price_int > 0) {
                                        side_1_price = " - בתוספת ₪"+entry.price_int+"";
                                    }
                                    
                                    if (entry.product_id == side_1_id) {
                                        side_option = '<option data-price="'+entry.price_int+'" value="'+entry.product_id+'" selected="selected">'+entry.name+side_1_price+'</option>';
                                    } else {
                                        side_option = '<option data-price="'+entry.price_int+'" value="'+entry.product_id+'">'+entry.name+side_1_price+'</option>';
                                    }
                                    
                                    $(".edit-box select[data-name='side-1']").append(side_option);
                                    
                                });
                            } else {
                                $(".edit-box select[data-name='side-1']").parent().hide();
                            }
                            
                            if (data.side_num == 2) {
                                data.sides.forEach(function(entry) {
                                    
                                    var side_2_price = "";
                                    if (entry.price_int > 0) {
                                        side_2_price = " - בתוספת ₪"+entry.price_int+"";
                                    }
                                    
                                    if (entry.product_id == side_2_id) {
                                        side_option = '<option data-price="'+entry.price_int+'" value="'+entry.product_id+'" selected="selected">'+entry.name+side_2_price+'</option>';
                                    } else {
                                        side_option = '<option data-price="'+entry.price_int+'" value="'+entry.product_id+'">'+entry.name+side_2_price+'</option>';
                                    }
                                    
                                    $(".edit-box select[data-name='side-2']").append(side_option);
                                    
                                });
                            } else {
                                $(".edit-box select[data-name='side-2']").parent().hide();
                            }
                            
                            edit_max_qunt = parseInt(data.intqunt) + parseInt(init_qty);
                            $('.edit-box .text_qty').html("נותרו " + edit_max_qunt);
                            $('#edit-quantity-input').val(init_qty);
                            $('#edit-quantity-input').trigger("change");
                            
                        }
		}
	});
    });

    
    $('#remove-from-cart').live('click', function(event) {
        
        event.stopPropagation();
        
        $(".loading-screen").show();
        
        var mykey = $('#remove-from-cart').parent().parent().attr("data-key");
        
        $.ajax({
            type: "POST",
            url: "index.php?route=supercheckout/supercheckout/cart",
            data: 'remove='+mykey,
            success: function(msg){
                
                //refreshAll();
                location.reload();
                //$("#edit-cart").toggle( "slide" , { direction: "right" } );
                //$(".edit-remove").addClass("edit-cart");
                //$(".edit-cart").removeClass("edit-remove");
                
            }
        });
        
        
    });
    
    $('#button-edit-cart').live('click', function() {
        
        // first we need to remove the old item
        var mykey = $(this).attr("data-key");
        
        $.ajax({
            type: "POST",
            url: "index.php?route=supercheckout/supercheckout/cart",
            data: 'remove='+mykey,
            success: function(msg){
                $.ajax({
                    url: 'index.php?route=checkout/cart/add',
                    type: 'post',
                    data: $('.edit-box input[type=\'text\'], .edit-box input[type=\'hidden\'], .edit-box input[type=\'radio\']:checked, .edit-box input[type=\'checkbox\']:checked, .edit-box select, .edit-box textarea'),
                    dataType: 'json',
                    success: function(json) {
                            $('.success, .warning, .attention, information, .error').remove();
                            
                            if (json['error']) {
                                    if (json['error']['option']) {
                                            for (i in json['error']['option']) {
                                                    $('#option-' + i).after('<span class="error">' + json['error']['option'][i] + '</span>');
                                            }
                                    }
                                    
                                    if (json['error']['profile']) {
                                            $('select[name="profile_id"]').after('<span class="error">' + json['error']['profile'] + '</span>');
                                    }
                            }
                            
                            if (json['success']) {
                                    
                                    
                                    $("#edit-cart").toggle( "slide" , { direction: "right" } );
				    $(".choosen-for-edit").addClass("edit-cart");
				    $(".choosen-for-edit").removeClass("choosen-for-edit");
                                    
                                    $("#day").trigger("change");
                                    $('#button-show-cart').trigger("click");
				    
				    refreshAll();
				    setTimeout(function(){
				    	refreshAll();
				    },500);
                                    
                            }	
                    }
                });	
            }
        });
        
        
    });
    
    $('#button-edit-cancel').live('click', function() {
        $("#edit-cart").toggle( "slide" , { direction: "right" } );
	$(".choosen-for-edit").addClass("edit-cart");
        $(".choosen-for-edit").removeClass("choosen-for-edit");
        
    });
    
    $(".choosen-for-edit").live('click', function() {
        $("#edit-cart").toggle( "slide" , { direction: "right" } );
	$(".choosen-for-edit").addClass("edit-cart");
        $(".choosen-for-edit").removeClass("choosen-for-edit");
    });
    
    
    $('#edit-quantity-input').live('change', function() {
	var multi = $(this).val();
	var price = $("#edit-int-price").val();
        
        price = parseInt(price.substr(1));
	
	var side_1 = $('.edit-box select[name="option[side-1]"]');
	
	if(side_1.val()) {
	    var side_1_price = side_1.find('option[value="'+side_1.val()+'"]').attr("data-price");
	} else {
	    var side_1_price = 0;
	}
        
	var side_2 = $('.edit-box select[name="option[side-2]"]');
	
	if(side_2.val()) {
	    var side_2_price = side_2.find('option[value="'+side_2.val()+'"]').attr("data-price");
	} else {
	    var side_2_price = 0;
	}
	
	var extra = 0;//parseInt(side_1_price) + parseInt(side_2_price); //$("#extra-price").val();
	
	var total = price + extra;
        
	$("#edit-total-price").html("₪"+parseFloat(Math.round(multi*total* 100) / 100));
    });
    
    $(".edit-plus-button").live("click",function () {
	if (!(parseInt($(this).parent().find("input").val()) == edit_max_qunt )) {
	   $(this).parent().find("input").val( parseInt($(this).parent().find("input").val())+1);
	   $('#edit-quantity-input').trigger("change");
	}
    });
    
    $(".edit-minus-button").live("click",function () {
	if (!(parseInt($(this).parent().find("input").val()) <= 1 )) {
	   $(this).parent().find("input").val( parseInt($(this).parent().find("input").val())-1);
	   $('#edit-quantity-input').trigger("change");
	}
    });
    
    
    // edit cart end
    
    // allwayz tel aviv
    $("#payment_address_table input[name='city']").val("תל אביב");
    
    $("input[name='apartment'],input[name='floor'],input[name='entrance']").on('change', function() {
        $("#payment-address-new").trigger("click");
    });
    
    // start    
    $("input[name='apartment'],input[name='floor'],input[name='entrance']").on('blur', function() {
        
        var apartment = $("input[name='apartment']").val();
        var floor = $("input[name='floor']").val();
        var entrance = $("input[name='entrance']").val();
        
        if (apartment == "") {
            apartment = "-"
        }
        if (floor == "") {
            floor = "-"
        }
        
        if (entrance == "") {
            entrance = "-"
        }
        
        var old_val = $("#payment_address_table input[name='address_2']").val();
        
        $("#payment_address_table input[name='address_2']").val("קומה "+floor+" דירה "+apartment+" כניסה "+entrance);
        
    });
    
    $("input[name='mobile']").on('change', function() {
       
        $("#payment-address-new").trigger("click");
    });
    
    $("input[name='mobile']").on('blur', function() {
        
        var phone_num = $.trim($(this).val().replace(/[A-Za-z$-]/g, ""));
        
        $("#payment_address_table input[name='telephone']").val(phone_num);
        
        $("#payment_address_table input[name='postcode']").val(phone_num);
        
        $('#payment_address_table input[name="phone"]').val(phone_num);
        
        $(this).val(phone_num);
    });
    
    $("input[name='homeals-company']").on('change', function() {
        $("#payment-address-new").trigger("click");
    });

    $("input[name='homeals-company']").on('blur', function() {
        $("#payment_address_table input[name='company']").val($(this).val());
    });
    
    $('#supercheckout-comment_order').on('change', function() {
        $("#payment-address-new").trigger("click");
    });

    $('#supercheckout-comment_order').on('blur', function() {
        $("#payment_address_table textarea[name='address_comment']").val($(this).val());
    });
    
    $("input[name='contact']").on('change', function() {
        $("#payment-address-new").trigger("click");
    });
    
    $("input[name='contact']").on('blur', function() {
        
        var str = $(this).val();
        var res = str.split(" ");
        
        if (!res[1]) {
            res[1] = "";
        } else {
            res[1] = res[1];
        }
        
        if (!res[2]) {
            res[2] = ""
        }else {
            res[2] = " "+res[2];
        }
        
        if (!res[3]) {
            res[3] = ""
        }else {
            res[3] = " "+res[3];
        }
        
        $("#payment_address_table input[name='firstname']").val(res[0]);
        $("#payment_address_table input[name='lastname']").val(res[1]+res[2]+res[3]);
    });
    
    $(".pay-with-credit").on("click",function() {
	
	// pp standart
	//$("#ppexbtn").prepend('<input type="hidden" name="solution_type" value="Sole" />')
	//$("#payapl-form").prepend('<input type="hidden" name="landing_page" value="Billing" />')
	
        $("#confirm_order").trigger("click");
    });
    
    $(".pay-with-paypal").on("click",function() {
	
	// pp standart
	$("#ppexbtn").attr("href",$("#ppexbtn").attr("href")+"&paypal");
	
        $("#confirm_order").trigger("click");
    });
    
    
    var street_name = $.trim($(".homeals-delivery-address").val());
    
    var error_street = false;
    
    var checkout_options = { serviceUrl:'system/helper/autocomplete.php',
                // callback function:
                
                onSelect: function(data){
                    street_name = data.value;
                    $(".homeals-delivery-address").trigger("change");
                }
        };
        
    $(".homeals-delivery-address").autocomplete(checkout_options);
    
    $(".homeals-delivery-address").on("change", function(event){
        
        event.stopPropagation();
        
        if ($.trim($(".homeals-delivery-address").val()) != "") {
            
            error_street = false;
	    
            var street_name = $.trim($(".homeals-delivery-address").val());
            var street_number = $.trim($("input[name='street_num']").val());
            $("#payment_address_table input[name='address_1']").val(street_name+" #"+street_number);
            
        } else {
            
            error_street = true;
            
            $(".homeals-delivery-address").val("");
            $("#payment_address_table input[name='address_1']").val("");
        }
        
        $("#payment-address-new").trigger("click");
        
    });
    
    
    
    $("input[name='street_num']").on('change', function() {
        $("#payment-address-new").trigger("click");
    });

    $("input[name='street_num']").on('blur', function() {
        $(".homeals-delivery-address").trigger("change");
    });
    
    
    /*** DISABLING CONFIRM BUTTON FOR 5 SEC AFTER IT IS CLICKED ONCE */
    $("#confirm_order").click(function() {
	
	if($("#ppexbtn").attr("href")) {
	    
	    $('#confirmLoader').show();
	    $('#confirm_order').attr('disabled', true);
	    var btn = $(this);
	    btn.prop('disabled', true);
	    setTimeout(function(){
		btn.prop('disabled', false);
		
		$('#cboxLoadingOverlay').hide();
		$('#cboxLoadingGraphic').hide();
		
		//refreshAll();
		
	    }, 6000);
	    
	    if(window.confirmclick==true){
		window.callconfirm=true;
	    }else{
		validateCheckout();
	    }
	    
	} else {
	   
	    refreshAll();
	    setTimeout(function(){
	    $("#confirm_order").click();
	    }, 1500); 
	    
	}
    });

    /*** ON CHANGING PAYMENT METHOD */
    $("input[name='payment_method']").change(function(){
        /*$('#confirmLoader').show();*/
        $.ajax({
            url: 'index.php?route=supercheckout/payment_method/validate',
            type: 'post',
            data: $('#payment-method input[type=\'radio\']:checked, #payment-method input[type=\'checkbox\']:checked, #payment-method textarea'),
            dataType: 'json',
            beforeSend: function() {
                $('#button-payment-method').attr('disabled', true);
                $('#button-payment-method').after('<span class="wait">&nbsp;<img src="catalog/view/theme/default/image/supercheckout/loading12.gif" alt="" /></span>');
            },
            complete: function() {
                $('#button-payment-method').attr('disabled', false);
                $('.wait').remove();
            },
            success: function(json) {
                //$('#confirmLoader').hide();
                $('.warning, .error').remove();

                if (json['redirect']) {
                    location = json['redirect'];
                } else if (json['error']) {
                    if (json['error']['warning']) {
                        $('#payment-method .supercheckout-checkout-content').prepend('<div class="warning" style="display: none;">' + json['error']['warning'] + '<img src="catalog/view/theme/default/image/close.png" alt="" class="close" /></div>');
                        $('.warning').fadeIn('slow');
                    }
                } else {
                    $.ajax({
                        url: 'index.php?route=supercheckout/payment_display',
                        dataType: 'html',
                        success: function(html) {
                            $('#display_payment').html(html);
                            //validatePaymentMethodRefresh();
                        },
                        error: function(xhr, ajaxOptions, thrownError) {
                            console.log(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                        }
                    });
                }
            },
            error: function(xhr, ajaxOptions, thrownError) {
                console.log(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
        });
    });

    // ON CHANGING SHIPPING  METHOD
    $("input[name='shipping_method']").change(function(){
        /*$('#confirmLoader').show();*/
        $.ajax({
            url: 'index.php?route=supercheckout/shipping_method/validate',
            type: 'post',
            data: $('#shipping-method input[type=\'radio\']:checked'),
            dataType: 'json',
            beforeSend: function() {
                $('#button-shipping-method').attr('disabled', true);
                $('#button-shipping-method').after('<span class="wait">&nbsp;<img src="catalog/view/theme/default/image/supercheckout/loading12.gif" alt="" /></span>');
            },
            complete: function() {
                $('#button-shipping-method').attr('disabled', false);
                $('.wait').remove();
            },
            success: function(json) {
                //$('#confirmLoader').hide();
                $('.warning, .error').remove();

                if (json['redirect']) {
                    //location = json['redirect'];
                } else if (json['error']) {
                    if (json['error']['warning']) {
                        $('#shipping-method .supercheckout-checkout-content').prepend('<div class="warning" style="display: none;">' + json['error']['warning'] + '<img src="catalog/view/theme/default/image/close.png" alt="" class="close" /></div>');
                        $('.warning').fadeIn('slow');
                    }
                } else {
                    /*$('#confirmLoader').show();*/
                    $.ajax({
                        url: 'index.php?route=supercheckout/confirm',
                        dataType: 'html',
                        success: function(html) {
                            //$('#confirmLoader').hide();
                            $('#confirmCheckout').html(html);
                            $('#paymentDisable').html("");
                            $.ajax({
                                url: 'index.php?route=supercheckout/payment_display',
                                dataType: 'html',
                                success: function(html) {
                                    $('#display_payment').html(html);
                                    //validatePaymentMethodRefresh();
                                },
                                error: function(xhr, ajaxOptions, thrownError) {
                                    console.log(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                                }
                            });
                        },
                        error: function(xhr, ajaxOptions, thrownError) {
                            console.log(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                        }
                    });
                }
            },
            error: function(xhr, ajaxOptions, thrownError) {
                console.log(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
        });
    });

    // ON CONFIRM CLICK VALIDATING THE WHOLE PAGE AT ONCE AND DISPLAYS ERROR ACCORDINGLY
    function goToByScroll(id){
        // Remove "link" from the ID
        id = id.replace("link", "");
        // Scroll
        $('html,body').animate({scrollTop: $("#"+id).offset().top}, 'slow');
    }

function validatePaymentAddress(){

    var paymentAddressEnable="1";
    
    var pre_check = true;
    
    if (!$.trim($('input[name="mobile"]').val())) {
        $("#wh-phone").fadeIn();
        $('input[name="mobile"]').addClass("warn-red");
        pre_check = false
    } else {
        $("#wh-phone").fadeOut();
        $('input[name="mobile"]').removeClass("warn-red");
    }
    
    if (!$.trim($('input[name="contact"]').val())) {
        $("#wh-contact").fadeIn();
        $('input[name="contact"]').addClass("warn-red");
        pre_check = false
    } else {
        $("#wh-contact").fadeOut();
        $('input[name="contact"]').removeClass("warn-red");
    }
    
    if (!$.trim($('input[name="street_num"]').val())) {
        $("#wh-street_num").fadeIn();
        $('input[name="street_num"]').addClass("warn-red");
        pre_check = false
    } else {
        $("#wh-street_num").fadeOut();
        $('input[name="street_num"]').removeClass("warn-red");
    }
    
    if (error_street) {
        $("#wh-street_name").fadeIn();
        $('input[name="homeals-delivery-address"]').addClass("warn-red");
        pre_check = false
    } else {
        $("#wh-street_name").fadeOut();
        $('input[name="homeals-delivery-address"]').removeClass("warn-red");
    }
    
    //if (!$('input[name="floor"]').val()) {
    //    $("#wh-floor").fadeIn();
    //    $('input[name="floor"]').addClass("warn-red");
    //    pre_check = false
    //} else {
    //    $("#wh-floor").fadeOut();
    //    $('input[name="floor"]').removeClass("warn-red");
    //}
    
    if (pre_check) {
    
    if(paymentAddressEnable==1){
        $.ajax({
            url: 'index.php?route=supercheckout/supercheckout/loginPaymentAddressValidate',
            type: 'post',
            data: $('#checkoutBillingAddress input[type=\'text\'],#checkoutBillingAddress textarea, #checkoutBillingAddress input[type=\'password\'], #checkoutBillingAddress input[type=\'checkbox\']:checked, #checkoutBillingAddress input[type=\'radio\']:checked, #checkoutBillingAddress input[type=\'hidden\'], #checkoutBillingAddress select'),
            dataType: 'json',
            beforeSend: function() {
                $('#button-payment-address').attr('disabled', true);
                $('#button-payment-address').after('<span class="wait">&nbsp;<img src="catalog/view/theme/default/image/supercheckout/loading12.gif" alt="" /></span>');
            },
            complete: function() {
                $('#button-payment-address').attr('disabled', false);
                $('.wait').remove();
            },
            success: function(json) {
                $('.warning, .errorsmall').remove();

                if (json['redirect']) {
                    location = json['redirect'];
                } else if (json['error']) {
                    goToByScroll('checkoutBillingAddress');
                    if (json['error']['warning']) {
                        $('#checkoutBillingAddress .supercheckout-checkout-content').prepend('<div class="warning" style="display: none;">' + json['error']['warning'] + '<img src="catalog/view/theme/default/image/close.png" alt="" class="close" /></div>');
                        $('#checkoutBillingAddress .supercheckout-checkout-content').show();
                        $('.warning').fadeIn('slow');
                    }

                    if (json['error']['firstname']) {
                        $('#payment-new input[name=\'contact\']').addClass("warn-red");
                        $('#payment-new input[name=\'firstname\']').after('<span class="errorsmall">' + json['error']['firstname'] + '</span>');
                    } else {
                        $('#payment-new input[name=\'contact\']').removeClass("warn-red");
                    }

                    if (json['error']['lastname']) {
                        $('#payment-new input[name=\'lastname\']').after('<span class="errorsmall">' + json['error']['lastname'] + '</span>');
                    }

                    if (json['error']['telephone']) { 
                        $('#payment-new input[name=\'mobile\']').addClass("warn-red");
                        $('#payment-new input[name=\'telephone\']').after('<span class="errorsmall">' + json['error']['telephone'] + '</span>');
                    } else {
                        $('#payment-new input[name=\'mobile\']').removeClass("warn-red");
                    }
                    
                    if (json['error']['company']) {
                        $('#payment-new input[name=\'company\']').after('<span class="errorsmall">' + json['error']['company'] + '</span>');
                    } else {
                        $('#payment-new input[name=\'apartment\']').removeClass("warn-red");
                    }
                    
                    if (json['error']['company_id']) {
                        $('#payment-new input[name=\'company_id\']').after('<span class="errorsmall">' + json['error']['company_id'] + '</span>');
                    }

                    if (json['error']['tax_id']) {
                        $('#payment-new input[name=\'tax_id\']').after('<span class="errorsmall">' + json['error']['tax_id'] + '</span>');
                    }

                    if (json['error']['address_1']) {
                        $('#payment-new input[name=\'homeals-delivery-address\']').addClass("warn-red");
                        $('#payment-new input[name=\'address_1\']').after('<span class="errorsmall">' + json['error']['address_1'] + '</span>');
                    } else {
                        $('#payment-new input[name=\'homeals-delivery-address\']').removeClass("warn-red");
                    }
                    
                    if (json['error']['address_2']) {
                        $('#payment-new input[name=\'address_2\']').after('<span class="errorsmall">' + json['error']['address_2'] + '</span>');
                    }

                    if (json['error']['city']) {
                        $('#payment-new input[name=\'homeals-delivery-address\']').addClass("warn-red");
                        $('#payment-new input[name=\'city\']').after('<span class="errorsmall">' + json['error']['city'] + '</span>');
                    } else {
                        $('#payment-new input[name=\'homeals-delivery-address\']').removeClass("warn-red");
                    }

                    if (json['error']['postcode']) {
                        $('#payment-new input[name=\'phone\']').addClass("warn-red");
                        $('#payment-new input[name=\'postcode\']').after('<span class="errorsmall">' + json['error']['postcode'] + '</span>');
                    } else {
                        $('#payment-new input[name=\'floor\']').removeClass("warn-red");
                    }

                    if (json['error']['country']) {
                        $('#payment-new select[name=\'country_id\']').after('<span class="errorsmall">' + json['error']['country'] + '</span>');
                    }

                    if (json['error']['zone']) {
                        $('#payment-new select[name=\'zone_id\']').after('<span class="errorsmall">' + json['error']['zone'] + '</span>');
                    }
                } else {
                    $("#progressbar" ).progressbar({ value:35 });
                    validateLoginShippingAddress();
                }
            }
        });
    }
    else{
        $("#progressbar" ).progressbar({ value:35 });
        validateLoginShippingAddress();
    }
    
    } else {
        $('#confirmLoader').hide();
    }
}
function validateLoginShippingAddress(){
    var paymentAddressEnable="1";
    var shippingMethodEnable="<?php echo $settings['step']['shipping_method']['display_options']; ?>";
    var paymentMethodEnable="<?php echo $settings['step']['payment_method']['display_options']; ?>";
    if(paymentAddressEnable==1){
        if(!$('#shipping_use').is(":checked")){
            $.ajax({
                url: 'index.php?route=supercheckout/supercheckout/loginShippingAddressValidate',
                type: 'post',
                data: $('#checkoutShippingAddress input[type=\'text\'], #checkoutShippingAddress input[type=\'password\'], #checkoutShippingAddress input[type=\'checkbox\']:checked, #checkoutShippingAddress input[type=\'radio\']:checked, #checkoutShippingAddress select'),
                dataType: 'json',
                beforeSend: function() {
                    $('#button-shipping-address').attr('disabled', true);
                    $('#button-shipping-address').after('<span class="wait">&nbsp;<img src="catalog/view/theme/default/image/supercheckout/loading12.gif" alt="" /></span>');
                },
                complete: function() {
                    $('#button-shipping-address').attr('disabled', false);
                    $('.wait').remove();
                },
                success: function(json) {
                    $('.warning, .error').remove();

                    if (json['redirect']) {
                        location = json['redirect'];
                    } else if (json['error']) {
                        if (json['error']['warning']) {
                            $('#checkoutShippingAddress .supercheckout-checkout-content').prepend('<div class="warning" style="display: none;">' + json['error']['warning'] + '<img src="catalog/view/theme/default/image/close.png" alt="" class="close" /></div>');
                            $('#checkoutShippingAddress .supercheckout-checkout-content').show();
                            $('.warning').fadeIn('slow');
                        }
                        goToByScroll('checkoutShippingAddress');
                        if (json['error']['firstname']) {
                            $('#shipping-new input[name=\'firstname\']').after('<span class="errorsmall">' + json['error']['firstname'] + '</span>');
                        }

                        if (json['error']['lastname']) {
                            $('#shipping-new input[name=\'lastname\']').after('<span class="errorsmall">' + json['error']['lastname'] + '</span>');
                        }

                        if (json['error']['email']) {
                            $('#shipping-new input[name=\'email\']').after('<span class="errorsmall">' + json['error']['email'] + '</span>');
                        }

                        if (json['error']['telephone']) {
                            $('#shipping-new input[name=\'telephone\']').after('<span class="errorsmall">' + json['error']['telephone'] + '</span>');
                        }

                        if (json['error']['address_1']) {
                            $('#shipping-new input[name=\'address_1\']').after('<span class="errorsmall">' + json['error']['address_1'] + '</span>');
                        }
                        if (json['error']['address_2']) {
                            $('#shipping-new input[name=\'address_2\']').after('<span class="errorsmall">' + json['error']['address_2'] + '</span>');
                        }
                        if (json['error']['city']) {
                            $('#shipping-new input[name=\'city\']').after('<span class="errorsmall">' + json['error']['city'] + '</span>');
                        }

                        if (json['error']['postcode']) {
                            $('#shipping-new input[name=\'postcode\']').after('<span class="errorsmall">' + json['error']['postcode'] + '</span>');
                        }

                        if (json['error']['country']) {
                            $('#shipping-new select[name=\'country_id\']').after('<span class="errorsmall">' + json['error']['country'] + '</span>');
                        }

                        if (json['error']['zone']) {
                            $('#shipping-new select[name=\'zone_id\']').after('<span class="errorsmall">' + json['error']['zone'] + '</span>');
                        }
                    } else {
                        $("#progressbar" ).progressbar({ value:50 });
                        if(shippingMethodEnable==1){
                            validateShippingMethod();
                        }else{
                            if(paymentMethodEnable==1){
                                validatePaymentMethod();
                            }else{
                                var agreeRequire="<?php if($logged){ if($settings['option']['logged']['confirm']['fields']['agree']['require']){ echo'loginblock';} }else{ if($settings['option']['guest']['confirm']['fields']['agree']['require']){ echo'guestblock';}} ?>";
                                if(agreeRequire=='loginblock' || agreeRequire=='guestblock'){
                                    validateAgree();
                                }
                                else{
                                    goToConfirm();
                                }
                            }
                        }
                    }
                }
            });
        }else{
            if(shippingMethodEnable==1){
                validateShippingMethod();
            }else{
                if(paymentMethodEnable==1){
                    validatePaymentMethod();
                }else{
                    var agreeRequire="<?php if($logged){ if($settings['option']['logged']['confirm']['fields']['agree']['require']){ echo'loginblock';} }else{ if($settings['option']['guest']['confirm']['fields']['agree']['require']){ echo'guestblock';}} ?>";
                    if(agreeRequire=='loginblock' || agreeRequire=='guestblock'){
                        validateAgree();
                    }
                    else{
                        goToConfirm();
                    }
                }
            }
        }
    }
    else{
        $.ajax({
            url: 'index.php?route=supercheckout/supercheckout/loginShippingAddressValidate',
            type: 'post',
            data: $('#checkoutShippingAddress input[type=\'text\'], #checkoutShippingAddress input[type=\'password\'], #checkoutShippingAddress input[type=\'radio\']:checked, #checkoutShippingAddress select'),
            dataType: 'json',
            beforeSend: function() {
                $('#button-shipping-address').attr('disabled', true);
                $('#button-shipping-address').after('<span class="wait">&nbsp;<img src="catalog/view/theme/default/image/supercheckout/loading12.gif" alt="" /></span>');
            },
            complete: function() {
                $('#button-shipping-address').attr('disabled', false);
                $('.wait').remove();
            },
            success: function(json) {
                $('.warning, .error').remove();

                if (json['redirect']) {
                    location = json['redirect'];
                } else if (json['error']) {
                    if (json['error']['warning']) {
                        $('#checkoutShippingAddress .supercheckout-checkout-content').prepend('<div class="warning" style="display: none;">' + json['error']['warning'] + '<img src="catalog/view/theme/default/image/close.png" alt="" class="close" /></div>');
                        $('#checkoutShippingAddress .supercheckout-checkout-content').show();
                        $('.warning').fadeIn('slow');
                    }

                    if (json['error']['firstname']) {
                        $('#shipping-new input[name=\'firstname\']').after('<span class="errorsmall">' + json['error']['firstname'] + '</span>');
                    }

                    if (json['error']['lastname']) {
                        $('#shipping-new input[name=\'lastname\']').after('<span class="errorsmall">' + json['error']['lastname'] + '</span>');
                    }

                    if (json['error']['email']) {
                        $('#shipping-new input[name=\'email\']').after('<span class="errorsmall">' + json['error']['email'] + '</span>');
                    }

                    if (json['error']['telephone']) {
                        $('#shipping-new input[name=\'telephone\']').after('<span class="errorsmall">' + json['error']['telephone'] + '</span>');
                    }

                    if (json['error']['address_1']) {
                        $('#shipping-new input[name=\'address_1\']').after('<span class="errorsmall">' + json['error']['address_1'] + '</span>');
                    }
                    if (json['error']['address_2']) {
                        $('#shipping-new input[name=\'address_2\']').after('<span class="errorsmall">' + json['error']['address_2'] + '</span>');
                    }
                    if (json['error']['city']) {
                        $('#shipping-new input[name=\'city\']').after('<span class="errorsmall">' + json['error']['city'] + '</span>');
                    }

                    if (json['error']['postcode']) {
                        $('#shipping-new input[name=\'postcode\']').after('<span class="errorsmall">' + json['error']['postcode'] + '</span>');
                    }

                    if (json['error']['country']) {
                        $('#shipping-new select[name=\'country_id\']').after('<span class="errorsmall">' + json['error']['country'] + '</span>');
                    }

                    if (json['error']['zone']) {
                        $('#shipping-new select[name=\'zone_id\']').after('<span class="errorsmall">' + json['error']['zone'] + '</span>');
                    }
                } else {
                    $("#progressbar" ).progressbar({ value:50 });
                    if(shippingMethodEnable==1){
                        validateShippingMethod();
                    }else{
                        if(paymentMethodEnable==1){
                            validatePaymentMethod();
                        }else{
                            var agreeRequire="<?php if($logged){ if($settings['option']['logged']['confirm']['fields']['agree']['require']){ echo'loginblock';} }else{ if($settings['option']['guest']['confirm']['fields']['agree']['require']){ echo'guestblock';}} ?>";
                            if(agreeRequire=='loginblock' || agreeRequire=='guestblock'){
                                validateAgree();
                            }
                            else{
                                goToConfirm();
                            }
                        }
                    }
                }
            }
        });
    }
}
function validateShippingMethod(){
    var paymentMethodEnable="<?php echo $settings['step']['payment_method']['display_options']; ?>";
    $.ajax({
        url: 'index.php?route=supercheckout/shipping_method/validate',
        type: 'post',
        data: $('#shipping-method input[type=\'radio\']:checked'),
        dataType: 'json',
        beforeSend: function() {
            $('#button-shipping-method').attr('disabled', true);
            $('#button-shipping-method').after('<span class="wait">&nbsp;<img src="catalog/view/theme/default/image/supercheckout/loading12.gif" alt="" /></span>');
        },
        complete: function() {
            $('#button-shipping-method').attr('disabled', false);
            $('.wait').remove();
        },
        success: function(json) {
            $('.warning, .error').remove();

            if (json['redirect']) {
            //				location = json['redirect'];
            } else if (json['error']) {
                if (json['error']['warning']) {
                    $('#shipping-method .supercheckout-checkout-content').prepend('<div class="warning" style="display: none;">' + json['error']['warning'] + '<img src="catalog/view/theme/default/image/close.png" alt="" class="close" /></div>');
                    $('#shipping-method .supercheckout-checkout-content').show();
                    $('.warning').fadeIn('slow');
                }
            } else {
                $("#progressbar" ).progressbar({ value:65 });
                if(paymentMethodEnable==1){
                    validatePaymentMethod();
                }else{
                    var agreeRequire="<?php if($logged){ if($settings['option']['logged']['confirm']['fields']['agree']['require']){ echo'loginblock';} }else{ if($settings['option']['guest']['confirm']['fields']['agree']['require']){ echo'guestblock';}} ?>";
                    if(agreeRequire=='loginblock' || agreeRequire=='guestblock'){
                        validateAgree();
                    }
                    else{
                        goToConfirm();
                    }
                }
            }
        }
    });
}
function validatePaymentMethod(){
    $.ajax({
        url: 'index.php?route=supercheckout/payment_method/validate',
        type: 'post',
        data: $('#payment-method input[type=\'radio\']:checked, #payment-method input[type=\'checkbox\']:checked, #payment-method textarea,#payment_display_block input[type=\'checkbox\']'),
        dataType: 'json',
        beforeSend: function() {
            $('#button-payment-method').attr('disabled', true);
            $('#button-payment-method').after('<span class="wait">&nbsp;<img src="catalog/view/theme/default/image/supercheckout/loading12.gif" alt="" /></span>');
        },
        complete: function() {
            $('#button-payment-method').attr('disabled', false);
            $('.wait').remove();
        },
        success: function(json) {
            $('.warning, .error').remove();

            if (json['redirect']) {
                location = json['redirect'];
            } else if (json['error']) {
                if (json['error']['warning']) {
                    $('#payment-method .supercheckout-checkout-content').prepend('<div class="warning" style="display: none;">' + json['error']['warning'] + '<img src="catalog/view/theme/default/image/close.png" alt="" class="close" /></div>');
                    $('#payment-method .supercheckout-checkout-content').show();
                    $('.warning').fadeIn('slow');
                }else if(json['error']['warnings']){
                    $('#payment_display_block .supercheckout-checkout-content').prepend('<div class="warning" style="display: none;">' + json['error']['warnings'] + '<img src="catalog/view/theme/default/image/close.png" alt="" class="close" /></div>');
                    $('#payment_display_block .supercheckout-checkout-content').show();
                    $('.warning').fadeIn('slow');
                    goToByScroll('payment_display_block');
                }
            } else {
                $("#progressbar" ).progressbar({ value:80 });
                var agreeRequire="<?php if($logged){ if($settings['option']['logged']['confirm']['fields']['agree']['require']){ echo'loginblock';} }else{ if($settings['option']['guest']['confirm']['fields']['agree']['require']){ echo'guestblock';}} ?>";
                var comment="<?php if($logged){ if($settings['option']['logged']['confirm']['fields']['comment']['display']){ echo'loginblock';} }else{ if($settings['option']['guest']['confirm']['fields']['comment']['display']){ echo'guestblock';}} ?>";
                if(comment=='loginblock' || comment=='guestblock'){
                    setComment();
                }
                if(agreeRequire=='loginblock' || agreeRequire=='guestblock'){
                    validateAgree();

                }
                else{
                    goToConfirm();
                }
            }
        }
    });
}
function setComment(){
    $.ajax({
        url: 'index.php?route=supercheckout/supercheckout/setCommentSession',
        type: 'post',
        data: $('#supercheckout-comment_order'),
        dataType: 'json',
        success: function(json) {

        }
    });
}
function validateAgree(){
    $.ajax({
        url: 'index.php?route=supercheckout/supercheckout/validateAgree',
        type: 'post',
        data: $('#payment_display_block input[type=\'checkbox\']:checked'),
        dataType: 'json',
        success: function(json) {
            $('.warning, .error').remove();

            if (json['error']) {
                if (json['error']['warning']) {
                    $('#payment_display_block .supercheckout-checkout-content').prepend('<div class="warning" style="display: none;">' + json['error']['warning'] + '<img src="catalog/view/theme/default/image/close.png" alt="" class="close" /></div>');
                    $('#payment_display_block .supercheckout-checkout-content').show();
                    $('.warning').fadeIn('slow');
                    goToByScroll('payment_display_block');
                }
            } else {
                goToConfirm();
            }
        }
    });

}
function createGuestAccount(){
    $.ajax({
        url: 'index.php?route=supercheckout/supercheckout/createGuestAccount',
        type:'post',
        data: $('#checkoutLogin input[type=\'text\']'),
        success: function(html) {            
            $("#progressbar" ).progressbar({ value:60 });
        },
        error: function(xhr, ajaxOptions, thrownError) {
            console.log(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
        }
    });
}
function goToConfirm(){
    var tools = 1;
    
    if($("#tools").attr("checked")){
	tools = 0;
    } else {
	tools = 1;
    }
    
    $.ajax({
        url: 'index.php?route=supercheckout/confirm&tools='+tools,
        dataType: 'html',
        success: function(html) {
            $("#progressbar" ).progressbar({ value:100 });
            var href = $("#display_payment .button, #display_payment .btn, #display_payment .button_oc, #display_payment input[type=submit]").attr('href');
            if(href != '' && href != undefined) {
                document.location.href = href;
                console.log('clicked')	
            }else{
                $("#display_payment .button, #display_payment .btn, #display_payment .button_oc, #display_payment input[type=submit]").trigger("click", function(){	
                console.log('clicked')	
            })
            }
//            $("#button-confirm , input[type=submit] , .display_payment .button, #display_payment .button").trigger("click");
        //            $("#button-confirm , input[type=submit]").unbind("click");
        },
        error: function(xhr, ajaxOptions, thrownError) {
            console.log(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
        }
    });

}
function validateEmail(){
    $.ajax({
        url: 'index.php?route=supercheckout/supercheckout/validateEmail',
        type: 'post',
        data: $('#checkoutLogin input[type=\'text\']'),
        dataType: 'json',
        success: function(json) {
            $('.warning, .error').remove();
            if (json['error']) {
                if (json['error']['warning']) {
                    $('#checkoutLogin .supercheckout-checkout-content').prepend('<div class="warning" style="display: none;">' + json['error']['warning'] + '</div>');
                    $('#checkoutLogin .supercheckout-checkout-content').show();
                    $('.warning').fadeIn('slow');
                    goToByScroll('checkoutLogin');
                }
            }
            else {
                $("#progressbar" ).progressbar({ value: 20 });
                validateGuestPaymentAddress();
            }

        }
    });
}
function validateEmailAndPassword(){
    $.ajax({
        url: 'index.php?route=supercheckout/supercheckout/validateEmailAndPassword',
        type: 'post',
        data: $('#checkoutLogin input[type=\'text\'],#checkoutLogin input[type=\'password\']'),
        dataType: 'json',
        success: function(json) {
            $('.warning, .error').remove();
            if (json['error']) {
                if (json['error']['warning']) {
                    $('#checkoutLogin .supercheckout-checkout-content').prepend('<div class="warning" style="display: none;">' + json['error']['warning'] + '</div>');
                    $('#checkoutLogin .supercheckout-checkout-content').show();
                    $('.warning').fadeIn('slow');
                    $('.panel-primary').find('#collapseTwo1').removeClass('collapse').addClass('collapsing');
                    $('.panel-primary').find('#collapseTwo1').removeClass('collapsing').addClass('in');
                    goToByScroll('checkoutLogin');
                }
                if(json['error']['mismatch']){ 
                    $('#checkoutLogin .supercheckout-checkout-content').prepend('<div class="warning" style="display: none;">' + json['error']['mismatch'] + '</div>');
                    $('#checkoutLogin .supercheckout-checkout-content').show();
                    $('.warning').fadeIn('slow');
                    $('.panel-primary').find('#collapseTwo1').removeClass('collapse').addClass('collapsing');
                    $('.panel-primary').find('#collapseTwo1').removeClass('collapsing').addClass('in');
                    goToByScroll('checkoutLogin');
                }
                if(json['error']['password']){ 
                    $('#checkoutLogin .supercheckout-checkout-content').prepend('<div class="warning" style="display: none;">' + json['error']['password'] + '</div>');
                    $('#checkoutLogin .supercheckout-checkout-content').show();
                    $('.warning').fadeIn('slow');
                    $('.panel-primary').find('#collapseTwo1').removeClass('collapse').addClass('collapsing');
                    $('.panel-primary').find('#collapseTwo1').removeClass('collapsing').addClass('in');
                    goToByScroll('checkoutLogin');
                }
            }else {
                $("#progressbar" ).progressbar({ value: 20 });
                validateGuestPaymentAddress();
            }

        }
    });
}
function matchPassword(){
    $.ajax({
        url: 'index.php?route=supercheckout/supercheckout/validateEmailAndPassword',
        type: 'post',
        data: $('#checkoutLogin input[type=\'text\'],#checkoutLogin input[type=\'password\']'),
        dataType: 'json',
        success: function(json) {
            $('.warning, .error').remove();
            if (json['error']) {
                if (json['error']['warning']) {
                    $('#checkoutLogin .supercheckout-checkout-content').prepend('<div class="warning" style="display: none;">' + json['error']['warning'] + '</div>');
                    $('#checkoutLogin .supercheckout-checkout-content').show();
                    $('.warning').fadeIn('slow');
                    $('.panel-primary').find('#collapseTwo1').removeClass('collapse').addClass('collapsing');
                    $('.panel-primary').find('#collapseTwo1').removeClass('collapsing').addClass('in');
                    goToByScroll('checkoutLogin ');
                }
                if(json['error']['mismatch']){ 
                    $('#checkoutLogin .supercheckout-checkout-content').prepend('<div class="warning" style="display: none;">' + json['error']['mismatch'] + '</div>');
                    $('#checkoutLogin .supercheckout-checkout-content').show();
                    $('.warning').fadeIn('slow');
                    $('.panel-primary').find('#collapseTwo1').removeClass('collapse').addClass('collapsing');
                    $('.panel-primary').find('#collapseTwo1').removeClass('collapsing').addClass('in');
                    goToByScroll('checkoutLogin ');
                }
                if(json['error']['password']){ 
                    $('#checkoutLogin .supercheckout-checkout-content').prepend('<div class="warning" style="display: none;">' + json['error']['password'] + '</div>');
                    $('#checkoutLogin .supercheckout-checkout-content').show();
                    $('.warning').fadeIn('slow');
                    $('.panel-primary').find('#collapseTwo1').removeClass('collapse').addClass('collapsing');
                    $('.panel-primary').find('#collapseTwo1').removeClass('collapsing').addClass('in');
                    goToByScroll('checkoutLogin ');
                }
            }
        }
    });
}
function validateGuestPaymentAddress(){
    var paymentGuestAddressEnable="1";
    var useforShippingEnable="<?php echo $settings['option']['guest']['payment_address']['fields']['shipping']['display']; ?>";
    
    
    if(paymentGuestAddressEnable==1){
        if(useforShippingEnable==1){
            $.ajax({
                url: 'index.php?route=supercheckout/supercheckout/guestPaymentAddressValidate',
                type: 'post',
                data: $('#payment-new  input[type=\'text\'], #checkoutBillingAddress input[type=\'checkbox\']:checked, #payment-new select, #checkoutLogin input[name=\'email\'] '),
                dataType: 'json',
                beforeSend: function() {
                    $('#button-guest').attr('disabled', true);
                    $('#button-guest').after('<span class="wait">&nbsp;<img src="catalog/view/theme/default/image/supercheckout/loading12.gif" alt="" /></span>');
                },
                complete: function() {
                    $('#button-guest').attr('disabled', false);
                    $('.wait').remove();
                },
                success: function(json) {
                    $('.warning, .errorsmall').remove();

                    if (json['redirect']) {
                        location = json['redirect'];
                    }
                    else if (json['error']) {
                        if (json['error']['warning']) {
                            $('#payment-new .supercheckout-checkout-content').prepend('<div class="warning" style="display: none;">' + json['error']['warning'] + '<img src="catalog/view/theme/default/image/close.png" alt="" class="close" /></div>');

                            $('.warning').fadeIn('slow');
                        }

                        if (json['error']['firstname']) {
                            $('#payment-new input[name=\'firstname\']').after('<span class="errorsmall">' + json['error']['firstname'] + '</span>');
                        }

                        if (json['error']['lastname']) {
                            $('#payment-new input[name=\'lastname\']').after('<span class="errorsmall">' + json['error']['lastname'] + '</span>');
                        }

                        if (json['error']['email']) {
                            $('#payment-new input[name=\'email\']').after('<span class="errorsmall">' + json['error']['email'] + '</span>');
                        }

                        if (json['error']['telephone']) {
                            $('#payment-new input[name=\'telephone\']').after('<span class="errorsmall">' + json['error']['telephone'] + '</span>');
                        }
                        if (json['error']['company']) {
                            $('#payment-new input[name=\'company\']').after('<span class="errorsmall">' + json['error']['company'] + '</span>');
                        }
                        if (json['error']['company_id']) {
                            $('#payment-new input[name=\'company_id\']').after('<span class="errorsmall">' + json['error']['company_id'] + '</span>');
                        }

                        if (json['error']['tax_id']) {
                            $('#payment-new input[name=\'tax_id\']').after('<span class="errorsmall">' + json['error']['tax_id'] + '</span>');
                        }

                        if (json['error']['address_1']) {
                            $('#payment-new input[name=\'address_1\']').after('<span class="errorsmall">' + json['error']['address_1'] + '</span>');
                        }
                        if (json['error']['address_2']) {
                            $('#payment-new input[name=\'address_2\']').after('<span class="errorsmall">' + json['error']['address_2'] + '</span>');
                        }
                        if (json['error']['city']) {
                            $('#payment-new input[name=\'city\']').after('<span class="errorsmall">' + json['error']['city'] + '</span>');
                        }

                        if (json['error']['postcode']) {
                            $('#payment-new input[name=\'postcode\']').after('<span class="errorsmall">' + json['error']['postcode'] + '</span>');
                        }

                        if (json['error']['country']) {
                            $('#payment-new select[name=\'country_id\']').after('<span class="errorsmall">' + json['error']['country'] + '</span>');
                        }

                        if (json['error']['zone']) {
                            $('#payment-new select[name=\'zone_id\']').after('<span class="errorsmall">' + json['error']['zone'] + '</span>');
                        }
                        goToByScroll('checkoutBillingAddress');
                    }
                    else {
                        $("#progressbar" ).progressbar({ value: 35 });
                        checkGuestShippingAddress();
                    }
                }
            });
        }
        else{
            $.ajax({
                url: 'index.php?route=supercheckout/supercheckout/guestPaymentAddressValidate',
                type: 'post',
                data: $('#payment-new  input[type=\'text\'],  #payment-new select , #checkoutLogin input[name=\'email\']'),
                dataType: 'json',
                beforeSend: function() {
                    $('#button-guest').attr('disabled', true);
                    $('#button-guest').after('<span class="wait">&nbsp;<img src="catalog/view/theme/default/image/supercheckout/loading12.gif" alt="" /></span>');
                },
                complete: function() {
                    $('#button-guest').attr('disabled', false);
                    $('.wait').remove();
                },
                success: function(json) {
                    $('.warning, .errorsmall').remove();

                    if (json['redirect']) {
                        location = json['redirect'];
                    }
                    else if (json['error']) {
                        if (json['error']['warning']) {
                            $('#payment-new .supercheckout-checkout-content').prepend('<div class="warning" style="display: none;">' + json['error']['warning'] + '<img src="catalog/view/theme/default/image/close.png" alt="" class="close" /></div>');
                            $('.warning').fadeIn('slow');
                        }

                        if (json['error']['firstname']) {
                            $('#payment-new input[name=\'firstname\']').after('<span class="errorsmall">' + json['error']['firstname'] + '</span>');
                        }

                        if (json['error']['lastname']) {
                            $('#payment-new input[name=\'lastname\']').after('<span class="errorsmall">' + json['error']['lastname'] + '</span>');
                        }

                        if (json['error']['email']) {
                            $('#payment-new input[name=\'email\']').after('<span class="errorsmall">' + json['error']['email'] + '</span>');
                        }

                        if (json['error']['telephone']) {
                            $('#payment-new input[name=\'telephone\']').after('<span class="errorsmall">' + json['error']['telephone'] + '</span>');
                        }

                        if (json['error']['company_id']) {
                            $('#payment-new input[name=\'company_id\'] + br').after('<span class="errorsmall">' + json['error']['company_id'] + '</span>');
                        }

                        if (json['error']['tax_id']) {
                            $('#payment-new input[name=\'tax_id\']').after('<span class="errorsmall">' + json['error']['tax_id'] + '</span>');
                        }

                        if (json['error']['address_1']) {
                            $('#payment-new input[name=\'address_1\']').after('<span class="errorsmall">' + json['error']['address_1'] + '</span>');
                        }
                        if (json['error']['address_2']) {
                            $('#payment-new input[name=\'address_2\']').after('<span class="errorsmall">' + json['error']['address_2'] + '</span>');
                        }
                        if (json['error']['city']) {
                            $('#payment-new input[name=\'city\']').after('<span class="errorsmall">' + json['error']['city'] + '</span>');
                        }

                        if (json['error']['postcode']) {
                            $('#payment-new input[name=\'postcode\']').after('<span class="errorsmall">' + json['error']['postcode'] + '</span>');
                        }

                        if (json['error']['country']) {
                            $('#payment-new select[name=\'country_id\']').after('<span class="errorsmall">' + json['error']['country'] + '</span>');
                        }

                        if (json['error']['zone']) {
                            $('#payment-new select[name=\'zone_id\']').after('<span class="errorsmall">' + json['error']['zone'] + '</span>');
                        }
                        goToByScroll('checkoutBillingAddress');
                    }
                    else {
                        $("#progressbar" ).progressbar({ value: 35 });
                        checkGuestShippingAddress();
                    }
                }
            });
        }
    }else{

        validateGuestShippingAddress();
    }
    
}
function validateGuestShippingAddress(){
    var paymentMethodEnable="<?php echo $settings['step']['payment_method']['display_options']; ?>";
    var shippingMethodEnable="<?php echo $settings['step']['shipping_method']['display_options']; ?>";
    $.ajax({
        url: 'index.php?route=supercheckout/supercheckout/guestShippingAddressValidate',
        type: 'post',
        data: $('#shipping-new input[type=\'text\'], #shipping-new select'),
        dataType: 'json',
        beforeSend: function() {
            $('#button-guest-shipping').attr('disabled', true);
            $('#button-guest-shipping').after('<span class="wait">&nbsp;<img src="catalog/view/theme/default/image/supercheckout/loading12.gif" alt="" /></span>');
        },
        complete: function() {
            $('#button-guest-shipping').attr('disabled', false);
            $('.wait').remove();
        },
        success: function(json) {
            $('.warning, .errorsmall').remove();

            if (json['redirect']) {
                location = json['redirect'];
            } else if (json['error']) {
                if (json['error']['warning']) {
                    $('#shipping-new .supercheckout-checkout-content').prepend('<div class="warning" style="display: none;">' + json['error']['warning'] + '<img src="catalog/view/theme/default/image/close.png" alt="" class="close" /></div>');

                    $('.warning').fadeIn('slow');
                }

                if (json['error']['firstname']) {
                    $('#shipping-new input[name=\'firstname\']').after('<span class="errorsmall">' + json['error']['firstname'] + '</span>');
                }

                if (json['error']['lastname']) {
                    $('#shipping-new input[name=\'lastname\']').after('<span class="errorsmall">' + json['error']['lastname'] + '</span>');
                }

                if (json['error']['address_1']) {
                    $('#shipping-new input[name=\'address_1\']').after('<span class="errorsmall">' + json['error']['address_1'] + '</span>');
                }
                if (json['error']['address_2']) {
                    $('#shipping-new input[name=\'address_2\']').after('<span class="errorsmall">' + json['error']['address_2'] + '</span>');
                }
                if (json['error']['city']) {
                    $('#shipping-new input[name=\'city\']').after('<span class="errorsmall">' + json['error']['city'] + '</span>');
                }

                if (json['error']['postcode']) {
                    $('#shipping-new input[name=\'postcode\']').after('<span class="errorsmall">' + json['error']['postcode'] + '</span>');
                }

                if (json['error']['country']) {
                    $('#shipping-new select[name=\'country_id\']').after('<span class="errorsmall">' + json['error']['country'] + '</span>');
                }

                if (json['error']['zone']) {
                    $('#shipping-new select[name=\'zone_id\']').after('<span class="errorsmall">' + json['error']['zone'] + '</span>');
                }
                goToByScroll('checkoutShippingAddress');
            } else {
                $("#progressbar" ).progressbar({ value:50 });
                var checkGuestRegisterEnable="<?php echo $settings['general']['guestenable']; ?>";
                if(checkGuestRegisterEnable==1){
                    $.ajax({
                        url: 'index.php?route=supercheckout/supercheckout/createGuestAccount',
                        type:'post',
                        data: $('#checkoutLogin input[type=\'text\']'),
                        success: function(html) {            
                            $("#progressbar" ).progressbar({ value:60 });
                            if(shippingMethodEnable==1){
                                validateShippingMethod();
                            }else{
                                if(paymentMethodEnable==1){
                                    validatePaymentMethod();
                                }else{
                                    var agreeRequire="<?php if($logged){ if($settings['option']['logged']['confirm']['fields']['agree']['require']){ echo'loginblock';} }else{ if($settings['option']['guest']['confirm']['fields']['agree']['require']){ echo'guestblock';}} ?>";
                                    if(agreeRequire=='loginblock' || agreeRequire=='guestblock'){
                                        validateAgree();
                                    }
                                    else{
                                        goToConfirm();
                                    }
                                }
                            }
                        },
                        error: function(xhr, ajaxOptions, thrownError) {
                            console.log(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                        }
                    });
                }else{
                    if(shippingMethodEnable==1){
                        validateShippingMethod();
                    }else{
                        if(paymentMethodEnable==1){
                            validatePaymentMethod();
                        }else{
                            var agreeRequire="<?php if($logged){ if($settings['option']['logged']['confirm']['fields']['agree']['require']){ echo'loginblock';} }else{ if($settings['option']['guest']['confirm']['fields']['agree']['require']){ echo'guestblock';}} ?>";
                            if(agreeRequire=='loginblock' || agreeRequire=='guestblock'){
                                validateAgree();
                            }
                            else{
                                goToConfirm();
                            }
                        }
                    }
                }
                
            }
        }
    });
}
function checkGuestShippingAddress(){
    var ShippingUseEnable="<?php echo $settings['option']['guest']['payment_address']['fields']['shipping']['display']; ?>"
    var paymentGuestAddressEnable="1";
    var paymentMethodEnable="<?php echo $settings['step']['payment_method']['display_options']; ?>";
    var shippingMethodEnable="<?php echo $settings['step']['shipping_method']['display_options']; ?>";
    if(ShippingUseEnable==1)
    {
        if($('#shipping_use').is(":checked")){
            var checkGuestRegisterEnable="<?php echo $settings['general']['guestenable']; ?>";
            if(checkGuestRegisterEnable==1){
                $.ajax({
                    url: 'index.php?route=supercheckout/supercheckout/createGuestAccount',
                    type:'post',
                    data: $('#checkoutLogin input[type=\'text\']'),
                    success: function(html) {            
                        $("#progressbar" ).progressbar({ value:60 });
                        if(shippingMethodEnable==1){
                            validateShippingMethod();
                        }else{
                            if(paymentMethodEnable==1){
                                validatePaymentMethod();
                            }else{
                                var agreeRequire="<?php if($logged){ if($settings['option']['logged']['confirm']['fields']['agree']['require']){ echo'loginblock';} }else{ if($settings['option']['guest']['confirm']['fields']['agree']['require']){ echo'guestblock';}} ?>";
                                if(agreeRequire=='loginblock' || agreeRequire=='guestblock'){
                                    validateAgree();
                                }
                                else{
                                    goToConfirm();
                                }
                            }
                        }
                    },
                    error: function(xhr, ajaxOptions, thrownError) {
                        console.log(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                    }
                });
            }else{
                if(shippingMethodEnable==1){
                    validateShippingMethod();
                }else{
                    if(paymentMethodEnable==1){
                        validatePaymentMethod();
                    }else{
                        var agreeRequire="<?php if($logged){ if($settings['option']['logged']['confirm']['fields']['agree']['require']){ echo'loginblock';} }else{ if($settings['option']['guest']['confirm']['fields']['agree']['require']){ echo'guestblock';}} ?>";
                        if(agreeRequire=='loginblock' || agreeRequire=='guestblock'){
                            validateAgree();
                        }
                        else{
                            goToConfirm();
                        }
                    }
                }
            }
            
        }
        else{
            validateGuestShippingAddress();
        }
    }else{
        validateGuestShippingAddress()
    }

}

function validateCheckout(){
    var guestEnable="<?php echo $settings['step']['login']['option']['guest']['display']; ?>";
    var loggedIn="<?php echo $logged; ?>";//271
    if(guestEnable==0)
    {
        if(loggedIn==''){
            $('#checkoutLogin .supercheckout-checkout-content').html('<div class="warning" style="display: none;">' + 'Please Login' + '</div>');
            $('#checkoutLogin .supercheckout-checkout-content').show();
            $('.warning').fadeIn('slow');
            goToByScroll('checkoutLogin');
        } else {
            validatePaymentAddress();
        }
    } else {
        var loginRadio=$("input:radio[name=account]:checked").val();
        if(loginRadio=="register") {
            if(loggedIn=='') {
                $('#checkoutLogin .supercheckout-checkout-content').html('<div class="warning" style="display: none;">' + 'Please Login' + '</div>');
                $('#checkoutLogin .supercheckout-checkout-content').show();
                $('.warning').fadeIn('slow');
                $('html, body').animate({
                    scrollTop: 0
                }, 'slow');
            }
        }
        else if(loginRadio == "guest") {
            validateEmail();
        }
        if(loggedIn) {
            validatePaymentAddress();
        }
    }

}
//FOR REFRESSHING STEPS TO GET ORDER DETAILS
function validatePaymentAddressRefresh(){

    var paymentAddressEnable="1";
    if(paymentAddressEnable==1){
        $.ajax({
            url: 'index.php?route=supercheckout/supercheckout/setValueForLoginPayment',
            type: 'post',
            dataType:'json',
            data: $('#checkoutBillingAddress input[type=\'text\'], #checkoutBillingAddress input[type=\'password\'], #checkoutBillingAddress input[type=\'checkbox\']:checked, #checkoutBillingAddress input[type=\'radio\']:checked, #checkoutBillingAddress input[type=\'hidden\'], #checkoutBillingAddress select, #checkoutBillingAddress input[type=\'checkbox\']:checked'),
            success: function(json) {
                    validateLoginShippingAddressRefresh();
            }
        });
    }
    else{
        validateLoginShippingAddressRefresh();
    }
}
function validateLoginShippingAddressRefresh(){
    var paymentAddressEnable="1";
    if(paymentAddressEnable==1){
        if(!$('#shipping_use').is(":checked")){
            $.ajax({
                url: 'index.php?route=supercheckout/supercheckout/setValueForLoginShipping',
                type: 'post',
                data: $('#checkoutShippingAddress input[type=\'text\'], #checkoutShippingAddress input[type=\'password\'], #checkoutShippingAddress input[type=\'checkbox\']:checked, #checkoutShippingAddress input[type=\'radio\']:checked, #checkoutShippingAddress select'),
                success: function(json) {
                    $('.warning, .error').remove();

                    {
                        refreshAll();
                    }
                }
            });
        }else{
            refreshAll();
        }
    }
    else{
        $.ajax({
            url: 'index.php?route=supercheckout/supercheckout/setValueForLoginShipping',
            type: 'post',
            data: $('#checkoutShippingAddress input[type=\'text\'], #checkoutShippingAddress input[type=\'password\'], #checkoutShippingAddress input[type=\'radio\']:checked, #checkoutShippingAddress select'),
            success: function(json) {
                refreshAll();
            }
        });
    }
}
function validateShippingMethodRefresh(){
    $.ajax({
        url: 'index.php?route=supercheckout/shipping_method/validate',
        type: 'post',
        data: $('#shipping-method input[type=\'radio\']:checked'),
        dataType: 'json',
        success: function(json) {
            $('.warning, .error').remove();

            if (json['redirect']) {
            } else if (json['error']) {
                if (json['error']['warning']) {
                    $('#shipping-method .supercheckout-checkout-content').prepend('<div class="warning" style="display: none;">' + json['error']['warning'] + '<img src="catalog/view/theme/default/image/close.png" alt="" class="close" /></div>');
                    $('#shipping-method .supercheckout-checkout-content').show();
                    $('.warning').fadeIn('slow');
                }
            } else {
                validatePaymentMethodRefresh();
            }
        }
    });
}
function validatePaymentMethodRefresh(){
    $.ajax({
        url: 'index.php?route=supercheckout/payment_method/validate',
        type: 'post',
        data: $('#payment-method input[type=\'radio\']:checked, #payment-method input[type=\'checkbox\']:checked, #payment-method textarea,#payment_display_block input[type=\'checkbox\']'),
        dataType: 'json',
        success: function(json) {
            $('.warning, .error').remove();

            if (json['redirect']) {
                location = json['redirect'];
            } else if (json['error']) {
                if (json['error']['warning']) {
                    $('#payment-method .supercheckout-checkout-content').prepend('<div class="warning" style="display: none;">' + json['error']['warning'] + '<img src="catalog/view/theme/default/image/close.png" alt="" class="close" /></div>');
                    $('#payment-method .supercheckout-checkout-content').show();
                    $('.warning').fadeIn('slow');
                }else if(json['error']['warnings']){
                    $('#payment_display_block .supercheckout-checkout-content').prepend('<div class="warning" style="display: none;">' + json['error']['warnings'] + '<img src="catalog/view/theme/default/image/close.png" alt="" class="close" /></div>');
                    $('#payment_display_block .supercheckout-checkout-content').show();
                    $('.warning').fadeIn('slow');
                    goToByScroll('payment_display_block');
                }
            } else {
                goToConfirmRefresh();
            }
        }
    });
}

function goToConfirmRefresh(){
    $.ajax({
        url: 'index.php?route=supercheckout/confirm',
        dataType: 'html',
        success: function(html) {
            //$('#confirmLoader').hide();
            $('#confirmCheckout').html(html);
            $('#paymentDisable').html("");
            $.ajax({
                url: 'index.php?route=supercheckout/payment_display',
                dataType: 'html',
                success: function(html) {
                    $('#display_payment').html(html);
                },
                error: function(xhr, ajaxOptions, thrownError) {
                    console.log(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                }
            });
            if(window.callconfirm==true){
                        validateCheckout();
                        window.callconfirm=false;
                        window.confirmclick=false;
            }
            window.callconfirm=false;
            window.confirmclick=false;
        },
        error: function(xhr, ajaxOptions, thrownError) {
            console.log(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
        }
    });

}
function validateGuestPaymentAddressRefresh(){
    var paymentGuestAddressEnable="1";
    var useforShippingEnable="<?php echo $settings['option']['guest']['payment_address']['fields']['shipping']['display']; ?>";
    if(paymentGuestAddressEnable==1){
        if(useforShippingEnable==1){
            $.ajax({
                url: 'index.php?route=supercheckout/supercheckout/setValueForGuestPayment',
                type: 'post',
                data: $('#payment-new  input[type=\'text\'], #checkoutBillingAddress input[type=\'checkbox\']:checked, #payment-new select, #checkoutLogin input[name=\'email\'] '),
                success: function(json) {
                    $('.warning, .errorsmall').remove();

                    {
                        checkGuestShippingAddressRefresh();
                    }
                }
            });
        }
        else{
            $.ajax({
                url: 'index.php?route=supercheckout/supercheckout/setValueForGuestPayment',
                type: 'post',
                data: $('#payment-new  input[type=\'text\'],  #payment-new select'),
                success: function(json) {
                {
                    checkGuestShippingAddressRefresh();
                }
                }
            });
        }
    }else{

        validateGuestShippingAddressRefresh();
    }
}
function validateGuestShippingAddressRefresh(){
    $.ajax({
        url: 'index.php?route=supercheckout/supercheckout/setValueForGuestShipping',
        type: 'post',
        data: $('#shipping-new input[type=\'text\'], #shipping-new select'),
        success: function() {
            $('.warning, .errorsmall').remove();
            refreshAll();

        }
    });
}
function checkGuestShippingAddressRefresh(){
    var ShippingUseEnable="<?php echo $settings['option']['guest']['payment_address']['fields']['shipping']['display']; ?>"
    var paymentGuestAddressEnable="1";

    if(ShippingUseEnable==1)
    {
        if($('#shipping_use').is(":checked")){

            refreshAll();
        }
        else{
            validateGuestShippingAddressRefresh();
        }
    }else{
        validateGuestShippingAddressRefresh()
    }

}


function validateCheckoutRefresh(){
    var guestEnable="<?php echo $settings['step']['login']['option']['guest']['display']; ?>";
    var loggedIn="<?php echo $logged; ?>";//271
    if(guestEnable==0)
    {
        validatePaymentAddressRefresh();
    }
//    if(loginEnable==0 && guestEnable==1)
//    {
//        validateGuestPaymentAddressRefresh();
//    }
    else
    {
        if(loggedIn!=""){
            validatePaymentAddressRefresh();
        }else{
            validateGuestPaymentAddressRefresh();
        }
    }
//    if(loginEnable==0 && guestEnable==0)
//    {
//            validatePaymentAddressRefresh();
//    }
}

</script>
<script type="text/javascript">
    $('#checkoutShippingAddress input[name=\'shipping_address\']').live('change', function() {
        if (this.value == 'new') {
            $('#shipping-existing').hide();
            $('#shipping-new').show();
            validateCheckoutRefresh();
        } else {
            $('#shipping-existing').show();
            $('#shipping-new').hide();
            validateCheckoutRefresh();
        }
    });
</script>
<script type="text/javascript">
// GENERAL SETTING FOR HIDING DISPLAYING NEW & EXISTING SHIPPING ADDRESS
$('#shipping-existing select[name=\'address_id\']').bind('change', function() {
        validateCheckoutRefresh();
    });
    $('#payment-existing select[name=\'address_id\']').bind('change', function() {
        validateCheckoutRefresh();
    });
    $('#shipping-new select[name=\'country_id\']').bind('change', function() {
        if (this.value == '') return;

        $.ajax({
            url: 'index.php?route=supercheckout/supercheckout/country&country_id=' + this.value,
            dataType: 'json',
            beforeSend: function() {
                $('#shipping-new select[name=\'country_id\']').after('<span class="wait">&nbsp;<img src="catalog/view/theme/default/image/supercheckout/loading12.gif" alt="" /></span>');
            },
            complete: function() {
                $('.wait').remove();
            },
            success: function(json) {
                if (json['postcode_required'] == '1') {                
                    $('#shipping-postcode-required').show();
                } else {                
                    $('#shipping-postcode-required').hide();
                }

                html = '<option value=""><?php echo $text_select; ?></option>';

                if (json['zone'] != '') {
                    for (i = 0; i < json['zone'].length; i++) {
                        html += '<option value="' + json['zone'][i]['zone_id'] + '"';

                        if (json['zone'][i]['zone_id'] == '<?php echo $zone_id; ?>') {
                            html += ' selected="selected"';
                        }

                        html += '>' + json['zone'][i]['name'] + '</option>';
                    }
                } else {
                    html += '<option value="0" selected="selected"><?php echo $text_none; ?></option>';
                }

                $('#shipping-new select[name=\'zone_id\']').html(html);
                validateCheckoutRefresh();


            },
            error: function(xhr, ajaxOptions, thrownError) {
                console.log(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
        });
    });
</script>
<script type="text/javascript">
// GENERAL SETTING FOR HIDING DISPLAYING NEW & EXISTING BILLING ADDRESS
$('#checkoutBillingAddress input[name=\'payment_address\']').live('change', function() {
    if (this.value == 'new') {
        /*$('#payment-existing').hide();
        $('#payment-new').show();*/
        validateCheckoutRefresh();
    } else {
       /* $('#payment-existing').show();
        $('#payment-new').hide();*/
        validateCheckoutRefresh();
    }
});
$('#payment-new select[name=\'zone_id\']').bind('change', function() {
    validateCheckoutRefresh();
});
$('#shipping-new select[name=\'zone_id\']').bind('change', function() {
    validateCheckoutRefresh();
});
$('#payment-new select[name=\'country_id\']').bind('change', function() {
    if (this.value == '') return;
    $.ajax({
        url: 'index.php?route=supercheckout/supercheckout/country&country_id=' + this.value,
        dataType: 'json',
        beforeSend: function() {
            $('#payment-new select[name=\'country_id\']').after('<span class="wait">&nbsp;<img src="catalog/view/theme/default/image/supercheckout/loading12.gif" alt="" /></span>');
        },
        complete: function() {
            $('.wait').remove();
        },
        success: function(json) {
            if (json['postcode_required'] == '1') {
                $('#payment-postcode-required').show();
            } else {
                $('#payment-postcode-required').hide();
            }

            html = '<option value=""><?php echo $text_select; ?></option>';
            if (json['zone'] != '') {
                for (i = 0; i < json['zone'].length; i++) {
                    html += '<option value="' + json['zone'][i]['zone_id'] + '"';

//                    if (json['zone'][i]['zone_id'] == '<?php echo $zone_id; ?>') {
//                        html += ' selected="selected"';
//                    }

                    html += '>' + json['zone'][i]['name'] + '</option>';
                }
            } else {
                html += '<option value="0" selected="selected"><?php echo $text_none; ?></option>';
            }
            validateCheckoutRefresh();
            $('#payment-new select[name=\'zone_id\']').html(html);
        },
        error: function(xhr, ajaxOptions, thrownError) {
            console.log(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
        }
    });
});

//ON CHECKING - UNCHECKING USE FOR SHIPPING CHECKBOX
    $(document).ready(function(){
var shipping_display="<?php  echo $settings['option']['guest']['payment_address']['fields']['shipping']['display'] ?>";
var loggedIn="<?php echo $logged; ?>";//271
if(shipping_display=="0" && loggedIn==""){
    $('#checkoutShippingAddress').slideDown('slow');
}
        $("#shipping_use").change(function(){
            if($(this).is(":checked"))
            {
                $('#checkoutShippingAddress').slideUp('slow');
                validateCheckoutRefresh();
            }
            else{
                $('#checkoutShippingAddress').slideDown('slow');
                validateCheckoutRefresh();
            }
        });
    });

// FOR GENERAL SETTINGS LIKE SLIDE UP AND DOWN, AND LOGIN VALIDATION
var generalDefault="<?php echo $settings['general']['default_option']; ?>";
var guestEnable="<?php echo $settings['step']['login']['option']['guest']['display']; ?>";
var current = $("input:radio[name=account]:checked").val();
if(generalDefault=='guest'&& guestEnable==1 && current=='guest'){
    $('#supercheckout-login').slideUp('fast');
}else{
    $('#supercheckout-login').slideDown('fast');
}

$("input:radio[name=account]").click(function() {
    var value = $("input:radio[name=account]:checked").val();
    if(value=='register')
    {
        $('#supercheckout-login').slideDown('fast');
    }
    else{
        $('#supercheckout-login').slideUp('fast');
    }
});
//login
$('#button-login').live('click', function() {
    $.ajax({
        url: 'index.php?route=supercheckout/supercheckout/loginValidate',
        type: 'post',
        data: $('#checkoutLogin :input'),
        dataType: 'json',
        beforeSend: function() {
            $('#button-login').attr('disabled', true);
            $('#button-login').after('<span class="wait">&nbsp;<img src="catalog/view/theme/default/image/supercheckout/loading12.gif" alt="" /></span>');
        },
        complete: function() {
            $('#button-login').attr('disabled', false);
            $('.wait').remove();
        },
        success: function(json) {
            $('.warning, .error').remove();

            if (json['redirect']) {
                location = json['redirect'];
            } else if (json['error']) {
                $('#checkoutLogin .supercheckout-checkout-content').prepend('<div class="warning" style="display: none;">' + json['error']['warning'] + '</div>');
                $('#checkoutLogin .supercheckout-checkout-content').show();
                $('.warning').fadeIn('slow');
            }
        },
        error: function(xhr, ajaxOptions, thrownError) {
            console.log(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
        }
    });
});
// FOR ADDING COUPON AND VOUCHERS AND ALSO REDEEMING THEM
//adding 
$('#confirm_password').blur(function () {        
        matchPassword();
});
function callCoupon(){
    $("#homeals-coupon-warn").hide();
    var code=$('#coupon_code').val();
    if(code==""){
        $("#coupon_code").css({
            "background-color": "#FFD1D1"
        });        
    }else{
        $("#coupon_code").css({
            "background-color": "white"
        });
        $('#confirm').html('<div id="loading" style="text-align:center;"><img src="image/data/checkoutPage/loading_large.gif" width="100" height="100"></div>');
        /*$('#confirmLoader').show();*/
        $.ajax({
            type: "POST",
            url: "index.php?route=supercheckout/supercheckout/validateCoupon",
            data: 'coupon='+code,

            success: function(json) {
                var obj = jQuery.parseJSON( json );

                if (obj.warning) {
                    
                    $("#coupon_code").val("");
                    $("#homeals-coupon-warn").show();

                $.gritter.add({
                        title: 'Notification!',
                        text: obj.warning,
                        class_name:'gritter-warning',
                        sticky: false,
                        time: '3000'
                });
                    //$('#confirmLoader').hide();
                    if(window.callconfirm==true){
                        validateCheckout();
                        window.callconfirm=false;
                        window.confirmclick=false;
                    }
                    window.callconfirm=false;
                    window.confirmclick=false;
                    window.couponBlur=true;
                }
                else{
                        $.gritter.add({
                        title: 'Notification!',
                        text: '<?php echo $text_coupon; ?>',
                //	image: '',
                        class_name:'gritter-success',
                        sticky: false,
                        time: '3000'
                    });
                           //validatePaymentMethodRefresh();
                           refreshAll();
                }

            }

        });
    }
}
//adding voucher
function callVoucher(){
    var code=$('#voucher_code').val();
    if(code==""){
        $("#voucher_code").css({
            "background-color": "#FFD1D1"
        });
    }else{
        $("#voucher_code").css({
            "background-color": "white"
        });
        $('#confirm').html('<div id="loading" style="text-align:center;"><img src="image/data/checkoutPage/loading_large.gif" width="100" height="100"></div>');
        /*$('#confirmLoader').show();*/
        $.ajax({
            type: "POST",
            url: "index.php?route=supercheckout/supercheckout/validateVoucher",
            data: 'voucher='+code,
            success: function(json) {
                var obj = jQuery.parseJSON( json );

                if (obj.warning) {
                $.gritter.add({
                        title: 'Notification!',
                        text: obj.warning,
                //	image: '',
                        class_name:'gritter-warning',
                        sticky: false,
                        time: '3000'
                });
//                    $('#confirmCheckout .supercheckout-checkout-content').html("");
//                    $('#confirmCheckout .supercheckout-checkout-content').prepend('<div class="warning" style="display: none;">' + obj.warning + '</div>');
//                    $('.supercheckout-checkout-content').show();
//                    $('.warning').fadeIn('slow');
                    //$('#confirmLoader').hide();
                    if(window.callconfirm==true){
                        validateCheckout();
                        window.callconfirm=false;
                        window.confirmclick=false;
                    }
                    window.callconfirm=false;
                    window.confirmclick=false;
                    window.voucherBlur=true;
                }
                else{
                    $.gritter.add({
                        title: 'Notification!',
                        text: '<?php echo $text_voucher_success; ?>',
                //	image: '',
                        class_name:'gritter-success',
                        sticky: false,
                        time: '3000'
                    });
                    validatePaymentMethodRefresh();
                }

            }

        });
    }
}
//    redeeming coupon and voucher
function redeem(id){
    $('#confirm').html('<div id="loading" style="text-align:center;"><img src="image/data/checkoutPage/loading_large.gif" width="100" height="100"></div>');
    /*$('#confirmLoader').show();*/
    $.ajax({
        type: "POST",
        url: "index.php?route=supercheckout/supercheckout/redeem",
        data: 'redeem='+id,
        success: function() {
            $.ajax({
                url: 'index.php?route=supercheckout/confirm',
                dataType: 'html',
                success: function(html) {
                    $('#confirmCheckout').html(html);
//                    $('#paymentDisable').html("");
//                    $('#confirmCheckout .supercheckout-checkout-content').prepend('<div class="success"><?php echo $text_remove; ?></div>');
//                    $('.supercheckout-checkout-content').show();
                $.gritter.add({
                        title: 'Notification!',
                        text: 'Your '+id+' has been redeemed Successfully!',
                //	image: '',
                        class_name:'gritter-success',
                        sticky: false,
                        time: '3000'
                });
                    //$('#confirmLoader').hide();
                    $.ajax({
                        url: 'index.php?route=supercheckout/payment_display',
                        dataType: 'html',
                        success: function(html) {
                            $('#display_payment').html(html);
//                            validatePaymentMethodRefresh();
                        },
                        error: function(xhr, ajaxOptions, thrownError) {
                            console.log(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                        }
                    });

                },
                error: function(xhr, ajaxOptions, thrownError) {
                    console.log(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                }

            });
        }

    });
}

//EDITING CART LIKE REMOVING PRODUCT AND UPDATING QUANTITY
    function removeProduct(id){
        var id=id;
        /*$('#confirmLoader').show();*/
        $.ajax({
            type: "POST",
            url: "index.php?route=supercheckout/supercheckout/cart",
            data: 'remove='+id,
            success: function(msg){
//          refreshAll();
            validateCheckoutRefresh();
            window.cart_modified=true;
            }
        });
    }
    function updateQuantity(id){
        var id=id;
        /*$('#confirmLoader').show();*/
        $.ajax({
            type: "POST",
            url: "index.php?route=supercheckout/supercheckout/cart&quantity",
            data: $('#confirmCheckout input[type=\'text\']'),
            success: function(msg){
            validateCheckoutRefresh();
//            refreshAll();
            window.cart_modified=true;            

            }
        });
    }

// FOR REFRESHING ALL AT ONCE i.e. PAYMENT METHOD, SHIPPING METHOD, CART AND PAYMENT GATEWAY
function refreshAll(){
//window.confirmclick=true;

    $('#cboxLoadingOverlay').show();
    $('#cboxLoadingGraphic').show();
    
    

    $("input[name='floor']").trigger("blur");
    $("input[name='apartment']").trigger("blur");
    $("input[name='mobile']").trigger("blur");
    $("input[name='contact']").trigger("blur");
    $("input[name='street_num']").trigger("blur");
    //$("input[name='homeals-delivery-address']").trigger("blur");
    
    $('#shippingMethodLoader').show();
    $.ajax({
        url: 'index.php?route=supercheckout/shipping_method',
        dataType: 'html',
        success: function(html) {
            $('#shipping-method').html(html);
            $('#shippingMethodLoader').hide();
            $('#paymentMethodLoader').show();
            $.ajax({
                url: 'index.php?route=supercheckout/payment_method',
                dataType: 'html',
                success: function(html) {
                    $('#payment-method').html(html);
                    $('#paymentMethodLoader').hide();
                    /*$('#confirmLoader').show();*/
                    $.ajax({
                        url: 'index.php?route=supercheckout/confirm',
                        dataType: 'html',
                        success: function(html) {            
                            $('#confirmLoader').hide();
                            $('#confirmCheckout').html(html);
                            $('#paymentDisable').html("");
                            $.ajax({
                                url: 'index.php?route=supercheckout/payment_display',
                                dataType: 'html',
                                success: function(html) {
				    
                                    $('#display_payment').html(html);
                                    
                                    var int_price = 0;
                                    
                                    var string_price = $('#homeals-total').html();
                                    
                                    if (string_price) {
                                        int_price = parseInt(string_price.substring(1));
                                    }
                                    
                                    var int_shipping = 0 ;
                                    var string_shipping = $('#homeals-shipping').html();
                                    
                                    if (string_shipping) {
                                        int_shipping = parseInt(string_shipping.substring(1));
                                    }
                                    
                                    
                                    var int_coupon = 0;
                                    
                                    if ($("#homeals-coupon").length) {
                                        
                                        var string_coupon = $('#homeals-coupon').html();
                                        var int_coupon = parseInt(string_coupon.substring(1));
                                        
                                        $('.total-coupon').show();
                                        $('#homeals-coupon-price').html("₪"+(int_coupon));
                                        $('#homeals-subtotal-price').html("₪"+(int_price-int_coupon));
                                        $('.homeals-coupon-input').hide();
                                        
                                        $("#payment-method input").trigger("click");
                                        $("#payment-method input").attr("checked","checked");
                                        
                                        if (!int_price) {
                                            $("#placeorderButton").html("לביצוע ההזמנה לחץ כאן");
                                        } else {
                                            $("#placeorderButton").html("<?php echo $text_pay_credit; ?>");
                                        }
                                        
                                    } else {
                                        $('.homeals-coupon-input').show();
                                        $('.total-coupon').hide();
                                    }
                                    
				    if($('#has-coupon').val() != 1){
					$('.homeals-coupon-input').hide();
                                        $('.total-coupon').hide();
				    }
                                    
                                    if ($("#homeals-credit").length) {
                                        
                                        var string_credit = $('#homeals-credit').html();
                                        var title_credit = $('#homeals-credit').parent().parent().find(".title").text()+":";
                                        var int_credit = parseInt(string_credit.substring(1));
                                        
                                        $("#homeals-credit-price").parent().find(".text").text(title_credit);
                                        
                                        $('.total-credit').show();
                                        $('#homeals-credit-price').html("₪"+(int_credit));
                                        $('#homeals-subtotal-price').html("₪"+(int_price-int_credit));
                                        
                                        $("#payment-method input").trigger("click");
                                        $("#payment-method input").attr("checked","checked");
                                        
                                        if (!int_price) {
                                            $("#placeorderButton").html("לביצוע ההזמנה לחץ כאן");
                                            $("#paypalicons").hide();
                                            $("#or").hide();
                                            $(".pay-with-paypal").hide();
                                        } else {
                                            $("#placeorderButton").html("<?php echo $text_pay_credit; ?>");
                                            $("#paypalicons").show();
                                            $("#or").show();
                                            $(".pay-with-paypal").show();
                                        }
                                        
                                    } else {
                                        //$('.total-credit').hide();
                                    }
                                    
                                    
                                    var string_sub_total = $('#homeals-sub_total').html();
                                    var int_sub_total = parseInt(string_sub_total.substring(1));
                                    
                                    //$('#total-pro').html("₪"+(int_price+int_shipping));
                                    $('#total-pro').html("₪"+(int_sub_total));
                                    
                                    $('#homeals-total-price').html($('#homeals-total').html());
                                    $('#total-shipping-cost').html($('#homeals-shipping').html());
                                    $('#homealscheckout-time-text').html($('#delivery-date-time').val());
				    
				    $('#cboxLoadingOverlay').hide();
				    $('#cboxLoadingGraphic').hide();
                                    
                                    if(window.callconfirm==true){
                                        validateCheckout();
                                        window.callconfirm=false;
                                        window.confirmclick=false;
                                    }
                                    
                                    window.callconfirm=false;
                                    window.confirmclick=false;
                                    if(window.cart_modified==true){
                                        $.gritter.add({
                                            title: 'Notification!',
                                            text: '<?php echo $text_remove; ?>',
                                            class_name:'gritter-success',
                                            sticky: false,
                                            time: '3000'
                                            });
                                            window.cart_modified=false;
                                    }
                                    //            validatePaymentMethodRefresh();

                                },
                                error: function(xhr, ajaxOptions, thrownError) {
                                }
                            });	
                        },
                        error: function(xhr, ajaxOptions, thrownError) {
                        }
                    });	
                },
                error: function(xhr, ajaxOptions, thrownError) {
                }
            });
        },
        error: function(xhr, ajaxOptions, thrownError) {
        }
    });
    
        
}
// Apply coupon on blur
$('#coupon_code').live('blur',function () {
        window.confirmclick=true;
        window.couponBlur=false;
        callCoupon();
}); 
// Apply voucher on blur
$('#voucher_code').live('blur',function () {
        window.confirmclick=true;
        window.voucherBlur=false;
        callVoucher();
}); 
//FOR REFRESHING CART AND PAYMENT GATEWAY IF CHANGE IS MADE IN CART ON BILLING ADDRESS
    $('#checkoutBillingAddress #payment-new input[type=\'text\']').blur(function () {
        cartPaymentRefresh();
    });    
    $('#checkoutBillingAddress #payment-new input[name=\'postcode\']').blur(function () {
        validateCheckoutRefresh();
    });
    $('#checkoutShippingAddress #shipping-new input[name=\'postcode\']').blur(function () {
        validateCheckoutRefresh();
    });
    function cartPaymentRefresh(){
    var guestEnable="<?php echo $settings['step']['login']['option']['guest']['display']; ?>";
    var loggedIn="<?php echo $logged; ?>";//271

    if(guestEnable==1)
    {
        if(loggedIn!=""){
            LoginPaymentAddressRefresh();
        }else{
            GuestPaymentAddressRefresh();
        }
    }
    else
    {
        if(loggedIn!=""){
            LoginPaymentAddressRefresh();
        }else{
            $('#checkoutLogin .supercheckout-checkout-content').html('<div class="warning" style="display: none;">' + 'Please Login' + '</div>');
            $('#checkoutLogin .supercheckout-checkout-content').show();
            $('.warning').fadeIn('slow');
            goToByScroll('checkoutLogin');
        }
    }
}
function LoginPaymentAddressRefresh(){
    var paymentAddressEnable="1";
    if(paymentAddressEnable==1){
        $.ajax({
            url: 'index.php?route=supercheckout/supercheckout/setValueForLoginPayment',
            type: 'post',
            dataType:'json',
            data: $('#checkoutBillingAddress input[type=\'text\'], #checkoutBillingAddress input[type=\'password\'], #checkoutBillingAddress input[type=\'checkbox\']:checked, #checkoutBillingAddress input[type=\'radio\']:checked, #checkoutBillingAddress input[type=\'hidden\'], #checkoutBillingAddress select, #checkoutBillingAddress input[type=\'checkbox\']:checked'),
            beforeSend: function() {
                $('#button-payment-address').attr('disabled', true);
                $('#button-payment-address').after('<span class="wait">&nbsp;<img src="catalog/view/theme/default/image/supercheckout/loading12.gif" alt="" /></span>');
            },
            complete: function() {
                $('#button-payment-address').attr('disabled', false);
                $('.wait').remove();
            },
            success: function(json) {
                $('.warning, .errorsmall').remove();
                if (json['error']){
                    if (json['error']['firstname']) {
                        $('#payment-new input[name=\'firstname\']').after('<span class="errorsmall">' + json['error']['firstname'] + '</span>');
                    }

                    if (json['error']['lastname']) {
                        $('#payment-new input[name=\'lastname\']').after('<span class="errorsmall">' + json['error']['lastname'] + '</span>');
                    }

                    if (json['error']['email']) {
                        $('#payment-new input[name=\'email\']').after('<span class="errorsmall">' + json['error']['email'] + '</span>');
                    }

                    if (json['error']['telephone']) {
                        $('#payment-new input[name=\'telephone\']').after('<span class="errorsmall">' + json['error']['telephone'] + '</span>');
                    }
                    if (json['error']['company']) {
                        $('#payment-new input[name=\'company\']').after('<span class="errorsmall">' + json['error']['company'] + '</span>');
                    }
                    if (json['error']['company_id']) {
                        $('#payment-new input[name=\'company_id\']').after('<span class="errorsmall">' + json['error']['company_id'] + '</span>');
                    }

                    if (json['error']['tax_id']) {
                        $('#payment-new input[name=\'tax_id\']').after('<span class="errorsmall">' + json['error']['tax_id'] + '</span>');
                    }

                    if (json['error']['address_1']) {
                        $('#payment-new input[name=\'address_1\']').after('<span class="errorsmall">' + json['error']['address_1'] + '</span>');
                    }
                    if (json['error']['address_2']) {
                        $('#payment-new input[name=\'address_2\']').after('<span class="errorsmall">' + json['error']['address_2'] + '</span>');
                    }
                    if (json['error']['city']) {
                        $('#payment-new input[name=\'city\']').after('<span class="errorsmall">' + json['error']['city'] + '</span>');
                    }

                    if (json['error']['postcode']) {
                        $('#payment-new input[name=\'postcode\']').after('<span class="errorsmall">' + json['error']['postcode'] + '</span>');
                    }

                    if (json['error']['country']) {
                        $('#payment-new select[name=\'country_id\']').after('<span class="errorsmall">' + json['error']['country'] + '</span>');
                    }

                    if (json['error']['zone']) {
                        $('#payment-new select[name=\'zone_id\']').after('<span class="errorsmall">' + json['error']['zone'] + '</span>');
                    }
                }else{
                    RefreshCart();
                }
            }
        });
    }
    else{
        RefreshCart();
    }

}
function GuestPaymentAddressRefresh(){
    var paymentGuestAddressEnable="1";
    var useforShippingEnable="<?php echo $settings['option']['guest']['payment_address']['fields']['shipping']['display']; ?>";
    if(paymentGuestAddressEnable==1){
        if(useforShippingEnable==1){
            $.ajax({
                url: 'index.php?route=supercheckout/supercheckout/setValueForGuestPayment',
                type: 'post',
                dataType:'json',
                data: $('#payment-new  input[type=\'text\'], #checkoutBillingAddress input[type=\'checkbox\']:checked, #payment-new select, #checkoutLogin input[name=\'email\'] '),
                beforeSend: function() {
                    $('#button-guest').attr('disabled', true);
                    $('#button-guest').after('<span class="wait">&nbsp;<img src="catalog/view/theme/default/image/supercheckout/loading12.gif" alt="" /></span>');
                },
                complete: function() {
                    $('#button-guest').attr('disabled', false);
                    $('.wait').remove();
                },
                success: function(json) {

                    $('.warning, .errorsmall').remove();
                if (json['error']){
                    if (json['error']['firstname']) {
                        $('#payment-new input[name=\'firstname\']').after('<span class="errorsmall">' + json['error']['firstname'] + '</span>');
                    }

                    if (json['error']['lastname']) {
                        $('#payment-new input[name=\'lastname\']').after('<span class="errorsmall">' + json['error']['lastname'] + '</span>');
                    }

                    if (json['error']['email']) {
                        $('#payment-new input[name=\'email\']').after('<span class="errorsmall">' + json['error']['email'] + '</span>');
                    }

                    if (json['error']['telephone']) {
                        $('#payment-new input[name=\'telephone\']').after('<span class="errorsmall">' + json['error']['telephone'] + '</span>');
                    }
                    if (json['error']['company']) {
                        $('#payment-new input[name=\'company\']').after('<span class="errorsmall">' + json['error']['company'] + '</span>');
                    }
                    if (json['error']['company_id']) {
                        $('#payment-new input[name=\'company_id\']').after('<span class="errorsmall">' + json['error']['company_id'] + '</span>');
                    }

                    if (json['error']['tax_id']) {
                        $('#payment-new input[name=\'tax_id\']').after('<span class="errorsmall">' + json['error']['tax_id'] + '</span>');
                    }

                    if (json['error']['address_1']) {
                        $('#payment-new input[name=\'address_1\']').after('<span class="errorsmall">' + json['error']['address_1'] + '</span>');
                    }
                    if (json['error']['address_2']) {
                        $('#payment-new input[name=\'address_2\']').after('<span class="errorsmall">' + json['error']['address_2'] + '</span>');
                    }
                    if (json['error']['city']) {
                        $('#payment-new input[name=\'city\']').after('<span class="errorsmall">' + json['error']['city'] + '</span>');
                    }

                    if (json['error']['postcode']) {
                        $('#payment-new input[name=\'postcode\']').after('<span class="errorsmall">' + json['error']['postcode'] + '</span>');
                    }

                    if (json['error']['country']) {
                        $('#payment-new select[name=\'country_id\']').after('<span class="errorsmall">' + json['error']['country'] + '</span>');
                    }

                    if (json['error']['zone']) {
                        $('#payment-new select[name=\'zone_id\']').after('<span class="errorsmall">' + json['error']['zone'] + '</span>');
                    }
                    }else{

                        RefreshCart();
                    }
                }
            });
        }
        else{
            $.ajax({
                url: 'index.php?route=supercheckout/supercheckout/setValueForGuestPayment',
                type: 'post',
                dataType:'json',
                data: $('#payment-new  input[type=\'text\'],  #payment-new select'),
                beforeSend: function() {
                    $('#button-guest').attr('disabled', true);
                    $('#button-guest').after('<span class="wait">&nbsp;<img src="catalog/view/theme/default/image/supercheckout/loading12.gif" alt="" /></span>');
                },
                complete: function() {
                    $('#button-guest').attr('disabled', false);
                    $('.wait').remove();
                },
                success: function(json) {
                $('.warning, .errorsmall').remove();
                if (json['error']){
                    if (json['error']['firstname']) {
                        $('#payment-new input[name=\'firstname\']').after('<span class="errorsmall">' + json['error']['firstname'] + '</span>');
                    }

                    if (json['error']['lastname']) {
                        $('#payment-new input[name=\'lastname\']').after('<span class="errorsmall">' + json['error']['lastname'] + '</span>');
                    }

                    if (json['error']['email']) {
                        $('#payment-new input[name=\'email\']').after('<span class="errorsmall">' + json['error']['email'] + '</span>');
                    }

                    if (json['error']['telephone']) {
                        $('#payment-new input[name=\'telephone\']').after('<span class="errorsmall">' + json['error']['telephone'] + '</span>');
                    }
                    if (json['error']['company']) {
                        $('#payment-new input[name=\'company\']').after('<span class="errorsmall">' + json['error']['company'] + '</span>');
                    }
                    if (json['error']['company_id']) {
                        $('#payment-new input[name=\'company_id\']').after('<span class="errorsmall">' + json['error']['company_id'] + '</span>');
                    }

                    if (json['error']['tax_id']) {
                        $('#payment-new input[name=\'tax_id\']').after('<span class="errorsmall">' + json['error']['tax_id'] + '</span>');
                    }

                    if (json['error']['address_1']) {
                        $('#payment-new input[name=\'address_1\']').after('<span class="errorsmall">' + json['error']['address_1'] + '</span>');
                    }
                    if (json['error']['address_2']) {
                        $('#payment-new input[name=\'address_2\']').after('<span class="errorsmall">' + json['error']['address_2'] + '</span>');
                    }
                    if (json['error']['city']) {
                        $('#payment-new input[name=\'city\']').after('<span class="errorsmall">' + json['error']['city'] + '</span>');
                    }

                    if (json['error']['postcode']) {
                        $('#payment-new input[name=\'postcode\']').after('<span class="errorsmall">' + json['error']['postcode'] + '</span>');
                    }

                    if (json['error']['country']) {
                        $('#payment-new select[name=\'country_id\']').after('<span class="errorsmall">' + json['error']['country'] + '</span>');
                    }

                    if (json['error']['zone']) {
                        $('#payment-new select[name=\'zone_id\']').after('<span class="errorsmall">' + json['error']['zone'] + '</span>');
                    }
                    }else{

                        RefreshCart();
                    }
                }
            });
        }
    }else{

        RefreshCart();
    }
}
function RefreshCart(){
    $.ajax({
        url: 'index.php?route=supercheckout/confirm',
        dataType: 'html',
        success: function(html) {
            $('#confirmCheckout').html(html);
//            $('#paymentDisable').html("");
//            $('#confirmCheckout .supercheckout-checkout-content').prepend('<div class="success"><?php echo $text_remove; ?></div>');
//            $('.supercheckout-checkout-content').show();
            //$('#confirmLoader').hide();

            $.ajax({
                url: 'index.php?route=supercheckout/payment_display',
                dataType: 'html',
                success: function(html) {
                    $('#display_payment').html(html);
                },
                error: function(xhr, ajaxOptions, thrownError) {
                    console.log(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                }
            });
        },
        error: function(xhr, ajaxOptions, thrownError) {
            console.log(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
        }
    });
}

// FOR SUBMITTING ON PRESSING ENTER LIKE LOGIN, COUPON, VOUCHER
$('#email').keypress(function(event){
    if(event.keyCode == 13){
        $('#button-login').click();
    }
});
$('#password').keypress(function(event){    
    if(event.keyCode == 13){
        $('#button-login').click();
    }
});
$('#coupon_code').keypress(function(event){
    if(event.keyCode == 13){
        $('#button-coupon').click();
    }
});
$('#voucher_code').keypress(function(event){
    if(event.keyCode == 13){
        $('#button-voucher').click();
    }
});
$(document).ready(function(){
    //payment address sorting
    $('#button-confirm').unbind('click');
<?php if($settings['general']['layout']=='3-Column'){ ?>
// FOR SORTING BLOCK AND FIELDS IN SUPERCHECKOUT PAGE

    $('.columnleftsort > .supercheckout-blocks').tsort({
        attr:'data-row'
    });
    $('.columnleftsort > .supercheckout-blocks').each(function(){
        $(this).appendTo('#columnleft-' + $(this).attr('data-column'));

    });

    var $wrapper = $('#payment_address_table');
    $wrapper.find('.sort_data').sort(function (a, b) {
        return +$(a).data("percentage") - + $(b).data("percentage");
    })
    .appendTo( $wrapper );

    //shippping address sorting
    var $wrapper = $('#shipping_address_table');
    $wrapper.find('.sort_data').sort(function (a, b) {
        return +$(a).data("percentage") - + $(b).data("percentage");
    })
    .appendTo( $wrapper );
    //block sorting
//    $('#button-confirm').unbind('click');
    $('.columnleftsort > .supercheckout-blocks').tsort({
        attr:'data-row'
    });
    $('.columnleftsort > .supercheckout-blocks').each(function(){
        $(this).appendTo('#columnleft-' + $(this).attr('data-column'));

    });

<?php } else if($settings['general']['layout'] == '2-Column') { ?>
    $('.columnleftsort > .supercheckout-blocks').tsort({attr:'col-inside-data'});
    $('.columnleftsort > .supercheckout-blocks').each(function(){
        if($(this).attr('data-column-inside')=="4") {
            $(this).appendTo('#column-2-lower' );
        } else if($(this).attr('data-column-inside')=="3") {
            $(this).appendTo('#column-1-inside' );
        } else if($(this).attr('data-column-inside')=="2") {
            $(this).appendTo('#column-2-upper');
        } else {
            $(this).appendTo('#columnleft-1');
        }
    })
    $('#columnleft-1 > .supercheckout-blocks').tsort({attr:'data-row'});
    $('#columnleft-1 > .supercheckout-blocks').each(function(){
        $(this).appendTo('#columnleft-' + $(this).attr('data-column') );
    })

    $('#column-2-upper > .supercheckout-blocks').tsort({attr:'data-row'});
    $('#column-2-upper > .supercheckout-blocks').each(function(){
        $(this).appendTo('#column-2-upper' );
    })

    $('#column-2-lower > .supercheckout-blocks').tsort({attr:'data-row'});
    $('#column-2-lower > .supercheckout-blocks').each(function(){
        $(this).appendTo('#column-2-lower' );
    })

    $('#column-1-inside > .supercheckout-blocks').tsort({attr:'data-row'});
    $('#column-1-inside > .supercheckout-blocks').each(function() {
        $(this).appendTo('#column-' + $(this).attr('data-column')+'-inside' );
    })

    var $wrapper = $('#payment_address_table');
    $wrapper.find('.sort_data').sort(function (a, b) {
        return +$(a).data("percentage") - + $(b).data("percentage");
    }).appendTo( $wrapper );

    //Shippping Address Sorting
    var $wrapper = $('#shipping_address_table');
    $wrapper.find('.sort_data').sort(function (a, b) {
        return + $(a).data("percentage") - + $(b).data("percentage");
    }).appendTo( $wrapper );

<?php } else if($settings['general']['layout'] == '1-Column') { ?>
    $('#columnleft-1 > .supercheckout-blocks').tsort({attr:'data-row'});
    $('#columnleft-1 > .supercheckout-blocks').each(function() {
        $(this).appendTo('#columnleft-1' );
    }); 
<?php } ?>
}); 
$(document).ready(function(){
<?php if($layout_name=='three-column'){ ?>
    var a = $('#supercheckout-columnleft').height(); 
    var d = $('#supercheckout_login_option_box').height();  
    var e = a-d;
    $('#columnleft-1').css('min-height', e + 'px');
    $('#columnleft-3').css('min-height', e + 'px');
    
    
<?php }elseif($layout_name=='two-column'){ ?>
    var a = $('#supercheckout-columnleft').height();    
    var d = $('#supercheckout_login_option_box').height();  
    var e = a-d;
    $('#columnleft-1').css('min-height', e + 'px');
    var b = $('#column-1-inside').height();
    var c = $('#column-2-inside').height();
    if(c > b) {
        $('#column-1-inside').css('min-height', c + 'px');
    } else {
        $('#column-2-inside').css('min-height', b + 'px');
    }
<?php } ?>
});
</script>
<script type="text/javascript"><!--
$('.colorbox').colorbox({
	width: 640,
	height: 480
});
//--></script> 
 <script>
var button_width = $('#confirm_order').width() + 76;
//alert(button_width);
$('#progressbar').css('width', button_width + 'px');

</script> 