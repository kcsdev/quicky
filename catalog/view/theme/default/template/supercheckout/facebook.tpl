<link rel="stylesheet" type="text/css" href="catalog/view/theme/default/stylesheet/supercheckout-light.css" />
<div id="fb-root"></div>
<script type="text/javascript">
// FACEBOOK LOGIN & REGISTERING SCRIPT
var facebookEnable="<?php echo $settings['step']['facebook_login']['display']; ?>";
    if('<?php echo $logged; ?>'=='' && facebookEnable==1){
        var button;
        var userInfo;
        window.fbAsyncInit = function() {
            FB.init({
                appId: '<?php echo $appId; ?>', //change the appId to your appId
                status: true,
                cookie: true,
                xfbml: true,
                oauth: true
            });
            function updateButton(response) {
                button       =   document.getElementById('fb-auth');
                userInfo     =   document.getElementById('user-info');
                if (response.authResponse) {
                    button.onclick = function() {
                        FB.logout(function(response) {
                            logout(response);
                            showLoader(true);
                            FB.login(function(response) {
                                if (response.authResponse) {
                                    FB.api('/me', function(info) {
                                        login(response, info);
                                    });
                                } else {
                                    //user cancelled login or did not grant authorization
                                    showLoader(false);
                                }
                            }, {
                                scope:'email,user_birthday,user_about_me'
                            });
                        });
                    };
                } else {
                    //user is not connected to your app or logged out
                    button.innerHTML = '';
                    button.onclick = function() {
                        showLoader(true);
                        FB.login(function(response) {
                            if (response.authResponse) {
                                FB.api('/me', function(info) {
                                    login(response, info);
                                });
                            } else {
                                //user cancelled login or did not grant authorization
                                showLoader(false);
                            }
                        }, {
                            scope:'email,user_birthday,user_about_me'
                        });
                    }
                }
            }

            // run once with current status and whenever the status changes
            FB.getLoginStatus(updateButton);
            FB.Event.subscribe('auth.statusChange', updateButton);
        };
        (function() {
            var e = document.createElement('script');
            e.async = true;
            e.src = document.location.protocol
                + '//connect.facebook.net/en_US/all.js';
            document.getElementById('fb-root').appendChild(e);
        }());


        function login(response, info){
            if (response.authResponse) {

                showLoader(false);
                    fqlQuery();
            }
        }
        function logout(response){
            showLoader(false);
        }
        function fqlQuery(){
            showLoader(true);
            FB.api('/me', function(response) {
                showLoader(false);
                var query       =  FB.Data.query('select name,first_name,last_name,email, profile_url, sex, pic_big, pic_square, contact_email from user where uid={0}', response.id);
                query.wait(function(rows) {
                    //console.log(rows);
                    $.ajax({
                        url: 'index.php?route=supercheckout/supercheckout/checkUser/&email='+rows[0].email,
                        success: function(response2) {
                            if(response2=="registered"){
                                showLoader(true);
                                $.ajax({
                                    url: 'index.php?route=supercheckout/supercheckout/doLogin&emailLogin='+rows[0].email,
                                    success: function(response3) {
                                        //alert(response3);
                                        location.reload();
                                        //window.location='index.php?route=supercheckout/'+response3
                                    },
                                    complete: function(){
                                        showLoader(false);
                                    }
                                });
                            }
                            else{
                                showLoader(true);
                                $.ajax({
                                    url: 'index.php?route=supercheckout/supercheckout/getvalue&firstname='+rows[0].first_name+'&last_name='+rows[0].last_name+'&useremail='+rows[0].email+'&sex='+rows[0].sex+'&pic_big='+rows[0].pic_big+'&pic_square='+rows[0].pic_square,
                                    success: function(response4) {
                                        location.reload();
                                        //alert(response4);
                                        //window.location='index.php?route=supercheckout/'+response4
                                    },
                                    complete: function(){
                                        showLoader(false);
                                    }
                                });
                            }
                        }
                    });

                });
            });
        }
        function showLoader(status){
            if (status){
                $('#loading-image').show();
            }
            else
                $('#loading-image').hide();
        }

    }
</script> 