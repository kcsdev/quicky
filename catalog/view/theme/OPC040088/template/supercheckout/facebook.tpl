<link rel="stylesheet" type="text/css" href="catalog/view/theme/default/stylesheet/supercheckout-light.css" />
<div id="fb-root"></div>
<script type="text/javascript">
// FACEBOOK LOGIN & REGISTERING SCRIPT

var facebookEnable="<?php echo $settings['step']['facebook_login']['display']; ?>";
    if(true || '<?php echo $logged; ?>'==''){
        var button;
        var userInfo;
        var myDetails;
        window.fbAsyncInit = function() {
            
            FB.init({
                appId: '<?php echo $appId; ?>', //change the appId to your appId
                status: true,
                cookie: true,
                xfbml: true,
                frictionlessRequests: true,
                oauth: true
            });
            
            function updateButton(response) {
                button       =   document.getElementById('fb-auth');
                userInfo     =   document.getElementById('user-info');
                if (response.authResponse) {
                    button.onclick = function() {
                        
                        $(".loading-screen").show();
                        
                        if (response.status === 'connected') {
                            FB.api('/me', function(info) {
                                    login(response, info);
                            });
                        } else {
                            FB.logout(function(response) {
                                logout(response);
                                showLoader(true);
                                
                                
                                FB.login(function(response) {
                                    if (response.authResponse) {
                                        FB.api('/me', function(info) {
                                            login(response, info);
                                        });
                                    } else {
                                        //user cancelled login or did not grant authorization
                                        $(".loading-screen").hide();
                                    }
                                }, {
                                    scope:'email,user_birthday,user_about_me'
                                });
                                
                                
                            });    
                        }
                    };
                } else {
                    //user is not connected to your app or logged out
                    button.innerHTML = '';
                    button.onclick = function() {
                        $(".loading-screen").show();
                        
                        
                        //var isMobile = false;
                        //try {
                        //    isMobile = (window.location.href == top.location.href && window.location.href.indexOf("/mobile/") != -1);
                        //} catch (e) {}
                        //if (!isMobile) {
                        //    FB.login();
                        //} else {
                        //    
                        //    var permissions = 'email,user_birthday,user_about_me';
                        //    var nappId = '<?php echo $appId; ?>';
                        //    var redirectPage = ;
                        //    
                        //    var permissionUrl = "https://m.facebook.com/dialog/oauth?client_id=" + nappId + "&response_type=code&redirect_uri=" + redirectPage + "&scope=" + permissions;
                        //    window.location = permissionUrl;
                        //    return;
                        //}
                        
                        FB.login(function(response) {
                            if (response.authResponse) {
                                FB.api('/me', function(info) {
                                    login(response, info);
                                });
                            } else {
                                //user cancelled login or did not grant authorization
                                $(".loading-screen").hide();
                            }
                        }, {
                            scope:'email,user_birthday,user_about_me'
                        });
                        
                        
                        
                    }
                }
            }

            // run once with current status and whenever the status changes
            FB.getLoginStatus(updateButton);
            FB.Event.subscribe('auth.statusChange', updateButton);
        };
        (function() {
            var e = document.createElement('script');
            e.async = true;
            e.src = document.location.protocol
                + '//connect.facebook.net/en_US/all.js';
            document.getElementById('fb-root').appendChild(e);
        }());


        function login(response, info){
            if (response.authResponse) {
                showLoader(false);
                    getUserDetails();
            }
        }
        
        function logout(response){
            showLoader(false);
        }
        
        function getUserImage(){
            FB.api(
                "/me/picture",
                {
                    "redirect": false,
                    "height": "200",
                    "type": "normal",
                    "width": "200"
                },
                function (response) {
                  if (response && !response.error) {
                    
                    //console.log(response.data);
                    
                    if (response.data.is_silhouette) {
                        //console.log("bad");
                        myDetails.avatar = "";
                        
                    } else {
                        //console.log("good");
                        myDetails.avatar = btoa(response.data.url);
                    }
                    
                    uploadDetails();
                    
                  }
                }
            );
        }
        
        function getUserDetails(){
            FB.api(
                "/me",
                function (response) {
                  if (response && !response.error) {
                    
                    //console.log(response);
                    myDetails = response;
                    
                    getUserImage();
                    
                  }
                }
            );
        }
        
        function uploadDetails(){
            showLoader(true);
            
            if(myDetails.email == ""){
                myDetails.email = "fake"+myDetails.first_name+"@nofacebook.com";
            }
            
            $.ajax({
                url: 'index.php?route=supercheckout/supercheckout/checkUser/&email='+myDetails.email,
                success: function(response2) {
                    showLoader(true);
                    
                    if(response2=="registered"){
                        $.ajax({
                            url: 'index.php?route=supercheckout/supercheckout/doLogin&emailLogin='+myDetails.email,
                            success: function(response3) {
                                
                                showLoader(true);
                                                                
                                // updating user image add in futer
                                //$.ajax({
                                //    url: 'index.php?route=supercheckout/supercheckout/updateUserImage&useremail='+myDetails.email+'&pic_big='+myDetails.avatar,
                                //    success:function(responseImage){
                                //        //console.log("responseImage");
                                //        //console.log(responseImage);
                                        
                                        $('#button-login-fb').attr("id","button-cart");
                                        
                                        var redirect = "<?php echo (isset($this->session->data['redirect']) ? $this->session->data['redirect'] : "" ); ?>";
                                        var redirect_decoded = redirect.replace(/&amp;/g, '&');
                                        
                                        if (add_to_cart) {
                                            
                                            $.ajax({
                                                url: 'index.php?route=checkout/cart/clear',
                                                success:function(){
                                                    
                                                    if ($("#time-of-day").val() != "") {
                                                        $('#button-cart').trigger("click");
                                                    }
                                                    
                                                    location = location.href + "&opencart";
                                                }
                                            });
                                            
                                        } else if (open_review_box) {
                                            location = location.href + "&review";
                                        } else  if (open_pm) {
                                           location = location.href + "&openpm";
                                        } else {
                                            if (!redirect) {
                                                location.reload(true);
                                            } else {
                                                location = redirect_decoded;
                                            }
                                        }
                                
                                    }
                                });
                        //    },
                        //    complete: function(){
                        //        showLoader(false);
                        //    }
                        //});
                        
                    } else {
                        
                        showLoader(true);
                        
                        if(myDetails.email == ""){
                            myDetails.email = "fake"+myDetails.first_name+"@nofacebook.com";
                        }
                        
                        $.ajax({
                            url: 'index.php?route=supercheckout/supercheckout/getvalue&firstname='+myDetails.first_name+'&last_name='+myDetails.last_name+'&useremail='+myDetails.email+'&sex='+myDetails.gender+'&pic_big='+myDetails.avatar+'&pic_square='+myDetails.avatar,
                            success: function(response4) {
                                location.reload();
                                //console.log(response4);
                                //window.location='index.php?route=supercheckout/'+response4
                            },
                            complete: function(){
                                showLoader(false);
                            }
                        });
                        
                    }
                }
            });
        }
        
        function fqlQuery(){
            showLoader(true);
            FB.api('/me', function(response) {
                showLoader(false);
                var query = FB.Data.query('select name,first_name,last_name,email, profile_url, sex, pic_big, pic_square, contact_email from user where uid={0}', response.id);
                query.wait(function(rows) {
                    //console.log(rows);
                    
                    if(rows[0].email == ""){
                                    rows[0].email = rows[0].first_name+"@nofacebook.com";
                                }
                    $.ajax({
                        url: 'index.php?route=supercheckout/supercheckout/checkUser/&email='+rows[0].email,
                        success: function(response2) {
                            if(response2=="registered"){
                                showLoader(true);
                                $.ajax({
                                    url: 'index.php?route=supercheckout/supercheckout/doLogin&emailLogin='+rows[0].email,
                                    success: function(response3) {
                                        //alert(response3);
                                        $('#button-login-fb').attr("id","button-cart");
                                        
                                        var redirect = "<?php echo (isset($this->session->data['redirect']) ? $this->session->data['redirect'] : "" ); ?>";
                                        var redirect_decoded = redirect.replace(/&amp;/g, '&');
                                        
                                        if (add_to_cart) {
                                            
                                            $.ajax({
                                                url: 'index.php?route=checkout/cart/clear',
                                                success:function(){
                                                    
                                                    if ($("#time-of-day").val() != "") {
                                                        $('#button-cart').trigger("click");
                                                    }
                                                    
                                                    location = location.href + "&opencart";
                                                }
                                            });
                                            
                                        } else if (open_review_box) {
                                            location = location.href + "&review";
                                        } else  if (open_pm) {
                                           location = location.href + "&openpm";
                                        } else {
                                            if (!redirect) {
                                                location.reload(true);
                                            } else {
                                                location = redirect_decoded;
                                            }
                                        }
                                    },
                                    complete: function(){
                                        showLoader(false);
                                    }
                                });
                            }
                            else{
                                showLoader(true);
                                
                                if(rows[0].email == ""){
                                    rows[0].email = rows[0].first_name+"@nofacebook.com";
                                }
                                
                                $.ajax({
                                    url: 'index.php?route=supercheckout/supercheckout/getvalue&firstname='+rows[0].first_name+'&last_name='+rows[0].last_name+'&useremail='+rows[0].email+'&sex='+rows[0].sex+'&pic_big='+rows[0].pic_big+'&pic_square='+rows[0].pic_square,
                                    success: function(response4) {
                                        location.reload();
                                        //alert(response4);
                                        //window.location='index.php?route=supercheckout/'+response4
                                    },
                                    complete: function(){
                                        showLoader(false);
                                    }
                                });
                            }
                        }
                    });

                });
            });
        }
        function showLoader(status){
            if (status){
                $('#loading-image').show();
            }
            else
                $('#loading-image').hide();
        }

    }
</script> 