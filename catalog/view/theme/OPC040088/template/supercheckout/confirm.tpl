<script type="text/javascript">
    <?php if($settings['general']['layout']=='3-Column'){ ?>
    var a = $('#supercheckout-columnleft').height();
    var d = $('#supercheckout_login_option_box').height();  
    var e = a-d;
    $('#columnleft-1').css('min-height', e + 'px');
    $('#columnleft-3').css('min-height', e + 'px');
    <?php } ?>
</script>
<?php 
        if($settings['general']['layout']=='3-Column'){
            $layout_name='three-column';
        }elseif($settings['general']['layout']=='2-Column'){
            $layout_name='two-column';
        }elseif($settings['general']['layout']=='1-Column'){
            $layout_name='one-column';
        }
?>






<?php foreach($products_cooks as $cook_id => $cook_products) { ?>
<div id="cook-con">
<div class="name">
    <div class="avatarcon">
            <img class="avatar" src="<?php echo $cooks[$cook_id]['image']; ?>" />
    </div>
    
    <a href="/index.php?route=seller/catalog-seller/profile&seller_id=<?php echo $cooks[$cook_id]['id']; ?>">
    <h3><?php echo $cooks[$cook_id]['name']; ?></h3>
    <?php print_r( $cooks[$cook_id]['city'] ); ?>
    </a>
</div>
</div>

<table class="supercheckout-summarys">
    <tbody>
        <tr class="padding-tr">
        <td class="border-td"></td>
        </tr>
        <?php
        foreach ($cook_products as $product) {   
       ?>
        <tr>
                                <td colspan="4" style="display:<?php if($logged){ if($settings['option']['logged']['cart']['columns']['name']){ echo' ';}else{ echo'none';} }else{ if($settings['option']['guest']['cart']['columns']['name']){ echo' ';}else{ echo'none';}} ?>;" class="supercheckout-name edit-cart" data-pid="<?php echo $product['product_id']; ?>" data-key="<?php echo $product['key']; ?>" >
                                    
                                    <div id="meal-title" >
                                        
                                        <i class="fa fa-chevron-left cart-arrow"></i>
                                        
                                        <a <?php if($logged){ if($settings['option']['logged']['cart']['columns']['image']){ echo'data-toggle="popover"';}else{ echo'';} }else{ if($settings['option']['guest']['cart']['columns']['image']){ echo'data-toggle="popover"';}else{ echo'';}} ?> data-title="<?php echo mb_truncate($product['name'],40); ?>" data-content="<img src='<?php echo $product['thumb']; ?>' />" data-placement="right" href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a>
                                        
                                        <div id="sides-comment">
                                            
                                            <?php if(!empty($product['side_1'])){
                                                
                                                if ((int)$product['side_1_price']) {
                                                    $extra = $product['side_1_extra'];
                                                } else {
                                                    $extra = "";
                                                }
                                            ?>
                                            <div id="side-1" class="side-meal" data-pid="<?php echo $product['side_1_id']; ?>">
                                                + <?php echo $product['side_1'].$extra; ?>  
                                            </div>
                                            <?php } ?>
                                            
                                            <?php if(!empty($product['side_2'])){
                                                
                                                if ((int)$product['side_2_price']) {
                                                    $extra = $product['side_2_extra'];
                                                } else {
                                                    $extra = "";
                                                }    
                                            ?>
                                            <div id="side-2" class="side-meal" data-pid="<?php echo $product['side_2_id']; ?>">
                                                + <?php echo $product['side_2'].$extra; ?>  
                                            </div>
                                            <?php } ?>
                                            
                                            <?php if(!empty($product['comment'])){ ?>
                                            <div id="comment-checkout">
                                                "<?php echo $product['comment']; ?>"
                                            </div>
                                            <?php } ?>
                                            
                                        </div>
                                        
                                        <?php foreach ($product['option'] as $option) { ?>
                                        <br />
                                        &nbsp;<small> - <?php echo $option['name']; ?>: <?php echo $option['value']; ?></small>
                                        <?php } ?>
                                        
                                    
                                    </div>
                                    
                                    <div id="total-con">
                                        <a id="remove-from-cart" class="tooltip" title="<?php echo $tip_remove; ?>" ><i class="fa fa-trash-o"></i></a>
                                        
                                        <span id="pro-qty" data-qty="<?php echo $product['quantity']; ?>">
                                        <?php if($product['quantity'] == 1) { ?>
                                        <?php echo $text_one_meal; ?>
                                        <?php } else { ?>
                                        <?php echo $product['quantity']." ".$text_meals; ?>
                                        <?php } ?>
                                        </span>
                                        
                                        <span id="pro-total" data-price="<?php echo $product['price']; ?>">
                                        <?php echo $product['total']; ?>  
                                        </span>
                                    </div>
                                    
                                    <div style="clear: both"></div>
                                    
                                    
                                </td>
                            </tr>
        <?php } ?>
    </tbody>
</table>



<?php } ?>

<div class="supercheckout-checkout-content"></div>

<?php if (!isset($redirect)) { ?>

<table class="supercheckout-summary">
    <tbody>
        
        <?php
        $total_product = 0;
        
        foreach ($products as $product) {   
            $total_product += $product['quantity'];
        }
       
        ?>
        
        <input type="hidden" id="total-product" value="<?php echo $total_product; ?>" />
        <input type="hidden" id="delivery-date-time" value="<?php echo $delivery_date_time; ?>" />
        <input type="hidden" id="has-coupon" value="<?php echo $hasCoupon; ?>" />
        
        <?php foreach ($vouchers as $voucher) { ?>
        <tr>
            <td class="supercheckout-name"><?php echo $voucher['description']; ?></td>
            <td class="supercheckout-qty">1</td>
            <td class="supercheckout-total"><?php echo $voucher['amount']; ?></td>
            <td class="supercheckout-total"><?php echo $voucher['amount']; ?></td>
        </tr>
        <?php } ?>
    </tbody>
</table>
<table class="supercheckout-totals">
    <tbody>
        <?php foreach ($totals as $total) {
            if($total['code'] == 'sub_total'){
                //continue;
            }
            
            ?>
        <tr style="display:none;">
            <td class="title <?php echo $total['code']; ?>" ><?php echo $total['title'];if($total['code'] == 'shipping'){echo "<br/><span class='pro-address'>".$text_delivery_address_demo."</span>";}; if($total['code']=='voucher' || $total['code']=='coupon'){echo'<a href="javascript://" id="'.$total['code'].'"  onclick="redeem(this.id);" ><div title="Redeem" class="removeProduct"></div></a></td>';} ?></td>
            <td class="value <?php echo $total['code']; ?>"><span class="price" id="homeals-<?php echo $total['code']; ?>"><?php echo $total['text']; ?></span> </td>
        </tr>
        <?php } ?>
        <tr style="display:<?php if($logged){ if($settings['option']['logged']['cart']['option']['voucher']['display']){ echo' ';}else{ echo'none';} }else{ if($settings['option']['guest']['cart']['option']['voucher']['display']){ echo' ';}else{ echo'none';}} ?>;">
            <td class="title"><b><?php echo $text_voucher_code; ?></b></td>
            <td class="value"><input  id="voucher_code" name="voucher" type="text" class="voucherText">
                <input type="hidden" value="voucher" name="next">
                <input id="button-voucher" type="button" onClick="if(window.voucherBlur==true){ callVoucher(); }" class="orangebuttonapply" value="Apply">
            </td>
        </tr>
    </tbody>
</table>
<script type="text/javascript">

    $(document).ready(function(){
        $('.tooltip').tooltipster({theme: 'tooltipster-light' });
        
    <?php if($layout_name=='three-column'){ ?>
        var a = $('#supercheckout-columnleft').height();    
        var d = $('#supercheckout_login_option_box').height();  
        var e = a-d;
        $('#columnleft-1').css('min-height', e + 'px');
        $('#columnleft-3').css('min-height', e + 'px');


    <?php }elseif($layout_name=='two-column'){ ?>
        var a = $('#supercheckout-columnleft').height();
        var d = $('#supercheckout_login_option_box').height();  
        var e = a-d;    
        $('#columnleft-1').css('min-height', e + 'px');
        var b = $('#column-1-inside').height();
        var c = $('#column-2-inside').height();
        if(c > b) {
            $('#column-1-inside').css('min-height', c + 'px');
        } else {
            $('#column-2-inside').css('min-height', b + 'px');
        }
    <?php } ?>
    });
    //$('[data-toggle="popover"]').popover();
</script>
<?php } else { ?>
<script type="text/javascript"><!--
    location = '<?php echo $redirect; ?>';
    //--></script>
<?php } ?>