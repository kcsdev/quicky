<div id="review-box" >
    
    <div id="close-msg" >X</div>
    
    <div class="review-meal-con">
    <div class="review-meal-name"><?php echo $msg_new_headline; ?></div>
    
    <div class="review-meal-text">
	
	<form class="dialog">
		<textarea class="review-textarea" name="ms-sellercontact-text" id="ms-sellercontact-text" placeholder="<?php echo $msg_placeholder; ?>" onfocus="this.placeholder = ''" onblur="this.placeholder = '<?php echo $msg_placeholder; ?>'"></textarea>
		<input type="hidden" name="seller_id" value="<?php echo $seller_id; ?>" />
		<input type="hidden" name="product_id" value="<?php echo $product_id; ?>" />
	</form>
	
    </div>
</div>
    
    <div class="action-buttons-con">
    <button id="send-pm"><?php echo $msg_send; ?></button>
    
    <div id="warn-place"></div>
    
    </div>
      
</div>