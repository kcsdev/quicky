<?php echo $header; ?><?php echo $column_left; ?><?php echo $column_right; ?>

<div id="content" class="ms-account-conversation">
	<?php echo $content_top; ?>

	<h1 id="inbox-text">
		תיבת דואר
		<?php if($unread_num) { ?> 
		<span class="unread-span">(<?php echo $unread_num; ?> לא נקרא)</span>
		<?php } ?>
	</h1>
	
	
	<?php foreach($conversations as $conversation) { ?>
	<div class="conversation">
		<div class="messages-header"><div class="review">
		<div class="review-header">
			<div class="avatarcon"><img class="avatar" src="image/no-avatar-women.png" data-src="<?php echo $conversation['image']; ?>" class="lazy"></div>
			<div class="author"><?php echo $conversation['with']; ?></div>
		</div>
		</div></div>
		
		<div class="conversation-body">
			<div class="conversation-text <?php echo ($conversation['status'] == "read") ? '' : 'notread' ?>">
				<a href="<?php echo $conversation['conv_link']; ?>"><?php echo $conversation['title']; ?></a>
			</div>
			<!--<div class="conversation-meal">
				<?php echo $conversation['product_id']; ?>
			</div>-->
			<div class="conversation-date">
				<?php echo $conversation['last_message_date']; ?>
			</div>
		</div>
	</div>
	<?php } ?>

	<?php echo $content_bottom; ?>
</div>
<?php echo $footer; ?>
