<?php echo $header; ?><?php echo $column_left; ?><?php echo $column_right; ?>

<div id="content" class="ms-account-order">
	<?php echo $content_top; ?>
	
	<h1 class="title"><?php echo $text_my_orders; ?></h1>
	
	<div id="seller-tabs" class="htabs">
		<a href="#tab-my-orders">
			<?php echo $text_recived_orders; ?>
			<img src="catalog/view/theme/<?php echo $this->config->get('config_template') ?>/image/homeals/spitz.png" />
		</a>
		<a href="#tab-history" id="historybutttn">
			<?php echo $text_history_orders; ?>
			<img src="catalog/view/theme/<?php echo $this->config->get('config_template') ?>/image/homeals/spitz.png" />
		</a>
	</div>
	
	<div id="tab-history" class="tab-content" style="display: none">
		
		<div class="filtter-con" id="send-box">
			<div class="select-con">
				<select name="option[date]" id="historyday">
					<option <?php echo (isset($_GET['sh']) && $_GET['sh'] == -30) ? "selected='selected'" : "" ?> value="-30"> 30 הימים הקודמים </option>
					<option <?php echo (!isset($_GET['sh']) || $_GET['sh'] == -7) ? "selected='selected'" : "" ?> value="-7"> 7 הימים הקודמים </option>
				</select>
		    </div>
			
			<div class="dtosum" style="display: none">
				<a class="selected">מפורט</a>
				<a>סיכום</a>
			</div>
		</div>
		<hr />
		
		<?php foreach($history_orders as $history_order_date => $history_order_s) { ?> 
				
		<table class="list invisible">
			<thead>
				<tr>
					<td class="tiny"><?php echo $cul_num; ?></td>
					<td class="boxx"><?php echo $cul_who_cook; ?></td>
					<td><?php echo $cul_meals; ?></td>
					<td class="medium"><?php echo $cul_time_delivery; ?></td>
					<td class="medium"><?php echo $cul_total; ?></td>
					<td class="large"><?php echo $cul_status; ?></td>
				</tr>
			</thead>
		</table>
		
		
		<table class="list seprate">
			<tbody>
				<?php foreach($history_order_s as $history_order) { ?> 
				<tr>
					<td class="tiny">
					<a href="<?php echo $history_order['order_details_link']; ?>" ><?php echo $history_order['order_id']; ?></a>
					</td>
					<td class="boxx">
						<?php
							if($history_order['cook_name'] == "בשלנים שונים"){
						?>
						<img src="<?php echo $history_order['cook_avatar']; ?>" />
						<div><?php echo $history_order['cook_name']; ?></div>
						<?php } else { ?>
						<a class="button-pm ajax_msg" href="index.php?route=seller/catalog-seller/jxRenderContactDialog&seller_id=<?php echo $cook_id; ?>">
						<img src="<?php echo $history_order['cook_avatar']; ?>" />
						<div><?php echo $history_order['cook_name']; ?></div>
						</a>
						<?php } ?>
					</td>
					<td class="text">
						<?php
						
						$i = 0;
						
						foreach($history_order['meals'] as $meal) {
							
							//if ($i == 3) break;
							
							$i++;
							
						?>
							
						<?php
							$side_text = $meal['quantity']." ".$meal['full_name'];
							if(!empty($meal['sides'])){
								
								$side_text .= " + ";
								$k=0;
								foreach($meal['sides'] as $side) {
									if($k) $side_text .= ", ו";
									$side_text .= $side['name'];
									$k++;
								}
							}
						?>
							
							
						<div class="meals-name-text tooltip" title="<?php echo $side_text; ?>">
							<?php echo $meal['quantity']." ".$meal['name']; ?>
							
							
						</div>
							<?php if(!empty($meal['comment'])){ ?>
							<div class="meal-comment tooltip" title="<?php echo $meal['comment']; ?>"></div>
							<?php } ?>
						<br>
						<?php } ?>
					</td>
					<td class="medium">
					<?php echo $history_order['order_date']; ?><br>
					<?php echo $history_order['order_time']; ?></td>
					<td class="medium"><?php echo $history_order['total_profit']; ?></td>
					<td class="large">
					<?php echo $history_order['status_text']; ?>
					<br>
					<a class="order-details-link" href="<?php echo $history_order['order_details_link']; ?>" ><?php echo $text_order_details_link; ?></a>
					</td>
				</tr>
				<?php } ?>
			</tbody>
		</table>
		
		
		
		<?php } ?>
	</div>
	
	<div id="tab-my-orders" class="tab-content">
		
		
		<?php foreach($orders as $order_date => $order_s) { ?> 
		
		<table class="list invisible">
			<thead>
				<tr>
					<td class="tiny"><?php echo $cul_num; ?></td>
					<td class="boxx"><?php echo $cul_who_cook; ?></td>
					<td><?php echo $cul_meals; ?></td>
					<td class="medium"><?php echo $cul_time_delivery; ?></td>
					<td class="medium"><?php echo $cul_total; ?></td>
					<td class="large"><?php echo $cul_status; ?></td>
				</tr>
			</thead>
		</table>
		
		
		<table class="list seprate">
			<tbody>
				<?php foreach($order_s as $order) { ?> 
				<tr>
					<td class="tiny">
					<a href="<?php echo $order['order_details_link']; ?>" ><?php echo $order['order_id']; ?></a>
					</td>
					<td class="boxx">
						<?php
							$cook_id_info = json_decode($order['cook_id']);
							foreach($cook_id_info as $key => $id){
								$cook_id = $key ;
							}
							
							if($order['cook_name'] == "בשלנים שונים"){
						?>
						<img src="<?php echo $order['cook_avatar']; ?>" />
						<div><?php echo $order['cook_name']; ?></div>
						<?php } else { ?>
						<a class="button-pm ajax_msg" href="index.php?route=seller/catalog-seller/jxRenderContactDialog&seller_id=<?php echo $cook_id; ?>">
						<img src="<?php echo $order['cook_avatar']; ?>" />
						<div><?php echo $order['cook_name']; ?></div>
						</a>
						<?php } ?>
					</td>
					<td class="text">
						<?php
						
						$i = 0;
						
						foreach($order['meals'] as $meal) {
							
							//if ($i == 3) break;
							
							$i++;
							
							?>
						<?php
							$side_text = $meal['quantity']." ".$meal['full_name'];
							if(!empty($meal['sides'])){
								
								$side_text .= " + ";
								$k=0;
								foreach($meal['sides'] as $side) {
									if($k) $side_text .= ", ו";
									$side_text .= $side['name'];
									$k++;
								}
							}
						?>
							
							
						<div class="meals-name-text tooltip" title="<?php echo $side_text; ?>">
							<?php echo $meal['quantity']." ".$meal['name']; ?>
							
							
						</div>
							<?php if(!empty($meal['comment'])){ ?>
							<div class="meal-comment tooltip" title="<?php echo $meal['comment']; ?>"></div>
							<?php } ?>
						<br>
						<?php } ?>
					</td>
					<td class="medium">
						<?php echo $order['order_date']; ?><br>
						<?php echo $order['order_time']; ?>
					</td>
					<td class="medium"><?php echo $order['total_profit']; ?></td>
					<td class="large">
					<?php echo $order['status_text']; ?>
					<br>
					<a class="order-details-link" href="<?php echo $order['order_details_link']; ?>" ><?php echo $text_order_details_link; ?></a>
					</td>
				</tr>
				<?php } ?>
			</tbody>
		</table>
		
		
		
		<?php } ?>
	
	</div>
	
	<?php echo $content_bottom; ?>
</div>

<script>
	$('html, body').addClass("noScroll");
	$('.loading-screen').show().addClass("opacityone");
	
	$(window).load(function() {
		$('html, body').removeClass("noScroll");
		$(".loading-screen").fadeOut("slow").removeClass("opacityone");
	});
	
	$(function() {
		
		$('#seller-tabs a').tabs();
		
		$('#seller-tabs a').on("click", function() {
			$('#column-right').height($('#content').height() + $('#footer-main').height());
		});
		
		
		$("#day").on("change",function() {
			$('html, body').addClass("noScroll");
			$('.loading-screen').show().addClass("opacityone");
			
			location = "?route=account/my-orders&showtime="+$("#day").val();
			});
		
		$("#historyday").on("change",function() {
			$('html, body').addClass("noScroll");
			$('.loading-screen').show().addClass("opacityone");
			
			location = "?route=account/my-orders&sh="+$("#historyday").val();
			});
		
		<?php if(isset($_GET['sh']) || isset($_GET['shs'])) { ?>
			$('#historybutttn').trigger("click");
		<?php } ?> 
		
	});
</script>

<?php echo $footer; ?>