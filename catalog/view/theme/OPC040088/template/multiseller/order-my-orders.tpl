<?php echo $header; ?><?php echo $column_left; ?><?php echo $column_right; ?>

<div id="content" class="ms-account-order">
	<?php echo $content_top; ?>
	
	<h1 class="title"><?php echo $text_your_orders; ?></h1>
	
	<div id="seller-tabs" class="htabs">
		<a href="#tab-my-orders">
			<?php echo $text_recived_orders; ?>
			<img src="catalog/view/theme/<?php echo $this->config->get('config_template') ?>/image/homeals/spitz.png" />
		</a>
		<a href="#tab-history" id="historybutttn">
			<?php echo $text_history_orders; ?>
			<img src="catalog/view/theme/<?php echo $this->config->get('config_template') ?>/image/homeals/spitz.png" />
		</a>
	</div>
	
	<div id="tab-history" class="tab-content" style="display: none">
		
		<div class="filtter-con" id="send-box">
			<div class="select-con">
				<select name="option[date]" id="historyday">
					<option <?php echo (isset($_GET['sh']) && $_GET['sh'] == -30) ? "selected='selected'" : "" ?> value="-30"> 30 הימים הקודמים </option>
					<option <?php echo (!isset($_GET['sh']) || $_GET['sh'] == -7) ? "selected='selected'" : "" ?> value="-7"> 7 הימים הקודמים </option>
				</select>
		    </div>
			
			<div class="dtosum" style="display: none">
				<a class="selected">מפורט</a>
				<a>סיכום</a>
			</div>
		</div>
		<hr />
		
		
		<?php if($this->customer->getId()== 229) { ?>
		<h1 class="order-title">חישוב סיכום</h1><br>
		
		<form name="input" action="index.php" method="get">
			
			<input type="hidden" value="seller/account-my-orders" name="route" />
			
			<label for="ckid">בשלן</label>
			
			<select name="ckid" >
				<option disabled="disabled" selected="selected">בחר בשלן</option>
			<?php foreach($all_cooks as $cook) { ?>
				<option value="<?php echo $cook['id']; ?>" <?php echo (isset($_GET['ckid']) && ($_GET['ckid'] == $cook['id'])) ? "selected=selected" : ""; ?>><?php echo $cook['name']; ?></option>
			<?php } ?>
			</select>
			
			<label for="shs">מתאריך</label>
			<input type="text" name="shs" value="<?php echo isset($_GET['shs']) ? $_GET['shs'] : ""; ?>" />
			
			<label for="she">עד תאריך</label>
			<input type="text" name="she" value="<?php echo isset($_GET['she']) ? $_GET['she'] : ""; ?>" />
			
			<input type="submit" value="שלח">
			
		</form>
		<br><br><br>
		<?php if(isset($_GET['shs'])) { ?>
		<table class="list invisible">
			<thead>
				<tr>
					<td class="tiny"><?php echo $cul_num; ?></td>
					<td class="boxx"><?php echo $cul_who; ?></td>
					<td></td>
					<td class="medium">סה"כ מנות</td>
					<td class="medium">סה"כ סכום</td>
					<td class="large"></td>
				</tr>
			</thead>
		</table>
		<table class="list seprate">
			<tbody>
				<tr>
					<td class="tiny"><?php echo $_GET['ckid']; ?></td>
					<td class="boxx">
						<?php foreach($all_cooks as $cook) {
							if($_GET['ckid'] == $cook['id']){
							?>
							
						<img src="<?php echo $cook['img']; ?>" />
						<div><?php echo $cook['name']; ?></div>
						
						
						<?php } } ?>
					</td>
					<td class="text">מציג ארוחות מתאריך <?php echo $_GET['shs']; ?> עד <?php echo $_GET['she']; ?></td>
					<td class="medium"><?php echo $total_meals; ?></td>
					<td class="medium"><?php echo $total_cost; ?></td>
					<td class="large"></td>
				</tr>
			</tbody>
		</table>
		
		<?php } } ?>
		
		<?php foreach($history_orders as $history_order_date => $history_order_s) { ?> 
		
		<h1 class="order-title"><?php echo strftime("%A",strtotime($history_order_date)); ?>, <?php echo strftime("%e ב%B",strtotime($history_order_date)); ?></h1>
		<span class="numofordrs">(<?php echo count($history_order_s); ?> הזמנות)</span>
		
		<table class="list invisible">
			<thead>
				<tr>
					<td class="tiny"><?php echo $cul_num; ?></td>
					<td class="boxx"><?php echo $cul_who; ?></td>
					<td><?php echo $cul_meals; ?></td>
					<td class="medium"><?php echo $cul_time; ?></td>
					<td class="medium"><?php echo $cul_total; ?></td>
					<td class="large"><?php echo $cul_status; ?></td>
				</tr>
			</thead>
		</table>
		
		
		<table class="list seprate">
			<tbody>
				<?php foreach($history_order_s as $history_order) { ?> 
				<tr>
					<td class="tiny"><a href="<?php echo $history_order['order_details_link']; ?>" ><?php echo $history_order['order_id']; ?></a></td>
					<td class="boxx">
						<a class="button-pm ajax_msg" href="index.php?route=seller/catalog-seller/jxRenderContactDialog&seller_id=<?php echo $history_order['customer_id']; ?>">
						<img src="<?php echo $history_order['customer_avatar']; ?>" class="tooltip" title="<?php echo $history_order['customer_contact']; ?>" />
						<div><?php echo $history_order['customer_name']; ?></div>
						</a>
					</td>
					<td class="text">
						<?php
						
						$i = 0;
						
						foreach($history_order['meals'] as $meal) {
							
							//if ($i == 3) break;
							
							$i++;
							
						?>
							
						<?php
							$side_text = $meal['quantity']." ".$meal['full_name'];
							if(!empty($meal['sides'])){
								
								$side_text .= " + ";
								$k=0;
								foreach($meal['sides'] as $side) {
									if($k) $side_text .= ", ו";
									$side_text .= $side['name'];
									$k++;
								}
							}
						?>
							
							
						<div class="meals-name-text tooltip" title="<?php echo $side_text; ?>">
							<?php echo $meal['quantity']." ".$meal['name']; ?>
							
							
						</div>
							<?php if(!empty($meal['comment'])){ ?>
							<div class="meal-comment tooltip" title="<?php echo $meal['comment']; ?>"></div>
							<?php } ?>
						<br>
						<?php } ?>
					</td>
					<td class="medium"><?php echo $history_order['cook_order_time']; ?></td>
					<td class="medium"><?php echo $history_order['total_profit']; ?></td>
					<td class="large">
						<?php echo $history_order['status_text']; ?>
						<br>
						<a class="order-details-link" href="<?php echo $history_order['order_details_link']; ?>" ><?php echo $text_order_details_link; ?></a>
					</td>
				</tr>
				<?php } ?>
			</tbody>
		</table>
		
		
		
		<?php } ?>
	</div>
	
	<div id="tab-my-orders" class="tab-content">
		
		<?php if(!empty($waiting_orders)) { ?>
		
		<table class="list" id="list-orders">
			<thead>
				<tr>
					<td colspan="6" class="list-title">
						<i class="waiting"></i><?php echo $text_wating_orders; ?>
						<span class="numofordrs">(<?php echo $waiting_orders_num; ?> הזמנות)</span>
					</td>
				</tr>
				<tr>
					<td class="tiny"><?php echo $cul_num; ?></td>
					<td class="boxx"><?php echo $cul_who; ?></td>
					<td><?php echo $cul_meals; ?></td>
					<td class="medium"><?php echo $cul_time; ?></td>
					<td class="medium"><?php echo $cul_total; ?></td>
					<td class="large"><?php echo $cul_status; ?></td>
				</tr>
			</thead>
			
			<tbody>
				
				<tr>
					<?php foreach($waiting_orders as $order) { ?> 
				<tr>
					<td class="tiny"><a href="<?php echo $order['order_details_link']; ?>" ><?php echo $order['order_id']; ?></a></td>
					<td class="boxx">
						<a class="button-pm ajax_msg" href="index.php?route=seller/catalog-seller/jxRenderContactDialog&seller_id=<?php echo $order['customer_id']; ?>">
						<img src="<?php echo $order['customer_avatar']; ?>" class="tooltip" title="<?php echo $order['customer_contact']; ?>" />
						<div><?php echo $order['customer_name']; ?></div>
						</a>
					</td>
					<td class="text">
						<?php
						
						$i = 0;
						
						foreach($order['meals'] as $meal) {
							
							//if ($i == 3) break;
							
							$i++;
							
							?>
						<?php
							$side_text = $meal['quantity']." ".$meal['full_name'];
							if(!empty($meal['sides'])){
								
								$side_text .= " + ";
								$k=0;
								foreach($meal['sides'] as $side) {
									if($k) $side_text .= ", ו";
									$side_text .= $side['name'];
									$k++;
								}
							}
						?>
							
							
						<div class="meals-name-text tooltip" title="<?php echo $side_text; ?>">
							<?php echo $meal['quantity']." ".$meal['name']; ?>
							
							
						</div>
							<?php if(!empty($meal['comment'])){ ?>
							<div class="meal-comment tooltip" title="<?php echo $meal['comment']; ?>"></div>
							<?php } ?>
						<br>
						<?php } ?>
					</td>
					<td class="medium"><?php echo $order['order_date_time']."<br>".$order['cook_order_time']; ?></td>
					<td class="medium"><?php echo $order['total_profit']; ?></td>
					<td class="large">
						<a class="order-details-link" href="<?php echo $order['aprove_link']; ?>"><?php echo "אשר הזמנה"; ?></a>
						<br>
						<a class="order-details-link" href="<?php echo $order['order_details_link']; ?>" ><?php echo $text_order_details_link; ?></a>
					</td>
				</tr>
				<?php } ?>
				</tr>
				
			</tbody>
		</table>
		
		<hr />
		
		<?php } ?>
		
		
		<div class="filtter-con" id="send-box">
			<div class="select-con">
				<select name="option[date]" id="day">
					<option <?php echo (!isset($_GET['showtime']) || $_GET['showtime'] == 0) ? "selected='selected'" : "" ?>  value="0"> היום </option>
					<option <?php echo (isset($_GET['showtime']) && $_GET['showtime'] == 1) ? "selected='selected'" : "" ?>  value="1"> מחר </option>
					<option <?php echo (isset($_GET['showtime']) && $_GET['showtime'] == 7) ? "selected='selected'" : "" ?>  value="7"> 7 הימים הבאים </option>
					<!--<option <?php echo (isset($_GET['showtime']) && $_GET['showtime'] == 30) ? "selected='selected'" : "" ?> value="30"> 30 הימים הבאים </option>-->
				</select>
		    </div>
			
			<div class="dtosum" style="display: none">
				<a class="selected">מפורט</a>
				<a>סיכום</a>
			</div>
		</div>
		<hr />
		
		
		<?php foreach($orders as $order_date => $order_s) { ?> 
		
		<h1 class="order-title"><?php echo strftime("%A",strtotime($order_date)); ?>, <?php echo strftime("%e ב%B",strtotime($order_date)); ?></h1>
		<span class="numofordrs">(<?php echo count($order_s); ?> הזמנות)</span>
		
		<table class="list invisible">
			<thead>
				<tr>
					<td class="tiny"><?php echo $cul_num; ?></td>
					<td class="boxx"><?php echo $cul_who; ?></td>
					<td><?php echo $cul_meals; ?></td>
					<td class="medium"><?php echo $cul_time; ?></td>
					<td class="medium"><?php echo $cul_total; ?></td>
					<td class="large"><?php echo $cul_status; ?></td>
				</tr>
			</thead>
		</table>
		
		
		<table class="list seprate">
			<tbody>
				<?php foreach($order_s as $order) { ?> 
				<tr>
					<td class="tiny"><a href="<?php echo $order['order_details_link']; ?>" ><?php echo $order['order_id']; ?></a></td>
					<td class="boxx">
						<a class="button-pm ajax_msg" href="index.php?route=seller/catalog-seller/jxRenderContactDialog&seller_id=<?php echo $order['customer_id']; ?>">
						<img src="<?php echo $order['customer_avatar']; ?>" class="tooltip" title="<?php echo $order['customer_contact']; ?>" />
						<div><?php echo $order['customer_name']; ?></div>
						</a>
					</td>
					<td class="text">
						<?php
						
						$i = 0;
						
						foreach($order['meals'] as $meal) {
							
							//if ($i == 3) break;
							
							$i++;
							
							?>
						<?php
							$side_text = $meal['quantity']." ".$meal['full_name'];
							if(!empty($meal['sides'])){
								
								$side_text .= " + ";
								$k=0;
								foreach($meal['sides'] as $side) {
									if($k) $side_text .= ", ו";
									$side_text .= $side['name'];
									$k++;
								}
							}
						?>
							
							
						<div class="meals-name-text tooltip" title="<?php echo $side_text; ?>">
							<?php echo $meal['quantity']." ".$meal['name']; ?>
							
							
						</div>
							<?php if(!empty($meal['comment'])){ ?>
							<div class="meal-comment tooltip" title="<?php echo $meal['comment']; ?>"></div>
							<?php } ?>
						<br>
						<?php } ?>
					</td>
					<td class="medium"><?php echo $order['cook_order_time']; ?></td>
					<td class="medium"><?php echo $order['total_profit']; ?></td>
					<td class="large">
					
					<?php echo $order['status_text']; ?>
					<br>
					<a class="order-details-link" href="<?php echo $order['order_details_link']; ?>" ><?php echo $text_order_details_link; ?></a>
						
					</td>
				</tr>
				<?php } ?>
			</tbody>
		</table>
		
		
		
		<?php } ?>
	
	</div>
	
	<?php echo $content_bottom; ?>
</div>

<script>
	$('html, body').addClass("noScroll");
	$('.loading-screen').show().addClass("opacityone");
	
	$(window).load(function() {
		$('html, body').removeClass("noScroll");
		$(".loading-screen").fadeOut("slow").removeClass("opacityone");
	});
	
	$(function() {
		
		$('#seller-tabs a').tabs();
		
		$('#seller-tabs a').on("click", function() {
			$('#column-right').height($('#content').height() + $('#footer-main').height());
		});
		
		
		$("#day").on("change",function() {
			$('html, body').addClass("noScroll");
			$('.loading-screen').show().addClass("opacityone");
			
			location = "?route=seller/account-my-orders&showtime="+$("#day").val();
			});
		
		$("#historyday").on("change",function() {
			$('html, body').addClass("noScroll");
			$('.loading-screen').show().addClass("opacityone");
			
			location = "?route=seller/account-my-orders&sh="+$("#historyday").val();
			});
		
		<?php if(isset($_GET['sh']) || isset($_GET['shs'])) { ?>
			$('#historybutttn').trigger("click");
		<?php } ?>
		
	});
</script>

<?php echo $footer; ?>