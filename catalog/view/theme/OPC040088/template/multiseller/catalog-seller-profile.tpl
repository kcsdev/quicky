<?php echo $header; ?><?php echo $column_left; ?><?php echo $column_right; ?>

<div id="content" class="ms-catalog-seller-profile">
	<?php echo $content_top; ?>
	
	<div class="ms-sellerprofile">
		<div class="seller-data">
			<div class="avatar-box">
				<img src="image/no-avatar-women.png" data-src="<?php echo $customer['image']; ?>" />
                <h2><?php echo $customer['name']; ?></h2>
                <p><?php echo $customer['city']; ?></p>
				<?php foreach($seller['badges'] as $badge) { ?>
					<img src="image/no-avatar-women.png" data-src="<?php echo $badge['image']; ?>" title="<?php echo $badge['description']; ?>" />
				<?php } ?>
                 <div class="speciallities">
                    <h3><?php echo $text_sp ?></h3>
                    <p><?php echo $customer['sp'] ?></p>
                </div>
			
			<?php if ((!$this->customer->getId() || ($this->customer->getId() && $this->customer->getId() != $seller['seller_id'])) && ($this->config->get('msconf_enable_private_messaging') == 2 || ($this->config->get('msconf_enable_private_messaging') == 1 && $this->customer->getId()))) { ?>
				<a id="button-pm" href="index.php?route=seller/catalog-seller/jxRenderContactDialog&seller_id=<?php echo $seller_id; ?>" class="ajax_msg"><?php echo $text_contactme ?></a>
			<?php } else { ?>
				<a onclick="$('#open_login').trigger('click')"><?php echo $text_contactme ?></a>
			<?php } ?>
		</div>
			
			<div class="info-box">
				
				
			<?php if (false) { ?>
			
				<?php if ($seller['country']) { ?>
					<p><b><?php echo $ms_catalog_seller_profile_country; ?></b> <?php echo $seller['country']; ?></p>
				<?php } ?>
				
				<?php if ($seller['company']) { ?>
					<p><b><?php echo $ms_catalog_seller_profile_company; ?></b> <?php echo $seller['company']; ?></p>
				<?php } ?>
				
				<?php if ($seller['website']) { ?>
					<p><b><?php echo $ms_catalog_seller_profile_website; ?></b> <?php echo $seller['website']; ?></p>
				<?php } ?>
				
				<?php
					if ($total_votes % 10 == 1) {
						$ms_rating_word = $ms_catalog_seller_profile_ratings_singular;
					} else {
						$ms_rating_word = $ms_catalog_seller_profile_ratings_plural;
					}
				?>
				
				<p><b><?php echo $ms_catalog_seller_profile_totalsales; ?></b> <?php echo $seller['total_sales']; ?></p>
				<p><b><?php echo $ms_catalog_seller_profile_totalproducts; ?></b> <?php echo $seller['total_products']; ?></p>
				<p><b><?php echo $ms_catalog_seller_profile_rating_overall; ?></b> <?php echo $avg_overall; ?> (<?php echo $total_votes . " " . $ms_rating_word; ?>)</p>
				<p><b><?php echo $ms_catalog_seller_profile_rating_communication; ?></b> <?php echo $avg_communication; ?></p>
				<p><b><?php echo $ms_catalog_seller_profile_rating_honesty; ?></b> <?php echo $avg_honesty; ?></p>
				
			<?php } ?>
		
                <div class="tab">
                    <p><?php echo $review_rating; ?>%<img src="catalog/view/theme/<?php echo $this->config->get('config_template') ?>/image/homeals/positive.png" align="absmiddle" /></p>
                    <p><?php echo $text_rating ?></p>
                </div>
				<?php if(false) { ?>
                <div class="tab">
                    <p>17<img src="catalog/view/theme/<?php echo $this->config->get('config_template') ?>/image/homeals/cookstar.png" align="absmiddle" /></p>
                    <p><?php echo $text_recommended ?></p>
                </div>
				<?php } else { ?>
				<div class="tab-split"></div>
				<?php } ?>
                <div class="tab last">
                    <p><a class="tablink"><?php echo $review_total; ?></a><img src="catalog/view/theme/<?php echo $this->config->get('config_template') ?>/image/homeals/cookcomment.png" align="absmiddle" /></p>
                    <p><?php echo $text_cookreviews ?></p>
                </div>
                <div class="clear"></div>
			</div>
		</div>
			
		<div class="seller-description">
			<div class="description-header">
			<div class="right">
				
				<?php
				function curPageURL() {
					$pageURL = 'http';
					if (isset($_SERVER["HTTPS"])) { if ($_SERVER["HTTPS"] == "on") {$pageURL .= "s";}; }
					$pageURL .= "://";
					if ($_SERVER["SERVER_PORT"] != "80") {
					 $pageURL .= $_SERVER["SERVER_NAME"].":".$_SERVER["SERVER_PORT"].$_SERVER["REQUEST_URI"];
					} else {
					 $pageURL .= $_SERVER["SERVER_NAME"].$_SERVER["REQUEST_URI"];
					}
					$pageURL = str_replace("&", urlencode("&"), html_entity_decode($pageURL));
					return $pageURL;
				   }
				?>
				
				
				<p class="share-charm">
					<a id="share-facebook" class="tooltip" title="<?php echo $sface; ?>"
						href="https://www.facebook.com/sharer/sharer.php?u=<?php echo curPageURL()?>"
						onclick="javascript:window.open(this.href, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=300,width=600');return false;"
						target="_blank"></a>
					<a id="share-twitter" class="tooltip" title="<?php echo $stwit; ?>"
						href="https://twitter.com/home?status=%D7%91%D7%A9%D7%9C%D7%9F%20%D7%9E%D7%93%D7%94%D7%99%D7%9D%20%D7%91-Homeals%20<?php echo curPageURL()?>" onclick="javascript:window.open(this.href, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=300,width=600');return false;" target="_blank"></a>
					<a id="share-mail" class="tooltip" title="<?php echo $smail; ?>" href="mailto:?subject=<?php echo $smail_sub; ?>&amp;body=<?php echo $smail_bud; ?>" target="_blank"></a>
				</p>
				
            </div>
            <h1><?php echo $text_aboutme ?></h1>
		</div>
		<div class="description"><div class="description_under">
                <span class="short">
				<?php //echo $seller['description']; ?>
				<?php
				$string = (mb_strlen($customer['profile_description'],"UTF-8") >= $description_max_size) ? mb_substr($customer['profile_description'],0,($description_max_size - 3),"utf-8").'...' : $customer['profile_description'];
				echo $string;
				?>
				</span>
				<span class="long">
				<?php echo nl2br($customer['profile_description']); ?>
				</span>
				<?php if(mb_strlen($customer['profile_description'],"UTF-8") >= $description_max_size) { ?>
				<br><a class="read-more"><?php echo $text_read_more ?></a>
				<a class="read-less"><?php echo $text_read_less ?></a>
				<?php } ?>
            </div></div>
            <div class="images">
                <?php
		$image_num = 0;
		foreach ($images as $image) {
			$image_num++;
			?>
			<div class="my-images<?php echo ($image_num == 1) ? " selected" : ""; ?>" id="<?php echo $image_num; ?>">
				<img src="<?php echo $image['popup']; ?>" align="absmiddle" />
				<div class="hide-next-image"></div>
				<div class="next-image" ><div class="next"></div></div>
				<div class="prev-image" ><div class="prev"></div></div>
			</div>
		<?php
		}
		?>
				
            </div>
            <div class="clear"></div>
		</div>
        <div class="clear"></div>
	</div>
	
	<div class="onethireds">
		<div id="bulk-order"> 
		<div class="bulk-order-header">
			<?php echo $title_bulk_order; ?>
		</div>
		<div class="bulk-order-body">
			<?php echo $text_bulk_order; ?>
		</div>
		</div>
	</div>
	
	<div class="twothireds">
	
	<div id="seller-tabs" class="htabs">
        <a href="#tab-products"><?php echo $text_mymeals; ?>
	<img src="catalog/view/theme/<?php echo $this->config->get('config_template') ?>/image/homeals/spitz.png" />
	</a>
	<?php if ($this->config->get('msconf_seller_comments_enable')) { ?><a href="#tab-comments" id="link-comments">
	<?php echo $text_cookreviews; ?>
	<img src="catalog/view/theme/<?php echo $this->config->get('config_template') ?>/image/homeals/spitz.png" />
	</a><?php } ?>
	</div>	
	
	<div id="tab-products" class="tab-content">
		<?php if ($seller['products']) { ?>
			<div class="product-grid-list">
				<ul class="product-list" id="product-list-grid">
				<?php foreach ($seller['products'] as $product) { ?>
				<li id="pid_<?php echo $product['product_id']; ?>">
	<div class="product-block">
	<div class="product-block-inner">
      <div class="image">
      <?php if ($product['thumb']) { ?>
      
	<?php if ($product['is_verified']) { ?>
	<div class="homaels-verified"></div>
	<?php } ?>
	
	<div class="pro_filter"></div>
	
	<?php if ($product['total_sides']) { ?>
		<div class="side-text-con" >
			<span class="side-text">
			<?php echo $product['total_sides']; ?>
			</span>
		</div>
	<?php } ?>
	
      <img src="image/no_image.jpg" data-src="<?php echo $product['thumb']; ?>" title="<?php echo $product['name']; ?>" alt="<?php echo $product['name']; ?>" /><?php } ?>
      
      	<div class="side-img-con-con" >
	    <?php
	    
	    $number_of_side_images = 1;
	    
	    foreach($product['sides'] as $side) { $number_of_side_images++; }
	    
	    ?>
	    
	    <div class="side-img-con" data-num="<?php echo $number_of_side_images; ?>">
		<div class="side-img selected" data-side-id="1">
		    <div class="pro_filter"></div>
		    <span class="side-name"><?php echo $product['name']; ?></span>
		    <img src="<?php echo $product['thumb']; ?>" />
		</div>
		<?php
		    
		    $side_img_id = 1;
		    
		foreach($product['sides'] as $side) {
		    
		    $side_img_id++;
		    
		?>
		<div class="side-img" data-side-id="<?php echo $side_img_id; ?>">
		    <div class="pro_filter"></div>
		    <span class="side-name">תוספת - <?php echo $side['name']; ?></span>
		    <img src="<?php echo $side['thumb']; ?>" />
		</div>
		<?php } ?>
	    </div>
	</div>
	
	<?php if($number_of_side_images > 1) { ?>
	<div class="side-arrows">
		
		
	    <div class="side-arrow-left"> <img src="image/data/left.png" /> </div>
		
	    <a class="center-href" href="<?php echo $product['href']; ?>"></a>
		
		
	    <div class="side-arrow-right"> <img src="image/data/right.png" /> </div>
	</div>
	<?php } else { ?>
	<div class="side-arrows">
	    <a class="full-href" href="<?php echo $product['href']; ?>"></a>
	</div>
	<?php } ?>
      
      </div>

      
		<div class="saleblock">
		<?php if (!$product['special']) { ?>
		<?php } else { ?>
		<span class="saleicon sale">Sale</span>         
		<?php } ?>
		</div>	
	<div style="clear: both"></div>
	  <div class="under-img-con">
		<div class="widget">
			<div class="widget-inner">
				<div class="top-inner">
					<img src="catalog/view/theme/<?php echo $this->config->get('config_template') ?>/image/homeals/vegeterian.png" />
				</div>
				<div class="bottom-inner">
					<span class="text"><?php echo $product['pro_cat']; ?></span>
				</div>
			</div>
			<div class="widget-inner">
				<div class="top-inner">
					<span class="numbercon"><?php echo $product['total_sides']; ?></span>
				</div>
				<div class="bottom-inner">
					<span class="text">Side dish</span>
				</div>
			</div>
			<div class="widget-inner">
				<div class="top-inner">
				
				<span class="pricecon">
				<?php if (!$product['special']) { ?>
				<?php echo $product['price']; ?>
				<?php } else { ?>
				<!-- <span class="price-old"><?php echo $product['price']; ?></span> --> <span class="price-new"><?php echo $product['special']; ?></span>
				<?php } ?>
				<?php if ($product['tax']) { ?>
				<br />
				<span class="price-tax"><?php echo $text_tax; ?> <?php echo $product['tax']; ?></span>
				<?php } ?>
				</span>	
					
				</div>
				<div class="bottom-inner">
					<span class="text">Per meal</span>
				</div>
			</div>
		</div>
		<div class="probox">
		<div class="namepro" style="margin-bottom: 10px;">
		    <span class="namecon">
			<a href="<?php echo $product['href']; ?>">
				<?php echo mb_truncate($product['name'],50); ?>
			</a>
		    </span>
		    <span class="pricecon">
			<?php if (!$product['special']) { ?>
			<?php echo $product['price']; ?>
			<?php } else { ?>
			<!-- <span class="price-old"><?php echo $product['price']; ?></span> --> <span class="price-new"><?php echo $product['special']; ?></span>
			<?php } ?>
			<?php if ($product['tax']) { ?>
			<br />
			<span class="price-tax"><?php echo $text_per_meal; ?> <?php //echo $product['tax']; ?></span>
			<?php } ?>
		    </span>
		</div>
		
		
		<div class="rating">
		<?php if($product['reviews'] != 0){ ?>
		    <div class="rating-scope" itemscope itemtype="http://schema.org/AggregateRating"><img itemprop="ratingValue" src="catalog/view/theme/<?php echo $this->config->get('config_template') ?>/image/stars-<?php echo $product['rating']; ?>.png" />&nbsp;&nbsp;<span itemprop="reviewCount" onclick="$('a[href=\'#tab-review\']').trigger('click');" ><?php echo $product['reviews']; ?> <?php echo $text_reviewsmin; ?></span>&nbsp;<?php echo (($product['reviews_text'] == 0) ? "" : "|"); ?>&nbsp;<a href="<?php echo $product['href']."#reviews_start"; ?>" class="review-write" <?php echo (($product['reviews_text'] == 0) ? "style='display:none'" : ""); ?>><img itemprop="ratingValue" src="catalog/view/theme/<?php echo $this->config->get('config_template') ?>/image/homeals/speachbubble.png" />&nbsp;<?php echo $product['reviews_text']; ?></a></div> 
		<?php } else { ?>
			
		<?php  } ?>
		    <div class="last-order-time"><?php echo $product['days_available']; ?></div>
			<script>
				var avaiabilityToday = <?php echo $product['avaiabilityToday']; ?>;
				
				if (avaiabilityToday) {
					$("#product-list-grid").prepend($("#pid_<?php echo $product['product_id']; ?>"));
				}
				
				
			</script>
		</div>
		
		<div style="clear: both"></div>
		</div>
		
		</div><!-- under-img-con - End -->
		<div class="underpro"></div>
		
	</div>
 </div>
    </li>
    <?php } ?>
  </ul>
			</div>
		<?php } ?>
	</div>
	
	<?php if ($this->config->get('msconf_seller_comments_enable')) { ?>
	<div id="tab-comments" class="tab-content">
		<?php if(false) { ?>
		<script type="text/javascript">
			$(function(){
                $('#tab-comments .pcForm').load('index.php?route=module/ms-comments/renderForm&seller_id=<?php echo $seller_id; ?>>');
				$('#tab-comments .pcComments').load('index.php?route=module/ms-comments/renderComments&seller_id=<?php echo $seller_id; ?>');
			});
		</script>
		
		<div class="reviews_icon">
            <img src="catalog/view/theme/<?php echo $this->config->get('config_template') ?>/image/homeals/reviews_icon.png" align="absmiddle" /> Reviews
        </div>
	        <?php } ?>

		<div class="product-info">
		<div id="review" class="review"></div>
		</div>
		<?php if(false) { ?>
		<div class="pcComments"></div>
		<div class="pcForm"></div>
		<?php } ?>
	</div>
	<?php } ?>
	</div>
	<?php echo $content_bottom; ?>
</div>

<script type="text/javascript">
	var image_num = <?php echo $image_num; ?>;
	
	function nextImage(image_id) {
		if (image_id == 0) {
			next_image =  image_num;
		} else if (image_id > image_num) {
			next_image = 1;
		} else {
			next_image = image_id;
		}
		
		return next_image;
	}
	
	function nextImage2(image_id,image_number) {
	
		if (image_id == 0) {
			next_image = image_number;
		} else if (image_id > image_number) {
			next_image = 1;
		} else {
			next_image = image_id;
		}
		
		return next_image;
	}
	
	$(function(){
		
		/**************************************************
		*
		*	new meal block with moving pictures
		*
		*************************************************/
		$(".side-arrow-right , .side-arrow-left").on("click",function() {
		
		$(this).parent().parent().find('.side-img-con').show();
		$(this).parent().parent().find('.side-text-con').hide();
		
		var image_num =  parseInt($(this).parent().parent().find(".side-img-con").data("num"),10);
		var curr_image_id = parseInt($(this).parent().parent().find(".side-img-con").find(".selected").data("side-id"),10);
		
		if($(this).hasClass("side-arrow-left")){
			var next_image_id = nextImage2(curr_image_id + 1,image_num);
		} else {
			var next_image_id = nextImage2(curr_image_id - 1,image_num);
		}
		
		$(this).parent().parent().find(".side-img.selected").removeClass("selected");
		$(this).parent().parent().find("[data-side-id='"+next_image_id+"']").addClass("selected");
		
		});
		
		// Cycle Side images
		$(".product-block").hover(function(){
		$(this).find('.side-arrows').show();
		
		}, function(){
		$(this).find('.side-arrows').hide();
		$(this).find('.side-img-con').hide();
		$(this).find('.side-text-con').show();
		});
		/*************************************************/
		
		
		
		
		
		$("a.tablink").on("click", function() {
			$("#link-comments").trigger("click");
			$("html, body").scrollTop($("#link-comments").offset().top - 90);
		});
		
		if( !( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) ) {
			$(".description_under").slimScroll({
				position: 'left',
				height: '310px'
			});	
		}
		
		var image_num = <?php echo $image_num; ?>;
		
		if (image_num == 1) {
			$('.next-image').hide();
			$('.prev-image').hide();
		}
		
		$('#review').load('index.php?route=product/product/review_seller&seller_id=<?php echo $seller_id; ?>');
		
		$('#seller-tabs a').tabs();
		
		$('.next-image').bind("click",function(){
			next_image_id = parseInt($(this).parent().attr("id"),10);
			$(this).parent().removeClass("selected");
			next_image = nextImage(next_image_id + 1);
			$(".images").find("#"+next_image).addClass("selected");
		});
		
		$('.prev-image').bind("click",function(){
			next_image_id = parseInt($(this).parent().attr("id"),10);
			$(this).parent().removeClass("selected");
			next_image = nextImage(next_image_id - 1);
			$(".images").find("#"+next_image).addClass("selected");
		});
		
		$('.read-more').bind("click",function(e){
			e.preventDefault();			
			$(".next-image").css("display","none");
			$(".images").animate({
				opacity: 1,
				width: "50"
			  }, 300,"swing", function() {
				$('.hide-next-image').show();
			  });
			
			$(".long").show();
			$(".short").hide();
			
			$(".description").animate({
				opacity: 1,
				width: "600"
			  }, 300,"swing", function() {
				$(".description").css("text-align","right");
				$(".description_under").css("padding-left","10px");
				$(".read-more").hide();
				$(".read-less").show();
			  });
		});
		
		$('.read-less').bind("click",function(e){
			e.preventDefault();
			$(".next-image").css("display","block");
			$(".images").animate({
				opacity: 1,
				width: "435"
			  }, 300,"swing", function() {
				$('.hide-next-image').hide();
			  });
			$(".description").animate({
				opacity: 1,
				width: "213"
			  }, 300,"swing", function() {
				$(".description").css("text-align","justify");
				$(".description_under").css("padding-left","0");
				$(".read-less").hide();
				$(".read-more").show();
				$(".short").show();
				$(".long").hide();
			  });
		});
		
	});
</script>

<?php echo $footer; ?>
