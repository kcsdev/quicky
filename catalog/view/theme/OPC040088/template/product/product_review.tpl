<?php if ($logged) { ?>
<div id="review-box-ajax" >
    
    <div class="close-btn" onclick="$.colorbox.close()" >X</div>
    
    <div class="review-meal-con">
    <div class="review-meal-name"><?php echo 'עזרו ל'.$username.' ולשאר הקהילה שלנו ודרגו את הארוחה שהזמנתם'; ?></div>
    
    <div class="review-meal-controler" id="meal-<?php echo $_GET['product_id']; ?>">
        
        <div class="main-dish dish-con" >
            <div class="meal-image">
                <img  src="<?php echo $review['image']; ?>"   />
            </div>
            <div class="meal-name"><?php echo $review['name']; ?></div>
            <div class="meal-star-rating">
                
                <select class="star-rating" name="star" id="<?php echo $_GET['product_id']; ?>">
                    <option value=""><?php echo$rate_word[0]; ?></option>
                    <option value="1"><?php echo$rate_word[1]; ?></option>
                    <option value="2"><?php echo$rate_word[2]; ?></option>
                    <option value="3"><?php echo$rate_word[3]; ?></option>
                    <option value="4"><?php echo$rate_word[4]; ?></option>
                    <option value="5"><?php echo$rate_word[5]; ?></option>
                </select>
                
            </div>
        </div>
        
        
        <?php foreach ($products as $side) { ?>
        <div class="side-dish dish-con" id="ajax_side_<?php echo $side['product_id']; ?>" style="display: none">
            <div class="meal-image">
                <img  src="<?php echo $side['image']; ?>"   />
            </div>
            <div class="meal-name"><?php echo $side['name']; ?></div>
            <div class="meal-star-rating">
                
                <select class="star-rating" name="star" id="<?php echo $side['product_id']; ?>">
                    <option value=""><?php echo$rate_word[0]; ?></option>
                    <option value="1"><?php echo$rate_word[1]; ?></option>
                    <option value="2"><?php echo$rate_word[2]; ?></option>
                    <option value="3"><?php echo$rate_word[3]; ?></option>
                    <option value="4"><?php echo$rate_word[4]; ?></option>
                    <option value="5"><?php echo$rate_word[5]; ?></option>
                </select>
                
            </div>
        </div>
        <?php } ?>
        
        <?php
        
        $i = 0;
        for ($i ; $i < $total_sides_int ; $i++) {
        ?>
        
        <div class="side-dish dish-con">
            <div class="select-side-dish">
                <select class="side-select-ajax" id="select">
                    <option value="0"><?php echo $text_choose_side; ?></option>
                    <?php foreach ($products as $side) { ?>
                    <option value="<?php echo $side['product_id']; ?>"><?php echo $side['name']; ?></option>
                    <?php } ?>
                </select>
            </div>
        </div>
        
        <?php   } ?>
        
        <div style="clear: both; border: 0;"></div>
        
    </div>
    
    <div class="review-meal-text">
        <textarea class="review-textarea" name="text" id="<?php echo $_GET['product_id']; ?>" placeholder="<?php echo $placeholder_meal; ?>" onfocus="this.placeholder = ''" onblur="this.placeholder = '<?php echo $placeholder_meal; ?>'"></textarea>    
    </div>
</div>
    
    <div class="action-buttons-con">
    <button id="submit-review"><?php echo $text_submit; ?></button>
    <a id="cancel-review"><?php echo $text_cancel; ?></a>
    </div>
      
</div>
<?php } ?>