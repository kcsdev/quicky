<?php echo $header; ?>
<?php echo $column_left; ?>
<?php echo $column_right; ?>

<div id="content">

<?php if ($success) { ?>
<div class="success-msg">
    <i class="fa fa-check"></i> <?php echo $success; ?>
    
    <span class="success-msg-close-me" onclick="$('.success-msg').slideToggle('fast')"><i class="fa fa-times"></i></span>
</div>
<?php } ?>


<?php if ($this->cart->hasProducts()) { ?>

    <div class="add-to-dorder"> 
	<h1 id="add-to-order-header"><?php echo $text_cook_page_heading ?></h1>
	<div class="text">
	    <span class="delivery"><?php echo $text_delivery; ?> <span id="cook-delivery" > <?php echo $cart_delivery_day.", ".$cart_delivery_time; ?> </span></span>
	    <span class="delivery-text"><?php echo $cart_delivery_text; ?></span>
	</div>
    </div>
    
<?php } ?>

<?php echo $content_top; ?>
    
  <?php if (/*isset($_GET['exclude']) && */$categories) { ?>
  <div class="category-list">
    <?php for ($i = 0; $i < count($categories);) { ?>
    <ul>
      <?php $j = $i + ceil(count($categories) / 4); ?>
      <?php for (; $i < $j; $i++) { ?>
      <?php if (isset($categories[$i])) { ?>
      <li class="tags-filtter" value="<?php echo $categories[$i]['cat_id']; ?>" style="display: none"><a class="filter-tag"><?php echo $categories[$i]['name']; ?></a></li>
      <?php } ?>
      <?php } ?>
    </ul>
    <?php } ?>
  </div>
<?php } ?>


  
<div class="product-grid-list">
  <ul class="product-list" id="product-list-grid">
    
    <?php if ($products) { ?>
    <?php foreach ($products as $product) { ?>
    <li class="<?php
    
    foreach($product['pro_cat'] as $cat){
	echo $cat['category_id']." ";
    }
    
    ?>">
	<div class="product-block">
	<div class="product-block-inner">
	    <?php if (isset($cook["firstname"])) { ?>
		<div class="name" style="display: none">
	    <?php } else { ?>
		<div class="name">
	    <? } ?>
			<a href="index.php?route=seller/catalog-seller/profile&seller_id=<?php echo $product['user_id']; ?>">
			<div class="avatarcon">
				<img class="avatar" src="image/no-avatar-women.png" data-src="<?php echo $product['user_image']; ?>" class="lazy" />
			</div>
			</a>
			<a class="a" href="index.php?route=seller/catalog-seller/profile&seller_id=<?php echo $product['user_id']; ?>">
			<h3><?php echo $product['username']; ?></h3>
			<?php echo $product['sp']; ?>
			</a>
			
		</div>
      
      <div class="image">
	
		
	<?php if ($product['is_verified']) { ?>
	<div class="homaels-verified"></div>
	<?php } ?>
	
	<div class="pro_filter"></div>
	
	<?php if ($product['totalsides']) { ?>
		<div class="side-text-con" >
			<span class="side-text">
			<?php echo $product['totalsides']; ?>
			</span>
		</div>
	<?php } ?>
	
	
	<?php if ($product['thumb']) { ?>
	<img src="image/no_image.jpg" data-src="<?php echo $product['thumb']; ?>" class="lazy" title="<?php echo $product['name']; ?>" alt="<?php echo $product['name']; ?>" />
	<?php } ?>
	
	<div class="side-img-con-con" >
	    
	    <?php
	    
	    $number_of_side_images = 1;
	    
	    foreach($product['sides'] as $side) { $number_of_side_images++; }
	    
	    ?>
	    
	    <div class="side-img-con" data-num="<?php echo $number_of_side_images; ?>">
		<div class="side-img selected" data-side-id="1">
		    <div class="pro_filter"></div>
		    <span class="side-name"><?php echo $product['name']; ?></span>
		    <img src="<?php echo $product['thumb']; ?>" />
		</div>
		<?php
		    
		    $side_img_id = 1;
		    
		foreach($product['sides'] as $side) {
		    
		    $side_img_id++;
		    
		?>
		<div class="side-img" data-side-id="<?php echo $side_img_id; ?>">
		    <div class="pro_filter"></div>
		    <span class="side-name">תוספת - <?php echo $side['name']; ?></span>
		    <img src="<?php echo $side['thumb']; ?>" />
		</div>
		<?php } ?>
	    </div>
	</div>
	
	<?php if($number_of_side_images > 1) { ?>
	<div class="side-arrows">
	    <div class="side-arrow-left"> <img src="image/data/left.png" /> </div>
	    <a class="center-href" href="<?php echo $product['href']; ?>"></a>
	    <div class="side-arrow-right"> <img src="image/data/right.png" /> </div>
	</div>
	<?php } else { ?>
	<div class="side-arrows">
	    <a class="full-href" href="<?php echo $product['href']; ?>"></a>
	</div>
	<?php } ?>
	
	</div>
		<div class="saleblock">
		<?php if (!$product['special']) { ?>
		<?php } else { ?>
		<span class="saleicon sale">Sale</span>         
		<?php } ?>
		</div>	
	<div style="clear: both"></div>
	  <div class="under-img-con">
		<div class="widget">
			<div class="widget-inner">
				<div class="top-inner">
					<img src="catalog/view/theme/<?php echo $this->config->get('config_template') ?>/image/homeals/vegeterian.png" />
				</div>
				<div class="bottom-inner">
					<span class="text"><?php echo $product['pro_cat']; ?></span>
				</div>
			</div>
			<div class="widget-inner">
				<div class="top-inner">
					<span class="numbercon">2</span>
				</div>
				<div class="bottom-inner">
					<span class="text">Side dish</span>
				</div>
			</div>
			<div class="widget-inner">
				<div class="top-inner">
				
				<span class="pricecon">
				<?php if (!$product['special']) { ?>
				<?php echo $product['price']; ?>
				<?php } else { ?>
				<!-- <span class="price-old"><?php echo $product['price']; ?></span> --> <span class="price-new"><?php echo $product['special']; ?></span>
				<?php } ?>
				<?php if ($product['tax']) { ?>
				<br />
				<span class="price-tax"><?php echo $text_tax; ?> <?php echo $product['tax']; ?></span>
				<?php } ?>
				</span>	
					
				</div>
				<div class="bottom-inner">
					<span class="text">Per meal</span>
				</div>
			</div>
		</div>
		<div class="probox">
		<div class="namepro" style="margin-bottom: 10px;">
		    <span class="namecon">
			<a href="<?php echo $product['href']; ?>">
			    <?php echo mb_truncate($product['name'],50); ?>
			</a>
		    </span>
		    <span class="pricecon">
				<?php if (!$product['special']) { ?>
				<?php echo $product['price']; ?>
				<?php } else { ?>
				<!-- <span class="price-old"><?php echo $product['price']; ?></span> --> <span class="price-new"><?php echo $product['special']; ?></span>
				<?php } ?>
				<?php if ($product['tax']) { ?>
				<br />
				<span class="price-tax"><?php echo $text_permeal; ?> <?php //echo $product['tax']; ?></span>
				<?php } ?>
		    </span>
		</div>
		
		<div class="rating">
		<?php if($product['reviews'] != 0){ ?>
		    <div class="rating-scope" itemscope itemtype="http://schema.org/AggregateRating"><img itemprop="ratingValue" src="catalog/view/theme/<?php echo $this->config->get('config_template') ?>/image/stars-<?php echo $product['rating']; ?>.png" />&nbsp;&nbsp;<span itemprop="reviewCount" onclick="$('a[href=\'#tab-review\']').trigger('click');" ><?php echo $product['reviews']; ?> <?php echo $text_reviewsmin; ?></span>&nbsp;<?php echo (($product['reviews_text'] == 0) ? "" : "|"); ?>&nbsp;<a href="<?php echo $product['href']."#reviews_start"; ?>" class="review-write" <?php echo (($product['reviews_text'] == 0) ? "style='display:none'" : ""); ?>><img itemprop="ratingValue" src="catalog/view/theme/<?php echo $this->config->get('config_template') ?>/image/homeals/speachbubble.png" />&nbsp;<?php echo $product['reviews_text']; ?></a></div> 
		<?php } else { ?>
		
		<?php  } ?>
		    <div class="last-order-time"><?php echo $product['last_order_time']; ?></div>
		</div>
		
		<div class="friends" style="display: none;">
			
			<div class="text">
				Friends that purchsed meals from this cook:
			</div>
			
			<div class="avatars">
				<div class="header-avatar">
					<img class="avatar" src="catalog/view/theme/<?php echo $this->config->get('config_template') ?>/image/homeals/avatar.jpg" />
				</div>
				<div class="header-avatar">
					<img class="avatar" src="catalog/view/theme/<?php echo $this->config->get('config_template') ?>/image/homeals/avatar.jpg" />
				</div>
				<div class="header-avatar">
					<img class="avatar" src="catalog/view/theme/<?php echo $this->config->get('config_template') ?>/image/homeals/avatar.jpg" />
				</div>
				<div class="header-avatar">
					<img class="avatar" src="catalog/view/theme/<?php echo $this->config->get('config_template') ?>/image/homeals/avatar.jpg" />
				</div>
				<div class="header-avatar">
					<img class="avatar" src="catalog/view/theme/<?php echo $this->config->get('config_template') ?>/image/homeals/avatar.jpg" />
				</div>
				<div class="dots">
					<img src="catalog/view/theme/<?php echo $this->config->get('config_template') ?>/image/homeals/dots.png" />
				</div>
			</div>
		</div>
		
		<div style="clear: both"></div>
		</div>
		
		</div><!-- under-img-con - End -->
		<div class="underpro"></div>
		
	</div>
 </div>
    </li>
    <?php } ?>
    <?php } ?>
    
    
    <?php if(isset($opposite_products)) { ?> 
    <div style="clear: both"></div>
    <div class="oppsite-tab"> 
	<div class="ot-title">
	    <?php echo $text_ot_title; ?>
	</div>
	<div class="ot-subtitle">
	    <?php echo $text_ot_subtitle; ?>
	</div>
	<div id="go-to-next-day"><?php echo $text_next_day; ?> >></div>
	
    </div>
    <div style="clear: both"></div>
    
    <?php foreach ($opposite_products as $product) { ?>
    <li class="<?php
    
    foreach($product['pro_cat'] as $cat){
	echo $cat['category_id']." ";
    }
    
    ?>">
	<div class="product-block opposite-product">
	<div class="product-block-inner">
	    <?php if (isset($cook["firstname"])) { ?>
		<div class="name" style="display: none">
	    <?php } else { ?>
		<div class="name">
	    <? } ?>
			<a href="index.php?route=seller/catalog-seller/profile&seller_id=<?php echo $product['user_id']; ?>">
			<div class="avatarcon">
				<img class="avatar" src="image/no-avatar-women.png" data-src="<?php echo $product['user_image']; ?>" class="lazy" />
			</div>
			</a>
			<a class="a" href="index.php?route=seller/catalog-seller/profile&seller_id=<?php echo $product['user_id']; ?>">
			<h3><?php echo $product['username']; ?></h3>
			<?php echo $product['sp']; ?>
			</a>
			
		</div>
      
      <div class="image">
	
	
	<?php //<a href="<?php echo $product['href']; ?>
	
	<?php if ($product['is_verified']) { ?>
	<div class="homaels-verified"></div>
	<?php } ?>
	
	<div class="pro_filter"></div>
	
	<?php if ($product['totalsides']) { ?>
		<div class="side-text-con" >
			<span class="side-text">
			<?php echo $product['totalsides']; ?>
			</span>
		</div>
	<?php } ?>
	
	
	<?php if ($product['thumb']) { ?>
	<img src="image/no_image.jpg" data-src="<?php echo $product['thumb']; ?>" class="lazy" title="<?php echo $product['name']; ?>" alt="<?php echo $product['name']; ?>" />
	<?php } ?>
	
	<div class="side-img-con-con" >
	    
	    <?php
	    
	    $number_of_side_images = 1;
	    
	    foreach($product['sides'] as $side) { $number_of_side_images++; }
	    
	    ?>
	    
	    <div class="side-img-con" data-num="<?php echo $number_of_side_images; ?>">
		<div class="side-img selected" data-side-id="1">
		    <div class="pro_filter"></div>
		    <span class="side-name"><?php echo $product['name']; ?></span>
		    <img src="<?php echo $product['thumb']; ?>" />
		</div>
		<?php
		    
		    $side_img_id = 1;
		    
		foreach($product['sides'] as $side) {
		    
		    $side_img_id++;
		    
		?>
		<div class="side-img" data-side-id="<?php echo $side_img_id; ?>">
		    <div class="pro_filter"></div>
		    <span class="side-name">תוספת - <?php echo $side['name']; ?></span>
		    <img src="<?php echo $side['thumb']; ?>" />
		</div>
		<?php } ?>
	    </div>
	</div>
	
	<?php if($number_of_side_images > 1) { ?>
	<div class="side-arrows">
	    <div class="side-arrow-left"> <img src="image/data/left.png" /> </div>
	    <a class="center-href" href="<?php echo $product['href']; ?>"></a>
	    <div class="side-arrow-right"> <img src="image/data/right.png" /> </div>
	</div>
	<?php } ?>
	
      </div>
	<div class="saleblock">
	<?php if (!$product['special']) { ?>
	<?php } else { ?>
	<span class="saleicon sale">Sale</span>         
	<?php } ?>
	</div>
	<div style="clear: both"></div>
	  <div class="under-img-con">
		<div class="widget">
			<div class="widget-inner">
				<div class="top-inner">
					<img src="catalog/view/theme/<?php echo $this->config->get('config_template') ?>/image/homeals/vegeterian.png" />
				</div>
				<div class="bottom-inner">
					<span class="text"><?php echo $product['pro_cat']; ?></span>
				</div>
			</div>
			<div class="widget-inner">
				<div class="top-inner">
					<span class="numbercon">2</span>
				</div>
				<div class="bottom-inner">
					<span class="text">Side dish</span>
				</div>
			</div>
			<div class="widget-inner">
				<div class="top-inner">
				
				<span class="pricecon">
				<?php if (!$product['special']) { ?>
				<?php echo $product['price']; ?>
				<?php } else { ?>
				<!-- <span class="price-old"><?php echo $product['price']; ?></span> --> <span class="price-new"><?php echo $product['special']; ?></span>
				<?php } ?>
				<?php if ($product['tax']) { ?>
				<br />
				<span class="price-tax"><?php echo $text_tax; ?> <?php echo $product['tax']; ?></span>
				<?php } ?>
				</span>	
					
				</div>
				<div class="bottom-inner">
					<span class="text">Per meal</span>
				</div>
			</div>
		</div>
		<div class="probox">
		<div class="namepro" style="margin-bottom: 10px;">
		    <span class="namecon">
			<a href="<?php echo $product['href']; ?>">
			<?php if(false/*mb_strlen ($product['name']." ".$product['totalsides'],"UTF-8") <= 55*/) { ?>
			    <?php echo mb_truncate($product['name'],55); ?>
			    <span class="sides"> <?php echo $product['totalsides']; ?></span>     
			<?php } else { ?>
			    <?php echo mb_truncate($product['name'],55); ?>
			<?php }  ?>
			</a>
		    </span>
		    <span class="pricecon">
				<?php if (!$product['special']) { ?>
				<?php echo $product['price']; ?>
				<?php } else { ?>
				<!-- <span class="price-old"><?php echo $product['price']; ?></span> --> <span class="price-new"><?php echo $product['special']; ?></span>
				<?php } ?>
				<?php if ($product['tax']) { ?>
				<br />
				<span class="price-tax"><?php echo $text_permeal; ?> <?php //echo $product['tax']; ?></span>
				<?php } ?>
		    </span>
		</div>
		
		<div class="rating">
		<?php if($product['reviews'] != 0){ ?>
		    <div class="rating-scope" itemscope itemtype="http://schema.org/AggregateRating"><img itemprop="ratingValue" src="catalog/view/theme/<?php echo $this->config->get('config_template') ?>/image/stars-<?php echo $product['rating']; ?>.png" />&nbsp;&nbsp;<span itemprop="reviewCount" onclick="$('a[href=\'#tab-review\']').trigger('click');" ><?php echo $product['reviews']; ?> <?php echo $text_reviewsmin; ?></span>&nbsp;<?php echo (($product['reviews_text'] == 0) ? "" : "|"); ?>&nbsp;<a href="<?php echo $product['href']."#reviews_start"; ?>" class="review-write" <?php echo (($product['reviews_text'] == 0) ? "style='display:none'" : ""); ?>><img itemprop="ratingValue" src="catalog/view/theme/<?php echo $this->config->get('config_template') ?>/image/homeals/speachbubble.png" />&nbsp;<?php echo $product['reviews_text']; ?></a></div> 
		<?php } else { ?>
		
		<?php  } ?>
		    <div class="last-order-time time-passed"><?php echo $product['last_order_time']; ?></div>
		</div>
		
		<div class="friends" style="display: none;">
			
			<div class="text">
				Friends that purchsed meals from this cook:
			</div>
			
			<div class="avatars">
				<div class="header-avatar">
					<img class="avatar" src="catalog/view/theme/<?php echo $this->config->get('config_template') ?>/image/homeals/avatar.jpg" />
				</div>
				<div class="header-avatar">
					<img class="avatar" src="catalog/view/theme/<?php echo $this->config->get('config_template') ?>/image/homeals/avatar.jpg" />
				</div>
				<div class="header-avatar">
					<img class="avatar" src="catalog/view/theme/<?php echo $this->config->get('config_template') ?>/image/homeals/avatar.jpg" />
				</div>
				<div class="header-avatar">
					<img class="avatar" src="catalog/view/theme/<?php echo $this->config->get('config_template') ?>/image/homeals/avatar.jpg" />
				</div>
				<div class="header-avatar">
					<img class="avatar" src="catalog/view/theme/<?php echo $this->config->get('config_template') ?>/image/homeals/avatar.jpg" />
				</div>
				<div class="dots">
					<img src="catalog/view/theme/<?php echo $this->config->get('config_template') ?>/image/homeals/dots.png" />
				</div>
			</div>
		</div>
		
		<div style="clear: both"></div>
		</div>
		
		</div><!-- under-img-con - End -->
		<div class="underpro"></div>
		
	</div>
 </div>
    </li>
    <?php } // for each oppsite close ?>
<?php } // if is set oppsite close ?>
    
  </ul>
</div>
    
    <div style="clear: both"></div>
    <div class="oppsite-tab"> 
	<div id="go-to-next-day"><?php echo $text_next_day; ?> >></div>
    </div>
    <div style="clear: both"></div>
    
  <?php // here we check if 3 O' Clock has passed ?>
  <?php if (!$products && (strtotime("now") > $cut_off_time) ) { ?>
  <script>
    $(document).ready(function() {
	$(".weekday").first().trigger("click");
    });
  </script>
  <h1 class="homeals-h1" ><?php echo $text_no_pros; ?></h1>  
  <?php } ?>
  <?php echo $content_bottom; ?></div>
<script>

$(window).load(function() {
    
});

var i = 0;

/*****************************************************
 *
 *	image_id - image id
 *	image_num - number of images
 *
 ***************************************************/
function nextImage(image_id,image_num) {
    
    if (image_id == 0) {
	next_image = image_num;
    } else if (image_id > image_num) {
	next_image = 1;
    } else {
	next_image = image_id;
    }
    
    return next_image;
}

$(document).ready(function() {
    
    var next_day = $(".day-con.selected").first().parent().next().find(".day-con");
    
    if (next_day.html()) {
	$(".next-day-mon").html(next_day.find(".day-date").html());
	$(".next-day-name").html(next_day.find(".day-name").html());
	
	$("#go-to-next-day").on("click", function() {
	    next_day.trigger("click");
	});
    } else {
	$("#go-to-next-day").hide();
    }
    
    /**************************************************
     *
     *	new meal block with moving pictures
     *
     *************************************************/
    $(".side-arrow-right , .side-arrow-left").on("click",function() {
	
	$(this).parent().parent().find('.side-img-con').show();
	$(this).parent().parent().find('.side-text-con').hide();
	
	
	var image_num =  parseInt($(this).parent().parent().find(".side-img-con").data("num"),10);
	var curr_image_id = parseInt($(this).parent().parent().find(".side-img-con").find(".selected").data("side-id"),10);
	
	if($(this).hasClass("side-arrow-left")){
	    var next_image_id = nextImage(curr_image_id + 1,image_num);
	} else {
	    var next_image_id = nextImage(curr_image_id - 1,image_num);
	}
	
	$(this).parent().parent().find(".side-img.selected").removeClass("selected");
	$(this).parent().parent().find("[data-side-id='"+next_image_id+"']").addClass("selected");
	
    });
    
    // Cycle Side images
    $(".product-block").hover(function(){
	$(this).find('.side-arrows').show();
	
    }, function(){
	$(this).find('.side-arrows').hide();
	$(this).find('.side-img-con').hide();
	$(this).find('.side-text-con').show();
    });
    /*************************************************/
    
    var $header = $("header"),
    $clone = $header.before($header.clone().addClass("clone"));
    
    if(/Android|webOS|iPhone|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
	$(window).on("scroll", function() {
	    var fromTop = $("body").scrollTop();
	    $('body').toggleClass("down", (fromTop > 68));
	});
    } else {
	$(window).on("scroll", function() {
	    var fromTop = $("body").scrollTop();
	    $('body').toggleClass("down", (fromTop > 98));
	});
    }
    
    $(".tags-filtter").on("click", function(){
	
	$(this).hide();
	
	var cat_id = $(this).val();
	
	$("input[name='use_cat']").filter(function(){return this.value==cat_id}).trigger("click");
	filter_cats();
	
    });
    
    // for new oppsite meals
    <?php if(isset($opposite_products)) { ?>
	//console.log(<?php echo json_encode($opposite_products); ?>);
    <?php } ?>
    
});
</script>
<?php echo $footer; ?>