<?php echo $header; ?>
<?php echo $column_left; ?><?php echo $column_right; ?>
<div id="content"><?php echo $content_top; ?>   
    <div class="product-info" itemscope itemtype="http://schema.org/Product">
    <div class="left product-image">
    <div class="product-header">
        <div class="left">
            <div class="left product-title">
                <h1 class="item name fn" itemprop="name" id="meal-title-pro"><?php echo $heading_title; ?>
		<?php if( false ) {  ?><span class="side-total"><?php echo $total_sides; ?></span><?php } ?>
		</h1> 
		
		<?php if ($review_status && ($review_count > 0)) { ?>
		<div class="rating">
		<div itemscope itemtype="http://schema.org/AggregateRating"><img itemprop="ratingValue" src="catalog/view/theme/<?php echo $this->config->get('config_template') ?>/image/stars-<?php echo $rating; ?>.png" alt="<?php echo $reviews; ?>" />&nbsp;&nbsp;<span itemprop="reviewCount"  ><?php echo $reviews; ?></span>
		<?php if($reviews_text >0){ ?>
		&nbsp;&nbsp;|&nbsp;&nbsp <a id="go-to-review" class="review-write"><img itemprop="ratingValue" src="catalog/view/theme/<?php echo $this->config->get('config_template') ?>/image/homeals/speachbubble.png" />&nbsp;<?php echo $reviews_text; ?></a>
		<?php  } ?>
		</div></div>
		<?php } ?>
		
            </div>
            <div class="clear"></div>
        </div>
        <div class="right">
	    <?php
		function curPageURL() {
		    $pageURL = 'http';
		    if (isset($_SERVER["HTTPS"])) { if ($_SERVER["HTTPS"] == "on") {$pageURL .= "s";}; }
		    $pageURL .= "://";
		    if ($_SERVER["SERVER_PORT"] != "80") {
		     $pageURL .= $_SERVER["SERVER_NAME"].":".$_SERVER["SERVER_PORT"].$_SERVER["REQUEST_URI"];
		    } else {
		     $pageURL .= $_SERVER["SERVER_NAME"].$_SERVER["REQUEST_URI"];
		    }
		    $pageURL = str_replace("&", urlencode("&"), html_entity_decode($pageURL));
		    return $pageURL;
		   }
	    ?>
	    
            <p class="share-charm">
			<a id="share-facebook" class="tooltip" title="<?php echo $sface; ?>"
				href="https://www.facebook.com/sharer/sharer.php?u=<?php echo curPageURL()?>"
				onclick="javascript:window.open(this.href, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=300,width=600');return false;"
				target="_blank"></a>
            <a id="share-twitter" class="tooltip" title="<?php echo $stwit; ?>"
				href="https://twitter.com/home?status=<?php echo $stwit_text; ?>%20<?php echo curPageURL()?>"
				onclick="javascript:window.open(this.href, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=300,width=600');return false;"
				target="_blank"></a>
			<a id="share-mail" class="tooltip" title="<?php echo $smail; ?>" href="mailto:?subject=<?php echo $smail_sub; ?>&amp;body=<?php echo $smail_bud; ?>" target="_blank"></a>
            </p>
        </div>
        <div class="clear"></div>
    </div>
    
    <?php if ($thumb || $images) { ?>

      <?php if ($thumb) { ?>      
	  <!-- Megnor Cloud-Zoom Image Effect Start -->
	  	<span class="image">
		    <?php if ($is_verified) { ?>
		    <div class="homaels-verified"></div>
		    <?php } ?>
		    <img id="cloud-zoom"  src="<?php echo $thumb; ?>"   title="<?php echo $heading_title; ?>" alt="<?php echo $heading_title; ?>" data-zoom-image="<?php echo $popup; ?>"/>
		</span> 
      
	<!-- Megnor Cloud-Zoom Image Effect End-->
      <?php } ?>
      <?php if ($images) { ?>
      
      <script>	  	  			
		$("#cloud-zoom").elevateZoom({gallery:'additional-carousel', cursor: 'pointer', galleryActiveClass: 'active', imageCrossfade: true, loadingIcon: 'catalog/view/theme/<?php echo $this->config->get('config_template') ?>/image/megnor/spinner.gif'}); 				
		$("#cloud-zoom").bind("click", function(e) {  
			var ez =   $('#cloud-zoom').data('elevateZoom');	
			$.fancybox(ez.getGalleryList());
			return false;
		});
	</script> 
	  	 <?php 
			$sliderFor = 3;
			$imageCount = sizeof($images); 
		 ?>	
		 <div class="additional-carousel">	
		  <?php if ($imageCount >= $sliderFor): ?>
		  	<div class="customNavigation">
				<a class="btn prev">&nbsp;</a>
				<a class="btn next">&nbsp;</a>
			</div> 
		  <?php endif; ?>
	  
		  <div id="additional-carousel" class="image-additional <?php if ($imageCount >= $sliderFor){?>product-carousel<?php }?>">
    	    <?php foreach ($images as $image) { ?>
				<div class="slider-item">
				<div class="product-block">		
        			<a href="<?php echo $image['popup']; ?>" title="<?php echo $heading_title; ?>" class="elevatezoom-gallery" data-image="<?php echo $image['thumb']; ?>" data-zoom-image="<?php echo $image['popup']; ?>"><img id="cloud-zoom"  src="<?php echo $image['thumb']; ?>"   width="74" title="<?php echo $heading_title; ?>" alt="<?php echo $heading_title; ?>" /></a>
				</div>
				</div>		
	        <?php } ?>				
    	  </div>
		 
		  <span class="additional_default_width" style="display:none; visibility:hidden"></span>
		  </div>		  	  
	  	
    <?php } ?>
    
    <div id="tab-cats-tags">
	<?php if ($tags) { ?>
	<div id="tab-cats">
	    
	    <?php foreach ($pro_cat as $cat) { ?>
	    <div class="pro-cat">
		<img  src="image/<?php echo $cat["image"]; ?>"   alt="<?php echo $cat["name"]; ?>"/>
	    </div>
	    <?php } ?>
	    
	    <div style="clear: both"></div>
	    
	</div>
	<?php } ?>
    </div>
    
    <div id="tab-description">
    
    <div id="description-con">
    <h3><?php echo $tab_description; ?></h3>
    </div>
    <?php
    
    $description_max_size = 250;
    $short_description = mb_truncate($description,$description_max_size);
    
    if (mb_strlen($description,"UTF-8") > $description_max_size){
	
    ?>
    
    <div class="short-description">
	<p><?php echo $short_description; ?></p>
	<a id="more"><?php echo $text_read_more; ?></a></div>
    <div class="full-description" style="display: none"><?php echo $description; ?><a id="less"><?php echo $text_read_less; ?></a></div>
    
    <?php } else { ?>
    <div class="full-description"><?php echo nl2br($description); ?></div>
    <?php } ?>
    </div>
    
    
    <?php if ($tags) { ?>
    <div id="tab-ingredients" >
	<h3><?php echo $text_ingredients; ?></h3>
	
	<?php
	$tags_string = "";
	for ($i = 0; $i < count($tags); $i++) { ?>
	<?php if ($i < (count($tags) - 1)) {
		$tags_string .= $tags[$i]['tag'].", ";
	    } else {
		$tags_string .= $tags[$i]['tag'];
	    }
	}
	
	
	$description_max_size = 120;
	
	?>
	
	<div class="tags product-info-tags">
	    <?php echo $tags_string ?>
	</div>    
	
	
	<?php
	// depracated
	if(false) { ?>
	    <?php for ($i = 0; $i < count($tags); $i++) { ?>
	    <?php if ($i < (count($tags) - 1)) { ?>
	    <!--<a href="<?php echo $tags[$i]['href']; ?>">--><?php echo $tags[$i]['tag']; ?><!--</a>-->,
	    <?php } else { ?>
	    <!--<a href="<?php echo $tags[$i]['href']; ?>">--><?php echo $tags[$i]['tag']; ?><!--</a>-->
	    <?php } ?>
	    <?php } ?>
	<?php } ?>
	
    <?php } ?>
    </div>
    
    <div style="clear: both; border: 0; margin: 0;"></div>
    	
    
    <hr style="margin-top: 0px">
	
	<?php if($products) { ?>
    <div id="tab-sides">
	<div>
	    <div class="left product-title">
		<h1 class="item name fn" itemprop="name" ><?php echo $text_sides; ?></h1>
	    </div>
	</div>
	
	<div class="side-swiper-container">
                <div class="swiper-wrapper">
		<?php
	    $j = 0;
	    foreach ($products as $product) {
		$j++;
		?>
	    <div class="swiper-slide">
		<div class="side-img">
		    <?php if ($product['is_verified']) { ?>
		    <div class="homaels-verified"></div>
		    <?php } ?>
		    <img  src="<?php echo $product['thumb'] ?>"   >
		</div>
		
		<div class="side-content">
		
		<div class="name">
		    <a><?php echo $product['name']; ?></a>
		    <?php if ($product['price_int']) { ?>
		    <div class="extra">
		    <?php echo $product['text_extra']; ?>
		    </div>
		    <?php } ?>
		</div>    
		
		<div class="rating">
		<?php if ($product['rating'] > 0) { ?>
		<div itemscope itemtype="http://schema.org/AggregateRating"><img itemprop="ratingValue" src="catalog/view/theme/<?php echo $this->config->get('config_template') ?>/image/stars-<?php echo $product['rating']; ?>.png" alt="<?php echo $product['reviews']; ?>" />&nbsp;&nbsp;<span itemprop="reviewCount" ><?php echo $product['reviews']; ?></span></div>
		<?php } ?>
		</div>
		
		<?php
			$side_tags = array();
			
			$tagss = explode(',', $product['tag']);
			
			foreach ($tagss as $tag) {
				$side_tags[] = array(
					'tag'  => trim($tag),
					'href' => $this->url->link('product/search', 'filter_tag=' . trim($tag))
				);
			}
		?>
		
		<?php if ($side_tags) { ?>
		<div id="tab-ingredients-sides">
		    <h3><?php echo $text_ingredients; ?></h3>
		    
		    <div class="tags product-info-tags">
		      <?php for ($i = 0; $i < count($side_tags); $i++) { ?>
		      <?php if ($i < (count($side_tags) - 1)) { ?>
		     <?php echo $side_tags[$i]['tag']; ?>,
		      <?php } else { ?>
		      <?php echo $side_tags[$i]['tag']; ?>
		      <?php } ?>
		      <?php } ?>
		    </div>
		    
		</div>
		
		</div>
		
		<?php } ?>
		
	    </div>
	    
	    <?php } ?>
	    
		</div>
	</div>
	
	
	<div id="sides">
		<div id="side-slider-con">
			<div id="image-page-up" class="pagenavbtn"></div>
			
			<div id="side-slider-inner-con">
			    <div id="side-slider" class="side-slider">
			    <?php
			    
				    
				    $j = 0;
				    foreach ($products as $product) {
				    $j++;
			    ?>
			    <div class="side-thumb <?php echo (($j == 1) ? ' selected' : ''); ?>" data-id="<?php echo $j ?>"><img  src="<?php echo $product['thumb'] ?>"   ></div>
			    <?php } ?>
			    </div>
			</div>
			
			<div id="image-page-down" class="pagenavbtn"></div>
	    </div>
		
	    <?php
	    $j = 0;
	    foreach ($products as $product) {
		$j++;
		?>
	    <div class="side<?php echo (($j == 1) ? ' selected' : ''); ?>" data-id="<?php echo $j; ?>">
		<div class="side-img">
		    <?php if ($product['is_verified']) { ?>
		    <div class="homaels-verified"></div>
		    <?php } ?>
		    <img  src="<?php echo $product['thumb'] ?>"   >
		</div>
		
		<div class="side-content">
		
		<div class="name">
		    <a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a>
		    <?php if ($product['price_int']) { ?>
		    <div class="extra">
		    <?php echo $product['text_extra']; ?>
		    </div>
		    <?php } ?>
		</div>    
		
		<div class="rating">
		<?php if ($product['rating'] > 0) { ?>
		<div itemscope itemtype="http://schema.org/AggregateRating"><img itemprop="ratingValue" src="catalog/view/theme/<?php echo $this->config->get('config_template') ?>/image/stars-<?php echo $product['rating']; ?>.png" alt="<?php echo $product['reviews']; ?>" />&nbsp;&nbsp;<span itemprop="reviewCount" ><?php echo $product['reviews']; ?></span></div>
		<?php } ?>
		</div>
		
		<?php
			$side_tags = array();
			
			$tagss = explode(',', $product['tag']);
			
			foreach ($tagss as $tag) {
				$side_tags[] = array(
					'tag'  => trim($tag),
					'href' => $this->url->link('product/search', 'filter_tag=' . trim($tag))
				);
			}
		?>
		
		<?php if ($side_tags) { ?>
		<div id="tab-ingredients-sides">
		    <h3><?php echo $text_ingredients; ?></h3>
	    
		    <div class="tags product-info-tags">
		      <?php for ($i = 0; $i < count($side_tags); $i++) { ?>
		      <?php if ($i < (count($side_tags) - 1)) { ?>
		     <?php echo $side_tags[$i]['tag']; ?>,
		      <?php } else { ?>
		      <?php echo $side_tags[$i]['tag']; ?>
		      <?php } ?>
		      <?php } ?>
		    </div>
	      
		</div>
		
		</div>
		
		<?php } ?>
		
	    </div>
	    
	    <?php } ?>
		
	    
	    <div style="clear: both"></div>
	</div>
	
    </div>
	<?php } ?>
    
    <div class="reviews">
	<div id="reviews_start"></div>
       <h1 id="reviews_head"><?php echo $lable_reviews; ?> <span class="review-count"><?php echo "(".$tab_review_count.")"; ?></span></h1>
	   <?php if ($logged) { ?>
	<a id="add-review" href="#review-box"><span><?php echo $text_add_review; ?></span></a>
	   <?php } else { ?>
	   <a id="login-review"><span><?php echo $text_add_review; ?></span></a>
	   <?php } ?>
       <div style="clear: both"></div>
       <hr style="margin: 10px 0px">
	
	<div id="review" class="review">
	</div>
	
	<?php if ($logged) { ?>
	<div id="review-box-con" >
	<div id="review-box" >
	    
	    <div class="close-btn" >X</div>
	    
	    <div class="review-meal-con">
            <div class="review-meal-name"><?php echo $text_review_start.$review['name'].$text_of.$username; ?></div>
            
            <div class="review-meal-controler" id="meal-<?php echo $_GET['product_id']; ?>">
		
                <div class="main-dish dish-con" >
                    <div class="meal-image">
                        <img  src="<?php echo $review['image']; ?>"   />
                    </div>
                    <div class="meal-name"><?php echo $review['name']; ?></div>
                    <div class="meal-star-rating">
                        
                        <select class="star-rating" name="star" id="<?php echo $_GET['product_id']; ?>">
                            <option value=""><?php echo$rate_word[0]; ?></option>
                            <option value="1"><?php echo$rate_word[1]; ?></option>
                            <option value="2"><?php echo$rate_word[2]; ?></option>
                            <option value="3"><?php echo$rate_word[3]; ?></option>
                            <option value="4"><?php echo$rate_word[4]; ?></option>
                            <option value="5"><?php echo$rate_word[5]; ?></option>
                        </select>
                        
                    </div>
                </div>
                
                
                <?php foreach ($products as $side) { ?>
                <div class="side-dish dish-con" id="side_<?php echo $side['product_id']; ?>" style="display: none">
                    <div class="meal-image">
                        <img  src="<?php echo $side['image']; ?>"   />
                    </div>
                    <div class="meal-name"><?php echo $side['name']; ?></div>
                    <div class="meal-star-rating">
                        
                        <select class="star-rating" name="star" id="<?php echo $side['product_id']; ?>">
                            <option value=""><?php echo$rate_word[0]; ?></option>
                            <option value="1"><?php echo$rate_word[1]; ?></option>
                            <option value="2"><?php echo$rate_word[2]; ?></option>
                            <option value="3"><?php echo$rate_word[3]; ?></option>
                            <option value="4"><?php echo$rate_word[4]; ?></option>
                            <option value="5"><?php echo$rate_word[5]; ?></option>
                        </select>
                        
                    </div>
                </div>
                <?php } ?>
                
                <?php
		
                $i = 0;
                for ($i ; $i < $total_sides_int ; $i++) {
                ?>
                
                <div class="side-dish dish-con">
                    <div class="select-side-dish">
                        <select class="side-select" id="select">
                            <option value="0"><?php echo $text_choose_side; ?></option>
                            <?php foreach ($products as $side) { ?>
                            <option value="<?php echo $side['product_id']; ?>"><?php echo $side['name']; ?></option>
                            <?php } ?>
                        </select>
                    </div>
                </div>
                
                <?php   } ?>
                
		<div style="clear: both; border: 0;"></div>
		
            </div>
            
            <div class="review-meal-text">
                <textarea class="review-textarea" name="text" id="<?php echo $_GET['product_id']; ?>" placeholder="<?php echo $placeholder_meal; ?>" onfocus="this.placeholder = ''" onblur="this.placeholder = '<?php echo $placeholder_meal; ?>'"></textarea>    
            </div>
        </div>
	    
	    <div class="action-buttons-con">
            <button id="submit"><?php echo $text_submit; ?></button>
            <button id="cancel"><?php echo $text_cancel; ?></button>
	    </div>
	      
	</div>
	</div>
	<?php } ?>
	
    </div>
    
    </div>
    <?php } ?>
    <div class="right">
        <div class="product_under-img-con">	 
            
            <?php if ($options) { ?>
              <div class="options">
                <h2><?php echo $text_option; ?></h2>
        		        
                <?php foreach ($options as $option) { ?>
                <?php if ($option['type'] == 'select') { ?>
                <div id="option-<?php echo $option['product_option_id']; ?>" class="option">
                  <?php if ($option['required']) { ?>
                  <span class="required">*</span>
                  <?php } ?>
                  <b><?php echo $option['name']; ?>:</b><br />
                  <select name="option[<?php echo $option['product_option_id']; ?>]">
                    <option value=""><?php echo $text_select; ?></option>
                    <?php foreach ($option['option_value'] as $option_value) { ?>
                    <option value="<?php echo $option_value['product_option_value_id']; ?>"><?php echo $option_value['name']; ?>
                    <?php if ($option_value['price']) { ?>
                    (<?php echo $option_value['price_prefix']; ?><?php echo $option_value['price']; ?>)
                    <?php } ?>
                    </option>
                    <?php } ?>
                  </select>
                </div>
                <br />
                <?php } ?>
                <?php if ($option['type'] == 'radio') { ?>
                <div id="option-<?php echo $option['product_option_id']; ?>" class="option">
                  <?php if ($option['required']) { ?>
                  <span class="required">*</span>
                  <?php } ?>
                  <b><?php echo $option['name']; ?>:</b><br />
                  <?php foreach ($option['option_value'] as $option_value) { ?>
                  <input type="radio" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option_value['product_option_value_id']; ?>" id="option-value-<?php echo $option_value['product_option_value_id']; ?>" />
                  <label for="option-value-<?php echo $option_value['product_option_value_id']; ?>"><?php echo $option_value['name']; ?>
                    <?php if ($option_value['price']) { ?>
                    (<?php echo $option_value['price_prefix']; ?><?php echo $option_value['price']; ?>)
                    <?php } ?>
                  </label>
                  <br />
                  <?php } ?>
                </div>
                <br />
                <?php } ?>
                <?php if ($option['type'] == 'checkbox') { ?>
                <div id="option-<?php echo $option['product_option_id']; ?>" class="option">
                  <?php if ($option['required']) { ?>
                  <span class="required">*</span>
                  <?php } ?>
                  <b><?php echo $option['name']; ?>:</b><br />
                  <?php foreach ($option['option_value'] as $option_value) { ?>
                  <input type="checkbox" name="option[<?php echo $option['product_option_id']; ?>][]" value="<?php echo $option_value['product_option_value_id']; ?>" id="option-value-<?php echo $option_value['product_option_value_id']; ?>" />
                  <label for="option-value-<?php echo $option_value['product_option_value_id']; ?>"><?php echo $option_value['name']; ?>
                    <?php if ($option_value['price']) { ?>
                    (<?php echo $option_value['price_prefix']; ?><?php echo $option_value['price']; ?>)
                    <?php } ?>
                  </label>
                  <br />
                  <?php } ?>
                </div>
                <br />
                <?php } ?>
                <?php if ($option['type'] == 'image') { ?>
                <div id="option-<?php echo $option['product_option_id']; ?>" class="option">
                  <?php if ($option['required']) { ?>
                  <span class="required">*</span>
                  <?php } ?>
                  <b><?php echo $option['name']; ?>:</b><br />
                  <table class="option-image">
                    <?php foreach ($option['option_value'] as $option_value) { ?>
                    <tr>
                      <td style="width: 1px;"><input type="radio" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option_value['product_option_value_id']; ?>" id="option-value-<?php echo $option_value['product_option_value_id']; ?>" /></td>
                      <td><label for="option-value-<?php echo $option_value['product_option_value_id']; ?>"><img src="<?php echo $option_value['image']; ?>" alt="<?php echo $option_value['name'] . ($option_value['price'] ? ' ' . $option_value['price_prefix'] . $option_value['price'] : ''); ?>" /></label></td>
                      <td><label for="option-value-<?php echo $option_value['product_option_value_id']; ?>"><?php echo $option_value['name']; ?>
                          <?php if ($option_value['price']) { ?>
                          (<?php echo $option_value['price_prefix']; ?><?php echo $option_value['price']; ?>)
                          <?php } ?>
                        </label></td>
                    </tr>
                    <?php } ?>
                  </table>
                </div>
                <br />
                <?php } ?>
                <?php if ($option['type'] == 'text') { ?>
                <div id="option-<?php echo $option['product_option_id']; ?>" class="option">
                  <?php if ($option['required']) { ?>
                  <span class="required">*</span>
                  <?php } ?>
                  <b><?php echo $option['name']; ?>:</b><br />
                  <input type="text" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option['option_value']; ?>" />
                </div>
                <br />
                <?php } ?>
                <?php if ($option['type'] == 'textarea') { ?>
                <div id="option-<?php echo $option['product_option_id']; ?>" class="option">
                  <?php if ($option['required']) { ?>
                  <span class="required">*</span>
                  <?php } ?>
                  <b><?php echo $option['name']; ?>:</b><br />
                  <textarea name="option[<?php echo $option['product_option_id']; ?>]" cols="40" rows="5"><?php echo $option['option_value']; ?></textarea>
                </div>
                <br />
                <?php } ?>
                <?php if ($option['type'] == 'file') { ?>
                <div id="option-<?php echo $option['product_option_id']; ?>" class="option">
                  <?php if ($option['required']) { ?>
                  <span class="required">*</span>
                  <?php } ?>
                  <b><?php echo $option['name']; ?>:</b><br />
                  <input type="button" value="<?php echo $button_upload; ?>" id="button-option-<?php echo $option['product_option_id']; ?>" class="button">
                  <input type="hidden" name="option[<?php echo $option['product_option_id']; ?>]" value="" />
                </div>
                <br />
                <?php } ?>
                <?php if ($option['type'] == 'date') { ?>
                <div id="option-<?php echo $option['product_option_id']; ?>" class="option">
                  <?php if ($option['required']) { ?>
                  <span class="required">*</span>
                  <?php } ?>
                  <b><?php echo $option['name']; ?>:</b><br />
                  <input type="text" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option['option_value']; ?>" class="date" />
                </div>
                <br />
                <?php } ?>
                <?php if ($option['type'] == 'datetime') { ?>
                <div id="option-<?php echo $option['product_option_id']; ?>" class="option">
                  <?php if ($option['required']) { ?>
                  <span class="required">*</span>
                  <?php } ?>
                  <b><?php echo $option['name']; ?>:</b><br />
                  <input type="text" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option['option_value']; ?>" class="datetime" />
                </div>
                <br />
                <?php } ?>
                <?php if ($option['type'] == 'time') { ?>
                <div id="option-<?php echo $option['product_option_id']; ?>" class="option">
                  <?php if ($option['required']) { ?>
                  <span class="required">*</span>
                  <?php } ?>
                  <b><?php echo $option['name']; ?>:</b><br />
                  <input type="text" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option['option_value']; ?>" class="time" />
                </div>
                <br />
                <?php } ?>
                <?php } ?>
              </div>
              <?php } ?>
            
	    <div class="grey-box" id="sendadd-box">
		<input type="hidden" name="product_id" value="<?php echo $product_id; ?>" />
		<input type="hidden" name="option[cook_id]" value="<?php echo $user_id; ?>" />
		<div>
		    
		    <div> 
			<span class="text_delivery">
			<?php echo $text_deliver_to; ?>
			</span>
			<span class="text_address">
			<?php echo (($delivery_address) ? $delivery_address : "תל-אביב") ; ?>
			</span>
		    </div>
		    <div class="select-con">
		    <?php
			date_default_timezone_set("Asia/Jerusalem");
			if(isset($this->session->data['date'])) {
				$day_choosen = date("m/d/y",strtotime($this->session->data['date']));
				$time_choosen = $this->session->data['time'];
		    }
			
			$day_error = true;
			$i = 0;
			$j = 0;
			?>
			<div id="day-quant-error">
			    <div class="dqe-title">
				<?php echo $text_otp_title; ?>
			    </div>
			    <div class="dqe-text">
				<?php echo $text_otp_subtitle; ?>
			    </div>
			    <div class="dqe-footer">
				<?php echo $text_otp_exit; ?>
			    </div>
			</div>
			
			<select name="option[date]" id="day" <?php echo ($has_item ? 'disabled="true"' : ''); ?> >
			<?php
			    for($i;$i<5;$i++){
				
				$this_day = date('m/d/y', strtotime('+'.$days_more[$i].' day'));
				setlocale(LC_ALL, 'he_IL.UTF-8');
				
				
				if($day_choosen == $this_day){
				    $day_error = false;
				}
			?>
				<option <?php echo ($day_choosen == $this_day ? 'selected="selected"' : ''); ?> value="<?php echo $this_day; ?>"> <?php echo strftime("%A %d/%m",strtotime($this_day)); ?> </option>
			<?php } ?>
			</select>
		    </div>
		    <div class="select-con">
		    <select name="option[time-of-day]" id="time-of-day" <?php echo ($has_item ? 'disabled="true"' : ''); ?> >
				<?php foreach($times_of_day as $key => $value){
					$passed_time = false;
					if($key == -1) { $passed_time = true; } ;?>
				<option <?php echo ($time_choosen == $key ? 'selected="selected"' : ''); ?> value="<?php echo $key; ?>"><?php echo $value; ?></option>
				<?php } ?>
		    </select>
		    </div>
		    </div>
	    </div>
            <?php if ($price) { ?>
              <div class="price">
		<div class="pricebox">
                <?php echo $price; ?>
                    <span style="font-size:12px;"><?php echo $text_per_meal; ?></span>
                    </div>
		
		<span id="text_price"><?php echo $text_meal_box; ?></span>
                <?php if (!$special) { ?>
                <div style="clear:both"></div>
                <?php } else { ?>
                <span class="price-old"><?php echo $price; ?></span> <span class="price-new"><?php echo $special; ?></span>
                <?php } ?>
                <?php //if ($tax) { ?>
                <!--<span class="price-tax"><?php echo $text_tax; ?> <?php echo $tax; ?></span><br />-->
                <?php //} ?>
                <?php if ($discounts) { ?>
                <br />
                <!--<div class="discount">
                  <?php foreach ($discounts as $discount) { ?>
                  <?php echo sprintf($text_discount, $discount['quantity'], $discount['price']); ?><br />
                  <?php } ?>
                </div>-->
                <?php } ?>
              </div>
	      
	      	<div class="white-box" id="qty-box">
			
			<div>
			<?php if($number_of_sides || $products) { ?>
			<span class="text_side">
			<?php echo $text_sides; ?>
			</span>
		    <div class="select-con">
            <select name="option[side-1]" data-name="side-1" <?php echo count($products) < 2 ? 'disabled="true"' : "" ;?>>
			<?php
			$i = 0;
			foreach($products as $product) {
				if ($product['price_int']) {
				    $extra = " - ".$product['text_extra'];
				} else {
				    $extra = "";
				}
				
				$string = (mb_strlen($product["name"],"UTF-8") >= 33) ? mb_substr($product["name"],0,30,"utf-8").'...' : $product["name"];
			    $i++ ?>
			<option data-price="<?php echo $product["price_int"]; ?>" value="<?php echo $product["product_id"]; ?>" <?php echo (($i==1) ? "selected" : ""); ?>><?php echo $string.$extra; ?></option>
			<?php } ?>
		    </select>
		    </div>
			<?php if($number_of_sides == 2) { ?>
		    <div class="select-con">
		    <select name="option[side-2]" data-name="side-2">
			<?php
			$i = 0;
			foreach($products as $product) {
				if ($product['price_int']) {
				    $extra = " - ".$product['text_extra'];
				} else {
				    $extra = "";
				}
				
				$string = (mb_strlen($product["name"],"UTF-8") >= 33) ? mb_substr($product["name"],0,30,"utf-8").'...' : $product["name"];
			    $i++ ?>
			<option data-price="<?php echo $product["price_int"]; ?>" value="<?php echo $product["product_id"]; ?>" <?php echo (($i==2) ? "selected" : ""); ?>><?php echo $string.$extra; ?></option>
			<?php } ?>
		    </select>
		    </div>
			<?php } //number_of_sides=2 end ?>
			<?php } //number_of_sides end ?>
		    </div>
		    
			
		    <div>
			<div class="plus-minus-controller">
			    <div class="minus-button">-</div>
			    <div class="input"><span><?php echo $text_meals; ?></span>
			    <input type="text" id="quantity-input" name="quantity" value="1" readonly="readonly" /></div>
			    <div class="plus-button">+</div>
			</div>
			
			<span class="text_qty">
			<?php echo $text_qty; ?>
			</span>
		    </div>
		    
		    <div class="add-to-favotite">
			<a id="open-comment" href="">
			    <?php echo $text_add_comment; ?>
			</a>
		    </div>
		    
		    <div id="comment">
			<textarea id="meal-comment" name="option[comment]"></textarea>
			<!--<a href="">
			    <div class="add-comment-button">
			    <?php echo $add_comment; ?>
			    </div>
			</a>-->
			<div style="clear: both"></div>
		    </div>
		    
		</div>
		
		<div class="white-box" id="deliver-box">
                    <?php echo $text_deliver; ?>
		</div>
              <?php } ?>
          
    		<!--
<div class="description">
    		<table class="product-description"> // Megnor <table> Start 	
            <?php if ($manufacturer) { ?>
    			<tr><td><span><?php echo $text_manufacturer; ?></span></td><td class="description-right"><a href="<?php echo $manufacturers; ?>"><?php echo $manufacturer; ?></a></td></tr>
            <?php } ?>
    	        <tr><td><span><?php echo $text_model; ?></span></td><td class="description-right"><?php echo $model; ?></td></tr>
            <?php if ($reward) { ?>
        	    <tr><td><span><?php echo $text_reward; ?></span></td><td class="description-right"><?php echo $reward; ?></td></tr>
            <?php } ?>
            	<tr><td><span><?php echo $text_stock; ?></span></td><td class="description-right"><?php echo $stock; ?></td></tr>
    		</table>	// Megnor <table> End 	
    		</div>
-->
    		
          <?php if (isset($profiles)): ?>
          <div class="option">
              <h2><span class="required">*</span><?php echo $text_payment_profile ?></h2>
              <br />
              <select name="profile_id">
                  <option value=""><?php echo $text_select; ?></option>
                  <?php foreach ($profiles as $profile): ?>
                  <option value="<?php echo $profile['profile_id'] ?>"><?php echo $profile['name'] ?></option>
                  <?php endforeach; ?>
              </select>
              <br />
              <br />
              <span id="profile-description"></span>
              <br />
              <br />
          </div>
          <?php endif; ?>
          <div class="cart" id="total-box">
	    
            <span><?php echo $text_total; ?>:</span>
		    <input id="int-price" type='hidden' value="<?php echo $priceint; ?>" />
		    <input id="extra-price" type='hidden' value="0" />
		    <span id="total-price"><?php echo $price; ?></span>
	    
            <div>
		<?php
		    if ($logged) {
			
			if(true || (isset($_COOKIE['alargic']) && $_COOKIE['alargic'] == 1)){ ?>
			    <input type="button" value="<?php echo $button_order_now; ?>" id="button-cart" class="button" />
			<?php } else {?>
			    <input type="button" value="<?php echo $button_order_now; ?>" id="button-cart-alargic" class="button" />
			<?php }
			
		    } else {?>
		    <input type="button" value="<?php echo $button_order_now; ?>" id="button-login-fb" class="button" onclick="openCart();"/>
		<?php } ?>
		
		<div class="lightbox-con" style="display: none">
		    
		    <div id="limitbox">
			<div id="limitbox-header">
			    <div id="close-limit-box" class="close-btn">X</div>
			</div>
			
			<div id="limitbox-body">
			    <h1><?php echo $limitbox_text_1; ?></h1>
			    <p><?php echo $limitbox_text_2; ?></p>
			    
			    <div class="button-con">
				<a href="" id="button-add-available">
				    <?php echo $button_add_avilable; ?>
				</a>
				<a id="button-close" onclick="$('.close-btn').trigger('click');" >
				    <?php echo $text_close; ?>
				</a>
			    </div>
			    
			</div>
		    </div>
		    
		</div>
		
              <!--<span class="links">
    		  <a onclick="addToWishList('<?php echo $product_id; ?>');" class="product_wishlist"><?php echo $button_wishlist; ?></a> 
                <a onclick="addToCompare('<?php echo $product_id; ?>');" class="product_compare"><?php echo $button_compare; ?></a></span>-->
            </div>
	    
            <?php if ($minimum > 1) { ?>
            <div class="minimum"><?php echo $text_minimum; ?></div>
            <?php } ?>
          </div>
	  
      </div>
      <div class="product_underpro"></div>
      
      <div class="fav">
	<div>
	      <?php if ($logged) { ?>
	      <input type="button" value="<?php echo $text_favorite; ?>" id="button-favorite" class="button" />
	      <?php } else {?>
	      <input type="button" value="<?php echo $text_favorite; ?>" id="button-favorite" class="button"  />
	      <?php } ?>
	    <!--<span class="links">
		<a onclick="addToWishList('<?php echo $product_id; ?>');" class="product_wishlist"><?php echo $button_wishlist; ?></a> 
	      <a onclick="addToCompare('<?php echo $product_id; ?>');" class="product_compare"><?php echo $button_compare; ?></a></span>-->
	</div>
      </div>
      
    <div class="cooker">
		
        <div class="avatarcon">
			<a href="index.php?route=seller/catalog-seller/profile&seller_id=<?php echo $user_id; ?>">
            <img class="avatar" src="<?php echo $cook_image; ?>" >
			</a>
        </div>
		
	
        <div class="name">
            <a class="gotocook" href="index.php?route=seller/catalog-seller/profile&seller_id=<?php echo $user_id; ?>"><span class="text"></span><?php echo $username; ?></a>
        </div>
	
	<span id="city"><?php echo $city; ?></span>
	
	<!--<div id="cook-reviews">
	    reviews
	</div>-->
	
	<span id="sp-text">
	<?php echo $text_sp; ?>
	</span>
	<span id="sp">
	<?php echo $sp; ?>
	</span>
	
	<div id="cook-contact">
	    <?php if ($logged) { ?>
		  
		    <!-- menny -->
		    <?php if (!$this->customer->getId() || ($this->customer->getId() && $this->customer->getId() != $seller_id)) { ?>
			<input type="button" value="<?php echo $text_contact; ?>" id="button-pm" href="index.php?route=seller/catalog-seller/jxRenderContactDialog&seller_id=<?php echo $seller_id; ?>" class="ajax_msg button" />
		    <?php } ?>
		  
		  
		  <?php } else {?>
		  <input type="button" value="<?php echo $text_contact; ?>" id="button-pm" class="button" onclick="openPM()" />
		  <?php } ?>
	</div>
	
	<div id="cook-products">
	    <div id="cook-products-header">
	    <span id="text-more"><?php echo $text_more; ?></span>
	    
	    </div>
	        
	    <?php foreach( $seller['products'] as $s_product) { ?>
	    
	    <a href="<?php echo $s_product['href']; ?>">
	    <div class="product-more">
		<img  src="<?php echo $s_product['thumb']; ?>"   />
		<?php $string = (mb_strlen($s_product['name'],"UTF-8") >= 55) ? mb_substr($s_product['name'],0,52,"utf-8").'...' : $s_product['name'];  ?>
		<span><?php echo $string; ?></span>
	    </div>
	    </a>
	    
	    <?php } ?>
	    
	    
	    <a id="view-all" href="index.php?route=seller/catalog-seller/profile&seller_id=<?php echo $user_id; ?>">
	    <span><?php echo $text_view_all; ?></span>
	    </a>
	    
	    <div style="clear:both"></div>
	    
	</div>
	<div style="clear:both; border: 0;" ></div>
    </div>
      
    </div>
  </div>
    
  <div id="tabs" class="htabs" style="display: none"><a href="#tab-description"><?php echo $tab_description; ?></a>
    <?php if ($attribute_groups) { ?>
    <a href="#tab-attribute"><?php echo $tab_attribute; ?></a>
    <?php } ?>
    <?php if ($review_status) { ?>
    <a href="#tab-review"><?php echo $tab_review; ?></a>
    <?php } ?>
  </div>
  <?php /*if ($attribute_groups) { ?>
  <div id="tab-attribute" class="tab-content">
    <table class="attribute">
      <?php foreach ($attribute_groups as $attribute_group) { ?>
      <thead>
        <tr>
          <td colspan="2"><?php echo $attribute_group['name']; ?></td>
        </tr>
      </thead>
      <tbody>
        <?php foreach ($attribute_group['attribute'] as $attribute) { ?>
        <tr>
          <td><?php echo $attribute['name']; ?></td>
          <td><?php echo $attribute['text']; ?></td>
        </tr>
        <?php } ?>
      </tbody>
      <?php } ?>
    </table>
  </div>
  <?php }*/ ?>
  <?php if ($review_status) { ?>
  <div id="tab-review" class="tab-content" style="display: none">
    <div class="review"></div>
    <h2 id="review-title"><?php echo $text_write; ?></h2>
    <label><?php echo $entry_name; ?></label>
    <input type="text" name="name" value="" class="entry_name" />
    <br />
    <br />
    <label><?php echo $entry_review; ?></label>
    <textarea name="text" cols="40" rows="8" style="width: 98%;"></textarea>
    <span style="font-size: 11px;"><?php echo $text_note; ?></span><br />
    <br />
    <label class="entery_rating"><?php echo $entry_rating; ?></label> <span><?php echo $entry_bad; ?></span>&nbsp;
    <input type="radio" name="rating" value="1" />
    &nbsp;
    <input type="radio" name="rating" value="2" />
    &nbsp;
    <input type="radio" name="rating" value="3" />
    &nbsp;
    <input type="radio" name="rating" value="4" />
    &nbsp;
    <input type="radio" name="rating" value="5" />
    &nbsp;<span><?php echo $entry_good; ?></span><br />
    <br />
    <label class="entery_captcha"><?php echo $entry_captcha; ?></label>
    <input type="text" name="captcha" value="" class="captch_input" />
    <img src="index.php?route=product/product/captcha" alt="" id="captcha" /><br />
    <br />
    <div class="buttons">
      <div class="right"><a id="button-review" class="button"><?php echo $button_continue; ?></a></div>
    </div>
  </div>
  <?php } ?>
  
  <div class="light-con" style="display: none">
    
    <div id="alargic" class="lightbox-homeals">
	<div class="close-btn">X</div>
	
	<h1 class="h1-homeals">הודעה חשובה לבעלי רגישויות ומגבלות תזונה</h1>
	
	<p>
אם אתם סובלים מאלרגיות, רגישויות או מחלות כרוניות ויש לכם מגבלות תזונה שאתם מודעים להם, הקפידו לקרוא את מרכיבי כל המנות הכלולות בארוחה, וציינו מפורשות בהערות לבשלן בכדי שהבשלנ/ית יידעו להתייחס לכך.
	</p>
	<p>
	    בתיאבון!
	</p>
	
	<div class="alargic-footer">
	    <div class="dsa-con">
		<input type="checkbox" name="alargic_dsa" value="dont_show_again" />
		<lable for="alargic_dsa">אל תציג הודעה זו שוב</lable>
	    </div>
	    <div class="alargic-buttons-con" >
		<a class="homeals-button-new" id="add-comment-cook">הוסף הערות לבשלן</a>
		<a class="homeals-button-new" id="continue-add-to-order">הוסף להזמנה</a>
		<a class="homeals-button-close" id="cancel-order" class="emptybtn">בטל הזמנה</a>
		<div style="clear:both"></div>
	    </div>
	</div>
	
    </div>
    
  </div>
  
  
  <?php echo $content_bottom; ?></div>
<script type="text/javascript"><!--

//--></script> 
<script type="text/javascript"><!--
function openPM() {
    open_pm = true;
    $("#open_login").trigger("click");
}

function openCart() {
    add_to_cart = true;
    $("#open_login").trigger("click");
}

$(document).ready(function() {
	$('.colorbox').colorbox({
	    overlayClose: true,
	    opacity: 0.5,
	    rel: "colorbox"
	});
});

//--></script>

<?php if ($options) { ?>
<script type="text/javascript" src="catalog/view/javascript/jquery/ajaxupload.js"></script>
<?php foreach ($options as $option) { ?>
<?php if ($option['type'] == 'file') { ?>
<script type="text/javascript"><!--
new AjaxUpload('#button-option-<?php echo $option['product_option_id']; ?>', {
	action: 'index.php?route=product/product/upload',
	name: 'file',
	autoSubmit: true,
	responseType: 'json',
	onSubmit: function(file, extension) {
		$('#button-option-<?php echo $option['product_option_id']; ?>').after('<img src="catalog/view/theme/default/image/loading.gif" class="loading" style="padding-left: 5px;" />');
		$('#button-option-<?php echo $option['product_option_id']; ?>').attr('disabled', true);
	},
	onComplete: function(file, json) {
		$('#button-option-<?php echo $option['product_option_id']; ?>').attr('disabled', false);
		
		$('.error').remove();
		
		if (json['success']) {
			//alert(json['success']);
			$('input[name=\'option[<?php echo $option['product_option_id']; ?>]\']').attr('value', json['file']);
		}
		
		if (json['error']) {
			$('#option-<?php echo $option['product_option_id']; ?>').after('<span class="error">' + json['error'] + '</span>');
		}
		
		$('.loading').remove();	
	}
});
//--></script>
<?php } ?>
<?php } ?>
<?php } ?>
<script type="text/javascript"><!--
$('#review .pagination a').live('click', function() {
	$('#review').fadeOut('slow');
	
	$('#review').load(this.href);
	
	$('#review').fadeIn('slow');
	
	return false;
});			

$('#review').load('index.php?route=product/product/review&product_id=<?php echo $product_id; ?>');

$('#button-review').bind('click', function() {
	$.ajax({
		url: 'index.php?route=product/product/write&product_id=<?php echo $product_id; ?>',
		type: 'post',
		dataType: 'json',
		data: 'name=' + encodeURIComponent($('input[name=\'name\']').val()) + '&text=' + encodeURIComponent($('textarea[name=\'text\']').val()) + '&rating=' + encodeURIComponent($('input[name=\'rating\']:checked').val() ? $('input[name=\'rating\']:checked').val() : '') + '&captcha=' + encodeURIComponent($('input[name=\'captcha\']').val()),
		beforeSend: function() {
			$('.success, .warning').remove();
			$('#button-review').attr('disabled', true);
			$('#review-title').after('<div class="attention"><img src="catalog/view/theme/default/image/loading.gif" alt="" /> <?php echo $text_wait; ?></div>');
		},
		complete: function() {
			$('#button-review').attr('disabled', false);
			$('.attention').remove();
		},
		success: function(data) {
			if (data['error']) {
				$('#review-title').after('<div class="warning">' + data['error'] + '</div>');
			}
			
			if (data['success']) {
				$('#review-title').after('<div class="success">' + data['success'] + '</div>');
								
				$('input[name=\'name\']').val('');
				$('textarea[name=\'text\']').val('');
				$('input[name=\'rating\']:checked').attr('checked', '');
				$('input[name=\'captcha\']').val('');
			}
		}
	});
});
//--></script> 
<script type="text/javascript"><!--
$('#tabs a').tabs();
//--></script> 
<script type="text/javascript" src="catalog/view/javascript/jquery/ui/jquery-ui-timepicker-addon.js"></script> 
<script type="text/javascript"><!--
$(document).ready(function() {
	if ($.browser.msie && $.browser.version == 6) {
	    $('.date, .datetime, .time').bgIframe();
	}
	
	$('.date').datepicker({dateFormat: 'yy-mm-dd'});
	
	$('.datetime').datetimepicker({
	    dateFormat: 'yy-mm-dd',
	    timeFormat: 'h:m'
	});
	$('.time').timepicker({timeFormat: 'h:m'});
});
//--></script>

<script type="text/javascript"><!--

<?php if (isset($this->session->data['time']))  { ?>
var time_key = parseInt("<?php echo $this->session->data['time']; ?>");
<?php } else { ?>
var time_key = 0;
<?php } ?>

<?php if ($this->cart->hasProducts())  { ?>
var cartset = true;
<?php } else { ?>
var cartset = false;
<?php } ?>

var review_count = parseInt("<?php echo $tab_review_count; ?>");
var max_qunt = parseInt("<?php echo $text_qty ?>");

$('html, body').addClass("noScroll");
$('.loading-screen').show().addClass("opacityone");

$(window).load(function() {
    $('html, body').removeClass("noScroll");
    $(".loading-screen").fadeOut("slow").removeClass("opacityone");
    
    <?php if(isset($_GET['opencart'])) { ?>
    
    if ($("#time-of-day").val() == "") {
	setTimeout(function(){$('#button-cart').trigger("click");},600);
    }
    
    <?php } ?>
    
});

$(document).ready(function() {
	
	if(/Android|webOS|iPhone|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
	    $(".product-info > .right").prepend($(".cooker"));
	}
	
	$("#add-meal-btn").live("click",function(){
	    $("html, body").animate({ scrollTop: ($('.product_under-img-con').offset().top - 81 )}, 1000);
	});
	
	/**************************
	 *	side image controler script
	 *************************/
	
	var num_of_sides = $.find(".side").length;
	
	$(".side-thumb").on("click", function(){
	    
	    $(this).addClass("selected");
	    $("#side-slider").find('.selected').removeClass("selected");
	    
	    var id = $(this).data("id");
	    
	    $("#sides").find('.selected').removeClass("selected");
	    $("#sides").find('*[data-id="'+id+'"]').addClass("selected");
	    
	});
	
	var height = $("#side-slider").height();
	
	$("#image-page-up").on("click", function(){
		
	    var id = $("#side-slider").find('.selected').data("id");
	    
	    id -= 1;
	    
	    if (id == 0) {
		    id = num_of_sides;
	    } else if (id > num_of_sides) {
		    id = 1;
	    }
	    
	    $("#side-slider").find('.selected').removeClass("selected");
	    
	    $("#sides").find('.selected').removeClass("selected");
	    $("#sides").find('*[data-id="'+id+'"]').addClass("selected");
	    
	    if (id > 4) {
		    var go_to = 204 - height;
		    $("#side-slider").animate({top: go_to}, 400,"swing");
	    } else if (id < 4) {
		    $("#side-slider").animate({top: "0px"}, 400,"swing");
	    }
		
	});
	
	$("#image-page-down").on("click", function(){
	    
	    var id = $("#side-slider").find('.selected').data("id");
	    
	    id += 1;
	    
	    if (id == 0) {
		id = num_of_sides;
	    } else if (id > num_of_sides) {
		id = 1;
	    }
	    
	    $("#side-slider").find('.selected').removeClass("selected");
	    
	    $("#sides").find('.selected').removeClass("selected");
	    $("#sides").find('*[data-id="'+id+'"]').addClass("selected");
	    
	    if (id > 4) {
		var go_to = 204 - height;
		$("#side-slider").animate({top: go_to}, 400,"swing");
	    } else if (id < 4) {
		$("#side-slider").animate({top: "0px"}, 400,"swing");
	    }
	    
	});
	
	/**************************
	 *	side image controler script end <<<<<<<<<<<<<<-----
	 *************************/
    
    $('#quantity-input').on('change', function() {
	
	var multi = $(this).val();
	var price = parseInt($("#int-price").val());
	
	var side_1 = $('.product_under-img-con select[name="option[side-1]"]');
	
	if(side_1.length) {
	    var side_1_price = parseInt(side_1.find('option[value="'+side_1.val()+'"]').attr("data-price"));
	} else {
	    var side_1_price = 0;
	}
	
	var side_2 = $('.product_under-img-con select[name="option[side-2]"]');
	
	if(side_2.length) {
	    var side_2_price = parseInt(side_2.find('option[value="'+side_2.val()+'"]').attr("data-price"));
	} else {
	    var side_2_price = 0;
	}
	
	var extra = side_1_price + side_2_price; // $("#extra-price").val();
	
	var total = price + extra;
	
	$("#total-price").html("<?php echo $ils ?>"+parseFloat(Math.round(multi*total* 100) / 100));
    });
    
    $(".plus-button").on("click",function () {
	if (!(parseInt($(this).parent().find("input").val()) == max_qunt )) {
	   $(this).parent().find("input").val( parseInt($(this).parent().find("input").val())+1);
	   $('#quantity-input').trigger("change");
	}
    });
    
    $(".minus-button").on("click",function () {
	if (!(parseInt($(this).parent().find("input").val()) <= 1 )) {
	   $(this).parent().find("input").val( parseInt($(this).parent().find("input").val())-1);
	   $('#quantity-input').trigger("change");
	}
	
    });
    
    $('select[name="option[side-1]"] , select[name="option[side-2]"]').on("change", function() {
	$('#quantity-input').trigger("change");
    });
    
    $("#open-comment").on("click",function (e) {
	e.preventDefault();
	$("#comment").slideToggle();
	$("#comment").find("textarea").focus();
    });
    
    $("#login-review").on("click",function (e) {
	open_review_box = true;
	$("#open_login").trigger("click");
    });
    
    $("#more").on("click", function() {
	$(".short-description").css("display","none");
	$(".full-description").css("display","block");
    });
    
    $("#less").on("click", function() {
	$(".short-description").css("display","block");
	$(".full-description").css("display","none");
    });
    
    $("#tags_more").on("click", function() {
	$("#short_list").css("display","none");
	$("#full_list").css("display","block");
    });
    
    $("#tags_less").on("click", function() {
	$("#short_list").css("display","block");
	$("#full_list").css("display","none");
    });
    
    $("#add-review").colorbox({inline:true});
    
    <?php if(isset($_GET['review'])) { ?>
    
    $("#add-review").trigger("click");
    
    <?php } ?>
    
    $("#day").on("change", function() {
	
	var date = "";
	var new_date = "";
	
	if (!cartset) {
	    date = $(this).val();
	} else {
	    date = "<?php echo $this->session->data['date']; ?>";
	    new_date = $(this).val();
	}
	
	$.ajax({
		url: 'index.php?route=product/product/ajax_update',
		type: 'post',
		dataType: 'json',
		data: 'date=' + encodeURIComponent(date) + '&product_id=<?php echo $product_id; ?>',
		beforeSend: function() {
			//show loading
			$(".product_under-img-con").prepend('<div class="loading-box" id="meal-box-load"></div>');
		},
		complete: function() {
			//hide loading
			$("#meal-box-load").remove();
		},
		success: function(data) {
			
			//console.log(data);
			
			$('.text_qty').html(data.quantity);
			
			var html = "";
			var passed = "";
			
			if (data.failed) {
				$('#button-cart').val("<?php echo $button_not_avilable; ?>");
				$('#button-cart').addClass("cboxElement");
				$('#button-cart').attr("href","#limitbox");
				$('#button-cart').attr("id","button-disable");
				$("#button-disable").colorbox({inline:true});
				
				$.ajax({
				    url: 'index.php?route=product/product/ajax_update&product_id=<?php echo $product_id; ?>&date='+new_date,
				    type: 'post',
				    dataType: 'json',
				    data: 'date=' + encodeURIComponent(new_date),
				    success: function(data) {
					
					if (data.failed) {
					    $('#time-of-day').html("<option value=0>צור קשר להזמנה</option>");
					    $('#day').html("<option value=0>צור קשר להזמנה</option>");
					    
					    $('#time-of-day , #day').attr("disabled","disabled");
					    
					    max_qunt = 0;
					} else {
					    max_qunt = parseInt(data.intqunt);
					    
					    $.each(data.times, function(index, value) {
						html += "<option value="+index+">"+value+"</option>";
					    });
					    
					    $('#time-of-day').html(html);
					    
					    $('.text_qty').html(data.quantity);
					    $('#quantity-input').trigger("change");
					}
				    }
				});
				
				$('#button-disable').trigger("click");
				
				return;
			}
			
			//if (!time_key) {
			    html += "<option value='' disabled selected >בחר שעה</option>";
			//}
			
			$.each(data.all_times, function(index, value) {
				passed = index;
				
				if (index == time_key && value.status == 'ok') {
				    selected = 'selected="selected"';
				    addtocart_error = false;
				} else {
				    selected = '';
				}
				
				if (value.status == 'ok') {
				    disabled = '';
				    status = 'הזמנות עד '+value.end_time;
				} else if(value.status == 'time passed'){
				    disabled = 'disabled';
				    status = 'הזמנות עד '+value.end_time;
				} else if(value.status == 'time over booked'){
				    disabled = 'disabled';
				    status = 'חלון ההזמנה מלא';
				} else {
				    disabled = 'disabled';
				    status = value.status;
				}
				
				html += '<option value='+index+' '+selected+' '+disabled+' >'+value.value+' | '+status+'</option>';
			});
			
			if (passed == -1 ) {
				$('#button-cart').val("<?php echo $button_not_avilable; ?>");
				$('#button-cart').addClass("cboxElement");
				$('#button-cart').attr("href","#limitbox");
				$('#button-cart').attr("id","button-disable");
				$("#button-disable").colorbox({inline:true});
				$('#button-disable').trigger("click");
				
			} else if (data.intqunt == 0 ){
				$('#button-cart').val("<?php echo $button_out_of_stock; ?>");
				$('#button-cart').addClass("cboxElement");
				$('#button-cart').attr("href","#limitbox");
				$('#button-cart').attr("id","button-disable");
				$('#button-disable').colorbox({inline:true});
				
			} else {
				$('#button-cart-pass').val("<?php echo $button_order_now; ?>");
				$('#button-cart-pass').attr("id","button-cart");
			}
			
			$('#time-of-day').html(html);
			
			max_qunt = parseInt(data.quantity);
			$('#quantity-input').trigger("change");
		}
	});
    });
    
    $("#day").trigger("change");
    
    $("#time-of-day").on("change", function(){
	time_key = $(this).val();
    });
    
    $(".side-select").on("change",function(){
        var side_id = $(this).val();
        $(this).parent().parent().css("display","none");
	$("#side_"+side_id).css("display","block");
    });
    
    $(".star-rating").barrating('show', {
        showSelectedRating:false,
        showValues:false,
	showSelectedRating:true
    });
    
    $("#go-to-review").on("click",function(){
	$("html, body").animate({ scrollTop: ($('#reviews_head').offset().top - 100 )}, 1000);
    });
    
    $("#cancel").on("click",function(){
        $.colorbox.close();
    });
        
    $("#submit").on("click",function(){
	
	if ($(".review-textarea").val() == "") {
	    alert("חובה לכתוב תגובה");
	    return false;
	}
	
	if (!$("#review-box").find("select").first().val()) {
	    alert("יש להזין דירוג");
	    return false;
	}
	
        var star = {};
        var text = {};
        
        var i = 0;
	
        $("#review-box").find("select").each(function() {
	    if ($(this).val()) {
		star[$(this).attr("id")] = $(this).val();
	    }
        });
        
        $("#review-box").find("textarea").each(function() {
		text[$(this).attr("id")] = $(this).val();
		text_data = $(this).val();
        });
        
        //console.log(star);
        //console.log(text);
        
        var data = {
            "stars" : star,
            "texts" : text
        }
        
        $.ajax({
            url: 'index.php?route=product/product/writemulti',
            type: 'post',
            dataType: 'json',
            data: data,
            beforeSend: function() {
                $('#submit').attr('disabled', true);
		showLoadBox();
            },
            complete: function() {
                $('#submit').attr('disabled', false);
		hideLoadBox();
            },
            success: function(data) {
		if (data['error']) {
			//$('#review-title').after('<div class="warning">' + data['error'] + '</div>');
		}
		
		if (data['success']) {
		    showLoadPage();
		    location.href = "?route=product/product&path=0&product_id=<?php echo $product_id; ?>";
		    
		//    if (text_data != "") {
		//	    $('#review').load('index.php?route=product/product/review&product_id=<?php echo $product_id; ?>');
		//	    review_count++;
		//	    $('.review-count').html("("+(review_count)+")");
		//    }
                }
            }
        });
    });
    
    
    // for new oppsite meals
    var ttime = $('select[name="option[date]"]').children().first().val();
    //console.log((ttime == "<?php echo strftime("%m/%d/%y" , strtotime("today") ); ?>"));
    
    <?php if($day_error) { ?>
	
	//$("#day-quant-error").show();
	
	$("#day").prepend('<option value="" disabled="" selected="">בחר יום</option>');
	
	addtocart_day_error = true;
	
    <?php } ?>
    
    $(".dqe-footer").on("click", function() {
	$("#day-quant-error").fadeOut();
    });
    
    
    
    
    
    $('input[name="alargic_dsa"]').on("change", function() {
	if($(this).attr("checked")){
	    $.ajax({
		url: 'index.php?route=product/product/alargicRemember',
		type: 'post',
		dataType: 'json',
		beforeSend: function() {
		    $('.dsa-con').append('<img id="reload" src="/img/loading.gif" />');
		},
		complete: function() {
		    $("#reload").remove();
		},
		success: function(data) {
		   //console.log(data);
		}
	    });
	} else {
	    $.ajax({
		url: 'index.php?route=product/product/alargicForget',
		type: 'post',
		dataType: 'json',
		beforeSend: function() {
		    $('.dsa-con').append('<img id="reload" src="/img/loading.gif" />');
		},
		complete: function() {
		    $("#reload").remove();
		},
		success: function(data) {
		   //console.log(data);
		}
	    });
	}
    });
    
    $("#button-cart-alargic").live("click", function() {
	
	$(this).attr("id","button-cart");
	
	$.colorbox({
	    inline: true,
	    open: true,
	    href: "#alargic"
	});
	
    });
    
    $("#add-comment-cook").live("click", function(event) {
	event.preventDefault();
	$("#open-comment").trigger("click");
	setTimeout(function(){$("#comment").find("textarea").focus();},500);
	$(".close-btn").trigger("click");
    });
    
    $("#continue-add-to-order").live("click", function(e) {
	event.preventDefault();
	$("#button-cart").trigger("click");
	$(".close-btn").trigger("click");
    });
    
    $("#cancel-order").live("click", function(e) {
	event.preventDefault();
	$(".emptybtn").trigger("click");
	$(".close-btn").trigger("click");
    });
    
});
//--></script>

<?php echo $footer; ?>
