<?php
    // try key words to add day afternoon
    // for more power over catagories
    //
    
    
    
    if (isset($this->request->get['exclude'])) {
	$exclude = explode('_', (string)$this->request->get['exclude']);
    } else {
	$exclude = array();
    }
?>

<div id="filterdiv">
	<div id="filterball"></div>
	<div id="filtertext"><?php echo $text_filter; ?></div>
</div>


<div class="box">
  <div class="box-heading"><?php echo $heading_title; ?></div>
  <div class="box-content">
    <ul class="box-category treeview-list treeview">
      <?php foreach ($categories as $category) { ?>
      <li>
        <?php if ($category['category_id'] == $category_id) { ?>
	    
	    <input type="checkbox" name="use_cat" value="<?php echo $category['category_id']; ?>" checked />
	    
	    <span class="active"><?php echo $category['name']; ?></span>
	    
        <?php } else { ?>
           
	    <input type="checkbox" name="use_cat" value="<?php echo $category['category_id']; ?>" <?php if (!empty($exclude) && !$category_id && !in_array($category['category_id'],$exclude)) { echo "checked"; } ?>>
          
	    <span <?php if (!empty($exclude) && !$category_id && !in_array($category['category_id'],$exclude)) { echo "class='active'"; } ?> ><?php echo $category['name']; ?></span>
	    
        <?php } ?>
        <?php if ($category['children']) { ?>
        <ul>
          <?php foreach ($category['children'] as $child) { ?>
          <li>
            <?php if ($child['category_id'] == $child_id) { ?>
            <a href="<?php echo $child['href']; ?>" class="active"><?php echo $child['name']; ?></a>
            <?php } else { ?>
            <a href="<?php echo $child['href']; ?>"><?php echo $child['name']; ?></a>
            <?php } ?>
          </li>
          <?php } ?>
        </ul>
        <?php } ?>
      </li>
      <?php } ?>
    </ul>	
  </div>
</div>

<script>
    
    function filter_cats() {
	var exclude = "", all = false;
      
      $(".product-list li").hide();
      $(".tags-filtter").hide();
      
      $.each( $("input[name='use_cat']") , function ( key , value) {
        //console.log($(value).attr("checked"));
        if (!$(value).attr("checked")) {
          //console.log($(value).val());
          exclude += $(value).val()+"_";
        } else {
	    $("."+$(value).val()).show();
	    $(".tags-filtter").filter(function(){return this.value==$(value).val()}).show();
	}
        
        if ($(value).attr("checked")) {
	    all = true;
        }
	
      })
      
      //console.log(exclude);
      
	if (all) {
	    //$("."+exclude)
	    //window.location = "index.php?route=product/category&path=0&exclude="+exclude;
	} else {
	    $(".product-list li").show();
	    //window.location = "index.php?route=product/category&path=0";
	}
    }
    
  $(document).ready(function() {
    
    $("input[name='use_cat']").on("click",function () {
      filter_cats();
      });
  })
</script>
