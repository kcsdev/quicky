<div class="keywords">
    <div class="title"><?php echo $heading_title; ?></div>
    <div class="content">
        <input type="text" name="filter_tag" onclick="this.placeholder = '';" placeholder="" value="">
        <?php $count = 0; foreach ($tags as $tag) { ?>
            <li<?php echo ($count++%2==0 ? ' class="first"' : '') ?>>                
                <a class="text" href="<?php echo $tag['href']; ?>"><?php echo $tag['name']; ?></a>
                <a class="x" href="<?php echo $tag['remove']; ?>"><img src="catalog/view/theme/<?php echo $this->config->get('config_template') ?>/image/homeals/x_gray.png" /></a>
                <div class="clear"></div>
            </li>
        <?php } ?>
        <div class="clear"></div>
    </div>
</div>

<div class="box">
  <div class="box-heading"><?php echo $heading_title; ?></div>
  <div class="box-content">
    <ul class="box-category treeview-list treeview">
      <li>
        <input type="checkbox" name="use_time" value="morning" />
        <span><?php echo $text_morning; ?></span>
      </li>
      <li>
        <input type="checkbox" name="use_time" value="afternoon" />
        <span><?php echo $text_afternoon; ?></span>
      </li>
    </ul>	
  </div>
</div>