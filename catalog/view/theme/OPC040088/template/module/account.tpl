<div class="account-menu">
    <ul>
      <?php if (!$logged && false) { ?>
      <a href="<?php echo $login; ?>"><li><?php echo $text_login; ?></a> / <a href="<?php echo $register; ?>"><?php echo $text_register; ?></a></li>
      <a href="<?php echo $forgotten; ?>"><li><?php echo $text_forgotten; ?></a></li>
      <?php } ?>
      
      <?php if ($logged) { ?>
      
      <a href="index.php?route=account/msconversation">
        <?php $pm_test = ($_GET['route'] == "account/msconversation" || $_GET['route'] == "account/msmessage"); ?>
        <li <?php echo ( $pm_test ? "class='selected'" : ""); ?>>תיבת דואר</li>
      </a>
      
      <a href="<?php echo $my_orders; ?>">
        <?php $my_orders_test = ($_GET['route'] == "account/my-orders" || $_GET['route'] == "account/order/info"); ?>
        <li <?php echo ( $my_orders_test ? "class='selected'" : ""); ?>><?php echo $text_my_orders; ?></li>
      </a>
      
      <?php if ($seller) { ?>
      <a href="index.php?route=seller/account-my-orders">
        <li <?php echo ( ($_GET['route'] == "seller/account-my-orders") ? "class='selected'" : ""); ?>><?php echo $my_orders_text; ?></li>
      </a>
      <?php } ?>
      
      <a href="index.php?route=account/password">
        <li <?php echo ( ($_GET['route'] == "account/password") ? "class='selected'" : ""); ?>><?php echo $change_my_pass; ?></li>
      </a>
      
      <!--<a href="<?php echo $account; ?>"><li><?php echo $text_account; ?></li></a> -->
      <!--<a href="<?php echo $edit; ?>"><li><?php echo $text_edit; ?></li></a> -->
      <!--<a href="<?php echo $password; ?>"><li><?php echo $text_password; ?></li></a> -->
      
      <?php } ?>
      
      <!--<a href="<?php echo $address; ?>"><li><?php echo $text_address; ?></li></a> -->
      <!-- <a href="<?php echo $wishlist; ?>"><li><?php echo $text_wishlist; ?></li></a> -->
      <!--<a href="<?php echo $order; ?>"><li><?php echo $text_order; ?></li></a> -->
      <!-- <a href="<?php echo $download; ?>"><li><?php echo $text_download; ?></li></a> -->
      <!-- <a href="<?php echo $return; ?>"><li><?php echo $text_return; ?></li></a> -->
      <!-- <a href="<?php echo $transaction; ?>"><li><?php echo $text_transaction; ?></li></a> -->
      <!-- <a href="<?php echo $newsletter; ?>"><li><?php echo $text_newsletter; ?></li></a> -->
      
      <?php if ($logged) { ?>
      <!--<a href="<?php echo $logout; ?>"><li><?php echo $text_logout; ?></li></a> -->
      <?php } ?>
      
      
    </ul>
</div>
