<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/1999/REC-html401-19991224/strict.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title><?php echo $title; ?></title>

<style>
  @import url(http://fonts.googleapis.com/earlyaccess/alefhebrew.css);
  @import url(http://fonts.googleapis.com/css?family=Ubuntu:400,700);
</style>

</head>
<body style="font-family: 'Alef Hebrew','Ubuntu', Arial, Helvetica, sans-serif; font-size: 12px; color: #000000;">
<div style="font-family: 'Alef Hebrew','Ubuntu', Arial, Helvetica, sans-serif; width: 100%; margin: auto; background-color: #FAF7EF; background-image: url('<?php echo $store_url; ?>catalog/view/theme/OPC040088/image/homeals/bg.png');">
  <a href="<?php echo $store_url; ?>" title="<?php echo $store_name; ?>" style="width: 100%; display: block; text-align: center; height: 110px;">
  <img src="<?php echo $logo; ?>" alt="<?php echo $store_name; ?>" style="margin-top: 10px; border: none;" />
  </a>
<div style="width:696px;margin:auto;direction: rtl;color: #372c2a;">
    <table style="border-collapse:collapse;width:100%;margin-bottom: 3px;background-color: #ffffff;">
      <tbody>
        <tr>
          <td style="font-size:12px; text-align: right; padding: 35px 27px 10px 27px;">
              <div style="float: right;font-size: 24px;">
                <span><?php echo $text_greeting; ?></span>
              </div>
              <div style="float: left;">
                <span style="color: #a2a1a1;"><?php echo $text_order_status; ?></span>
                <span style="color: #15b915;"><?php echo $order_status; ?></span>
                <span style="color: #a2a1a1;"><?php echo $text_order_id; ?></span>
                <span><?php echo $order_id; ?></span>
              </div>
          </td>
        </tr>
        <tr>
          <td style="font-size: 14px; padding: 0 27px 15px 27px;">
            <p><?php echo $text_order_aproved; ?></p>
            
            <p><?php echo $text_here_are_details; ?></p>
            
            <p style="color: #e7604a;"><?php echo $text_meals_word; ?></p>
            <p>
              <?php foreach ($products as $product){ ?>
                
                <?php
                echo $product['quantity']." ".$product['name'];
                if(isset($product['sides'])){
                  foreach ($product['sides'] as $side) {
                    if(isset($side['name'])){
                      echo " + ".$side['name'];
                    }
                  }
                }
                ?>
              <br>
             <?php } ?>
            </p>
            <p  style="color: #e7604a;"><?php echo $text_deliver_word; ?></p>
            <p>
              <?php echo $order_when; ?><br>
              <?php echo $order_address; ?><br>
              <?php echo $order_address_2; ?><br>
              <?php if($order_compony != "") { ?>
              <?php echo $order_compony; ?><br>
              <?php } ?>
              <?php echo $order_contact; ?><br>
              <?php echo $order_phone; ?><br>
              <?php echo $text_order_tools." ".$order_tools; ?>
            </p>
            
            
          </td>
        </tr>
        <tr>
          <td style="font-size: 18px;font-weight: bold; padding: 0 27px 40px 27px;">
          <span><?php echo $text_order_thanks_you; ?></span>
          </td>
        </tr>
      </tbody>
    </table>
  
    
    
    <p style="margin-top:40px;margin-bottom:20px;color: #a2a1a1;text-align: center;">
    <?php echo $text_follow_us; ?><br><br>
    <a href="https://www.facebook.com/pages/Homeals-Israel/608776639200255"><img src="<?php echo $store_url; ?>catalog/view/theme/OPC040088/image/homeals/fbmail.png" /></a>
    <a href="http://instagram.com/homealsisrael"><img src="<?php echo $store_url; ?>catalog/view/theme/OPC040088/image/homeals/instamail.png" /></a>
    </p>
    <p style="margin-top:0px;color: #a2a1a1;text-align: center;">
      הודעה זו נשלחה ל-<?php echo $email; ?> ע״י Homeals Media Ltd, משה דיין 16, ת.ד. 10073 קרית אריה, פתח תקווה.
    </p>
    <p style="padding-bottom:40px;color: #a2a1a1;text-align: center;">
      <span style="display: inline-block;">
      2014 © 		</span>  
      <span>
      כל הזכויות שמורות ל- Homeals		</span>
    </p>
    
    </div>

</div>
</body>
</html>