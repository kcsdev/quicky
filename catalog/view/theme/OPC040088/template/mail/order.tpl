<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/1999/REC-html401-19991224/strict.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title><?php echo $title; ?></title>

<style>
  @import url(http://fonts.googleapis.com/earlyaccess/alefhebrew.css);
  @import url(http://fonts.googleapis.com/css?family=Ubuntu:400,700);
</style>

</head>
<body style="font-family: 'Alef Hebrew','Ubuntu', Arial, Helvetica, sans-serif; font-size: 12px; color: #000000;">
<div style="font-family: 'Alef Hebrew','Ubuntu', Arial, Helvetica, sans-serif; width: 100%; margin: auto; background-color: #FAF7EF; background-image: url('<?php echo $store_url; ?>catalog/view/theme/OPC040088/image/homeals/bg.png');">
  <a href="<?php echo $store_url; ?>" title="<?php echo $store_name; ?>" style="width: 100%; display: block; text-align: center; height: 110px;">
  <img src="<?php echo $logo; ?>" alt="<?php echo $store_name; ?>" style="margin-top: 10px; border: none;" />
  </a>
<div style="width:696px;margin:auto;background-color: #faf7ef;direction: rtl;color: #372c2a;">
    <table style="border-collapse:collapse;width:100%;margin-bottom: 3px;background-color: #ffffff;">
      <tbody>
        <tr>
          <td style="font-size:12px; text-align: right; padding: 35px 27px 10px 27px;">
              <div style="float: right;font-size: 24px;">
                <span><?php echo $text_greeting; ?></span>
              </div>
              <div style="float: left;">
                <span style="color: #a2a1a1;"><?php echo $text_order_status; ?></span>
                <span style="color: #15b915;"><?php echo $order_status; ?></span>
                <span style="color: #a2a1a1;"><?php echo $text_order_id; ?></span>
                <span><?php echo $order_id; ?></span>
              </div>
          </td>
        </tr>
        <tr>
          <td style="font-size: 14px; padding: 0 27px 15px 27px;">
            <span><?php echo $text_order_aproved; ?></span>
          </td>
        </tr>
        <tr>
          <td style="font-size: 18px;font-weight: bold; padding: 0 27px 40px 27px;">
          <span><?php echo $text_order_thank_you; ?></span>
          </td>
        </tr>
      </tbody>
    </table>
    
    <table style="border-collapse:collapse;width:100%;background-color: #ffffff;margin-bottom: 3px;">
      <thead>
        <tr>
          <td style="line-height: 32px;font-size: 18px;font-weight: bold;padding: 14px 20px 5px 27px;color: #e7604a;  width: 50%;">
            <img style="margin-left: 10px;" src="<?php echo $store_url; ?>catalog/view/theme/OPC040088/image/homeals/mailbike.png" /><?php echo $text_delivery_details; ?>
          </td>
        </tr>
      </thead>
      <tbody>
        <tr>
          <td style="font-size: 14px;padding: 0 20px 20px 27px; vertical-align: top;">
            <div style="width: 50%; float:right;">
              <div style="height: 30px;  line-height: 30px;">
                <span style="color: #a2a1a1;"><?php echo $text_order_when; ?></span>
                <?php echo $order_when; ?>
              </div>
              <div style="height: 30px;  line-height: 30px;">
                <span style="color: #a2a1a1;"><?php echo $text_order_address; ?></span>
                <?php echo $order_address; ?>
              </div>
              <div style="height: 30px;  line-height: 30px;">
                <span style="color: #a2a1a1;"><?php echo $text_order_compony; ?></span>
                <?php echo ($order_compony != "") ? $order_compony : "-"; ?>
              </div>
              <div style="height: 30px;  line-height: 30px;">
                <span style="color: #a2a1a1;"><?php echo $text_order_comment; ?></span>
                <?php echo ($order_comment != "") ? $order_comment : "-"; ?>
              </div>
            </div>
            
            <div style="padding-right: 30px; float: right;">
              <div style="height: 30px;  line-height: 30px;">
                <span style="color: #a2a1a1;"><?php echo $text_order_contact; ?></span>
                <?php echo $order_contact; ?>
              </div>
              
              <div style="height: 30px;  line-height: 30px;">
                <span style="color: #a2a1a1;"><?php echo $text_order_phone; ?></span>
                <?php echo $order_phone; ?>
              </div>
              
              <div style="height: 30px;  line-height: 30px;">
                <span style="color: #a2a1a1;"><?php echo $text_order_tools; ?></span>
                <?php echo $order_tools; ?>
              </div>
              
            </div>
            <div style="clear:both"></div>
            
              
          </td>
        </tr>
      </tbody>
    </table>
    
    <table style="border-collapse:collapse;width:100%;background-color: #ffffff;margin-bottom: 3px;font-size: 14px;">
      <thead>
        <tr>
          <td style="font-size: 18px;font-weight: bold;padding: 14px 27px 0px 27px;  color: #e7604a;  width: 50%;" colspan="2">
            <?php echo $text_meal_details; ?>
          </td>
        </tr>
      </thead>
      <?php foreach ($products as $product) { ?>
      <tbody>
        <tr>
          <td style="padding: 25px 27px 25px  20px;width: 1%;"><img src="<?php echo $product['image']; ?>" /></td>
          <td style="padding: 25px 27px 25px 20px;">
            
            <div style="font-size: 16px; margin-bottom: 20px;">
              <?php echo $product['name'];
                if(isset($product['sides'])){
                  foreach ($product['sides'] as $side) {
                    if(isset($side['name'])){
                      echo " + ".$side['name'];
                    }
                  }
                }
              ?>
            </div>
            
            <?php if(false) { ?>
            <div style="height: 20px;margin-bottom: 7px;">
              <span style="color: #a2a1a1;"><?php echo $text_meal_coment; ?></span>
              <span><?php echo $text_meal_coment; ?></span>
            </div>
            <?php } ?>
            
            <table style="width: 100%;">
              <tbody>
                <tr>
                  
                  <td style="vertical-align: top; width: 33%;">
                    <div style="height: 20px;">
                      <span style="color: #a2a1a1;">
                        <?php echo $text_cooks; ?>
                      </span>
                      <span>
                        <?php echo $product['cook_name']; ?>
                      </span>
                    </div>
                    <div style="height: 20px;">
                      <span>
                        <a href="<?php echo $product['cook_pm_link']; ?>"><?php echo $text_contactme; ?></a>
                      </span>
                    </div>
                    
                  </td>
                  
                  <td style="vertical-align: top; text-align:center; width: 33%;">
                    <div style="height: 20px;">
                      <span style="color: #a2a1a1;">
                        <?php echo $text_quantity; ?>
                      </span>
                      <span>
                        <?php
                          if($product['quantity'] == 1){
                           echo "ארוחה אחת";
                          } else {
                           echo $product['quantity']." ארוחות";
                          }
                        ?>
                      </span>
                    </div>
                  </td>
                  
                  <td style="vertical-align: top; text-align:left; width: 33%;">
                    <div style="height: 20px;">
                      <span style="color: #a2a1a1;">
                        <?php echo $text_per_price; ?>
                      </span>
                      <span>
                        <?php echo $product['price']; ?>
                      </span>
                    </div>
                    
                    <div style="height: 37px;font-size: 30px;color: #e7604a;">
                    <!--<span><?php echo $product['price']; ?></span>-->
                    <span><?php echo $product['total']; ?></span>
                    </div>
                  </td>
                  
                </tr>
              </tbody>
            </table>
            
          </td>
        </tr>
        <?php } ?>
      </tbody>
    </table>


    <table style="border-collapse:collapse;width:100%;margin-bottom:3px">
      <tbody>
        <tr>
          <td style="font-size: 18px;  font-weight: bold;  padding: 14px 27px 5px 27px;  color: #e7604a;text-align: left;vertical-align: top;width: 60%;background-image: url('<?php echo $store_url; ?>catalog/view/theme/OPC040088/image/homeals/bg.png');">
            <?php echo $text_order_summary; ?>
          </td>
          <td style="background-color: #ffffff;font-size: 14px;">
        <?php foreach ($totals as $total) { ?>
        <?php if($total['title'] === 'סה"כ') { ?> 
          <div style="padding-top: 3px;background-color: #faf7ef;">
          <table style="border-collapse: collapse;  width: 100%;background-color: #ffffff;">
            <tr  style="color: #e7604a;">
            <td style="font-size: 16px;text-align: right;  padding: 15px 20px;  width: 50%;">
              <?php echo $total['title']; ?>
            </td>
            <td style="text-align: left;  padding: 10px 20px 0 20px;  width: 50%;  font-size: 26px;">
              <?php echo $total['text']; ?>
            </td>
          </tr></table>
          </div>
          <?php } else { ?>
          <div>
          <table style="border-collapse: collapse;  width: 100%;background-color: #ffffff;"><tr>
            <td style="font-size: 16px;text-align: right;  padding: 5px 20px 15px 20px;  width: 50%; color: #a2a1a1;">
              <?php echo $total['title']; ?>
            </td>
            <td style="text-align: left;  padding: 10px 20px;  width: 50%;  font-size: 26px;">
              <?php echo $total['text']; ?>
            </td>
          </tr></table>
          </div>
          <?php } ?>
        <?php } ?>
          </td>
        </tr>
      </tbody>
    </table>
    
    <table style="border-collapse:collapse;width:100%;margin-bottom: 3px;background-color: #ffffff; color: #a2a1a1;">
      <tbody>
        <tr>
          <td style="font-size:12px; text-align: right; padding: 5px 27px 20px 27px;">
            <div class="payment-block">
              <h1 style="font-weight: normal">תשלום</h1>
              <div class="text">
                חויבת באמצעות PayPal בסכום מלא הכולל את דמי המשלוח עבור ההזמנה. התשלום בגין הארוחות יועבר לבשלנ/ית עד 24 שעות לאחר שההזמנה תמסר לך.
              </div>
            </div>
          </td>
        </tr>
      </tbody>
    </table>
    
      
          <table style="border-collapse:collapse;width:100%;margin-bottom: 3px;background-color: #ffffff; color: #a2a1a1;">
      <tbody>
        <tr>
          <td style="font-size:12px; text-align: right; padding: 5px 27px 20px 27px;">
            
      <div class="payment-block">
        <h1 style="font-weight: normal">ביטול ההזמנה</h1>
        <div class="text">
          בהתאם למדיניות הביטולים שלנו המפורטת בתנאי השימוש, במידה ומאיזושהי סיבה ברצונך לבטל את ההזמנה, באפשרותך לעשות זאת עד 3 שעות לפני מועד המשלוח ולקבל זיכוי כספי מלא בגובה 100% מסכום החיוב בניכוי העמלות שגובה PayPal בגין ביצוע פעולה זו. במידה והביטול יתבצע פחות מ-3 שעות לפני מועד המשלוח, תחויב בגובה 50% מסכום החיוב המלא.        </div>
      </div>
      
      </td>
        </tr>
      </tbody>
    </table>
      
          <table style="border-collapse:collapse;width:100%;margin-bottom: 3px;background-color: #ffffff; color: #a2a1a1;">
      <tbody>
        <tr>
          <td style="font-size:12px; text-align: right; padding: 5px 27px 20px 27px;">
      <div class="payment-block">
        <h1 style="font-weight: normal">סיוע ותמיכה</h1>
        <div class="text">
          זוהי הודעת דואר אלקטרוני אוטומטית. לא ניתן להשיב למייל זה. במידה ונתקלת באיזושהי בעיה בהזמנתך, צוות התמיכה שלנו ישמח לסייע לך עד שתהיה שבע רצון, בטלפון:03-3741886 או במייל: support@homeals.com.</div>
      </div>
</td>
        </tr>
      </tbody>
    </table>
    
    
    <p style="margin-top:40px;margin-bottom:20px;color: #a2a1a1;text-align: center;">
    <?php echo $text_follow_us; ?><br><br>
    <a href="https://www.facebook.com/pages/Homeals-Israel/608776639200255"><img src="<?php echo $store_url; ?>catalog/view/theme/OPC040088/image/homeals/fbmail.png" /></a>
    <a href="http://instagram.com/homealsisrael"><img src="<?php echo $store_url; ?>catalog/view/theme/OPC040088/image/homeals/instamail.png" /></a>
    </p>
    <p style="margin-top:0px;color: #a2a1a1;text-align: center;">
      הודעה זו נשלחה ל-<?php echo $email; ?> ע״י Homeals Media Ltd, משה דיין 16, ת.ד. 10073 קרית אריה, פתח תקווה.
    </p>
    <p style="padding-bottom:40px;color: #a2a1a1;text-align: center;">
      <span style="display: inline-block;">
      2014 © 		</span>  
      <span>
      כל הזכויות שמורות ל- Homeals		</span>
    </p>
    
    </div>

</div>
</body>
</html>