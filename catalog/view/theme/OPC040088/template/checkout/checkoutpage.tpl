<div class="container">
    
    <div class="step step1">
        <div class="title">
            <h2>Your</h2>
            <h1>Order</h1>
        </div>
        <div class="order">
            
            <div class="shef">
                <div class="avatarcon">
                                <img class="avatar" src="http://homeals.c14.co.il/image/tmp/ms_no_image-100x100.jpg">
                        </div>
                <div class="details">
                    <span class="name">seller name</span>
                    <span class="city">Tel-Aviv</span>
                    <span class="sp">specislizes in indian cooking</span>
                </div> 
            </div>
            
            <div class="meal">
                <div class="title">chicken, broccoli & cheese skillet 2 side dishes</div>
                <div class="amount">2 meals</div>
                <div class="price">$70.00</div>
            </div>
            
            <div class="delivery">
                <div class="delivery-title">delivery:</div>
                <div class="address">rotchiled 12, tel aviv</div>
                <div class="price">$17.50</div>
            </div>
            
            <div class="total">
                <div class="total-title">total</div>
                <div class="total-price">$87.50</div>
            </div>
            
        </div>
    </div>
    
    <div class="step step2">
        <div class="title">
            <h2>Delivery</h2>
            <h1>Details</h1>
        </div>
        <div class="delivery-form">
            <form action="">
            <div class="lable-text">
                <div class="lable">delivery time:</div>
                <div class="text">mon 22/10/2013, 12:00 - 13:00</div>
            </div>
            <div class="lable-text">
                <div class="lable">delivery address:</div>
                <div class="text">rotchiled 12, tel aviv</div>
            </div>
            
            <div id="floor-apartment">
                <div class="lable-input half-size" id="floor-con">
                    <div class="lable">floor</div>
                    <div class="input"><input type="text" name="floor" id="floor" ></div>
                </div>
                
                <div class="lable-input half-size" id="apartment-con">
                    <div class="lable">apartment</div>
                    <div class="input"><input type="text" name="apartment" id="apartment" ></div>
                </div>
            </div>
            
            <div class="lable-textarea" id="comments-con">
                <div class="lable">comments</div>
                <div class="input"><textarea name="comments" id="comments" ></textarea></div>
            </div>
            
            <div class="lable-input" id="contact-con">
                <div class="lable">contact</div>
                <div class="input"><input type="text" name="contact" id="contact" ></div>
            </div>
            <div class="lable-input" id="mobile-con">
                <div class="lable">mobile</div>
                <div class="input"><input type="text" name="mobile" id="mobile" ></div>
            </div>
            
                
            </form>
        </div>
    </div>
      
    <div class="step step3">
        <div class="title">
            <h2>Order</h2>
            <h1>Summary</h1>
        </div>
        
        <div class="summary">
            <div class="items">
                <span class="lable">Number of Meals</span>
                <span class="value">5</span>
            </div>
            <div class="items">
                <span class="lable">Total</span>
                <span class="value">105</span>
            </div>
            <hr>
            <div>
                <input type="button" value="<?php echo "text_paypal"; ?>" id="paypal" class="button cyanbtn">
                <span class="or">or</span>
                <input type="button" value="<?php echo "text_creditcard"; ?>" id="creditcard" class="button cyanbtn">
            </div>
        </div>
    </div>
    
    <div class="step step4" style="display: none">
        <div class="title">
            <h2>Order</h2>
            <h1>Summary</h1>
        </div>
        
        <div class="summary">
            <div class="items">
                <span class="lable">Number of Meals</span>
                <span class="value">5</span>
            </div>
            <div class="items">
                <span class="lable">Total</span>
                <span class="value">105</span>
            </div>
            <hr>
            <div>
                
            </div>
        </div>
    </div>
</div>

<div style="display: none">
    
</div>


<script>
</script>