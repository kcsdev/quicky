<?php echo $header; ?>
<div id="content"><?php echo $content_top; ?>
  
  <h1 id="conversation-text">
    <a href="<?php echo $my_orders_url; ?>" id="inbox-bread"><?php echo "ההזמנות שלך"; ?></a>
    <span id="conversation-chevron"> > </span>
    <span id="conversation-title"><?php echo "הזמנה מס' ".$order_id; ?></span>
    
    <a class="print-icon" href="index.php?route=account/order/info&print&order_id=<?php echo $order_id; ?>"  target="_blank">
        הדפס <i class="fa fa-print"></i>
    </a>
    <a class="review-icon" href="index.php?route=account/order/reviewOrder&order_id=<?php echo $order_id; ?>" target="_blank">
        דרג הזמנה <i class="fa fa-comment"></i>
    </a>
  </h1>
  
    <div class="order-details-box">
        <div class="order-details-box-title" >
        <span class="status-title"><?php echo "סטטוס הזמנה: "; ?></span>
        <span class="status-name"><?php echo $status_name; ?></span>
        
        </div>
        
        <div class="order-recived <?php echo in_array($status,array(1,5,17,18,19,20,21,22)) ? "on" : ""; ?>">
            <div class="order-recived-pic pic"></div>
            <div class="order-recived-title title"><?php echo "הזמנה התקבלה"; ?></div>
            <div class="order-recived-time time"><?php echo $date_added ; ?></div>
        </div>
        <div class="chevron-right"></div>
        <div class="order-confirmed <?php echo ($aproved_status == "מאושר חלקית" || $aproved_status == "מאושר") ? "on" : ""; ?>">
            <div class="order-confirmed-pic pic"></div>
            <div class="order-confirmed-title title"><?php echo $aproved_status; ?></div>
            
            <?php
            
            $time = false;
            
            foreach($histories as $history){
                if($history['status'] == 'מאושר' ){
                   $time = $history['date_added'];
                }
            }
            
            ?>
            
            <div class="order-confirmed-time time"><?php echo ($time) ? $time : "" ; ?></div>
        </div>
        <div class="chevron-right"></div>
        <div class="order-pickup <?php echo in_array($status,array(18,19,20)) ? "on" : ""; ?>">
            <div class="order-pickup-pic pic"></div>
            
            <?php
            
            $time = false;
            $status_text = "בדרך לסועד";
            
            foreach($histories as $history){
                if($history['status'] == 'בדרך לסועד' ){
                   $time = $history['date_added'];
                }
            }
            
            ?>
            
            <div class="order-pickup-title title"><?php echo $status_text; ?></div>
            <div class="order-pickup-time time"><?php echo ($time) ? $time : "" ; ?></div>
        </div>
        <div class="chevron-right"></div>
        <div class="order-delivered <?php echo in_array($status,array(18)) ? "on" : ""; ?>">
            <div class="order-delivered-pic pic"></div>
            <div class="order-delivered-title title"><?php echo "נמסר"; ?></div>
            
            <?php
            
            $time = false;
            
            foreach($histories as $history){
                if($history['status'] == 'נמסר לסועד' ){
                   $time = $history['date_added'];
                } else {
                    $time = $delivery_time;
                }
            }
            
            if(empty($histories)){
                $time = $delivery_time;
            }
            
            ?>
            
            <div class="order-delivered-time time"><?php echo ($time) ? $time : "" ; ?></div>
        </div>
        <div class="line"></div>
        <div class="zigzag-shadow"></div>
    </div>
  
    <div class="order-details-main">
        
        <div class="order-details-right">
            <div class="order-details-boxy-con">
                <div class="order-details-boxy-title">
                    <?php echo "פרטי משלוח"; ?>
                </div>
                <div class="order-details-boxy">
                    
                    <div class="when">
                        <h2>
                            <?php echo "מתי"; ?>
                            <a class="edit-icon" style="display:none"></a>
                        </h2>
                        <div class="text">
                            <?php echo $order_when; ?>
                        </div>
                    </div>
                
                    <div class="where">
                        <h2>
                            <?php echo "כתובת"; ?>
                            <a class="edit-icon" style="display:none"></a>
                        </h2>
                        <div class="text">
                            <?php echo $payment_address; ?>
                        </div>
                    </div>

                    <div class="contact">
                        <h2>
                            <?php echo "איש קשר"; ?>
                            <a class="edit-icon" style="display:none"></a>
                        </h2>
                        <div class="text">
                            <?php echo $contact; ?><br>
                            <?php echo $phone; ?>
                        </div>
                    </div>
                   
                    <div class="comments">
                        <h2>
                            <?php echo "הערות לשליח"; ?>
                            <a class="edit-icon" style="display:none"></a>
                        </h2>
                        <div class="text">
                            <?php echo (empty($comment)) ? "ללא הערה" : $comment; ?>
                        </div>
                    </div>
                    
                    <div class="forks">
                        <h2>
                            <?php echo 'סכו"ם'; ?>
                            <a class="edit-icon" style="display:none"></a>
                        </h2>
                        <div class="text">
                            <?php echo $order_forks; ?>
                        </div>
                    </div>
                    
                </div>
                <div class="order-details-boxy-shadow"></div>
            </div>
            
            <div class="order-details-boxy-con">
                <div class="order-details-boxy-title">
                    <?php echo "פרטי תשלום"; ?>
                </div>
                <div class="order-details-boxy">
                    
                    <div class="totals">
                        <b><?php echo "עלות"; ?></b><br />
                    <?php foreach ($totals as $total) {
                        if($total['title'] != 'סה"כ'){
                    ?>
                            <div class="total-line">
                            <span class="total-title"><?php echo $total['title']; ?></span>
                            <span class="total-text"><?php echo $total['text']; ?></span>
                            </div>
                    <?php
                        } else {
                    ?>
                            <div class="totaly-line">
                            <span class="totaly-title"><?php echo $total['title']; ?></span>
                            <span class="totaly-text"><?php echo $total['text']; ?></span>
                            </div>
                    <?php  
                        }
                    ?>
                    <?php } ?>
                    </div>
                    
                    <div class="payment-method-con">
                    
                        <div class="payment-method">
                           <div class="payment-method-title"><?php echo $text_payment_method; ?></div>
                        
                           <?php echo $payment_method; ?><br />
                           <?php echo $payment_time; ?>
                           
                        </div>
                    
                    </div>
                    
                </div>
                <div class="order-details-boxy-shadow"></div>
            </div>
            
        </div>
        
        <div class="order-details-left">
            
            
            <?php foreach ($products_cooks as $key => $products) { ?>
            
            <?php
            
            $query = $this->db->query("SELECT *  FROM " . DB_PREFIX . "customer WHERE customer_id = '".$key."'");
            
            $cook_info = $query->row;
            
            if(!empty($cook_info['image'])){
                if (strpos($cook_info['image'], 'http') === 0) {
                    $author_pic = $cook_info['pic_square'];
                }else{
                    $author_pic = $this->model_tool_image->resize($cook_info['image'], 60, 60);
                }
            } else {
                $author_pic = $this->model_tool_image->resize("no-avatar-women.png", 60, 60);
            }
                        
            ?>
            <div class="meals-details-header">
            
            <?php
            
            $quanty = 0;
            
            foreach ($products as $product) { 
            
            $quanty += $product['quantity'];
            
            }
            
            ?>
            
            <?php echo $quanty." "."ארוחות הוזמנו מ".$cook_info['firstname']; ?>
            
            <div class="avatarcon">
				<img class="avatar" src="<?php echo $author_pic; ?>">
                <div class="cook-name"><?php echo $cook_info['firstname']; ?></div>
			</div>
            </div>
            
            
            
            <?php foreach ($products as $product) { ?>
            
            <div class="product-right-img">
                <img src="<?php echo $product['image']; ?>" />
            </div>
            <div class="product-left">
                
                <div class="name-description">
                    <div class="name">
                    <?php echo $product['name']; ?>
                    </div>
                    
                    <?php if ($product['sides'] != "בתוספת: ") { ?>
                    <div class="sides">
                    <?php echo $product['sides']; ?><br>
                    </div>
                    <?php } ?>
                    
                    <?php foreach ($product['option'] as $option) { ?>
                    <?php echo $option['name']; ?>: <?php echo $option['value']; ?><br>
                    <?php } ?>
                </div>
                
                <div class="price-quant">
                    <span class="quant-text"><?php echo "כמות:"; ?></span>
                    <span class="quant"><?php echo $product['quantity']; ?></span>
                    <br>
                    <span class="price-text"><?php echo "מחיר:"; ?></span>
                    <span class="price"><?php echo $product['price']; ?></span>
                    
                </div>
                <?php
                if(!empty($product['comment'])) { ?>
                <div class="comment-meal">
                    <span class="comment-text"><?php echo "הערה לבשלן"; ?></span><br>
                    <span class="comment">"<?php echo nl2br($product['comment']); ?>"</span>
                </div>
                <?php } ?>
            </div>
            
            <?php } ?>
            <?php } ?>
        </div>
        
    </div>
  
  
  
  <?php echo $content_bottom; ?></div>
<?php echo $footer; ?> 