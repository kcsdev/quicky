<?php echo $header; ?>
<div id="content" class="print-width"><?php echo $content_top; ?>
  
  <h1 id="conversation-text">
    <a href="<?php echo $my_orders_url; ?>" id="inbox-bread"><?php echo "ההזמנות שלך"; ?></a>
    <span id="conversation-chevron"> > </span>
    <span id="conversation-title"><?php echo "הזמנה מס' ".$order_id; ?></span>
    
  </h1>
  
  <div class="order-details-main">
        
        <div class="order-details-right">
            <div class="order-details-boxy-con">
                <div class="order-details-boxy-title">
                    <?php echo "פרטי משלוח"; ?>
                </div>
                <div class="order-details-boxy">
                    
                    <div class="when">
                        <h2>
                            <?php echo "מתי"; ?>
                            <a class="edit-icon" style="display:none"></a>
                        </h2>
                        <div class="text">
                            <?php echo $order_when; ?>
                        </div>
                    </div>
                
                    <div class="where">
                        <h2>
                            <?php echo "כתובת"; ?>
                            <a class="edit-icon" style="display:none"></a>
                        </h2>
                        <div class="text">
                            <?php echo $payment_address; ?>
                        </div>
                    </div>

                    <div class="contact">
                        <h2>
                            <?php echo "איש קשר"; ?>
                            <a class="edit-icon" style="display:none"></a>
                        </h2>
                        <div class="text">
                            <?php echo $contact; ?><br>
                            <?php echo $phone; ?>
                        </div>
                    </div>
                   
                    <div class="comments">
                        <h2>
                            <?php echo "הערות לשליח"; ?>
                            <a class="edit-icon" style="display:none"></a>
                        </h2>
                        <div class="text">
                            <?php echo (empty($comment)) ? "ללא הערה" : $comment; ?>
                        </div>
                    </div>
                    
                    <div class="forks">
                        <h2>
                            <?php echo 'סכו"ם'; ?>
                            <a class="edit-icon" style="display:none"></a>
                        </h2>
                        <div class="text">
                            <?php echo $order_forks; ?>
                        </div>
                    </div>
                    
                </div>
                <div class="order-details-boxy-shadow"></div>
            </div>
            
            <div class="order-details-boxy-con">
                <div class="order-details-boxy-title">
                    <?php echo "פרטי תשלום"; ?>
                </div>
                <div class="order-details-boxy">
                    
                    <div class="totals">
                        <b><?php echo "עלות"; ?></b><br />
                    <?php foreach ($totals as $total) {
                        if($total['title'] != 'סה"כ'){
                    ?>
                            <div class="total-line">
                            <span class="total-title"><?php echo $total['title']; ?></span>
                            <span class="total-text"><?php echo $total['text']; ?></span>
                            </div>
                    <?php
                        } else {
                    ?>
                            <div class="totaly-line">
                            <span class="totaly-title"><?php echo $total['title']; ?></span>
                            <span class="totaly-text"><?php echo $total['text']; ?></span>
                            </div>
                    <?php  
                        }
                    ?>
                    <?php } ?>
                    </div>
                    
                    <div class="payment-method-con">
                    
                        <div class="payment-method">
                           <div class="payment-method-title"><?php echo $text_payment_method; ?></div>
                        
                           <?php echo $payment_method; ?><br />
                           <?php echo $payment_time; ?>
                           
                        </div>
                    
                    </div>
                    
                </div>
                <div class="order-details-boxy-shadow"></div>
            </div>
            
        </div>
        
        <div class="order-details-left">
            
            
            <?php foreach ($products_cooks as $key => $products) { ?>
            
            <?php
            
            $query = $this->db->query("SELECT *  FROM " . DB_PREFIX . "customer WHERE customer_id = '".$key."'");
            
            $cook_info = $query->row;
            
            if(!empty($cook_info['image'])){
                if (strpos($cook_info['image'], 'http') === 0) {
                    $author_pic = $cook_info['pic_square'];
                }else{
                    $author_pic = $this->model_tool_image->resize($cook_info['image'], 60, 60);
                }
            } else {
                $author_pic = $this->model_tool_image->resize("no-avatar-women.png", 60, 60);
            }
                        
            ?>
            <div class="meals-details-header">
            
            <?php
            
            $quanty = 0;
            
            foreach ($products as $product) { 
            
            $quanty += $product['quantity'];
            
            }
            
            ?>
            
            <?php echo $quanty." "."ארוחות הוזמנו מ".$cook_info['firstname']; ?>
            
            <div class="avatarcon">
				<img class="avatar" src="<?php echo $author_pic; ?>">
                <div class="cook-name"><?php echo $cook_info['firstname']; ?></div>
			</div>
            
            
            </div>
            
            
            
            <?php foreach ($products as $product) { ?>
            
            <div class="product-right-img">
                <img src="<?php echo $product['image']; ?>" />
            </div>
            <div class="product-left">
                
                <div class="name-description">
                    <div class="name">
                    <?php echo $product['name']; ?>
                    </div>
                    
                    <?php if ($product['sides'] != "בתוספת: ") { ?>
                    <div class="sides">
                    <?php echo $product['sides']; ?><br>
                    </div>
                    <?php } ?>
                    
                    <?php foreach ($product['option'] as $option) { ?>
                    <?php echo $option['name']; ?>: <?php echo $option['value']; ?><br>
                    <?php } ?>
                </div>
                
                <div class="price-quant">
                    <span class="quant-text"><?php echo "כמות:"; ?></span>
                    <span class="quant"><?php echo $product['quantity']; ?></span>
                    <br>
                    <span class="price-text"><?php echo "מחיר:"; ?></span>
                    <span class="price"><?php echo $product['price']; ?></span>
                    
                </div>
                <?php
                if(!empty($product['comment'])) { ?>
                <div class="comment-meal">
                    <span class="comment-text"><?php echo "הערה לבשלן"; ?></span><br>
                    <span class="comment">"<?php echo nl2br($product['comment']); ?>"</span>
                </div>
                <?php } ?>
            </div>
            
            <?php } ?>
            <?php } ?>
        </div>
        
    </div>
  
  <script>
	$(function() {
          window.print();
  	});
</script>
  <?php echo $content_bottom; ?></div>
<?php echo $footer; ?> 