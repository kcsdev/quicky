<?php echo $header; ?>
<div id="content print"><?php echo $content_top; ?>

    <div class="order-details-main">
        
        <div class="order-details-right">
          
            <div class="order-details-boxy-con">
                <div class="order-details-boxy-title">
                    <div class="order-summery-text">
                    <?php echo "סיכום הזמנה"; ?>
                    </div>
                    <div class="avatarcon">
                        <div class="cook-name"><?php echo $contact; ?></div>
                        <img class="avatar" src="<?php echo $customer_img; ?>">
                    </div>
                    
                </div>
                
                <div class="order-details-boxy">
                    
                    <div class="when">
                        <h2>
                            <?php echo "זמן איסוף"; ?>
                            <a class="edit-icon" style="display:none"></a>
                        </h2>
                        <div class="text">
                            <?php echo $order_when; ?>
                        </div>
                    </div>
                    
                    <div class="forks">
                        <h2>
                            <?php echo 'סכו"ם'; ?>
                            <a class="edit-icon" style="display:none"></a>
                        </h2>
                        <div class="text">
                            <?php echo $order_forks; ?>
                        </div>
                    </div>
                    
                    <div class="totals payment-method-con">
                        <b><?php echo "תשלום"; ?></b><br />
                        
                        <div class="total-line ">
                                <span class="total-title"><?php echo 'סה״כ ארוחות'; ?></span>
                                <span class="total-text"><?php echo $all_totals; ?></span>
                        </div>
                        
                        <div class="total-line ">
                                <span class="total-title"><?php echo 'סה״כ עמלות'; ?></span>
                                <span class="total-text"><?php echo $tax_totals; ?></span>
                                
                        </div>
                        
                        <div class="totaly-line ">
                                <span class="totaly-title"><?php echo "הסכום שלך"; ?></span>
                                <span class="totaly-text"><?php echo $cook_totals; ?></span>
                        </div>
                    
                    </div>
                </div>
                <div class="order-details-boxy-shadow"></div>
            </div>
            
            <?php if($messages) { ?> 
            <div class="order-details-boxy-con">
                <div class="order-details-boxy-title">
                    <?php echo "הודעות אחרונות מ".$contact; ?>
                </div>
                <div class="order-details-boxy">
                    <?php foreach($messages as $message) { ?> 
                    <div class="totals">
                        <div class="massage">
                           <?php echo mb_truncate($message['message'],75); ?>
                        </div>
                        <div class="time-date">
                           <?php
                            setlocale(LC_ALL, 'he_IL.UTF-8');
                            echo strftime("%A, %d/%m | %H:%M",strtotime($message['date_created']));
                            ?>
                        </div>
                        <div style="clear: both"></div>
                    </div>
                    <?php } ?>
                </div>
                <div class="order-details-boxy-shadow"></div>
                
                <a class="conv-link" href="<?php echo $conv_link; ?>"> כל ההודעות <i class="fa fa-chevron-left"></i></a>
                
            </div>
            <?php } ?>
            
            <div class="order-details-boxy-con">
                <div class="order-details-boxy-title">
                    <?php echo "פרטי משלוח"; ?>
                </div>
                <div class="order-details-boxy">
                  
                  <div class="contact">
                        <h2>
                            <?php echo "איש קשר"; ?>
                            <a class="edit-icon" style="display:none"></a>
                        </h2>
                        <div class="text">
                            <?php echo $contact; ?><br>
                            <?php echo $phone; ?>
                        </div>
                    </div>
                  
                    <div class="where">
                        <h2>
                            <?php echo "כתובת"; ?>
                            <a class="edit-icon" style="display:none"></a>
                        </h2>
                        <div class="text">
                            <?php echo $payment_address; ?>
                        </div>
                    </div>
                    
                    <div class="comments">
                        <h2>
                            <?php echo "הערות לשליח"; ?>
                            <a class="edit-icon" style="display:none"></a>
                        </h2>
                        <div class="text">
                            <?php echo (empty($comment)) ? "ללא הערה" : $comment; ?>
                        </div>
                    </div>
                </div>
                <div class="order-details-boxy-shadow"></div>
            </div>
            
        </div>
        
        <div class="order-details-left">
            
            
            <?php foreach ($products_cooks as $key => $products) { ?>
            
            <?php
            
            $query = $this->db->query("SELECT *  FROM " . DB_PREFIX . "customer WHERE customer_id = '".$key."'");
            
            $cook_info = $query->row;
            
            if(!empty($cook_info['image'])){
                if (strpos($cook_info['image'], 'http') === 0) {
                    $author_pic = $cook_info['pic_square'];
                }else{
                    $author_pic = $this->model_tool_image->resize($cook_info['image'], 60, 60);
                }
            } else {
                $author_pic = $this->model_tool_image->resize("no-avatar-women.png", 60, 60);
            }
                        
            ?>
            <div class="meals-details-header">
            
            <?php
            
            $quanty = 0;
            
            foreach ($products as $product) { 
            
            $quanty += $product['quantity'];
            
            }
            
            ?>
            
            <?php echo $quanty." "."ארוחות"; ?>
            
            
            </div>
            
            
            
            <?php foreach ($products as $product) { ?>
            
            <div class="product-right-img">
                <img src="<?php echo $product['image']; ?>" />
            </div>
            <div class="product-left">
                
                <div class="name-description">
                    <div class="name">
                    <?php echo $product['name']; ?>
                    </div>
                    
                    <?php if ($product['sides'] != "בתוספת: ") { ?>
                    <div class="sides">
                    <?php echo $product['sides']; ?><br>
                    </div>
                    <?php } ?>
                    
                    <?php foreach ($product['option'] as $option) { ?>
                    <?php echo $option['name']; ?>: <?php echo $option['value']; ?><br>
                    <?php } ?>
                </div>
                
                <div class="price-quant">
                    <span class="quant-text"><?php echo "כמות:"; ?></span>
                    <span class="quant"><?php echo $product['quantity']; ?></span>
                    
                    <span class="price-text"><?php echo "מחיר לארוחה:"; ?></span>
                    <span class="price"><?php echo $product['price']; ?></span>
                    
                    <br>
                    <span class="total-text"><?php echo 'סה"כ:'; ?></span>
                    <span class="total"><?php echo $product['total']; ?></span>
                </div>
        
                <div class="comment-meal">
                    <span class="comment-text"><?php echo "הערה לבשלן"; ?></span><br>
                    <?php if(!empty($product['comment'])) { ?>
                    <span class="comment">"<?php echo $product['comment']; ?>"</span>
                    <?php } else { ?>
                    <span><?php echo "ללא הערה"; ?></span>
                    <?php } ?>
                </div>
        
            </div>
            
            <?php } ?>
            <?php } ?>
        </div>
        
    </div>
  
    <script>
	$(function() {
          window.print();
  	});
</script>
  
  <?php echo $content_bottom; ?></div>
<?php echo $footer; ?> 