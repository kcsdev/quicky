<?php echo $homealscheckout; ?>
</div><!-- content inner div end -->
</section>
<!-- section code end -->

<!-- קוד Google לתג שיווק מחדש -->
<!--------------------------------------------------
אין לשייך תגי שיווק מחדש עם מידע המאפשר זיהוי אישי ואין להציב אותם בדפים הקשורים לקטגוריות רגישות. ראה מידע נוסף והוראות על התקנת התג ב: http://google.com/ads/remarketingsetup
--------------------------------------------------->
<script type="text/javascript">
/* <![CDATA[ */
var google_conversion_id = 960498678;
var google_custom_params = window.google_tag_params;
var google_remarketing_only = true;
/* ]]> */
</script>
<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
</script>
<noscript>
<div style="display:inline;">
<img height="1" width="1" style="border-style:none;" alt="" src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/960498678/?value=0&amp;guid=ON&amp;script=0"/>
</div>
</noscript>

<div style="display: none;">
	
	<div id="join-newsletter">
		<div class="close-btn" onclick="$.colorbox.close();"></div>
		<div id="homepage-footer">
			<div id="footer-newsletter">
				<div class="black-filter">
				<div id="newsletter-title">הירשמו וקבלו הצעות לארוחות</div>
				<div id="newsletter-text">הצטרפו לרשימת התפוצה שלנו וקבלו עידכונים והצעות <span class="mobilebr"><br></span>לארוחות טעימות מדי יום.</div>
				<div id="newsletter-con">
					<input id="newsletter-firstname" name="firstname" type="text" placeholder="שם פרטי" />
					<input id="newsletter-lastname" name="lastname" type="text" placeholder="שם משפחה" />
					<input id="newsletter-email" name="email" type="text" placeholder="דואר אלקטרוני (חובה)" />
					<input id="newsletter-city" name="city" type="text" placeholder="עיר למשלוחים" />
					<input id="newsletter-method" name="method" type="hidden" value="popup box signup" />
					<button id="newsletter-send">הירשם</button>
				</div>
				</div>
				<div class="real-filter">
				</div>
			</div>
			
		</div>
	</div>
	<?php if(false) { ?>
	<div id="roshhashana">
		<div class="close-btn" onclick="$.colorbox.close();">X</div>
		<h1 class="title">
			ראש השנה ב-Homeals
		</h1>
		<div class="roshhashana-con">
			<p>			
				לא בא לכם לבשל לחג? יש לנו תפריט חגיגי בשבילכם במיוחד לראש השנה.<br>
				רק ב-Homeasl תוכלו להרכיב את ארוחת החג ולבחור את מנות שתרצו ממגוון הבשלנים באתר.
			</p>
			<p>
				שימו לב: המשלוחים זמינים לתל אביב, גוש דן, ואזור השרון
			</p>
			
			<a href="index.php?route=product/roshhashana" id="button-dead">לתפריט ראש השנה</a>
			
		</div>
		
	</div>
	<?php } ?>
</div>

<footer id="footer-container">
<div class="footer-inner">
	
	<div id="footer" style="display: none">
	  <?php if ($informations) { ?>
	  <div class="column">
		<h3><?php echo $text_information; ?></h3>
		<ul>
		  <?php foreach ($informations as $information) { ?>
		  <li><a href="<?php echo $information['href']; ?>"><?php echo $information['title']; ?></a></li>
		  <?php } ?>
		</ul>
	  </div>
	  <?php } ?>
	  <div class="custom_footer_main">
			<div class="custom_footer_inner">
				<?php echo $content_footer; ?>
			</div>
		</div>
	  <div class="column">
		<h3><?php echo $text_service; ?></h3>
		<ul>
		  <li><a href="<?php echo $contact; ?>"><?php echo $text_contact; ?></a></li>
		  <li><a href="<?php echo $return; ?>"><?php echo $text_return; ?></a></li>
		  <li><a href="<?php echo $sitemap; ?>"><?php echo $text_sitemap; ?></a></li>
		</ul>
	  </div>
	  <div class="column">
		<h3><?php echo $text_extra; ?></h3>
		<ul>
		  <li><a href="<?php echo $manufacturer; ?>"><?php echo $text_manufacturer; ?></a></li>
		  <li><a href="<?php echo $voucher; ?>"><?php echo $text_voucher; ?></a></li>
		  <li><a href="<?php echo $affiliate; ?>"><?php echo $text_affiliate; ?></a></li>
		  <li><a href="<?php echo $special; ?>"><?php echo $text_special; ?></a></li>
		</ul>
	  </div>
	  <div class="column">
		<h3><?php echo $text_account; ?></h3>
		<ul>
		  <li><a href="<?php echo $account; ?>"><?php echo $text_account; ?></a></li>
		  <li><a href="<?php echo $order; ?>"><?php echo $text_order; ?></a></li>
		  <li><a href="<?php echo $wishlist; ?>"><?php echo $text_wishlist; ?></a></li>
		  <li><a href="<?php echo $newsletter; ?>"><?php echo $text_newsletter; ?></a></li>
		</ul>
		</div>
	  </div>
	 </div>

	 <!--<div id="topofbottomfooter"></div>-->
	 <div id="bottomfooter" style="display: none;">
		
	<div id="bottomfootercon">
	<div id="footer-left">
		
		<ul>
		   <li><b><?php echo $the_service; ?></b></li>
		   <li><a href="<?php echo $how_it_works; ?>"><?php echo $how_it_works; ?></a></li>
		   <li><a href="<?php echo $trust; ?>"><?php echo $trust; ?></a></li>
		   <li><a href="<?php echo $faq; ?>"><?php echo $faq; ?></a></li>	   
		   <li><a href="<?php echo $join_us; ?>"><?php echo $join_us; ?></a></li>
		</ul>
		
		<ul>
			<li><b><?php echo $the_company; ?></b></li>
			<?php foreach ($informations as $information) { ?>
			<li><a href="<?php echo $information['href']; ?>"><?php echo $information['title']; ?></a></li>
			<?php } ?>
		</ul>
		
	</div>
	<div id="footer-right">
		
		<div id="followus">
			<span style="float: left">FOLLOW US &nbsp;&nbsp;</span>
			<a href="http://twitter.com"><div id="twitter"></div></a>
			<a href="http://facebook.com"><div id="facebook"></div></a>
			<a href="http://google.com"><div id="google"></div></a>
		</div>
		
		<div style="clear: both"></div>
		<span>
		<?php echo $rights; ?>
		</span>
		<span style="display: inline-block;">
		<?php echo $rights_ex; ?>
		</span>
		
		
	</div>
	</div> 

	</div>
	
	<div id="homepage-footer">
        <div class="concenter1024">
            <div id="footer-main">
                <div id="footer-main-logo">
                    <img src="image/data/greylogo.png" />
                </div>
		<div style="clear: both"></div>
                <div id="footer-main-links">
                    <ul>
                        <?php foreach ($informations as $information) { ?>
				<li><a href="<?php echo $information['href']; ?>" target="_blank"><?php echo $information['title']; ?></a></li>
			<?php } ?>
			<?php if (false) { ?>
				<li><a href="index.php?route=product/roshhashana" target="_blank">לתפריט ראש השנה</a></li>
			<?php } ?>
                    </ul>
		    <div style="clear: both"></div>
                </div>
		<div style="clear: both"></div>
                <div id="footer-main-followus">
                    <div id="followus-title">עקבו אחרינו</div>
                    <div id="followus-body">
                        <a href="https://www.facebook.com/pages/Homeals-Israel/608776639200255" target="blank" >
				<div id="facebook-follow"></div>
			</a>
			<a href="http://instagram.com/homealsisrael" target="blank" >
				<div id="insta-fllow"></div>
			</a>
			<a href="http://saloona.co.il/homeals/" target="blank" >
				<div id="saloona-fllow"></div>
			</a>
			<div style="clear: both"></div>
                    </div>
			<div class="fb-like" data-href="https://www.facebook.com/HOMEALS" data-width="300px" data-layout="standard" data-action="like" data-show-faces="true" data-share="true"></div>
                </div>
		<div style="clear: both"></div>
            </div>
	    <div style="clear: both"></div>
        </div> 
        <div id="footer-epilog">
            <div class="concenter1024">
                <div class="right">
                                 <span>
		 כל הזכויות שמורות ל- Quiky		</span>
		<span style="display: inline-block;">
		 © 2014		</span>   
                    
                </div>
                <div class="center">&nbsp;|&nbsp;</div>
                <div class="left">
                <span id="epilog-links">
			<a href="index.php?route=information/information&information_id=4"  style="display: none" target="_blank">אודות</a>
			<a href="index.php?route=information/information&information_id=5"  target="_blank">תנאי שימוש</a>
			<a href="index.php?route=information/information&information_id=3"  target="_blank">מדיניות פרטיות</a>
                </span>   
                    
                </div>


            </div>
        </div>
    </div>
	
</footer>


<div id="fb-root"></div>



</div>

<span class="grid_default_width" style="display: none; visibility: hidden;" ></span>
<span class="home_products_default_width" style="display:none; visibility:hidden"></span>
<span class="module_default_width" style="display:none; visibility:hidden"></span>

<div id="button-show-cart"></div>

<script>
	
<?php if ($this->cart->hasProducts())  { ?>
    var addtocart_error = false;
<?php } else { ?>
    var addtocart_error = true;
<?php } ?>
	
	var addtocart_day_error = false;
	var add_to_cart = false;
	var open_review_box = false;
	var open_pm = false;
	var added_now = false;
	
$(document).ready(function() {
	
	//$("#signinpass , #loginpass").placeholder();
	
	
	/********************************
	 *	FOR INTERNET EXPLORER
	 *********************************/
	if( $.browser.msie ) {
		
		$('#loginpass').show();
		$('#loginpass').on("blur", function() {
			$('#loginpass').show();
		});
		
	} 
	
	/********************************
	 *	FOR RESPONSIVE DESIGN - footer (there is also in common)
	 *********************************/
	
	if(/Android|webOS|iPhone|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
		
		$("#logo-homeals img,#hp-header-logo img").attr("src","catalog/view/theme/<?php echo $this->config->get('config_template') ?>/image/homeals/mobile_logo.png");
		$("#logo-homeals img,#hp-header-logo img").show();
		
		$("#slider-con").remove();
		
		$("#supercheckout-columnleft").append($("#columnleft-2"));
		$("#supercheckout-columnleft").append($("#columnleft-1"));
		
		$("#hp-header-logo").append("<div class='open-menu menu-left push-body' ><div></div></div>");
		
		$(".header-homeals>div:first-child").append("<div class='open-menu menu-left push-body' ><div></div></div>");
		
		$('.open-menu').jPushMenu();
		
		$(".header-homeals>div:first-child , #hp-header-logo").append("<div class='go-back' onclick='showLoad();goBack()'><i class='fa fa-angle-right'></i></div>");
		
		$('#container').on("click", function () {
			$('.jPushMenuBtn,body,.cbp-spmenu , #hp-header-con').removeClass('disabled active cbp-spmenu-open cbp-spmenu-push-toleft cbp-spmenu-push-toright');
		});
		
		$("#nav_open_login").on("click", function(e){
			$('.jPushMenuBtn,body,.cbp-spmenu , #hp-header-con').removeClass('disabled active cbp-spmenu-open cbp-spmenu-push-toleft cbp-spmenu-push-toright');
			$("#open_login").colorbox({inline:true});
			$("#open_login").trigger("click");
		});
		
		$("#nav_open_signin").on("click", function(e){
			$('.jPushMenuBtn,body,.cbp-spmenu , #hp-header-con').removeClass('disabled active cbp-spmenu-open cbp-spmenu-push-toleft cbp-spmenu-push-toright');
			$("#open_signin").colorbox({inline:true});
			$("#open_signin").trigger("click");
		});
		
		$(".side-swiper-container").show();
		
		$("#epilog-links").find($("a")).show();
		$("#footer-main-links").html($("#epilog-links"));
		$("#epilog-links").show();
		
		$(".a_menu_needhelp").on("click", function(e){
			$("#menu-needhelp").slideToggle(200);
		});
		
		$("#menu-user").on("click", function(e){
			$("#menu-user-menu").slideToggle(200);
		});
		
		$("#nav-log-me-out").on("click", function(e){
			$("#log-me-out").trigger("click");
		});
		
		var mySwiper = $('.swiper-container').swiper({
					slidesPerView: 'auto',
					mode:'horizontal',
					loop: true
				      });
		
		var sideSwiper = $('.side-swiper-container').swiper({
					slidesPerView: 'auto',
					mode:'horizontal',
					loop: false
				      });
		
		$("div.left.product-image span.image").after('<div class="add-meal-con"><div id="add-meal-btn">הוסף להזמנה</div></div>');
		
		//no facebook like for mobile
		$(".fb-like").remove();
		
		$("iframe[name=\"google_conversion_frame\"]").css("display","none");
		
		$( ".more-meals-cook" ).after( $( "#success-menu-span-cook" ) );
		$( ".more-meals-cat" ).after( $( "#success-menu-span-cat" ) );
		
	} else {
		// only on non mobile browser
		<?php
		if (isset($_GET['route']) && ( $_GET['route'] == 'product/product' || $_GET['route'] == 'seller/catalog-seller/profile' ) ) {
			if(isset($this->session->data['nl_seen_page'])){
				$this->session->data['nl_seen_page'] = $this->session->data['nl_seen_page'] + 1;
			} else {
				$this->session->data['nl_seen_page'] = 1;
			}
		
		if(!$this->customer->isLogged() && $this->session->data['nl_seen_page'] ==  2) {
		?>
		$(window).load(function() {
				
			setTimeout(function(){
				if ($("#colorbox").css("display") == "none") {
					$.colorbox({href:"#join-newsletter",inline:true, open: true
						,onOpen:function(){
							$("body").css("height",$(window).height());
							$("body").css("overflow","hidden");
						}
						,onComplete:function(){
							
						}
						,onClosed:function(){
							$("body").css("height","100%");
							$("body").css("overflow","visible");
						}
					});
				}	
				},10000);
		});
		<?php } } ?>
		

	<?php
	if(false){
		if(isset($this->session->data['roshhashana_seen_page'])){
			$this->session->data['roshhashana_seen_page'] = 2;
		} else {
			$this->session->data['roshhashana_seen_page'] = 1;
		}
		
		if(false && $this->session->data['roshhashana_seen_page'] ==  1) {
		?>
		$(window).load(function() {
				
			setTimeout(function(){
			
				if ($("#colorbox").css("display") == "none") {
					$.colorbox({href:"#roshhashana",inline:true, open: true
						,onOpen:function(){
							$("body").css("height",$(window).height());
							$("body").css("overflow","hidden");
						}
						,onComplete:function(){
							
						}
						,onClosed:function(){
							$("body").css("height","100%");
							$("body").css("overflow","visible");
						}
					});
				}
				
				},500);
		});
		<?php } ?>
	<?php } ?>
		
	}
	
	/********************************
	 *	FOR RESPONSIVE DESIGN END
	 *********************************/
	
	var towpointtwo = false;
	
	<?php if($review_this_meal_id){ ?>
	// need to change the button from this #open_checkout to open this colorbox and the button there to open checkout
	
	<?php if(isset($_GET['opencart'])){ ?>
		towpointtwo = true;
	<?php } ?>
	
	if ($("#colorbox").css("display") != "none") {
		towpointtwo = true;
	}
	
	if (!towpointtwo) {
		// after 7 seconds
		setTimeout(function(){openReviewMeal()},7000);
	} else {
		$("#open_checkout_review").live("click", function() {
			openReviewMeal();
		});
	}
	
	function openReviewMeal(){
		// ajax add cookie once
		var review_href = 'index.php?route=product/product/review_ajax&product_id=<?php echo $review_this_meal_id; ?>';
		$.colorbox({href:review_href, open: true
					,onOpen:function(){
						$("body").css("height",$(window).height());
						$("body").css("overflow","hidden");
					}
					,onComplete:function(){
						
						if (towpointtwo) {
							$("#cancel-review").html("המשך לתשלום ללא דירוג");
							$("#submit-review").html("אשר והמשך לתשלום");
							$("#submit-review").addClass("homeals-button-continue");
							
						}
						
						$(".star-rating").barrating('show', {
							showSelectedRating:false,
							showValues:false,
							showSelectedRating:true
						});
						
						$(".side-select-ajax").on("change",function(){
							var side_id = $(this).val();
							$(this).parent().parent().css("display","none");
							$("#ajax_side_"+side_id).css("display","block");
						});
						
						$("#cancel-review").on("click",function(){
							$.colorbox.close();
							
							if (towpointtwo) {
								setTimeout(function(){$("#open_checkout").trigger("click");},500);
							}
							
						});
							
						$("#submit-review").on("click",function(){
							
							if (!$("#review-box-ajax").find("select").first().val()) {
								alert("יש להזין דירוג");
								return false;
							}
							
							var star = {};
							var text = {};
							
							var i = 0;
							
							$("#review-box-ajax").find("select").each(function() {
								if ($(this).val()) {
									star[$(this).attr("id")] = $(this).val();
								}
							});
							
							$("#review-box-ajax").find("textarea").each(function() {
								text[$(this).attr("id")] = $(this).val();
								text_data = $(this).val();
							});
							
							var data = {
								"stars" : star,
								"texts" : text
							}
							
							$.ajax({
								url: 'index.php?route=product/product/writemulti',
								type: 'post',
								dataType: 'json',
								data: data,
								beforeSend: function() {
									$('#submit-review').attr('disabled', true);
									showLoadBox();
								},
								complete: function() {
									$('#submit-review').attr('disabled', false);
									hideLoadBox();
								},
								success: function(data) {
									if (data['error']) {
										//$('#review-title').after('<div class="warning">' + data['error'] + '</div>');
									}
									
									if (data['success']) {
										//showLoadPage();
										$.colorbox.close();
										
										if (towpointtwo) {
											setTimeout(function(){$("#open_checkout").trigger("click");},500);
										}
										
									}
								}
							});
						});
					}
					,onClosed:function(){
						$("body").css("height","100%");
						$("body").css("overflow","visible");
					}
			});
	}
	<?php } ?>
	
	
	$('#time-of-day').on("change", function() {
		addtocart_error = false;
		$(this).parent().css("border","1px solid #e7e3d9");
	});
	
	$('#day').on("change", function() {
		addtocart_day_error = false;
		addtocart_error = true;
		$(this).parent().css("border","1px solid #e7e3d9");
	});

	$('#button-show-cart').live('click', function() {
		$.ajax({
			url: 'index.php?route=checkout/cart/show',
			type: 'post',
			dataType: 'json',
			success: function(json) {
				if (json['success']) {
					var recent_items = json['recent_items'];
					
					if( $.browser.msie ) {
						//for IE
						var item_one = recent_items[0];
						if (typeof recent_items[1] != "undefined") {
							var item_two = recent_items[1];
						} else {
							var item_two = "";
						}
						
					} else {
						var item_one = recent_items[ Object.keys(recent_items).sort().pop() ];
						
						if (typeof recent_items[ Object.keys(recent_items).sort().pop() -1 ] != "undefined") {
							var item_two = recent_items[ Object.keys(recent_items).sort().pop() -1 ];
						} else {
							var item_two = "";
						}
					}
					
					$('#notification').addClass("isDown");
					
					var open_checkout = '<a href="#mealcheckout" id="open_checkout"><?php echo $text_checkout ?></a>';
					
					if (towpointtwo) {
						open_checkout = '<a id="open_checkout_review"><?php echo $text_checkout ?></a><a href="#mealcheckout" id="open_checkout" style="display:none;">open</a>';
					}
					
					$('#notification').html('<div class="success" style="display: none;"><div class="itemsinbag"><div class="bagitems"><div class="bagitems-left"><span class="number"> ' + json['total_number'] + '</span><span class="text"><?php echo $text_items_in_bag ?></span></div><div class="bagitems-right"><img src="catalog/view/theme/OPC040088/image/homeals/plus.png" /><span class="text"><?php echo $text_add_meal ?></span></div></div><div class="recentadded"><div class="avatar"><img src="'+json['cook_image']+'" /><span>'+json['cook_name']+'</span></div><h2><?php echo $text_recent_meal_added ?>:</h2>' + item_one + item_two +'</div></div><div class="summary"><div class="summary-up"><h2><?php echo $text_address_time ?>:</h2><span><?php echo htmlentities($delivery_address, ENT_QUOTES, "UTF-8"); ?>'+" / "+json['day']+", "+json['time']+'</span></div><div class="summary-down"><h2><?php echo $text_total ?>: <span class="cash"><?php echo $ils ?>' + json['total_cash'] + '</span></h2><div class="viewcart"><a id="viewcart"  class="emptybtn"><?php echo $text_empty_cart ?></a></div><div class="checkout">'+ open_checkout +'</div></div></div><div class="closebtn" style="display: none;"><img src="catalog/view/theme/OPC040088/image/homeals/x_white.png" alt="<?php echo $text_close ?>" /></div><div class="closedcart" id="closed-cart-button" ><div class="avatar"><img src="'+json['cook_image']+'" /><span>'+json['cook_name']+'</span></div><div class="bagitems-leftt"><span class="number"> ' + json['total_number'] + '</span><span class="text"><?php echo $text_items_in_bag ?></span></div><div class="clear"></div></div>');
					
					<?php if(isset($_GET['opencart'])) { ?>
					
					added_now = true;
					
					<?php } ?>
					
					if (added_now) {
						//$('.closedcart').fadeIn('slow');
						$('.success').fadeIn('slow');
						
						$('#notification').fadeIn(10, function() {
							$('.closedcart').trigger("click");
						});
					} else {
						
						$('.closedcart').fadeIn('slow');
						$('.success').fadeIn('slow');
						$('#notification').fadeIn('slow');
					}

						
					$('#cart-total').html(json['total']);
					
					
					//console.log($('#notification').outerHeight());
					var down_height = $('#notification').outerHeight() - 9;
					$('#notification').css("bottom",-down_height);
					
					/* $('html, body').animate({ scrollTop: 0 }, 'slow'); */
					
					
				}	
			}
		});	
	});
	
	$('#button-cart').live('click', function(e) {
		e.preventDefault();
		
		if (!addtocart_error && !addtocart_day_error) {
			$.ajax({
				url: 'index.php?route=checkout/cart/add',
				type: 'post',
				data: $('.product-info input[type=\'text\'], .product-info input[type=\'hidden\'], .product-info input[type=\'radio\']:checked, .product-info input[type=\'checkbox\']:checked, .product-info select, .product-info textarea'),
				dataType: 'json',
				beforeSend: function() {
					//show loading
					$(".product_under-img-con").prepend('<div class="loading-box" id="meal-box-load"></div>');
					$('#button-cart').attr("disabled", true);
				},
				complete: function() {
					//hide loading
					$("#meal-box-load").remove();
					$('#button-cart').removeAttr("disabled");
				},
				success: function(json) {
					$('.success, .warning, .attention, information, .error').remove();
					
					if (json['error']) {
						if (json['error']['option']) {
							for (i in json['error']['option']) {
								$('#option-' + i).after('<span class="error">' + json['error']['option'][i] + '</span>');
							}
						}
						
						if (json['error']['profile']) {
							$('select[name="profile_id"]').after('<span class="error">' + json['error']['profile'] + '</span>');
						}
						
						// blocks crazy requests
						if (json['error']['no_meal']) {
							alert("אין באפשרותך להוסיף את מוצר זה זמנו עבר או אזל המלאי");
							$('#button-show-cart').trigger("click");
						}
						
						if (json['error']['wrong_date']) {
							alert("תאריך המוצר שהינך מנסה להוסיף אינו תואם את תאריך המוצרים שכבר בעגלה, כדי להוסיף אנא בחר בתאריך המתאים.");
							$('#button-show-cart').trigger("click");
						}
						
						if (json['error']['wrong_time']) {
							alert("זמן המוצר שהינך מנסה להוסיף אינו תואם את זמן המוצרים שכבר בעגלה, כדי להוסיף אנא בחר בזמן המתאים.");
							$('#button-show-cart').trigger("click");
						}
					}
					
					if (json['success']) {
						
						if (add_to_cart) {
							location.reload();
						} else {
							added_now = true;
							$('#button-show-cart').trigger("click");
							//refreshAll();
						}
						
						$("#day").trigger("change");
						
						$("#quantity-input").val(1);
						
						$("#day").attr("disabled", true);
						$("#time-of-day").attr("disabled", true);
						
						$(".edit-box input[name='option[time-of-day]']").val($("#time-of-day").val());
						
						/* $('html, body').animate({ scrollTop: 0 }, 'slow'); */
					}	
				}
			});
		}
		
		
		var alert_msg = "";
		
		if (addtocart_day_error) {
			$("#day").parent().css("border","1px solid #e7604a");
			alert_msg = "בחר יום לביצוע ההזמנה";
		}
		
		if (addtocart_error) {
			$("#time-of-day").parent().css("border","1px solid #e7604a");
			
			if (addtocart_day_error) {
				alert_msg = "בחר יום ושעה לביצוע ההזמנה";
			} else {
				alert_msg = "בחר שעה לביצוע ההזמנה";
			}
		}
		
		if (alert_msg != "") {
			alert(alert_msg);
		}
	});
	
	$('#button-show-cart').trigger("click");
	
	$(".close-btn").on("click", function(){
		$.colorbox.close();
	});
	
	
	
	$("#loginbutton").live("click", function(e){
		
		var _email = $("#loginemail").val();
		var _pass = $("#loginpass").val();
		var _remmber_me = $('#rememberme').is(":checked");
		
		if (_email == "" || _pass == "") {
			$(".logbox-warning").css("visibility","visible");
			$("#loginemail").css("border","1px solid #F00");
			$("#loginpass").css("border","1px solid #F00");
		}else{
			$(".logbox-warning").css("visibility","hidden");
			$("#loginemail").css("border","1px solid #DDDDDD");
			$("#loginpass").css("border","1px solid #DDDDDD");
			
			$.ajax({
				type: "POST",
				url: "index.php?route=account/login/ajax_validate/",
				data: { email: _email, password: _pass , remmber_me: _remmber_me},
				cache: false,
			        beforeSend: function () {
						$(".loading-screen").show();
					}
				}).done(function( msg ) {
					if (msg == "logedin") {
						
						var redirect = "<?php echo (isset($this->session->data['redirect']) ? $this->session->data['redirect'] : "" ); ?>";
						var redirect_decoded = redirect.replace(/&amp;/g, '&');
						
						$('#button-login-fb').attr("id","button-cart");
							
						//$('.emptybtn').trigger("click");
						
						if (add_to_cart) {
							
							$.ajax({
							    url: 'index.php?route=checkout/cart/clear',
							    success:function(){
									if ($("#time-of-day").val() != "") {
										$('#button-cart').trigger("click");
									}
								location = location.href + "&opencart";
							    }
							});
							
						} else if (open_review_box) {
							location = location.href + "&review";
						} else  if (open_pm) {
						       location = location.href + "&openpm";
						} else {
							if (!redirect) {
								location.reload(true);
							} else {
								location = redirect_decoded;
							}
						}
						
					} else if(msg == "error_login") {
						$(".loading-screen").hide();
						
						$(".logbox-warning").css("visibility","visible");
						$("#loginemail").css("border","1px solid #F00");
						$("#loginpass").css("border","1px solid #F00");
					}
				});	
		}
		
	});
	
	
	
	
	
	
	/* HOME PAGE */
	
	$(".hp-howitworks").live("click", function() {
		window.location = "index.php?route=information/information&information_id=7";
	});
	
	$(".hp-joinus").live("click", function() {
		window.location = "index.php?route=information/information&information_id=9";
	});
	
	$(".hp-login").live("click", function(e){
		$("#open_login").trigger("click");
	});
	
	/* HOME PAGE END */
	
	<?php if(isset($_GET['openpm']) && $this->customer->isLogged()) { ?>
	
	$('#button-pm').trigger("click");
	
	<?php } ?>
	
	<?php if(isset($_GET['open_login']) && !$this->customer->isLogged()) { ?>
	$(window).load(function() {
		$("#open_login").colorbox({inline:true});
		$("#open_login").trigger("click");
	});
	<?php } ?>
	
	<?php if(isset($_GET['open_signin']) && !$this->customer->isLogged()) { ?>
	$(window).load(function() {
		$("#open_signin").colorbox({inline:true});
		$("#open_signin").trigger("click");
	});
	<?php } ?>
	
	function ajax_newsletter(event) {
		
		$("#newsletter-send").attr("id","newsletter-send-off");
		
		event.stopPropagation();
		event.preventDefault();
		
		var _email = $("#newsletter-email").val(),
		    _first = $("#newsletter-firstname").val(),
		    _last = $("#newsletter-lastname").val(),
			_city = $("#newsletter-city").val(),
			_method = $("#newsletter-method").val();
		
		if (!(_email == "")) {
		    $.ajax({
			type: "POST",
			url: "index.php?route=product/product/SubscribeToNewsletter",
			data: { email: _email, first: _first, last: _last ,city: _city, method: _method},
			dataType: 'json',
			beforeSend: function () {
				// show loading
			}
			  }).done(function( msg ) {
				
			    if (msg.success.status != "error") {
					$("#newsletter-con").fadeOut(function() {
						$(this).html("<h1 style='color: #FFFFFF;'>תודה שהצטרפתם לרשימת התפוצה שלנו. הצעות לארוחות טעימות עושות דרכן לתיבת המייל שלכם. בתיאבון.</h1>").fadeIn();
						
						setTimeout(function(){
							$.colorbox.close();
						},2000);
						
					});
			    } else {
					alert(msg.success.error);
					$("#newsletter-send-off").attr("id","newsletter-send");
			    }
		    });
		} else {
			$("#newsletter-send-off").attr("id","newsletter-send");
			$("#newsletter-email").addClass("error");
		}
	}
	
	$("#newsletter-send").live("click", ajax_newsletter);
	
	setTimeout(function(){
		window.location.reload();   
	},600000);
});
</script>

</body></html>