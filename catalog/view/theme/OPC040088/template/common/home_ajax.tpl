<div id="hp-header-con">
    <div id="black-line"></div>
    <div id="homepage-header" >
        <div class="concenter1024">
            <div id="hp-header-logo"><img src="<?php echo $logo; ?>" /></div>
            
            <div id="hp-header-search"></div>
            
            <div id="hp-header-links">
                <div class="hp-joinus">הצטרפו כבשלנים</div>
                <div class="hp-login">התחבר</div>
                <div class="hp-howitworks">איך זה עובד?</div>
            </div>
        </div>
    </div>
    <div class="headershadow"></div>
</div>

<div id="homepage-con">
    <div id="header-placeholder"></div>
    <div id="search">
        <div id="slider-con">
            <div id="slider1_container" style="position: relative; margin: 0 auto;
                    top: 0px; left: 0px; width: 1600px; height: 600px; overflow: hidden;">
                    <!-- Loading Screen -->
                    <div u="loading" style="position: absolute; top: 0px; left: 0px;">
                        <div style="filter: alpha(opacity=70); opacity: 0.7; position: fixed; display: block;
                            top: 0px; left: 0px; width: 100%; height: 100%;">
                        </div>
                        <div style="position: fixed; display: block; background: #000 url(../img/loading.gif) no-repeat center center;
                            top: 0px; left: 0px; width: 100%; height: 100%; z-index: 99999;">
                        </div>
                    </div>
                    <!-- Slides Container -->
                    <div u="slides" style="position: absolute; left: 0px; top: 0px; width: 1600px;
                        height: 600px; overflow: hidden;">
                        
                        <?
                        $i = 0;
                        foreach($meals as $meal) {
                            $i++;
                        ?>
                        <div>
                            <img u="image" src="<?php echo $meal['image']; ?>" />
                            
                            <div class="shadow" style="height: 100%; width: 100%;">
                                <div style="width:1024px; margin: auto; height: 100%; position: relative;">
                                    <div style="height: 180px; width: 220px; position: absolute; bottom: 0; left:30px;">
                                        <div class="s-cook-avatar">
                                            <img class="s-image-avatar" src="<?php echo $meal['cook_image']; ?>">
                                        </div>
                                        <div class="s-cook-name"><?php echo $meal['cook_name'].", ".$meal['cook_city']; ?></div>
                                        <div class="s-meal-name"><?php echo $meal['name']; ?></div>
                                        <div class="s-meal-price"><?php echo $meal['price']; ?></div>
                                    </div>
                                    
                                </div>
                            </div>
                            
                        </div>
                        <?php } ?>
                        
                    </div>
                    
                    <!-- Bullets Navigator -->
                    <div u="navigator" class="jssorb21" style="position: absolute; bottom: 26px; left: 6px;">
                        <div u="prototype" style="POSITION: absolute; WIDTH: 19px; HEIGHT: 19px; text-align:center; line-height:19px; color:White; font-size:12px;"></div>
                    </div>
                    <!-- Arrow Navigator -->
                    <span u="arrowleft" class="jssora21l" style="width: 55px; height: 55px; top: 123px; left: 8px;">
                    </span>
                    <span u="arrowright" class="jssora21r" style="width: 55px; height: 55px; top: 123px; right: 8px">
                    </span>
                    <!-- Arrow Navigator Skin End -->
                    <a style="display: none" href="http://www.jssor.com">jQuery Slider</a>
                </div>
        </div>
        
        <div id="search-controller-con">
        
        <div class="back-img"></div>
            
        <div id="search-controller">
            <div id="title">
                מישהו מבשל בשבילך
            </div>
            <div id="subtitle">
                ארוחה טעימה ומזינה מבשלנים מוכשרים ישירות לשולחן שלך
            </div>
            <div id="controller">
                <input type="text" name="search" id="hp-search-input" placeholder="הקלידו כתובת למשלוח בתל אביב"  onfocus="this.placeholder = ''" onblur="this.placeholder = $('#input-search-homeals').attr('placeholder') " />
                <button id="hp-search-btn">חפש ארוחות</button>
            </div>
        </div>
        </div>
        
    </div>
    
    <div id="info">
        <div class="concenter1024">
            <div class="info-title">פשוט להזמין, טעים לאכול</div>
            <div class="dubbleline"></div>
            <div class="info-body" id="info-one">
                <div class="info-body-number"><a href="<?php echo $cat_link; ?>">1</a></div>
                <div class="info-body-title"><a href="<?php echo $cat_link; ?>">בוחרים</a></div>
                <div class="info-body-text">בחרו ארוחת צהריים או ערב ממגוון ארוחות שמשתנה מדי יום</div>
            </div>
            <div class="info-body" id="info-two">
                <div class="info-body-number"><a href="<?php echo $cat_link; ?>">2</a></div>
                <div class="info-body-title"><a href="<?php echo $cat_link; ?>">מזמינים</a></div>
                <div class="info-body-text">הזמינו ארוחה להיום או להמשך השבוע. המשלוח יגיע בשעה הנוחה לכם, לבית או למשרד</div>
            </div>
            <div class="info-body" id="info-three">
                <div class="info-body-number"><a href="<?php echo $cat_link; ?>">3</a></div>
                <div class="info-body-title"><a href="<?php echo $cat_link; ?>">אוכלים</a></div>
                <div class="info-body-text">הארוחה תגיע באריזה המיועדת לחימום קצר במיקרוגל. זהו, הכל מוכן. בתיאבון</div>
            </div>
            <div style="clear: both"></div>
            <div class="info-fotter"><a href="index.php?route=information/information&information_id=7">איך זה עובד ></a></div>
        </div>
    </div >
    
    <div id="cooks">
        <div class="concenter1024">
            <div class="cooks-title">הכירו את הבשלנים</div>
            <div class="singleline"></div>
            <div class="singleline"></div>
            <?php
            $i = 1;
            $string_1 = "אני לא מרסן את עצמי כשמדובר באוכל, יום אחד אני מבשל איטלקי, יום אחד מרוקאי, יום אחד צמחוני, ועוד.  הסיפוק הזה שבלקחת חומרי גלם ולהפוך אותם למנה מיוחדת, הוא אדיר, וכשמישהו אחר אוכל ונהנה מזה כפי שאני נהניתי להכין, זו בכלל הרגשה מטורפת...";
            
            $string_2 = "את אהבתי לבישול קיבלתי מאימי ומסבתי. כבר בגיל צעיר הייתי מקשיבה בלהט לסיפורי המטבח של אמי מימי נעוריה באלכסנדריה. עיקר דרכי עבודתי במטבח, שמירה על טריות ונקיון המוצרים וכן טכניקות בישול והשבחת התבשיל, מבוססים על רקע זה, של אמי ומשפחתה...";
            
            $string_3 = "אני אוהב אנשים, אוהב אוכל ואוהב לראות אנשים נהנים ממה שבישלתי ולקבל פידבק חיובי. הכיף שלי הוא להשתמש בחומרי גלם טריים ולהוסיף טאצ' לכל ארוחה. כך שכל מי שאכל מהארוחה שהכנתי יתרווח בסיומה וישמח בחלקו על הפשטות שבאוכל, הפשטות שבאהבה";
            
            foreach($sellers as $cook) {
                ?>
            <div class="cooks-body" id="cooks-<?php echo ($i == 1 ? 'one' : ($i == 2 ? 'two' : 'three')) ; ?>">
                <div class="cooks-body-picture">
                    <img src="<?php echo $cook['image']; ?>" />
                    <div id="shadow"></div>
                </div>
                <div class="cooks-body-name"><?php echo $cook['name']; ?>, <?php echo $cook['city']; ?></div>
                <div class="cooks-body-sp"><?php echo $cook['sp']; ?></div>
                <div class="cooks-body-text">"<?php echo ($i == 1 ? $string_1 : ($i == 2 ? $string_2 : $string_3)); ?>"</div>
            </div>
            <?php
            $i++;
            }
            ?>
            
            <div style="clear: both"></div>
            <div class="cooks-fotter"><a href="index.php?route=information/information&information_id=9">הצטרפו כבשלנים ></a></div>
        </div>
    </div>
    
    <div id="health">
        <div class="concenter1024">
            <div class="health-title">בריאות ובטיחות מעל הכל</div>
            <div class="dubbleline"></div>
            <div class="health-body" id="cooks-one">
                <div class="health-body-logo"></div>
                <div class="health-body-text">
                    בריאות האישית שלכם כסועדים חשובה יותר מכל. לכן אנחנו משקיעים את מירב המאמצים בכדי להבטיח שתוכלו להזמין ללא חשש
                </div>
            </div>
            <div style="clear: both"></div>
            <div class="health-fotter"><a href="index.php?route=information/information&information_id=8">קראו עוד על אמון ובטיחות ></a></div>
        </div>
    </div>
    
    <div id="homepage-footer">
        <div id="footer-newsletter">
            <div class="black-filter">
            <div id="newsletter-title">הירשמו וקבלו הצעות לארוחות</div>
            <div id="newsletter-text">הצטרפו לרשימת התפוצה שלנו וקבלו עידכונים והצעות לארוחות טעימות מדי יום.</div>
            <div id="newsletter-con">
                <input id="newsletter-firstname" name="firstname" type="text" placeholder="שם פרטי" />
                <input id="newsletter-lastname" name="lastname" type="text" placeholder="שם משפחה" />
                <input id="newsletter-email" name="email" type="text" placeholder="דואר אלקטרוני (חובה)" />
                <button id="newsletter-send">הירשם</button>
            </div>
            </div>
            <img src="<?php echo $img_dir."homeals/newsletterbg.jpg"; ?>" style="width: 100%;" />
        </div>
        
    </div>
</div>

<script>
    $(document).ready(function ($) {
        var ajx_options = { serviceUrl:'system/helper/autocomplete.php',
			// callback function:
			onSelect: function(data){
				
				if (data.value != $("#input-search-homeals").val()) {
                                        $(".loading-screen").show();
					window.location = "index.php?route=product/category&path=0&filter_place="+data.value;
				}
				
			}
		};
		
	$("#hp-search-input").autocomplete(ajx_options);
        
        var options = {
                $FillMode: 2,                                       //[Optional] The way to fill image in slide, 0 stretch, 1 contain (keep aspect ratio and put all inside slide), 2 cover (keep aspect ratio and cover whole slide), 4 actuall size, default value is 0
                $AutoPlay: true,                                    //[Optional] Whether to auto play, to enable slideshow, this option must be set to true, default value is false
                $AutoPlayInterval: 4000,                            //[Optional] Interval (in milliseconds) to go for next slide since the previous stopped if the slider is auto playing, default value is 3000
                $PauseOnHover: 3,                                   //[Optional] Whether to pause when mouse over if a slider is auto playing, 0 no pause, 1 pause for desktop, 2 pause for touch device, 3 pause for desktop and touch device, default value is 3

                $ArrowKeyNavigation: true,   			            //[Optional] Allows keyboard (arrow key) navigation or not, default value is false
                $SlideEasing: $JssorEasing$.$EaseOutQuart,          //[Optional] Specifies easing for right to left animation, default value is $JssorEasing$.$EaseOutQuad
                $SlideDuration: 1200,                                //[Optional] Specifies default duration (swipe) for slide in milliseconds, default value is 500
                $MinDragOffsetToSlide: 20,                          //[Optional] Minimum drag offset to trigger slide , default value is 20
                $SlideSpacing: 0, 					                //[Optional] Space between each slide in pixels, default value is 0
                $DisplayPieces: 1,                                  //[Optional] Number of pieces to display (the slideshow would be disabled if the value is set to greater than 1), the default value is 1
                $ParkingPosition: 0,                                //[Optional] The offset position to park slide (this options applys only when slideshow disabled), default value is 0.
                $UISearchMode: 1,                                   //[Optional] The way (0 parellel, 1 recursive, default value is 1) to search UI components (slides container, loading screen, navigator container, arrow navigator container, thumbnail navigator container etc).
                $PlayOrientation: 1,                                //[Optional] Orientation to play slide (for auto play, navigation), 1 horizental, 2 vertical, default value is 1
                $DragOrientation: 0,                                //[Optional] Orientation to drag slide, 0 no drag, 1 horizental, 2 vertical, 3 either, default value is 1 (Note that the $DragOrientation should be the same as $PlayOrientation when $DisplayPieces is greater than 1, or parking position is not 0)


                $BulletNavigatorOptions: {                                //[Optional] Options to specify and enable navigator or not
                    $Class: $JssorBulletNavigator$,                       //[Required] Class to create navigator instance
                    $ChanceToShow: 2,                               //[Required] 0 Never, 1 Mouse Over, 2 Always
                    $AutoCenter: 1,                                 //[Optional] Auto center navigator in parent container, 0 None, 1 Horizontal, 2 Vertical, 3 Both, default value is 0
                    $SpacingX: 8,                                   //[Optional] Horizontal space between each item in pixel, default value is 0
                    $SpacingY: 8,                                   //[Optional] Vertical space between each item in pixel, default value is 0
                    $Orientation: 1                                 //[Optional] The orientation of the navigator, 1 horizontal, 2 vertical, default value is 1
                },

                $ArrowNavigatorOptions: {                       //[Optional] Options to specify and enable arrow navigator or not
                    $Class: $JssorArrowNavigator$,              //[Requried] Class to create arrow navigator instance
                    $ChanceToShow: 1,                               //[Required] 0 Never, 1 Mouse Over, 2 Always
                    $AutoCenter: 2,                                 //[Optional] Auto center arrows in parent container, 0 No, 1 Horizontal, 2 Vertical, 3 Both, default value is 0
                    $Steps: 1                                       //[Optional] Steps to go for each navigation request, default value is 1
                }
            };

            var jssor_slider1 = new $JssorSlider$("slider1_container", options);

            ////responsive code begin
            ////you can remove responsive code if you don't want the slider scales while window resizes
            function ScaleSlider() {
                var bodyWidth = document.body.clientWidth;
                if (bodyWidth)
                    jssor_slider1.$SetScaleWidth(Math.min(bodyWidth, 1920));
                else
                    window.setTimeout(ScaleSlider, 30);
            }
            
            ScaleSlider();
            
            if (!navigator.userAgent.match(/(iPhone|iPod|iPad|BlackBerry|IEMobile)/)) {
                $(window).bind('resize', ScaleSlider);
            }
            ////responsive code end
            
            $("#hp-header-search").html($("#search-homeals"));
            
            $("#hp-search-input").attr("placeholder",($("#input-search-homeals").attr("placeholder")));
            
            // showing search on scroll
            $(window).on("scroll", function() {
                var fromTop = $("body").scrollTop();
                if (fromTop > 265) {
                    $('#hp-header-search').fadeIn("fast");
                } else {
                    $('#hp-header-search').fadeOut("fast");
                }
            });
            
            function ajax_newsletter() {
                $("#newsletter-send").off();
                
                var _email = $("#newsletter-email").val(),
                    _first = $("#newsletter-firstname").val(),
                    _last = $("#newsletter-lastname").val();
                
                if (!(_email == "")) {
                    $.ajax({
                        type: "POST",
                        url: "/system/helper/ravcode.php",
                        data: { email: _email, first: _first, last: _last }
                          }).done(function( msg ) {
                            if (msg == "success") {
                                $("#newsletter-con").fadeOut(function() {
                                    $(this).html("<h1 style='color: #FFFFFF;'>תודה שהצטרפתם לרשימת התפוצה שלנו. הצעות לארוחות טעימות עושות דרכן לתיבת המייל שלכם. בתיאבון.</h1>").fadeIn();
                                });
                            } else {
                                $("#newsletter-send").live("click",ajax_newsletter);
                            }
                    });
                } else {
                    $("#newsletter-send").live("click",ajax_newsletter);
                }
                
                
            }
            
            $("#newsletter-send").live("click", ajax_newsletter);
            
            <?php if($logged) { ?>
            $("#hp-header-links").html($("#welcome-homeals").html());
            $("#hp-header-links").attr("id","welcome-homeals");
            <?php } ?>
    });
</script>
