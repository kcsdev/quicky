<?php echo $header; ?>
<div id="thank-you-banner" class="kill-select">
    <?php echo 'ההזמנה נקלטה במערכת'; ?>
    <div class="zigzag-shadow"></div>
</div>

<div id="content" style="margin-top: 140px;">
    <?php echo $content_top; ?>
    
    <div class="thank-you-right">
        <h1><?php echo $text_order_summary; ?></h1>
        <div class="order-details-boxy">
            <div class="order-details">
                <h2>
                    <?php echo $text_order_details; ?>
                </h2>
                <div class="text">
                    <?php echo $order_time; ?>
                    <br>
                    <?php echo str_replace('#','',$order_address); ?>
                </div>
            </div>
            
            <div class="order-ordered">
                <h2>
                    <?php echo 'ארוחות בהזמנה'; ?>
                </h2>
                <div class="text">
                    <?php echo $text_order_orderd; ?><br>
                    <?php foreach ($cooks as $cook) { ?>
                        <div class="cook-con">
                            <img src="<?php echo $cook['image'] ?>" >
                        </div>
                    <?php } ?>
                </div>
            </div>
            <!--
            <div class="totals">
                <h2>
                    <?php echo $text_order_total; ?>
                </h2>
                <?php foreach ($totals as $total) {
                    if($total['title'] != 'סה"כ'){
                ?>
                    <div class="total-line">
                    <span class="total-title"><?php echo $total['title']; ?></span>
                    <span class="total-text"><?php echo $total['text']; ?></span>
                    </div>
                <?php } else { ?>
                    <div class="totaly-line">
                    <span class="totaly-title"><?php echo $total['title']; ?></span>
                    <span class="totaly-text"><?php echo $total['text']; ?></span>
                    </div>
                <?php } ?>
            <?php } ?>
            </div>
             -->
            <div class="order-link">
                <a id="button-favorite" href="?route=account/order/info&order_id=<?php echo $order_id; ?>" style="margin: auto;display: inline-block;line-height: 43px;">לפרטי ההזמנה</a>
            </div>
        </div>
        <div class="order-details-boxy-shadow-full"></div>
        <div class="money-back"></div>
    </div>
    
    <div class="thank-you-main">
        
        <div class="order-is-in">
            <h1 class="homeals-h1" ><?php echo "ההזמנה נקלטה ונשלחה לבשלנים לאישור"; ?></h1>
            <hr>
            <p class="bigger">
                <?php echo "הודעה על אישור ההזמנה תשלח אליכם במייל. כמו כן באפשרותכם לעקוב אחר ההזמנה במסך "; ?>
                <a href="?route=account/order/info&order_id=<?php echo $order_id; ?>">
                <?php echo "פרטי הזמנה"; ?>
                </a>
            </p>
            
            <h1 class="link-homeals">
                <?php echo "אוהבים את Homeals? שתפו עם החברים בפייסבוק"; ?>
            </h1>
            
            <p class="bigger mobile-hide">
                <?php echo "ספרו לחברים על ארוחת הצהריים שהזמנתם ועזרו לנו להפיץ את הבשורה."; ?>
            </p>
            
             <a href="https://www.facebook.com/sharer/sharer.php?app_id=687143061308736&sdk=joey&u=http%3A%2F%2Fhomeals.com%2Findex.php%3Froute%3Dproduct%2Fproduct%26path%3D0%26product_id%3D<?php echo $share_meal_id; ?>%2F&display=popup&ref=plugin" onclick="window.open(this.href, 'mywin',
'left=150,top=50,width=660,height=360,toolbar=1,resizable=0'); return false;" class="friends-button">שתפו בפייסבוק</a>
             
            <div class="pre-order-deal">
                <h1 class="homeals-success-h1" ><?php echo 'למה לחכות לרגע האחרון? הזמינו כבר עכשיו לימים הקרובים'; ?></h1>
                <p class="bigger">
                <?php echo "באפשרותכם להקדים ולבצע הזמנות נוספות לימים הקרובים, ולהבטיח את זמינות הארוחות האהובות עליכם ואת זמן המשלוח המועדף."; ?>
                </p>
                
                <p class="bigger">
                <span style="text-decoration: underline"><?php echo "שימו לב:"; ?></span>
                <?php echo "הבשלנים מציעים כמות מוגבלת מכל ארוחה בכל יום"; ?>
                </p>
            </div>
        </div>

        <section id="top-cook">
        <h1 class="success-title">
            <?php echo "ארוחות נוספות מ".$cooks[$top_cook]['firstname'];?>
            <span class="success-menu-span" id="success-menu-span-cook">
               <a href="index.php?route=seller/catalog-seller/profile&seller_id=<?php echo $top_cook;?>#seller-tabs" class="success-menu-link"><?php echo "לכל הארוחות של ".$cooks[$top_cook]['firstname']." >";?></a>
            </span>
        </h1>
            
        <div class="more-meals-cook">
            <?php if ($seller_products) { ?>
                <div class="product-grid-list">
                        <ul class="product-list" id="product-list-grid">
                        <?php foreach ($seller_products as $product) { ?>
                        <li id="pid_<?php echo $product['product_id']; ?>">
                            <div class="product-block">
                            <div class="product-block-inner">
                          <div class="image">
                          <?php if ($product['thumb']) { ?>
                          
                            <?php if ($product['is_verified']) { ?>
                            <div class="homaels-verified"></div>
                            <?php } ?>
                            
                            <div class="pro_filter"></div>
                            
                            <?php if ($product['total_sides']) { ?>
                                    <div class="side-text-con" >
                                            <span class="side-text">
                                            <?php echo $product['total_sides']; ?>
                                            </span>
                                    </div>
                            <?php } ?>
                            
                          <img src="image/no_image.jpg" data-src="<?php echo $product['thumb']; ?>" title="<?php echo $product['name']; ?>" alt="<?php echo $product['name']; ?>" /><?php } ?>
                          
                            <div class="side-img-con-con" >
                                <?php
                                
                                $number_of_side_images = 1;
                                
                                foreach($product['sides'] as $side) { $number_of_side_images++; }
                                
                                ?>
                                
                                <div class="side-img-con" data-num="<?php echo $number_of_side_images; ?>">
                                    <div class="side-img selected" data-side-id="1">
                                        <div class="pro_filter"></div>
                                        <span class="side-name"><?php echo $product['name']; ?></span>
                                        <img src="<?php echo $product['thumb']; ?>" />
                                    </div>
                                    <?php
                                        
                                        $side_img_id = 1;
                                        
                                    foreach($product['sides'] as $side) {
                                        
                                        $side_img_id++;
                                        
                                    ?>
                                    <div class="side-img" data-side-id="<?php echo $side_img_id; ?>">
                                        <div class="pro_filter"></div>
                                        <span class="side-name">תוספת - <?php echo $side['name']; ?></span>
                                        <img src="<?php echo $side['thumb']; ?>" />
                                    </div>
                                    <?php } ?>
                                </div>
                            </div>
                            
                            <?php if($number_of_side_images > 1) { ?>
                            <div class="side-arrows">
                                    
                                    
                                <div class="side-arrow-left"> <img src="image/data/left.png" /> </div>
                                    
                                <a class="center-href" href="<?php echo $product['href']; ?>"></a>
                                    
                                    
                                <div class="side-arrow-right"> <img src="image/data/right.png" /> </div>
                            </div>
                            <?php } else { ?>
                            <div class="side-arrows">
                                <a class="full-href" href="<?php echo $product['href']; ?>"></a>
                            </div>
                            <?php } ?>
                          
                          </div>
                    
                          
                                    <div class="saleblock">
                                    <?php if (!$product['special']) { ?>
                                    <?php } else { ?>
                                    <span class="saleicon sale">Sale</span>         
                                    <?php } ?>
                                    </div>	
                            <div style="clear: both"></div>
                              <div class="under-img-con">
                                    <div class="widget">
                                            <div class="widget-inner">
                                                    <div class="top-inner">
                                                            <img src="catalog/view/theme/<?php echo $this->config->get('config_template') ?>/image/homeals/vegeterian.png" />
                                                    </div>
                                                    <div class="bottom-inner">
                                                            <span class="text"><?php echo $product['pro_cat']; ?></span>
                                                    </div>
                                            </div>
                                            <div class="widget-inner">
                                                    <div class="top-inner">
                                                            <span class="numbercon"><?php echo $product['total_sides']; ?></span>
                                                    </div>
                                                    <div class="bottom-inner">
                                                            <span class="text">Side dish</span>
                                                    </div>
                                            </div>
                                            <div class="widget-inner">
                                                    <div class="top-inner">
                                                    
                                                    <span class="pricecon">
                                                    <?php if (!$product['special']) { ?>
                                                    <?php echo $product['price']; ?>
                                                    <?php } else { ?>
                                                    <!-- <span class="price-old"><?php echo $product['price']; ?></span> --> <span class="price-new"><?php echo $product['special']; ?></span>
                                                    <?php } ?>
                                                    <?php if ($product['tax']) { ?>
                                                    <br />
                                                    <span class="price-tax"><?php echo $text_tax; ?> <?php echo $product['tax']; ?></span>
                                                    <?php } ?>
                                                    </span>	
                                                            
                                                    </div>
                                                    <div class="bottom-inner">
                                                            <span class="text">Per meal</span>
                                                    </div>
                                            </div>
                                    </div>
                                    <div class="probox">
                                    <div class="namepro" style="margin-bottom: 10px;">
                                        <span class="namecon">
                                            <a href="<?php echo $product['href']; ?>">
                                                    <?php echo mb_truncate($product['name'],50); ?>
                                            </a>
                                        </span>
                                        <span class="pricecon">
                                            <?php if (!$product['special']) { ?>
                                            <?php echo $product['price']; ?>
                                            <?php } else { ?>
                                            <!-- <span class="price-old"><?php echo $product['price']; ?></span> --> <span class="price-new"><?php echo $product['special']; ?></span>
                                            <?php } ?>
                                            <?php if ($product['tax']) { ?>
                                            <br />
                                            <span class="price-tax"><?php echo $text_per_meal; ?> <?php //echo $product['tax']; ?></span>
                                            <?php } ?>
                                        </span>
                                    </div>
                                    
                                    
                                    <div class="rating">
                                    <?php if($product['reviews'] != 0){ ?>
                                        <div class="rating-scope" itemscope itemtype="http://schema.org/AggregateRating"><img itemprop="ratingValue" src="catalog/view/theme/<?php echo $this->config->get('config_template') ?>/image/stars-<?php echo $product['rating']; ?>.png" />&nbsp;&nbsp;<span itemprop="reviewCount" onclick="$('a[href=\'#tab-review\']').trigger('click');" ><?php echo $product['reviews']; ?> <?php echo $text_reviewsmin; ?></span>&nbsp;<?php echo (($product['reviews_text'] == 0) ? "" : "|"); ?>&nbsp;<a href="<?php echo $product['href']."#reviews_start"; ?>" class="review-write" <?php echo (($product['reviews_text'] == 0) ? "style='display:none'" : ""); ?>><img itemprop="ratingValue" src="catalog/view/theme/<?php echo $this->config->get('config_template') ?>/image/homeals/speachbubble.png" />&nbsp;<?php echo $product['reviews_text']; ?></a></div> 
                                    <?php } else { ?>
                                            
                                    <?php  } ?>
                                        <div class="last-order-time"><?php echo $product['days_available']; ?></div>
                                    </div>
                                    
                                    <div style="clear: both"></div>
                                    </div>
                                    
                                    </div><!-- under-img-con - End -->
                                    <div class="underpro"></div>
                            </div>
                     </div>
                        </li>
                        <?php } ?>
                      </ul>
                </div>
            <?php } ?>
        </div>
        </section>
        
        
        <section id="top-cat">
        <h1 class="success-title">
            <?php echo "ארוחות נוספות בקטגוריה: ".$top_cat_name;?>
            <span class="success-menu-span" id="success-menu-span-cat">
               <a href="index.php?route=product/category&path=<?php echo $top_cat; ?>" class="success-menu-link">לארוחות נוספות בקטגוריה ></a>
            </span>
        </h1>
        
        <div class="more-meals-cat">
            
            <?php if ($category_products) { ?>
                <div class="product-grid-list">
                        <ul class="product-list" id="product-list-grid">
                        <?php
                        $x = 0;
                        foreach ($category_products as $product) {
                            
                            if($x == 2){
                                break;
                            }
                            
                            ++$x;
                        ?>                        
                        <li id="pid_<?php echo $product['product_id']; ?>">
                            <div class="product-block">
                            <div class="product-block-inner">
                          <div class="image">
                          <?php if ($product['thumb']) { ?>
                          
                            <?php if ($product['is_verified']) { ?>
                            <div class="homaels-verified"></div>
                            <?php } ?>
                            
                            <div class="pro_filter"></div>
                            
                            <?php if ($product['total_sides']) { ?>
                                    <div class="side-text-con" >
                                            <span class="side-text">
                                            <?php echo $product['total_sides']; ?>
                                            </span>
                                    </div>
                            <?php } ?>
                            
                          <img src="image/no_image.jpg" data-src="<?php echo $product['thumb']; ?>" title="<?php echo $product['name']; ?>" alt="<?php echo $product['name']; ?>" /><?php } ?>
                          
                            <div class="side-img-con-con" >
                                <?php
                                
                                $number_of_side_images = 1;
                                
                                foreach($product['sides'] as $side) { $number_of_side_images++; }
                                
                                ?>
                                
                                <div class="side-img-con" data-num="<?php echo $number_of_side_images; ?>">
                                    <div class="side-img selected" data-side-id="1">
                                        <div class="pro_filter"></div>
                                        <span class="side-name"><?php echo $product['name']; ?></span>
                                        <img src="<?php echo $product['thumb']; ?>" />
                                    </div>
                                    <?php
                                        
                                        $side_img_id = 1;
                                        
                                    foreach($product['sides'] as $side) {
                                        
                                        $side_img_id++;
                                        
                                    ?>
                                    <div class="side-img" data-side-id="<?php echo $side_img_id; ?>">
                                        <div class="pro_filter"></div>
                                        <span class="side-name">תוספת - <?php echo $side['name']; ?></span>
                                        <img src="<?php echo $side['thumb']; ?>" />
                                    </div>
                                    <?php } ?>
                                </div>
                            </div>
                            
                            <?php if($number_of_side_images > 1) { ?>
                            <div class="side-arrows">
                                    
                                    
                                <div class="side-arrow-left"> <img src="image/data/left.png" /> </div>
                                    
                                <a class="center-href" href="<?php echo $product['href']; ?>"></a>
                                    
                                    
                                <div class="side-arrow-right"> <img src="image/data/right.png" /> </div>
                            </div>
                            <?php } else { ?>
                            <div class="side-arrows">
                                <a class="full-href" href="<?php echo $product['href']; ?>"></a>
                            </div>
                            <?php } ?>
                          
                          </div>
                    
                          
                                    <div class="saleblock">
                                    <?php if (!$product['special']) { ?>
                                    <?php } else { ?>
                                    <span class="saleicon sale">Sale</span>         
                                    <?php } ?>
                                    </div>	
                            <div style="clear: both"></div>
                              <div class="under-img-con">
                                    <div class="widget">
                                            <div class="widget-inner">
                                                    <div class="top-inner">
                                                            <img src="catalog/view/theme/<?php echo $this->config->get('config_template') ?>/image/homeals/vegeterian.png" />
                                                    </div>
                                                    <div class="bottom-inner">
                                                            <span class="text"><?php echo $product['pro_cat']; ?></span>
                                                    </div>
                                            </div>
                                            <div class="widget-inner">
                                                    <div class="top-inner">
                                                            <span class="numbercon"><?php echo $product['total_sides']; ?></span>
                                                    </div>
                                                    <div class="bottom-inner">
                                                            <span class="text">Side dish</span>
                                                    </div>
                                            </div>
                                            <div class="widget-inner">
                                                    <div class="top-inner">
                                                    
                                                    <span class="pricecon">
                                                    <?php if (!$product['special']) { ?>
                                                    <?php echo $product['price']; ?>
                                                    <?php } else { ?>
                                                    <!-- <span class="price-old"><?php echo $product['price']; ?></span> --> <span class="price-new"><?php echo $product['special']; ?></span>
                                                    <?php } ?>
                                                    <?php if ($product['tax']) { ?>
                                                    <br />
                                                    <span class="price-tax"><?php echo $text_tax; ?> <?php echo $product['tax']; ?></span>
                                                    <?php } ?>
                                                    </span>	
                                                            
                                                    </div>
                                                    <div class="bottom-inner">
                                                            <span class="text">Per meal</span>
                                                    </div>
                                            </div>
                                    </div>
                                    <div class="probox">
                                    <div class="namepro" style="margin-bottom: 10px;">
                                        <span class="namecon">
                                            <a href="<?php echo $product['href']; ?>">
                                                    <?php echo mb_truncate($product['name'],50); ?>
                                            </a>
                                        </span>
                                        <span class="pricecon">
                                            <?php if (!$product['special']) { ?>
                                            <?php echo $product['price']; ?>
                                            <?php } else { ?>
                                            <!-- <span class="price-old"><?php echo $product['price']; ?></span> --> <span class="price-new"><?php echo $product['special']; ?></span>
                                            <?php } ?>
                                            <?php if ($product['tax']) { ?>
                                            <br />
                                            <span class="price-tax"><?php echo $text_per_meal; ?> <?php //echo $product['tax']; ?></span>
                                            <?php } ?>
                                        </span>
                                    </div>
                                    
                                    
                                    <div class="rating">
                                    <?php if($product['reviews'] != 0){ ?>
                                        <div class="rating-scope" itemscope itemtype="http://schema.org/AggregateRating"><img itemprop="ratingValue" src="catalog/view/theme/<?php echo $this->config->get('config_template') ?>/image/stars-<?php echo $product['rating']; ?>.png" />&nbsp;&nbsp;<span itemprop="reviewCount" onclick="$('a[href=\'#tab-review\']').trigger('click');" ><?php echo $product['reviews']; ?> <?php echo $text_reviewsmin; ?></span>&nbsp;<?php echo (($product['reviews_text'] == 0) ? "" : "|"); ?>&nbsp;<a href="<?php echo $product['href']."#reviews_start"; ?>" class="review-write" <?php echo (($product['reviews_text'] == 0) ? "style='display:none'" : ""); ?>><img itemprop="ratingValue" src="catalog/view/theme/<?php echo $this->config->get('config_template') ?>/image/homeals/speachbubble.png" />&nbsp;<?php echo $product['reviews_text']; ?></a></div> 
                                    <?php } else { ?>
                                            
                                    <?php  } ?>
                                        <div class="last-order-time"><?php echo $product['days_available']; ?></div>
                                    </div>
                                    
                                    <div style="clear: both"></div>
                                    </div>
                                    
                                    </div><!-- under-img-con - End -->
                                    <div class="underpro"></div>
                            </div>
                     </div>
                        </li>
                        <?php } ?>
                      </ul>
                </div>
            <?php } ?>
        </div>
        </section>
        
        <div id="button-success-gotomeals-con">
            <a href="index.php?route=product/category&path=0" id="button-success-gotomeals">
            לתפריט המלא לימים קרובים
            </a>
        </div>
        
    </div>
    
    <?php echo $content_bottom; ?>
</div>

<?php echo $footer; ?>

<!-- Facebook Conversion Code for Buyers -->
<script>(function() {
  var _fbq = window._fbq || (window._fbq = []);
  if (!_fbq.loaded) {
    var fbds = document.createElement('script');
    fbds.async = true;
    fbds.src = '//connect.facebook.net/en_US/fbds.js';
    var s = document.getElementsByTagName('script')[0];
    s.parentNode.insertBefore(fbds, s);
    _fbq.loaded = true;
  }
})();
window._fbq = window._fbq || [];
window._fbq.push(['track', '6019669301263', {'value':'0.00','currency':'ILS'}]);
</script>
<noscript><img height="1" width="1" alt="" style="display:none" src="https://www.facebook.com/tr?ev=6019669301263&amp;cd[value]=0.00&amp;cd[currency]=ILS&amp;noscript=1" /></noscript>


<script>
    function nextImage2(image_id,image_number) {
        if (image_id == 0) {
                next_image = image_number;
        } else if (image_id > image_number) {
                next_image = 1;
        } else {
                next_image = image_id;
        }
        return next_image;
    }
    
    $(function(){
        
        /**************************************************
        *
        *	new meal block with moving pictures
        *
        *************************************************/
       $(".side-arrow-right , .side-arrow-left").on("click",function() {
           
           $(this).parent().parent().find('.side-img-con').show();
           $(this).parent().parent().find('.side-text-con').hide();
           
           
           var image_num =  parseInt($(this).parent().parent().find(".side-img-con").data("num"),10);
           var curr_image_id = parseInt($(this).parent().parent().find(".side-img-con").find(".selected").data("side-id"),10);
           
           if($(this).hasClass("side-arrow-left")){
               var next_image_id = nextImage2(curr_image_id + 1,image_num);
           } else {
               var next_image_id = nextImage2(curr_image_id - 1,image_num);
           }
           
           $(this).parent().parent().find(".side-img.selected").removeClass("selected");
           $(this).parent().parent().find("[data-side-id='"+next_image_id+"']").addClass("selected");
           
       });
       
       $(".product-block").hover(function(){
           $(this).find('.side-arrows').show();
       }, function(){
           $(this).find('.side-arrows').hide();
           $(this).find('.side-img-con').hide();
           $(this).find('.side-text-con').show();
       });
       /*************************************************/
    });
</script>