$(document).ready(function() {
	
	var options = { serviceUrl:'system/helper/autocomplete.php',
			// callback function:
			onSelect: function(data){
				
				if (data.value != $("#input-search-homeals").val()) {
					window.location = "index.php?route=product/category&path=0&filter_place="+data.value;
				}
				
			}
		};
		
	var hp_options = { serviceUrl:'system/helper/autocomplete.php' };
		
	$('#input-search-homeals').autocomplete(options);
	$("#hp-search-input").autocomplete(hp_options);
	
	$("img").unveil(250, function() {
		$(this).load(function() {
			this.style.opacity = 1;
		});
	});
	
	$('.tooltip').tooltipster({theme: 'tooltipster-light' });
	
	$("#column-right").height($("#content").height());
	
	/*
	var defaultBounds = new google.maps.LatLngBounds(
		new google.maps.LatLng(32.13840869677251, 34.869232177734375),
		new google.maps.LatLng(32.02437770874076, 34.729156494140625)
		);
	
	$("#input-search-homeals").geocomplete({
		bounds: defaultBounds,
		componentRestrictions: { country: 'il' },
		details: "#search-form"
		}).bind("geocode:result", function(event, result){
		$('#input-search-homeals').val(result.formatted_address);
		
		$('.button-search-homeals').trigger("click");
		//console.log(result);
	});
	
	$("#hp-search-input").geocomplete({bounds: defaultBounds,componentRestrictions: { country: 'il' },details: "#search-form"}).bind("geocode:result", function(event, result){
		$('#hp-search-input').val(result.formatted_address);
		
		$('.button-search-homeals').trigger("click");
		//console.log(result);
	});
	*/
	/* Search */
	//$('#hp-search-btn , .button-search-homeals').bind('click', function() {
	//	
	//	url = $('base').attr('href') + 'index.php?route=product/category&path=0';
	//	
	//	var street_name = $('#street_name').val();
	//	var street_number = $('#street_number').val();
	//	var city_name = $('#city_name').val();
	//	
	//	
	//	if (city_name == "תל אביב יפו") {
	//		url += '&filter_place=' + encodeURIComponent(street_name+" "+street_number+", "+city_name);
	//		url += '&street_name=' + encodeURIComponent(street_name);
	//		url += '&street_number=' + encodeURIComponent(street_number);
	//		url += '&city=' + encodeURIComponent(city_name);
	//		location = url;
	//	} else if (city_name == "") {
	//		location = url;
	//	} else {
	//		alert("כרגע אנו מקבלים משלוחים רק מאזור תל אביב");
	//	}
	//	
	//	
	//});
	
//	$('#hp-search-btn , .button-search-homeals').live('click', function() {
		
	//	$(".loading-screen").show();
	//	var city = $('hp-search-input').val();
	//	url = 'index.php?route=product/quickyCategory&city=' + city ;
		
		//var street_name = $("#input-search-homeals").val();
		//
		//if ( street_name == "" ) {
		//} else {
		//	url += '&filter_place=' + encodeURIComponent(street_name);
		//}
		
	//	location = url;
//	});
	
	$("#button-disable").colorbox({inline:true});
	$("#open_login,#open_signin,#open_login_new").colorbox({inline:true});
	
	$("#close-msg").live("click", function(){
		$.colorbox.close();
	});
	
	$("#open-logout-box").live("click", function(e){
		$(".logout-box").slideToggle(200);
		if ($("#open-logout-box").hasClass("pressed") ) {
			$("#open-logout-box").removeClass("pressed");
			
			$('.address-box').unbind("clickoutside");
			$('.logout-box').unbind("clickoutside");
			$('.need-help-box').unbind("clickoutside");
		} else {
			setTimeout(function(){
				$('.logout-box').bind("clickoutside" , function(event){
						$("#open-logout-box").trigger("click");
						event.stopPropagation();
					});   
			},100);
			$("#open-logout-box").addClass("pressed");
		}
	});
	
	$("#open-address-box").live("click", function(e){
		$(".address-box").slideToggle(200);
		if ($("#open-address-box").hasClass("pressed-address") ) {
			$("#open-address-box").removeClass("pressed-address");
			$('.address-box').unbind("clickoutside");
			$('.logout-box').unbind("clickoutside");
			$('.need-help-box').unbind("clickoutside");
		} else {
			setTimeout(function(){
				$('.address-box').bind("clickoutside" , function(event){
						$("#open-address-box").trigger("click");
						event.stopPropagation();
					});   
			},100);
			$("#open-address-box").addClass("pressed-address");
		}
	});
	
	$(".address-book-btn").live("click", function(e){
		$(".address-box-con").slideToggle(200);
		$(".address-box-con").on("clickoutside",hideBook);
		
		$(".address-book-btn").attr("class","address-book-btn-disable");
	});
	
	//$(".address-box-con").on("clickoutside",hideBook);
	//$(".address-box-con").slideToggle(200);
	
	var times = 0;
	
	function hideBook(e) {
		times++;
		
		$(".address-box-con").slideToggle(200);
		
		
		if (times == 1) {
			$(".address-box-con").off();
			$(".address-book-btn-disable").attr("class","address-book-btn");
			times = 0;
		}
		
	}
	
	$(".set-address-con").live("click", function(e){
		//$(".address-box-con").slideToggle(200);
		//console.log($(this).attr("id"));
		
		$(".homeals-delivery-address").val($(this).find(".street_name").val());
		$("input[name='street_num']").val($(this).find(".street_num").val());
		
		$("input[name='floor']").val($(this).find(".a_floor").val());
		$("input[name='apartment']").val($(this).find(".a_apart").val());
		$("input[name='entrance']").val($(this).find(".a_entrnce").val());
		$("input[name='homeals-company']").val($(this).find(".a_compony").val());
		$("input[name='contact']").val($(this).find(".a_contact").val());
		$("input[name='mobile']").val($(this).find(".a_phone").val());
		$("#supercheckout-comment_order").val($(this).find(".a_comment").val());
		
		$("#payment-existing-address-id").val($(this).attr("id"));
		
		setTimeout(function(){
			$("#payment-address-existing").trigger("click");
		},50);
	});
	
	
	
	//$('#open-logout-box').bind("clickoutside" , function(event){console.log("test");});
	
	$("#need-help").live("click", function(e){
		$(".need-help-box").slideToggle(200);
		if ($(".need-help").hasClass("pressed-help") ) {
			$(".need-help").removeClass("pressed-help");
			
			$('.address-box').unbind("clickoutside");
			$('.logout-box').unbind("clickoutside");
			$('.need-help-box').unbind("clickoutside");
		} else {
			setTimeout(function(){
				$('.need-help-box').bind( "clickoutside", function(event){
						$("#need-help").trigger("click");
						event.stopPropagation();
					});
			},100);
			$(".need-help").addClass("pressed-help");
		}
	});	

	$("#log-me-out").live("click", function(e){
		$.ajax( "index.php?route=account/logout" )
			.done(function() {
				location.href = "index.php?route=common/home";
			});
	});
	
	$("#loginemail , #loginpass").on('keypress',function(event) {
		if (event.which == 13 || event.keyCode == 13) {
			$("#loginbutton").trigger("click");
			return false;
		}
		return true;
	});
	
	$("#signincity ,#signinfirstname , #signinlastname , #signinemail , #signinpass").on('keypress',function(event) {
		if (event.which == 13 || event.keyCode == 13) {
			$("#signinbutton").trigger("click");
			return false;
		}
		return true;
	});
	
	
	
	$("#signinbutton").live("click", function(e){
		
		var _firstname =$("#signinfirstname").val();
		var _lastname =$("#signinlastname").val();
		var _city = $("#signincity").val();
		var _email =$("#signinemail").val();
		var _pass =$("#signinpass").val();
		var _join_list;
		
		if($("#join_list").attr("checked") == "checked"){
			_join_list = true;
		} else {
			_join_list = false;
		}
		
		if (_email == "" || _firstname == "" || _lastname == "" || _pass == "" || _city == "") {
			$("#signinfirstname").css("border","1px solid #F00");
			$("#signinlastname").css("border","1px solid #F00");
			$("#signinemail").css("border","1px solid #F00");
			$("#signinpass").css("border","1px solid #F00");
			$("#signincity").css("border","1px solid #F00");
		}else{
			
			$.ajax({
				type: "POST",
				url: "index.php?route=account/register/ajax_validate/",
				data: { firstname:_firstname,lastname:_lastname,city:_city,email: _email, password: _pass , join_list: _join_list },
				dataType: "json",
				cache: false
			      }).done(function( msg ) {
					//console.log(msg);
					
					if (msg == "singedup") {
						//location.reload();
						var rtrn = encodeURIComponent(btoa(location.href));
						location.href = "index.php?route=account/thankyou&rtrn="+rtrn;
					} 
					
					if(msg.email == "error_email" ||  msg.warning == "error_exists") {
						$("#signinemail").css("border","1px solid #F00");
						if ( msg.warning == "error_exists" ) {
							$("#signinemail-error-exists").show();
						} else {
							$("#signinemail-error").show();
						}
						
					}else{
						$("#signinemail").css("border","1px solid #DDDDDD");
						$("#signinemail-error").hide();
						$("#signinemail-error-exists").hide();
					}
					
					if(msg.city == "error_city" ) {
						$("#signincity").css("border","1px solid #F00");
						$("#signincity-error").show();
					}else{
						$("#signincity").css("border","1px solid #DDDDDD");
						$("#signincity-error").hide();
					}
					
					
					if(msg.password == "error_password") {
						$("#signinpass").css("border","1px solid #F00");
						$("#signinpass-error").show();
					}else{
						$("#signinpass").css("border","1px solid #DDDDDD");
						$("#signinmail-error").hide();
					}
					
					if(msg.firstname == "error_firstname") {
						$("#signinfirstname").css("border","1px solid #F00");
						$("#signinfirstname-error").show();
					}else{
						$("#signinfirstname").css("border","1px solid #DDDDDD");
						$("#signinfirstname-error").hide();
					}
					
					if(msg.lastname == "error_lastname") {
						$("#signinlastname").css("border","1px solid #F00");
						$("#signinlastname-error").show();
					}else{
						$("#signinlastname").css("border","1px solid #DDDDDD");
						$("#signinlastname-error").hide();
					}
				});	
		}
		
	});
	

	$("#loginnow").live("click", function(e){
		$("#open_login").colorbox({inline:true});
		$("#open_login").trigger("click");
	});
	
	$(".fbButton").live("click", function(e){
	    $("#fb-auth").trigger("click");
	});
	
	
	$("#signinnow").live("click", function(e){
		$("#open_signin").colorbox({inline:true});
		$("#open_signin").trigger("click");
	});
	
	$("#open_checkout").live("click", function(e){
		e.preventDefault();
		
		if(!( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) )) {
			$(".confirmCheckoutBack").slimscroll({
			    height: '506px'
			});  // menny
		}
		
		$(window).height();
		
		// we will check all cart items can be orderd if they are not we will look for a better time if there is no better
		// time we will kill the cart.
		// preCheck();
		
		$.colorbox({href:"#open-checkout",inline:true
			,onOpen:function(){
				$("body").css("height",$(window).height());
				$("body").css("overflow","hidden");
			}
			,onComplete:function(){
				refreshAll();
			}
			,onClosed:function(){
				$("body").css("height","100%");
				$("body").css("overflow","visible");
			}
		});
	});
	
	$("#save-address").live("click", function(e){
		e.preventDefault();
	});
	
	$('.emptybtn').live('click', function() {
		$(this).parents('#notification').fadeOut('fast'); 
			$.ajax({
				url: 'index.php?route=checkout/cart/clear',
				success:function(){
					location.reload();
				}
			});
	});
    
    
	/********************************
	 *	FOR Q&A PAGE
	 *********************************/
    
	$(".question").find("ul").hide();
	$(".question").on("click" , function() {
		$(this).find("ul").slideToggle(200);
	});
	
	
	/********************************
	 *	FOR PM
	 *********************************/
	$(".ajax_msg").colorbox();
	
	$("#send-pm").live("click", function() {
		$.ajax({
			type: "POST",
			dataType: "json",
			url: 'index.php?route=seller/catalog-seller/jxSubmitContactDialog',
			data: $("form.dialog").serialize(),
			beforeSend: function() {
				// Remove warning
				$('#warn-place .warn-text').remove();
				// Disable button
				$('#send-pm').attr('disabled', true);
				// Add loading
				$('#warn-place').append('<div class="attention-pm" style="clear:both"><img src="catalog/view/theme/default/image/loading.gif" alt="" /></div>');
			},
			complete: function() {
				// enable button 
				$('#send-pm').attr('disabled', false);
				// remove loading 
				$('#warn-place .attention-pm').remove();
			},
			success: function(data) {
				var errortext = '';
				if (!jQuery.isEmptyObject(data.errors)) {
					for (error in data.errors) {
					    if (!data.errors.hasOwnProperty(error)) {
						continue;
					    }
					    errortext += data.errors[error] + '<br />';
					}
					// adding warning
					$('#warn-place').append('<div class="warn-text" style="clear:both">' + errortext + '</div>');
				}
				
				if (data.success) {
					// empty pm
					$('#review-box textarea').val('');
					
					// close lightbox
					$.colorbox({href:"#msg-success", inline: true, open: true});
				}
			}
		});
	});
	
	/********************************
	 *	FOR PM END
	 *********************************/
	
	
	/********************************
	 *	FOR COOK JOIN
	 *********************************/
	
	$("#sendjoin").on("click",send_join);
	
	function send_join() {
		
		var _firstname = $("#join-firstname").val();
		var _lastname = $("#join-lastname").val();
		var _mail = $("#join-mail").val();
		var _address = $("#join-address").val();
		var _phone = $("#join-phone").val();
					
		if (IsEmail(_mail)) {
			$.ajax({
			    type: 'POST',
			    url: '/system/helper/join-ajax.php',
			    cache:false,
			    dataType: 'json',
			    data: { firstname: _firstname,lastname: _lastname,mail: _mail,address:_address,phone:_phone},
			    error : function() {
				    alert("קרתה בעיה :( נסה לרענן את הדף ולנסות שוב");
			    },
			    beforeSend: function() {
					// Disable button
					$("#sendjoin").off();
					// Add loading
					//$('#warn-place').append('<div class="attention-pm" style="clear:both"><img src="catalog/view/theme/default/image/loading.gif" alt="" /></div>');
			    },
			    complete: function() {
					// enable button 
					$("#sendjoin").on("click",send_join);
					// remove loading 
					//$('#warn-place .attention-pm').remove();
			    },
			    success: function(data) {
				
				if (data == "failed") {
					alert("ההרשמה נכשלה, בדוק שכל הפרטים שהכנסת נכונים");
				} else {
					$("#join-firstname").val("");
					$("#join-lastname").val("");
					$("#join-mail").val("");
					$("#join-address").val("");
					$("#join-phone").val("");
					
					alert("תודה! ניצור איתך קשר בהקדם.");
				}
				
				
				//$("#sendjoin").html("תודה!")
				//$("#sendjoin").off();
			    }
			});
		}else{
			alert("יש להזין כתובת מייל תקינה.");
		}
	}
	
	/********************************
	 *	FOR COOK JOIN END
	 *********************************/
	
	
	/********************************
	 *	FOR RESPONSIVE DESIGN START
	 *********************************/
	
	if(/Android|webOS|iPhone|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
		
		
		
		
	}
	
	/********************************
	 *	FOR RESPONSIVE DESIGN END
	 *********************************/
	
	
	$('#header input[name=\'filter_name\']').bind('keydown', function(e) {
		if (e.keyCode == 13) {
			url = $('base').attr('href') + 'index.php?route=product/search';
			 
			var search = $('input[name=\'filter_name\']').attr('value');
			
			if (search) {
				url += '&filter_name=' + encodeURIComponent(search);
			}
			
			location = url;
		}
	});
    
	$('#column-left input[name=\'filter_tag\']').bind('keydown', function(e) {
		if (e.keyCode == 13) {
			url = $('base').attr('href') + 'index.php?route=product/search';
			 
			var search = $(this).attr('value');
			var newParams;
			if (search) {
                if (getURLVar("filter_tag") != "") {
                    params = getAllURLVar();
                    search = params["filter_tag"] + "," + encodeURIComponent(search);
                }
                url += '&filter_tag=' + encodeURIComponent(search);
			}
			
			location = url;
		}
	});
	
	/* Ajax Cart */
	$('#cart > .heading a').live('click', function() {
		$('#cart').addClass('active');
		
		$('#cart').load('index.php?route=module/cart #cart > *');
		
		$('#cart').live('mouseleave', function() {
			$(this).removeClass('active');
		});
	});
	
	/* Mega Menu */
	$('#menu ul > li > a + div').each(function(index, element) {
		// IE6 & IE7 Fixes
		if ($.browser.msie && ($.browser.version == 7 || $.browser.version == 6)) {
			var category = $(element).find('a');
			var columns = $(element).find('ul').length;
			
			$(element).css('width', (columns * 143) + 'px');
			$(element).find('ul').css('float', 'left');
		}		
		
		var menu = $('#menu').offset();
		var dropdown = $(this).parent().offset();
		
		i = (dropdown.left + $(this).outerWidth()) - (menu.left + $('#menu').outerWidth());
		
		if (i > 0) {
			$(this).css('margin-left', '-' + (i + 5) + 'px');
		}
	});

	// IE6 & IE7 Fixes
	if ($.browser.msie) {
		if ($.browser.version <= 6) {
			$('#column-left + #column-right + #content, #column-left + #content').css('margin-left', '195px');
			
			$('#column-right + #content').css('margin-right', '195px');
		
			$('.box-category ul li a.active + ul').css('display', 'block');	
		}
		
		if ($.browser.version <= 7) {
			$('#menu > ul > li').bind('mouseover', function() {
				$(this).addClass('active');
			});
				
			$('#menu > ul > li').bind('mouseout', function() {
				$(this).removeClass('active');
			});	
		}
		
		
		if ($.browser.version <= 9) {
			// On DOM ready, hide the real password
			$('#loginpass').hide();
			
			// Show the fake pass (because JS is enabled)
			$('#fpass').show();
			
			// On focus of the fake password field
			$('#fpass').focus(function(){
			    $(this).hide(); //  hide the fake password input text
			    $('#loginpass').show().focus(); // and show the real password input password
			});
			
			// On blur of the real pass
			$('#loginpass').blur(function(){
			    if($(this).val() == ""){ // if the value is empty, 
				$(this).hide(); // hide the real password field
				$('#fpass').show(); // show the fake password
			    }
			    // otherwise, a password has been entered,
			    // so do nothing (leave the real password showing)
			});
			
			
			
			// On DOM ready, hide the real password
			$('#signinpass').hide();
			
			// Show the fake pass (because JS is enabled)
			$('#sfpass').show();
			
			// On focus of the fake password field
			$('#sfpass').focus(function(){
			    $(this).hide(); //  hide the fake password input text
			    $('#signinpass').show().focus(); // and show the real password input password
			});
			
			// On blur of the real pass
			$('#signinpass').blur(function(){
			    if($(this).val() == ""){ // if the value is empty, 
				$(this).hide(); // hide the real password field
				$('#sfpass').show(); // show the fake password
			    }
			    // otherwise, a password has been entered,
			    // so do nothing (leave the real password showing)
			});
			
		}
		
		
		
	}
	
	// open check-out
	$(".bagitems-left").live('click', function() {
		$("#open_checkout").trigger("click");
	});
	// go to cat page of the cook
	$(".bagitems-right").live('click', function() {
		$(".loading-screen").show();
		
		window.location = "index.php?route=product/category&path=0&cook=true";
	});
	
	// showing loading for varius links
	$(".gotocook , .a , .homeals-button").live('click', function() {
		$(".loading-screen").show();
	});
	
//	$('#notification').bind( "clickoutside", function(event){
//		$(window).trigger("scroll");
//	});
//	
//	$(window).scroll(function() {
//		if ($('#notification').hasClass("isDown") ) {
//			// do nothing
//        } else {
//            $('#notification').animate({ 
//				bottom: '-105px'
//			}, 300); 
//			$('#notification').addClass("isDown");
//			$('.success .closedcart').fadeIn(250);
//			$('.success .closebtn').fadeOut(250);
//        }
//	});
	
	
	// cart go up and down
    $('.success .closebtn,.success .closedcart').live('click', function() {
        if ($(this).parents('#notification').hasClass("isDown") ) {
            $(this).parents('#notification').animate({ 
                bottom: '0px'
           }, 300);         
            $(this).parents('#notification').removeClass("isDown");
			$('.success .closedcart').fadeOut(250);
			$('.success .closebtn').fadeIn(250);
        } else {
            $(this).parents('#notification').animate({ 
                bottom: "-"+($(this).parents('#notification').outerHeight() - 9 )
           }, 300); 
           $(this).parents('#notification').addClass("isDown");
		   $('.success .closedcart').fadeIn(250);
			$('.success .closebtn').fadeOut(250);
        }
    });
    
	$('.warning img, .attention img, .information img').live('click', function() {
		$(this).parent().fadeOut('slow', function() {
			$(this).remove();
		});
	});	
});

function getURLVar(key) {
	var value = [];
	
	var query = String(document.location).split('?');
	
	if (query[1]) {
		var part = query[1].split('&');

		for (i = 0; i < part.length; i++) {
			var data = part[i].split('=');
			
			if (data[0] && data[1]) {
				value[data[0]] = data[1];
			}
		}
		
        if (value[key]) {
			return value[key];
		} else {
			return '';
		}
	}
} 

function showLoadBox() {
	$("#cboxLoadingOverlay").show();
	$("#cboxLoadingGraphic").show();
}

function hideLoadBox() {
	$("#cboxLoadingOverlay").hide();
	$("#cboxLoadingGraphic").hide();
}

function showLoadPage() {
	if( !$.browser.msie ) {
		$("#hp-load").show();
	}
}

function hideLoadPage() {
	$("#hp-load").hide();
}

function showLoad() {
	if( !$.browser.msie ) {
		$("#confirmLoader").show();
	}
}

function hideLoad() {
	$("#confirmLoader").hide();
}

function goBack() {
    window.history.back()
}

function IsEmail(email) {
  var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
  return regex.test(email);
}

function getAllURLVar() {
	var value = [];
	
	var query = String(document.location).split('?');
	
	if (query[1]) {
		var part = query[1].split('&');

		for (i = 0; i < part.length; i++) {
			var data = part[i].split('=');
			
			if (data[0] && data[1]) {
				value[data[0]] = data[1];
			}
		}
		
		return value;
	}
	
	return value;
} 

function addToCart(product_id, quantity) {
	quantity = typeof(quantity) != 'undefined' ? quantity : 1;

	$.ajax({
		url: 'index.php?route=checkout/cart/add',
		type: 'post',
		data: 'product_id=' + product_id + '&quantity=' + quantity,
		dataType: 'json',
		success: function(json) {
			$('.success, .warning, .attention, .information, .error').remove();
			
			if (json['redirect']) {
				location = json['redirect'];
			}
			
			if (json['success']) {
				
				$('#button-show-cart').trigger("click");
				
				/*
				$('#notification').show("slide", { direction: "down" }, 400);
			
				//$('#notification').html('<div class="container"><div class="success" style="display: none;">' + json['success'] + '<img src="catalog/view/theme/default/image/close.png" alt="" class="close" /></div></div>');
				
				$('#notification').html('<div class="success" style="display: none;"><div class="itemsinbag"><img src="catalog/view/theme/OPC040088/image/homeals/bag.png" /><span class="number"> ' + json['total_number'] + '</span><br />items in your bag</div><div class="recentadded"><h2>recent meal added:</h2>' + json['recent_item'] + '</div><div class="summary"><h2>total: <span class="cash">' + json['total_cash'] + '</span></h2><div class="checkout"><a href="index.php?route=checkout/checkout">Checkout</a></div></div><div class="closebtn"><img src="catalog/view/theme/OPC040088/image/homeals/x_white.png" alt="Close" /></div><div class="clear"></div></div>');
				
				$('.success').fadeIn('slow');
				
				$('#cart-total').html(json['total']);
				
				$('html, body').animate({ scrollTop: 0 }, 'slow');
				*/
			}	
		}
	});
}
function addToWishList(product_id) {
	$.ajax({
		url: 'index.php?route=account/wishlist/add',
		type: 'post',
		data: 'product_id=' + product_id,
		dataType: 'json',
		success: function(json) {
			$('.success, .warning, .attention, .information').remove();
						
			if (json['success']) {
				$('#notification').html('<div class="success" style="display: none;">' + json['success'] + '<img src="catalog/view/theme/default/image/close.png" alt="" class="close" /></div>');
				
				$('.success').fadeIn('slow');
				
				$('#wishlist-total').html(json['total']);
				
				$('html, body').animate({ scrollTop: 0 }, 'slow');
			}	
		}
	});
}

function addToCompare(product_id) { 
	$.ajax({
		url: 'index.php?route=product/compare/add',
		type: 'post',
		data: 'product_id=' + product_id,
		dataType: 'json',
		success: function(json) {
			$('.success, .warning, .attention, .information').remove();
						
			if (json['success']) {
				$('#notification').html('<div class="success" style="display: none;">' + json['success'] + '<img src="catalog/view/theme/default/image/close.png" alt="" class="close" /></div>');
				
				$('.success').fadeIn('slow');
				
				$('#compare-total').html(json['total']);
				
				$('html, body').animate({ scrollTop: 0 }, 'slow'); 
			}	
		}
	});
}



/* jquery tabs */
$.fn.tabs = function() {
	var selector = this;
	
	this.each(function() {
		var obj = $(this); 
		
		$(obj.attr('href')).hide();
		
		obj.click(function() {
			$(selector).removeClass('selected');
			
			$(this).addClass('selected');
			
			$($(this).attr('href')).fadeIn();
			
			$(selector).not(this).each(function(i, element) {
				$($(element).attr('href')).hide();
			});
			
			return false;
		});
	});
	
	$(this).show();
	
	$(this).first().click();
};

/*
 * jQuery outside events - v1.1 - 3/16/2010
 * http://benalman.com/projects/jquery-outside-events-plugin/
 * 
 * Copyright (c) 2010 "Cowboy" Ben Alman
 * Dual licensed under the MIT and GPL licenses.
 * http://benalman.com/about/license/
 */
(function($,c,b){$.map("click dblclick mousemove mousedown mouseup mouseover mouseout change select submit keydown keypress keyup".split(" "),function(d){a(d)});a("focusin","focus"+b);a("focusout","blur"+b);$.addOutsideEvent=a;function a(g,e){e=e||g+b;var d=$(),h=g+"."+e+"-special-event";$.event.special[e]={setup:function(){d=d.add(this);if(d.length===1){$(c).bind(h,f)}},teardown:function(){d=d.not(this);if(d.length===0){$(c).unbind(h)}},add:function(i){var j=i.handler;i.handler=function(l,k){l.target=k;j.apply(this,arguments)}}};function f(i){$(d).each(function(){var j=$(this);if(this!==i.target&&!j.has(i.target).length){j.triggerHandler(e,[i.target])}})}}})(jQuery,document,"outside");
 
 /**
 * jQuery Bar Rating Plugin v1.0.4
 *
 * http://github.com/netboy/jquery-bar-rating
 *
 * Copyright (c) 2012-2014 Kazik Pietruszewski
 *
 * Dual licensed under the MIT and GPL licenses:
 * http://www.opensource.org/licenses/mit-license.php
 * http://www.gnu.org/licenses/gpl.html
 */
 (function(c){var b,a;a=typeof window!=="undefined"&&window!==null?window:global;a.BarRating=b=(function(){function d(){this.show=function(){var g=c(this.elem),j,f,h=this.options,e,i;if(!g.data("barrating")){if(h.initialRating){i=c('option[value="'+h.initialRating+'"]',g)}else{i=c("option:selected",g)}g.data("barrating",{currentRatingValue:i.val(),currentRatingText:i.text(),originalRatingValue:i.val(),originalRatingText:i.text()});j=c("<div />",{"class":"br-widget"}).insertAfter(g);g.find("option").each(function(){var n,m,l,k;n=c(this).val();if(n){m=c(this).text();l=c("<a />",{href:"#","data-rating-value":n,"data-rating-text":m});k=c("<span />",{text:(h.showValues)?m:""});j.append(l.append(k))}});if(h.showSelectedRating){j.append(c("<div />",{text:"","class":"br-current-rating"}))}g.data("barrating").deselectable=(!g.find("option:first").val())?true:false;if(h.reverse){e="nextAll"}else{e="prevAll"}if(h.reverse){j.addClass("br-reverse")}if(h.readonly){j.addClass("br-readonly")}j.on("ratingchange",function(k,l,m){l=l?l:g.data("barrating").currentRatingValue;m=m?m:g.data("barrating").currentRatingText;g.find('option[value="'+l+'"]').prop("selected",true);if(h.showSelectedRating){c(this).find(".br-current-rating").text(m)}}).trigger("ratingchange");j.on("updaterating",function(k){c(this).find('a[data-rating-value="'+g.data("barrating").currentRatingValue+'"]').addClass("br-selected br-current")[e]().addClass("br-selected")}).trigger("updaterating");f=j.find("a");f.on("touchstart",function(k){k.preventDefault();k.stopPropagation();c(this).click()});if(h.readonly){f.on("click",function(k){k.preventDefault()})}if(!h.readonly){f.on("click",function(k){var m=c(this),l,n;k.preventDefault();f.removeClass("br-active br-selected");m.addClass("br-selected")[e]().addClass("br-selected");l=m.attr("data-rating-value");n=m.attr("data-rating-text");if(m.hasClass("br-current")&&g.data("barrating").deselectable){m.removeClass("br-selected br-current")[e]().removeClass("br-selected br-current");l="",n=""}else{f.removeClass("br-current");m.addClass("br-current")}g.data("barrating").currentRatingValue=l;g.data("barrating").currentRatingText=n;j.trigger("ratingchange");h.onSelect.call(this,g.data("barrating").currentRatingValue,g.data("barrating").currentRatingText);return false});f.on({mouseenter:function(){var k=c(this);f.removeClass("br-active").removeClass("br-selected");k.addClass("br-active")[e]().addClass("br-active");j.trigger("ratingchange",[k.attr("data-rating-value"),k.attr("data-rating-text")])}});j.on({mouseleave:function(){f.removeClass("br-active");j.trigger("ratingchange").trigger("updaterating")}})}g.hide()}};this.clear=function(){var e=c(this.elem);var f=e.next(".br-widget");if(f&&e.data("barrating")){f.find("a").removeClass("br-selected br-current");e.data("barrating").currentRatingValue=e.data("barrating").originalRatingValue;e.data("barrating").currentRatingText=e.data("barrating").originalRatingText;f.trigger("ratingchange").trigger("updaterating");this.options.onClear.call(this,e.data("barrating").currentRatingValue,e.data("barrating").currentRatingText)}};this.destroy=function(){var f=c(this.elem);var h=f.next(".br-widget");if(h&&f.data("barrating")){var e=f.data("barrating").currentRatingValue;var g=f.data("barrating").currentRatingText;f.removeData("barrating");h.off().remove();f.show();this.options.onDestroy.call(this,e,g)}}}d.prototype.init=function(f,g){var e;e=this;e.elem=g;return e.options=c.extend({},c.fn.barrating.defaults,f)};return d})();c.fn.barrating=function(e,d){return this.each(function(){var f=new b();if(!c(this).is("select")){c.error("Sorry, this plugin only works with select fields.")}if(f.hasOwnProperty(e)){f.init(d,this);return f[e]()}else{if(typeof e==="object"||!e){d=e;f.init(d,this);return f.show()}else{c.error("Method "+e+" does not exist on jQuery.barrating")}}})};return c.fn.barrating.defaults={initialRating:null,showValues:false,showSelectedRating:true,reverse:false,readonly:false,onSelect:function(d,e){},onClear:function(d,e){},onDestroy:function(d,e){}}})(jQuery);
 
 /* Tooltipster v3.2.1 */;(function(e,t,n){function s(t,n){this.bodyOverflowX;this.callbacks={hide:[],show:[]};this.checkInterval=null;this.Content;this.$el=e(t);this.$elProxy;this.elProxyPosition;this.enabled=true;this.options=e.extend({},i,n);this.mouseIsOverProxy=false;this.namespace="tooltipster-"+Math.round(Math.random()*1e5);this.Status="hidden";this.timerHide=null;this.timerShow=null;this.$tooltip;this.options.iconTheme=this.options.iconTheme.replace(".","");this.options.theme=this.options.theme.replace(".","");this._init()}function o(t,n){var r=true;e.each(t,function(e,i){if(typeof n[e]==="undefined"||t[e]!==n[e]){r=false;return false}});return r}function f(){return!a&&u}function l(){var e=n.body||n.documentElement,t=e.style,r="transition";if(typeof t[r]=="string"){return true}v=["Moz","Webkit","Khtml","O","ms"],r=r.charAt(0).toUpperCase()+r.substr(1);for(var i=0;i<v.length;i++){if(typeof t[v[i]+r]=="string"){return true}}return false}var r="tooltipster",i={animation:"fade",arrow:true,arrowColor:"",autoClose:true,content:null,contentAsHTML:false,contentCloning:true,delay:200,fixedWidth:0,maxWidth:0,functionInit:function(e,t){},functionBefore:function(e,t){t()},functionReady:function(e,t){},functionAfter:function(e){},icon:"(?)",iconCloning:true,iconDesktop:false,iconTouch:false,iconTheme:"tooltipster-icon",interactive:false,interactiveTolerance:350,multiple:false,offsetX:0,offsetY:0,onlyOne:false,position:"top",positionTracker:false,speed:350,timer:0,theme:"tooltipster-default",touchDevices:true,trigger:"hover",updateAnimation:true};s.prototype={_init:function(){var t=this;if(n.querySelector){if(t.options.content!==null){t._content_set(t.options.content)}else{var r=t.$el.attr("title");if(typeof r==="undefined")r=null;t._content_set(r)}var i=t.options.functionInit.call(t.$el,t.$el,t.Content);if(typeof i!=="undefined")t._content_set(i);t.$el.removeAttr("title").addClass("tooltipstered");if(!u&&t.options.iconDesktop||u&&t.options.iconTouch){if(typeof t.options.icon==="string"){t.$elProxy=e('<span class="'+t.options.iconTheme+'"></span>');t.$elProxy.text(t.options.icon)}else{if(t.options.iconCloning)t.$elProxy=t.options.icon.clone(true);else t.$elProxy=t.options.icon}t.$elProxy.insertAfter(t.$el)}else{t.$elProxy=t.$el}if(t.options.trigger=="hover"){t.$elProxy.on("mouseenter."+t.namespace,function(){if(!f()||t.options.touchDevices){t.mouseIsOverProxy=true;t._show()}}).on("mouseleave."+t.namespace,function(){if(!f()||t.options.touchDevices){t.mouseIsOverProxy=false}});if(u&&t.options.touchDevices){t.$elProxy.on("touchstart."+t.namespace,function(){t._showNow()})}}else if(t.options.trigger=="click"){t.$elProxy.on("click."+t.namespace,function(){if(!f()||t.options.touchDevices){t._show()}})}}},_show:function(){var e=this;if(e.Status!="shown"&&e.Status!="appearing"){if(e.options.delay){e.timerShow=setTimeout(function(){if(e.options.trigger=="click"||e.options.trigger=="hover"&&e.mouseIsOverProxy){e._showNow()}},e.options.delay)}else e._showNow()}},_showNow:function(n){var r=this;r.options.functionBefore.call(r.$el,r.$el,function(){if(r.enabled&&r.Content!==null){if(n)r.callbacks.show.push(n);r.callbacks.hide=[];clearTimeout(r.timerShow);r.timerShow=null;clearTimeout(r.timerHide);r.timerHide=null;if(r.options.onlyOne){e(".tooltipstered").not(r.$el).each(function(t,n){var r=e(n),i=r.data("tooltipster-ns");e.each(i,function(e,t){var n=r.data(t),i=n.status(),s=n.option("autoClose");if(i!=="hidden"&&i!=="disappearing"&&s){n.hide()}})})}var i=function(){r.Status="shown";e.each(r.callbacks.show,function(e,t){t.call(r.$el)});r.callbacks.show=[]};if(r.Status!=="hidden"){var s=0;if(r.Status==="disappearing"){r.Status="appearing";if(l()){r.$tooltip.clearQueue().removeClass("tooltipster-dying").addClass("tooltipster-"+r.options.animation+"-show");if(r.options.speed>0)r.$tooltip.delay(r.options.speed);r.$tooltip.queue(i)}else{r.$tooltip.stop().fadeIn(i)}}else if(r.Status==="shown"){i()}}else{r.Status="appearing";var s=r.options.speed;r.bodyOverflowX=e("body").css("overflow-x");e("body").css("overflow-x","hidden");var o="tooltipster-"+r.options.animation,a="-webkit-transition-duration: "+r.options.speed+"ms; -webkit-animation-duration: "+r.options.speed+"ms; -moz-transition-duration: "+r.options.speed+"ms; -moz-animation-duration: "+r.options.speed+"ms; -o-transition-duration: "+r.options.speed+"ms; -o-animation-duration: "+r.options.speed+"ms; -ms-transition-duration: "+r.options.speed+"ms; -ms-animation-duration: "+r.options.speed+"ms; transition-duration: "+r.options.speed+"ms; animation-duration: "+r.options.speed+"ms;",f=r.options.fixedWidth>0?"width:"+Math.round(r.options.fixedWidth)+"px;":"",c=r.options.maxWidth>0?"max-width:"+Math.round(r.options.maxWidth)+"px;":"",h=r.options.interactive?"pointer-events: auto;":"";r.$tooltip=e('<div class="tooltipster-base '+r.options.theme+'" style="'+f+" "+c+" "+h+" "+a+'"><div class="tooltipster-content"></div></div>');if(l())r.$tooltip.addClass(o);r._content_insert();r.$tooltip.appendTo("body");r.reposition();r.options.functionReady.call(r.$el,r.$el,r.$tooltip);if(l()){r.$tooltip.addClass(o+"-show");if(r.options.speed>0)r.$tooltip.delay(r.options.speed);r.$tooltip.queue(i)}else{r.$tooltip.css("display","none").fadeIn(r.options.speed,i)}r._interval_set();e(t).on("scroll."+r.namespace+" resize."+r.namespace,function(){r.reposition()});if(r.options.autoClose){e("body").off("."+r.namespace);if(r.options.trigger=="hover"){if(u){setTimeout(function(){e("body").on("touchstart."+r.namespace,function(){r.hide()})},0)}if(r.options.interactive){if(u){r.$tooltip.on("touchstart."+r.namespace,function(e){e.stopPropagation()})}var p=null;r.$elProxy.add(r.$tooltip).on("mouseleave."+r.namespace+"-autoClose",function(){clearTimeout(p);p=setTimeout(function(){r.hide()},r.options.interactiveTolerance)}).on("mouseenter."+r.namespace+"-autoClose",function(){clearTimeout(p)})}else{r.$elProxy.on("mouseleave."+r.namespace+"-autoClose",function(){r.hide()})}}else if(r.options.trigger=="click"){setTimeout(function(){e("body").on("click."+r.namespace+" touchstart."+r.namespace,function(){r.hide()})},0);if(r.options.interactive){r.$tooltip.on("click."+r.namespace+" touchstart."+r.namespace,function(e){e.stopPropagation()})}}}}if(r.options.timer>0){r.timerHide=setTimeout(function(){r.timerHide=null;r.hide()},r.options.timer+s)}}})},_interval_set:function(){var t=this;t.checkInterval=setInterval(function(){if(e("body").find(t.$el).length===0||e("body").find(t.$elProxy).length===0||t.Status=="hidden"||e("body").find(t.$tooltip).length===0){if(t.Status=="shown"||t.Status=="appearing")t.hide();t._interval_cancel()}else{if(t.options.positionTracker){var n=t._repositionInfo(t.$elProxy),r=false;if(o(n.dimension,t.elProxyPosition.dimension)){if(t.$elProxy.css("position")==="fixed"){if(o(n.position,t.elProxyPosition.position))r=true}else{if(o(n.offset,t.elProxyPosition.offset))r=true}}if(!r){t.reposition()}}}},200)},_interval_cancel:function(){clearInterval(this.checkInterval);this.checkInterval=null},_content_set:function(e){if(typeof e==="object"&&e!==null&&this.options.contentCloning){e=e.clone(true)}this.Content=e},_content_insert:function(){var e=this,t=this.$tooltip.find(".tooltipster-content");if(typeof e.Content==="string"&&!e.options.contentAsHTML){t.text(e.Content)}else{t.empty().append(e.Content)}},_update:function(e){var t=this;t._content_set(e);if(t.Content!==null){if(t.Status!=="hidden"){t._content_insert();t.reposition();if(t.options.updateAnimation){if(l()){t.$tooltip.css({width:"","-webkit-transition":"all "+t.options.speed+"ms, width 0ms, height 0ms, left 0ms, top 0ms","-moz-transition":"all "+t.options.speed+"ms, width 0ms, height 0ms, left 0ms, top 0ms","-o-transition":"all "+t.options.speed+"ms, width 0ms, height 0ms, left 0ms, top 0ms","-ms-transition":"all "+t.options.speed+"ms, width 0ms, height 0ms, left 0ms, top 0ms",transition:"all "+t.options.speed+"ms, width 0ms, height 0ms, left 0ms, top 0ms"}).addClass("tooltipster-content-changing");setTimeout(function(){if(t.Status!="hidden"){t.$tooltip.removeClass("tooltipster-content-changing");setTimeout(function(){if(t.Status!=="hidden"){t.$tooltip.css({"-webkit-transition":t.options.speed+"ms","-moz-transition":t.options.speed+"ms","-o-transition":t.options.speed+"ms","-ms-transition":t.options.speed+"ms",transition:t.options.speed+"ms"})}},t.options.speed)}},t.options.speed)}else{t.$tooltip.fadeTo(t.options.speed,.5,function(){if(t.Status!="hidden"){t.$tooltip.fadeTo(t.options.speed,1)}})}}}}else{t.hide()}},_repositionInfo:function(e){return{dimension:{height:e.outerHeight(false),width:e.outerWidth(false)},offset:e.offset(),position:{left:parseInt(e.css("left")),top:parseInt(e.css("top"))}}},hide:function(n){var r=this;if(n)r.callbacks.hide.push(n);r.callbacks.show=[];clearTimeout(r.timerShow);r.timerShow=null;clearTimeout(r.timerHide);r.timerHide=null;var i=function(){e.each(r.callbacks.hide,function(e,t){t.call(r.$el)});r.callbacks.hide=[]};if(r.Status=="shown"||r.Status=="appearing"){r.Status="disappearing";var s=function(){r.Status="hidden";if(typeof r.Content=="object"&&r.Content!==null){r.Content.detach()}r.$tooltip.remove();r.$tooltip=null;e(t).off("."+r.namespace);e("body").off("."+r.namespace).css("overflow-x",r.bodyOverflowX);e("body").off("."+r.namespace);r.$elProxy.off("."+r.namespace+"-autoClose");r.options.functionAfter.call(r.$el,r.$el);i()};if(l()){r.$tooltip.clearQueue().removeClass("tooltipster-"+r.options.animation+"-show").addClass("tooltipster-dying");if(r.options.speed>0)r.$tooltip.delay(r.options.speed);r.$tooltip.queue(s)}else{r.$tooltip.stop().fadeOut(r.options.speed,s)}}else if(r.Status=="hidden"){i()}return r},show:function(e){this._showNow(e);return this},update:function(e){return this.content(e)},content:function(e){if(typeof e==="undefined"){return this.Content}else{this._update(e);return this}},reposition:function(){var n=this;if(e("body").find(n.$tooltip).length!==0){n.$tooltip.css("width","");n.elProxyPosition=n._repositionInfo(n.$elProxy);var r=null,i=e(t).width(),s=n.elProxyPosition,o=n.$tooltip.outerWidth(false),u=n.$tooltip.innerWidth()+1,a=n.$tooltip.outerHeight(false);if(n.$elProxy.is("area")){var f=n.$elProxy.attr("shape"),l=n.$elProxy.parent().attr("name"),c=e('img[usemap="#'+l+'"]'),h=c.offset().left,p=c.offset().top,d=n.$elProxy.attr("coords")!==undefined?n.$elProxy.attr("coords").split(","):undefined;if(f=="circle"){var v=parseInt(d[0]),m=parseInt(d[1]),g=parseInt(d[2]);s.dimension.height=g*2;s.dimension.width=g*2;s.offset.top=p+m-g;s.offset.left=h+v-g}else if(f=="rect"){var v=parseInt(d[0]),m=parseInt(d[1]),y=parseInt(d[2]),b=parseInt(d[3]);s.dimension.height=b-m;s.dimension.width=y-v;s.offset.top=p+m;s.offset.left=h+v}else if(f=="poly"){var w=[],E=[],S=0,x=0,T=0,N=0,C="even";for(var k=0;k<d.length;k++){var L=parseInt(d[k]);if(C=="even"){if(L>T){T=L;if(k===0){S=T}}if(L<S){S=L}C="odd"}else{if(L>N){N=L;if(k==1){x=N}}if(L<x){x=L}C="even"}}s.dimension.height=N-x;s.dimension.width=T-S;s.offset.top=p+x;s.offset.left=h+S}else{s.dimension.height=c.outerHeight(false);s.dimension.width=c.outerWidth(false);s.offset.top=p;s.offset.left=h}}if(n.options.fixedWidth===0){n.$tooltip.css({width:Math.round(u)+"px","padding-left":"0px","padding-right":"0px"})}var A=0,O=0,M=0,_=parseInt(n.options.offsetY),D=parseInt(n.options.offsetX),P=n.options.position;function H(){var n=e(t).scrollLeft();if(A-n<0){r=A-n;A=n}if(A+o-n>i){r=A-(i+n-o);A=i+n-o}}function B(n,r){if(s.offset.top-e(t).scrollTop()-a-_-12<0&&r.indexOf("top")>-1){P=n}if(s.offset.top+s.dimension.height+a+12+_>e(t).scrollTop()+e(t).height()&&r.indexOf("bottom")>-1){P=n;M=s.offset.top-a-_-12}}if(P=="top"){var j=s.offset.left+o-(s.offset.left+s.dimension.width);A=s.offset.left+D-j/2;M=s.offset.top-a-_-12;H();B("bottom","top")}if(P=="top-left"){A=s.offset.left+D;M=s.offset.top-a-_-12;H();B("bottom-left","top-left")}if(P=="top-right"){A=s.offset.left+s.dimension.width+D-o;M=s.offset.top-a-_-12;H();B("bottom-right","top-right")}if(P=="bottom"){var j=s.offset.left+o-(s.offset.left+s.dimension.width);A=s.offset.left-j/2+D;M=s.offset.top+s.dimension.height+_+12;H();B("top","bottom")}if(P=="bottom-left"){A=s.offset.left+D;M=s.offset.top+s.dimension.height+_+12;H();B("top-left","bottom-left")}if(P=="bottom-right"){A=s.offset.left+s.dimension.width+D-o;M=s.offset.top+s.dimension.height+_+12;H();B("top-right","bottom-right")}if(P=="left"){A=s.offset.left-D-o-12;O=s.offset.left+D+s.dimension.width+12;var F=s.offset.top+a-(s.offset.top+s.dimension.height);M=s.offset.top-F/2-_;if(A<0&&O+o>i){var I=parseFloat(n.$tooltip.css("border-width"))*2,q=o+A-I;n.$tooltip.css("width",q+"px");a=n.$tooltip.outerHeight(false);A=s.offset.left-D-q-12-I;F=s.offset.top+a-(s.offset.top+s.dimension.height);M=s.offset.top-F/2-_}else if(A<0){A=s.offset.left+D+s.dimension.width+12;r="left"}}if(P=="right"){A=s.offset.left+D+s.dimension.width+12;O=s.offset.left-D-o-12;var F=s.offset.top+a-(s.offset.top+s.dimension.height);M=s.offset.top-F/2-_;if(A+o>i&&O<0){var I=parseFloat(n.$tooltip.css("border-width"))*2,q=i-A-I;n.$tooltip.css("width",q+"px");a=n.$tooltip.outerHeight(false);F=s.offset.top+a-(s.offset.top+s.dimension.height);M=s.offset.top-F/2-_}else if(A+o>i){A=s.offset.left-D-o-12;r="right"}}if(n.options.arrow){var R="tooltipster-arrow-"+P;if(n.options.arrowColor.length<1){var U=n.$tooltip.css("background-color")}else{var U=n.options.arrowColor}if(!r){r=""}else if(r=="left"){R="tooltipster-arrow-right";r=""}else if(r=="right"){R="tooltipster-arrow-left";r=""}else{r="left:"+Math.round(r)+"px;"}if(P=="top"||P=="top-left"||P=="top-right"){var z=parseFloat(n.$tooltip.css("border-bottom-width")),W=n.$tooltip.css("border-bottom-color")}else if(P=="bottom"||P=="bottom-left"||P=="bottom-right"){var z=parseFloat(n.$tooltip.css("border-top-width")),W=n.$tooltip.css("border-top-color")}else if(P=="left"){var z=parseFloat(n.$tooltip.css("border-right-width")),W=n.$tooltip.css("border-right-color")}else if(P=="right"){var z=parseFloat(n.$tooltip.css("border-left-width")),W=n.$tooltip.css("border-left-color")}else{var z=parseFloat(n.$tooltip.css("border-bottom-width")),W=n.$tooltip.css("border-bottom-color")}if(z>1){z++}var X="";if(z!==0){var V="",J="border-color: "+W+";";if(R.indexOf("bottom")!==-1){V="margin-top: -"+Math.round(z)+"px;"}else if(R.indexOf("top")!==-1){V="margin-bottom: -"+Math.round(z)+"px;"}else if(R.indexOf("left")!==-1){V="margin-right: -"+Math.round(z)+"px;"}else if(R.indexOf("right")!==-1){V="margin-left: -"+Math.round(z)+"px;"}X='<span class="tooltipster-arrow-border" style="'+V+" "+J+';"></span>'}n.$tooltip.find(".tooltipster-arrow").remove();var K='<div class="'+R+' tooltipster-arrow" style="'+r+'">'+X+'<span style="border-color:'+U+';"></span></div>';n.$tooltip.append(K)}n.$tooltip.css({top:Math.round(M)+"px",left:Math.round(A)+"px"})}return n},enable:function(){this.enabled=true;return this},disable:function(){this.hide();this.enabled=false;return this},destroy:function(){var t=this;t.hide();if(t.$el[0]!==t.$elProxy[0])t.$elProxy.remove();t.$el.removeData(t.namespace).off("."+t.namespace);var n=t.$el.data("tooltipster-ns");if(n.length===1){var r=typeof t.Content==="string"?t.Content:e("<div></div>").append(t.Content).html();t.$el.removeClass("tooltipstered").attr("title",r).removeData(t.namespace).removeData("tooltipster-ns").off("."+t.namespace)}else{n=e.grep(n,function(e,n){return e!==t.namespace});t.$el.data("tooltipster-ns",n)}return t},elementIcon:function(){return this.$el[0]!==this.$elProxy[0]?this.$elProxy[0]:undefined},elementTooltip:function(){return this.$tooltip?this.$tooltip[0]:undefined},option:function(e){return this.options[e]},status:function(e){return this.Status}};e.fn[r]=function(){var t=arguments;if(this.length===0){if(typeof t[0]==="string"){var n=true;switch(t[0]){case"setDefaults":e.extend(i,t[1]);break;default:n=false;break}if(n)return true;else return this}else{return this}}else{if(typeof t[0]==="string"){var r="#*$~&";this.each(function(){var n=e(this).data("tooltipster-ns"),i=n?e(this).data(n[0]):null;if(i){if(typeof i[t[0]]==="function"){var s=i[t[0]](t[1])}else{throw new Error('Unknown method .tooltipster("'+t[0]+'")')}if(s!==i){r=s;return false}}else{throw new Error("You called Tooltipster's \""+t[0]+'" method on an uninitialized element')}});return r!=="#*$~&"?r:this}else{var o=[],u=t[0]&&typeof t[0].multiple!=="undefined",a=u&&t[0].multiple||!u&&i.multiple;this.each(function(){var n=false,r=e(this).data("tooltipster-ns"),i=null;if(!r){n=true}else{if(a)n=true;}if(n){i=new s(this,t[0]);if(!r)r=[];r.push(i.namespace);e(this).data("tooltipster-ns",r);e(this).data(i.namespace,i)}o.push(i)});if(a)return o;else return this}}};var u=!!("ontouchstart"in t);var a=false;e("body").one("mousemove",function(){a=true})})(jQuery,window,document);
 
 
 /*!
	Autosize v1.18.9 - 2014-05-27
	Automatically adjust textarea height based on user input.
	(c) 2014 Jack Moore - http://www.jacklmoore.com/autosize
	license: http://www.opensource.org/licenses/mit-license.php
*/
(function(e){var t,o={className:"autosizejs",id:"autosizejs",append:"\n",callback:!1,resizeDelay:10,placeholder:!0},i='<textarea tabindex="-1" style="position:absolute; top:-999px; left:0; right:auto; bottom:auto; border:0; padding: 0; -moz-box-sizing:content-box; -webkit-box-sizing:content-box; box-sizing:content-box; word-wrap:break-word; height:0 !important; min-height:0 !important; overflow:hidden; transition:none; -webkit-transition:none; -moz-transition:none;"/>',n=["fontFamily","fontSize","fontWeight","fontStyle","letterSpacing","textTransform","wordSpacing","textIndent"],s=e(i).data("autosize",!0)[0];s.style.lineHeight="99px","99px"===e(s).css("lineHeight")&&n.push("lineHeight"),s.style.lineHeight="",e.fn.autosize=function(i){return this.length?(i=e.extend({},o,i||{}),s.parentNode!==document.body&&e(document.body).append(s),this.each(function(){function o(){var t,o=window.getComputedStyle?window.getComputedStyle(u,null):!1;o?(t=u.getBoundingClientRect().width,(0===t||"number"!=typeof t)&&(t=parseInt(o.width,10)),e.each(["paddingLeft","paddingRight","borderLeftWidth","borderRightWidth"],function(e,i){t-=parseInt(o[i],10)})):t=p.width(),s.style.width=Math.max(t,0)+"px"}function a(){var a={};if(t=u,s.className=i.className,s.id=i.id,d=parseInt(p.css("maxHeight"),10),e.each(n,function(e,t){a[t]=p.css(t)}),e(s).css(a).attr("wrap",p.attr("wrap")),o(),window.chrome){var r=u.style.width;u.style.width="0px",u.offsetWidth,u.style.width=r}}function r(){var e,n;t!==u?a():o(),s.value=!u.value&&i.placeholder?(p.attr("placeholder")||"")+i.append:u.value+i.append,s.style.overflowY=u.style.overflowY,n=parseInt(u.style.height,10),s.scrollTop=0,s.scrollTop=9e4,e=s.scrollTop,d&&e>d?(u.style.overflowY="scroll",e=d):(u.style.overflowY="hidden",c>e&&(e=c)),e+=w,n!==e&&(u.style.height=e+"px",f&&i.callback.call(u,u))}function l(){clearTimeout(h),h=setTimeout(function(){var e=p.width();e!==g&&(g=e,r())},parseInt(i.resizeDelay,10))}var d,c,h,u=this,p=e(u),w=0,f=e.isFunction(i.callback),z={height:u.style.height,overflow:u.style.overflow,overflowY:u.style.overflowY,wordWrap:u.style.wordWrap,resize:u.style.resize},g=p.width(),y=p.css("resize");p.data("autosize")||(p.data("autosize",!0),("border-box"===p.css("box-sizing")||"border-box"===p.css("-moz-box-sizing")||"border-box"===p.css("-webkit-box-sizing"))&&(w=p.outerHeight()-p.height()),c=Math.max(parseInt(p.css("minHeight"),10)-w||0,p.height()),p.css({overflow:"hidden",overflowY:"hidden",wordWrap:"break-word"}),"vertical"===y?p.css("resize","none"):"both"===y&&p.css("resize","horizontal"),"onpropertychange"in u?"oninput"in u?p.on("input.autosize keyup.autosize",r):p.on("propertychange.autosize",function(){"value"===event.propertyName&&r()}):p.on("input.autosize",r),i.resizeDelay!==!1&&e(window).on("resize.autosize",l),p.on("autosize.resize",r),p.on("autosize.resizeIncludeStyle",function(){t=null,r()}),p.on("autosize.destroy",function(){t=null,clearTimeout(h),e(window).off("resize",l),p.off("autosize").off(".autosize").css(z).removeData("autosize")}),r())})):this}})(window.jQuery||window.$);
 

/**
 * jQuery Unveil
 * A very lightweight jQuery plugin to lazy load images
 * http://luis-almeida.github.com/unveil
 *
 * Licensed under the MIT license.
 * Copyright 2013 Luís Almeida
 * https://github.com/luis-almeida
 */

;(function($) {

  $.fn.unveil = function(threshold, callback) {

    var $w = $(window),
        th = threshold || 0,
        retina = window.devicePixelRatio > 1,
        attrib = retina? "data-src-retina" : "data-src",
        images = this,
        loaded;

    this.one("unveil", function() {
      var source = this.getAttribute(attrib);
      source = source || this.getAttribute("data-src");
      if (source) {
        this.setAttribute("src", source);
        if (typeof callback === "function") callback.call(this);
      }
    });

    function unveil() {
      var inview = images.filter(function() {
        var $e = $(this);
        if ($e.is(":hidden")) return;

        var wt = $w.scrollTop(),
            wb = wt + $w.height(),
            et = $e.offset().top,
            eb = et + $e.height();

        return eb >= wt - th && et <= wb + th;
      });

      loaded = inview.trigger("unveil");
      images = images.not(loaded);
    }

    $w.on("scroll.unveil resize.unveil lookup.unveil", unveil);

    unveil();

    return this;

  };

})(window.jQuery || window.Zepto);



/**
*  Ajax Autocomplete for jQuery, version 1.2.9
*  (c) 2013 Tomas Kirda
*
*  Ajax Autocomplete for jQuery is freely distributable under the terms of an MIT-style license.
*  For details, see the web site: https://github.com/devbridge/jQuery-Autocomplete
*
*/

(function(e){"use strict";if(typeof define==="function"&&define.amd){define(["jquery"],e)}else{e(jQuery)}})(function(e){"use strict";function r(t,n){var i=function(){},s=this,o={autoSelectFirst:false,appendTo:"body",serviceUrl:null,lookup:null,onSelect:null,width:"auto",minChars:1,maxHeight:300,deferRequestBy:0,params:{},formatResult:r.formatResult,delimiter:null,zIndex:9999,type:"GET",noCache:false,onSearchStart:i,onSearchComplete:i,onSearchError:i,containerClass:"autocomplete-suggestions",tabDisabled:false,dataType:"text",currentRequest:null,triggerSelectOnValidInput:true,preventBadQueries:true,lookupFilter:function(e,t,n){return e.value.toLowerCase().indexOf(n)!==-1},paramName:"query",transformResult:function(t){return typeof t==="string"?e.parseJSON(t):t}};s.element=t;s.el=e(t);s.suggestions=[];s.badQueries=[];s.selectedIndex=-1;s.currentValue=s.element.value;s.intervalId=0;s.cachedResponse={};s.onChangeInterval=null;s.onChange=null;s.isLocal=false;s.suggestionsContainer=null;s.options=e.extend({},o,n);s.classes={selected:"autocomplete-selected",suggestion:"autocomplete-suggestion"};s.hint=null;s.hintValue="";s.selection=null;s.initialize();s.setOptions(n)}var t=function(){return{escapeRegExChars:function(e){return e.replace(/[\-\[\]\/\{\}\(\)\*\+\?\.\\\^\$\|]/g,"\\$&")},createNode:function(e){var t=document.createElement("div");t.className=e;t.style.position="absolute";t.style.display="none";return t}}}(),n={ESC:27,TAB:9,RETURN:13,LEFT:37,UP:38,RIGHT:39,DOWN:40};r.utils=t;e.Autocomplete=r;r.formatResult=function(e,n){var r="("+t.escapeRegExChars(n)+")";return e.value.replace(new RegExp(r,"gi"),"<strong>$1</strong>")};r.prototype={killerFn:null,initialize:function(){var t=this,n="."+t.classes.suggestion,i=t.classes.selected,s=t.options,o;t.element.setAttribute("autocomplete","off");t.killerFn=function(n){if(e(n.target).closest("."+t.options.containerClass).length===0){t.killSuggestions();t.disableKillerFn()}};t.suggestionsContainer=r.utils.createNode(s.containerClass);o=e(t.suggestionsContainer);o.appendTo(s.appendTo);if(s.width!=="auto"){o.width(s.width)}o.on("mouseover.autocomplete",n,function(){t.activate(e(this).data("index"))});o.on("mouseout.autocomplete",function(){t.selectedIndex=-1;o.children("."+i).removeClass(i)});o.on("click.autocomplete",n,function(){t.select(e(this).data("index"))});t.fixPosition();t.fixPositionCapture=function(){if(t.visible){t.fixPosition()}};e(window).on("resize.autocomplete",t.fixPositionCapture);t.el.on("keydown.autocomplete",function(e){t.onKeyPress(e)});t.el.on("keyup.autocomplete",function(e){t.onKeyUp(e)});t.el.on("blur.autocomplete",function(){t.onBlur()});t.el.on("focus.autocomplete",function(){t.onFocus()});t.el.on("change.autocomplete",function(e){t.onKeyUp(e)})},onFocus:function(){var e=this;e.fixPosition();if(e.options.minChars<=e.el.val().length){e.onValueChange()}},onBlur:function(){this.enableKillerFn()},setOptions:function(t){var n=this,r=n.options;e.extend(r,t);n.isLocal=e.isArray(r.lookup);if(n.isLocal){r.lookup=n.verifySuggestionsFormat(r.lookup)}e(n.suggestionsContainer).css({"max-height":r.maxHeight+"px",width:r.width+"px","z-index":r.zIndex})},clearCache:function(){this.cachedResponse={};this.badQueries=[]},clear:function(){this.clearCache();this.currentValue="";this.suggestions=[]},disable:function(){var e=this;e.disabled=true;if(e.currentRequest){e.currentRequest.abort()}},enable:function(){this.disabled=false},fixPosition:function(){var t=this,n,r;if(t.options.appendTo!=="body"){return}n=t.el.offset();r={top:n.top+t.el.outerHeight()+"px",left:n.left+"px"};if(t.options.width==="auto"){r.width=t.el.outerWidth()-2+"px"}e(t.suggestionsContainer).css(r)},enableKillerFn:function(){var t=this;e(document).on("click.autocomplete",t.killerFn)},disableKillerFn:function(){var t=this;e(document).off("click.autocomplete",t.killerFn)},killSuggestions:function(){var e=this;e.stopKillSuggestions();e.intervalId=window.setInterval(function(){e.hide();e.stopKillSuggestions()},50)},stopKillSuggestions:function(){window.clearInterval(this.intervalId)},isCursorAtEnd:function(){var e=this,t=e.el.val().length,n=e.element.selectionStart,r;if(typeof n==="number"){return n===t}if(document.selection){r=document.selection.createRange();r.moveStart("character",-t);return t===r.text.length}return true},onKeyPress:function(e){var t=this;if(!t.disabled&&!t.visible&&e.which===n.DOWN&&t.currentValue){t.suggest();return}if(t.disabled||!t.visible){return}switch(e.which){case n.ESC:t.el.val(t.currentValue);t.hide();break;case n.RIGHT:if(t.hint&&t.options.onHint&&t.isCursorAtEnd()){t.selectHint();break}return;case n.TAB:if(t.hint&&t.options.onHint){t.selectHint();return};case n.RETURN:if(t.selectedIndex===-1){t.hide();return}t.select(t.selectedIndex);if(e.which===n.TAB&&t.options.tabDisabled===false){return}break;case n.UP:t.moveUp();break;case n.DOWN:t.moveDown();break;default:return}e.stopImmediatePropagation();e.preventDefault()},onKeyUp:function(e){var t=this;if(t.disabled){return}switch(e.which){case n.UP:case n.DOWN:return}clearInterval(t.onChangeInterval);if(t.currentValue!==t.el.val()){t.findBestHint();if(t.options.deferRequestBy>0){t.onChangeInterval=setInterval(function(){t.onValueChange()},t.options.deferRequestBy)}else{t.onValueChange()}}},onValueChange:function(){var t=this,n=t.options,r=t.el.val(),i=t.getQuery(r),s;if(t.selection){t.selection=null;(n.onInvalidateSelection||e.noop).call(t.element)}clearInterval(t.onChangeInterval);t.currentValue=r;t.selectedIndex=-1;if(n.triggerSelectOnValidInput){s=t.findSuggestionIndex(i);if(s!==-1){t.select(s);return}}if(i.length<n.minChars){t.hide()}else{t.getSuggestions(i)}},findSuggestionIndex:function(t){var n=this,r=-1,i=t.toLowerCase();e.each(n.suggestions,function(e,t){if(t.value.toLowerCase()===i){r=e;return false}});return r},getQuery:function(t){var n=this.options.delimiter,r;if(!n){return t}r=t.split(n);return e.trim(r[r.length-1])},getSuggestionsLocal:function(t){var n=this,r=n.options,i=t.toLowerCase(),s=r.lookupFilter,o=parseInt(r.lookupLimit,10),u;u={suggestions:e.grep(r.lookup,function(e){return s(e,t,i)})};if(o&&u.suggestions.length>o){u.suggestions=u.suggestions.slice(0,o)}return u},getSuggestions:function(t){var n,r=this,i=r.options,s=i.serviceUrl,o,u;i.params[i.paramName]=t;o=i.ignoreParams?null:i.params;if(r.isLocal){n=r.getSuggestionsLocal(t)}else{if(e.isFunction(s)){s=s.call(r.element,t)}u=s+"?"+e.param(o||{});n=r.cachedResponse[u]}if(n&&e.isArray(n.suggestions)){r.suggestions=n.suggestions;r.suggest()}else if(!r.isBadQuery(t)){if(i.onSearchStart.call(r.element,i.params)===false){return}if(r.currentRequest){r.currentRequest.abort()}r.currentRequest=e.ajax({url:s,data:o,type:i.type,dataType:i.dataType}).done(function(e){var n;r.currentRequest=null;n=i.transformResult(e);r.processResponse(n,t,u);i.onSearchComplete.call(r.element,t,n.suggestions)}).fail(function(e,n,s){i.onSearchError.call(r.element,t,e,n,s)})}},isBadQuery:function(e){if(!this.options.preventBadQueries){return false}var t=this.badQueries,n=t.length;while(n--){if(e.indexOf(t[n])===0){return true}}return false},hide:function(){var t=this;t.visible=false;t.selectedIndex=-1;e(t.suggestionsContainer).hide();t.signalHint(null)},suggest:function(){if(this.suggestions.length===0){this.hide();return}var t=this,n=t.options,r=n.formatResult,i=t.getQuery(t.currentValue),s=t.classes.suggestion,o=t.classes.selected,u=e(t.suggestionsContainer),a=n.beforeRender,f="",l,c;if(n.triggerSelectOnValidInput){l=t.findSuggestionIndex(i);if(l!==-1){t.select(l);return}}e.each(t.suggestions,function(e,t){f+='<div class="'+s+'" data-index="'+e+'">'+r(t,i)+"</div>"});if(n.width==="auto"){c=t.el.outerWidth()-2;u.width(c>0?c:300)}u.html(f);if(n.autoSelectFirst){t.selectedIndex=0;u.children().first().addClass(o)}if(e.isFunction(a)){a.call(t.element,u)}u.show();t.visible=true;t.findBestHint()},findBestHint:function(){var t=this,n=t.el.val().toLowerCase(),r=null;if(!n){return}e.each(t.suggestions,function(e,t){var i=t.value.toLowerCase().indexOf(n)===0;if(i){r=t}return!i});t.signalHint(r)},signalHint:function(t){var n="",r=this;if(t){n=r.currentValue+t.value.substr(r.currentValue.length)}if(r.hintValue!==n){r.hintValue=n;r.hint=t;(this.options.onHint||e.noop)(n)}},verifySuggestionsFormat:function(t){if(t.length&&typeof t[0]==="string"){return e.map(t,function(e){return{value:e,data:null}})}return t},processResponse:function(e,t,n){var r=this,i=r.options;e.suggestions=r.verifySuggestionsFormat(e.suggestions);if(!i.noCache){r.cachedResponse[n]=e;if(i.preventBadQueries&&e.suggestions.length===0){r.badQueries.push(t)}}if(t!==r.getQuery(r.currentValue)){return}r.suggestions=e.suggestions;r.suggest()},activate:function(t){var n=this,r,i=n.classes.selected,s=e(n.suggestionsContainer),o=s.children();s.children("."+i).removeClass(i);n.selectedIndex=t;if(n.selectedIndex!==-1&&o.length>n.selectedIndex){r=o.get(n.selectedIndex);e(r).addClass(i);return r}return null},selectHint:function(){var t=this,n=e.inArray(t.hint,t.suggestions);t.select(n)},select:function(e){var t=this;t.hide();t.onSelect(e)},moveUp:function(){var t=this;if(t.selectedIndex===-1){return}if(t.selectedIndex===0){e(t.suggestionsContainer).children().first().removeClass(t.classes.selected);t.selectedIndex=-1;t.el.val(t.currentValue);t.findBestHint();return}t.adjustScroll(t.selectedIndex-1)},moveDown:function(){var e=this;if(e.selectedIndex===e.suggestions.length-1){return}e.adjustScroll(e.selectedIndex+1)},adjustScroll:function(t){var n=this,r=n.activate(t),i,s,o,u=25;if(!r){return}i=r.offsetTop;s=e(n.suggestionsContainer).scrollTop();o=s+n.options.maxHeight-u;if(i<s){e(n.suggestionsContainer).scrollTop(i)}else if(i>o){e(n.suggestionsContainer).scrollTop(i-n.options.maxHeight+u)}n.el.val(n.getValue(n.suggestions[t].value));n.signalHint(null)},onSelect:function(t){var n=this,r=n.options.onSelect,i=n.suggestions[t];n.currentValue=n.getValue(i.value);if(n.currentValue!==n.el.val()){n.el.val(n.currentValue)}n.signalHint(null);n.suggestions=[];n.selection=i;if(e.isFunction(r)){r.call(n.element,i)}},getValue:function(e){var t=this,n=t.options.delimiter,r,i;if(!n){return e}r=t.currentValue;i=r.split(n);if(i.length===1){return e}return r.substr(0,r.length-i[i.length-1].length)+e},dispose:function(){var t=this;t.el.off(".autocomplete").removeData("autocomplete");t.disableKillerFn();e(window).off("resize.autocomplete",t.fixPositionCapture);e(t.suggestionsContainer).remove()}};e.fn.autocomplete=function(t,n){var i="autocomplete";if(arguments.length===0){return this.first().data(i)}return this.each(function(){var s=e(this),o=s.data(i);if(typeof t==="string"){if(o&&typeof o[t]==="function"){o[t](n)}}else{if(o&&o.dispose){o.dispose()}o=new r(this,t);s.data(i,o)}})}})
 
 
 
 /**
 * Copyright (c) 2007-2014 Ariel Flesler - aflesler<a>gmail<d>com | http://flesler.blogspot.com
 * Licensed under MIT
 * @author Ariel Flesler
 * @version 1.4.12
 */
;(function(a){if(typeof define==='function'&&define.amd){define(['jquery'],a)}else{a(jQuery)}}(function($){var j=$.scrollTo=function(a,b,c){return $(window).scrollTo(a,b,c)};j.defaults={axis:'xy',duration:parseFloat($.fn.jquery)>=1.3?0:1,limit:true};j.window=function(a){return $(window)._scrollable()};$.fn._scrollable=function(){return this.map(function(){var a=this,isWin=!a.nodeName||$.inArray(a.nodeName.toLowerCase(),['iframe','#document','html','body'])!=-1;if(!isWin)return a;var b=(a.contentWindow||a).document||a.ownerDocument||a;return/webkit/i.test(navigator.userAgent)||b.compatMode=='BackCompat'?b.body:b.documentElement})};$.fn.scrollTo=function(f,g,h){if(typeof g=='object'){h=g;g=0}if(typeof h=='function')h={onAfter:h};if(f=='max')f=9e9;h=$.extend({},j.defaults,h);g=g||h.duration;h.queue=h.queue&&h.axis.length>1;if(h.queue)g/=2;h.offset=both(h.offset);h.over=both(h.over);return this._scrollable().each(function(){if(f==null)return;var d=this,$elem=$(d),targ=f,toff,attr={},win=$elem.is('html,body');switch(typeof targ){case'number':case'string':if(/^([+-]=?)?\d+(\.\d+)?(px|%)?$/.test(targ)){targ=both(targ);break}targ=win?$(targ):$(targ,this);if(!targ.length)return;case'object':if(targ.is||targ.style)toff=(targ=$(targ)).offset()}var e=$.isFunction(h.offset)&&h.offset(d,targ)||h.offset;$.each(h.axis.split(''),function(i,a){var b=a=='x'?'Left':'Top',pos=b.toLowerCase(),key='scroll'+b,old=d[key],max=j.max(d,a);if(toff){attr[key]=toff[pos]+(win?0:old-$elem.offset()[pos]);if(h.margin){attr[key]-=parseInt(targ.css('margin'+b))||0;attr[key]-=parseInt(targ.css('border'+b+'Width'))||0}attr[key]+=e[pos]||0;if(h.over[pos])attr[key]+=targ[a=='x'?'width':'height']()*h.over[pos]}else{var c=targ[pos];attr[key]=c.slice&&c.slice(-1)=='%'?parseFloat(c)/100*max:c}if(h.limit&&/^\d+$/.test(attr[key]))attr[key]=attr[key]<=0?0:Math.min(attr[key],max);if(!i&&h.queue){if(old!=attr[key])animate(h.onAfterFirst);delete attr[key]}});animate(h.onAfter);function animate(a){$elem.animate(attr,g,h.easing,a&&function(){a.call(this,targ,h)})}}).end()};j.max=function(a,b){var c=b=='x'?'Width':'Height',scroll='scroll'+c;if(!$(a).is('html,body'))return a[scroll]-$(a)[c.toLowerCase()]();var d='client'+c,html=a.ownerDocument.documentElement,body=a.ownerDocument.body;return Math.max(html[scroll],body[scroll])-Math.min(html[d],body[d])};function both(a){return $.isFunction(a)||typeof a=='object'?a:{top:a,left:a}};return j}));



/*!
 * jQuery Cycle Plugin (with Transition Definitions)
 * Examples and documentation at: http://jquery.malsup.com/cycle/
 * Copyright (c) 2007-2013 M. Alsup
 * Version: 3.0.3 (11-JUL-2013)
 * Dual licensed under the MIT and GPL licenses.
 * http://jquery.malsup.com/license.html
 * Requires: jQuery v1.7.1 or later
 */
;(function(e,t){"use strict";function r(t){if(e.fn.cycle.debug)i(t)}function i(){}function s(t,n,r){var i=e(t).data("cycle.opts");if(!i)return;var s=!!t.cyclePause;if(s&&i.paused)i.paused(t,i,n,r);else if(!s&&i.resumed)i.resumed(t,i,n,r)}function o(n,r,o){function l(t,n,r){if(!t&&n===true){var s=e(r).data("cycle.opts");if(!s){i("options not found, can not resume");return false}if(r.cycleTimeout){clearTimeout(r.cycleTimeout);r.cycleTimeout=0}p(s.elements,s,1,!s.backwards)}}if(n.cycleStop===t)n.cycleStop=0;if(r===t||r===null)r={};if(r.constructor==String){switch(r){case"destroy":case"stop":var u=e(n).data("cycle.opts");if(!u)return false;n.cycleStop++;if(n.cycleTimeout)clearTimeout(n.cycleTimeout);n.cycleTimeout=0;if(u.elements)e(u.elements).stop();e(n).removeData("cycle.opts");if(r=="destroy")a(n,u);return false;case"toggle":n.cyclePause=n.cyclePause===1?0:1;l(n.cyclePause,o,n);s(n);return false;case"pause":n.cyclePause=1;s(n);return false;case"resume":n.cyclePause=0;l(false,o,n);s(n);return false;case"prev":case"next":u=e(n).data("cycle.opts");if(!u){i('options not found, "prev/next" ignored');return false}if(typeof o=="string")u.oneTimeFx=o;e.fn.cycle[r](u);return false;default:r={fx:r}}return r}else if(r.constructor==Number){var f=r;r=e(n).data("cycle.opts");if(!r){i("options not found, can not advance slide");return false}if(f<0||f>=r.elements.length){i("invalid slide index: "+f);return false}r.nextSlide=f;if(n.cycleTimeout){clearTimeout(n.cycleTimeout);n.cycleTimeout=0}if(typeof o=="string")r.oneTimeFx=o;p(r.elements,r,1,f>=r.currSlide);return false}return r}function u(t,n){if(!e.support.opacity&&n.cleartype&&t.style.filter){try{t.style.removeAttribute("filter")}catch(r){}}}function a(t,n){if(n.next)e(n.next).unbind(n.prevNextEvent);if(n.prev)e(n.prev).unbind(n.prevNextEvent);if(n.pager||n.pagerAnchorBuilder)e.each(n.pagerAnchors||[],function(){this.unbind().remove()});n.pagerAnchors=null;e(t).unbind("mouseenter.cycle mouseleave.cycle");if(n.destroy)n.destroy(n)}function f(n,r,o,a,f){var d;var y=e.extend({},e.fn.cycle.defaults,a||{},e.metadata?n.metadata():e.meta?n.data():{});var b=e.isFunction(n.data)?n.data(y.metaAttr):null;if(b)y=e.extend(y,b);if(y.autostop)y.countdown=y.autostopCount||o.length;var w=n[0];n.data("cycle.opts",y);y.$cont=n;y.stopCount=w.cycleStop;y.elements=o;y.before=y.before?[y.before]:[];y.after=y.after?[y.after]:[];if(!e.support.opacity&&y.cleartype)y.after.push(function(){u(this,y)});if(y.continuous)y.after.push(function(){p(o,y,0,!y.backwards)});l(y);if(!e.support.opacity&&y.cleartype&&!y.cleartypeNoBg)g(r);if(n.css("position")=="static")n.css("position","relative");if(y.width)n.width(y.width);if(y.height&&y.height!="auto")n.height(y.height);if(y.startingSlide!==t){y.startingSlide=parseInt(y.startingSlide,10);if(y.startingSlide>=o.length||y.startSlide<0)y.startingSlide=0;else d=true}else if(y.backwards)y.startingSlide=o.length-1;else y.startingSlide=0;if(y.random){y.randomMap=[];for(var E=0;E<o.length;E++)y.randomMap.push(E);y.randomMap.sort(function(e,t){return Math.random()-.5});if(d){for(var S=0;S<o.length;S++){if(y.startingSlide==y.randomMap[S]){y.randomIndex=S}}}else{y.randomIndex=1;y.startingSlide=y.randomMap[1]}}else if(y.startingSlide>=o.length)y.startingSlide=0;y.currSlide=y.startingSlide||0;var x=y.startingSlide;r.css({position:"absolute",top:0,left:0}).hide().each(function(t){var n;if(y.backwards)n=x?t<=x?o.length+(t-x):x-t:o.length-t;else n=x?t>=x?o.length-(t-x):x-t:o.length-t;e(this).css("z-index",n)});e(o[x]).css("opacity",1).show();u(o[x],y);if(y.fit){if(!y.aspect){if(y.width)r.width(y.width);if(y.height&&y.height!="auto")r.height(y.height)}else{r.each(function(){var t=e(this);var n=y.aspect===true?t.width()/t.height():y.aspect;if(y.width&&t.width()!=y.width){t.width(y.width);t.height(y.width/n)}if(y.height&&t.height()<y.height){t.height(y.height);t.width(y.height*n)}})}}if(y.center&&(!y.fit||y.aspect)){r.each(function(){var t=e(this);t.css({"margin-left":y.width?(y.width-t.width())/2+"px":0,"margin-top":y.height?(y.height-t.height())/2+"px":0})})}if(y.center&&!y.fit&&!y.slideResize){r.each(function(){var t=e(this);t.css({"margin-left":y.width?(y.width-t.width())/2+"px":0,"margin-top":y.height?(y.height-t.height())/2+"px":0})})}var T=(y.containerResize||y.containerResizeHeight)&&n.innerHeight()<1;if(T){var N=0,C=0;for(var k=0;k<o.length;k++){var L=e(o[k]),A=L[0],O=L.outerWidth(),M=L.outerHeight();if(!O)O=A.offsetWidth||A.width||L.attr("width");if(!M)M=A.offsetHeight||A.height||L.attr("height");N=O>N?O:N;C=M>C?M:C}if(y.containerResize&&N>0&&C>0)n.css({width:N+"px",height:C+"px"});if(y.containerResizeHeight&&C>0)n.css({height:C+"px"})}var _=false;if(y.pause)n.bind("mouseenter.cycle",function(){_=true;this.cyclePause++;s(w,true)}).bind("mouseleave.cycle",function(){if(_)this.cyclePause--;s(w,true)});if(c(y)===false)return false;var D=false;a.requeueAttempts=a.requeueAttempts||0;r.each(function(){var t=e(this);this.cycleH=y.fit&&y.height?y.height:t.height()||this.offsetHeight||this.height||t.attr("height")||0;this.cycleW=y.fit&&y.width?y.width:t.width()||this.offsetWidth||this.width||t.attr("width")||0;if(t.is("img")){var n=this.cycleH===0&&this.cycleW===0&&!this.complete;if(n){if(f.s&&y.requeueOnImageNotLoaded&&++a.requeueAttempts<100){i(a.requeueAttempts," - img slide not loaded, requeuing slideshow: ",this.src,this.cycleW,this.cycleH);setTimeout(function(){e(f.s,f.c).cycle(a)},y.requeueTimeout);D=true;return false}else{i("could not determine size of image: "+this.src,this.cycleW,this.cycleH)}}}return true});if(D)return false;y.cssBefore=y.cssBefore||{};y.cssAfter=y.cssAfter||{};y.cssFirst=y.cssFirst||{};y.animIn=y.animIn||{};y.animOut=y.animOut||{};r.not(":eq("+x+")").css(y.cssBefore);e(r[x]).css(y.cssFirst);if(y.timeout){y.timeout=parseInt(y.timeout,10);if(y.speed.constructor==String)y.speed=e.fx.speeds[y.speed]||parseInt(y.speed,10);if(!y.sync)y.speed=y.speed/2;var P=y.fx=="none"?0:y.fx=="shuffle"?500:250;while(y.timeout-y.speed<P)y.timeout+=y.speed}if(y.easing)y.easeIn=y.easeOut=y.easing;if(!y.speedIn)y.speedIn=y.speed;if(!y.speedOut)y.speedOut=y.speed;y.slideCount=o.length;y.currSlide=y.lastSlide=x;if(y.random){if(++y.randomIndex==o.length)y.randomIndex=0;y.nextSlide=y.randomMap[y.randomIndex]}else if(y.backwards)y.nextSlide=y.startingSlide===0?o.length-1:y.startingSlide-1;else y.nextSlide=y.startingSlide>=o.length-1?0:y.startingSlide+1;if(!y.multiFx){var H=e.fn.cycle.transitions[y.fx];if(e.isFunction(H))H(n,r,y);else if(y.fx!="custom"&&!y.multiFx){i("unknown transition: "+y.fx,"; slideshow terminating");return false}}var B=r[x];if(!y.skipInitializationCallbacks){if(y.before.length)y.before[0].apply(B,[B,B,y,true]);if(y.after.length)y.after[0].apply(B,[B,B,y,true])}if(y.next)e(y.next).bind(y.prevNextEvent,function(){return v(y,1)});if(y.prev)e(y.prev).bind(y.prevNextEvent,function(){return v(y,0)});if(y.pager||y.pagerAnchorBuilder)m(o,y);h(y,o);return y}function l(t){t.original={before:[],after:[]};t.original.cssBefore=e.extend({},t.cssBefore);t.original.cssAfter=e.extend({},t.cssAfter);t.original.animIn=e.extend({},t.animIn);t.original.animOut=e.extend({},t.animOut);e.each(t.before,function(){t.original.before.push(this)});e.each(t.after,function(){t.original.after.push(this)})}function c(t){var n,s,o=e.fn.cycle.transitions;if(t.fx.indexOf(",")>0){t.multiFx=true;t.fxs=t.fx.replace(/\s*/g,"").split(",");for(n=0;n<t.fxs.length;n++){var u=t.fxs[n];s=o[u];if(!s||!o.hasOwnProperty(u)||!e.isFunction(s)){i("discarding unknown transition: ",u);t.fxs.splice(n,1);n--}}if(!t.fxs.length){i("No valid transitions named; slideshow terminating.");return false}}else if(t.fx=="all"){t.multiFx=true;t.fxs=[];for(var a in o){if(o.hasOwnProperty(a)){s=o[a];if(o.hasOwnProperty(a)&&e.isFunction(s))t.fxs.push(a)}}}if(t.multiFx&&t.randomizeEffects){var f=Math.floor(Math.random()*20)+30;for(n=0;n<f;n++){var l=Math.floor(Math.random()*t.fxs.length);t.fxs.push(t.fxs.splice(l,1)[0])}r("randomized fx sequence: ",t.fxs)}return true}function h(t,n){t.addSlide=function(r,i){var s=e(r),o=s[0];if(!t.autostopCount)t.countdown++;n[i?"unshift":"push"](o);if(t.els)t.els[i?"unshift":"push"](o);t.slideCount=n.length;if(t.random){t.randomMap.push(t.slideCount-1);t.randomMap.sort(function(e,t){return Math.random()-.5})}s.css("position","absolute");s[i?"prependTo":"appendTo"](t.$cont);if(i){t.currSlide++;t.nextSlide++}if(!e.support.opacity&&t.cleartype&&!t.cleartypeNoBg)g(s);if(t.fit&&t.width)s.width(t.width);if(t.fit&&t.height&&t.height!="auto")s.height(t.height);o.cycleH=t.fit&&t.height?t.height:s.height();o.cycleW=t.fit&&t.width?t.width:s.width();s.css(t.cssBefore);if(t.pager||t.pagerAnchorBuilder)e.fn.cycle.createPagerAnchor(n.length-1,o,e(t.pager),n,t);if(e.isFunction(t.onAddSlide))t.onAddSlide(s);else s.hide()}}function p(n,i,s,o){function m(){var e=0,t=i.timeout;if(i.timeout&&!i.continuous){e=d(n[i.currSlide],n[i.nextSlide],i,o);if(i.fx=="shuffle")e-=i.speedOut}else if(i.continuous&&u.cyclePause)e=10;if(e>0)u.cycleTimeout=setTimeout(function(){p(n,i,0,!i.backwards)},e)}var u=i.$cont[0],a=n[i.currSlide],f=n[i.nextSlide];if(s&&i.busy&&i.manualTrump){r("manualTrump in go(), stopping active transition");e(n).stop(true,true);i.busy=0;clearTimeout(u.cycleTimeout)}if(i.busy){r("transition active, ignoring new tx request");return}if(u.cycleStop!=i.stopCount||u.cycleTimeout===0&&!s)return;if(!s&&!u.cyclePause&&!i.bounce&&(i.autostop&&--i.countdown<=0||i.nowrap&&!i.random&&i.nextSlide<i.currSlide)){if(i.end)i.end(i);return}var l=false;if((s||!u.cyclePause)&&i.nextSlide!=i.currSlide){l=true;var c=i.fx;a.cycleH=a.cycleH||e(a).height();a.cycleW=a.cycleW||e(a).width();f.cycleH=f.cycleH||e(f).height();f.cycleW=f.cycleW||e(f).width();if(i.multiFx){if(o&&(i.lastFx===t||++i.lastFx>=i.fxs.length))i.lastFx=0;else if(!o&&(i.lastFx===t||--i.lastFx<0))i.lastFx=i.fxs.length-1;c=i.fxs[i.lastFx]}if(i.oneTimeFx){c=i.oneTimeFx;i.oneTimeFx=null}e.fn.cycle.resetState(i,c);if(i.before.length)e.each(i.before,function(e,t){if(u.cycleStop!=i.stopCount)return;t.apply(f,[a,f,i,o])});var h=function(){i.busy=0;e.each(i.after,function(e,t){if(u.cycleStop!=i.stopCount)return;t.apply(f,[a,f,i,o])});if(!u.cycleStop){m()}};r("tx firing("+c+"); currSlide: "+i.currSlide+"; nextSlide: "+i.nextSlide);i.busy=1;if(i.fxFn)i.fxFn(a,f,i,h,o,s&&i.fastOnEvent);else if(e.isFunction(e.fn.cycle[i.fx]))e.fn.cycle[i.fx](a,f,i,h,o,s&&i.fastOnEvent);else e.fn.cycle.custom(a,f,i,h,o,s&&i.fastOnEvent)}else{m()}if(l||i.nextSlide==i.currSlide){var v;i.lastSlide=i.currSlide;if(i.random){i.currSlide=i.nextSlide;if(++i.randomIndex==n.length){i.randomIndex=0;i.randomMap.sort(function(e,t){return Math.random()-.5})}i.nextSlide=i.randomMap[i.randomIndex];if(i.nextSlide==i.currSlide)i.nextSlide=i.currSlide==i.slideCount-1?0:i.currSlide+1}else if(i.backwards){v=i.nextSlide-1<0;if(v&&i.bounce){i.backwards=!i.backwards;i.nextSlide=1;i.currSlide=0}else{i.nextSlide=v?n.length-1:i.nextSlide-1;i.currSlide=v?0:i.nextSlide+1}}else{v=i.nextSlide+1==n.length;if(v&&i.bounce){i.backwards=!i.backwards;i.nextSlide=n.length-2;i.currSlide=n.length-1}else{i.nextSlide=v?0:i.nextSlide+1;i.currSlide=v?n.length-1:i.nextSlide-1}}}if(l&&i.pager)i.updateActivePagerLink(i.pager,i.currSlide,i.activePagerClass)}function d(e,t,n,i){if(n.timeoutFn){var s=n.timeoutFn.call(e,e,t,n,i);while(n.fx!="none"&&s-n.speed<250)s+=n.speed;r("calculated timeout: "+s+"; speed: "+n.speed);if(s!==false)return s}return n.timeout}function v(t,n){var r=n?1:-1;var i=t.elements;var s=t.$cont[0],o=s.cycleTimeout;if(o){clearTimeout(o);s.cycleTimeout=0}if(t.random&&r<0){t.randomIndex--;if(--t.randomIndex==-2)t.randomIndex=i.length-2;else if(t.randomIndex==-1)t.randomIndex=i.length-1;t.nextSlide=t.randomMap[t.randomIndex]}else if(t.random){t.nextSlide=t.randomMap[t.randomIndex]}else{t.nextSlide=t.currSlide+r;if(t.nextSlide<0){if(t.nowrap)return false;t.nextSlide=i.length-1}else if(t.nextSlide>=i.length){if(t.nowrap)return false;t.nextSlide=0}}var u=t.onPrevNextEvent||t.prevNextClick;if(e.isFunction(u))u(r>0,t.nextSlide,i[t.nextSlide]);p(i,t,1,n);return false}function m(t,n){var r=e(n.pager);e.each(t,function(i,s){e.fn.cycle.createPagerAnchor(i,s,r,t,n)});n.updateActivePagerLink(n.pager,n.startingSlide,n.activePagerClass)}function g(t){function n(e){e=parseInt(e,10).toString(16);return e.length<2?"0"+e:e}function i(t){for(;t&&t.nodeName.toLowerCase()!="html";t=t.parentNode){var r=e.css(t,"background-color");if(r&&r.indexOf("rgb")>=0){var i=r.match(/\d+/g);return"#"+n(i[0])+n(i[1])+n(i[2])}if(r&&r!="transparent")return r}return"#ffffff"}r("applying clearType background-color hack");t.each(function(){e(this).css("background-color",i(this))})}var n="3.0.3";e.expr[":"].paused=function(e){return e.cyclePause};e.fn.cycle=function(t,n){var s={s:this.selector,c:this.context};if(this.length===0&&t!="stop"){if(!e.isReady&&s.s){i("DOM not ready, queuing slideshow");e(function(){e(s.s,s.c).cycle(t,n)});return this}i("terminating; zero elements found by selector"+(e.isReady?"":" (DOM not ready)"));return this}return this.each(function(){var u=o(this,t,n);if(u===false)return;u.updateActivePagerLink=u.updateActivePagerLink||e.fn.cycle.updateActivePagerLink;if(this.cycleTimeout)clearTimeout(this.cycleTimeout);this.cycleTimeout=this.cyclePause=0;this.cycleStop=0;var a=e(this);var l=u.slideExpr?e(u.slideExpr,this):a.children();var c=l.get();if(c.length<2){i("terminating; too few slides: "+c.length);return}var h=f(a,l,c,u,s);if(h===false)return;var v=h.continuous?10:d(c[h.currSlide],c[h.nextSlide],h,!h.backwards);if(v){v+=h.delay||0;if(v<10)v=10;r("first timeout: "+v);this.cycleTimeout=setTimeout(function(){p(c,h,0,!u.backwards)},v)}})};e.fn.cycle.resetState=function(t,n){n=n||t.fx;t.before=[];t.after=[];t.cssBefore=e.extend({},t.original.cssBefore);t.cssAfter=e.extend({},t.original.cssAfter);t.animIn=e.extend({},t.original.animIn);t.animOut=e.extend({},t.original.animOut);t.fxFn=null;e.each(t.original.before,function(){t.before.push(this)});e.each(t.original.after,function(){t.after.push(this)});var r=e.fn.cycle.transitions[n];if(e.isFunction(r))r(t.$cont,e(t.elements),t)};e.fn.cycle.updateActivePagerLink=function(t,n,r){e(t).each(function(){e(this).children().removeClass(r).eq(n).addClass(r)})};e.fn.cycle.next=function(e){v(e,1)};e.fn.cycle.prev=function(e){v(e,0)};e.fn.cycle.createPagerAnchor=function(t,n,i,o,u){var a;if(e.isFunction(u.pagerAnchorBuilder)){a=u.pagerAnchorBuilder(t,n);r("pagerAnchorBuilder("+t+", el) returned: "+a)}else a='<a href="#">'+(t+1)+"</a>";if(!a)return;var f=e(a);if(f.parents("body").length===0){var l=[];if(i.length>1){i.each(function(){var t=f.clone(true);e(this).append(t);l.push(t[0])});f=e(l)}else{f.appendTo(i)}}u.pagerAnchors=u.pagerAnchors||[];u.pagerAnchors.push(f);var c=function(n){n.preventDefault();u.nextSlide=t;var r=u.$cont[0],i=r.cycleTimeout;if(i){clearTimeout(i);r.cycleTimeout=0}var s=u.onPagerEvent||u.pagerClick;if(e.isFunction(s))s(u.nextSlide,o[u.nextSlide]);p(o,u,1,u.currSlide<t)};if(/mouseenter|mouseover/i.test(u.pagerEvent)){f.hover(c,function(){})}else{f.bind(u.pagerEvent,c)}if(!/^click/.test(u.pagerEvent)&&!u.allowPagerClickBubble)f.bind("click.cycle",function(){return false});var h=u.$cont[0];var d=false;if(u.pauseOnPagerHover){f.hover(function(){d=true;h.cyclePause++;s(h,true,true)},function(){if(d)h.cyclePause--;s(h,true,true)})}};e.fn.cycle.hopsFromLast=function(e,t){var n,r=e.lastSlide,i=e.currSlide;if(t)n=i>r?i-r:e.slideCount-r;else n=i<r?r-i:r+e.slideCount-i;return n};e.fn.cycle.commonReset=function(t,n,r,i,s,o){e(r.elements).not(t).hide();if(typeof r.cssBefore.opacity=="undefined")r.cssBefore.opacity=1;r.cssBefore.display="block";if(r.slideResize&&i!==false&&n.cycleW>0)r.cssBefore.width=n.cycleW;if(r.slideResize&&s!==false&&n.cycleH>0)r.cssBefore.height=n.cycleH;r.cssAfter=r.cssAfter||{};r.cssAfter.display="none";e(t).css("zIndex",r.slideCount+(o===true?1:0));e(n).css("zIndex",r.slideCount+(o===true?0:1))};e.fn.cycle.custom=function(t,n,r,i,s,o){var u=e(t),a=e(n);var f=r.speedIn,l=r.speedOut,c=r.easeIn,h=r.easeOut,p=r.animInDelay,d=r.animOutDelay;a.css(r.cssBefore);if(o){if(typeof o=="number")f=l=o;else f=l=1;c=h=null}var v=function(){a.delay(p).animate(r.animIn,f,c,function(){i()})};u.delay(d).animate(r.animOut,l,h,function(){u.css(r.cssAfter);if(!r.sync)v()});if(r.sync)v()};e.fn.cycle.transitions={fade:function(t,n,r){n.not(":eq("+r.currSlide+")").css("opacity",0);r.before.push(function(t,n,r){e.fn.cycle.commonReset(t,n,r);r.cssBefore.opacity=0});r.animIn={opacity:1};r.animOut={opacity:0};r.cssBefore={top:0,left:0}}};e.fn.cycle.ver=function(){return n};e.fn.cycle.defaults={activePagerClass:"activeSlide",after:null,allowPagerClickBubble:false,animIn:null,animInDelay:0,animOut:null,animOutDelay:0,aspect:false,autostop:0,autostopCount:0,backwards:false,before:null,center:null,cleartype:!e.support.opacity,cleartypeNoBg:false,containerResize:1,containerResizeHeight:0,continuous:0,cssAfter:null,cssBefore:null,delay:0,easeIn:null,easeOut:null,easing:null,end:null,fastOnEvent:0,fit:0,fx:"fade",fxFn:null,height:"auto",manualTrump:true,metaAttr:"cycle",next:null,nowrap:0,onPagerEvent:null,onPrevNextEvent:null,pager:null,pagerAnchorBuilder:null,pagerEvent:"click.cycle",pause:0,pauseOnPagerHover:0,prev:null,prevNextEvent:"click.cycle",random:0,randomizeEffects:1,requeueOnImageNotLoaded:true,requeueTimeout:250,rev:0,shuffle:null,skipInitializationCallbacks:false,slideExpr:null,slideResize:1,speed:1e3,speedIn:null,speedOut:null,startingSlide:t,sync:1,timeout:4e3,timeoutFn:null,updateActivePagerLink:null,width:null}})(jQuery);(function(e){"use strict";e.fn.cycle.transitions.none=function(t,n,r){r.fxFn=function(t,n,r,i){e(n).show();e(t).hide();i()}};e.fn.cycle.transitions.fadeout=function(t,n,r){n.not(":eq("+r.currSlide+")").css({display:"block",opacity:1});r.before.push(function(t,n,r,i,s,o){e(t).css("zIndex",r.slideCount+(o!==true?1:0));e(n).css("zIndex",r.slideCount+(o!==true?0:1))});r.animIn.opacity=1;r.animOut.opacity=0;r.cssBefore.opacity=1;r.cssBefore.display="block";r.cssAfter.zIndex=0};e.fn.cycle.transitions.scrollUp=function(t,n,r){t.css("overflow","hidden");r.before.push(e.fn.cycle.commonReset);var i=t.height();r.cssBefore.top=i;r.cssBefore.left=0;r.cssFirst.top=0;r.animIn.top=0;r.animOut.top=-i};e.fn.cycle.transitions.scrollDown=function(t,n,r){t.css("overflow","hidden");r.before.push(e.fn.cycle.commonReset);var i=t.height();r.cssFirst.top=0;r.cssBefore.top=-i;r.cssBefore.left=0;r.animIn.top=0;r.animOut.top=i};e.fn.cycle.transitions.scrollLeft=function(t,n,r){t.css("overflow","hidden");r.before.push(e.fn.cycle.commonReset);var i=t.width();r.cssFirst.left=0;r.cssBefore.left=i;r.cssBefore.top=0;r.animIn.left=0;r.animOut.left=0-i};e.fn.cycle.transitions.scrollRight=function(t,n,r){t.css("overflow","hidden");r.before.push(e.fn.cycle.commonReset);var i=t.width();r.cssFirst.left=0;r.cssBefore.left=-i;r.cssBefore.top=0;r.animIn.left=0;r.animOut.left=i};e.fn.cycle.transitions.scrollHorz=function(t,n,r){t.css("overflow","hidden").width();r.before.push(function(t,n,r,i){if(r.rev)i=!i;e.fn.cycle.commonReset(t,n,r);r.cssBefore.left=i?n.cycleW-1:1-n.cycleW;r.animOut.left=i?-t.cycleW:t.cycleW});r.cssFirst.left=0;r.cssBefore.top=0;r.animIn.left=0;r.animOut.top=0};e.fn.cycle.transitions.scrollVert=function(t,n,r){t.css("overflow","hidden");r.before.push(function(t,n,r,i){if(r.rev)i=!i;e.fn.cycle.commonReset(t,n,r);r.cssBefore.top=i?1-n.cycleH:n.cycleH-1;r.animOut.top=i?t.cycleH:-t.cycleH});r.cssFirst.top=0;r.cssBefore.left=0;r.animIn.top=0;r.animOut.left=0};e.fn.cycle.transitions.slideX=function(t,n,r){r.before.push(function(t,n,r){e(r.elements).not(t).hide();e.fn.cycle.commonReset(t,n,r,false,true);r.animIn.width=n.cycleW});r.cssBefore.left=0;r.cssBefore.top=0;r.cssBefore.width=0;r.animIn.width="show";r.animOut.width=0};e.fn.cycle.transitions.slideY=function(t,n,r){r.before.push(function(t,n,r){e(r.elements).not(t).hide();e.fn.cycle.commonReset(t,n,r,true,false);r.animIn.height=n.cycleH});r.cssBefore.left=0;r.cssBefore.top=0;r.cssBefore.height=0;r.animIn.height="show";r.animOut.height=0};e.fn.cycle.transitions.shuffle=function(t,n,r){var i,s=t.css("overflow","visible").width();n.css({left:0,top:0});r.before.push(function(t,n,r){e.fn.cycle.commonReset(t,n,r,true,true,true)});if(!r.speedAdjusted){r.speed=r.speed/2;r.speedAdjusted=true}r.random=0;r.shuffle=r.shuffle||{left:-s,top:15};r.els=[];for(i=0;i<n.length;i++)r.els.push(n[i]);for(i=0;i<r.currSlide;i++)r.els.push(r.els.shift());r.fxFn=function(t,n,r,i,s){if(r.rev)s=!s;var o=s?e(t):e(n);e(n).css(r.cssBefore);var u=r.slideCount;o.animate(r.shuffle,r.speedIn,r.easeIn,function(){var n=e.fn.cycle.hopsFromLast(r,s);for(var a=0;a<n;a++){if(s)r.els.push(r.els.shift());else r.els.unshift(r.els.pop())}if(s){for(var f=0,l=r.els.length;f<l;f++)e(r.els[f]).css("z-index",l-f+u)}else{var c=e(t).css("z-index");o.css("z-index",parseInt(c,10)+1+u)}o.animate({left:0,top:0},r.speedOut,r.easeOut,function(){e(s?this:t).hide();if(i)i()})})};e.extend(r.cssBefore,{display:"block",opacity:1,top:0,left:0})};e.fn.cycle.transitions.turnUp=function(t,n,r){r.before.push(function(t,n,r){e.fn.cycle.commonReset(t,n,r,true,false);r.cssBefore.top=n.cycleH;r.animIn.height=n.cycleH;r.animOut.width=n.cycleW});r.cssFirst.top=0;r.cssBefore.left=0;r.cssBefore.height=0;r.animIn.top=0;r.animOut.height=0};e.fn.cycle.transitions.turnDown=function(t,n,r){r.before.push(function(t,n,r){e.fn.cycle.commonReset(t,n,r,true,false);r.animIn.height=n.cycleH;r.animOut.top=t.cycleH});r.cssFirst.top=0;r.cssBefore.left=0;r.cssBefore.top=0;r.cssBefore.height=0;r.animOut.height=0};e.fn.cycle.transitions.turnLeft=function(t,n,r){r.before.push(function(t,n,r){e.fn.cycle.commonReset(t,n,r,false,true);r.cssBefore.left=n.cycleW;r.animIn.width=n.cycleW});r.cssBefore.top=0;r.cssBefore.width=0;r.animIn.left=0;r.animOut.width=0};e.fn.cycle.transitions.turnRight=function(t,n,r){r.before.push(function(t,n,r){e.fn.cycle.commonReset(t,n,r,false,true);r.animIn.width=n.cycleW;r.animOut.left=t.cycleW});e.extend(r.cssBefore,{top:0,left:0,width:0});r.animIn.left=0;r.animOut.width=0};e.fn.cycle.transitions.zoom=function(t,n,r){r.before.push(function(t,n,r){e.fn.cycle.commonReset(t,n,r,false,false,true);r.cssBefore.top=n.cycleH/2;r.cssBefore.left=n.cycleW/2;e.extend(r.animIn,{top:0,left:0,width:n.cycleW,height:n.cycleH});e.extend(r.animOut,{width:0,height:0,top:t.cycleH/2,left:t.cycleW/2})});r.cssFirst.top=0;r.cssFirst.left=0;r.cssBefore.width=0;r.cssBefore.height=0};e.fn.cycle.transitions.fadeZoom=function(t,n,r){r.before.push(function(t,n,r){e.fn.cycle.commonReset(t,n,r,false,false);r.cssBefore.left=n.cycleW/2;r.cssBefore.top=n.cycleH/2;e.extend(r.animIn,{top:0,left:0,width:n.cycleW,height:n.cycleH})});r.cssBefore.width=0;r.cssBefore.height=0;r.animOut.opacity=0};e.fn.cycle.transitions.blindX=function(t,n,r){var i=t.css("overflow","hidden").width();r.before.push(function(t,n,r){e.fn.cycle.commonReset(t,n,r);r.animIn.width=n.cycleW;r.animOut.left=t.cycleW});r.cssBefore.left=i;r.cssBefore.top=0;r.animIn.left=0;r.animOut.left=i};e.fn.cycle.transitions.blindY=function(t,n,r){var i=t.css("overflow","hidden").height();r.before.push(function(t,n,r){e.fn.cycle.commonReset(t,n,r);r.animIn.height=n.cycleH;r.animOut.top=t.cycleH});r.cssBefore.top=i;r.cssBefore.left=0;r.animIn.top=0;r.animOut.top=i};e.fn.cycle.transitions.blindZ=function(t,n,r){var i=t.css("overflow","hidden").height();var s=t.width();r.before.push(function(t,n,r){e.fn.cycle.commonReset(t,n,r);r.animIn.height=n.cycleH;r.animOut.top=t.cycleH});r.cssBefore.top=i;r.cssBefore.left=s;r.animIn.top=0;r.animIn.left=0;r.animOut.top=i;r.animOut.left=s};e.fn.cycle.transitions.growX=function(t,n,r){r.before.push(function(t,n,r){e.fn.cycle.commonReset(t,n,r,false,true);r.cssBefore.left=this.cycleW/2;r.animIn.left=0;r.animIn.width=this.cycleW;r.animOut.left=0});r.cssBefore.top=0;r.cssBefore.width=0};e.fn.cycle.transitions.growY=function(t,n,r){r.before.push(function(t,n,r){e.fn.cycle.commonReset(t,n,r,true,false);r.cssBefore.top=this.cycleH/2;r.animIn.top=0;r.animIn.height=this.cycleH;r.animOut.top=0});r.cssBefore.height=0;r.cssBefore.left=0};e.fn.cycle.transitions.curtainX=function(t,n,r){r.before.push(function(t,n,r){e.fn.cycle.commonReset(t,n,r,false,true,true);r.cssBefore.left=n.cycleW/2;r.animIn.left=0;r.animIn.width=this.cycleW;r.animOut.left=t.cycleW/2;r.animOut.width=0});r.cssBefore.top=0;r.cssBefore.width=0};e.fn.cycle.transitions.curtainY=function(t,n,r){r.before.push(function(t,n,r){e.fn.cycle.commonReset(t,n,r,true,false,true);r.cssBefore.top=n.cycleH/2;r.animIn.top=0;r.animIn.height=n.cycleH;r.animOut.top=t.cycleH/2;r.animOut.height=0});r.cssBefore.height=0;r.cssBefore.left=0};e.fn.cycle.transitions.cover=function(t,n,r){var i=r.direction||"left";var s=t.css("overflow","hidden").width();var o=t.height();r.before.push(function(t,n,r){e.fn.cycle.commonReset(t,n,r);r.cssAfter.display="";if(i=="right")r.cssBefore.left=-s;else if(i=="up")r.cssBefore.top=o;else if(i=="down")r.cssBefore.top=-o;else r.cssBefore.left=s});r.animIn.left=0;r.animIn.top=0;r.cssBefore.top=0;r.cssBefore.left=0};e.fn.cycle.transitions.uncover=function(t,n,r){var i=r.direction||"left";var s=t.css("overflow","hidden").width();var o=t.height();r.before.push(function(t,n,r){e.fn.cycle.commonReset(t,n,r,true,true,true);if(i=="right")r.animOut.left=s;else if(i=="up")r.animOut.top=-o;else if(i=="down")r.animOut.top=o;else r.animOut.left=-s});r.animIn.left=0;r.animIn.top=0;r.cssBefore.top=0;r.cssBefore.left=0};e.fn.cycle.transitions.toss=function(t,n,r){var i=t.css("overflow","visible").width();var s=t.height();r.before.push(function(t,n,r){e.fn.cycle.commonReset(t,n,r,true,true,true);if(!r.animOut.left&&!r.animOut.top)e.extend(r.animOut,{left:i*2,top:-s/2,opacity:0});else r.animOut.opacity=0});r.cssBefore.left=0;r.cssBefore.top=0;r.animIn.left=0};e.fn.cycle.transitions.wipe=function(t,n,r){var i=t.css("overflow","hidden").width();var s=t.height();r.cssBefore=r.cssBefore||{};var o;if(r.clip){if(/l2r/.test(r.clip))o="rect(0px 0px "+s+"px 0px)";else if(/r2l/.test(r.clip))o="rect(0px "+i+"px "+s+"px "+i+"px)";else if(/t2b/.test(r.clip))o="rect(0px "+i+"px 0px 0px)";else if(/b2t/.test(r.clip))o="rect("+s+"px "+i+"px "+s+"px 0px)";else if(/zoom/.test(r.clip)){var u=parseInt(s/2,10);var a=parseInt(i/2,10);o="rect("+u+"px "+a+"px "+u+"px "+a+"px)"}}r.cssBefore.clip=r.cssBefore.clip||o||"rect(0px 0px 0px 0px)";var f=r.cssBefore.clip.match(/(\d+)/g);var l=parseInt(f[0],10),c=parseInt(f[1],10),h=parseInt(f[2],10),p=parseInt(f[3],10);r.before.push(function(t,n,r){if(t==n)return;var o=e(t),u=e(n);e.fn.cycle.commonReset(t,n,r,true,true,false);r.cssAfter.display="block";var a=1,f=parseInt(r.speedIn/13,10)-1;(function d(){var e=l?l-parseInt(a*(l/f),10):0;var t=p?p-parseInt(a*(p/f),10):0;var n=h<s?h+parseInt(a*((s-h)/f||1),10):s;var r=c<i?c+parseInt(a*((i-c)/f||1),10):i;u.css({clip:"rect("+e+"px "+r+"px "+n+"px "+t+"px)"});a++<=f?setTimeout(d,13):o.css("display","none")})()});e.extend(r.cssBefore,{display:"block",opacity:1,top:0,left:0});r.animIn={left:0};r.animOut={left:0}}})(jQuery)

$( document ).ready(function() {
    
var special = [];
     
$('input.special_add').change(function(){
    var index = $(this).parent().parent().next().text();
    var value = parseInt($(this).parent().parent().next().next().next().val());
if($(this).prop('checked')){
   var arr = [];
   arr.push(index);
   arr.push(value);
   special.push(arr);
   }else{
       var the_index = special.indexOf(index);
       console.log(the_index);
       if (the_index > -1) {
           special.splice(the_index, 1);
      }
 } 
  console.log(special);
});
    
$('.edit_special').click(function(){

    if($(this).hasClass('up')){
     if($(this).prev().prev().children().children().prop('checked')){
      var value = parseInt($(this).next().val());
       var index = $(this).prev().text();
       $.each(special, function(key, val){
          if(val[0] == index){
            val[1] = value;
            
           }
        });
       }
     }
       else{
        if($(this).prev().prev().prev().prev().children().children().prop('checked')){
       var value = parseInt($(this).prev().val());
      var index = $(this).prev().prev().prev().text();

       $.each(special, function(key, val){
          if(val['index'] == index){
            val['value'] = value;

           }
        });
          }
       }  
console.log(special);
 });
    
//add to order
$('#add_order').on('click', function(){
var time = $('select.hour').val() + ':' + $('select.time-first').val();
console.log(time);
var name = $('input[name="name"]').val();
var phone = $('input[name="phone"]').val();
var mail = $('input[name="mail"]').val();
var company = $('input[name="company"]').val();
var company_phone = $('input[name="company_phone"]').val();
var city = $('input[name="city"]').val();
var street = $('input[name="street"]').val();
var house_number = $('input[name="house_number"]').val();
var entrence = $('input[name="entrence"]').val();
var apartment_number = $('input[name="apartment_number"]').val();
var floor = $('input[name="floor"]').val();
var comments = $('textarea[name="comments"]').val();
var password= $('input[name="regis_password"]').val();
console.log(password);
var re_password = $('input[name="re_password"]').val();
var fix_price = $('#fix_price').html();
var rest_comments = $('table#total textarea').val();
var delivery = $('#delivery').html();
var final_price = $('#final').html();
var restaurent = $('#rest').html();
var rest_id = $('#rest_id').html();
var special_string = special.join();
if(name == ''){
     alert('שם שדה נדרש'); 
    
    }else if(phone == ' '){
    
   alert('טלפון שדה נדרש');
   }else if(city == ''){
 
    alert('עיר שדה נדרש');
 
    } else if(street == ''){
   alert('רחוב שדה נדרש')
 
  }else if(house_number == ''){
  
     alert('מספר בית שדה נדרש')
    }else if(password == ''){
        
         alert('סיסמא נדרשת');
    
    }else if(password != re_password){
    
      alert('סיסמא לא תואמת');
    
       }else{
    
       $.ajax({
			  url: "index.php?route=checkout/cart/addToNewCart/",
			  type: "POST",
			  dataType: "html",
			  async: "false", 
			  data: { name:name, mail:mail, phone:phone, company:company, company_phone:company_phone, city:city, street:street, house_number:house_number, entrence:entrence, apartment_number:apartment_number, floor:floor, comments:comments, fix_price:fix_price, rest_comments:rest_comments, delivery:delivery, final_price:final_price, restaurent:restaurent, rest_id:rest_id, special_string:special_string, password:password, time:time},
			  success: function(response) {			
                    if(response){
                       
                       window.location = 'index.php?route=account/thankyou';
                    
                     }	  
			}	
    
        });
}
});
    
    

    
//choose city
    
$('#hp-search-btn').click(function(){

 var city = $('#hp-search-input').val();
    console.log(city);
        $.ajax({
			  url: "index.php?route=product/quickyCategory/createSess",
			  type: "POST",
			  dataType: "html",
			  async: "false", 
			  data: { city:city},  
            success: function(response) {	
               window.location = 'index.php?route=product/quickyCategory';
            }
        });


});
    
//document ready
});

function editCart(name, quantity, sign, elem){
        
        $.ajax({
			  url: "index.php?route=checkout/cart/editCart",
			  type: "POST",
			  dataType: "html",
			  async: "false", 
			  data: { name:name, quantity:quantity, sign:sign},  
            success: function(response) {
               var price_arr = response.split(",");
               $(elem).next().html(price_arr[0] + ' שח');
               var finel_price = $('div#notification .total.overall span:nth-child(2)').html();
              var finel_price_arr = finel_price.split(" ");
             if(sign == "plus"){
              var int = parseFloat(finel_price_arr[0]) + parseFloat(price_arr[1]);
             }else{
                 var int = parseFloat(finel_price_arr[0]) - parseFloat(price_arr[1]);
             }
             var s =  parseFloat(Math.round(int * 100) / 100).toFixed(1);
              $('div#notification .total.overall span:nth-child(2)').html(s + ' ₪');
            }
        });



 }
 
 function editfinelCart(key, quantity,elem, deliver, sign){
       $.ajax({
			  url: "index.php?route=checkout/cart/editCart",
			  type: "POST",
			  dataType: "html",
			  async: "false",
             data: {name:key, quantity:quantity},
             success:function(response){
             var res = response.split(",");
             $(elem).html('₪' + res[0]);
            if(sign == "plus"){
             var finel_price = parseFloat(res[1]) + deliver;
            }else{
                
             var finel_price = deliver -  parseFloat(res[1]);
            }
             $('#final').html(parseFloat(Math.round(finel_price * 100) / 100).toFixed(1));
             }
         });
 
 }
 
 function makeFrame() { 
   ifrm = document.createElement("IFRAME"); 
   ifrm.setAttribute("src", "http://developerfusion.com/"); 
   ifrm.style.width = 640+"px"; 
   ifrm.style.height = 480+"px"; 
   document.body.appendChild(ifrm); 
}

