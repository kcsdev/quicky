function nl2br (str, is_xhtml) {
    var breakTag = (is_xhtml || typeof is_xhtml === 'undefined') ? '<br />' : '<br>';
    return (str + '').replace(/([^>\r\n]?)(\r\n|\n\r|\r|\n)/g, '$1' + breakTag + '$2');
}

function sendMsg() {
	
	$.ajax({
		type: "POST",
		dataType: "json",
		url: 'index.php?route=account/msmessage/jxSendMessage',
		data: $(this).parents("form").serialize(),
		beforeSend: function() {
			$('#ms-message-form .button').before('<span class="wait">&nbsp;<img src="catalog/view/theme/default/image/loading.gif" alt="" /></span>');
			$("#ms-message-reply").off();
		},
		complete: function() {
			$('.wait').hide();
			$("#ms-message-reply").on("click",sendMsg);
		},
		success: function(jsonData) {
			$('.error').text('');
			if (!jQuery.isEmptyObject(jsonData.errors)) {
				$('#ms-message-form a.button').show().prev('span.wait').remove();
				for (error in jsonData.errors) {
					if (!jsonData.errors.hasOwnProperty(error)) {
						continue;
					}
					$('#error_text').text(jsonData.errors[error]);
					window.scrollTo(0,0);
				}
			} else {
				var message = $(".messages-info .review-list").first().clone();
				//just a precaution
				message.removeClass("him");
				message.addClass("me");
				
				message.find(".text").html(nl2br(jsonData['msg_text']));
				message.find(".author").text($("#ms-message-div .review-list").first().find(".author").text());
				message.find(".avatar").attr("src",$("#ms-message-div .review-list").first().find(".avatar").attr("src"));
				message.find(".review_date").text("עכשיו");
				
				message.fadeIn("slow").prependTo(".messages-info .review");
				$("#ms-message-text").val("").trigger('autosize.resize');
				//location = jsonData['redirect'];
			}
		}
	});
	
};

$(function() {
	
	$("#ms-message-reply").on("click",sendMsg);
	
	$("#ms-message-text").focus(function() {
		$(this).val('').unbind('focus');
	});
});
