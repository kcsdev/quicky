<?php
class ModelCheckoutCoupon extends Model {
	public function getCoupon($code) {
		$status = true;
		
		$coupon_query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "coupon` WHERE code = '" . $this->db->escape($code) . "' AND ((date_start = '0000-00-00' OR date_start < NOW()) AND (date_end = '0000-00-00' OR date_end > NOW())) AND status = '1'");
		
		if ($coupon_query->num_rows) {
			if ($coupon_query->row['total'] >= $this->cart->getTotal()) {
				$status = false;
			}
			
			if (isset($this->session->data['coupon_new'])) {
				$status = true;
			}
			

			$coupon_history_query = $this->db->query("SELECT COUNT(*) AS total FROM `" . DB_PREFIX . "coupon_history` ch WHERE ch.coupon_id = '" . (int)$coupon_query->row['coupon_id'] . "'");

			if ($coupon_query->row['uses_total'] > 0 && ($coupon_history_query->row['total'] >= $coupon_query->row['uses_total'])) {
				$status = false;
			}

			if ($coupon_query->row['logged'] && !$this->customer->getId()) {
				$status = false;
			}

			if ($this->customer->getId()) {
				$coupon_history_query = $this->db->query("SELECT COUNT(*) AS total FROM `" . DB_PREFIX . "coupon_history` ch WHERE ch.coupon_id = '" . (int)$coupon_query->row['coupon_id'] . "' AND ch.customer_id = '" . (int)$this->customer->getId() . "'");

				if ($coupon_query->row['uses_customer'] > 0 && ($coupon_history_query->row['total'] >= $coupon_query->row['uses_customer'])) {
					$status = false;
				}
			}
			
			
			
			/****************************************
			 ****************************************/
			// new homeals spiceal coupon add ons
			/****************************************
			****************************************/
			
			
			
			if ($coupon_query->row['link_only']) {
				
				// if customer has this cuopon on
				if ($this->customer->getId() && !isset($this->session->data['coupon_new'])) {
					$coupon_customer_query = $this->db->query("SELECT COUNT(*) AS total FROM `" . DB_PREFIX . "coupon_customer` cc WHERE cc.coupon_id = '" . (int)$coupon_query->row['coupon_id'] . "' AND cc.customer_id = '" . (int)$this->customer->getId() . "'");
					// if we didnt find the user in the list we disallow the cuopon
					if (!$coupon_customer_query->row['total']) {
						$status = false;
					}
				}
				
			}
			
			
			
			/****************************************
			 ****************************************/
			// ends here
			/****************************************
			 ****************************************/
			
			// Products
			$coupon_product_data = array();

			$coupon_product_query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "coupon_product` WHERE coupon_id = '" . (int)$coupon_query->row['coupon_id'] . "'");

			foreach ($coupon_product_query->rows as $product) {
				$coupon_product_data[] = $product['product_id'];
			}

			// Categories
			$coupon_category_data = array();

			$coupon_category_query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "coupon_category` cc LEFT JOIN `" . DB_PREFIX . "category_path` cp ON (cc.category_id = cp.path_id) WHERE cc.coupon_id = '" . (int)$coupon_query->row['coupon_id'] . "'");

			foreach ($coupon_category_query->rows as $category) {
				$coupon_category_data[] = $category['category_id'];
			}			
			
			$product_data = array();
			
			if ($coupon_product_data || $coupon_category_data) {
				foreach ($this->cart->getProducts() as $product) {
					if (in_array($product['product_id'], $coupon_product_data)) {
						$product_data[] = $product['product_id'];

						continue;
					}

					foreach ($coupon_category_data as $category_id) {
						$coupon_category_query = $this->db->query("SELECT COUNT(*) AS total FROM `" . DB_PREFIX . "product_to_category` WHERE `product_id` = '" . (int)$product['product_id'] . "' AND category_id = '" . (int)$category_id . "'");

						if ($coupon_category_query->row['total']) {
							$product_data[] = $product['product_id'];

							continue;
						}						
					}
				}	

				if (!$product_data) {
					$status = false;
				}
			}
		} else {
			$status = false;
		}

		if ($status) {
			return array(
				'coupon_id'     => $coupon_query->row['coupon_id'],
				'code'          => $coupon_query->row['code'],
				'name'          => $coupon_query->row['name'],
				'type'          => $coupon_query->row['type'],
				'discount'      => $coupon_query->row['discount'],
				'shipping'      => $coupon_query->row['shipping'],
				'total'         => $coupon_query->row['total'],
				'product'       => $product_data,
				'date_start'    => $coupon_query->row['date_start'],
				'date_end'      => $coupon_query->row['date_end'],
				'uses_total'    => $coupon_query->row['uses_total'],
				'uses_customer' => $coupon_query->row['uses_customer'],
				'status'        => $coupon_query->row['status'],
				'date_added'    => $coupon_query->row['date_added']
			);
		}
	}

	public function redeem($coupon_id, $order_id, $customer_id, $amount) {
		$this->db->query("INSERT INTO `" . DB_PREFIX . "coupon_history` SET coupon_id = '" . (int)$coupon_id . "', order_id = '" . (int)$order_id . "', customer_id = '" . (int)$customer_id . "', amount = '" . (float)$amount . "', date_added = NOW()");
	}
	
	public function hasCoupon() {
		
		$success = false;
		
		if ($this->customer->getId()) {
			
			$coupon_query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "coupon_customer` cc LEFT JOIN `oc_coupon` c ON cc.coupon_id = c.coupon_id WHERE customer_id = '" . (int)$this->customer->getId() . "'");
			
			foreach ($coupon_query->rows as $coupon_info) {
				$this->load->model('checkout/coupon');
				
				if($this->cart->hasProducts()){
					if($this->model_checkout_coupon->getCoupon($coupon_info['code'])){
						$success = true;
					}	
				} else {
					// if we have no products but have listing we will show the coupon becuase we cant tell before
					$success = true;
				}
				
				
			}
			
			if($success){
				return true;
			}
			
		}
		
		return false;
	}
	
	public function hasDirectCoupon() {
		
		$success = false;
		
		if ($this->customer->getId()) {
			
			$coupon_query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "coupon_customer` cc LEFT JOIN `oc_coupon` c ON cc.coupon_id = c.coupon_id WHERE customer_id = '" . (int)$this->customer->getId() . "' AND cc.cc_type = 'direct'");
			
			foreach ($coupon_query->rows as $coupon_info) {
			
				$coupon_info = $coupon_query->row;
				
				$this->load->model('checkout/coupon');
				
				if($this->model_checkout_coupon->getCoupon($coupon_info['code'])){
					$success = $coupon_info['code'];
				}
			
			}
			
			//return print_r($coupon_query->rows);
			
			if($success){
				return $success;
			}
			
		}
		
		return false;
	}
	
	public function forceCoupons() {
		//more code later
		
		return true;
	}
	
	public function couponCheck($code){
		$coupon_query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "coupon` WHERE code = '" . $this->db->escape($code) . "' AND ((date_start = '0000-00-00' OR date_start < NOW()) AND (date_end = '0000-00-00' OR date_end > NOW())) AND status = '1'");
		
		if ($coupon_query->num_rows) {
			return array(
				'coupon_id'     => $coupon_query->row['coupon_id'],
				'code'          => $coupon_query->row['code'],
				'name'          => $coupon_query->row['name'],
				'type'          => $coupon_query->row['type'],
				'discount'      => $coupon_query->row['discount'],
				'status'        => $coupon_query->row['status'],
				'date_added'    => $coupon_query->row['date_added']
			);
		} else {
			return false;
		}
	}
	
	public function addToCCList($coupon_id, $customer_id) {
		$coupon_query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "coupon_customer` WHERE coupon_id = '" . (int)$coupon_id . "' AND customer_id = '" . (int)$customer_id . "'");
		
		if (!$coupon_query->num_rows) {
			if (isset($this->session->data['coupon_new'])) {
				$this->db->query("INSERT INTO `" . DB_PREFIX . "coupon_customer` SET coupon_id = '" . (int)$coupon_id . "', customer_id = '" . (int)$customer_id . "', date_added = NOW()");
				return "added";
			} else {
				return false;
			}
		} else {
			return "was before";
		}
	}
	
	public function addToCCListDirect($coupon_id, $customer_id) {
		$coupon_query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "coupon_customer` WHERE coupon_id = '" . (int)$coupon_id . "' AND customer_id = '" . (int)$customer_id . "'");
		
		if (!$coupon_query->num_rows) {
			if (isset($this->session->data['coupon_new'])) {
				$this->db->query("INSERT INTO `" . DB_PREFIX . "coupon_customer` SET coupon_id = '" . (int)$coupon_id . "', customer_id = '" . (int)$customer_id . "', cc_type = 'direct', date_added = NOW()");
				return "added";
			} else {
				return false;
			}
		} else {
			return "was before";
		}
	}
	
}
?>
