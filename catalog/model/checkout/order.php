<?php
class ModelCheckoutOrder extends Model {
    
    public function addNewOrder($data){
    
    $new =  $this->db->query("INSERT INTO oc_new_order SET name = '" . $data['name'] . "', phone= '" . $data['phone'] . "', mail = '" . $data['mail'] . "', company = '" . $data['company'] . "', company_phone = '" . $data['company_phone'] . "', city = '" . $data['city'] . "', street = '" . $data['street'] . "', house_number = '" . $data['house_number']. "', entrence = '" . $data['entrence'] . "', appartment_number = '" . $data['apartment_number'] . "', floor = '" . $data['floor'] . "', comments = '" . $data['comments'] . "', price = '" . $data['price'] . "', rest_comments = '" . $data['rest_comments'] . "', order_cart = '" . $data['order'] . "', create_at = NOW(), delivey = '" . $data['delivery'] . "', final_price = '" . $data['final_price'] . "', restaurent = '" . $data['restaurent'] . "', rest_id = " . $data['rest_id'] . ", special = '" . $data['special'] . "', time = '" . $data['time'] . "'");
    
    return $new;
    
    }
    
    public function checkuser($data){
    
    $user = $this->db->query("SELECT * FROM oc_customer WHERE telephone = '" . $data['phone'] . "'");
    
    if(empty($user->rows)){
        
        $this->db->query("INSERT INTO " . DB_PREFIX . "customer SET store_id = 0, firstname = '" . $this->db->escape($data['name']) . "', lastname = '', email = '" . $this->db->escape($data['mail']) . "', telephone = '" . $this->db->escape($data['phone']) . "', fax = '12345', salt = '" . $this->db->escape($salt = substr(md5(uniqid(rand(), true)), 0, 9)) . "', password = '" . $this->db->escape(sha1($salt . sha1($salt . sha1($data['password'])))) . "', newsletter = '" . (isset($data['newsletter']) ? (int)$data['newsletter'] : 0) . "', customer_group_id = '1', ip = '" . $this->db->escape($this->request->server['REMOTE_ADDR']) . "', status = '1', approved = '1', date_added = NOW(), city = '" . $data['city'] .  "'");
    
    
       }
    
    
    }
    
    
    
    
	public function addOrder($data) {
		
		if(isset($data['delivery_time'])){
			$d_time = $data['delivery_time'];
		} else {
			$d_time ="";
		}
		
		if(isset($data['delivery_date'])){
			$d_date = $data['delivery_date'];
		} else {
			$d_date ="";
		}
		
		// new multicook code
		
		$cooks = array();
		
		foreach ($data['products'] as $product) {
			$cooks[$product['cook_id']] = $product['cook_id'];
		}
		
		if(isset($data['cook_id'])){
			$cook_id = $data['cook_id'];
		} else {
			$cook_id = -1337;
		}
		
		$cook_id = json_encode($cooks);
		
		// new multicook code end
		
		if(isset($data['tools'])){
			$tools = $data['tools'];
		} else {
			$tools = 1;
		}
		
		$this->db->query("INSERT INTO `" . DB_PREFIX . "order` SET invoice_prefix = '" . $this->db->escape($data['invoice_prefix']) . "', store_id = '" . (int)$data['store_id'] . "', store_name = '" . $this->db->escape($data['store_name']) . "', store_url = '" . $this->db->escape($data['store_url']) . "', customer_id = '" . (int)$data['customer_id'] . "', cook_id = '" . (int)$cook_id . "', customer_group_id = '" . (int)$data['customer_group_id'] . "', firstname = '" . $this->db->escape($data['firstname']) . "', lastname = '" . $this->db->escape($data['lastname']) . "', email = '" . $this->db->escape($data['email']) . "', telephone = '" . $this->db->escape($data['telephone']) . "', fax = '" . $this->db->escape($data['fax']) . "', payment_firstname = '" . $this->db->escape($data['payment_firstname']) . "', payment_lastname = '" . $this->db->escape($data['payment_lastname']) . "', payment_company = '" . $this->db->escape($data['payment_company']) . "', payment_company_id = '" . $this->db->escape($data['payment_company_id']) . "', payment_tax_id = '" . $this->db->escape($data['payment_tax_id']) . "', payment_address_1 = '" . $this->db->escape($data['payment_address_1']) . "', payment_address_2 = '" . $this->db->escape($data['payment_address_2']) . "', payment_city = '" . $this->db->escape($data['payment_city']) . "', payment_postcode = '" . $this->db->escape($data['payment_postcode']) . "', payment_country = '" . $this->db->escape($data['payment_country']) . "', payment_country_id = '" . (int)$data['payment_country_id'] . "', payment_zone = '" . $this->db->escape($data['payment_zone']) . "', payment_zone_id = '" . (int)$data['payment_zone_id'] . "', payment_address_format = '" . $this->db->escape($data['payment_address_format']) . "', payment_method = '" . $this->db->escape($data['payment_method']) . "', payment_code = '" . $this->db->escape($data['payment_code']) . "', shipping_firstname = '" . $this->db->escape($data['shipping_firstname']) . "', shipping_lastname = '" . $this->db->escape($data['shipping_lastname']) . "', shipping_company = '" . $this->db->escape($data['shipping_company']) . "', shipping_address_1 = '" . $this->db->escape($data['shipping_address_1']) . "', shipping_address_2 = '" . $this->db->escape($data['shipping_address_2']) . "', shipping_city = '" . $this->db->escape($data['shipping_city']) . "', shipping_postcode = '" . $this->db->escape($data['shipping_postcode']) . "', shipping_country = '" . $this->db->escape($data['shipping_country']) . "', delivery_time = '" . $this->db->escape($d_time) . "',  delivery_date = '" . $this->db->escape($d_date) . "', shipping_country_id = '" . (int)$data['shipping_country_id'] . "', shipping_zone = '" . $this->db->escape($data['shipping_zone']) . "', shipping_zone_id = '" . (int)$data['shipping_zone_id'] . "', shipping_address_format = '" . $this->db->escape($data['shipping_address_format']) . "', shipping_method = '" . $this->db->escape($data['shipping_method']) . "', shipping_code = '" . $this->db->escape($data['shipping_code']) . "', comment = '" . $this->db->escape($data['comment']) . "', total = '" . (float)$data['total'] . "', affiliate_id = '" . (int)$data['affiliate_id'] . "', commission = '" . (float)$data['commission'] . "', language_id = '" . (int)$data['language_id'] . "', currency_id = '" . (int)$data['currency_id'] . "', tools  = '" . (int)$tools . "', currency_code = '" . $this->db->escape($data['currency_code']) . "', currency_value = '" . (float)$data['currency_value'] . "', ip = '" . $this->db->escape($data['ip']) . "', forwarded_ip = '" .  $this->db->escape($data['forwarded_ip']) . "', user_agent = '" . $this->db->escape($data['user_agent']) . "', accept_language = '" . $this->db->escape($data['accept_language']) . "', date_added = NOW(), date_modified = NOW()");

		$order_id = $this->db->getLastId();
		
		foreach ($data['products'] as $product) {
			
			if(!empty($product['time'])){
				$time = $product['time'];
			} else {
				$time = "";
			}
			
			if(!empty($product['date'])){
				$date = $product['date'];
			} else {
				$date = "";
			}
			
			if(!empty($product['side_1'])){
				$sides = $product['side_1'];
			} else {
				$sides = "";
			}
			
			if(!empty($product['side_2'])){
				$sides .= ",";
				$sides .= $product['side_2'];
			}
			
			if(!empty($product['comment'])){
				$comment = $product['comment'];
			} else {
				$comment = "";
			}
			
			$this->db->query("INSERT INTO " . DB_PREFIX . "order_product SET order_id = '" . (int)$order_id . "', product_id = '" . (int)$product['product_id'] . "', cook_id = '" . (int)$product['cook_id'] . "', name = '" . $this->db->escape($product['name']) . "', model = '" . $this->db->escape($product['model']) . "', delivery_time = '" . $this->db->escape($time) . "',  delivery_date = '" . $this->db->escape($date) . "',  comment = '" . $this->db->escape($comment) . "',sides = '" . $this->db->escape($sides) . "', quantity = '" . (int)$product['quantity'] . "', price = '" . (float)$product['price'] . "', total = '" . (float)$product['total'] . "', tax = '" . (float)$product['tax'] . "', reward = '" . (int)$product['reward'] . "'");
			
			$order_product_id = $this->db->getLastId();
			
			foreach ($product['option'] as $option) {
				$this->db->query("INSERT INTO " . DB_PREFIX . "order_option SET order_id = '" . (int)$order_id . "', order_product_id = '" . (int)$order_product_id . "', product_option_id = '" . (int)$option['product_option_id'] . "', product_option_value_id = '" . (int)$option['product_option_value_id'] . "', name = '" . $this->db->escape($option['name']) . "', `value` = '" . $this->db->escape($option['value']) . "', `type` = '" . $this->db->escape($option['type']) . "'");
			}
				
			foreach ($product['download'] as $download) {
				$this->db->query("INSERT INTO " . DB_PREFIX . "order_download SET order_id = '" . (int)$order_id . "', order_product_id = '" . (int)$order_product_id . "', name = '" . $this->db->escape($download['name']) . "', filename = '" . $this->db->escape($download['filename']) . "', mask = '" . $this->db->escape($download['mask']) . "', remaining = '" . (int)($download['remaining'] * $product['quantity']) . "'");
			}	
		}
		
		foreach ($data['vouchers'] as $voucher) {
			$this->db->query("INSERT INTO " . DB_PREFIX . "order_voucher SET order_id = '" . (int)$order_id . "', description = '" . $this->db->escape($voucher['description']) . "', code = '" . $this->db->escape($voucher['code']) . "', from_name = '" . $this->db->escape($voucher['from_name']) . "', from_email = '" . $this->db->escape($voucher['from_email']) . "', to_name = '" . $this->db->escape($voucher['to_name']) . "', to_email = '" . $this->db->escape($voucher['to_email']) . "', voucher_theme_id = '" . (int)$voucher['voucher_theme_id'] . "', message = '" . $this->db->escape($voucher['message']) . "', amount = '" . (float)$voucher['amount'] . "'");
		}
			
		foreach ($data['totals'] as $total) {
			$this->db->query("INSERT INTO " . DB_PREFIX . "order_total SET order_id = '" . (int)$order_id . "', code = '" . $this->db->escape($total['code']) . "', title = '" . $this->db->escape($total['title']) . "', text = '" . $this->db->escape($total['text']) . "', `value` = '" . (float)$total['value'] . "', sort_order = '" . (int)$total['sort_order'] . "'");
		}	
		
		return $order_id;
	}

	public function getOrder($order_id) {
		$order_query = $this->db->query("SELECT *, (SELECT os.name FROM `" . DB_PREFIX . "order_status` os WHERE os.order_status_id = o.order_status_id AND os.language_id = o.language_id) AS order_status FROM `" . DB_PREFIX . "order` o WHERE o.order_id = '" . (int)$order_id . "'");
			
		if ($order_query->num_rows) {
			$country_query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "country` WHERE country_id = '" . (int)$order_query->row['payment_country_id'] . "'");
			
			if ($country_query->num_rows) {
				$payment_iso_code_2 = $country_query->row['iso_code_2'];
				$payment_iso_code_3 = $country_query->row['iso_code_3'];
			} else {
				$payment_iso_code_2 = '';
				$payment_iso_code_3 = '';				
			}
			
			$zone_query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "zone` WHERE zone_id = '" . (int)$order_query->row['payment_zone_id'] . "'");
			
			if ($zone_query->num_rows) {
				$payment_zone_code = $zone_query->row['code'];
			} else {
				$payment_zone_code = '';
			}			
			
			$country_query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "country` WHERE country_id = '" . (int)$order_query->row['shipping_country_id'] . "'");
			
			if ($country_query->num_rows) {
				$shipping_iso_code_2 = $country_query->row['iso_code_2'];
				$shipping_iso_code_3 = $country_query->row['iso_code_3'];
			} else {
				$shipping_iso_code_2 = '';
				$shipping_iso_code_3 = '';				
			}
			
			$zone_query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "zone` WHERE zone_id = '" . (int)$order_query->row['shipping_zone_id'] . "'");
			
			if ($zone_query->num_rows) {
				$shipping_zone_code = $zone_query->row['code'];
			} else {
				$shipping_zone_code = '';
			}
			
			$this->load->model('localisation/language');
			
			$language_info = $this->model_localisation_language->getLanguage($order_query->row['language_id']);
			
			if ($language_info) {
				$language_code = $language_info['code'];
				$language_filename = $language_info['filename'];
				$language_directory = $language_info['directory'];
			} else {
				$language_code = '';
				$language_filename = '';
				$language_directory = '';
			}
		 			
			return array(
				'order_id'                => $order_query->row['order_id'],
				'invoice_no'              => $order_query->row['invoice_no'],
				'invoice_prefix'          => $order_query->row['invoice_prefix'],
				'store_id'                => $order_query->row['store_id'],
				'store_name'              => $order_query->row['store_name'],
				'store_url'               => $order_query->row['store_url'],				
				'customer_id'             => $order_query->row['customer_id'],
				'firstname'               => $order_query->row['firstname'],
				'lastname'                => $order_query->row['lastname'],
				'telephone'               => $order_query->row['telephone'],
				'fax'                     => $order_query->row['fax'],
				'email'                   => $order_query->row['email'],
				'payment_firstname'       => $order_query->row['payment_firstname'],
				'payment_lastname'        => $order_query->row['payment_lastname'],				
				'payment_company'         => $order_query->row['payment_company'],
				'payment_company_id'      => $order_query->row['payment_company_id'],
				'payment_tax_id'          => $order_query->row['payment_tax_id'],
				'payment_address_1'       => $order_query->row['payment_address_1'],
				'payment_address_2'       => $order_query->row['payment_address_2'],
				'payment_postcode'        => $order_query->row['payment_postcode'],
				'payment_city'            => $order_query->row['payment_city'],
				'payment_zone_id'         => $order_query->row['payment_zone_id'],
				'payment_zone'            => $order_query->row['payment_zone'],
				'payment_zone_code'       => $payment_zone_code,
				'payment_country_id'      => $order_query->row['payment_country_id'],
				'payment_country'         => $order_query->row['payment_country'],	
				'payment_iso_code_2'      => $payment_iso_code_2,
				'payment_iso_code_3'      => $payment_iso_code_3,
				'payment_address_format'  => $order_query->row['payment_address_format'],
				'payment_method'          => $order_query->row['payment_method'],
				'payment_code'            => $order_query->row['payment_code'],
				'shipping_firstname'      => $order_query->row['shipping_firstname'],
				'shipping_lastname'       => $order_query->row['shipping_lastname'],				
				'shipping_company'        => $order_query->row['shipping_company'],
				'shipping_address_1'      => $order_query->row['shipping_address_1'],
				'shipping_address_2'      => $order_query->row['shipping_address_2'],
				'shipping_postcode'       => $order_query->row['shipping_postcode'],
				'shipping_city'           => $order_query->row['shipping_city'],
				'shipping_zone_id'        => $order_query->row['shipping_zone_id'],
				'shipping_zone'           => $order_query->row['shipping_zone'],
				'shipping_zone_code'      => $shipping_zone_code,
				'shipping_country_id'     => $order_query->row['shipping_country_id'],
				'shipping_country'        => $order_query->row['shipping_country'],	
				'shipping_iso_code_2'     => $shipping_iso_code_2,
				'shipping_iso_code_3'     => $shipping_iso_code_3,
				'shipping_address_format' => $order_query->row['shipping_address_format'],
				'shipping_method'         => $order_query->row['shipping_method'],
				'shipping_code'           => $order_query->row['shipping_code'],
				'comment'                 => $order_query->row['comment'],
				'total'                   => $order_query->row['total'],
				'order_status_id'         => $order_query->row['order_status_id'],
				'order_status'            => $order_query->row['order_status'],
				'language_id'             => $order_query->row['language_id'],
				'language_code'           => $language_code,
				'language_filename'       => $language_filename,
				'language_directory'      => $language_directory,
				'currency_id'             => $order_query->row['currency_id'],
				'currency_code'           => $order_query->row['currency_code'],
				'currency_value'          => $order_query->row['currency_value'],
				'ip'                      => $order_query->row['ip'],
				'forwarded_ip'            => $order_query->row['forwarded_ip'], 
				'user_agent'              => $order_query->row['user_agent'],	
				'accept_language'         => $order_query->row['accept_language'],				
				'date_modified'           => $order_query->row['date_modified'],
				'date_added'              => $order_query->row['date_added'],
				// added by homeals
				'tools'                   => $order_query->row['tools'],
				'parent_order_id'         => $order_query->row['parent_order_id'],
				'cook_id'              	  => $order_query->row['cook_id'],
				'delivery_date'           => $order_query->row['delivery_date'],
				'delivery_time'           => $order_query->row['delivery_time'],
				'driver'		  => $order_query->row['driver_pickup'],
				'driver_deliver'	  => $order_query->row['driver_deliver'],
			);
		} else {
			return false;	
		}
	}
	
	// for multicook
	// we add order and confirm it imidiatly
	public function addAndConfirm($parent_order_id, $order_status_id, $cook_id) {
		$data = $this->getOrder($parent_order_id);
		
		if(isset($data['delivery_time'])){
			$d_time = $data['delivery_time'];
		} else {
			$d_time ="";
		}
		
		if(isset($data['delivery_date'])){
			$d_date = $data['delivery_date'];
		} else {
			$d_date ="";
		}
		
		if(isset($data['customer_group_id'])){
			$customer_group_id = $data['customer_group_id'];
		} else {
			$customer_group_id = 1;
		}
		
		if(isset($data['affiliate_id'])){
			$affiliate_id = $data['affiliate_id'];
		} else {
			$affiliate_id = 0;
		}
		 
		
		// we add the order with parent order id and cook id and order status
		$this->db->query("INSERT INTO `" . DB_PREFIX . "order` SET invoice_prefix = '" . $this->db->escape($data['invoice_prefix']) . "', store_id = '" . (int)$data['store_id'] . "', store_name = '" . $this->db->escape($data['store_name']) . "', store_url = '" . $this->db->escape($data['store_url']) . "', customer_id = '" . (int)$data['customer_id'] . "', parent_order_id = '" . (int)$parent_order_id . "', cook_id = '" . (int)$cook_id . "', customer_group_id = '" . (int)$customer_group_id . "', firstname = '" . $this->db->escape($data['firstname']) . "', lastname = '" . $this->db->escape($data['lastname']) . "', email = '" . $this->db->escape($data['email']) . "', telephone = '" . $this->db->escape($data['telephone']) . "', fax = '" . $this->db->escape($data['fax']) . "', payment_firstname = '" . $this->db->escape($data['payment_firstname']) . "', payment_lastname = '" . $this->db->escape($data['payment_lastname']) . "', payment_company = '" . $this->db->escape($data['payment_company']) . "', payment_company_id = '" . $this->db->escape($data['payment_company_id']) . "', payment_tax_id = '" . $this->db->escape($data['payment_tax_id']) . "', payment_address_1 = '" . $this->db->escape($data['payment_address_1']) . "', payment_address_2 = '" . $this->db->escape($data['payment_address_2']) . "', payment_city = '" . $this->db->escape($data['payment_city']) . "', payment_postcode = '" . $this->db->escape($data['payment_postcode']) . "', payment_country = '" . $this->db->escape($data['payment_country']) . "', payment_country_id = '" . (int)$data['payment_country_id'] . "', payment_zone = '" . $this->db->escape($data['payment_zone']) . "', payment_zone_id = '" . (int)$data['payment_zone_id'] . "', payment_address_format = '" . $this->db->escape($data['payment_address_format']) . "', payment_method = '" . $this->db->escape($data['payment_method']) . "', payment_code = '" . $this->db->escape($data['payment_code']) . "', shipping_firstname = '" . $this->db->escape($data['shipping_firstname']) . "', shipping_lastname = '" . $this->db->escape($data['shipping_lastname']) . "', shipping_company = '" . $this->db->escape($data['shipping_company']) . "', shipping_address_1 = '" . $this->db->escape($data['shipping_address_1']) . "', shipping_address_2 = '" . $this->db->escape($data['shipping_address_2']) . "', shipping_city = '" . $this->db->escape($data['shipping_city']) . "', shipping_postcode = '" . $this->db->escape($data['shipping_postcode']) . "', shipping_country = '" . $this->db->escape($data['shipping_country']) . "', delivery_time = '" . $this->db->escape($d_time) . "',  delivery_date = '" . $this->db->escape($d_date) . "', shipping_country_id = '" . (int)$data['shipping_country_id'] . "', shipping_zone = '" . $this->db->escape($data['shipping_zone']) . "', shipping_zone_id = '" . (int)$data['shipping_zone_id'] . "', shipping_address_format = '" . $this->db->escape($data['shipping_address_format']) . "', shipping_method = '" . $this->db->escape($data['shipping_method']) . "', shipping_code = '" . $this->db->escape($data['shipping_code']) . "', comment = '" . $this->db->escape($data['comment']) . "', total = '" . (float)0 . "', affiliate_id = '" . (int)$affiliate_id . "', commission = '" . (float)$data['commission'] . "', language_id = '" . (int)$data['language_id'] . "', tools  = '" . (int)$data['tools'] . "', currency_id = '" . (int)$data['currency_id'] . "', currency_code = '" . $this->db->escape($data['currency_code']) . "', currency_value = '" . (float)$data['currency_value'] . "', ip = '" . $this->db->escape($data['ip']) . "', forwarded_ip = '" .  $this->db->escape($data['forwarded_ip']) . "', user_agent = '" . $this->db->escape($data['user_agent']) . "',order_status_id = '" . (int)$order_status_id . "', accept_language = '" . $this->db->escape($data['accept_language']) . "', date_added = NOW(), date_modified = NOW()");
		
		$order_id = $this->db->getLastId();
		$data['cook_id'] = $cook_id;
		// sending mail to cook and to user
		$this->sendMailToCook($order_id,$parent_order_id);
		
	}
	
	public function confirm($order_id, $order_status_id, $comment = '', $notify = false) {
		$order_info = $this->getOrder($order_id);
		
		///////////////////////////////////////////////////////////////////////
		// multicook start! we will make a new order for every cook
		if($order_info && !$order_info['order_status_id']) {
			
			$order_product_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "order_product WHERE order_id = '" . (int)$order_id . "'");
			$cooks_array = array();
			//getting all cooks into one array
			foreach($order_product_query->rows as $product){
				$cooks_array[$product['cook_id']] = $product['cook_id'];
			}
			
			$cooks_array = json_decode($order_info['cook_id']);
			
			// for each cook we create a new order and confirm it with the same order status
			// we add the order with parent id and save it
			foreach($cooks_array as $cook_id){
				
				// the cook id $cook_id
				// the parent id $order_id
				// the order status $order_status_id
				
				// add order and confirm
				$this->addAndConfirm($order_id, $order_status_id, $cook_id);
			}
			
			// Multi Cook Status is paied multi
			if(true){
				$order_status_id = 22;//$this->config->get('config_multi_order_status_id');
			}
			
		}
		//  multicook end
		///////////////////////////////////////////////////////////////////////
		
		if ($order_info && !$order_info['order_status_id']) {
			
			$order_info['payment_address_1'] = str_replace('#', '',$order_info['payment_address_1']);
			
			// Fraud Detection
			if ($this->config->get('config_fraud_detection')) {
				$this->load->model('checkout/fraud');
				
				$risk_score = $this->model_checkout_fraud->getFraudScore($order_info);
				
				if ($risk_score > $this->config->get('config_fraud_score')) {
					$order_status_id = $this->config->get('config_fraud_status_id');
				}
			}

			// Ban IP
			$status = false;
			
			$this->load->model('account/customer');
			
			if ($order_info['customer_id']) {
				$results = $this->model_account_customer->getIps($order_info['customer_id']);
				
				foreach ($results as $result) {
					if ($this->model_account_customer->isBanIp($result['ip'])) {
						$status = true;
						
						break;
					}
				}
			} else {
				$status = $this->model_account_customer->isBanIp($order_info['ip']);
			}
			
			if ($status) {
				$order_status_id = $this->config->get('config_order_status_id');
			}		
				
			$this->db->query("UPDATE `" . DB_PREFIX . "order` SET order_status_id = '" . (int)$order_status_id . "', date_modified = NOW() WHERE order_id = '" . (int)$order_id . "'");

			$this->db->query("INSERT INTO " . DB_PREFIX . "order_history SET order_id = '" . (int)$order_id . "', order_status_id = '" . (int)$order_status_id . "', notify = '1', comment = '" . $this->db->escape(($comment && $notify) ? $comment : '') . "', date_added = NOW()");
			
			$order_product_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "order_product WHERE order_id = '" . (int)$order_id . "'");
			
			
			foreach ($order_product_query->rows as $order_product) {
				// subtracting meal quantity, need less in our case
				/*$this->db->query("UPDATE " . DB_PREFIX . "product SET quantity = (quantity - " . (int)$order_product['quantity'] . ") WHERE product_id = '" . (int)$order_product['product_id'] . "' AND subtract = '1'");*/
				
				$order_option_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "order_option WHERE order_id = '" . (int)$order_id . "' AND order_product_id = '" . (int)$order_product['order_product_id'] . "'");
				
				foreach ($order_option_query->rows as $option) {
					/*
					$order_product_option_quantity = 1;
					foreach ($this->session->data['cart'] as $key => $quantity) {
						$productt = explode(':', $key);
						if(isset($productt[2])) {
							$optionsQuantities = unserialize(base64_decode($productt[2]));
							if (array_key_exists((int)$option['product_option_value_id'], $optionsQuantities)) {
								$order_product_option_quantity = $optionsQuantities[(int)$option['product_option_value_id']][0];
							}
						} 
					}
					$this->db->query("UPDATE " . DB_PREFIX . "product_option_value SET quantity = (quantity - " . (int)$order_product['quantity']*$order_product_option_quantity . ") WHERE product_option_value_id = '" . (int)$option['product_option_value_id'] . "' AND subtract = '1'");
				*/
				// Modified for option quantity ==========================================================================================
					$order_product_option_quantity = 1;
					foreach ($this->session->data['cart'] as $key => $quantity) {
						$productt = explode(':', $key);
						if(isset($productt[2])) {
							$optionsQuantities = unserialize(base64_decode($productt[2]));
							if (array_key_exists((int)$option['product_option_value_id'], $optionsQuantities)) {
								$order_product_option_quantity = $optionsQuantities[(int)$option['product_option_value_id']][0];
							}
						} 
					}
					$this->db->query("UPDATE " . DB_PREFIX . "product_option_value SET quantity = (quantity - " . (int)$order_product['quantity']*$order_product_option_quantity . ") WHERE product_option_value_id = '" . (int)$option['product_option_value_id'] . "' AND subtract = '1'");
//========================================================================================================================
				}
				
			}
			
			$this->cache->delete('product');
			
			// Downloads
			$order_download_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "order_download WHERE order_id = '" . (int)$order_id . "'");
			
			// Gift Voucher
			$this->load->model('checkout/voucher');
			
			$order_voucher_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "order_voucher WHERE order_id = '" . (int)$order_id . "'");
			
			foreach ($order_voucher_query->rows as $order_voucher) {
				$voucher_id = $this->model_checkout_voucher->addVoucher($order_id, $order_voucher);
				
				$this->db->query("UPDATE " . DB_PREFIX . "order_voucher SET voucher_id = '" . (int)$voucher_id . "' WHERE order_voucher_id = '" . (int)$order_voucher['order_voucher_id'] . "'");
			}			
			
			// Send out any gift voucher mails
			if ($this->config->get('config_complete_status_id') == $order_status_id) {
				$this->model_checkout_voucher->confirm($order_id);
			}
					
			// Order Totals			
			$order_total_query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "order_total` WHERE order_id = '" . (int)$order_id . "' ORDER BY sort_order ASC");
			
			foreach ($order_total_query->rows as $order_total) {
				$this->load->model('total/' . $order_total['code']);
				
				if (method_exists($this->{'model_total_' . $order_total['code']}, 'confirm')) {
					$this->{'model_total_' . $order_total['code']}->confirm($order_info, $order_total);
				}
				
			}
			
			/****************************
			 *
			 *	HOMEALS cardcom recipt code start
			 *
			 **************************/
			// vars for recipt
			$CustName = $order_info['firstname']." ".$order_info['lastname'];
			$CustEmail = $order_info['email'];
			$CustAddresLine1 = $order_info['payment_address_1'];
			$CustAddresLine2 = $order_info['payment_address_2'];
			$CustCity = $order_info['payment_city'];
			
			$tot = 0;
			
			$pros_quantity = 0;
			$main_total_store = 0;
			$total_paid = 0;
			$shiptot = 0;
			$sub_total = 0;
			$cook_total = 0;
			
			$final_total = 0;
			
			// Order Totals
			foreach ($order_total_query->rows as $order_total) {				
				if ($order_total['code'] == "total"){
					$total_paid = $order_total['value'];
				}
				
				if ($order_total['code'] == "shipping"){
					$shiptot = $order_total['value'];
				}
				
				if ($order_total['code'] == "sub_total"){
					$sub_total = $order_total['value'];
				}
			}
			
			foreach ($order_product_query->rows as $product) {
				
				$pros_quantity += $product['quantity'];
				$main_total_store += $this->config->get('config_tax') ? ($product['tax']*$product['quantity']) : 0;
				
			}
			
			//$cook_total = $sub_total - $main_total_store - $shiptot;
			//$final_total = $total_paid - $cook_total;
			//$calculated_tax_value = $final_total / $pros_quantity;
			$cook_total = $sub_total - $main_total_store;
			$final_total = $total_paid - $cook_total - $shiptot;
			$calculated_tax_value = ( $final_total ) / $pros_quantity;
			
			// debug 
			//print_r("cook total = sub total - tax total<br>");
			//print_r("$cook_total = $sub_total - $main_total_store - $shiptot<br><br>");
			//
			//print_r("final total = total paid - cook total - shipping total<br>");
			//print_r("$final_total = $total_paid - $cook_total<br><br>");
			//
			//print_r("calculated tax value = (total paid - cook total) / pros quantity<br>");
			//print_r("$calculated_tax_value = ($total_paid - $cook_total) / $pros_quantity<br><br>");
			//
			//die;
			// debug 
			
			$InvoiceLinesDescription = "עמלת סועד";
			$InvoiceLinesPrice = $calculated_tax_value;
			$InvoiceLinesQuantity = $pros_quantity;
			
			$InvoiceLines1Description = "דמי משלוח להזמנה";
			$InvoiceLines1Price = $shiptot;
			$InvoiceLines1Quantity = 1;
			
			$TranDate = date("d/m/Y");
			$Asmacta = $order_id ;
			$CustomPaySum = $final_total + $shiptot; //$main_total_store + $InvoiceLines1Price;
			
			$recipt_data = "codepage=65001&terminalnumber=20977&username=homeals&InvoiceType=1";
			
			$recipt_data .= "&InvoiceHead.CustName=".urlencode($CustName)."&InvoiceHead.SendByEmail=false&InvoiceHead.Language=he";
			$recipt_data .= "&InvoiceHead.Email=".urlencode($CustEmail)."&InvoiceHead.CoinID=1";
			$recipt_data .= "&InvoiceHead.CustAddresLine1=".urlencode($CustAddresLine1)."&InvoiceHead.CustAddresLine2=".urlencode($CustAddresLine2)."&InvoiceHead.CustCity=".urlencode($CustCity);
			
			$recipt_data .= "&InvoiceLines.Description=".urlencode($InvoiceLinesDescription)."&InvoiceLines.Price=".urlencode($InvoiceLinesPrice)."&InvoiceLines.Quantity=".urlencode($InvoiceLinesQuantity)."&InvoiceLines.IsPriceIncludeVAT=true";
			
			$recipt_data .= "&InvoiceLines1.Description=".urlencode($InvoiceLines1Description)."&InvoiceLines1.Price=".urlencode($InvoiceLines1Price)."&InvoiceLines1.Quantity=".urlencode($InvoiceLines1Quantity)."&InvoiceLines1.IsPriceIncludeVAT=true";
			
			$recipt_data .= "&CustomPay.TransactionID=32&CustomPay.TranDate=".urlencode($TranDate)."&CustomPay.Asmacta=".urlencode($Asmacta)."&CustomPay.Sum=".urlencode($CustomPaySum);
			
			// if final total if bigger then 0 we issue an invoice
			if ($final_total > 0){
				$ch = curl_init();
				
				curl_setopt($ch, CURLOPT_URL,"https://secure.cardcom.co.il/Interface/CreateInvoice.aspx");
				curl_setopt($ch, CURLOPT_POST, 1);
				curl_setopt($ch, CURLOPT_POSTFIELDS,$recipt_data);
				curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/x-www-form-urlencoded'));
				
				// receive server response ...
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
				
				$server_output = curl_exec($ch);
				
				curl_close($ch);
			}
			/****************************
			 *
			 *	HOMEALS cardcom recipt code end
			 *
			 **************************/
			
			// notifing customer
			$this->sendMailToCustomer($order_id);
			
			
		}
	}
	
	
	public function sendReviewMail($order_id){
		$order_info = $this->getOrder($order_id);
		$order_product_sql = "SELECT * FROM " . DB_PREFIX . "order_product WHERE order_id = '" . (int)$order_id . "'";
		
		$order_product_query = $this->db->query($order_product_sql);
		
		// Send out order confirmation mail
		$language = new Language($order_info['language_directory']);
		$language->load($order_info['language_filename']);
		$language->load('mail/order');
		
		$this->load->model('account/customer'); // needed for cook
		
		//$cook_info = $this->model_account_customer->getCustomer($order_info['cook_id']);
		
		// homeals mail greeting
		setlocale(LC_ALL, 'he_IL.UTF-8');
		
		//$cook_name = html_entity_decode($cook_info['firstname']." ".$cook_info['lastname'], ENT_QUOTES, 'UTF-8');
		$d_date = strftime("%A, %e ב%B",strtotime($order_info['delivery_date']));
		$delivery_date = html_entity_decode($d_date, ENT_QUOTES, 'UTF-8');
		$d_time_int = strftime("%k",strtotime($order_info['delivery_time']));
		$d_time_format = ( $d_time_int ).":30 - ".( $d_time_int -1 ).":30";
		$delivery_time = html_entity_decode($d_time_format, ENT_QUOTES, 'UTF-8');
		
		/****************************
		*
		*	HOMEALS review order mail
		*
		**************************/
	       
	        $cook_name = "";
		$pros_quantity = 0;
		
		$cooks = json_decode($order_info['cook_id']);
		
		foreach ($cooks as $cook_id) {
			$cook_info = $this->model_account_customer->getCustomer($cook_id);
			$cook_name .= trim($cook_info['firstname']).", ";
		}
		
		foreach ($order_product_query->rows as $product) {
			$pros_quantity += $product['quantity'];
		}
	       
	       $subject = sprintf($language->get('text_review_subject'), $cook_name, $delivery_date);
	       
	       // HTML Mail
	       $template = new Template();
	       
	       $template->data['title'] = sprintf($language->get('text_review_subject'), html_entity_decode($order_info['store_name'], ENT_QUOTES, 'UTF-8'), $order_id);
	       $template->data['logo'] = $this->config->get('config_url') . 'image/' . $this->config->get('config_logo');		
	       $template->data['store_name'] = $order_info['store_name'];
	       $template->data['store_url'] = $order_info['store_url'];
	       $template->data['text_review_greeting'] = sprintf($language->get('text_review_greeting'), $order_info['firstname']);
	       
	       $template->data['text_review_text_prolog'] = sprintf($language->get('text_review_text_prolog'), $cook_name, $delivery_date);
	       $template->data['text_review_text_epilog'] = sprintf($language->get('text_review_text_epilog'), $cook_info['firstname']);
	       
	       $template->data['rate_order_url'] = $order_info['store_url'] . 'index.php?route=account/order/revieworder&order_id=' . $order_id;
	       $template->data['text_rate_order'] = $language->get('text_rate_order');
	       
	       // fotter
	       $template->data['text_follow_us'] = $language->get('text_follow_us');
	       $template->data['text_homeals_help'] = sprintf($language->get('text_homeals_help'),"03-3741886",$order_info['email']);
	       
	       if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/mail/review_order.tpl')) {
		       $html = $template->fetch($this->config->get('config_template') . '/template/mail/review_order.tpl');
	       } else {
		       $html = $template->fetch('default/template/mail/review_order.tpl');
	       }
	       
	       $mail = new Mail(); 
	       $mail->protocol = $this->config->get('config_mail_protocol');
	       $mail->parameter = $this->config->get('config_mail_parameter');
	       $mail->hostname = $this->config->get('config_smtp_host');
	       $mail->username = $this->config->get('config_smtp_username');
	       $mail->password = $this->config->get('config_smtp_password');
	       $mail->port = $this->config->get('config_smtp_port');
	       $mail->timeout = $this->config->get('config_smtp_timeout');			
	       $mail->setTo($order_info['email']);
	       $mail->setFrom($this->config->get('config_email'));
	       $mail->setSender($order_info['store_name']);
	       $mail->setSubject(html_entity_decode($subject, ENT_QUOTES, 'UTF-8'));
	       $mail->setHtml($html);
	       $mail->send(); //enable at the end
	       
	       // homeals review mail end
		
	}
	
	public function update($order_id, $order_status_id, $comment = '', $notify = false) {
		$order_info = $this->getOrder($order_id);
		
		if($order_info['parent_order_id']){
			$parent_order_id = (int)$order_info['parent_order_id'];
		} else {
			$parent_order_id = 0;
		}
		
		if ($order_info && $order_info['order_status_id']) {
			$order_info['payment_address_1'] = str_replace('#', '',$order_info['payment_address_1']);
			// Fraud Detection
			if ($this->config->get('config_fraud_detection')) {
				$this->load->model('checkout/fraud');
				
				$risk_score = $this->model_checkout_fraud->getFraudScore($order_info);
				
				if ($risk_score > $this->config->get('config_fraud_score')) {
					$order_status_id = $this->config->get('config_fraud_status_id');
				}
			}
			
			// Ban IP
			$status = false;
			
			$this->load->model('account/customer');
			
			if ($order_info['customer_id']) {
				
				$results = $this->model_account_customer->getIps($order_info['customer_id']);
				
				foreach ($results as $result) {
					if ($this->model_account_customer->isBanIp($result['ip'])) {
						$status = true;
						
						break;
					}
				}
			} else {
				$status = $this->model_account_customer->isBanIp($order_info['ip']);
			}
			
			if ($status) {
				$order_status_id = $this->config->get('config_order_status_id');
			}		
						
			$this->db->query("UPDATE `" . DB_PREFIX . "order` SET order_status_id = '" . (int)$order_status_id . "', date_modified = NOW() WHERE order_id = '" . (int)$order_id . "'");
			
			
			$this->db->query("INSERT INTO " . DB_PREFIX . "order_history SET order_id = '" . (int)$order_id . "', order_status_id = '" . (int)$order_status_id . "', notify = '" . (int)$notify . "', comment = '" . $this->db->escape($comment) . "', date_added = NOW()");
	
			// Send out any gift voucher mails
			if ($this->config->get('config_complete_status_id') == $order_status_id) {
				
				// Order Totals			
				if($parent_order_id) {
					$order_total_query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "order_total` WHERE order_id = '" . (int)$parent_order_id . "' ORDER BY sort_order ASC");	
				} else {
					$order_total_query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "order_total` WHERE order_id = '" . (int)$order_id . "' ORDER BY sort_order ASC");	
				}
				
				foreach ($order_total_query->rows as $order_total) {
					$this->load->model('total/' . $order_total['code']);
					
					if (method_exists($this->{'model_total_' . $order_total['code']}, 'confirm')) {
						$this->{'model_total_' . $order_total['code']}->confirm($order_info, $order_total);
					}
				}
				
				// Send out order confirmation mail menny
				$language = new Language($order_info['language_directory']);
				$language->load($order_info['language_filename']);
				$language->load('mail/order');
				
				$order_status_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "order_status WHERE order_status_id = '" . (int)$order_status_id . "' AND language_id = '" . (int)$order_info['language_id'] . "'");
			
			if ($order_status_query->num_rows) {
				$order_status = $order_status_query->row['name'];
			} else {
				$order_status = '';
			}
				
			$this->load->model('checkout/voucher');
			
			$this->model_checkout_voucher->confirm($order_id);
			
			if($this->allAproved($parent_order_id)){
				$this->sendMailToConfirm($parent_order_id);
			}
			
			 /***********************************************************
			 *	FOR BUYER
			 *	HTML Mail	HTML Mail	HTML Mail
			 *		HTML Mail	HTML Mail	HTML Mail
			 *			HTML Mail	HTML Mail	HTML Mail
			 *				HTML Mail	HTML Mail	HTML Mail
			 *
			 ***********************************************************/
			 /*
			$this->load->model('account/customer'); // needed for cook
			
			$cook_info = $this->model_account_customer->getCustomer($order_info['cook_id']);
			
			$this->load->model('account/address'); // needed for cook address
			
			$cook_address = $this->model_account_address->getAddressesById((int)$order_info['cook_id']);
			$cook_address = reset($cook_address);
			
			$cook_pm_link = $this->url->link('seller/catalog-seller/profile', '&seller_id='.$order_info['cook_id'].'&openpm', 'SSL');
			
			// HTML Mail
			$template = new Template();
			
			$template->data['title'] = sprintf($language->get('text_new_subject'), html_entity_decode($order_info['store_name'], ENT_QUOTES, 'UTF-8'), $order_id);
			
			// homeals mail greeting
			setlocale(LC_ALL, 'he_IL.UTF-8');
			
			$cook_name = html_entity_decode($cook_info['firstname'], ENT_QUOTES, 'UTF-8');
			$address_1 = html_entity_decode($order_info['payment_address_1'].", ".$order_info['payment_city'], ENT_QUOTES, 'UTF-8');
			$d_date = strftime("%A, %e ב%B",strtotime($order_info['delivery_date']));
			$delivery_date = html_entity_decode($d_date, ENT_QUOTES, 'UTF-8');
			$d_time_int = strftime("%k",strtotime($order_info['delivery_time']));
			$d_time_format = ( $d_time_int ).":30 - ".( $d_time_int -1 ).":30";
			$delivery_time = html_entity_decode($d_time_format, ENT_QUOTES, 'UTF-8');
			
			$subject = sprintf($language->get('text_new_subject'), $cook_name, $delivery_date);
			
			$template->data['cook_info'] = $order_info['cook_id'];
			
			$template->data['text_greeting'] = sprintf($language->get('text_homeals_greeting'),$order_info['firstname']);
			$template->data['text_order_status'] = $language->get('text_order_status');
			$template->data['order_status'] = $order_status;
			$template->data['text_order_id'] = $language->get('text_order_id');
			$template->data['order_id'] = $order_id;
			
			$template->data['text_order_aproved'] = sprintf($language->get('text_order_aproved'),$cook_name,$address_1,$delivery_date,$delivery_time);
			$template->data['text_order_thank_you'] = $language->get('text_order_thank_you');
			
			//cook details
			$template->data['text_cook_details'] = $language->get('text_cook_details');
			$template->data['cook_name'] = sprintf("%s %s",$cook_info['firstname'],$cook_info['lastname']);
			$template->data['cook_city'] = $cook_address['city'];
			$template->data['cook_phone'] = $cook_info['telephone'];
			$template->data['cook_email'] = $cook_info['email'];
			
			$template->data['cook_pm'] = "צור קשר עם הבשלן";
			$template->data['cook_pm_link'] = $cook_pm_link;
			
			//delivery details
			$template->data['text_delivery_details'] = $language->get('text_delivery_details');
			$template->data['text_order_when'] = $language->get('text_order_when');
			$template->data['order_when'] = sprintf($language->get('order_when'),$delivery_date,$delivery_time);
			$template->data['text_order_address'] = $language->get('text_order_address');
			$address_format = $order_info['payment_address_1'].", ".$order_info['payment_city'].", ".$order_info['payment_address_2'];
			$template->data['order_address'] = html_entity_decode($address_format, ENT_QUOTES, 'UTF-8');
			
			$template->data['text_order_compony'] = $language->get('text_order_compony');
			$template->data['order_compony'] = $order_info['payment_company'];
			
			$template->data['text_order_contact'] = $language->get('text_order_contact');
			$template->data['order_contact'] = sprintf($language->get('order_contact'),$order_info['payment_firstname'],$order_info['payment_postcode']);
			$template->data['text_order_comment'] = $language->get('text_order_comment');
			$template->data['order_comment'] = $order_info['comment'];
			
			
			// meal(product) details
			$template->data['text_meal_details'] = $language->get('text_meal_details');
			$template->data['text_per_price'] = $language->get('text_per_price');
			
			// product need to have picture, name + side names, comment, quantity, price
			$template->data['text_meal_coment'] = $language->get('text_meal_coment');
			$template->data['text_quantity'] = $language->get('text_quantity');
			
			// order summary
			$template->data['text_order_summary'] = $language->get('text_order_summary');
			$template->data['text_meals_total'] = $language->get('text_meals_total');
			$template->data['text_delivery_cost'] = $language->get('text_delivery_cost');
			$template->data['text_total'] = $language->get('text_new_total');
			
			// fotter
			$template->data['text_follow_us'] = $language->get('text_follow_us');
			$template->data['text_homeals_help'] = sprintf($language->get('text_homeals_help'),"03-3741886",$order_info['email']);
			
			// end of homeals mail
			
			$template->data['text_link'] = $language->get('text_new_link');
			$template->data['text_download'] = $language->get('text_new_download');
			$template->data['text_order_detail'] = $language->get('text_new_order_detail');
			$template->data['text_instruction'] = $language->get('text_new_instruction');
			
			$template->data['text_date_added'] = $language->get('text_new_date_added');
			$template->data['text_payment_method'] = $language->get('text_new_payment_method');	
			$template->data['text_shipping_method'] = $language->get('text_new_shipping_method');
			$template->data['text_email'] = $language->get('text_new_email');
			$template->data['text_telephone'] = $language->get('text_new_telephone');
			$template->data['text_ip'] = $language->get('text_new_ip');
			$template->data['text_payment_address'] = $language->get('text_new_payment_address');
			$template->data['text_shipping_address'] = $language->get('text_new_shipping_address');
			$template->data['text_product'] = $language->get('text_new_product');
			$template->data['text_model'] = $language->get('text_new_model');
			$template->data['text_price'] = $language->get('text_new_price');

			$template->data['text_footer'] = $language->get('text_new_footer');
			$template->data['text_powered'] = $language->get('text_new_powered');
			
			$template->data['logo'] = $this->config->get('config_url') . 'image/' . $this->config->get('config_logo');		
			$template->data['store_name'] = $order_info['store_name'];
			$template->data['store_url'] = $order_info['store_url'];
			$template->data['customer_id'] = $order_info['customer_id'];
			$template->data['link'] = $order_info['store_url'] . 'index.php?route=account/order/info&order_id=' . $order_id;
			
			$template->data['date_added'] = date($language->get('date_format_short'), strtotime($order_info['date_added']));    	
			$template->data['payment_method'] = $order_info['payment_method'];
			$template->data['shipping_method'] = $order_info['shipping_method'];
			$template->data['email'] = $order_info['email'];
			$template->data['telephone'] = $order_info['payment_postcode'];
			$template->data['ip'] = $order_info['ip'];
			
			if ($comment && $notify) {
				$template->data['comment'] = nl2br($comment);
			} else {
				$template->data['comment'] = '';
			}
						
			if ($order_info['payment_address_format']) {
				$format = $order_info['payment_address_format'];
			} else {
				$format = '{firstname} {lastname}' . "\n" . '{company}' . "\n" . '{address_1}' . "\n" . '{address_2}' . "\n" . '{city} {postcode}' . "\n" . '{zone}' . "\n" . '{country}';
			}
			
			$find = array(
				'{firstname}',
				'{lastname}',
				'{company}',
				'{address_1}',
				'{address_2}',
				'{city}',
				'{postcode}',
				'{zone}',
				'{zone_code}',
				'{country}'
			);
		
			$replace = array(
				'firstname' => $order_info['payment_firstname'],
				'lastname'  => $order_info['payment_lastname'],
				'company'   => $order_info['payment_company'],
				'address_1' => $order_info['payment_address_1'],
				'address_2' => $order_info['payment_address_2'],
				'city'      => $order_info['payment_city'],
				'postcode'  => $order_info['payment_postcode'],
				'zone'      => $order_info['payment_zone'],
				'zone_code' => $order_info['payment_zone_code'],
				'country'   => $order_info['payment_country']  
			);
			
		
			$template->data['payment_address'] = str_replace(array("\r\n", "\r", "\n"), '<br />', preg_replace(array("/\s\s+/", "/\r\r+/", "/\n\n+/"), '<br />', trim(str_replace($find, $replace, $format))));						
									
			if ($order_info['shipping_address_format']) {
				$format = $order_info['shipping_address_format'];
			} else {
				$format = '{firstname} {lastname}' . "\n" . '{company}' . "\n" . '{address_1}' . "\n" . '{address_2}' . "\n" . '{city} {postcode}' . "\n" . '{zone}' . "\n" . '{country}';
			}
			
			$find = array(
				'{firstname}',
				'{lastname}',
				'{company}',
				'{address_1}',
				'{address_2}',
				'{city}',
				'{postcode}',
				'{zone}',
				'{zone_code}',
				'{country}'
			);
		
			$replace = array(
				'firstname' => $order_info['shipping_firstname'],
				'lastname'  => $order_info['shipping_lastname'],
				'company'   => $order_info['shipping_company'],
				'address_1' => $order_info['shipping_address_1'],
				'address_2' => $order_info['shipping_address_2'],
				'city'      => $order_info['shipping_city'],
				'postcode'  => $order_info['shipping_postcode'],
				'zone'      => $order_info['shipping_zone'],
				'zone_code' => $order_info['shipping_zone_code'],
				'country'   => $order_info['shipping_country']  
			);
		
			$template->data['shipping_address'] = str_replace(array("\r\n", "\r", "\n"), '<br />', preg_replace(array("/\s\s+/", "/\r\r+/", "/\n\n+/"), '<br />', trim(str_replace($find, $replace, $format))));
			
			// Products
			$template->data['products'] = array();
			
			if($parent_order_id) {
				$order_product_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "order_product WHERE order_id = '" . (int)$parent_order_id . "' AND cook_id ='".$order_info['cook_id']."'");
			}
			
			foreach ($order_product_query->rows as $product) {
				$option_data = array();
				
				$order_option_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "order_option WHERE order_id = '" . (int)$order_id . "' AND order_product_id = '" . (int)$product['order_product_id'] . "'");
				
				foreach ($order_option_query->rows as $option) {
					if ($option['type'] != 'file') {
						$value = $option['value'];
					} else {
						$value = utf8_substr($option['value'], 0, utf8_strrpos($option['value'], '.'));
					}
					
					$option_data[] = array(
						'name'  => $option['name'],
						'value' => (utf8_strlen($value) > 20 ? utf8_substr($value, 0, 20) . '..' : $value)
					);					
				}
				
				//homeals added data for side dish
				$sides_data = array();
				
				$sides = array();
				$sides = explode(",",$product['sides']);
				
				foreach ($sides as $side_id){
					$sides_info = $this->db->query("SELECT pd.name FROM " . DB_PREFIX . "product p LEFT JOIN " . DB_PREFIX . "product_description pd ON p.product_id = pd.product_id WHERE p.product_id = '" . (int)$side_id . "' AND pd.language_id = '" . (int)$order_info['language_id'] . "'");
					$sides_data[] = $sides_info->row; 
				}
				
				// product image
				$this->load->model('tool/image');
				
				
				$image_info = $this->db->query("SELECT image FROM " . DB_PREFIX . "product WHERE product_id = '" . (int)$product['product_id'] . "'");
				
				$image = $this->model_tool_image->resize($image_info->row['image'], 177, 122); 
				
			  
				$template->data['products'][] = array(
					'name'     => $product['name'],
					'model'    => $product['model'],
					'image'    => $image,
					'option'   => $option_data,
					'sides'    => $sides_data,
					'quantity' => $product['quantity'],
					'price'    => $this->currency->format($product['price'] + ($this->config->get('config_tax') ? $product['tax'] : 0), $order_info['currency_code'], $order_info['currency_value']),
					'total'    => $this->currency->format($product['total'] + ($this->config->get('config_tax') ? ($product['tax'] * $product['quantity']) : 0), $order_info['currency_code'], $order_info['currency_value'])
				);
			}
	
	
			$template->data['totals'] = $order_total_query->rows;
			
			if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/mail/order.tpl')) {
				$html = $template->fetch($this->config->get('config_template') . '/template/mail/order.tpl');
			} else {
				$html = $template->fetch('default/template/mail/order.tpl');
			}
			
			// Text Mail
			$text  = sprintf($language->get('text_new_greeting'), html_entity_decode($order_info['store_name'], ENT_QUOTES, 'UTF-8')) . "\n\n";
			$text .= $language->get('text_new_order_id') . ' ' . $order_id . "\n";
			$text .= $language->get('text_new_date_added') . ' ' . date($language->get('date_format_short'), strtotime($order_info['date_added'])) . "\n";
			$text .= $language->get('text_new_order_status') . ' ' . $order_status . "\n\n";
			
			if ($comment && $notify) {
				$text .= $language->get('text_new_instruction') . "\n\n";
				$text .= $comment . "\n\n";
			}
			
			// Products
			$text .= $language->get('text_new_products') . "\n";
			
			foreach ($order_product_query->rows as $product) {
				$text .= $product['quantity'] . 'x ' . $product['name'] . ' (' . $product['model'] . ') ' . html_entity_decode($this->currency->format($product['total'] + ($this->config->get('config_tax') ? ($product['tax'] * $product['quantity']) : 0), $order_info['currency_code'], $order_info['currency_value']), ENT_NOQUOTES, 'UTF-8') . "\n";
				
				$order_option_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "order_option WHERE order_id = '" . (int)$order_id . "' AND order_product_id = '" . $product['order_product_id'] . "'");
				
				foreach ($order_option_query->rows as $option) {
					$text .= chr(9) . '-' . $option['name'] . ' ' . (utf8_strlen($option['value']) > 20 ? utf8_substr($option['value'], 0, 20) . '..' : $option['value']) . "\n";
				}
			}
						
			$text .= "\n";
			
			$text .= $language->get('text_new_order_total') . "\n";
			
			foreach ($order_total_query->rows as $total) {
				$text .= $total['title'] . ': ' . html_entity_decode($total['text'], ENT_NOQUOTES, 'UTF-8') . "\n";
			}			
			
			$text .= "\n";
			
			if ($order_info['customer_id']) {
				$text .= $language->get('text_new_link') . "\n";
				$text .= $order_info['store_url'] . 'index.php?route=account/order/info&order_id=' . $order_id . "\n\n";
			}
			
			// Comment
			if ($order_info['comment']) {
				$text .= $language->get('text_new_comment') . "\n\n";
				$text .= $order_info['comment'] . "\n\n";
			}

			$text .= $language->get('text_new_footer') . "\n\n";
					
				$mail = new Mail(); 
				$mail->protocol = $this->config->get('config_mail_protocol');
				$mail->parameter = $this->config->get('config_mail_parameter');
				$mail->hostname = $this->config->get('config_smtp_host');
				$mail->username = $this->config->get('config_smtp_username');
				$mail->password = $this->config->get('config_smtp_password');
				$mail->port = $this->config->get('config_smtp_port');
				$mail->timeout = $this->config->get('config_smtp_timeout');			
				$mail->setTo($order_info['email']);
				$mail->setFrom($this->config->get('config_email'));
				$mail->setSender($order_info['store_name']);
				$mail->setSubject(html_entity_decode($subject, ENT_QUOTES, 'UTF-8'));
				$mail->setHtml($html);
				$mail->setText(html_entity_decode($text, ENT_QUOTES, 'UTF-8'));
				$mail->send();
				
			*/
			}
			
			if ($notify) {
				$language = new Language($order_info['language_directory']);
				$language->load($order_info['language_filename']);
				$language->load('mail/order');
			
				$subject = sprintf($language->get('text_update_subject'), html_entity_decode($order_info['store_name'], ENT_QUOTES, 'UTF-8'), $order_id);
	
				$message  = $language->get('text_update_order') . ' ' . $order_id . "\n";
				$message .= $language->get('text_update_date_added') . ' ' . date($language->get('date_format_short'), strtotime($order_info['date_added'])) . "\n\n";
				
				$order_status_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "order_status WHERE order_status_id = '" . (int)$order_status_id . "' AND language_id = '" . (int)$order_info['language_id'] . "'");
				
				if ($order_status_query->num_rows) {
					$message .= $language->get('text_update_order_status') . "\n\n";
					$message .= $order_status_query->row['name'] . "\n\n";					
				}
				
				if ($order_info['customer_id']) {
					$message .= $language->get('text_update_link') . "\n";
					$message .= $order_info['store_url'] . 'index.php?route=account/order/info&order_id=' . $order_id . "\n\n";
				}
				
				if ($comment) { 
					$message .= $language->get('text_update_comment') . "\n\n";
					$message .= $comment . "\n\n";
				}
					
				$message .= $language->get('text_update_footer');

				$mail = new Mail();
				$mail->protocol = $this->config->get('config_mail_protocol');
				$mail->parameter = $this->config->get('config_mail_parameter');
				$mail->hostname = $this->config->get('config_smtp_host');
				$mail->username = $this->config->get('config_smtp_username');
				$mail->password = $this->config->get('config_smtp_password');
				$mail->port = $this->config->get('config_smtp_port');
				$mail->timeout = $this->config->get('config_smtp_timeout');				
				$mail->setTo($order_info['email']);
				$mail->setFrom($this->config->get('config_email'));
				$mail->setSender($order_info['store_name']);
				$mail->setSubject(html_entity_decode($subject, ENT_QUOTES, 'UTF-8'));
				$mail->setText(html_entity_decode($message, ENT_QUOTES, 'UTF-8'));
				$mail->send();
			}
		}
	}
	
	
	public function updatePaypal($order_id, $txn_id) {
		$order_info = $this->getOrder($order_id);
	
		if ($order_info && $order_info['order_status_id']) {
			// Fraud Detection
			if ($this->config->get('config_fraud_detection')) {
				$this->load->model('checkout/fraud');
				
				$risk_score = $this->model_checkout_fraud->getFraudScore($order_info);
				
				if ($risk_score > $this->config->get('config_fraud_score')) {
					$order_status_id = $this->config->get('config_fraud_status_id');
				}
			}			
	
			// Ban IP
			$status = false;
			
			$this->load->model('account/customer');
			
			if ($order_info['customer_id']) {
								
				$results = $this->model_account_customer->getIps($order_info['customer_id']);
				
				foreach ($results as $result) {
					if ($this->model_account_customer->isBanIp($result['ip'])) {
						$status = true;
						
						break;
					}
				}
			} else {
				$status = $this->model_account_customer->isBanIp($order_info['ip']);
			}
			
			if ($status) {
				$order_status_id = $this->config->get('config_order_status_id');
			}		
						
			$this->db->query("UPDATE `" . DB_PREFIX . "order` SET pp_txn_id = '" . $this->db->escape($txn_id) . "' WHERE order_id = '" . (int)$order_id . "'");
			
		}
	}

	private function sendSms($msg,$to) {
		
		$data = array (
			"Msg" => $msg,
			"name" => "515076842",
			"pass" => "IEI07LNJHT",
			"from" => "0557000927",
			"to" => $to,
			"codepage" => "65001",
		);
		
		$data = http_build_query($data);
		
		$header = 	"Content-type: application/x-www-form-urlencoded\r\n".
					"Content-Length: ".mb_strlen($data,'UTF-8')."\r\n";
		
		$response = $this->do_post_request($data,$header);
		
		return $response;
	}
	
	
	private function do_post_request($data, $optional_headers = null)
	{
	   $url = "https://www.cardcom.co.il/SendSMS/Sendsms.aspx";
	   
	   $params = array('http' => array(
						'method' => 'POST',
						'content' => $data
				 ));
	   
	   if ($optional_headers !== null) {
		  $params['http']['header'] = $optional_headers;
	   }
	   
	   $ctx = stream_context_create($params);
	   	   
	   $fp = @fopen($url, 'rb', false, $ctx);
	   
	   if (!$fp) {
		  throw new Exception("Problem with $url, $php_errormsg");
	   }
	   
	   $response = @stream_get_contents($fp);
	   
	   if ($response === false) {
		  throw new Exception("Problem reading data from $url, $php_errormsg");
	   }
	   
	   return $response;
	
	}
	
	public function clearCustomerCart($customer_id){
	
		$this->db->query("UPDATE `" . DB_PREFIX . "customer` SET cart = '' WHERE customer_id = '" . (int)$customer_id . "'");
		$this->session->data['cart'] = array();
	
	}
	
	public function forceSuccess($customer_id){
	
		$this->db->query("UPDATE `" . DB_PREFIX . "customer` SET token = '1' WHERE customer_id = '" . (int)$customer_id . "'");
	
	}
	
	/*******************************************************************
	 *
	 *	function that gets order_id and sends cook mail with details Multicook
	 *	$order_id
	 *	$order_info
	 *
	 ********************************************************************/
	private function sendMailToCook($order_id,$parent_order_id,$comment=""){
		$order_info = $this->getOrder($order_id);
		
		$order_info['payment_address_1'] = str_replace('#', '',$order_info['payment_address_1']);
		
		
		
		if($parent_order_id){
		    // Downloads
		    $order_download_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "order_download WHERE order_id = '" . (int)$parent_order_id . "'");
		} else {
		    $order_download_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "order_download WHERE order_id = '" . (int)$order_id . "'");
		}   
		    
		// Gift Voucher
		$this->load->model('checkout/voucher');
		
		if($parent_order_id){
		    // Downloads
		    $order_voucher_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "order_voucher WHERE order_id = '" . (int)$parent_order_id . "'");
		} else {
		    $order_voucher_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "order_voucher WHERE order_id = '" . (int)$order_id . "'");
		}
		
		if($parent_order_id){
		    // Order Totals			
		    $order_total_query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "order_total` WHERE order_id = '" . (int)$parent_order_id . "' ORDER BY sort_order ASC");
		} else {
		    $order_total_query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "order_total` WHERE order_id = '" . (int)$order_id . "' ORDER BY sort_order ASC");
		}
		
		// Send out order confirmation mail
		$language = new Language($order_info['language_directory']);
		$language->load($order_info['language_filename']);
		$language->load('mail/order');
		
		$order_status_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "order_status WHERE order_status_id = '" . (int)$order_info['order_status_id'] . "' AND language_id = '" . (int)$order_info['language_id'] . "'");
		
		if ($order_status_query->num_rows) {
			$order_status = $order_status_query->row['name'];
		} else {
			$order_status = '';
		}
		
		$this->load->model('account/customer'); // needed for cook
		
		$cook_info = $this->model_account_customer->getCustomer($order_info['cook_id']);
		$user_info = $this->model_account_customer->getCustomer($order_info['customer_id']);
		
		$this->load->model('account/address'); // needed for cook address
		
		$cook_address = $this->model_account_address->getAddressesById((int)$order_info['cook_id']);
		$cook_address = reset($cook_address);
		
		$cook_pm_link = $this->url->link('seller/catalog-seller/profile', '&seller_id='.$order_info['cook_id'].'&openpm', 'SSL');
		
		/***********************************************************
		 *	FOR COOK
		 *	HTML Mail	HTML Mail	HTML Mail
		 *		HTML Mail	HTML Mail	HTML Mail
		 *			HTML Mail	HTML Mail	HTML Mail
		 *				HTML Mail	HTML Mail	HTML Mail
		 *
		 ***********************************************************/
		// HTML Mail
		$template = new Template();
		
		$template->data['title'] = sprintf($language->get('text_new_subject'), html_entity_decode($order_info['store_name'], ENT_QUOTES, 'UTF-8'), $order_id);
		
		// homeals mail greeting
		setlocale(LC_ALL, 'he_IL.UTF-8');
		
		$cook_name = html_entity_decode($cook_info['firstname']." ".$cook_info['lastname'], ENT_QUOTES, 'UTF-8');
		$address_1 = html_entity_decode($order_info['payment_address_1'].", ".$order_info['payment_city'], ENT_QUOTES, 'UTF-8');
		$d_date = strftime("%A, %e ב%B",strtotime($order_info['delivery_date']));
		$delivery_date = html_entity_decode($d_date, ENT_QUOTES, 'UTF-8');
		$d_time_int = strftime("%k",strtotime($order_info['delivery_time']));
		$d_time_format = ( $d_time_int - 1 ).":30 - ".( $d_time_int - 2 ).":30";
		$delivery_time = html_entity_decode($d_time_format, ENT_QUOTES, 'UTF-8');
		
		$subject = sprintf($language->get('text_new_subject_seller'), $user_info['firstname']." ".$user_info['lastname'], $delivery_date);
		
		$template->data['cook_info'] = $order_info['cook_id'];
		
		$template->data['text_greeting'] = sprintf($language->get('text_homeals_greeting'),$cook_info['firstname']);
		$template->data['text_order_status'] = $language->get('text_order_status');
		$template->data['order_status'] = $order_status;
		$template->data['text_order_id'] = $language->get('text_order_id');
		$template->data['order_id'] = $order_id;
		
		$template->data['tools'] = $order_info['tools'];
		
		$template->data['text_order_aproved'] = sprintf($language->get('text_order_aproved'),$cook_name,$address_1,$delivery_date,$delivery_time);
		$template->data['text_order_thank_you'] = $language->get('text_order_thank_you');
		
		//cook details
		$template->data['text_cook_details'] = $language->get('text_cook_details');
		$template->data['cook_name'] = sprintf("%s %s",$cook_info['firstname'],$cook_info['lastname']);
		$template->data['cook_city'] = $cook_address['city'];
		$template->data['cook_phone'] = $cook_info['telephone'];
		$template->data['cook_email'] = $cook_info['email'];
		$template->data['cook_pm'] = $cook_pm_link;
		
		
		//delivery details
		$template->data['text_delivery_details'] = $language->get('text_delivery_details');
		$template->data['text_order_when'] = $language->get('text_order_when');
		$template->data['order_when'] = sprintf($language->get('order_when'),$delivery_date,$delivery_time);
		$template->data['text_order_address'] = $language->get('text_order_address');
		$address_format = $order_info['payment_address_1'].", ".$order_info['payment_city'].", ".$order_info['payment_address_2'];
		$template->data['order_address'] = html_entity_decode($address_format, ENT_QUOTES, 'UTF-8');
		
		$template->data['text_order_compony'] = $language->get('text_order_compony');
		$template->data['order_compony'] = $order_info['payment_company'];
		
		$template->data['text_order_contact'] = $language->get('text_order_contact');
		$template->data['order_contact'] = $order_info['payment_firstname']." ".$order_info['payment_lastname'];
		$template->data['text_order_comment'] = $language->get('text_order_comment');
		$template->data['order_comment'] = $order_info['comment'];
		
		// meal(product) details
		$template->data['text_meal_details'] = $language->get('text_meal_details');
		
		// product need to have picture, name + side names, comment, quantity, price
		$template->data['text_meal_coment'] = $language->get('text_meal_coment');
		$template->data['text_quantity'] = $language->get('text_quantity');
		$template->data['text_per_price'] = $language->get('text_per_price');
		
		// sakum yes or no
		$template->data['text_order_tools'] = $language->get('text_order_tools');
		if($order_info['tools']){
			$template->data['order_tools'] = $language->get('need_tools');
		} else {
			$template->data['order_tools'] = $language->get('dont_need_tools');
		}
		
		// order summary
		$template->data['text_order_summary'] = $language->get('text_order_summary');
		$template->data['text_meals_total'] = $language->get('text_meals_total');
		$template->data['text_delivery_cost'] = $language->get('text_delivery_cost');
		$template->data['text_total'] = $language->get('text_new_total');
		
		// fotter
		$template->data['text_follow_us'] = $language->get('text_follow_us');
		$template->data['text_homeals_help'] = sprintf($language->get('text_homeals_help'),"03-3741886",$cook_info['email']);
		
		// end of homeals mail
		
		$template->data['text_link'] = $language->get('text_new_link');
		$template->data['text_download'] = $language->get('text_new_download');
		$template->data['text_order_detail'] = $language->get('text_new_order_detail');
		$template->data['text_instruction'] = $language->get('text_new_instruction');
		
		$template->data['text_date_added'] = $language->get('text_new_date_added');
		$template->data['text_payment_method'] = $language->get('text_new_payment_method');	
		$template->data['text_shipping_method'] = $language->get('text_new_shipping_method');
		$template->data['text_email'] = $language->get('text_new_email');
		$template->data['text_telephone'] = $language->get('text_new_telephone');
		$template->data['text_ip'] = $language->get('text_new_ip');
		$template->data['text_payment_address'] = $language->get('text_new_payment_address');
		$template->data['text_shipping_address'] = $language->get('text_new_shipping_address');
		$template->data['text_product'] = $language->get('text_new_product');
		$template->data['text_model'] = $language->get('text_new_model');
		$template->data['text_price'] = $language->get('text_new_price');
		
		$template->data['text_footer'] = $language->get('text_new_footer');
		$template->data['text_powered'] = $language->get('text_new_powered');
		
		$template->data['logo'] = $this->config->get('config_url') . 'image/' . $this->config->get('config_logo');		
		$template->data['store_name'] = $order_info['store_name'];
		$template->data['store_url'] = $order_info['store_url'];
		$template->data['customer_id'] = $order_info['customer_id'];
		$template->data['link'] = $order_info['store_url'] . 'index.php?route=account/order/info&order_id=' . $order_id;
		
		if ($order_download_query->num_rows) {
			$template->data['download'] = $order_info['store_url'] . 'index.php?route=account/download';
		} else {
			$template->data['download'] = '';
		}
		
		
		$template->data['date_added'] = date($language->get('date_format_short'), strtotime($order_info['date_added']));    	
		$template->data['payment_method'] = $order_info['payment_method'];
		$template->data['shipping_method'] = $order_info['shipping_method'];
		$template->data['email'] = $order_info['email'];
		$template->data['telephone'] = $order_info['payment_postcode'];
		$template->data['ip'] = $order_info['ip'];
		
		
		$template->data['cook_text_greeting'] = sprintf($language->get('cook_text_greeting'),$order_info['firstname'],$order_info['lastname']);
		$template->data['cook_text_meals'] = $language->get('cook_text_meals');
		$template->data['cook_text_payment'] = $language->get('cook_text_payment');
		$template->data['cook_text_aprove'] = $language->get('cook_text_aprove');
		$template->data['cook_link_aprove'] = $order_info['store_url'] . 'index.php?route=seller/account-order/approveOrder&oid=' . $order_id;
		$template->data['cook_text_delivery_time'] = $language->get('cook_text_delivery_time');
		$template->data['cook_text_details'] = $language->get('cook_text_details');
		$template->data['cook_text_place'] = $language->get('cook_text_place');
		$template->data['cook_text_mail'] = $language->get('cook_text_mail');
		$template->data['cook_text_phone'] = $language->get('cook_text_phone');
		$template->data['cook_text_phone_extra'] = $language->get('cook_text_phone_extra');
		$template->data['cook_text_username'] = $language->get('cook_text_username');
		
		$template->data['cook_order_username'] = $order_info['payment_firstname'].' '.$order_info['payment_lastname'];
		
		$template->data['cook_order_city_name'] = $order_info['payment_city'];
		
		$template->data['cook_order_address_1'] = $order_info['payment_address_1'].", ".$order_info['payment_city'];
		$template->data['cook_order_address_2'] = $order_info['payment_address_2'];
				
		if ($comment) {
			$template->data['comment'] = nl2br($comment);
		} else {
			$template->data['comment'] = '';
		}
					
		if ($order_info['payment_address_format']) {
			$format = $order_info['payment_address_format'];
		} else {
			$format = '{firstname} {lastname}' . "\n" . '{company}' . "\n" . '{address_1}' . "\n" . '{address_2}' . "\n" . '{city} {postcode}' . "\n" . '{zone}' . "\n" . '{country}';
		}
		
		$find = array(
			'{firstname}',
			'{lastname}',
			'{company}',
			'{address_1}',
			'{address_2}',
			'{city}',
			'{postcode}',
			'{zone}',
			'{zone_code}',
			'{country}'
		);
		
		$replace = array(
			'firstname' => $order_info['payment_firstname'],
			'lastname'  => $order_info['payment_lastname'],
			'company'   => $order_info['payment_company'],
			'address_1' => $order_info['payment_address_1'],
			'address_2' => $order_info['payment_address_2'],
			'city'      => $order_info['payment_city'],
			'postcode'  => $order_info['payment_postcode'],
			'zone'      => $order_info['payment_zone'],
			'zone_code' => $order_info['payment_zone_code'],
			'country'   => $order_info['payment_country']  
		);
		
		
		$template->data['payment_address'] = str_replace(array("\r\n", "\r", "\n"), '<br />', preg_replace(array("/\s\s+/", "/\r\r+/", "/\n\n+/"), '<br />', trim(str_replace($find, $replace, $format))));						
								
		if ($order_info['shipping_address_format']) {
			$format = $order_info['shipping_address_format'];
		} else {
			$format = '{firstname} {lastname}' . "\n" . '{company}' . "\n" . '{address_1}' . "\n" . '{address_2}' . "\n" . '{city} {postcode}' . "\n" . '{zone}' . "\n" . '{country}';
		}
		
		$find = array(
			'{firstname}',
			'{lastname}',
			'{company}',
			'{address_1}',
			'{address_2}',
			'{city}',
			'{postcode}',
			'{zone}',
			'{zone_code}',
			'{country}'
		);
		
		$replace = array(
			'firstname' => $order_info['shipping_firstname'],
			'lastname'  => $order_info['shipping_lastname'],
			'company'   => $order_info['shipping_company'],
			'address_1' => $order_info['shipping_address_1'],
			'address_2' => $order_info['shipping_address_2'],
			'city'      => $order_info['shipping_city'],
			'postcode'  => $order_info['shipping_postcode'],
			'zone'      => $order_info['shipping_zone'],
			'zone_code' => $order_info['shipping_zone_code'],
			'country'   => $order_info['shipping_country']  
		);
		
		$template->data['shipping_address'] = str_replace(array("\r\n", "\r", "\n"), '<br />', preg_replace(array("/\s\s+/", "/\r\r+/", "/\n\n+/"), '<br />', trim(str_replace($find, $replace, $format))));
		
		// Products
		$template->data['products'] = array();
		
		$pros_quantity = 0;
		$main_total_cook = 0;
		
		$cook_comments = array();
		
		$order_product_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "order_product WHERE order_id = '" . (int)$parent_order_id . "' AND cook_id ='".$order_info['cook_id']."'");
		
		foreach ($order_product_query->rows as $product) {
			$option_data = array();
			
			if($order_info['parent_order_id']){
				$order_option_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "order_option WHERE order_id = '" . (int)$order_info['parent_order_id'] . "' AND order_product_id = '" . (int)$product['order_product_id'] . "'");
			} else {
				$order_option_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "order_option WHERE order_id = '" . (int)$order_id . "' AND order_product_id = '" . (int)$product['order_product_id'] . "'");
			}
			
			foreach ($order_option_query->rows as $option) {
				if ($option['type'] != 'file') {
					$value = $option['value'];
				} else {
					$value = utf8_substr($option['value'], 0, utf8_strrpos($option['value'], '.'));
				}
				
				$option_data[] = array(
					'name'  => $option['name'],
					'value' => (utf8_strlen($value) > 20 ? utf8_substr($value, 0, 20) . '..' : $value)
				);					
			}
			
			
			$cook_comments[] = $product['comment'];
			
			//homeals added data for side dish
			$sides_data = array();
			
			$sides = array();
			$sides = explode(",",$product['sides']);
			
			foreach ($sides as $side_id){
				$sides_info = $this->db->query("SELECT pd.name FROM " . DB_PREFIX . "product p LEFT JOIN " . DB_PREFIX . "product_description pd ON p.product_id = pd.product_id WHERE p.product_id = '" . (int)$side_id . "' AND pd.language_id = '" . (int)$order_info['language_id'] . "'");
				$sides_data[] = $sides_info->row; 
			}
			
			// product image
			$this->load->model('tool/image');
			
			
			$image_info = $this->db->query("SELECT image FROM " . DB_PREFIX . "product WHERE product_id = '" . (int)$product['product_id'] . "'");
			
			$image = $this->model_tool_image->resize($image_info->row['image'], 177, 122); 
			
		  
			$template->data['products'][] = array(
				'name'     => $product['name'],
				'model'    => $product['model'],
				'image'    => $image,
				'option'   => $option_data,
				'sides'    => $sides_data,
				'comment'  => $product['comment'],
				'quantity' => $product['quantity'],
				'price'    => $this->currency->format($product['price'], $order_info['currency_code'], $order_info['currency_value']),
				'total'    => $this->currency->format($product['total'], $order_info['currency_code'], $order_info['currency_value'])
			);
			
			$pros_quantity += $product['quantity'];
			$main_total_cook += $product['total'];
			$main_total_store += $this->config->get('config_tax') ? $product['tax'] : 0;
			$last_tax_value = $this->config->get('config_tax') ? $product['tax'] : $last_tax_value;
		}
		
		$template->data['cook_total_numofmeals'] = $pros_quantity;
		
		$totaly = 0;
		// Order Totals			
		foreach ($order_total_query->rows as $order_total) {				
			if ($order_total['code'] == "total"){
				$totaly = $order_total['value'];
			}
		}
		
		if($totaly == 0){
			$main_total_cook = 0;
		}
		
		$template->data['cook_total_pay'] = $this->currency->format($main_total_cook, $order_info['currency_code'], $order_info['currency_value']);
		
		// Vouchers
		$template->data['vouchers'] = array();
		
		foreach ($order_voucher_query->rows as $voucher) {
			$template->data['vouchers'][] = array(
				'description' => $voucher['description'],
				'amount'      => $this->currency->format($voucher['amount'], $order_info['currency_code'], $order_info['currency_value']),
			);
		}
		
		$template->data['totals'] = $order_total_query->rows;
		
		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/mail/cook.tpl')) {
			$html = $template->fetch($this->config->get('config_template') . '/template/mail/cook.tpl');
		} else {
			$html = $template->fetch('default/template/mail/cook.tpl');
		}
		
		// Text Mail
		$text  = sprintf($language->get('text_new_greeting'), html_entity_decode($order_info['store_name'], ENT_QUOTES, 'UTF-8')) . "\n\n";
		$text .= $language->get('text_new_order_id') . ' ' . $order_id . "\n";
		$text .= $language->get('text_new_date_added') . ' ' . date($language->get('date_format_short'), strtotime($order_info['date_added'])) . "\n";
		$text .= $language->get('text_new_order_status') . ' ' . $order_status . "\n\n";
		
		if ($comment && $notify) {
			$text .= $language->get('text_new_instruction') . "\n\n";
			$text .= $comment . "\n\n";
		}
		
		// Products
		$text .= $language->get('text_new_products') . "\n";
		
		foreach ($order_product_query->rows as $product) {
			$text .= $product['quantity'] . 'x ' . $product['name'] . ' (' . $product['model'] . ') ' . html_entity_decode($this->currency->format($product['total'] + ($this->config->get('config_tax') ? ($product['tax'] * $product['quantity']) : 0), $order_info['currency_code'], $order_info['currency_value']), ENT_NOQUOTES, 'UTF-8') . "\n";
			
			$order_option_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "order_option WHERE order_id = '" . (int)$order_id . "' AND order_product_id = '" . $product['order_product_id'] . "'");
			
			foreach ($order_option_query->rows as $option) {
				$text .= chr(9) . '-' . $option['name'] . ' ' . (utf8_strlen($option['value']) > 20 ? utf8_substr($option['value'], 0, 20) . '..' : $option['value']) . "\n";
			}
		}
		
		foreach ($order_voucher_query->rows as $voucher) {
			$text .= '1x ' . $voucher['description'] . ' ' . $this->currency->format($voucher['amount'], $order_info['currency_code'], $order_info['currency_value']);
		}
					
		$text .= "\n";
		
		$text .= $language->get('text_new_order_total') . "\n";
		
		foreach ($order_total_query->rows as $total) {
			$text .= $total['title'] . ': ' . html_entity_decode($total['text'], ENT_NOQUOTES, 'UTF-8') . "\n";
		}			
		
		$text .= "\n";
		
		if ($order_info['customer_id']) {
			$text .= $language->get('text_new_link') . "\n";
			$text .= $order_info['store_url'] . 'index.php?route=account/order/info&order_id=' . $order_id . "\n\n";
		}
		
		if ($order_download_query->num_rows) {
			$text .= $language->get('text_new_download') . "\n";
			$text .= $order_info['store_url'] . 'index.php?route=account/download' . "\n\n";
		}
		
		// Comment
		if ($order_info['comment']) {
			$text .= $language->get('text_new_comment') . "\n\n";
			$text .= $order_info['comment'] . "\n\n";
		}
		
		$text .= $language->get('text_new_footer') . "\n\n";
		
		$mail = new Mail(); 
		$mail->protocol = $this->config->get('config_mail_protocol');
		$mail->parameter = $this->config->get('config_mail_parameter');
		$mail->hostname = $this->config->get('config_smtp_host');
		$mail->username = $this->config->get('config_smtp_username');
		$mail->password = $this->config->get('config_smtp_password');
		$mail->port = $this->config->get('config_smtp_port');
		$mail->timeout = $this->config->get('config_smtp_timeout');			
		$mail->setTo($cook_info['email']); // <-- send here
		$mail->setFrom($this->config->get('config_email'));
		$mail->setSender($order_info['store_name']);
		$mail->setSubject(html_entity_decode($subject, ENT_QUOTES, 'UTF-8'));
		$mail->setHtml($html);
		//$mail->setText(html_entity_decode($text, ENT_QUOTES, 'UTF-8'));
		$mail->send();
	       
	       /****************************
		*
		*	HOMEALS sms code
		*
		**************************/
			
		setlocale(LC_ALL, 'he_IL.UTF-8');
		
		$sms_date = strftime("%a %d/%m",strtotime($order_info['delivery_date']));
		$sms_time_1 = strftime("%R",strtotime($order_info['delivery_time']." -2 hour"));
		$sms_time_2 = strftime("%R",strtotime($order_info['delivery_time']." -1 hour"));
		$sms_date_time = $sms_date." ".$sms_time_1."-".$sms_time_2."";
		
		$msg = sprintf($language->get('sms_new_order'),$order_info['firstname'],$order_info['lastname'],$sms_date_time);
		
		$order_product_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "order_product WHERE order_id = '" . (int)$parent_order_id . "' AND cook_id ='".$order_info['cook_id']."'");
		
		foreach ($order_product_query->rows as $product) {
			
			$pieces = explode(" ", $product['name']);
			$product_name_short = implode(" ", array_splice($pieces, 0, 3));
			
			$msg .= ' '. $product['quantity'] . ' ' . $product_name_short .'..,'. $product['comment']." ";
		}
		
		$msg = mb_truncate($msg,480);
		
		$msg .= sprintf($language->get('sms_approve'),$order_id);
		
		$to = $cook_info['telephone'];
		
		// turn back on
		$response = $this->sendSms($msg,$to);
		
		/****************************
		 *
		 *	HOMEALS sms code end
		 *
		 **************************/
	}
	
	
	
	/***********************************************************
	*	FOR BUYER sendMailToCustomer
	*	HTML Mail	HTML Mail	HTML Mail
	*		HTML Mail	HTML Mail	HTML Mail
	*			HTML Mail	HTML Mail	HTML Mail
	*				HTML Mail	HTML Mail	HTML Mail
	*
	***********************************************************/
	private function sendMailToCustomer($order_id){
		$order_info = $this->getOrder($order_id);
		
		$order_info['payment_address_1'] = str_replace('#', '',$order_info['payment_address_1']);
		
		if($order_info['parent_order_id']){
		    $order_product_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "order_product WHERE order_id = '" . (int)$order_info['parent_order_id'] . "' AND cook_id ='".$order_info['cook_id']."'");
		} else {
		    $order_product_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "order_product WHERE order_id = '" . (int)$order_id . "'");
		}
		
		if($order_info['parent_order_id']){
		    // Downloads
		    $order_download_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "order_download WHERE order_id = '" . (int)$order_info['parent_order_id'] . "'");
		} else {
		    $order_download_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "order_download WHERE order_id = '" . (int)$order_id . "'");
		}   
		    
		// Gift Voucher
		$this->load->model('checkout/voucher');
		
		if($order_info['parent_order_id']){
		    // Downloads
		    $order_voucher_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "order_voucher WHERE order_id = '" . (int)$order_info['parent_order_id'] . "'");
		} else {
		    $order_voucher_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "order_voucher WHERE order_id = '" . (int)$order_id . "'");
		}
		
		if($order_info['parent_order_id']){
		    // Order Totals			
		    $order_total_query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "order_total` WHERE order_id = '" . (int)$order_info['parent_order_id'] . "' ORDER BY sort_order ASC");
		} else {
		    $order_total_query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "order_total` WHERE order_id = '" . (int)$order_id . "' ORDER BY sort_order ASC");
		}
		
		// Send out order confirmation mail
		$language = new Language($order_info['language_directory']);
		$language->load($order_info['language_filename']);
		$language->load('mail/order');
		
		$order_status_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "order_status WHERE order_status_id = '" . (int)$order_info['order_status_id'] . "' AND language_id = '" . (int)$order_info['language_id'] . "'");
		
		if ($order_status_query->num_rows) {
			$order_status = $order_status_query->row['name'];
		} else {
			$order_status = '';
		}
		
		$this->load->model('account/customer'); // needed for cook
		
		$cook_info = $this->model_account_customer->getCustomer($order_info['cook_id']);
		$user_info = $this->model_account_customer->getCustomer($order_info['customer_id']);
		
		$this->load->model('account/address'); // needed for cook address
		
		$cook_address = $this->model_account_address->getAddressesById((int)$order_info['cook_id']);
		$cook_address = reset($cook_address);
		
		$cook_pm_link = $this->url->link('seller/catalog-seller/profile', '&seller_id='.$order_info['cook_id'].'&openpm', 'SSL');
	       
	       // HTML Mail
	       $template = new Template();
	       
	       $template->data['title'] = sprintf($language->get('text_new_subject'), html_entity_decode($order_info['store_name'], ENT_QUOTES, 'UTF-8'), $order_id);
	       
	       // homeals mail greeting
	       setlocale(LC_ALL, 'he_IL.UTF-8');
	       
		$cook_name = html_entity_decode($cook_info['firstname']." ".$cook_info['lastname'], ENT_QUOTES, 'UTF-8');
		$address_1 = html_entity_decode($order_info['payment_address_1'].", ".$order_info['payment_city'], ENT_QUOTES, 'UTF-8');
		$d_date = strftime("%A, %e ב%B",strtotime($order_info['delivery_date']));
		$delivery_date = html_entity_decode($d_date, ENT_QUOTES, 'UTF-8');
		$d_time_int = strftime("%k",strtotime($order_info['delivery_time']));
		$d_time_format = ( $d_time_int ).":30 - ".( $d_time_int - 1 ).":30";
		$delivery_time = html_entity_decode($d_time_format, ENT_QUOTES, 'UTF-8');
		
		$cook_name = "";
		$pros_quantity = 0;
		
		$cooks = json_decode($order_info['cook_id']);
		
		foreach ($cooks as $cook_id) {
			$cook_info = $this->model_account_customer->getCustomer($cook_id);
			$cook_name .= trim($cook_info['firstname']).", ";
		}
		
		foreach ($order_product_query->rows as $product) {
			$pros_quantity += $product['quantity'];
		}
		
	       $subject = sprintf($language->get('text_new_order_subject'), $pros_quantity , $cook_name, $delivery_date);
	       
	       $template->data['cook_info'] = $order_info['cook_id'];
	       
	       $template->data['text_greeting'] = sprintf($language->get('text_homeals_greeting'),$order_info['firstname']);
	       $template->data['text_order_status'] = $language->get('text_order_status');
	       $template->data['order_status'] = "ממתין לאישור";//$order_status;
	       $template->data['text_order_id'] = $language->get('text_order_id');
	       $template->data['order_id'] = $order_id;
	       
	       $template->data['text_order_aproved'] = sprintf($language->get('text_order_new'),$pros_quantity,$cook_name,$delivery_date);
	       $template->data['text_here_are_details'] = $language->get('text_here_are_details');
	       
	       $template->data['text_meals_word'] = $language->get('text_meals_word');
	       $template->data['text_here_are_details'] = $language->get('text_here_are_details');
	       
	       $template->data['text_deliver_word'] = $language->get('text_deliver_word');
	       $template->data['text_here_are_details'] = $language->get('text_here_are_details');
	       
	       $template->data['text_order_aproved'] = sprintf($language->get('text_order_new'),$pros_quantity,$cook_name,$delivery_date);
	       
	       $template->data['text_order_thanks_you'] = $language->get('text_order_thanks_you');
	       
	       //cook details
	       $template->data['text_cook_details'] 	= $language->get('text_cook_details');
	       $template->data['cook_name']  			= sprintf("%s %s",$cook_info['firstname'],$cook_info['lastname']);
	       $template->data['cook_city']  			= $cook_address['city'];
	       $template->data['cook_phone'] 			= $cook_info['telephone'];
	       $template->data['cook_email'] 			= $cook_info['email'];
	       $template->data['cook_pm']   			= $cook_pm_link;
	       
			// sakum yes or no
			$template->data['text_order_tools'] = $language->get('text_order_tools');
			if($order_info['tools']){
				$template->data['order_tools'] = $language->get('need_tools');
			} else {
				$template->data['order_tools'] = $language->get('dont_need_tools');
			}
	       
	       //delivery details
	       $template->data['text_delivery_details'] = $language->get('text_delivery_details');
	       $template->data['text_order_when'] = $language->get('text_order_when');
	       $template->data['order_when'] = sprintf($language->get('order_when'),$delivery_date,$delivery_time);
	       $template->data['text_order_address'] = $language->get('text_order_address');
	       $address_format = $order_info['payment_address_1'].", ".$order_info['payment_city'].", ".$order_info['payment_address_2'];
	       $template->data['order_address'] = html_entity_decode($order_info['payment_address_1'].", ".$order_info['payment_city'], ENT_QUOTES, 'UTF-8');
	       
	       $template->data['order_address_2'] =  html_entity_decode($order_info['payment_address_2'], ENT_QUOTES, 'UTF-8');
	       
	       $template->data['text_order_compony'] = $language->get('text_order_compony');
	       $template->data['order_compony'] = $order_info['payment_company'];
	       
	       $template->data['text_order_contact'] = $language->get('text_order_contact');
	       $template->data['order_contact'] = $order_info['payment_firstname']." ".$order_info['payment_lastname'];
	       $template->data['text_order_comment'] = $language->get('text_order_comment');
	       $template->data['order_comment'] = $order_info['comment'];
	       
		   $template->data['text_order_phone'] = $language->get('text_order_phone');
	       $template->data['order_phone'] = sprintf($language->get('order_phone'),$order_info['payment_postcode']);
	       
	       // meal(product) details
	       $template->data['text_meal_details'] = $language->get('text_meal_details');
	       $template->data['text_per_price'] = $language->get('text_per_price');
	       
	       // product need to have picture, name + side names, comment, quantity, price
	       $template->data['text_meal_coment'] = $language->get('text_meal_coment');
	       $template->data['text_quantity'] = $language->get('text_quantity');
	       
	       // order summary
	       $template->data['text_order_summary'] = $language->get('text_order_summary');
	       $template->data['text_meals_total'] = $language->get('text_meals_total');
	       $template->data['text_delivery_cost'] = $language->get('text_delivery_cost');
	       $template->data['text_total'] = $language->get('text_new_total');
	       
	       // fotter
	       $template->data['text_follow_us'] = $language->get('text_follow_us');
	       $template->data['text_homeals_help'] = sprintf($language->get('text_homeals_help'),"03-3741886",$order_info['email']);
	       
	       // end of homeals mail
	       
	       $template->data['text_link'] = $language->get('text_new_link');
	       $template->data['text_download'] = $language->get('text_new_download');
	       $template->data['text_order_detail'] = $language->get('text_new_order_detail');
	       $template->data['text_instruction'] = $language->get('text_new_instruction');
	       
	       $template->data['text_date_added'] = $language->get('text_new_date_added');
	       $template->data['text_payment_method'] = $language->get('text_new_payment_method');	
	       $template->data['text_shipping_method'] = $language->get('text_new_shipping_method');
	       $template->data['text_email'] = $language->get('text_new_email');
	       $template->data['text_telephone'] = $language->get('text_new_telephone');
	       $template->data['text_ip'] = $language->get('text_new_ip');
	       $template->data['text_payment_address'] = $language->get('text_new_payment_address');
	       $template->data['text_shipping_address'] = $language->get('text_new_shipping_address');
	       $template->data['text_product'] = $language->get('text_new_product');
	       $template->data['text_model'] = $language->get('text_new_model');
	       $template->data['text_price'] = $language->get('text_new_price');

	       $template->data['text_footer'] = $language->get('text_new_footer');
	       $template->data['text_powered'] = $language->get('text_new_powered');
	       
	       $template->data['logo'] = $this->config->get('config_url') . 'image/' . $this->config->get('config_logo');		
	       $template->data['store_name'] = $order_info['store_name'];
	       $template->data['store_url'] = $order_info['store_url'];
	       $template->data['customer_id'] = $order_info['customer_id'];
	       $template->data['link'] = $order_info['store_url'] . 'index.php?route=account/order/info&order_id=' . $order_id;
	       
	       if ($order_download_query->num_rows) {
		       $template->data['download'] = $order_info['store_url'] . 'index.php?route=account/download';
	       } else {
		       $template->data['download'] = '';
	       }
	       
	       
	       $template->data['date_added'] = date($language->get('date_format_short'), strtotime($order_info['date_added']));    	
	       $template->data['payment_method'] = $order_info['payment_method'];
	       $template->data['shipping_method'] = $order_info['shipping_method'];
	       $template->data['email'] = $order_info['email'];
	       $template->data['telephone'] = $order_info['payment_postcode'];
	       $template->data['ip'] = $order_info['ip'];
	       
	       if ($comment && $notify) {
		       $template->data['comment'] = nl2br($comment);
	       } else {
		       $template->data['comment'] = '';
	       }
				       
	       if ($order_info['payment_address_format']) {
		       $format = $order_info['payment_address_format'];
	       } else {
		       $format = '{firstname} {lastname}' . "\n" . '{company}' . "\n" . '{address_1}' . "\n" . '{address_2}' . "\n" . '{city} {postcode}' . "\n" . '{zone}' . "\n" . '{country}';
	       }
	       
	       $find = array(
		       '{firstname}',
		       '{lastname}',
		       '{company}',
		       '{address_1}',
		       '{address_2}',
		       '{city}',
		       '{postcode}',
		       '{zone}',
		       '{zone_code}',
		       '{country}'
	       );
       
	       $replace = array(
		       'firstname' => $order_info['payment_firstname'],
		       'lastname'  => $order_info['payment_lastname'],
		       'company'   => $order_info['payment_company'],
		       'address_1' => $order_info['payment_address_1'],
		       'address_2' => $order_info['payment_address_2'],
		       'city'      => $order_info['payment_city'],
		       'postcode'  => $order_info['payment_postcode'],
		       'zone'      => $order_info['payment_zone'],
		       'zone_code' => $order_info['payment_zone_code'],
		       'country'   => $order_info['payment_country']  
	       );
	       
       
	       $template->data['payment_address'] = str_replace(array("\r\n", "\r", "\n"), '<br />', preg_replace(array("/\s\s+/", "/\r\r+/", "/\n\n+/"), '<br />', trim(str_replace($find, $replace, $format))));						
							       
	       if ($order_info['shipping_address_format']) {
		       $format = $order_info['shipping_address_format'];
	       } else {
		       $format = '{firstname} {lastname}' . "\n" . '{company}' . "\n" . '{address_1}' . "\n" . '{address_2}' . "\n" . '{city} {postcode}' . "\n" . '{zone}' . "\n" . '{country}';
	       }
	       
	       $find = array(
		       '{firstname}',
		       '{lastname}',
		       '{company}',
		       '{address_1}',
		       '{address_2}',
		       '{city}',
		       '{postcode}',
		       '{zone}',
		       '{zone_code}',
		       '{country}'
	       );
       
	       $replace = array(
		       'firstname' => $order_info['shipping_firstname'],
		       'lastname'  => $order_info['shipping_lastname'],
		       'company'   => $order_info['shipping_company'],
		       'address_1' => $order_info['shipping_address_1'],
		       'address_2' => $order_info['shipping_address_2'],
		       'city'      => $order_info['shipping_city'],
		       'postcode'  => $order_info['shipping_postcode'],
		       'zone'      => $order_info['shipping_zone'],
		       'zone_code' => $order_info['shipping_zone_code'],
		       'country'   => $order_info['shipping_country']  
	       );
       
	       $template->data['shipping_address'] = str_replace(array("\r\n", "\r", "\n"), '<br />', preg_replace(array("/\s\s+/", "/\r\r+/", "/\n\n+/"), '<br />', trim(str_replace($find, $replace, $format))));
	       
	       // Products
	       $template->data['products'] = array();
		       
	       foreach ($order_product_query->rows as $product) {
		       $option_data = array();
		       
		       $order_option_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "order_option WHERE order_id = '" . (int)$order_id . "' AND order_product_id = '" . (int)$product['order_product_id'] . "'");
		       
		       foreach ($order_option_query->rows as $option) {
			       if ($option['type'] != 'file') {
				       $value = $option['value'];
			       } else {
				       $value = utf8_substr($option['value'], 0, utf8_strrpos($option['value'], '.'));
			       }
			       
			       $option_data[] = array(
				       'name'  => $option['name'],
				       'value' => (utf8_strlen($value) > 20 ? utf8_substr($value, 0, 20) . '..' : $value)
			       );					
		       }
		       
		       //homeals added data for side dish
		       $sides_data = array();
		       
		       $sides = array();
		       $sides = explode(",",$product['sides']);
		       
		       foreach ($sides as $side_id){
			       $sides_info = $this->db->query("SELECT pd.name FROM " . DB_PREFIX . "product p LEFT JOIN " . DB_PREFIX . "product_description pd ON p.product_id = pd.product_id WHERE p.product_id = '" . (int)$side_id . "' AND pd.language_id = '" . (int)$order_info['language_id'] . "'");
			       $sides_data[] = $sides_info->row; 
		       }
		       
		       // product image
		       $this->load->model('tool/image');
		       
		       
		       $image_info = $this->db->query("SELECT image FROM " . DB_PREFIX . "product WHERE product_id = '" . (int)$product['product_id'] . "'");
		       
		       $image = $this->model_tool_image->resize($image_info->row['image'], 177, 122); 
		       
		 
		       $template->data['products'][] = array(
			       'name'     => $product['name'],
			       'model'    => $product['model'],
			       'image'    => $image,
			       'option'   => $option_data,
			       'comment'  => $product['comment'],
			       'sides'    => $sides_data,
			       'quantity' => $product['quantity'],
			       'price'    => $this->currency->format($product['price'] + ($this->config->get('config_tax') ? $product['tax'] : 0), $order_info['currency_code'], $order_info['currency_value']),
			       'total'    => $this->currency->format($product['total'] + ($this->config->get('config_tax') ? ($product['tax'] * $product['quantity']) : 0), $order_info['currency_code'], $order_info['currency_value'])
		       );
	       }

	       // Vouchers
	       $template->data['vouchers'] = array();
	       
	       foreach ($order_voucher_query->rows as $voucher) {
		       $template->data['vouchers'][] = array(
			       'description' => $voucher['description'],
			       'amount'      => $this->currency->format($voucher['amount'], $order_info['currency_code'], $order_info['currency_value']),
		       );
	       }

	       $template->data['totals'] = $order_total_query->rows;
	       
	       if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/mail/new_order.tpl')) {
		       $html = $template->fetch($this->config->get('config_template') . '/template/mail/new_order.tpl');
	       } else {
		       $html = $template->fetch('default/template/mail/new_order.tpl');
	       }
	       
	       // Text Mail
	       $text  = sprintf($language->get('text_new_greeting'), html_entity_decode($order_info['store_name'], ENT_QUOTES, 'UTF-8')) . "\n\n";
	       $text .= $language->get('text_new_order_id') . ' ' . $order_id . "\n";
	       $text .= $language->get('text_new_date_added') . ' ' . date($language->get('date_format_short'), strtotime($order_info['date_added'])) . "\n";
	       $text .= $language->get('text_new_order_status') . ' ' . $order_status . "\n\n";
	       
	       if ($comment && $notify) {
		       $text .= $language->get('text_new_instruction') . "\n\n";
		       $text .= $comment . "\n\n";
	       }
	       
	       // Products
	       $text .= $language->get('text_new_products') . "\n";
	       
	       foreach ($order_product_query->rows as $product) {
		       $text .= $product['quantity'] . 'x ' . $product['name'] . ' (' . $product['model'] . ') ' . html_entity_decode($this->currency->format($product['total'] + ($this->config->get('config_tax') ? ($product['tax'] * $product['quantity']) : 0), $order_info['currency_code'], $order_info['currency_value']), ENT_NOQUOTES, 'UTF-8') . "\n";
		       
		       $order_option_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "order_option WHERE order_id = '" . (int)$order_id . "' AND order_product_id = '" . $product['order_product_id'] . "'");
		       
		       foreach ($order_option_query->rows as $option) {
			       $text .= chr(9) . '-' . $option['name'] . ' ' . (utf8_strlen($option['value']) > 20 ? utf8_substr($option['value'], 0, 20) . '..' : $option['value']) . "\n";
		       }
	       }
	       
	       foreach ($order_voucher_query->rows as $voucher) {
		       $text .= '1x ' . $voucher['description'] . ' ' . $this->currency->format($voucher['amount'], $order_info['currency_code'], $order_info['currency_value']);
	       }
				       
	       $text .= "\n";
	       
	       $text .= $language->get('text_new_order_total') . "\n";
	       
	       foreach ($order_total_query->rows as $total) {
		       $text .= $total['title'] . ': ' . html_entity_decode($total['text'], ENT_NOQUOTES, 'UTF-8') . "\n";
	       }			
	       
	       $text .= "\n";
	       
	       if ($order_info['customer_id']) {
		       $text .= $language->get('text_new_link') . "\n";
		       $text .= $order_info['store_url'] . 'index.php?route=account/order/info&order_id=' . $order_id . "\n\n";
	       }
       
	       if ($order_download_query->num_rows) {
		       $text .= $language->get('text_new_download') . "\n";
		       $text .= $order_info['store_url'] . 'index.php?route=account/download' . "\n\n";
	       }
	       
	       // Comment
	       if ($order_info['comment']) {
		       $text .= $language->get('text_new_comment') . "\n\n";
		       $text .= $order_info['comment'] . "\n\n";
	       }
	       
	       $text .= $language->get('text_new_footer') . "\n\n";
	       $template->data['text_per_price'] = $language->get('text_per_price');
	       
	       $mail = new Mail();
	       $mail->protocol = $this->config->get('config_mail_protocol');
	       $mail->parameter = $this->config->get('config_mail_parameter');
	       $mail->hostname = $this->config->get('config_smtp_host');
	       $mail->username = $this->config->get('config_smtp_username');
	       $mail->password = $this->config->get('config_smtp_password');
	       $mail->port = $this->config->get('config_smtp_port');
	       $mail->timeout = $this->config->get('config_smtp_timeout');			
	       $mail->setTo($order_info['email']);
	       $mail->setFrom($this->config->get('config_email'));
	       $mail->setSender($order_info['store_name']);
	       $mail->setSubject(html_entity_decode($subject, ENT_QUOTES, 'UTF-8'));
	       $mail->setHtml($html);
	       //$mail->setText(html_entity_decode($text, ENT_QUOTES, 'UTF-8'));
	       $mail->send();
	}
	
	private function sendMailToConfirm($order_id){
		$order_info = $this->getOrder($order_id);
		
		$order_status_id = $order_info['order_status_id'];
		
		$order_info['payment_address_1'] = str_replace('#', '',$order_info['payment_address_1']);
		
		if($order_info['parent_order_id']){
			$parent_order_id = $order_info['parent_order_id'];
		    $order_product_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "order_product WHERE order_id = '" . (int)$order_info['parent_order_id'] . "' AND cook_id ='".$order_info['cook_id']."'");
		} else {
			$parent_order_id = false;
		    $order_product_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "order_product WHERE order_id = '" . (int)$order_id . "'");
		}
		
		if($order_info['parent_order_id']){
		    // Downloads
		    $order_download_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "order_download WHERE order_id = '" . (int)$order_info['parent_order_id'] . "'");
		} else {
		    $order_download_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "order_download WHERE order_id = '" . (int)$order_id . "'");
		}   
		    
		// Gift Voucher
		$this->load->model('checkout/voucher');
		
		if($order_info['parent_order_id']){
		    // Downloads
		    $order_voucher_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "order_voucher WHERE order_id = '" . (int)$order_info['parent_order_id'] . "'");
		} else {
		    $order_voucher_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "order_voucher WHERE order_id = '" . (int)$order_id . "'");
		}
		
		if($order_info['parent_order_id']){
		    // Order Totals			
		    $order_total_query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "order_total` WHERE order_id = '" . (int)$order_info['parent_order_id'] . "' ORDER BY sort_order ASC");
		} else {
		    $order_total_query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "order_total` WHERE order_id = '" . (int)$order_id . "' ORDER BY sort_order ASC");
		}
		
		// Send out order confirmation mail
		$language = new Language($order_info['language_directory']);
		$language->load($order_info['language_filename']);
		$language->load('mail/order');
		
		$order_status_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "order_status WHERE order_status_id = '" . (int)$order_info['order_status_id'] . "' AND language_id = '" . (int)$order_info['language_id'] . "'");
		
		if ($order_status_query->num_rows) {
			$order_status = $order_status_query->row['name'];
		} else {
			$order_status = '';
		}
		
		
		 /***********************************************************
		*	FOR BUYER
		*	HTML Mail	HTML Mail	HTML Mail
		*		HTML Mail	HTML Mail	HTML Mail
		*			HTML Mail	HTML Mail	HTML Mail
		*				HTML Mail	HTML Mail	HTML Mail
		*
		***********************************************************/
	       $this->load->model('account/customer'); // needed for cook
	       
	       $user_info = $this->model_account_customer->getCustomer($order_info['customer_id']);
	       
	       $this->load->model('account/address'); // needed for cook address
	       
	       $cook_address = $this->model_account_address->getAddressesById((int)$order_info['cook_id']);
	       $cook_address = reset($cook_address);
	       
	       $cook_pm_link = $this->url->link('seller/catalog-seller/profile', '&seller_id='.$order_info['cook_id'].'&openpm', 'SSL');
	       
	       // HTML Mail
	       $template = new Template();
	       
	       $template->data['title'] = sprintf($language->get('text_new_subject'), html_entity_decode($order_info['store_name'], ENT_QUOTES, 'UTF-8'), $order_id);
	       
	       // homeals mail greeting
	       setlocale(LC_ALL, 'he_IL.UTF-8');
	       
	       $cook_name = "";
	       
	       $address_1 = html_entity_decode($order_info['payment_address_1'].", ".$order_info['payment_city'], ENT_QUOTES, 'UTF-8');
	       $d_date = strftime("%A, %e ב%B",strtotime($order_info['delivery_date']));
	       $delivery_date = html_entity_decode($d_date, ENT_QUOTES, 'UTF-8');
	       $d_time_int = strftime("%k",strtotime($order_info['delivery_time']));
	       $d_time_format = ( $d_time_int ).":30 - ".( $d_time_int -1 ).":30";
	       $delivery_time = html_entity_decode($d_time_format, ENT_QUOTES, 'UTF-8');
	       
	       $subject = sprintf($language->get('text_new_subject'), $delivery_date);
	       
	       $template->data['cook_info'] = $order_info['cook_id'];
	       
	       $template->data['text_greeting'] = sprintf($language->get('text_homeals_greeting'),$order_info['firstname']);
	       $template->data['text_order_status'] = $language->get('text_order_status');
	       $template->data['order_status'] = "מאושר";//$order_status;
	       $template->data['text_order_id'] = $language->get('text_order_id');
	       $template->data['order_id'] = $order_id;
	       
	       $template->data['text_order_aproved'] = sprintf($language->get('text_order_aproved'),$address_1,$delivery_date,$delivery_time);
	       $template->data['text_order_thank_you'] = $language->get('text_order_thank_you');
	       
	       //delivery details
	       $template->data['text_delivery_details'] = $language->get('text_delivery_details');
	       $template->data['text_order_when'] = $language->get('text_order_when');
	       $template->data['order_when'] = sprintf($language->get('order_when'),$delivery_date,$delivery_time);
	       $template->data['text_order_address'] = $language->get('text_order_address');
	       $address_format = $order_info['payment_address_1'].", ".$order_info['payment_city'].", ".$order_info['payment_address_2'];
	       $template->data['order_address'] = html_entity_decode($address_format, ENT_QUOTES, 'UTF-8');
	       
	       $template->data['text_order_compony'] = $language->get('text_order_compony');
	       $template->data['order_compony'] = $order_info['payment_company'];
	       
	       $template->data['text_order_contact'] = $language->get('text_order_contact');
	       $template->data['order_contact'] = sprintf($language->get('order_contact'),$order_info['payment_firstname']." ".$order_info['payment_lastname']);
	       
	       $template->data['text_order_phone'] = $language->get('text_order_phone');
	       $template->data['order_phone'] = sprintf($language->get('order_phone'),$order_info['payment_postcode']);
	       
	       $template->data['text_order_comment'] = $language->get('text_order_comment');
	       $template->data['order_comment'] = $order_info['comment'];
	       
	       $template->data['text_order_tools'] = $language->get('text_order_tools');
		if($order_info['tools']){
			$template->data['order_tools'] = $language->get('need_tools');
		} else {
			$template->data['order_tools'] = $language->get('dont_need_tools');
		}
		
	       // meal(product) details
	       $template->data['text_meal_details'] = $language->get('text_meal_details');
	       $template->data['text_per_price'] = $language->get('text_per_price');
	       
	       // product need to have picture, name + side names, comment, quantity, price
	       $template->data['text_meal_coment'] = $language->get('text_meal_coment');
	       $template->data['text_quantity'] = $language->get('text_quantity');
	       
	       // order summary
	       $template->data['text_order_summary'] = $language->get('text_order_summary');
	       $template->data['text_meals_total'] = $language->get('text_meals_total');
	       $template->data['text_delivery_cost'] = $language->get('text_delivery_cost');
	       $template->data['text_total'] = $language->get('text_new_total');
	       
	       // fotter
	       $template->data['text_follow_us'] = $language->get('text_follow_us');
	       $template->data['text_homeals_help'] = sprintf($language->get('text_homeals_help'),"03-3741886",$order_info['email']);
	       
	       // end of homeals mail
	       
	       $template->data['text_link'] = $language->get('text_new_link');
	       $template->data['text_download'] = $language->get('text_new_download');
	       $template->data['text_order_detail'] = $language->get('text_new_order_detail');
	       $template->data['text_instruction'] = $language->get('text_new_instruction');
	       
	       $template->data['text_date_added'] = $language->get('text_new_date_added');
	       $template->data['text_payment_method'] = $language->get('text_new_payment_method');	
	       $template->data['text_shipping_method'] = $language->get('text_new_shipping_method');
	       $template->data['text_email'] = $language->get('text_new_email');
	       $template->data['text_telephone'] = $language->get('text_new_telephone');
	       $template->data['text_ip'] = $language->get('text_new_ip');
	       $template->data['text_payment_address'] = $language->get('text_new_payment_address');
	       $template->data['text_shipping_address'] = $language->get('text_new_shipping_address');
	       $template->data['text_product'] = $language->get('text_new_product');
	       $template->data['text_model'] = $language->get('text_new_model');
	       $template->data['text_price'] = $language->get('text_new_price');

	       $template->data['text_footer'] = $language->get('text_new_footer');
	       $template->data['text_powered'] = $language->get('text_new_powered');
	       
	       $template->data['logo'] = $this->config->get('config_url') . 'image/' . $this->config->get('config_logo');		
	       $template->data['store_name'] = $order_info['store_name'];
	       $template->data['store_url'] = $order_info['store_url'];
	       $template->data['customer_id'] = $order_info['customer_id'];
	       $template->data['link'] = $order_info['store_url'] . 'index.php?route=account/order/info&order_id=' . $order_id;
	       
	       $template->data['date_added'] = date($language->get('date_format_short'), strtotime($order_info['date_added']));    	
	       $template->data['payment_method'] = $order_info['payment_method'];
	       $template->data['shipping_method'] = $order_info['shipping_method'];
	       $template->data['email'] = $order_info['email'];
	       $template->data['telephone'] = $order_info['payment_postcode'];
	       $template->data['ip'] = $order_info['ip'];
	       
				       
	       if ($order_info['payment_address_format']) {
		       $format = $order_info['payment_address_format'];
	       } else {
		       $format = '{firstname} {lastname}' . "\n" . '{company}' . "\n" . '{address_1}' . "\n" . '{address_2}' . "\n" . '{city} {postcode}' . "\n" . '{zone}' . "\n" . '{country}';
	       }
	       
	       $find = array(
		       '{firstname}',
		       '{lastname}',
		       '{company}',
		       '{address_1}',
		       '{address_2}',
		       '{city}',
		       '{postcode}',
		       '{zone}',
		       '{zone_code}',
		       '{country}'
	       );
       
	       $replace = array(
		       'firstname' => $order_info['payment_firstname'],
		       'lastname'  => $order_info['payment_lastname'],
		       'company'   => $order_info['payment_company'],
		       'address_1' => $order_info['payment_address_1'],
		       'address_2' => $order_info['payment_address_2'],
		       'city'      => $order_info['payment_city'],
		       'postcode'  => $order_info['payment_postcode'],
		       'zone'      => $order_info['payment_zone'],
		       'zone_code' => $order_info['payment_zone_code'],
		       'country'   => $order_info['payment_country']  
	       );
	       
       
	       $template->data['payment_address'] = str_replace(array("\r\n", "\r", "\n"), '<br />', preg_replace(array("/\s\s+/", "/\r\r+/", "/\n\n+/"), '<br />', trim(str_replace($find, $replace, $format))));						
							       
	       if ($order_info['shipping_address_format']) {
		       $format = $order_info['shipping_address_format'];
	       } else {
		       $format = '{firstname} {lastname}' . "\n" . '{company}' . "\n" . '{address_1}' . "\n" . '{address_2}' . "\n" . '{city} {postcode}' . "\n" . '{zone}' . "\n" . '{country}';
	       }
	       
	       $find = array(
		       '{firstname}',
		       '{lastname}',
		       '{company}',
		       '{address_1}',
		       '{address_2}',
		       '{city}',
		       '{postcode}',
		       '{zone}',
		       '{zone_code}',
		       '{country}'
	       );
       
	       $replace = array(
		       'firstname' => $order_info['shipping_firstname'],
		       'lastname'  => $order_info['shipping_lastname'],
		       'company'   => $order_info['shipping_company'],
		       'address_1' => $order_info['shipping_address_1'],
		       'address_2' => $order_info['shipping_address_2'],
		       'city'      => $order_info['shipping_city'],
		       'postcode'  => $order_info['shipping_postcode'],
		       'zone'      => $order_info['shipping_zone'],
		       'zone_code' => $order_info['shipping_zone_code'],
		       'country'   => $order_info['shipping_country']  
	       );
       
	       $template->data['shipping_address'] = str_replace(array("\r\n", "\r", "\n"), '<br />', preg_replace(array("/\s\s+/", "/\r\r+/", "/\n\n+/"), '<br />', trim(str_replace($find, $replace, $format))));
	       
	       // Products
	       $template->data['products'] = array();
	       
	       if($parent_order_id) {
		       $order_product_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "order_product WHERE order_id = '" . (int)$parent_order_id . "' AND cook_id ='".$order_info['cook_id']."'");
	       }
	       
	       foreach ($order_product_query->rows as $product) {
		       $option_data = array();
		       
		       $order_option_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "order_option WHERE order_id = '" . (int)$order_id . "' AND order_product_id = '" . (int)$product['order_product_id'] . "'");
		       
		       foreach ($order_option_query->rows as $option) {
			       if ($option['type'] != 'file') {
				       $value = $option['value'];
			       } else {
				       $value = utf8_substr($option['value'], 0, utf8_strrpos($option['value'], '.'));
			       }
			       
			       $option_data[] = array(
				       'name'  => $option['name'],
				       'value' => (utf8_strlen($value) > 20 ? utf8_substr($value, 0, 20) . '..' : $value)
			       );					
		       }
		       
		       //homeals added data for side dish
		       $sides_data = array();
		       
		       $sides = array();
		       $sides = explode(",",$product['sides']);
		       
		       foreach ($sides as $side_id){
			       $sides_info = $this->db->query("SELECT pd.name FROM " . DB_PREFIX . "product p LEFT JOIN " . DB_PREFIX . "product_description pd ON p.product_id = pd.product_id WHERE p.product_id = '" . (int)$side_id . "' AND pd.language_id = '" . (int)$order_info['language_id'] . "'");
			       $sides_data[] = $sides_info->row; 
		       }
		       
		       // product image
		       $this->load->model('tool/image');
		       
		       
		       $image_info = $this->db->query("SELECT image FROM " . DB_PREFIX . "product WHERE product_id = '" . (int)$product['product_id'] . "'");
		       
		       $image = $this->model_tool_image->resize($image_info->row['image'], 177, 122); 
		       
		       
			$this->load->model('account/customer'); // needed for cook
			
			$cook_info = $this->model_account_customer->getCustomer($product['cook_id']);
			$cook_pm_link = $this->url->link('seller/catalog-seller/profile', '&seller_id='.$product['cook_id'].'&openpm', 'SSL');
			$cook_name = html_entity_decode($cook_info['firstname'], ENT_QUOTES, 'UTF-8');
			
		       $template->data['products'][] = array(
			       'name'    	=> $product['name'],
			       'cook_name'     	=> $cook_name,
			       'cook_pm_link' 	=> $cook_pm_link,
			       'model'    	=> $product['model'],
			       'image'    	=> $image,
			       'option'   	=> $option_data,
			       'sides'    	=> $sides_data,
			       'quantity' 	=> $product['quantity'],
			       'price'    	=> $this->currency->format($product['price'] + ($this->config->get('config_tax') ? $product['tax'] : 0), $order_info['currency_code'], $order_info['currency_value']),
			       'total'    	=> $this->currency->format($product['total'] + ($this->config->get('config_tax') ? ($product['tax'] * $product['quantity']) : 0), $order_info['currency_code'], $order_info['currency_value'])
		       );
	       }
		
		$template->data['text_cooks'] = $language->get('text_cooks');
		$template->data['text_contactme'] = $language->get('text_contactme');
		
	       $template->data['totals'] = $order_total_query->rows;
	       
	       if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/mail/order.tpl')) {
		       $html = $template->fetch($this->config->get('config_template') . '/template/mail/order.tpl');
	       } else {
		       $html = $template->fetch('default/template/mail/order.tpl');
	       }
	       
	       // Text Mail
	       $text  = sprintf($language->get('text_new_greeting'), html_entity_decode($order_info['store_name'], ENT_QUOTES, 'UTF-8')) . "\n\n";
	       $text .= $language->get('text_new_order_id') . ' ' . $order_id . "\n";
	       $text .= $language->get('text_new_date_added') . ' ' . date($language->get('date_format_short'), strtotime($order_info['date_added'])) . "\n";
	       $text .= $language->get('text_new_order_status') . ' ' . $order_status . "\n\n";
	       
	       // Products
	       $text .= $language->get('text_new_products') . "\n";
	       
	       foreach ($order_product_query->rows as $product) {
		       $text .= $product['quantity'] . 'x ' . $product['name'] . ' (' . $product['model'] . ') ' . html_entity_decode($this->currency->format($product['total'] + ($this->config->get('config_tax') ? ($product['tax'] * $product['quantity']) : 0), $order_info['currency_code'], $order_info['currency_value']), ENT_NOQUOTES, 'UTF-8') . "\n";
		       
		       $order_option_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "order_option WHERE order_id = '" . (int)$order_id . "' AND order_product_id = '" . $product['order_product_id'] . "'");
		       
		       foreach ($order_option_query->rows as $option) {
			       $text .= chr(9) . '-' . $option['name'] . ' ' . (utf8_strlen($option['value']) > 20 ? utf8_substr($option['value'], 0, 20) . '..' : $option['value']) . "\n";
		       }
	       }
				       
	       $text .= "\n";
	       
	       $text .= $language->get('text_new_order_total') . "\n";
	       
	       foreach ($order_total_query->rows as $total) {
		       $text .= $total['title'] . ': ' . html_entity_decode($total['text'], ENT_NOQUOTES, 'UTF-8') . "\n";
	       }			
	       
	       $text .= "\n";
	       
	       if ($order_info['customer_id']) {
		       $text .= $language->get('text_new_link') . "\n";
		       $text .= $order_info['store_url'] . 'index.php?route=account/order/info&order_id=' . $order_id . "\n\n";
	       }
	       
	       // Comment
	       if ($order_info['comment']) {
		       $text .= $language->get('text_new_comment') . "\n\n";
		       $text .= $order_info['comment'] . "\n\n";
	       }

	       $text .= $language->get('text_new_footer') . "\n\n";
	       
		$mail = new Mail(); 
		$mail->protocol = $this->config->get('config_mail_protocol');
		$mail->parameter = $this->config->get('config_mail_parameter');
		$mail->hostname = $this->config->get('config_smtp_host');
		$mail->username = $this->config->get('config_smtp_username');
		$mail->password = $this->config->get('config_smtp_password');
		$mail->port = $this->config->get('config_smtp_port');
		$mail->timeout = $this->config->get('config_smtp_timeout');			
		$mail->setTo($order_info['email']);
		$mail->setFrom($this->config->get('config_email'));
		$mail->setSender($order_info['store_name']);
		$mail->setSubject(html_entity_decode($subject, ENT_QUOTES, 'UTF-8'));
		$mail->setHtml($html);
		//$mail->setText(html_entity_decode($text, ENT_QUOTES, 'UTF-8'));
		$mail->send();	
	}
	
	private function allAproved($order_id){
		
		$allAproved = true;
		
		//check it;
		$orders_status_query = $this->db->query("SELECT order_status_id FROM " . DB_PREFIX . "order WHERE parent_order_id = '" . (int)$order_id . "'");
		
		foreach ($orders_status_query->rows as $order) {
			if($order['order_status_id'] == 1){
				$allAproved = false;
			}
		}
		
		return $allAproved;
	}
	
}

?>