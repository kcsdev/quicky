<?php
class ModelAccountCustomer extends Model {
    
    public function getMail($id){
    $query = $this->db->query("SELECT email FROM oc_customer WHERE customer_id = " . $id);
    return $query->rows;
    }
    
    public function getAllRestaurant(){
    
    $query = $this->db->query("SELECT firstname, lastname FROM oc_customer WHERE customer_group_id = 2");
    return $query->rows;
    }
    
    public function getRestSpecial($name){
    
    $query = $this->db->query("SELECT a.name 
                               FROM oc_option_value_description  a 
                               JOIN oc_option_description d ON d.option_id = a.option_id
                               JOIN oc_option_to_category  b  ON d.option_id = b.option_id
                               JOIN oc_category_description c  ON c.category_id= b.category_id
                               WHERE d.name = 'תוספות להזמנה' AND d.language_id = 2 AND a.language_id = 2 AND c.language_id = 2
                               AND c.name='" . $name . "'");
    return $query->rows;
    
    }
    
    public function getMenuCat(){
    
    $query = $this->db->query("SELECT * FROM oc_option_value_description WHERE option_id = 50 AND language_id = 2 ");
    
   return $query->rows;     
    
    }
    
    public function getDays($id){
    
    $query = $this->db->query("SELECT * FROM oc_customer_days WHERE customer_id = " . $id);
    
    return $query->rows;
    
    }
    
    
    public function getTheCity(){
    
    $query = $this->db->query("SELECT * FROM `oc_option_value_description` WHERE `option_id` = 31 AND `language_id` = 2");

    return $query->rows;
    
    
    }
    
    public function getCustomerName($id){
    
    $query = $this->db->query("SELECT `firstname`, `lastname` FROM `oc_customer` WHERE `customer_id` =" . $id);
    return $query->rows;
    
    
    }
    
    public function getCustomerCity(){
        
    $query = $this->db->query("SELECT city FROM oc_customer WHERE email = '" . $this->customer->getEmail() . "'");
    return $query->rows;
    
    }
    
    public function getRestCity($id){
    $query = $this->db->query("SELECT data FROM oc_customer_cities WHERE customer_id = " . $id);
    
     return $query->rows;
    }
    
    
	public function addCustomer($data) {
		if (isset($data['customer_group_id']) && is_array($this->config->get('config_customer_group_display')) && in_array($data['customer_group_id'], $this->config->get('config_customer_group_display'))) {
			$customer_group_id = $data['customer_group_id'];
		} else {
			$customer_group_id = $this->config->get('config_customer_group_id');
		}
		
		$this->load->model('account/customer_group');
	
		
		$customer_group_info = $this->model_account_customer_group->getCustomerGroup($customer_group_id);
		
		$this->db->query("INSERT INTO " . DB_PREFIX . "customer SET store_id = '" . (int)$this->config->get('config_store_id') . "', firstname = '" . $this->db->escape($data['firstname']) . "', lastname = '" . $this->db->escape($data['lastname']) . "', email = '" . $this->db->escape($data['email']) . "', telephone = '" . $this->db->escape($data['telephone']) . "', fax = '" . $this->db->escape($data['fax']) . "', salt = '" . $this->db->escape($salt = substr(md5(uniqid(rand(), true)), 0, 9)) . "', password = '" . $this->db->escape(sha1($salt . sha1($salt . sha1($data['password'])))) . "', newsletter = '" . (isset($data['newsletter']) ? (int)$data['newsletter'] : 0) . "', customer_group_id = '" . (int)$customer_group_id . "', ip = '" . $this->db->escape($this->request->server['REMOTE_ADDR']) . "', status = '1', approved = '" . (int)!$customer_group_info['approval'] . "', date_added = NOW(), city = '" . $data['city'] .  "'");
      	
		$customer_id = $this->db->getLastId();
			
		/*
		$this->db->query("INSERT INTO " . DB_PREFIX . "address SET customer_id = '" . (int)$customer_id . "', firstname = '" . $this->db->escape($data['firstname']) . "', lastname = '" . $this->db->escape($data['lastname']) . "', company = '" . $this->db->escape($data['company']) . "', company_id = '" . $this->db->escape($data['company_id']) . "', tax_id = '" . $this->db->escape($data['tax_id']) . "', address_1 = '" . $this->db->escape($data['address_1']) . "', address_2 = '" . $this->db->escape($data['address_2']) . "', city = '" . $this->db->escape($data['city']) . "', postcode = '" . $this->db->escape($data['postcode']) . "', country_id = '" . (int)$data['country_id'] . "', zone_id = '" . (int)$data['zone_id'] . "'");
			
			$address_id = $this->db->getLastId();
			
		$this->db->query("UPDATE " . DB_PREFIX . "customer SET address_id = '" . (int)$address_id . "' WHERE customer_id = '" . (int)$customer_id . "'");
		*/
	/*	$this->language->load('mail/customer');
		
		// HTML Mail
		$template = new Template();
		
		// homeals mail greeting
		setlocale(LC_ALL, 'he_IL.UTF-8');
		
		
		$template->data['logo'] = $this->config->get('config_url') . 'image/' . $this->config->get('config_logo');		
		$template->data['store_name'] = "Quicky";
		$template->data['store_url'] = $this->config->get('config_url');
		
		$template->data['title'] = "אנא אשרו את כתובת המייל שלכם ב-Homeals";
		$subject  = "אנא אשרו את כתובת המייל שלכם ב-Homeals";
		
		$template->data['line_1'] = sprintf("שלום %s",$data['firstname']);
		$template->data['line_2'] = "תודה שנרשמת ל-Quicky. בכדי לוודא שתקבל/י הודעות חשובות הקשורות להזמנות שלך באתר, אנא אשר/י את כתובת המייל שלך ע״י לחיצה על הלינק הבא:";
		$template->data['link'] = $this->url->link('account/login/aprovemail', '&q='.urlencode(base64_encode($data['email'])) , 'SSL');
		$template->data['line_3'] = "תודה וברוכים הבאים  <br> צוות Quicky";
		
		
		// fotter
		$template->data['text_follow_us'] = $language->get('text_follow_us');
		$template->data['text_homeals_help'] = sprintf($language->get('text_homeals_help'),"03-3741886",$data['email']);
		$template->data['email'] = $data['email'];
		
		$message = sprintf($this->language->get('text_welcome'), $this->config->get('config_name')) . "\n\n";
		
		$message .= $this->language->get('text_login') . "\n";
		
		$message .= $this->url->link('account/login', '', 'SSL') . "\n\n";
		$message .= $this->language->get('text_services') . "\n\n";
		$message .= $this->language->get('text_thanks') . "\n";
		$message .= $this->config->get('config_name');
		
		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/mail/new_customer.tpl')) {
			$html = $template->fetch($this->config->get('config_template') . '/template/mail/new_customer.tpl');
		} else {
			$html = $template->fetch('default/template/mail/new_customer.tpl');
		}
		
		$mail = new Mail();
		$mail->protocol = $this->config->get('config_mail_protocol');
		$mail->parameter = $this->config->get('config_mail_parameter');
		$mail->hostname = $this->config->get('config_smtp_host');
		$mail->username = $this->config->get('config_smtp_username');
		$mail->password = $this->config->get('config_smtp_password');
		$mail->port = $this->config->get('config_smtp_port');
		$mail->timeout = $this->config->get('config_smtp_timeout');				
		$mail->setTo($data['email']);
		$mail->setFrom($this->config->get('config_email'));
		$mail->setSender($this->config->get('config_name'));
		$mail->setSubject(html_entity_decode($subject, ENT_QUOTES, 'UTF-8'));
		$mail->setHtml($html);
		$mail->setText(html_entity_decode($message, ENT_QUOTES, 'UTF-8'));
		$mail->send();*/
		/************************************************
		 *	RAV MESER
		 ***********************************************/
		
		setlocale(LC_ALL, 'he_IL.UTF-8');
		    
		# include the libraries needed to make the REST API requests
		include DIR_TEMPLATE .'default/rav/OAuth.php';
		include DIR_TEMPLATE .'default/rav/responder_sdk.php';
		
		# Tokens; fill with the tokens acquired from the responder support team
		$client_key = '80596FBC05FCA1E2E846F33E7EE6556C';
		$client_secret = '188954282EA299B41995A7C4F0A9BE6D';
		
		$user_key = 'DBC1448D51BFD73B3AEF3DD2A6E39FA4';
		$user_secret = '4A0B30285DE590299A1DCB2064B5570B';
		
		# create the responder request instance
		$responder = new ResponderOAuth($client_key, $client_secret, $user_key, $user_secret);
		
		# the data passed with the request (not needed with GET method)
		$post_data = array(
			'subscribers' => json_encode(
				array(
					array(
						'EMAIL' => $data['email'],
						'NAME' => $data['firstname']." ".$data['lastname']
					)
				)
			)
		);
		
		# execute the request
		$response = $responder->http_request('lists/86383/subscribers', 'post', $post_data);
		
		/************************************************
		 *	RAV MESER END
		 ***********************************************/
		
		
		
		// Send to main admin email if new account email is enabled
		if ($this->config->get('config_account_mail')) {
			$message  = $this->language->get('text_signup') . "\n\n";
			$message .= $this->language->get('text_website') . ' ' . $this->config->get('config_name') . "\n";
			$message .= $this->language->get('text_firstname') . ' ' . $data['firstname'] . "\n";
			$message .= $this->language->get('text_lastname') . ' ' . $data['lastname'] . "\n";
			$message .= $this->language->get('text_customer_group') . ' ' . $customer_group_info['name'] . "\n";
			
			if ($data['company']) {
				$message .= $this->language->get('text_company') . ' '  . $data['company'] . "\n";
			}
			
			$message .= $this->language->get('text_email') . ' '  .  $data['email'] . "\n";
			$message .= $this->language->get('text_telephone') . ' ' . $data['telephone'] . "\n";
			
			$mail->setTo($this->config->get('config_email'));
			$mail->setSubject(html_entity_decode($this->language->get('text_new_customer'), ENT_QUOTES, 'UTF-8'));
			$mail->setText(html_entity_decode($message, ENT_QUOTES, 'UTF-8'));
			$mail->send();
			
			// Send to additional alert emails if new account email is enabled
			$emails = explode(',', $this->config->get('config_alert_emails'));
			
			foreach ($emails as $email) {
				if (strlen($email) > 0 && preg_match('/^[^\@]+@.*\.[a-z]{2,6}$/i', $email)) {
					$mail->setTo($email);
					$mail->send();
				}
			}
		}
		
	
	}
    
    public function getMenu($id){
    $menu = $this->db->query("SELECT data FROM oc_menu_categories WHERE customer_id='" . $id . "'");
 
    return $menu->row;
    
    }
	
	public function addCustomerAsWell($data) {
	    
		if (isset($this->request->post['customer_group_id'])) {
		    $customer_group_id = $this->request->post['customer_group_id'];
		} else {
		    $customer_group_id = $this->config->get('config_customer_group_id');
		}
		
		$this->db->query("INSERT INTO " . DB_PREFIX . "customer SET store_id = '0', firstname = '" . $this->db->escape($data['firstname']) . "', lastname = '" . $this->db->escape($data['lastname']) . "', email = '" . $this->db->escape($data['email']) . "', telephone = ' ', fax = ' ', salt = '" . $this->db->escape($salt = substr(md5(uniqid(rand(), true)), 0, 9)) . "', password = '" . $this->db->escape(sha1($salt . sha1($salt . sha1($data['password'])))) . "', newsletter = '0', customer_group_id = '". (int)$customer_group_id ."', ip = '" . $this->db->escape($this->request->server['REMOTE_ADDR']) . "', status = '1', approved = '1', date_added = NOW()");
      	
		$customer_id = $this->db->getLastId();
			
		// only some
		$this->db->query("INSERT INTO " . DB_PREFIX . "address SET customer_id = '" . (int)$customer_id . "', firstname = '" . $this->db->escape($data['firstname']) . "', lastname = '" . $this->db->escape($data['lastname']) . "', city = '" . $this->db->escape($data['city']) . "'");
		// all
		//$this->db->query("INSERT INTO " . DB_PREFIX . "address SET customer_id = '" . (int)$customer_id . "', firstname = '" . $this->db->escape($data['firstname']) . "', lastname = '" . $this->db->escape($data['lastname']) . "', company = '" . $this->db->escape($data['company']) . "', company_id = '" . $this->db->escape($data['company_id']) . "', tax_id = '" . $this->db->escape($data['tax_id']) . "', address_1 = '" . $this->db->escape($data['address_1']) . "', address_2 = '" . $this->db->escape($data['address_2']) . "', city = '" . $this->db->escape($data['city']) . "', postcode = '" . $this->db->escape($data['postcode']) . "', country_id = '" . (int)$data['country_id'] . "', zone_id = '" . (int)$data['zone_id'] . "'");
		
		$address_id = $this->db->getLastId();
		
		$this->db->query("UPDATE " . DB_PREFIX . "customer SET address_id = '" . (int)$address_id . "' WHERE customer_id = '" . (int)$customer_id . "'");
		
		
		$language = new Language('hebrew');
		$language->load('mail/order');
		$language->load('mail/customer');
		
		// HTML Mail
		$template = new Template();
		
		// homeals mail greeting
		setlocale(LC_ALL, 'he_IL.UTF-8');
		
		$template->data['logo'] = $this->config->get('config_url') . 'image/' . $this->config->get('config_logo');		
		$template->data['store_name'] = "Homeals";
		$template->data['store_url'] = $this->config->get('config_url');
		
		$template->data['title'] = "אנא אשרו את כתובת המייל שלכם ב-Homeals";
		
		$subject  = "אנא אשרו את כתובת המייל שלכם ב-Homeals";
		
		$template->data['line_1'] = sprintf("שלום %s",$data['firstname']);
		$template->data['line_2'] = "תודה שנרשמת ל-Homeals. בכדי לוודא שתקבל/י הודעות חשובות הקשורות להזמנות שלך באתר, אנא אשר/י את כתובת המייל שלך ע״י לחיצה על הלינק הבא:";
		$template->data['link'] = $this->url->link('account/login/aprovemail', '&q='.urlencode(base64_encode($data['email'])) , 'SSL');
		$template->data['line_3'] = "תודה וברוכים הבאים  <br> צוות Homeals";
		
		
		// fotter
		$template->data['text_follow_us'] = "עקבו אחרינו";
		$template->data['text_homeals_help'] = sprintf($language->get('text_homeals_help'),"03-3741886",$data['email']);
		$template->data['email'] = $data['email'];
			
		$message = sprintf($this->language->get('text_welcome'), $this->config->get('config_name')) . "\n\n";
		
		$message .= $this->language->get('text_login') . "\n";
		
		$message .= $this->url->link('account/login', '', 'SSL') . "\n\n";
		$message .= $this->language->get('text_services') . "\n\n";
		$message .= $this->language->get('text_thanks') . "\n";
		$message .= $this->config->get('config_name');
		
		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/mail/new_customer.tpl')) {
			$html = $template->fetch($this->config->get('config_template') . '/template/mail/new_customer.tpl');
		} else {
			$html = $template->fetch('default/template/mail/new_customer.tpl');
		}
		
		$mail = new Mail();
		$mail->protocol = $this->config->get('config_mail_protocol');
		$mail->parameter = $this->config->get('config_mail_parameter');
		$mail->hostname = $this->config->get('config_smtp_host');
		$mail->username = $this->config->get('config_smtp_username');
		$mail->password = $this->config->get('config_smtp_password');
		$mail->port = $this->config->get('config_smtp_port');
		$mail->timeout = $this->config->get('config_smtp_timeout');				
		$mail->setTo($data['email']);
		$mail->setFrom($this->config->get('config_email'));
		$mail->setSender($this->config->get('config_name'));
		$mail->setSubject(html_entity_decode($subject, ENT_QUOTES, 'UTF-8'));
		$mail->setHtml($html);
		$mail->setText(html_entity_decode($message, ENT_QUOTES, 'UTF-8'));
		$mail->send();
		
		// subscribe to mailchimp mailing list
		// join_list is the variable coming from the join list checkbox at regestrition form
		if ($data['join_list'] == "true") {
			$data = array (
				'want_in' => 1,
				'email' => $data['email'],
				'city' => $data['city'],
				'firstname' => $data['firstname'],
				'lastname' => $data['lastname'],
			);
		} else {
			$data = array (
				'want_in' => 0,// want in?
				'email' => $data['email'],// city
				'city' => $data['city'],// email
				'firstname' => $data['firstname'],// first name
				'lastname' => $data['lastname'],// last name
			);
		}
		
		$this->mailChimp($data);
		// subscribe to mailchimp mailing list end
		
		$this->session->data['first_time'] = true;
		
	}
	
	
	
	public function addCustomer_service($data) {
		if (isset($data['customer_group_id'])) {
			$customer_group_id = $data['customer_group_id'];
		} else {
			$customer_group_id = $this->config->get('config_customer_group_id');
		}
		
		$this->load->model('account/customer_group');
		
		$customer_group_info = $this->model_account_customer_group->getCustomerGroup($customer_group_id);
		
		$this->db->query("INSERT INTO " . DB_PREFIX . "customer SET store_id = '" . (int)$this->config->get('config_store_id') . "', firstname = '" . $this->db->escape($data['firstname']) . "', lastname = '" . $this->db->escape($data['lastname']) . "', email = '" . $this->db->escape($data['email']) . "', telephone = '" . $this->db->escape($data['telephone']) . "', fax = '" . $this->db->escape($data['fax']) . "', salt = '" . $this->db->escape($salt = substr(md5(uniqid(rand(), true)), 0, 9)) . "', password = '" . $this->db->escape(sha1($salt . sha1($salt . sha1($data['password'])))) . "', newsletter = '" . (isset($data['newsletter']) ? (int)$data['newsletter'] : 0) . "', customer_group_id = '" . (int)$customer_group_id . "', ip = '" . $this->db->escape($this->request->server['REMOTE_ADDR']) . "', status = '1', approved = '" . (int)!$customer_group_info['approval'] . "', date_added = NOW()");
		
		$customer_id = $this->db->getLastId();
		
		
		if ($_FILES["file"]["tmp_name"]) {
			move_uploaded_file($_FILES["file"]["tmp_name"],"/home/c14/public_html/z/uploads/users/upload/img_".$customer_id.".jpg");	
		}
		
		/*
		if (isset($data['address_1'])) {
			$this->db->query("INSERT INTO " . DB_PREFIX . "address SET customer_id = '" . (int)$customer_id . "', firstname = '" . $this->db->escape($data['firstname']) . "', lastname = '" . $this->db->escape($data['lastname']) . "', company = '" . $this->db->escape($data['company']) . "', company_id = '" . $this->db->escape($data['company_id']) . "', tax_id = '" . $this->db->escape($data['tax_id']) . "', address_1 = '" . $this->db->escape($data['address_1']) . "', address_2 = '" . $this->db->escape($data['address_2']) . "', city = '" . $this->db->escape($data['city']) . "', postcode = '" . $this->db->escape($data['postcode']) . "', country_id = '" . (int)$data['country_id'] . "', zone_id = '" . (int)$data['zone_id'] . "'");
			$address_id = $this->db->getLastId();
			$this->db->query("UPDATE " . DB_PREFIX . "customer SET address_id = '" . (int)$address_id . "' WHERE customer_id = '" . (int)$customer_id . "'");
		}
		*/
		
		/************************************************
		 *	RAV MESER
		 ***********************************************/
		
		setlocale(LC_ALL, 'he_IL.UTF-8');
		    
		# include the libraries needed to make the REST API requests
		include DIR_TEMPLATE .'default/rav/OAuth.php';
		include DIR_TEMPLATE .'default/rav/responder_sdk.php';
		
		# Tokens; fill with the tokens acquired from the responder support team
		$client_key = '80596FBC05FCA1E2E846F33E7EE6556C';
		$client_secret = '188954282EA299B41995A7C4F0A9BE6D';
		
		$user_key = 'DBC1448D51BFD73B3AEF3DD2A6E39FA4';
		$user_secret = '4A0B30285DE590299A1DCB2064B5570B';
		
		# create the responder request instance
		$responder = new ResponderOAuth($client_key, $client_secret, $user_key, $user_secret);
		
		# the data passed with the request (not needed with GET method)
		$post_data = array(
			'subscribers' => json_encode(
				array(
					array(
						'EMAIL' => $data['email'],
						'NAME' => $data['firstname']." ".$data['lastname']
					)
				)
			)
		);
		
		# execute the request
		$response = $responder->http_request('lists/86383/subscribers', 'post', $post_data);
		
		/************************************************
		 *	RAV MESER END
		 ***********************************************/
		
		if ($customer_id) return true;
	}
	
	public function mailChimp($data) {
		
		$MailChimp = new \Drewm\MailChimp('3d058d8d6da676724f993faafba9c34c-us8');
		
		$homeals_tlv = '43b8806f12';// 43b8806f12 Homeals Tel Aviv
		$homeals_il = 'b284333344';// b284333344 Homeals IL Users
		
		if($data['want_in']){
		    $list_id = $homeals_tlv;
		} else {
		    $list_id = $homeals_il;
		}
		
		$result = $MailChimp->call('lists/subscribe', array(
				'id'                => $list_id,
				'email'             => array('email'=>$data['email']),
				'merge_vars'        => array('FNAME'=>$data['firstname'],
							     'LNAME'=>$data['lastname'],
							     'CITY' => $data['city'],
							     'METHOD' => "New User Signup",
							     'optin_ip' => $_SERVER['REMOTE_ADDR'],
							     'optin_time' => date("Y-m-d H:i:s")),
				'double_optin'      => false,
				'update_existing'   => true,
				'replace_interests' => false,
				'send_welcome'      => false,
			    ));
		
		if($result){
			return true;
		} else {
			return "failed";
		}
	}
	
	public function editCustomer($data) {
		$this->db->query("UPDATE " . DB_PREFIX . "customer SET firstname = '" . $this->db->escape($data['firstname']) . "', lastname = '" . $this->db->escape($data['lastname']) . "', email = '" . $this->db->escape($data['email']) . "', telephone = '" . $this->db->escape($data['telephone']) . "', fax = '" . $this->db->escape($data['fax']) . "' WHERE customer_id = '" . (int)$this->customer->getId() . "'");
	}

	public function editPassword($email, $password) {
	    $this->db->query("UPDATE " . DB_PREFIX . "customer SET salt = '" . $this->db->escape($salt = substr(md5(uniqid(rand(), true)), 0, 9)) . "', password = '" . $this->db->escape(sha1($salt . sha1($salt . sha1($password)))) . "' WHERE LOWER(email) = '" . $this->db->escape(utf8_strtolower($email)) . "'");
	}

	public function editNewsletter($newsletter) {
		$this->db->query("UPDATE " . DB_PREFIX . "customer SET newsletter = '" . (int)$newsletter . "' WHERE customer_id = '" . (int)$this->customer->getId() . "'");
	}
					
	public function getCustomer($customer_id) {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "customer WHERE customer_id = '" . (int)$customer_id . "'");
		
		return $query->row;
	}
	
	public function getCustomerByEmail($email) {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "customer WHERE LOWER(email) = '" . $this->db->escape(utf8_strtolower($email)) . "'");
		
		return $query->row;
		
	}
	
	public function ApproveEmail($email) {
		$query = $this->db->query("UPDATE " . DB_PREFIX . "customer SET sms_aproved = '1' WHERE LOWER(email) = '" . $this->db->escape(utf8_strtolower($email)) . "'");
		
	}
		
	public function getCustomerByToken($token) {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "customer WHERE token = '" . $this->db->escape($token) . "' AND token != ''");
		
		$this->db->query("UPDATE " . DB_PREFIX . "customer SET token = ''");
		
		return $query->row;
	}
		
	public function getCustomers($data = array()) {
		$sql = "SELECT *, CONCAT(c.firstname, ' ', c.lastname) AS name, cg.name AS customer_group FROM " . DB_PREFIX . "customer c LEFT JOIN ( ";
		
		$sql .= "SELECT cg.*,cgd.name,cgd.description FROM " . DB_PREFIX . "customer_group cg LEFT JOIN " . DB_PREFIX . "customer_group_description cgd ON (cg.customer_group_id = cgd.customer_group_id) WHERE cgd.language_id = '" . (int)$this->config->get('config_language_id') . "' ORDER BY cg.sort_order ASC, cgd.name ASC";
		
		$sql .= " ) cg ON (c.customer_group_id = cg.customer_group_id) ";

		$implode = array();
		
		if (isset($data['filter_name']) && !is_null($data['filter_name'])) {
			$implode[] = "LCASE(CONCAT(c.firstname, ' ', c.lastname)) LIKE '" . $this->db->escape(utf8_strtolower($data['filter_name'])) . "%'";
		}
		
		if (isset($data['filter_email']) && !is_null($data['filter_email'])) {
			$implode[] = "LCASE(c.email) = '" . $this->db->escape(utf8_strtolower($data['filter_email'])) . "'";
		}
		
		if (isset($data['filter_customer_group_id']) && !is_null($data['filter_customer_group_id'])) {
			$implode[] = "cg.customer_group_id = '" . $this->db->escape($data['filter_customer_group_id']) . "'";
		}	
		
		if (isset($data['filter_status']) && !is_null($data['filter_status'])) {
			$implode[] = "c.status = '" . (int)$data['filter_status'] . "'";
		}	
		
		if (isset($data['filter_approved']) && !is_null($data['filter_approved'])) {
			$implode[] = "c.approved = '" . (int)$data['filter_approved'] . "'";
		}	
			
		if (isset($data['filter_ip']) && !is_null($data['filter_ip'])) {
			$implode[] = "c.customer_id IN (SELECT customer_id FROM " . DB_PREFIX . "customer_ip WHERE ip = '" . $this->db->escape($data['filter_ip']) . "')";
		}	
				
		if (isset($data['filter_date_added']) && !is_null($data['filter_date_added'])) {
			$implode[] = "DATE(c.date_added) = DATE('" . $this->db->escape($data['filter_date_added']) . "')";
		}
		
		if ($implode) {
			$sql .= " WHERE " . implode(" AND ", $implode);
		}
		
		$sort_data = array(
			'name',
			'c.email',
			'customer_group',
			'c.status',
			'c.ip',
			'c.date_added'
		);	
			
		if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
			$sql .= " ORDER BY " . $data['sort'];	
		} else {
			$sql .= " ORDER BY name";	
		}
			
		if (isset($data['order']) && ($data['order'] == 'DESC')) {
			$sql .= " DESC";
		} else {
			$sql .= " ASC";
		}
		
		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}			

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}	
			
			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}		
		
		$query = $this->db->query($sql);
		
		return $query->rows;	
	}
		
	public function getTotalCustomersByEmail($email) {
		$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "customer WHERE LOWER(email) = '" . $this->db->escape(utf8_strtolower($email)) . "'");
		
		return $query->row['total'];
	}
	
	public function getIps($customer_id) {
		$query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "customer_ip` WHERE customer_id = '" . (int)$customer_id . "'");
		
		return $query->rows;
	}	
	
	public function isBanIp($ip) {
		$query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "customer_ban_ip` WHERE ip = '" . $this->db->escape($ip) . "'");
		
		return $query->num_rows;
	}
	
	public function getCustomerImages($customer_id) {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "customer_image WHERE customer_id = '" . (int)$customer_id . "' ORDER BY sort_order ASC");

		return $query->rows;
	}
    
    public function getManiImage ($customer_id){
        		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "customer_image WHERE customer_id = '" . (int)$customer_id . "' LIMIT 1");

		return $query->rows;
    
      }
}
?>
