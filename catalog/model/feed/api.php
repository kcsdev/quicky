<?php
#####################################################################################
#  API for Opencart 1.5.x From HostJars http://opencart.hostjars.com    			#
#####################################################################################


class ModelFeedApi extends Model {

	//You can put any DB calls you like in here to post/delete/put/get
	
	// TODO - complete the user add and edit in the API.
	
	public function CustomerLogin() {
		if (!$this->customer->login($this->request->get['email'], $this->request->get['password'])) {
      		echo "no";
		} else {
			echo "yes";
		}
	}
	
	
	public function newCustomer() {
		//$customer=array("firstname"=>"aaaa","lastname"=>"bbbbbbb","email"=>"asda33sd@sgf.kl","password"=>"123456", "telephone"=>"04523452","fax"=>"034524353","status"=>0,"customer_group_id"=>1,"newsletter"=>0);
		//$this->request->post=$customer;
		//print_r($this->request->post);
		
		$validate=$this->validate();
		
		if($validate==1) {
			if ($this->model_account_customer->addCustomer_service($this->request->get)) {
				echo 'Success';
			}
		} else{
			echo $validate[0];
		}
		
	}
	
	protected function validate() {
    	$errors=array();
		$this->language->load('account/register');
		if ((utf8_strlen($this->request->get['firstname']) < 1) || (utf8_strlen($this->request->get['firstname']) > 32)) {
      		//$this->error['firstname'] = $this->language->get('error_firstname');
			$errors[]=$this->language->get('error_firstname');
    	}

    	if ((utf8_strlen($this->request->get['lastname']) < 1) || (utf8_strlen($this->request->get['lastname']) > 32)) {
      		$errors[]= $this->language->get('error_lastname');
    	}

    	if ((utf8_strlen($this->request->get['email']) > 96) || !preg_match('/^[^\@]+@.*\.[a-z]{2,6}$/i', $this->request->get['email'])) {
      		$errors[]=$this->language->get('error_email');

    	}

    	if ($this->model_account_customer->getTotalCustomersByEmail($this->request->get['email'])) {
      		$errors[]= $this->language->get('error_exists');

    	}
		
    	if ((utf8_strlen($this->request->get['telephone']) < 3) || (utf8_strlen($this->request->get['telephone']) > 32)) {
      		$errors[]= $this->language->get('error_telephone');

    	}
		
		// Customer Group
		$this->load->model('account/customer_group');
		
		if (isset($this->request->get['customer_group_id']) && is_array($this->config->get('config_customer_group_display')) && in_array($this->request->get['customer_group_id'], $this->config->get('config_customer_group_display'))) {
			$customer_group_id = $this->request->get['customer_group_id'];
		} else {
			$customer_group_id = $this->config->get('config_customer_group_id');
		}
	
		$customer_group = $this->model_account_customer_group->getCustomerGroup($customer_group_id);
			
		if ($customer_group) {	
			// Company ID
			if ($customer_group['company_id_display'] && $customer_group['company_id_required'] && empty($this->request->get['company_id'])) {
				$errors[]= $this->language->get('error_company_id');
			}
			
			// Tax ID 
			if ($customer_group['tax_id_display'] && $customer_group['tax_id_required'] && empty($this->request->get['tax_id'])) {
				$errors[]= $this->language->get('error_tax_id');
			}						
		}
			
	
	
		if ((utf8_strlen($this->request->get['password']) < 4) || (utf8_strlen($this->request->get['password']) > 20)) {
				$errors[]= $this->language->get('error_password');
		}
		if (count($errors)>0){
	
			return $errors;
		} else {
			return 1;
		}
    	
  	}
	
	
	
	
	
	
	 
	public function editCustomer() {

	}	 
	 
			

	public function getCustomers() {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "customer");
		
		return $query->row;
	}
	
	 
	public function getOrders() {
		
		$query = $this->db->query("SELECT o.order_id, o.firstname, o.lastname, os.name as status, o.date_added, o.total, o.currency_code, o.currency_value FROM `" . DB_PREFIX . "order` o LEFT JOIN " . DB_PREFIX . "order_status os ON (o.order_status_id = os.order_status_id) WHERE o.customer_id = '" . (int)$this->customer->getId() . "' AND o.order_status_id > '0' AND os.language_id = '" . (int)$this->config->get('config_language_id') . "'");	
	
		return $query->rows;
	}
	
	public function deleteProduct($product_id) {
		$query = $this->db->query("DELETE FROM " . DB_PREFIX . "product WHERE product_id='" . $this->db->escape($product_id) . "'");
		$query = $this->db->query("DELETE FROM " . DB_PREFIX . "product_to_store WHERE product_id='" . $this->db->escape($product_id) . "'");
		$query = $this->db->query("DELETE FROM " . DB_PREFIX . "product_description WHERE product_id='" . $this->db->escape($product_id) . "'");
		$query = $this->db->query("DELETE FROM " . DB_PREFIX . "product_to_category WHERE product_id='" . $this->db->escape($product_id) . "'");
		$query = $this->db->query("DELETE FROM " . DB_PREFIX . "review WHERE product_id='" . $this->db->escape($product_id) . "'");
	}
	
	
	public function forgotPassword() {
		$this->language->load('account/forgotten');
		$this->load->model('account/customer');
		
		$errors="";
		if (!isset($this->request->get['email'])) {
			$errors = $this->language->get('error_email');
		} elseif (!$this->model_account_customer->getTotalCustomersByEmail($this->request->get['email'])) {
			$errors = $this->language->get('error_email');
		}
		if ($errors) {
			echo $errors;
			return;
		}
		
		
		$this->language->load('mail/forgotten');
		$password = substr(sha1(uniqid(mt_rand(), true)), 0, 10);
			
		$this->model_account_customer->editPassword($this->request->get['email'], $password);
			
		$subject = sprintf($this->language->get('text_subject'), $this->config->get('config_name'));
			
		$message  = sprintf($this->language->get('text_greeting'), $this->config->get('config_name')) . "\n\n";
		$message .= $this->language->get('text_password') . "\n\n";
		$message .= $password;

		$mail = new Mail();
		$mail->protocol = $this->config->get('config_mail_protocol');
		$mail->parameter = $this->config->get('config_mail_parameter');
		$mail->hostname = $this->config->get('config_smtp_host');
		$mail->username = $this->config->get('config_smtp_username');
		$mail->password = $this->config->get('config_smtp_password');
		$mail->port = $this->config->get('config_smtp_port');
		$mail->timeout = $this->config->get('config_smtp_timeout');				
		$mail->setTo($this->request->get['email']);
		$mail->setFrom($this->config->get('config_email'));
		$mail->setSender($this->config->get('config_name'));
		$mail->setSubject(html_entity_decode($subject, ENT_QUOTES, 'UTF-8'));
		$mail->setText(html_entity_decode($message, ENT_QUOTES, 'UTF-8'));
		$mail->send();
		
		$this->session->data['success'] = $this->language->get('text_success');
		echo "password changed";
		return true;
	}
	
	
	
	
	

}
?>