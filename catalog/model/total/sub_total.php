<?php
class ModelTotalSubTotal extends Model {
	public function getTotal(&$total_data, &$total, &$taxes) {
		$this->language->load('total/sub_total');
		
		$sub_total = $this->cart->getSubTotal();
		
		$tax_total = $this->cart->getTaxes();
		$tax_value = 0; // initialize tax;
		foreach($tax_total as $value){
		 $tax_value = $tax_value + $value;
		}
		
		if (isset($this->session->data['vouchers']) && $this->session->data['vouchers']) {
			foreach ($this->session->data['vouchers'] as $voucher) {
				$sub_total += $voucher['amount'];
			}
		}
		
		$total_data[] = array( 
			'code'       => 'sub_total',
			'title'      => $this->language->get('text_sub_total'),
			'text'       => $this->currency->format($sub_total + $tax_value),
			'value'      => $sub_total + $tax_value,
			'sort_order' => $this->config->get('sub_total_sort_order')
		);
		
		$total += ($sub_total + $tax_value);
	}
}
?>