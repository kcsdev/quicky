<?php
class ModelCatalogAvailability extends Model {
	
	public function addAvailabilitys($product_id,$data) {
		foreach ($data as $availability){
			
			if(isset($data['day_time'])){
				$data['day_time'] = '';
			}
			
			if(isset($data['by_start_time'])){
				$data['by_start_time'] = 0;
			}
			
			$this->db->query("INSERT INTO " . DB_PREFIX . "product_availability SET quantity = '" . (int)$data['quantity'] . "', product_id = '" . (int)$product_id . "', date = '" . $data['date'] . "', last_order_time = '" . (int)$data['last_order_time'] . "', day = '" . $data['day'] . "', day_time = '" . $data['day_time'] . "', end_time = '" . $data['end_time'] . "', start_time = '" . $data['start_time'] . "', by_start_time = '" . $data['by_start_time'] . "', type = '" . $data['type'] . "'");	
		}
		
	}

	public function editAvailability($availability_id, $data) {
		
		if(isset($data['day_time'])){
			$data['day_time'] = '';
		}
		
		$this->db->query("UPDATE " . DB_PREFIX . "product_availability SET quantity = '" . (int)$data['quantity'] . "', product_id = '" . (int)$data['product_id'] . "', date = '" . $data['date'] . "', last_order_time = '" . (int)$data['last_order_time'] . "', day = '" . $data['day'] . "', day_time = '" . $data['day_time'] . "', end_time = '" . $data['end_time'] . "', start_time = '" . $data['start_time'] . "', by_start_time = '" . $data['by_start_time'] . "' WHERE availability_id = '" . (int)$availability_id . "'");
		
	}
	
	public function deleteAvailability($availability_id) {
		$this->db->query("DELETE FROM " . DB_PREFIX . "product_availability WHERE availability_id = '" . (int)$availability_id . "'");
	}
		
	public function getAvailability($product_id,$date) {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "product_availability WHERE product_id = '" . (int)$product_id . "' AND date = '". $date ."'");
		
		return $query->row;
	}
	
	public function getDefAvailabilitys($product_id,$data = array()) {
		
		$data['def'] = true; 
		
		return $this->getAvailabilitys($product_id,$data);
	}
	
	public function getDaysAvailability($product_id) {
		$sql = "SELECT * FROM " . DB_PREFIX . "product_availability a WHERE a.product_id='". (int)$product_id ."' AND a.quantity != 0 AND (a.date = '0000-00-00')";
		
		$query = $this->db->query($sql);
		
		$days_array = array();
		
		setlocale(LC_ALL, 'he_IL.UTF-8');
		
		foreach ($query->rows as $result) {
			
			if($result['type'] == 'custom'){
				$day_name = strftime("%a",strtotime($result['date']));
				$day_num = strftime("%w",strtotime($result['date']));
			} else {
				$day_name = strftime("%a",strtotime($result['day']));
				$day_num = strftime("%w",strtotime($result['day']));
			}
			
			$days_array[$day_num] = $day_name;
			
		}
		
		return $days_array;
		
	}
	

	public function getAvailabilitys($product_id,$data = array()) {
		$sql = "SELECT * FROM " . DB_PREFIX . "product_availability WHERE product_id='". (int)$product_id ."' ";
		
		$sort_data = array(
			'date',
			'availability_id'
		);
		
		if (isset($data['def'])) {
			$sql .= " AND  type = 'def'";
		} else {
			$sql .= " AND  type = 'custom'";
		}
		
		if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
			$sql .= " ORDER BY " . $data['sort'];	
		} else {
			$sql .= " ORDER BY date, availability_id";	
		}	
		
		if (isset($data['order']) && ($data['order'] == 'DESC')) {
			$sql .= " DESC";
		} else {
			$sql .= " ASC";
		}
		
		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}				

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}	
		
			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}
		
		
		
		$query = $this->db->query($sql);
		
		return $query->rows;
	}
		
	
	public function getTotalAvailabilitys() {
      	$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "product_availability");
		
		return $query->row['total'];
	}	

}
?>