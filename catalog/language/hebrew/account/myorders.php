<?php
// Heading
$_['page_title'] = 'הזמנות שהתקבלו';
$_['your_orders_page_title'] = 'ההזמנות שלך';

// Text
$_['text_your_orders']  = 'הזמנות שהתקבלו';
$_['text_recived_orders'] = 'הזמנות פעילות';
$_['text_history_orders'] = 'היסטוריה';
$_['text_wating_orders']    = 'יש לך הזמנות הממתינות לאישור';


$_['text_order_details_link']    = 'פרטי ההזמנה';

$_['cul_num']  = "מס'";
$_['cul_who']    = 'סועד';
$_['cul_meals']  = 'ארוחות בהזמנה';
$_['cul_time']    = 'זמן איסוף';
$_['cul_time_delivery']    = 'זמן משלוח';
$_['cul_total']  = 'סה"כ';
$_['cul_status']    = 'סטטוס';
$_['cul_who_cook']    = 'בשלן';

$_['text_success']  = 'הצלחה';
$_['text_account']    = 'חשבון';
$_['text_success']  = 'הצלחה';

?>