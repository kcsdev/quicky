<?php
// Heading 
$_['heading_title']  = 'שינוי סיסמה';

// Text
$_['text_account']    = 'חשבון';
$_['text_password']  = 'הסיסמה שלך';
$_['text_success']   = 'הסיסמה שלך עודכנה בהצלחה.';

// Entry
$_['entry_my_password'] = 'סיסמה נוכחית';
$_['entry_password'] = 'סיסמה חדשה';
$_['entry_confirm']  = 'אימות הסיסמה';

$_['entry_forgot_my_password'] = 'שכחת את הסיסמה?';

// Error
$_['error_password']       = 'הסיסמה חייבת להכיל בין 3 ל-20 תווים!';
$_['error_confirm']        = 'שתי הסיסמאות שהוקלדו אינן תואמות זו לזו!';
$_['error_wrong_old_pass']        = 'הסיסמה נוכחית שהכנסת איננה תואמת לסיסמתך השמורה באתר!';







?>