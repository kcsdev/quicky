<?php
// Heading
$_['heading_title'] = 'סוג ארוחה';

$_['filter_kitchen'] = 'סינון לפי סוג מטבח';
$_['filter_kitchen_placeholder'] = 'מטבח אסייאתי';

$_['filter_by_cost'] = 'סינון על פי מינמום הזמנה';
$_['filter_by_cost_placeholder'] = 'מהסכום הנמוך לגבוה';

$_['filter_by_kosher'] = 'סינון על פי כשרות';
$_['filter_by_kosher_placeholder'] = 'כל המסעדות';

$_['find_order'] = 'מצא הזמנה';

?>