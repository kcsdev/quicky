<?php
// Locale
$_['code']                  = 'he';
$_['direction']             = 'rtl';
$_['date_format_short']     = 'd/m/Y';
$_['date_format_long']      = 'l dS F Y';
$_['date_format_long_strf'] = '%e %B, %Y';
$_['date_time_format_strf'] = '%A, %e ב%B';
$_['time_format']           = 'H:i:s';
$_['decimal_point']         = '.';
$_['thousand_point']        = ',';

$_['date_strf'] = '%e ב%B';
$_['date_strf_cart'] = '%A, %e ב%B';
$_['date_strf_message'] = 'יום %A, %e ב%B, %R';

// Text
$_['text_home']             = 'דף בית';
$_['text_yes']              = 'כן';
$_['text_no']               = 'לא';
$_['text_none']             = ' --- לא נבחר --- ';
$_['text_select']           = ' --- נא לבחור --- ';
$_['text_all_zones']        = 'כל האיזורים';
$_['text_pagination']       = 'מציג {start} עד {end} מתוך {total} ({pages} עמודים)';
$_['text_separator']        = ' &raquo; ';

$_['text_read_more']         = 'עוד';
$_['text_read_less']         = 'סגור';
$_['text_choose_side']       = 'בחר את התוספת שאכלת';

$_['text_review_start']       = 'הוסף ביקורת ל';

$_['text_what_orders']       = 'הזמנות שהתקבלו';

$_['text_order_time_passed']    = 'זמן ההזמנה חלף להיום';
$_['text_ot_title_no_orders']   = 'אין ארוחות זמינות ליום %s';
$_['text_ot_title']             = 'ארוחות ליום %s שאינן זמינות יותר';
$_['text_ot_subtitle']          = 'ארוחות שאזל המלאי שלהן או עבר הזמן להזמנה';

$_['text_otp_title']            = 'הארוחה אינה זמינה ליום המבוקש';
$_['text_otp_subtitle']         = 'לא ניתן להזמין את הארוחה ליום שבחרת מכיוון שזמן ההזמנה חלף או אזל המלאי. באפשרותך להזמין את הארוחה כבר עכשיו ליום אחר מהאפשרויות הזמינות.';
$_['text_otp_exit']             = 'סגור';

$_['title_bulk_order']          = 'רוצים לעשות הזמנה מרוכזת מ%s?';
$_['text_bulk_order']           = 'להזמנות מרוכזות ממגוון הארוחות של %s לארועים, לארוחות שישי או למהלך השבוע, התקשרו אלינו 03-3741886';

//$_['text_order_time_passed']       = 'זמן ההזמנה להיום עבר';


$_['cart_delivery_text'] = 'ארוחות שאינן זמינות לזמן המשלוח הנבחר אינן מוצגות';


$_['text_cooked_by']                = 'בשלן:';
$_['cook_name']                     = '%s %s';
$_['text_delivered']                = 'משלוח:';
$_['delivery_time']                 = '%s, %s';
$_['placeholder_meal']              = 'כיתבו ביקורת. ככל שאתם משתפים כך אתם עוזרים לאחרים ללמוד על הארוחה.';
$_['placeholder_delivery']          = 'הערות נוספות על המשלוח...';

// Buttons
$_['button_add_address']    = 'הוסף כתובת';
$_['button_back']           = 'אחורה';
$_['button_continue']       = 'המשך';
$_['button_cart']           = 'הוסף להזמנה';
$_['button_compare']        = 'הוסף להשוואה';
$_['button_wishlist']       = 'הוסף רשימת מועדפים';
$_['button_checkout']       = 'לקופה';
$_['button_confirm']        = 'אישור הזמנה';
$_['button_coupon']         = 'הוסף קופון';
$_['button_delete']         = 'מחק';
$_['button_edit']           = 'עריכה';
$_['button_new_address']    = 'כתובת חדשה';
$_['button_change_address'] = 'שנה כתובת';
$_['button_add_product']    = 'הוסף מוצר';
$_['button_remove']         = 'הסרה';
$_['button_reviews']        = 'תגובות';
$_['button_write']          = 'הוסף תגובה';
$_['button_login']          = 'כניסה';
$_['button_update']         = 'עידכון';
$_['button_shopping']       = 'חזרה לתפריט';
$_['button_search']         = 'חיפוש';
$_['button_shipping']       = 'הוסף משלוח';
$_['button_guest']          = 'תשלום ללא הרשמה';
$_['button_view']           = 'הצג';
$_['button_voucher']        = 'הוסף שובר';
$_['button_upload']         = 'העלה קובץ';
$_['button_reward']         = 'הוסף נקודות זכות';
$_['button_quote']          = 'שלח בקשה להצעות מחיר';

$_['button_out_of_stock']   = 'אזל המלאי';
$_['button_not_avilable']   = 'לא ניתן להזמין';

$_['limitbox_text_1']   = 'לא ניתן להוסיף ארוחה זו להזמנה הקיימת.';
$_['limitbox_text_2']   = 'באפשרותך להוסיף להזמנה הנוכחית אך ורק ארוחות נוספות אשר זמינות מאותו הבשלן עבור זמן המשלוח שבחרת. באפשרותך לבצע הזמנה נוספת לארוחה זו מייד עם סיום ההזמנה הנוכחית.';

$_['last_order_time_catpage']   = 'הזמנות עד %s';

$_['only_cook_title'] = 'ארוחות נוספות להזמנה';
$_['only_cook_text1'] = 'בשלב זה ניתן להוסיף להזמנה הנוכחית אך ורק ארוחות נוספות מאותו הבשלן.';
$_['only_cook_text2'] = 'תוכלו לבצע הזמנה חדשה מבשלנים נוספים מייד עם סיום ההזמנה הנוכחית.<br><br>לעזרה בהזמנה מרוכזת ממספר בשלנים צרו איתנו קשר בטלפון 03-3741886 או במייל <a href="https://mail.google.com/mail/?view=cm&amp;fs=1&amp;tf=1&amp;to=support@Quiky.com" target="_blank">support@Quiky.com</a>';

$_['button_add_avilable']   = 'הוסף ארוחה זמינה';

$_['text_inbox']   = 'תיבת דואר';
$_['text_inbox_s']   = 'דואר';

$_['button_order_now']      = 'הוסף להזמנה';
$_['text_cook']             = 'בשלן';

$_['text_on']               = 'בתאריך';
$_['text_ingredients']      = 'מרכיבים';
$_['text_sides']            = 'תוספות';

$_['text_my_orders']            = 'ההזמנות שלך';

$_['text_items_in_bag']             = 'ארוחות בהזמנה';
$_['text_recent_meal_added']        = 'ארוחות אחרונות שנוספו';
$_['text_add_meal']                 = 'הוסף ארוחה';
$_['text_checkout']                 = 'לתשלום';
$_['text_close']                    = 'סגור';
$_['text_empty']                    = 'רוקן';

$_['text_customer_my_orders']       = 'ההזמנות שלך';

$_['rate_word_0']                    = '';
$_['rate_word_1']                    = 'לא טעים';
$_['rate_word_2']                    = 'מאכזב';
$_['rate_word_3']                    = 'סביר';
$_['rate_word_4']                    = 'טעים';
$_['rate_word_5']                    = 'טעים מאוד';

$_['delivery_header']                       = 'ספר/י לנו איך היה המשלוח';
$_['text_my_orders']                       = 'ההזמנות שלך';
$_['add_privet_review']                      = '+ הוסף פידבק פרטי לבשלן';

$_['delivery_question_1']                    = 'האם ההזמנה שלך הגיעה בזמן?';
$_['delivery_question_2']                    = 'האם הארוחות הכלולות בהזמנה הגיעו במצב טוב?';
$_['delivery_question_3']                    = 'האם השליח היה מקצועי ואדיב?';

$_['text_yes']                = 'כן';
$_['text_no']                 = 'לא';

$_['text_submit']             = 'אשר';
$_['text_cancel']             = 'בטל';

$_['text_address_time']         = 'פרטי משלוח';
$_['text_address_time_demo']         = '%s, 12:00 - 13:00';
$_['text_meals']         = 'ארוחות';
$_['text_deliver_to']         = 'משלוח';
$_['delivery_address']         = 'רוטשילד 12, תל אביב';
$_['text_empty_cart']         = 'בטל הזמנה';

$_['text_shipping_cost'] = 'דמי משלוח:';

$_['text_delivery_title']          = 'כתובת:';
$_['text_delivery_address']        = 'רחוב:';
$_['text_delivery_address_demo']   = 'רוטשילד 12, תל אביב';
$_['text_time']                    = 'זמן:';
$_['text_time_demo']               = 'שני 28/3/2014, 12:00 - 13:00';

$_['text_floor']                   = 'קומה:';
$_['text_apartment']               = 'דירה:';
$_['text_entrance']                = 'כניסה:';
$_['text_street_num']              = 'מס\' בית:';

$_['text_company']                 = 'חברה:';


$_['text_delivery_comments']               = 'הערות לשליח: (הערות לבשלן יש לצרף לארוחה)';

$_['text_help_save']       = 'עזרו לנו לשמור על הסביבה';
$_['text_need_tools']       = 'אין לי צורך בסכו"ם חד פעמי';


$_['text_delivery_contact']               = 'איש קשר:';
$_['text_delivery_contact_demo']               = 'מידד';
$_['text_delivery_mobile']               = 'טלפון:';
$_['text_delivery_mobile_demo']               = '054-3214567';
$_['text_save_address']               = 'שמור את פרטי הכתובת';

$_['text_number_of_meals']               = 'סה״כ ארוחות:';
$_['text_total_meals']               = 'סיכום:';
$_['text_total_coupon']               = 'הנחה:'; 
$_['text_pay_credit']               = 'לתשלום בכרטיס אשראי';
$_['text_pay_pal']               = 'לתשלום ב-PayPal';

$_['text_joinlist']               = 'אני מעוניין לקבל הצעות לארוחות טעימות מידי יום';

$_['ils']                       = '₪';

$_['text_sp']                   = 'התמחות';
$_['text_more']                 = 'עוד ארוחות של %s';
$_['text_view_all']             = 'כל הארוחות';
$_['total_sides']               = '%s תוספות לבחירה';
$_['total_sides_one']           = 'תוספת אחת לבחירה';

$_['total_sides_cat']               = '<span class="side-num">%s</span> תוספות';
$_['total_sides_one_cat']           = 'תוספת אחת';

$_['text_next_day']           = 'לארוחות של יום <span class="next-day-name">שלישי</span>, <span class="next-day-mon">7 ביולי</span>';

$_['total_sides_one_nc']        = 'תוספות ללא בחירה';
$_['total_sides_clear']         = '%s תוספות';
$_['total_sides_clear_one']     = 'תוספת אחת';
$_['text_add_review']           = 'הוסף ביקורת';

$_['text_cook_page_heading'] = "הוסף ארוחות להזמנה";

$_['text_delivery'] = "משלוח:";
$_['text_cook'] = "בשלן:";


$_['cook_profile_title'] = "%s %s";

$_['lable_reviews']         = 'ביקורות';

$_['text_per_meal']         = 'לארוחה';
$_['text_deliver']          = 'שלח אל';
$_['text_addmsg']           = 'הוסף הודעה לבשלן';
$_['text_addfav']           = 'הוסף בשלן למעודפים';
$_['text_total']            = 'סה"כ';

$_['text_mymeals']            = 'הארוחות שלי';
$_['text_cookreviews']            = 'ביקורות';

$_['text_sp'] = 'התמחות';
$_['text_contactme'] = 'צור קשר';

$_['text_share'] = 'שתף';
$_['text_aboutme'] = 'קצת עליי';

$_['text_rating'] = 'דירוג חיובי';
$_['text_recommended'] = 'המלצות';
$_['text_reviews_text'] = 'מדרגים';

$_['text_your_order_part_1'] = 'ההזמנה';
$_['text_your_order_part_2'] = 'שלך';

$_['text_delivery_details_part_1'] = 'פרטי';
$_['text_delivery_details_part_2'] = 'משלוח';

$_['text_order_summary_part_1'] = 'סיכום';
$_['text_order_summary_part_2'] = 'הזמנה';

$_['text_meals'] = 'ארוחות';
$_['text_one_meal'] = 'ארוחה אחת';

// Error
$_['error_upload_1']        = 'Warning: The uploaded file exceeds the upload_max_filesize directive in php.ini!';
$_['error_upload_2']        = 'Warning: The uploaded file exceeds the MAX_FILE_SIZE directive that was specified in the HTML form!';
$_['error_upload_3']        = 'Warning: The uploaded file was only partially uploaded!';
$_['error_upload_4']        = 'Warning: No file was uploaded!';
$_['error_upload_6']        = 'Warning: Missing a temporary folder!';
$_['error_upload_7']        = 'Warning: Failed to write file to disk!';
$_['error_upload_8']        = 'Warning: File upload stopped by extension!';
$_['error_upload_999']      = 'Warning: No error code available!';


// Footer
$_['the_service']      = 'השירות';
$_['how_it_works']      = 'איך זה עובד?';
$_['trust']      = 'אמון וביטחות';
$_['faq']      = 'שאלות נפוצות';
$_['join_us']      = 'הצטרפות לבשלנים';

$_['the_company']      = 'החברה';
$_['about_us']      = 'אודותינו';
$_['terms_of_use']      = 'תנאי השימוש';
$_['privacy_policy']      = 'מדיניות פרטיות';

$_['rights']      = ' כל הזכויות שמורות ל- %s';

$_['rights_ex']      = ' © 2014';
$_['text_of']      = ' של ';

// success

$_['heading_success'] = 'הזמנתך התקבלה.';
$_['soon_aproved'] = 'הזמנתך נשלחה לבשלן לאישור. עם אישור ההזמנה, תשלח אליך הודעה עם פרטי ההזמנה לכתובת המייל שמופיעה במערכת.';
$_['heading_deal'] = 'למה לחכות לרגע האחרון? הזמן כבר עכשיו להמשך השבוע.';
$_['deal_text'] = 'באפשרותך להקדים ולבצע הזמנות נוספות לימים הקרובים, ולהבטיח את זמינות הארוחות בימים ובשעות שתרצה (שים לב, כמות המנות הזמינות מכל ארוחה מוגבלת בהתאם להגדרת הבשלן).';
$_['order_now'] = 'הזמן עכשיו';

//share charms

$_['sface'] = 'שתף בפייסבוק';
$_['stwit'] = 'שתף בטוויטר';
$_['smail'] = 'שתף במייל';
$_['smail_sub'] = '%s, בשלן מדהים בQuiky';
$_['smail_bud'] = 'רציתי לשתף איתך בשלן מדהים בQuiky שמתמחה ב: %s %s';
$_['smail_bud_meal'] = 'רציתי לשתף איתך את הארוחה הזו ב-Quiky : %s';

// multi review mail

$_['Quiky'] = 'Quiky';
$_['text_Quiky_greeting'] = '%s שלח לך את ההודעה הבאה';
$_['text_Quiky_subject'] = '%s שלח לך הודעה אישית';

// pm mail text

$_['text_greeting_pm'] = 'שלום %s,';
$_['text_subject_pm'] = 'נשלחה הודעה חדשה מ%s';
$_['text_pre_msg'] = '%s שלח לך הודעה:';

$_['text_follow_us'] = 'עקבו אחרינו';

$_['text_extra_long']      = ' - בתוספת %s';
// no products

$_['text_no_pros'] = 'מכין את השולחן, מייד תועבר לארוחות.';

// no addresses

$_['text_no_saved_addresses'] = 'לא קיימות כתובות שמורות במערכת.';
$_['text_log_in_addresses'] = 'התחבר למערכת להצגת הכתובות שלך.';
$_['text_tel_aviv'] = 'תל אביב';
$_['text_find_meals'] = 'חפש ארוחות בתל אביב';

// checkout warning

$_['warn_address'] = 'נא הקלידו כתובת מלאה כולל מספר בית';
$_['warn_floor'] = 'יש למאה קומה';
$_['warn_contact'] = 'נא הקלידו שם של איש קשר להזמנה';
$_['warn_phone'] = 'נא הקלידו מספר טלפון שתהיו זמינים בו';
$_['warn_street_name'] = 'נא הקלידו שם רחוב תקין';
$_['warn_street_num'] = 'נא הקלידו מספר רחוב';

$_['text_coupon_code'] = 'קוד קופון';




$_['text_new_subject'] = 'אין ארוחות להציג לסינון הנוכחי.';


$_['text_filter'] = 'סנן תוצאות';

$_['text_terms'] = 'תנאי שימוש';
$_['text_privet'] = 'מדיניות פרטיות';

$_['text_approved'] = 'הזמנתך אושרה!';
$_['text_approved_more'] = 'הזמנה מספר %s עבור %s %s בשעה %s אושרה בהצלחה!';
$_['text_not_pendding'] = 'הזמנה מספר %s איננה ממתינה לאישור.';

// sms msg text!

$_['sms_new_order'] = 'התקבלה הזמנה מ%s %s ליום %s:';
$_['sms_approve'] = 'לאישור הזמנה השב להודעה זו עם המספר %s ';


// messaging text

$_['conversations_heading']     = 'תיבת הדואר שלך';
$_['msg_title']                 = 'שיחה עם %s';
$_['msg_placeholder']           = 'ההודעה שלך...';
$_['msg_new_headline']          = 'שלח הודעה פרטית ל%s';
$_['msg_send']                  = 'שלח';

// tool tip for remove
$_['tip_remove']                  = 'הסר ארוחה';
$_['change_my_pass']  = 'שינוי סיסמה';


$_['info_line_1'] = '%s, %s';
$_['info_line_2'] = 'רחוב %s, %s';
$_['info_line_3'] = 'למשלוחים חייגו %s';


?>