<?php
// Text
$_['text_new_subject']          = 'אישור הזמנה ליום %s';
$_['text_new_subject_seller']   = 'התקבלה הזמנה חדשה מ%s ליום %s';
$_['text_new_order_subject']    = 'הזמנתך נקלטה במערכת - %s ארוחות מ%s ליום %s';
$_['text_new_greeting']         = 'תודה על התענינותך ב %s . ההזמנתך התקבלה, נטפל בהזמנתך בהקדם האפשרי לאחר שנוודא שהתשלום בוצע.';
$_['text_new_received']         = 'קיבלת הזמנה חדשה.';
$_['text_new_link']             = 'על מנת לצפות בהזמנה שלך לחץ על הקישור הבא:';
$_['text_new_order_detail']     = 'פרטי ההזמנה';
$_['text_new_invoice_no']       = 'קבלה מספר.:';
$_['text_new_order_id']         = 'מספר הזמנה:';
$_['text_new_date_added']       = 'הוסף בתאריך:';
$_['text_new_order_status']     = 'מצב השמנה:';
$_['text_new_payment_method']   = 'שיטת תשלום:';
$_['text_new_shipping_method']  = 'שיטת משלוח:';
$_['text_new_email']  		= 'דואר אלקטרוני:';
$_['text_new_telephone']  	= 'טלפון:';
$_['text_new_ip']  	        = 'כתובת IP:';
$_['text_new_payment_address']  = 'כתובת לנושאי תשלום';
$_['text_new_shipping_address'] = 'כתובת משלוח';
$_['text_new_products']         = 'מוצרים';
$_['text_new_product']          = 'מוצר';
$_['text_new_model']            = 'דגם';
$_['text_new_quantity']         = 'כמות';
$_['text_new_price']            = 'מחיר';
$_['text_new_order_total']      = 'סיכום הסכומים להזמנה';	
$_['text_new_total']            = 'סך הכול';	
$_['text_new_download']         = 'לאחר אישור שהתשלום באפשרותך ללחוץ על הקישור למטה כדי לקבל גישה למוצרים שלך שהם להורדה:';
$_['text_new_comment']          = 'ההערות עבור הזמנתך הן:';
$_['text_new_footer']           = 'נא השב לאימייל זה אם שיל לך שאלות כלשהן.';
$_['text_new_powered']          = '';//'Powered By <a href="http://www.opencart.com">OpenCart</a>.';
$_['text_update_subject']       = '%s - עידכון הזמנה %s';
$_['text_update_order']         = 'הזמנה מספר:';
$_['text_update_date_added']    = 'הוזמן בתאריך:';
$_['text_update_order_status']  = 'הזמנתך עודכנה למצב הבא:';
$_['text_update_comment']       = 'ההערות עבור הזמנתך הן:';
$_['text_update_link']          = 'על מנת לצפות בהזמנתך לחץ על הקישור להלן:';
$_['text_update_footer']        = 'נא השב לאימייל זה אם שיל לך שאלות כלשהן.';


$_['text_review_subject']       = 'דירוג וביקורת ההזמנה שלך מ%s מיום %s.​';
$_['text_review_greeting']      = 'שלום %s';
$_['text_review_text_prolog']      = 'אנו מקווים שנהנית מהארוחות שהזמנת מ%s ביום %s. ';
$_['text_review_text_epilog']      = 'נודה לך אם תוכל/י להקדיש מספר רגעים לדירוג וביקורת הארוחות בכדי לסייע ל%s ולכל קהילת Homeals. דירוג וביקורות כתובות עוזרות לשמור על איכות הקהילה והארוחות שמוצעות ע״י הבשלנים.';
$_['text_rate_order']           = 'לדירוג ההזמנה';


$_['text_homeals_greeting'] = 'שלום %s';
$_['text_order_status'] = 'סטטוס:';
$_['text_order_id'] = 'קוד הזמנה:';

$_['text_order_aproved'] = 'הזמנתך אושרה ע״י הבשלנ/ים. הארוחות הכלולות בהזמנה יגיעו אליך באמצעות שליח ל%s, ביום %s, בין השעות %s.';
$_['text_order_new'] = 'הזמנתך ל-%s ארוחות מ%s ליום %s נקלטה במערכת. באפשרות הבשלנים לאשר את ההזמנה עד שעתיים לפני זמן המשלוח.';
$_['text_order_thank_you'] = 'תודה שבחרת להזמין Homeals. בתיאבון.';

$_['text_order_tools'] = 'סכו״ם חד פעמי:';
$_['need_tools'] = 'צריך';
$_['dont_need_tools'] = 'אין צורך';

$_['text_order_thanks_you'] = 'בתיאבון, <br>צוות Homeals.';
$_['text_here_are_details'] = 'להלן פרטי ההזמנה:';
$_['text_meals_word'] = 'ארוחות';
$_['text_deliver_word'] = 'משלוח';


//cook details

$_['text_cook_details'] = 'פרטי בשלן';

//delivery details
$_['text_delivery_details'] = 'פרטי משלוח';
$_['text_order_when'] = 'מתי:';
$_['order_when'] = '%s, %s';
$_['text_order_address'] = 'כתובת:';
$_['text_order_contact'] = 'איש קשר:';
$_['order_contact'] = "%s";
$_['text_order_phone'] = 'טלפון:';
$_['order_phone'] = "%s";
$_['text_order_comment'] = 'הערה:';
$_['text_cooks'] = 'בשלנ/ית:';

$_['text_order_compony'] = 'חברה:';

// meal(product) details
$_['text_meal_details'] = 'פרטי ארוחות';

// product need to have picture, name + side names, comment, quantity, price
$_['text_meal_coment'] = 'הערה לבשלן:';
$_['text_quantity'] = 'כמות:';
$_['text_per_price'] = 'מחיר לארוחה:';

// order summary
$_['text_order_summary'] = 'פרטי החיוב';
$_['text_meals_total'] = 'עלות הארוחות';
$_['text_delivery_cost'] = 'דמי משלוח';
$_['text_total'] = 'סה"כ';

// fotter
$_['text_homeals_help'] = 'במידה ונתקלת באיזושהי בעיה בהזמנתך, צוות התמיכה שלנו ישמח לסייע לך בכל דרך בטלפון:%s או במייל: %s צוות Homeals.';


$_['cook_text_greeting'] = 'התקבלה הזמנה חדשה מ%s %s';
$_['cook_text_meals'] = 'ארוחות';
$_['cook_text_payment'] = 'התשלום שלך';
$_['cook_text_aprove'] = 'לאישור ההזמנה';
$_['cook_text_delivery_time'] = 'זמן איסוף';
$_['cook_text_details'] = 'פרטי הסועד';
$_['cook_text_username'] = 'שם';
$_['cook_text_place'] = 'מיקום';
$_['cook_text_mail'] = 'מייל';
$_['cook_text_phone'] = 'טלפון';
$_['cook_text_phone_extra'] = 'טלפון נוסף';




?>