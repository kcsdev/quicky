<?php
// Text
$_['text_home']     = 'בית';
$_['text_wishlist'] = 'רשימת מועדפים (%s)';
$_['text_cart']     = 'סל קניות';
$_['text_items']    = '%s פריט(ים) - %s';
$_['text_search']   = 'רחוב למשלוח בתל אביב';
$_['text_welcome']  = '<a href="%s">התחבר</a>/<a href="%s">צור חשבון</a>.';
$_['text_welcomehome']  = '<a href="#loginbox" id="open_login">התחבר</a> / <a href="#signinbox" id="open_signin">צור חשבון.</a>';
$_['text_logged']   = '<a href="%s">%s</a>';
$_['text_account']  = 'החשבון שלי';
$_['text_checkout'] = 'לקופה';
$_['text_language'] = 'Language';
$_['text_currency'] = 'מטבע';

$_['text_day_1'] = 'ראשון';
$_['text_day_2'] = 'שני';
$_['text_day_3'] = 'שלישי';
$_['text_day_4'] = 'רביעי';
$_['text_day_5'] = 'חמישי';
$_['text_day_6'] = 'שישי';
$_['text_day_7'] = 'שבת';

$_['text_month_1'] = 'בינואר';
$_['text_month_2'] = 'בפברואר';
$_['text_month_3'] = 'במרץ';
$_['text_month_4'] = 'באפריל';
$_['text_month_5'] = 'במאי';
$_['text_month_6'] = 'ביוני';
$_['text_month_7'] = 'ביולי';
$_['text_month_8'] = 'באוגוסט';
$_['text_month_9'] = 'בספטמבר';
$_['text_month_10'] = 'באוקטובר';
$_['text_month_11'] = 'בנובמבר';
$_['text_month_12'] = 'בדצמבר';

$_['text_loginboxtop'] = 'כניסה';
$_['text_loginemail'] = 'כתובת אימייל';
$_['text_loginpass'] = 'סיסמה';
$_['text_loginforgot'] = '<a href="%s">שכחת סיסמה?</a>';
$_['text_loginremember'] = 'זכור אותי';
$_['text_loginboxbottom'] = 'משתמש חדש? <a id="%s">הירשם</a>';

$_['text_logintext'] = 'התחבר באמצעות כתובת האימייל שלך';

$_['text_or'] = 'או';

$_['text_signinboxtop'] = 'הרשמה';
$_['text_signintext'] = 'הירשם באמצעות כתובת האימייל שלך';
$_['text_signinfirstname'] = 'שם פרטי';
$_['text_signinlastname'] = 'שם משפחה';
$_['text_signincity'] = 'עיר';
$_['text_signinagree']  = 'בהרשמה לשירות, אני מסכים ל<a href="%s" target="_blank">תנאי השימוש</a> ו<a href="%s" target="_blank">מדיניות הפרטיות</a> של Quiky.';
$_['text_signinboxbottom'] = 'משתמש קיים? <a id="%s">התחבר</a>';

$_['text_signup'] = 'הירשם';
$_['text_login'] = 'התחבר';
$_['text_need_help'] = 'צריכים עזרה?';
$_['text_loginerror'] = 'שם משתמש או סיסמה אינם נכונים';
$_['text_logout'] = 'התנתק';

$_['text_street'] = 'תל-אביב';
$_['text_resualts'] = 'מציג %s תוצאות עבור';

$_['text_signin'] = 'הירשם';

$_['text_write_us']     = 'כתבו לנו:';
$_['support_mail']      = 'support@Quiky.com';
$_['text_call_us']      = 'התקשרו אלינו:';
$_['support_number']    = '03-3741886';

$_['text_error_mail']           = 'כתובת מייל לא תקינה';
$_['text_error_mail_exists']    = 'כתובת מייל בשימוש';
$_['text_error_pass']           = 'הסיסמה חלשה מיידי';
$_['text_error_firstname']      = 'שם פרטי לא תקין';
$_['text_error_lastname']       = 'שם משפחה לא תקין';
$_['text_error_city']       = 'עיר לא תקינה';

$_['text_find_meals'] = '';
$_['text_login_return'] = 'כניסה ללקוחות חוזרים';
$_['placeholder_search'] = 'חפש ארוחה, מסעדה, משלוחים';

$_['text_about'] = "אודות קוויקי";
$_['text_rest_list'] = "רשימת מסעדות";
$_['text_how_it_works'] = "כיצב זה עובד";
$_['text_policy'] = "מדיניות משלוחים";
$_['text_like_on_facebook'] =  "עשו לנו לייק בפייסבוק";

?>