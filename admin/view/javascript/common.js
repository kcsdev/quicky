$(document).ready(function() {
	$('#menu > ul').superfish({
		pathClass	: 'overideThisToUse',
		delay		: 0,
		animation	: {height: 'show'},
		speed		: 'normal',
		autoArrows   	: false,
		dropShadows  	: false, 
		disableHI	: false, /* set to true to disable hoverIntent detection */
		onInit		: function(){},
		onBeforeShow 	: function(){},
		onShow		: function(){},
		onHide		: function(){}
	});
	
	$('#menu > ul').css('display', 'block');
});
 
function getURLVar(key) {
	var value = [];
	
	var query = String(document.location).split('?');
	
	if (query[1]) {
		var part = query[1].split('&');

		for (i = 0; i < part.length; i++) {
			var data = part[i].split('=');
			
			if (data[0] && data[1]) {
				value[data[0]] = data[1];
			}
		}
		
		if (value[key]) {
			return value[key];
		} else {
			return '';
		}
	}
	
	return '';
}

$(document).ready(function() {
	route = getURLVar('route');
	
	if (!route) {
		$('#dashboard').addClass('selected');
	} else {
		part = route.split('/');
		
		url = part[0];
		
		if (part[1]) {
			url += '/' + part[1];
		}
		
		$('a[href*=\'' + url + '\']').parents('li[id]').addClass('selected');
	}
	
	$('#menu ul li').on('click', function() {
		$(this).addClass('hover');
	});

	$('#menu ul li').on('mouseout', function() {
		$(this).removeClass('hover');
	});
});

//get items categories from db
/*
$(document).ready(function() {
    
function getUrlPar(sParam)
{
    var sPageURL = window.location.search.substring(1);
    var sURLVariables = sPageURL.split('&');
    for (var i = 0; i < sURLVariables.length; i++) 
    {
        var sParameterName = sURLVariables[i].split('=');
        if (sParameterName[0] == sParam) 
        {
            return sParameterName[1];
        }
    }
}  
    
var tok = getUrlPar('token');
    
    
  $('#getCat').click(function(){
  $('td#rest_cat').html('');
  var rest = $('input#choose_rest').val();
  var rest_id = $('#choose_rest_id').val();
  $.ajax({
			  url: "http://54.77.118.39/admin/index.php?route=sale/customer/getItemCategories&token=" + tok,
			  type: "GET",
			  dataType: "json",
			  async: "false",
              data: {my_id:rest_id},
             success: function(res) {
                var output = '';
                var my_data = res;
                var data_str =  my_data.data;
                var data_arr = JSON.parse(data_str)
                 $.each( data_arr, function(index,value) {

                        output += '<input class="catBox" type="checkbox" id="'+index+'" name="cat[]" value="' +value.day +'">' + value.day + '<br>';
                     
                     
                      });
                
                   $('td#rest_cat').append(output);
                 }
    });

   });
});


window.onload = function(){
  document.getElementById("getCat").click();
}
*/
