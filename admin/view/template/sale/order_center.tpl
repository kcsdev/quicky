<?php echo $header; ?>

<div id="content">
<div class="box">
    
<div class="heading">
 <h1><img src="view/image/order.png" alt="" /> <?php echo $heading_title; ?></h1>   
</div>    
  
<div class="content">
    
<table class="list">
    <thead>
     <tr>
         <td class="left"><?php echo $entery_mail ?></td>
         <td class="left"><?php echo $entery_phone ?></td>
        <td class="left"><?php echo $entery_price ?></td>
        <td class="left"><?php echo $entery_restaurent ?></td>
         <td class="right"><?php echo $entery_edit ?></td>
         <td class="right"><?php echo $entery_remove ?></td>
        </tr>
  </thead>
    <tbody>
        
    <?php foreach($order as $key=>$value){ ?>  
        <tr>
            <td class="left"><?php echo $value['mail'] ?></td>
            <td class="left"><?php echo $value['phone'] ?></td>
            <td class="left"><?php echo $value['final_price'] ?></td>
            <td class="left"><?php echo 'מסעדה' ?></td>
            <td class="right"><a href="<?php echo $action_edit . '&order_id=' . $value['id'] ?>"><?php echo $entery_edit ?></a></td>
            <td><a href="<?php echo $action_delete . '&order_id=' . $value['id'] ?>"><?php echo $entery_remove ?></a></td>
        
        </tr>
     <?php } ?> 
    </tbody>

    
    
    
</table>
    
    
    
</div>    
    
</div>
</div>



<?php echo $footer; ?>