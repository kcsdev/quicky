<?php echo $header ?>

<div id="content">
<div class="box">
 <div class="heading">
 <h1><img src="view/image/order.png" alt="" /> <?php echo $heading_title; ?></h1>
     <div class="buttons"><a onclick="$('#form').submit()"  class="button" id="sub_but"><?php echo $button_save; ?></a><a onclick="history.go(-1);" class="button"><?php echo $entery_back; ?></a></div>
</div>    
 <div class="content">
<form action="<?php echo $form_action  ?>" method="post" enctype="multipart/form-data" id="form">
<table class="form">

        <tr>
        <td><?php echo $text_restaurant; ?></td>
        <td>
            <input id="tags" name="restaurant">
            <input type="hidden" name="rest_id" value="" id="rest_id">
        </td>
    </tr>
    <tr>
        <td><?php echo $text_time_make ?></td>
        <td><input type="text" name="time" id="time" readonly></td>
    </tr>
    <tr style="display:none">
        <td id="city_row">
          
        
        </td>
    
    </tr>
    <tr style="display:none">
        <input type="text" name="the_city" style="display:none">
    </tr>
    <table class="list">
        <thead>
           <tr>
               <td>שם החברה</td>
                <td>ישוב</td>
                <td>רחוב</td>
                <td>מזומן</td>
                <td>הערות</td>
            <tr>
        </thead>
        <tbody id="row_table">
        </tbody>
    </table>
    <a  class="button" id="add_row"><?php echo $text_add_row; ?></a>
</table>     
</form>     

</div>
</div>
</div>
<script>
function getUrlParameter(sParam)
{
    var sPageURL = window.location.search.substring(1);
    var sURLVariables = sPageURL.split('&');
    for (var i = 0; i < sURLVariables.length; i++) 
    {
        var sParameterName = sURLVariables[i].split('=');
        if (sParameterName[0] == sParam) 
        {
            return sParameterName[1];
        }
    }
}
var token = getUrlParameter('token');
</script>

<script>
$(function() {
    var availableTags = [
        <?php foreach($restaurants as $restaurant){ ?>
                <?php echo '"' .  $restaurant['firstname'] . '",' ?>
            <?php } ?>
    ];
    $( "#tags" ).autocomplete({
      source: availableTags
    });
  });

</script>
<script>
$('#tags').focusout(function(){
var value = $(this).val();
 $.ajax({
			  url: "index.php?route=sale/quick/get_id&token=" + token,
			  type: "POST",
			  dataType: "html",
			  async: "false",
              data: {name:value},
             success: function(response) {
                $('#rest_id').val(response)
             }
  });
  $.ajax({
			  url: "index.php?route=sale/quick/get_city&token=" + token,
			  type: "POST",
			  dataType: "html",
			  async: "false",
              data: {name:value},
             success: function(res) {
                var cities = JSON.parse(res);
                var cities_output = '<select id="city_select">';
                var counter = 1;
                var city = '';
                $.each(cities , function( key, value ) {
                    if(counter == 1){
                      city = value.city;
                    }
                     cities_output += '<option value"' + value.city + '">' +  value.city + '</option>';
                    counter = counter + 1;
                });
                cities_output += '</select>';
                $('td#city_row').html(cities_output);
                $('input[name="the_city"]').val(city);
             }
  });
    
   $.ajax({
			  url: "index.php?route=sale/quick/get_time&token=" + token,
			  type: "POST",
			  dataType: "html",
			  async: "false",
              data: {name:value},
             success: function(re) {
                $('#time').val(re);
             }
  });  

});
</script>
<script>
$('#add_row').on('click', function(){
$children = $('#row_table').children().length;
var output = '';
output += '<tr>';
output += '<td><input type="text" name="order[' + $children +'][name]"></td>';
output += '<td><select class="city_change" name="order[' + $children +'][city]">' + $('#city_select').html() +'</select>';
output += '<td><input type="text" name="order[' + $children +'][street]"></td>';
output += '<td><input type="text" name="order[' + $children +'][cash]"></td>';
output += '<td><textarea name="order[' + $children +'][comment]"></textarea></td>';
output += '</tr>';
$('#row_table').append(output);
})


</script>
<script>
$('#row_table').on('change', 'select', function(){

$('input[name="the_city"]').val($(this).val());

})

</script>
<?php echo $footer ?>