<?php echo $header; ?>

<div id="content">
  <div class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
    <?php } ?>
  </div>
  <?php if ($error_warning) { ?>
  <div class="warning"><?php echo $error_warning; ?></div>
  <?php } ?>
  <div class="box">
    <div class="heading">
      <?php if($this->session->data['destination'] == 'rest') {?>
      <h1><img src="view/image/customer.png" alt="" /> <?php echo 'מסעדות' ?></h1>
    <?php }else{?>
        <h1><img src="view/image/customer.png" alt="" /> <?php echo 'לקוח' ?></h1>
        <?php } ?>
      <div class="buttons"><a onclick="$('#form').submit();" class="button"><?php echo $button_save; ?></a><a href="#" class="button prev" onclick="history.go(-1);"><?php echo $button_cancel; ?></a></div>
    </div>
    <div class="content">
      <div id="htabs" class="htabs"><a href="#tab-general"><?php echo $tab_general; ?></a><a href="#tab-image"><?php echo $tab_image; ?></a>
        <?php if ($customer_id) { ?>
       <!-- <a href="#tab-history"><?php echo $tab_history; ?></a>--><a href="#tab-transaction"><?php echo $tab_transaction; ?></a><!--<a href="#tab-reward"><?php echo $tab_reward; ?></a>-->
        <?php } ?>
       <!-- <a href="#tab-ip"><?php echo $tab_ip; ?></a>-->
        <a href="#availability"><?php echo 'זמינות'; ?></a>
        <a href="#menu_cat"><?php echo 'קטגוריות תפריט'; ?></a>
        <a href="#conatct"><?php echo 'אנשי קשר' ?></a>
        <?php if ($customer_id) { ?>
        <a href="#addProd"><?php echo 'הוסף מוצר' ?></a>
         <?php }?>
         <a href="#subscription"><?php echo 'דמי מנוי' ?></a>
        <a href="#comission"><?php echo 'עמלה' ?></a></div>
      <!--<form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form" class="<?php echo 'no' . $customer_group_id;?>">-->
      <?php if($this->session->data['destination'] == 'rest'){  ?>
             <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form" class="<?php echo 'no2' ?>">
        <?php }else{ ?>
            <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form" class="<?php echo 'no1' ?>">
        <?php } ?>
        <div id="tab-general">
          <div id="vtabs" class="vtabs cust_no rest_no"><a href="#tab-customer"><?php echo $tab_general; ?></a>
            <?php $address_row = 1; ?>
            <?php foreach ($addresses as $address) { ?>
            <a href="#tab-address-<?php echo $address_row; ?>" id="address-<?php echo $address_row; ?>"><?php echo $tab_address . ' ' . $address_row; ?>&nbsp;<img src="view/image/delete.png" alt="" onclick="$('#vtabs a:first').trigger('click'); $('#address-<?php echo $address_row; ?>').remove(); $('#tab-address-<?php echo $address_row; ?>').remove(); return false;" /></a>
            <?php $address_row++; ?>
            <?php } ?>
            <span id="address-add"><?php echo 'איש קשר'; ?>&nbsp;<img src="view/image/add.png" alt="" onclick="addAddress();" /></span></div>
          <div id="tab-customer" class="vtabs-content">
    <!--  start of costumer table -->
            <table class="form">
              <tr>
                <?php if($this->session->data['destination'] == 'rest') {?>
                  <td><span class="required">*</span> <?php echo 'שם:'; ?></td>
                 <?php }else{?> 
                  <td><span class="required">*</span> <?php echo $entry_firstname; ?></td>
                  <?php } ?>
                <td><input type="text" name="firstname" value="<?php echo $firstname; ?>"  id="rest_first"/>
                  <?php if ($error_firstname) { ?>
                  <span class="error"><?php echo $error_firstname; ?></span>
                  <?php } ?></td>
              </tr>
              <tr>
                  <?php if($this->session->data['destination'] == 'rest') {?>
                  <td> <?php echo 'סלוגן:'; ?></td>
                 <?php }else{?>
                <td><span class="required">*</span> <?php echo $entry_lastname; ?></td>
                  <?php } ?>
                <td><input type="text" name="lastname" value="<?php echo $lastname; ?>"  id="rest_last"/>
                  <?php if ($error_lastname) { ?>
                  <span class="error"><?php echo $error_lastname; ?></span>
                  <?php } ?></td>
              </tr>
              <tr>
                <td> <?php echo $entry_email; ?></td>
                <td><input type="text" name="email" value="<?php echo $email; ?>" />
                  <?php if ($error_email) { ?>
                  <span class="error"><?php echo $error_email; ?></span>
                  <?php  } ?></td>
              </tr>
              
              <tr class="rest_no">
                <td><?php echo $entry_sex; ?></td>
                <td><select name="sex">
                    <?php if ($sex) { ?>
                    <option value="1" selected="selected"><?php echo $text_male; ?></option>
                    <option value="0"><?php echo $text_female; ?></option>
                    <?php } else { ?>
                    <option value="1"><?php echo $text_male; ?></option>
                    <option value="0" selected="selected"><?php echo $text_female; ?></option>
                    <?php } ?>
                  </select></td>
              </tr>
              <tr class="rest_no">
                <td><?php echo $entry_birth_day; ?></td>
                <td><input type="text" name="birth_day" value="<?php echo $birth_day; ?>" size="12" class="date" /></td>
              </tr>
              <tr>
                <td><?php echo $entry_description; ?></td>
                <td><textarea name="profile_description" cols=100 rows=10><?php echo isset($profile_description) ? $profile_description : ''; ?></textarea></td>
              </tr>
              <tr>
                <td><?php echo $entry_sp; ?></td>
                <td><input type="text" name="sp" value="<?php echo $sp; ?>" /></td>
              </tr>
              <tr>
                <td><?php echo $entry_sms_aproved; ?></td>
                <td><select name="sms_aproved">
                    <?php if ($sms_aproved) { ?>
                    <option value="1" selected="selected"><?php echo $text_enabled; ?></option>
                    <option value="0"><?php echo $text_disabled; ?></option>
                    <?php } else { ?>
                    <option value="1"><?php echo $text_enabled; ?></option>
                    <option value="0" selected="selected"><?php echo $text_disabled; ?></option>
                    <?php } ?>
                  </select></td>
              </tr>
              <tr>
                <td><?php echo $entry_image; ?></td>
                <td><div class="image"><img src="<?php echo $thumb; ?>" alt="" id="thumb" /><br />
                    <input type="hidden" name="image" value="<?php echo $image; ?>" id="image" />
                    <a onclick="image_upload('image', 'thumb');"><?php echo $text_browse; ?></a>&nbsp;&nbsp;|&nbsp;&nbsp;<a onclick="$('#thumb').attr('src', '<?php echo $no_image; ?>'); $('#image').attr('value', '');"><?php echo $text_clear; ?></a></div></td>
              </tr>
              
              <tr>
                <td><span class="required">*</span> <?php echo $entry_telephone; ?></td>
                <td><input type="text" name="telephone" value="<?php echo $telephone; ?>" />
                  <?php if ($error_telephone) { ?>
                  <span class="error"><?php echo $error_telephone; ?></span>
                  <?php  } ?></td>
              </tr>
              <tr>
                <td><?php echo $entry_fax; ?></td>
                <td>
                  <!-- fake fields are a workaround for chrome autofill getting the wrong fields -->
                  <input style="display:none" type="text" name="fakeusernameremembered"/>
                  <input style="display:none" type="password" name="fakepasswordremembered"/>
                  <input type="text" name="fax" value="<?php echo $fax; ?>" /></td>
              </tr>
                
                       <!--  cities table -->        
        <tr class="cust_no">
            <td>עלות משלוח:</td>
            <td>     
                <table id="cities_table" border="1">
                    <tr>
                        <th>עיר</th>
                        <th>מינימום הזמנה</th>
                         <th>עלות הזמנה למשלוח חינם</th>
                        <th>דמי משלוח</th>
                        <th>זמן נסיעה</th>
                    </tr>
                   <?php
                      if(!empty($city)){
                          foreach($city as $key=>$value){
                               $name_city = "city";
                               $name_order = "min_order";
                               $name_deliver = "free_delivery";
                               $name_pay = "pay";
                               $name_time = "time";
                               $city = $value['city'];
                               $min_order = $value['min_order'];
                               $free_delivery = $value['free_delivery'];
                               $pay = $value['pay'];
                               $time = $value['time'];
                               $output= '<tr class="label-' . $key .'">';
                            $output .= '<td><input name="city[' . $key.'][' . $name_city . ']" value="'.$city .                                 '" readonly></td>';
                           $output .= '<td><input name="city[' . $key.'][' . $name_order . ']" value="'.$min_order                                          . '"></td>';
                           $output .= '<td><input name="city[' . $key.'][' . $name_deliver . ']"                                               value="'.$free_delivery .'"></td>';
        $output .= '<td><input name="city[' . $key.'][' . $name_pay . ']"                                               value="'.$pay .'"></td>';
        $output .= '<td><input name="city[' . $key.'][' . $name_time . ']"                                               value="'.$time .'"></td>';
                            $output .= '</tr>';
                            echo $output;
                                    }


                               }


                      ?>
                </table>
            
            </td>
            
        </tr>
                
       <!-- end of cities table --> 
        <!-- user password -->
            <?php if($this->session->data['destination'] == 'rest') {?>    
              <tr class="rest_no">
                <td><?php echo $entry_password; ?></td>
                <td><input type="password" name="password" autocomplete="off" value="12345"  />
                  <?php if ($error_password) { ?>
                  <span class="error"><?php echo $error_password; ?></span>
                  <?php  } ?></td>
              </tr>
              <tr class="rest_no">
                <td><?php echo $entry_confirm; ?></td>
                <td><input type="password" name="confirm" value="12345" />
                  <?php if ($error_confirm) { ?>
                  <span class="error"><?php echo $error_confirm; ?></span>
                  <?php  } ?></td>
              </tr>
                <?php }else{ ?>
                
                <tr>
                <td><?php echo $entry_password; ?></td>
                <td><input type="password" name="password" autocomplete="off" value="<?php echo $password; ?>"  />
                  <?php if ($error_password) { ?>
                  <span class="error"><?php echo $error_password; ?></span>
                  <?php  } ?></td>
              </tr>
              <tr>
                <td><?php echo $entry_confirm; ?></td>
                <td><input type="password" name="confirm" value="<?php echo $confirm; ?>" />
                  <?php if ($error_confirm) { ?>
                  <span class="error"><?php echo $error_confirm; ?></span>
                  <?php  } ?></td>               
                
                <?php } ?>
                
                <!-- end of user password -->
                  
              <tr>
                <td><?php echo $entry_newsletter; ?></td>
                <td><select name="newsletter">
                    <?php if ($newsletter) { ?>
                    <option value="1" selected="selected"><?php echo $text_enabled; ?></option>
                    <option value="0"><?php echo $text_disabled; ?></option>
                    <?php } else { ?>
                    <option value="1"><?php echo $text_enabled; ?></option>
                    <option value="0" selected="selected"><?php echo $text_disabled; ?></option>
                    <?php } ?>
                  </select></td>
              </tr>
            <!-- customer group -->
              <tr class="rest_no">
                <td><?php echo $entry_customer_group; ?></td>
                <td><select name="customer_group_id">
                  <?php if($this->session->data['destination'] == 'rest'){ ?>
                    <option value="1">לקוח</option>
                    <option value="2" selected="selected">מסעדה</option>
                    <?php }else{ ?>
                     <option value="1" selected="selected">לקוח</option>
                    <option value="2">מסעדה</option>
                    <?php } ?>
                  </select></td>
              </tr>
                <!-- end of customer group -->
              <tr>
                <td><?php echo $entry_status; ?></td>
                <td><select name="status">
                    <?php if ($status) { ?>
                    <option value="1" selected="selected"><?php echo $text_enabled; ?></option>
                    <option value="0"><?php echo $text_disabled; ?></option>
                    <?php } else { ?>
                    <option value="1"><?php echo $text_enabled; ?></option>
                    <option value="0" selected="selected"><?php echo $text_disabled; ?></option>
                    <?php } ?>
                  </select></td>
              </tr>
                
                
            </table>
              
        <!-- end of costumer table -->
          </div>
          <?php $address_row = 1; ?>
          <?php foreach ($addresses as $address) { ?>
          <div id="tab-address-<?php echo $address_row; ?>" class="vtabs-content">
            <input type="hidden" name="address[<?php echo $address_row; ?>][address_id]" value="<?php echo $address['address_id']; ?>" />
            <table class="form">
              <tr>
                <td><span class="required">*</span> <?php echo $entry_firstname; ?></td>
                <td><input type="text" name="address[<?php echo $address_row; ?>][firstname]" value="<?php echo $address['firstname']; ?>" />
                  <?php if (isset($error_address_firstname[$address_row])) { ?>
                  <span class="error"><?php echo $error_address_firstname[$address_row]; ?></span>
                  <?php } ?></td>
              </tr>
              <tr>
                <td><span class="required">*</span> <?php echo $entry_lastname; ?></td>
                <td><input type="text" name="address[<?php echo $address_row; ?>][lastname]" value="<?php echo $address['lastname']; ?>" />
                  <?php if (isset($error_address_lastname[$address_row])) { ?>
                  <span class="error"><?php echo $error_address_lastname[$address_row]; ?></span>
                  <?php } ?></td>
              </tr>
              <tr>
                <td><?php echo $entry_company; ?></td>
                <td><input type="text" name="address[<?php echo $address_row; ?>][company]" value="<?php echo $address['company']; ?>" /></td>
              </tr>
              <tr class="company-id-display">
                <td><?php echo $entry_company_id; ?></td>
                <td><input type="text" name="address[<?php echo $address_row; ?>][company_id]" value="<?php echo $address['company_id']; ?>" /></td>
              </tr>
              <tr class="tax-id-display">
                <td><?php echo $entry_tax_id; ?></td>
                <td><input type="text" name="address[<?php echo $address_row; ?>][tax_id]" value="<?php echo $address['tax_id']; ?>" />
                  <?php if (isset($error_address_tax_id[$address_row])) { ?>
                  <span class="error"><?php echo $error_address_tax_id[$address_row]; ?></span>
                  <?php } ?></td>
              </tr>
              <tr>
                <td><span class="required">*</span> <?php echo $entry_address_1; ?></td>
                <td><input type="text" name="address[<?php echo $address_row; ?>][address_1]" value="<?php echo $address['address_1']; ?>" />
                  <?php if (isset($error_address_address_1[$address_row])) { ?>
                  <span class="error"><?php echo $error_address_address_1[$address_row]; ?></span>
                  <?php } ?></td>
              </tr>
              <tr>
                <td><?php echo $entry_address_2; ?></td>
                <td><input type="text" name="address[<?php echo $address_row; ?>][address_2]" value="<?php echo $address['address_2']; ?>" /></td>
              </tr>
              <tr>
                <td><span class="required">*</span> <?php echo $entry_city; ?></td>
                <td><input type="text" name="address[<?php echo $address_row; ?>][city]" value="<?php echo $address['city']; ?>" />
                  <?php if (isset($error_address_city[$address_row])) { ?>
                  <span class="error"><?php echo $error_address_city[$address_row]; ?></span>
                  <?php } ?></td>
              </tr>
              <tr>
                <td><span id="postcode-required<?php echo $address_row; ?>" class="required">*</span> <?php echo $entry_postcode; ?></td>
                <td><input type="text" name="address[<?php echo $address_row; ?>][postcode]" value="<?php echo $address['postcode']; ?>" /></td>
              </tr>
              <tr>
                <td><span class="required">*</span> <?php echo $entry_country; ?></td>
                <td><select name="address[<?php echo $address_row; ?>][country_id]" onchange="country(this, '<?php echo $address_row; ?>', '<?php echo $address['zone_id']; ?>');">
                    <option value=""><?php echo $text_select; ?></option>
                    <?php foreach ($countries as $country) { ?>
                    <?php if ($country['country_id'] == $address['country_id']) { ?>
                    <option value="<?php echo $country['country_id']; ?>" selected="selected"><?php echo $country['name']; ?></option>
                    <?php } else { ?>
                    <option value="<?php echo $country['country_id']; ?>"><?php echo $country['name']; ?></option>
                    <?php } ?>
                    <?php } ?>
                  </select>
                  <?php if (isset($error_address_country[$address_row])) { ?>
                  <span class="error"><?php echo $error_address_country[$address_row]; ?></span>
                  <?php } ?></td>
              </tr>
              <tr>
                <td><span class="required">*</span> <?php echo $entry_zone; ?></td>
                <td><select name="address[<?php echo $address_row; ?>][zone_id]">
                  </select>
                  <?php if (isset($error_address_zone[$address_row])) { ?>
                  <span class="error"><?php echo $error_address_zone[$address_row]; ?></span>
                  <?php } ?></td>
              </tr>
              <tr>
                <td><?php echo $entry_default; ?></td>
                <td><?php if (($address['address_id'] == $address_id) || !$addresses) { ?>
                  <input type="radio" name="address[<?php echo $address_row; ?>][default]" value="<?php echo $address_row; ?>" checked="checked" /></td>
                <?php } else { ?>
                <input type="radio" name="address[<?php echo $address_row; ?>][default]" value="<?php echo $address_row; ?>" />
                  </td>
                <?php } ?>
              </tr>
            </table>
          </div>
          <?php $address_row++; ?>
          <?php } ?>
          
              
                 
        </div>
        <?php if ($customer_id) { ?>
        <div id="tab-history" class="rest_no cust_no">
          <div id="history"></div>
          <table class="form">
            <tr>
              <td><?php echo $entry_comment; ?></td>
              <td><textarea name="comment" cols="40" rows="8" style="width: 99%;"></textarea></td>
            </tr>
            <tr>
              <td colspan="2" style="text-align: right;"><a id="button-history" class="button"><span><?php echo $button_add_history; ?></span></a></td>
            </tr>
          </table>
        </div>
        <div id="tab-transaction">
          <table class="form">
            <tr>
              <td><?php echo $entry_description; ?></td>
              <td><input type="text" name="description" value="" /></td>
            </tr>
            <tr>
              <td><?php echo $entry_amount; ?></td>
              <td><input type="text" name="amount" value="" /></td>
            </tr>
            <tr>
              <td colspan="2" style="text-align: right;"><a id="button-transaction" class="button" onclick="addTransaction();"><span><?php echo $button_add_transaction; ?></span></a></td>
            </tr>
          </table>
          <div id="transaction"></div>
        </div>
        <div id="tab-reward" class="rest_no cust_no">
          <table class="form">
            <tr>
              <td><?php echo $entry_description; ?></td>
              <td><input type="text" name="description" value="" /></td>
            </tr>
            <tr>
              <td><?php echo $entry_points; ?></td>
              <td><input type="text" name="points" value="" /></td>
            </tr>
            <tr>
              <td colspan="2" style="text-align: right;"><a id="button-reward" class="button" onclick="addRewardPoints();"><span><?php echo $button_add_reward; ?></span></a></td>
            </tr>
          </table>
          <div id="reward"></div>
        </div>
        <?php } ?>
        <div id="tab-ip" class="diss_no">
          <table class="list">
            <thead>
              <tr>
                <td class="left"><?php echo $column_ip; ?></td>
                <td class="right"><?php echo $column_total; ?></td>
                <td class="left"><?php echo $column_date_added; ?></td>
                <td class="right"><?php echo $column_action; ?></td>
              </tr>
            </thead>
            <tbody>
              <?php if ($ips) { ?>
              <?php foreach ($ips as $ip) { ?>
              <tr>
                <td class="left"><a href="http://www.geoiptool.com/en/?IP=<?php echo $ip['ip']; ?>" target="_blank"><?php echo $ip['ip']; ?></a></td>
                <td class="right"><a href="<?php echo $ip['filter_ip']; ?>" target="_blank"><?php echo $ip['total']; ?></a></td>
                <td class="left"><?php echo $ip['date_added']; ?></td>
                <td class="right"><?php if ($ip['ban_ip']) { ?>
                  <b>[</b> <a id="<?php echo str_replace('.', '-', $ip['ip']); ?>" onclick="removeBanIP('<?php echo $ip['ip']; ?>');"><?php echo $text_remove_ban_ip; ?></a> <b>]</b>
                  <?php } else { ?>
                  <b>[</b> <a id="<?php echo str_replace('.', '-', $ip['ip']); ?>" onclick="addBanIP('<?php echo $ip['ip']; ?>');"><?php echo $text_add_ban_ip; ?></a> <b>]</b>
                  <?php } ?></td>
              </tr>
              <?php } ?>
              <?php } else { ?>
              <tr>
                <td class="center" colspan="4"><?php echo $text_no_results; ?></td>
              </tr>
              <?php } ?>
            </tbody>
          </table>
        </div>
      <!--   availability form  -->
      <div  id="availability">
          <h1>זמינות שבועית של מסעדה</h1>
      <table id="my-availability" border="1">
         <tr>
              <th>יום</th>
               <th>שעת פתיחה</th>
               <th>שעת סגירה</th>
          </tr>
          <?php if(empty($days)) {?>
          <tr>
              <td><input type="text" name="days[1][day]" value="ראשון" readonly></td>
               <td><input type="text" name="days[1][start]"></td>
               <td><input type="text" name="days[1][close]"></td>
          </tr>
           <tr>
              <td><input type="text" name="days[2][day]" value="שני" readonly></td>
               <td><input type="text" name="days[2][start]"></td>
               <td><input type="text" name="days[2][close]"></td>
          </tr>
         <tr>
            <td><input type="text" name="days[3][day]" value="שלישי" readonly></td>
               <td><input type="text" name="days[3][start]"></td>
               <td><input type="text" name="days[3][close]"></td>
          </tr>
            <tr>
              <td><input type="text" name="days[4][day]" value="רביעי" readonly></td>
               <td><input type="text" name="days[4][start]"></td>
               <td><input type="text" name="days[4][close]"></td>
          </tr>
          <tr>
              <td><input type="text" name="days[5][day]" value="חמישי" readonly></td>
               <td><input type="text" name="days[5][start]"></td>
               <td><input type="text" name="days[5][close]"></td>
          </tr>
            <tr>
              <td><input type="text" name="days[6][day]" value="שישי" readonly></td>
               <td><input type="text" name="days[6][start]"></td>
               <td><input type="text" name="days[6][close]"></td>
          </tr>
            <tr>
              <td><input type="text" name="days[7][day]" value="שבת" readonly></td>
              <td><input type="text" name="days[7][start]"></td>
             <td><input type="text" name="days[7][close]"></td>
          </tr>
        <?php }else {
          foreach($days as $key=>$value){

            $d = $value['day'];
            $start = $value['start'];
             $close = $value['close'];
             $output_d = "<tr>";
             $output_d .= '<td><input type="text" name="days[' . $key . '][day]" value="' . $d . '" readonly>                                </td>';
             $output_d .= '<td><input type="text" name="days[' . $key . '][start]" value="' . $start . '"></td>';
            $output_d .= '<td><input type="text" name="days[' . $key . '][close]" value="' . $close . '"></td>';
             $output_d .= "</tr>";
             echo $output_d;
                    } 



            } 
           ?>
      </table>
   <!-- end of days table -->   
      </div>
      <!-- start of menu categories -->
      <div  id="menu_cat">
         
        <h1>קטגוריות של תפריט</h1>
          
          <table id="my_menu" border="1">
             <tr id="0">
                 <th>שם</th>
                 <th>שעת התחלה</th>
                 <th>שעת סיום</th>
                 <th>עמלה</th>
                 <th>הערות</th>                
             </tr>
          <?php
             if(!empty($menu_cat)){
                 foreach($menu_cat as $key=>$value){

                           $name = $value['day'];
                           $start = $value['start_time'];
                           $end = $value['end_time'];
                           $comm = $value['commision'];
                           $ref = $value['say'];
                           $output = '<tr id="'. $key .'">';
                           $output .= '<td><input type="text" name="menu_cat[' . $key .'][day]" value="' .    $value['day'].'"></td>';
                           $output .= '<td><input type="text" name="menu_cat[' . $key .'][start_time]" value="'.$value['start_time'].'"></td>';
                           $output .= '<td><input type="text" name="menu_cat[' . $key .'][end_time]" value="'.$value['end_time'].'"></td>';
                           $output .= '<td><input type="text" name="menu_cat[' . $key .'][commision]" value="'. $value['commision'].'"></td>';
                           $output .= '<td><input type="text" name="menu_cat[' . $key .'][say]" value="'.$value['say'].'"></td>';
                           $output .= '</tr>';
                           echo $output;
                          }


                  }

             ?>
          </table>
          <br>
          <a href="#" id="my-row" class="button">הוסף שורה</a>
      
      </div>   
      
    <!-- end of menu categories -->
            <!-- start of contact -->
    <div id="conatct">
        <h1>אנשי קשר</h1>
        <table id="contact-table" border="1">
            <tr>
                <th>שם</th>
                <th>אימייל</th>
                <th>מספר</th>
            </tr>
            <?php
              if(!empty($person)){
               foreach($person as $key=>$value){
                 $noutput = '<tr>';
                 $noutput .= '<td><input type="text" name="person[' . $key . '][name]" value="' . $value['name']    .'"></td>';
                 $noutput .= '<td><input type="text" name="person[' . $key . '][email]" value="' . $value['email']    .'"></td>';
                 $noutput .= '<td><input type="text" name="person[' . $key . '][phone]" value="' . $value['phone']    .'"></td>';
                  $noutput .= '</tr>';
                  echo $noutput;
                 }

               }

             ?>
        </table>
        <br>
        <a href="#" id="new-row" class="button">הוסף שורה</a>
    </div>
    <!-- end of contact -->
      <?php if ($customer_id) { ?>  
    <!--add new product -->
        <div id="addProd">
        <tr> 
         <td>יצירת מוצר</td>
         <td>&nbsp;&nbsp;<a href="#" class="button" id="create-product">הוספה</a></td>     
              
          </tr>
        
        </div>
        
      <!--end of new product -->
     <?php } ?>
  <!-- subscription table -->   
     <div id="subscription">
     <h1>דמי מנוי</h1>
     <table id="subscription_table" border="1">
     <tr>
     <th>עלות דמי מנוי</th>
     <th>מסכום</th>
     <th>עד סכום</th>
     </tr>
        <?php
       if(!empty($subs)){
        foreach($subs as $key=>$value){
        $output = '<tr>';   
        $output .= '<td><input type="text" name="subs[' . $key . '][number]" value="' . $value['number'] . '"></td>';
        $output .= '<td><input type="text" name="subs[' . $key . '][min_pay]" value="' . $value['min_pay'] . '"></td>';
        $output .= '<td><input type="text" name="subs[' . $key . '][max_pay]" value="' . $value['max_pay'] . '"></td>';
       $output .= '</tr>';
       echo $output;
          }


          }
        ?> 
     </table>  
     <br>
       <a href="#" id="subs-row" class="button">הוסף שורה</a>
        
     </div>
     <!-- end of subscription table -->  
        
        <!-- start of comission table -->  
        <div id="comission">
        <h1>עמלה</h1>
        <table id="table-commission" border="1">
           <tr> 
               <th>אחוז עמלה</th>
               <th>מסכום</th>
               <th>עד סכום</th>
           </tr>
      <?php
       if(!empty($com)){
        foreach($com as $key=>$value){
        $output = '<tr>';   
        $output .= '<td><input type="text" name="com[' . $key . '][commission]" value="' . $value['commission'] . '"></td>';
        $output .= '<td><input type="text" name="com[' . $key . '][min_pay]" value="' . $value['min_pay'] . '"></td>';
        $output .= '<td><input type="text" name="com[' . $key . '][max_pay]" value="' . $value['max_pay'] . '"></td>';
       $output .= '</tr>';
       echo $output;
          }


          }
        ?>
        </table>
       <br>
       <a href="#" id="commission-row" class="button">הוסף שורה</a> 
            
        
        </div>
        <!-- end of comission table -->  
        <div id="tab-image"> 
          <table id="images" class="list">
            <thead>
              <tr>
                <td class="left"><?php echo $entry_image; ?></td>
                <td class="right"><?php echo $entry_sort_order; ?></td>
                <td></td>
              </tr>
            </thead>
            <?php $image_row = 0; ?>
            <?php foreach ($customer_images as $customer_image) { ?>
            <tbody id="image-row<?php echo $image_row; ?>">
              <tr>
                <td class="left"><div class="image"><img src="<?php echo $customer_image['thumb']; ?>" alt="" id="thumb<?php echo $image_row; ?>" />
                    <input type="hidden" name="customer_image[<?php echo $image_row; ?>][image]" value="<?php echo $customer_image['image']; ?>" id="image<?php echo $image_row; ?>" />
                    <br />
                    <a onclick="image_upload('image<?php echo $image_row; ?>', 'thumb<?php echo $image_row; ?>');"><?php echo $text_browse; ?></a>&nbsp;&nbsp;|&nbsp;&nbsp;<a onclick="$('#thumb<?php echo $image_row; ?>').attr('src', '<?php echo $no_image; ?>'); $('#image<?php echo $image_row; ?>').attr('value', '');"><?php echo $text_clear; ?></a></div></td>
                <td class="right"><input type="text" name="customer_image[<?php echo $image_row; ?>][sort_order]" value="<?php echo $customer_image['sort_order']; ?>" size="2" /></td>
                <td class="left"><a onclick="$('#image-row<?php echo $image_row; ?>').remove();" class="button"><?php echo $button_remove; ?></a></td>
              </tr>
            </tbody>
            <?php $image_row++; ?>
            <?php } ?>
            <tfoot>
              <tr>
                <td colspan="2"></td>
                <td class="left"><a onclick="addImage();" class="button"><?php echo $button_add_image; ?></a></td>
              </tr>
            </tfoot>
          </table>
        </div>
        
      </form>
    </div>
  </div>
</div>

<?php // new for homeals ?>
<script type="text/javascript" src="view/javascript/ckeditor/ckeditor.js"></script> 

<script type="text/javascript"><!--
CKEDITOR.replace('description', {
	filebrowserBrowseUrl: 'index.php?route=common/filemanager&token=<?php echo $token; ?>',
	filebrowserImageBrowseUrl: 'index.php?route=common/filemanager&token=<?php echo $token; ?>',
	filebrowserFlashBrowseUrl: 'index.php?route=common/filemanager&token=<?php echo $token; ?>',
	filebrowserUploadUrl: 'index.php?route=common/filemanager&token=<?php echo $token; ?>',
	filebrowserImageUploadUrl: 'index.php?route=common/filemanager&token=<?php echo $token; ?>',
	filebrowserFlashUploadUrl: 'index.php?route=common/filemanager&token=<?php echo $token; ?>'
});
//--></script> 
<script type="text/javascript"><!--
function image_upload(field, thumb) {
	$('#dialog').remove();
	
	$('#content').prepend('<div id="dialog" style="padding: 3px 0px 0px 0px;"><iframe src="index.php?route=common/filemanager&token=<?php echo $token; ?>&field=' + encodeURIComponent(field) + '" style="padding:0; margin: 0; display: block; width: 100%; height: 100%;" frameborder="no" scrolling="auto"></iframe></div>');
	
	$('#dialog').dialog({
		title: '<?php echo $text_image_manager; ?>',
		close: function (event, ui) {
			if ($('#' + field).attr('value')) {
				$.ajax({
					url: 'index.php?route=common/filemanager/image&token=<?php echo $token; ?>&image=' + encodeURIComponent($('#' + field).attr('value')),
					dataType: 'text',
					success: function(text) {
						$('#' + thumb).replaceWith('<img src="' + text + '" alt="" id="' + thumb + '" />');
					}
				});
			}
		},	
		bgiframe: false,
		width: 800,
		height: 400,
		resizable: false,
		modal: false
	});
};
//--></script> 

<script type="text/javascript" src="view/javascript/jquery/ui/jquery-ui-timepicker-addon.js"></script> 
<script type="text/javascript"><!--
$('.date').datepicker({dateFormat: 'yy-mm-dd'});
$('.datetime').datetimepicker({
	dateFormat: 'yy-mm-dd',
	timeFormat: 'h:m'
});
$('.time').timepicker({timeFormat: 'h:m'});
//--></script> 

<script type="text/javascript"><!--
$('select[name=\'customer_group_id\']').live('change', function() {
	var customer_group = [];
	
<?php foreach ($customer_groups as $customer_group) { ?>
	customer_group[<?php echo $customer_group['customer_group_id']; ?>] = [];
	customer_group[<?php echo $customer_group['customer_group_id']; ?>]['company_id_display'] = '<?php echo $customer_group['company_id_display']; ?>';
	customer_group[<?php echo $customer_group['customer_group_id']; ?>]['tax_id_display'] = '<?php echo $customer_group['tax_id_display']; ?>';
<?php } ?>	

	if (customer_group[this.value]) {
		if (customer_group[this.value]['company_id_display'] == '1') {
			$('.company-id-display').show();
		} else {
			$('.company-id-display').hide();
		}
		
		if (customer_group[this.value]['tax_id_display'] == '1') {
			$('.tax-id-display').show();
		} else {
			$('.tax-id-display').hide();
		}
	}
});

$('select[name=\'customer_group_id\']').trigger('change');
//--></script> 
<script type="text/javascript"><!--
function country(element, index, zone_id) {
  if (element.value != '') {
		$.ajax({
			url: 'index.php?route=sale/customer/country&token=<?php echo $token; ?>&country_id=' + element.value,
			dataType: 'json',
			beforeSend: function() {
				$('select[name=\'address[' + index + '][country_id]\']').after('<span class="wait">&nbsp;<img src="view/image/loading.gif" alt="" /></span>');
			},
			complete: function() {
				$('.wait').remove();
			},			
			success: function(json) {
				if (json['postcode_required'] == '1') {
					$('#postcode-required' + index).show();
				} else {
					$('#postcode-required' + index).hide();
				}
				
				html = '<option value=""><?php echo $text_select; ?></option>';
				
				if (json['zone'] != '') {
					for (i = 0; i < json['zone'].length; i++) {
						html += '<option value="' + json['zone'][i]['zone_id'] + '"';
						
						if (json['zone'][i]['zone_id'] == zone_id) {
							html += ' selected="selected"';
						}
		
						html += '>' + json['zone'][i]['name'] + '</option>';
					}
				} else {
					html += '<option value="0"><?php echo $text_none; ?></option>';
				}
				
				$('select[name=\'address[' + index + '][zone_id]\']').html(html);
			},
			error: function(xhr, ajaxOptions, thrownError) {
				alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
			}
		});
	}
}

$('select[name$=\'[country_id]\']').trigger('change');
//--></script> 
<script type="text/javascript"><!--
var address_row = <?php echo $address_row; ?>;

function addAddress() {	
	html  = '<div id="tab-address-' + address_row + '" class="vtabs-content" style="display: none;">';
	html += '  <input type="hidden" name="address[' + address_row + '][address_id]" value="" />';
	html += '  <table class="form">'; 
	html += '    <tr>';
    html += '	   <td><span class="required">*</span> <?php echo $entry_firstname; ?></td>';
    html += '	   <td><input type="text" name="address[' + address_row + '][firstname]" value="" /></td>';
    html += '    </tr>';
    html += '    <tr>';
    html += '      <td><span class="required">*</span> <?php echo $entry_lastname; ?></td>';
    html += '      <td><input type="text" name="address[' + address_row + '][lastname]" value="" /></td>';
    html += '    </tr>';
    html += '    <tr>';
    html += '      <td><?php echo $entry_company; ?></td>';
    html += '      <td><input type="text" name="address[' + address_row + '][company]" value="" /></td>';
    html += '    </tr>';	
    html += '    <tr class="company-id-display">';
    html += '      <td><?php echo $entry_company_id; ?></td>';
    html += '      <td><input type="text" name="address[' + address_row + '][company_id]" value="" /></td>';
    html += '    </tr>';
    html += '    <tr class="tax-id-display">';
    html += '      <td><?php echo $entry_tax_id; ?></td>';
    html += '      <td><input type="text" name="address[' + address_row + '][tax_id]" value="" /></td>';
    html += '    </tr>';			
    html += '    <tr>';
    html += '      <td><span class="required">*</span> <?php echo $entry_address_1; ?></td>';
    html += '      <td><input type="text" name="address[' + address_row + '][address_1]" value="" /></td>';
    html += '    </tr>';
    html += '    <tr>';
    html += '      <td><?php echo $entry_address_2; ?></td>';
    html += '      <td><input type="text" name="address[' + address_row + '][address_2]" value="" /></td>';
    html += '    </tr>';
    html += '    <tr>';
    html += '      <td><span class="required">*</span> <?php echo $entry_city; ?></td>';
    html += '      <td><input type="text" name="address[' + address_row + '][city]" value="" /></td>';
    html += '    </tr>';
    html += '    <tr>';
    html += '      <td><span id="postcode-required' + address_row + '" class="required">*</span> <?php echo $entry_postcode; ?></td>';
    html += '      <td><input type="text" name="address[' + address_row + '][postcode]" value="" /></td>';
    html += '    </tr>';
	html += '    <tr>';
    html += '      <td><span class="required">*</span> <?php echo $entry_country; ?></td>';
    html += '      <td><select name="address[' + address_row + '][country_id]" onchange="country(this, \'' + address_row + '\', \'0\');">';
    html += '         <option value=""><?php echo $text_select; ?></option>';
    <?php foreach ($countries as $country) { ?>
    html += '         <option value="<?php echo $country['country_id']; ?>"><?php echo addslashes($country['name']); ?></option>';
    <?php } ?>
    html += '      </select></td>';
    html += '    </tr>';
    html += '    <tr>';
    html += '      <td><span class="required">*</span> <?php echo $entry_zone; ?></td>';
    html += '      <td><select name="address[' + address_row + '][zone_id]"><option value="false"><?php echo $this->language->get('text_none'); ?></option></select></td>';
    html += '    </tr>';
	html += '    <tr>';
    html += '      <td><?php echo $entry_default; ?></td>';
    html += '      <td><input type="radio" name="address[' + address_row + '][default]" value="1" /></td>';
    html += '    </tr>';
    html += '  </table>';
    html += '</div>';
	
	$('#tab-general').append(html);
	
	$('select[name=\'address[' + address_row + '][country_id]\']').trigger('change');	
	
	$('#address-add').before('<a href="#tab-address-' + address_row + '" id="address-' + address_row + '"><?php echo "איש קשר "; ?>' + address_row + '&nbsp;<img src="view/image/delete.png" alt="" onclick="$(\'#vtabs a:first\').trigger(\'click\'); $(\'#address-' + address_row + '\').remove(); $(\'#tab-address-' + address_row + '\').remove(); return false;" /></a>');
		 
	$('.vtabs a').tabs();
	
	$('#address-' + address_row).trigger('click');
	
	address_row++;
}
//--></script> 
<script type="text/javascript"><!--
$('#history .pagination a').live('click', function() {
	$('#history').load(this.href);
	
	return false;
});			

$('#history').load('index.php?route=sale/customer/history&token=<?php echo $token; ?>&customer_id=<?php echo $customer_id; ?>');

$('#button-history').bind('click', function() {
	$.ajax({
		url: 'index.php?route=sale/customer/history&token=<?php echo $token; ?>&customer_id=<?php echo $customer_id; ?>',
		type: 'post',
		dataType: 'html',
		data: 'comment=' + encodeURIComponent($('#tab-history textarea[name=\'comment\']').val()),
		beforeSend: function() {
			$('.success, .warning').remove();
			$('#button-history').attr('disabled', true);
			$('#history').before('<div class="attention"><img src="view/image/loading.gif" alt="" /> <?php echo $text_wait; ?></div>');
		},
		complete: function() {
			$('#button-history').attr('disabled', false);
			$('.attention').remove();
      		$('#tab-history textarea[name=\'comment\']').val('');
		},
		success: function(html) {
			$('#history').html(html);
			
			$('#tab-history input[name=\'comment\']').val('');
		}
	});
});
//--></script>
<script type="text/javascript"><!--
var image_row = <?php echo $image_row; ?>;

function addImage() {
    html  = '<tbody id="image-row' + image_row + '">';
	html += '  <tr>';
	html += '    <td class="left"><div class="image"><img src="<?php echo $no_image; ?>" alt="" id="thumb' + image_row + '" /><input type="hidden" name="customer_image[' + image_row + '][image]" value="" id="image' + image_row + '" /><br /><a onclick="image_upload(\'image' + image_row + '\', \'thumb' + image_row + '\');"><?php echo $text_browse; ?></a>&nbsp;&nbsp;|&nbsp;&nbsp;<a onclick="$(\'#thumb' + image_row + '\').attr(\'src\', \'<?php echo $no_image; ?>\'); $(\'#image' + image_row + '\').attr(\'value\', \'\');"><?php echo $text_clear; ?></a></div></td>';
	html += '    <td class="right"><input type="text" name="customer_image[' + image_row + '][sort_order]" value="" size="2" /></td>';
	html += '    <td class="left"><a onclick="$(\'#image-row' + image_row  + '\').remove();" class="button"><?php echo $button_remove; ?></a></td>';
	html += '  </tr>';
	html += '</tbody>';
	
	$('#images tfoot').before(html);
	
	image_row++;
}
//--></script> 
<script type="text/javascript"><!--
$('#transaction .pagination a').live('click', function() {
	$('#transaction').load(this.href);
	
	return false;
});			

$('#transaction').load('index.php?route=sale/customer/transaction&token=<?php echo $token; ?>&customer_id=<?php echo $customer_id; ?>');

$('#button-transaction').bind('click', function() {
	$.ajax({
		url: 'index.php?route=sale/customer/transaction&token=<?php echo $token; ?>&customer_id=<?php echo $customer_id; ?>',
		type: 'post',
		dataType: 'html',
		data: 'description=' + encodeURIComponent($('#tab-transaction input[name=\'description\']').val()) + '&amount=' + encodeURIComponent($('#tab-transaction input[name=\'amount\']').val()),
		beforeSend: function() {
			$('.success, .warning').remove();
			$('#button-transaction').attr('disabled', true);
			$('#transaction').before('<div class="attention"><img src="view/image/loading.gif" alt="" /> <?php echo $text_wait; ?></div>');
		},
		complete: function() {
			$('#button-transaction').attr('disabled', false);
			$('.attention').remove();
		},
		success: function(html) {
			$('#transaction').html(html);
			
			$('#tab-transaction input[name=\'amount\']').val('');
			$('#tab-transaction input[name=\'description\']').val('');
		}
	});
});
//--></script> 
<script type="text/javascript"><!--
$('#reward .pagination a').live('click', function() {
	$('#reward').load(this.href);
	
	return false;
});			

$('#reward').load('index.php?route=sale/customer/reward&token=<?php echo $token; ?>&customer_id=<?php echo $customer_id; ?>');

function addRewardPoints() {
	$.ajax({
		url: 'index.php?route=sale/customer/reward&token=<?php echo $token; ?>&customer_id=<?php echo $customer_id; ?>',
		type: 'post',
		dataType: 'html',
		data: 'description=' + encodeURIComponent($('#tab-reward input[name=\'description\']').val()) + '&points=' + encodeURIComponent($('#tab-reward input[name=\'points\']').val()),
		beforeSend: function() {
			$('.success, .warning').remove();
			$('#button-reward').attr('disabled', true);
			$('#reward').before('<div class="attention"><img src="view/image/loading.gif" alt="" /> <?php echo $text_wait; ?></div>');
		},
		complete: function() {
			$('#button-reward').attr('disabled', false);
			$('.attention').remove();
		},
		success: function(html) {
			$('#reward').html(html);
								
			$('#tab-reward input[name=\'points\']').val('');
			$('#tab-reward input[name=\'description\']').val('');
		}
	});
}

function addBanIP(ip) {
	var id = ip.replace(/\./g, '-');
	
	$.ajax({
		url: 'index.php?route=sale/customer/addbanip&token=<?php echo $token; ?>',
		type: 'post',
		dataType: 'json',
		data: 'ip=' + encodeURIComponent(ip),
		beforeSend: function() {
			$('.success, .warning').remove();
			
			$('.box').before('<div class="attention"><img src="view/image/loading.gif" alt="" /> <?php echo $text_wait; ?></div>');		
		},
		complete: function() {
			
		},			
		success: function(json) {
			$('.attention').remove();
			
			if (json['error']) {
				 $('.box').before('<div class="warning" style="display: none;">' + json['error'] + '</div>');
				
				$('.warning').fadeIn('slow');
			}
						
			if (json['success']) {
                $('.box').before('<div class="success" style="display: none;">' + json['success'] + '</div>');
				
				$('.success').fadeIn('slow');
				
				$('#' + id).replaceWith('<a id="' + id + '" onclick="removeBanIP(\'' + ip + '\');"><?php echo $text_remove_ban_ip; ?></a>');
			}
		}
	});	
}

function removeBanIP(ip) {
	var id = ip.replace(/\./g, '-');
	
	$.ajax({
		url: 'index.php?route=sale/customer/removebanip&token=<?php echo $token; ?>',
		type: 'post',
		dataType: 'json',
		data: 'ip=' + encodeURIComponent(ip),
		beforeSend: function() {
			$('.success, .warning').remove();
			
			$('.box').before('<div class="attention"><img src="view/image/loading.gif" alt="" /> <?php echo $text_wait; ?></div>');					
		},	
		success: function(json) {
			$('.attention').remove();
			
			if (json['error']) {
				 $('.box').before('<div class="warning" style="display: none;">' + json['error'] + '</div>');
				
				$('.warning').fadeIn('slow');
			}
			
			if (json['success']) {
				 $('.box').before('<div class="success" style="display: none;">' + json['success'] + '</div>');
				
				$('.success').fadeIn('slow');
				
				$('#' + id).replaceWith('<a id="' + id + '" onclick="addBanIP(\'' + ip + '\');"><?php echo $text_add_ban_ip; ?></a>');
			}
		}
	});	
};
//--></script> 
<script type="text/javascript"><!--
$('.htabs a').tabs();
$('.vtabs a').tabs();
//--></script> 


<script>
$('tr#row-31 input[type="checkbox"]').change(function(){

var elem = this.id;
var elem_id = elem.replace("label-", "");
if($('input#' + elem).attr('checked')){
var par = $('input#' + elem).parent();
var city = par.text();
var name_city = 'city';
var name_order = 'min_order';
var name_delider = 'free_delivery';
var pay = 'pay';
var time = 'time';
$('table#cities_table').append('<tr class ="' + elem + '"><td><input name="city['+elem_id+'][' + name_city + ']" value="' + city +  '" readonly></td><td><input type="text" name="city['+elem_id+']['+name_order+']"></td><td><input type="text" name="city['+elem_id+']['+name_delider+']"></td><td><input type="text" name="city['+elem_id+']['+pay+']"></td><td><input type="text" name="city['+elem_id+']['+time+']"></td></tr>');
    }else{
    
    $('tr.' + elem).remove();
    
    }
});    
    
</script>


<script>

$('#my-row').on('click', function(e){
         e.preventDefault();
         var number_child = $('table#my_menu tr').length;  
         var next_child = number_child + 1; 
         var output = '<tr id="' + next_child +'">';
            output += '<td><input type="text" name="menu_cat[' + number_child +'][day]"></td>';
            output += '<td><input type="text" name="menu_cat[' + number_child +'][start_time]"></td>';
            output += '<td><input type="text" name="menu_cat[' + number_child +'][end_time]"></td>';
            output += '<td><input type="text" name="menu_cat[' + number_child +'][commision]"></td>';
            output += '<td><input type="text" name="menu_cat[' + number_child +'][say]"></td>';
            output += '</tr>';
            $('table#my_menu').append(output);
            });

</script>
<script>

$('#new-row').on('click', function(e){
         e.preventDefault();
         var number_new_child = $('table#contact-table tr').length;  
         var next_child = number_new_child + 1; 
         var output = '<tr id="' + next_child +'">';
            output += '<td><input type="text" name="person[' + number_new_child +'][name]"></td>';
            output += '<td><input type="text" name="person[' + number_new_child +'][email]"></td>';
            output += '<td><input type="text" name="person[' + number_new_child +'][phone]"></td>';
            output += '</tr>';
            $('table#contact-table').append(output);
            });

</script>
<script>
$('.prev').on('click', function(e){

e.preventDefault();

});

</script>



<script>

function getUrlParP(sParam)
{
    var sPageURL = window.location.search.substring(1);
    var sURLVariables = sPageURL.split('&');
    for (var i = 0; i < sURLVariables.length; i++) 
    {
        var sParameterName = sURLVariables[i].split('=');
        if (sParameterName[0] == sParam) 
        {
            return sParameterName[1];
        }
    }
}  
    
var tokK = getUrlParP('token');    
   
$('#create-product').on('click', function(e){

e.preventDefault();
var rest_id = getUrlParP('customer_id'); 
var rest_name = $('input#rest_first').val() + ' ' + $('input#rest_last').val();
console.log(rest_name);
      $.ajax({
			  url: "http://54.77.118.39/admin/index.php?route=sale/customer/sessItem&token=" + tokK,
			  type: "POST",
			  dataType: "html",
			  async: "false",
             data: {rest_id:rest_id, rest_name:rest_name},
             success: function(res) {
             
                window.open("http://54.77.118.39/admin/index.php?route=catalog/product/insert&token=" + tokK);
                 
                 
             
             }
      });
});
</script>


<script>

$('#subs-row').on('click',function(e){
	e.preventDefault();
	      var number_new_childn = $('table#subscription_table tr').length;  
          var next_childn = number_new_childn + 1; 
           var output = '<tr id="' + next_childn +'">';
            output += '<td><input type="text" name="subs[' + number_new_childn +'][number]"></td>';
            output += '<td><input type="text" name="subs[' + number_new_childn +'][min_pay]"></td>';
            output += '<td><input type="text" name="subs[' + number_new_childn +'][max_pay]"></td>';
            output += '</tr>';
            $('table#subscription_table').append(output);
	
	});
</script>

<script>

$('#commission-row').on('click',function(e){
	e.preventDefault();
	      var number_new_childnd = $('table#table-commission tr').length;  
          var next_childnd = number_new_childnd + 1; 
           var output = '<tr id="' + next_childnd +'">';
            output += '<td><input type="text" name="com[' + number_new_childnd +'][commission]"></td>';
            output += '<td><input type="text" name="com[' + number_new_childnd +'][min_pay]"></td>';
            output += '<td><input type="text" name="com[' + number_new_childnd +'][max_pay]"></td>';
            output += '</tr>';
            $('table#table-commission').append(output);
	
	});
</script>
<?php echo $footer; ?>