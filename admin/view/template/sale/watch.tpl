<?php echo $header ?>
<?php
//date_default_timezone_set('Asia/Jerusalem');
//echo $today = date("H:i:s Y-m-d"); 
?>
<div id="content">
<div class="box">
    
<div class="heading">
 <h1><img src="view/image/order.png" alt="" /> <?php echo $heading_title; ?></h1> 
<!--<div class="buttons"><a href="<?php echo $action_insert ?>"  class="button"><?php echo $text_insert; ?></a></div>-->
</div>    
  
<div class="content">
  
  <table class="list manager_table">
     
    <thead>
     <tr>
         <td class="left"><?php echo $entery_name ?></td>
         <td class="right"><?php echo $text_edit ?></td>
         <td class="right"><?php echo $text_enter ?></td>
        </tr>
  </thead>
     
    <tbody>
        
    <?php foreach($curiers as $key=>$value){ ?>  
        <tr>
            <td class="left"><?php echo $value['first_name'] . ' ' .  $value['last_name'] ?></td>
            <td class="right"><a href="<?php echo $action_edit . '&curier_id=' . $value['id'] ?>"><?php echo $text_edit ?></a></td>
            <td>
                <?php if($value['curier_id'] == null){ ?>
                <input class="add_to_watch" type="checkbox" value="<?php echo $value['id'] ?>"><label><?php echo $text_active ?></label>
                <?php }else{ ?>
                   <input class="add_to_watch" type="checkbox" value="<?php echo $value['id'] ?>" checked><?php echo $text_active ?>
                <?php } ?>
                <br>
                <?php if($value['break'] == 1){ ?>
                <input class="add_to_break" type="checkbox" value="<?php echo $value['id'] ?>" checked><label><?php echo $text_break ?></label>
                <?php }else{ ?>
                   <input class="add_to_break" type="checkbox" value="<?php echo $value['id'] ?>"><?php echo $text_break ?>
                <?php } ?>
            </td>
        </tr>
     <?php } ?> 
    </tbody>

 </table>
  
    
    <table class="list side_manager">
        <thead>
            <tr><td class="table_header"><?php echo $text_tableheader ?></td></tr>
        </thead>
        <!-- going to be available -->
        <thead>
            <tr><td><?php echo $text_going ?></td></tr>
        </thead>
       <tbody>
           <tr><td></td></tr>  
      </tbody>
        
        <!-- end of going to be available -->
        
        <!--  available -->
        <thead>
            <tr><td><?php echo $text_available ?></td></tr>
        </thead>
       <tbody>
           <tr><td></td></tr>  
      </tbody>
        
        <!-- end of  available -->
        
        <!--  links -->
        <thead>
            <tr><td><?php echo $text_links?></td></tr>
        </thead>
       <tbody>
        <tr><td class="table_link"><a href="<?php echo $manager_link ?>"><?php echo $text_manager ?></a></td></tr>
           <tr><td class="table_link"><a href="<?php echo $watch_link ?>"><?php echo $text_watch ?></a></td></tr>
            <tr><td class="table_link"><a href="<?php echo $rest_link ?>"><?php echo $text_rest ?></a></td></tr>
           <tr><td class="table_link"><a href="<?php echo $quik_link ?>"><?php echo $text_quick ?></a></td></tr>
           <tr><td class="table_link"><a href="<?php echo $curier_collect_link ?>"><?php echo $text_collect_curier ?></a></td></tr>
      </tbody>
        
        <!-- end of  links -->
    </table>
    
</div>
</div>
</div>

<script>
function getUrlParameter(sParam)
{
    var sPageURL = window.location.search.substring(1);
    var sURLVariables = sPageURL.split('&');
    for (var i = 0; i < sURLVariables.length; i++) 
    {
        var sParameterName = sURLVariables[i].split('=');
        if (sParameterName[0] == sParam) 
        {
            return sParameterName[1];
        }
    }
} 
    
var tok = getUrlParameter('token');
    
$('input.add_to_watch').on('change', function(){
var id = $(this).val();
if($(this).prop('checked') == true){
    
   window.location.href = "index.php?route=sale/watch/insert&token=" + tok + "&curier_id=" + id;
}else{
     window.location.href = "index.php?route=sale/watch/delete&token=" + tok + "&curier_id=" + id;
}

})

//update break
$('input.add_to_break').on('change', function(){
var id = $(this).val();
if($(this).prop('checked') == true){
    
   window.location.href = "index.php?route=sale/watch/updateBreak&token=" + tok + "&curier_id=" + id;
}else{
     window.location.href = "index.php?route=sale/watch/endBreak&token=" + tok + "&curier_id=" + id;
}

})

</script>
<?php echo $footer ?>