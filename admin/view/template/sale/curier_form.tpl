<?php echo $header ?>

<div id="content">
<div class="box">
 <div class="heading">
 <h1><img src="view/image/order.png" alt="" /> <?php echo $heading_title; ?></h1>
     <div class="buttons"><a onclick="$('#form').submit();" class="button"><?php echo $button_save; ?></a><a onclick="history.go(-1);" class="button"><?php echo $entery_back; ?></a></div>
</div>       
   
<div class="content">
<form action="<?php echo $form_action  ?>" method="post" enctype="multipart/form-data" id="form">
<table class="form">
  
    <tr>
      <td><?php echo $entery_firstname ?></td>
      <td>
        <?php if(isset($firstname)){ ?>
          
          <input name="firstname" value="<?php echo $firstname ?>">
           
        <?php }else{ ?>
            <input name="firstname">
        <?php } ?>
      </td>
    </tr>
    
        <tr>
      <td><?php echo $entery_lastname ?></td>
      <td>
        <?php if(isset($lastname)){ ?>
          
          <input name="lastname" value="<?php echo $lastname ?>">
           
        <?php }else{ ?>
            <input name="lastname">
        <?php } ?>
      </td>
    </tr>
    
           <tr>
      <td><?php echo $entery_phone ?></td>
      <td>
        <?php if(isset($phone)){ ?>
          
          <input name="phone" value="<?php echo $phone ?>">
           
        <?php }else{ ?>
            <input name="phone">
        <?php } ?>
      </td>
    </tr>
    
           <tr>
      <td><?php echo $entery_homephone ?></td>
      <td>
        <?php if(isset($homephone)){ ?>
          
          <input name="homephone" value="<?php echo $homephone ?>">
           
        <?php }else{ ?>
            <input name="homephone">
        <?php } ?>
      </td>
    </tr>
    
     <tr>
      <td><?php echo $entery_id ?></td>
      <td>
        <?php if(isset($id)){ ?>
          
          <input name="id" value="<?php echo $id ?>">
           
        <?php }else{ ?>
            <input name="id">
        <?php } ?>
      </td>
    </tr>
    
               <tr>
      <td><?php echo $entery_address ?></td>
      <td>
        <?php if(isset($address)){ ?>
          
          <input name="address" value="<?php echo $address ?>">
           
        <?php }else{ ?>
            <input name="address">
        <?php } ?>
      </td>
    </tr>
    
                   <tr>
      <td><?php echo $entery_job ?></td>
      <td>
        <?php if($curier_job == 1){ ?>
          
          <input type="checkbox" name="curier_job" checked><label><?php echo $entery_curier ?></label>
           
        <?php }else{ ?>
            <input type="checkbox" name="curier_job" value="1">
          <label><?php echo $entery_curier ?></label>
        <?php } ?>
          <br>
         <?php if($phone_job == 1){ ?>
          
          <input type="checkbox" name="phone_job" checked>
            <label><?php echo $entery_phone_job ?></label>
        <?php }else{ ?>
            <input type="checkbox" name="phone_job" value="1">
          <label><?php echo $entery_phone_job ?></label>
        <?php } ?>
      </td>
    </tr>
    
                   <tr>
      <td><?php echo $entery_status ?></td>
      <td>
         <?php if($status == 1){ ?>
          
          <input type="checkbox" name="status" checked>
            <label><?php echo $entery_activate ?></label>
        <?php }else{ ?>
            <input type="checkbox" name="status" value="1">
          <label><?php echo $entery_activate ?></label>
        <?php } ?>
      </td>
    </tr>
    
    
    
    
</table>
</form>
</div> <!-- conrent -->
</div> <!-- box -->
</div> <!-- content -->

<?php echo $footer ?>