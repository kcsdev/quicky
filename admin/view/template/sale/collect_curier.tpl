<?php echo $header; ?>
<?php
//echo "<pre>";
//print_r($curiers);
//die;
?>
<div id="content">
<div class="box">
    
<div class="heading">
 <h1><img src="view/image/order.png" alt="" /> <?php echo $heading_title; ?></h1> 
    <div class="date_box"><?php echo $text_date ?> <input type="date" class="date_picker" value="<?php echo $date ?>"></div>
</div>    
  
<div class="content">
    
     <table class="list manager_table">
      <thead>
          <tr>
              <td align="center"><?php echo $text_names; ?></td>
              <td align="center"><?php echo $text_details; ?></td>    
          </tr>
      </thead>
     <tbody>
     <?php foreach($curiers as $curier){ ?>
         <tr>
             <td align="center"><?php echo $curier['first_name'] . ' ' . $curier['last_name'] ?></td>
             <td align="center"><a href="<?php echo $action_details .'&id=' . $curier['curier_id'] . '&date=' . date('Y-m-d') ?>">פרטי המשמרת</a></td>
         </tr>
     <?php } ?>
         </tbody>
    </table>
    
    <!-- side table -->
        <table class="list side_manager">
        <thead>
            <tr><td class="table_header"><?php echo $text_tableheader ?></td></tr>
        </thead>
        <!-- going to be available -->
        <thead>
            <tr><td><?php echo $text_going ?></td></tr>
        </thead>
       <tbody>
           <tr><td></td></tr>  
      </tbody>
        
        <!-- end of going to be available -->
        
        <!--  available -->
        <thead>
            <tr><td><?php echo $text_available ?></td></tr>
        </thead>
       <tbody>
           <tr><td></td></tr>  
      </tbody>
        
        <!-- end of  available -->
        
        <!--  links -->
        <thead>
            <tr><td><?php echo $text_links?></td></tr>
        </thead>
       <tbody>
           <tr><td class="table_link"><a href="<?php echo $manager_link ?>"><?php echo $text_manager ?></a></td></tr>
           <tr><td class="table_link"><a href="<?php echo $watch_link ?>"><?php echo $text_watch ?></a></td></tr>
            <tr><td class="table_link"><a href="<?php echo $rest_link ?>"><?php echo $text_rest ?></a></td></tr>
             <tr><td class="table_link"><a href="<?php echo $quik_link ?>"><?php echo $text_quick ?></a></td></tr>
           <tr><td class="table_link"><a href="<?php echo $curier_collect_link ?>"><?php echo $text_collect_curier ?></a></td></tr>
      </tbody>
        
        <!-- end of  links -->
    </table>
    <!-- end of side table -->
    
    
</div>
</div>
</div> 
<script>

function getUrlParameter(sParam)
{
    var sPageURL = window.location.search.substring(1);
    var sURLVariables = sPageURL.split('&');
    for (var i = 0; i < sURLVariables.length; i++) 
    {
        var sParameterName = sURLVariables[i].split('=');
        if (sParameterName[0] == sParam) 
        {
            return sParameterName[1];
        }
    }
}
    
var tok = getUrlParameter('token');
    
$('input.date_picker').on('change', function(){
var output = ''
var date =  $(this).val();
var date_arr = date.split("-");
var new_date = date_arr[2] + '-' + date_arr[1] + '-' + date_arr[0];
			$.ajax({
			  url: "index.php?route=sale/collect_curier/getData&token=" + tok,
			  type: "POST",
			  dataType: "html",
			  async: "false", 
			  data: { date:new_date },
			  success: function(response) {			
			    var res = JSON.parse(response);
                $.each( res, function( key, value ) {
                    var url = "index.php?route=sale/collect_curier/details&token=" + tok + '&id=';
                    output += '<tr>';
                    output += '<td align="center">' + value.first_name + ' ' + value.last_name +'</td>';
                    output +=  '<td align="center"><a href="' + url + value.curier_id +'&date='+ date +'">פרטי המשמרת</a></td>';
                    output += '</tr>';
                });
              $('table.manager_table tbody').empty();
              $('table.manager_table tbody').html(output);
                			  			  					  					  					  				  				  					  	
			  }	  
			});

})
</script>
<?php echo $footer; ?>