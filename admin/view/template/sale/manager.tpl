<?php echo $header; ?>

<div id="content">
<div class="box">
 <div class="heading">
  <h1><img src="view/image/order.png" alt="" /> <?php echo $heading_title; ?></h1>  
</div>
 
<div class="content">
   
<div id="tabs" class="htabs">
<?php if(!isset($_GET['curier']) && !isset($_GET['fax']) && !isset($_GET['status']) && !isset($_GET['not']) ){ ?>
<a href="<?php echo $all_link ?>" class="selected" style="display:inline"><?php echo $all ?></a> 
<?php }else{ ?>
 <a href="<?php echo $all_link ?>"  style="display:inline"><?php echo $all ?></a>
<?php } ?>
<?php if(isset($_GET['status']) && $_GET['status'] == 'not'){ ?>
<a href="<?php echo $running_link ?>" class="selected"  style="display:inline"><?php echo $run_text ?></a>
<?php }else{ ?>
<a href="<?php echo $running_link ?>"  style="display:inline"><?php echo $run_text ?></a>
<?php } ?>
<a href="#"  style="display:inline"><?php echo $ready_text ?></a>
<?php if(isset($_GET['curier']) && $_GET['curier'] == 0){ ?>
<a href="<?php echo $staffing ?>" class="selected"  style="display:inline"><?php echo $staffing_text ?></a>
<?php }else{ ?>
<a href="<?php echo $staffing ?>" style="display:inline"><?php echo $staffing_text ?></a>  
<?php } ?>
<a href="#"  style="display:inline"><?php echo $process_text ?></a>
<?php if(isset($_GET['fax']) && $_GET['fax'] == 3){ ?>
<a href="<?php echo $validate_link ?>" class="selected"  style="display:inline"><?php echo $validate_text ?></a>
<?php }else{ ?>
   <a href="<?php echo $validate_link ?>"  style="display:inline"><?php echo $validate_text ?></a> 
<?php } ?>
<?php if(isset($_GET['status']) && $_GET['status'] == 2){ ?>
<a href="<?php echo $gived_link ?>" class="selected"  style="display:inline"><?php echo $gived_text ?></a>
<?php }else{ ?>
<a href="<?php echo $gived_link ?>"  style="display:inline"><?php echo $gived_text ?></a>
<?php } ?>    
</div>
    
    
    

    
   <!-- start of manager list -->
    <table class="list manager_table">
        <thead>
            <tr>
                <td style="display:none" class="order_id"><?php echo $id_text ?></td>
                <td><?php echo $time_text ?></td>
                 <td><?php echo $customer_text ?></td>
                 <td><?php echo $fax_text ?></td>
                 <td><?php echo $brend_text ?></td>
                 <td><?php echo $restaurant_text ?></td>
                 <td><?php echo $address_text ?></td>
                 <td><?php echo $courier_text ?></td>
                 <td><?php echo $timebar_text ?></td>
                 <td><?php echo $status_text ?></td>
                 <td><?php echo $money_text ?></td>
            
            </tr>
        </thead>
        <tbody>
         <?php foreach($orders as $order){ ?>
            <tr>
                <td style="display:none" class="order_id"><?php echo $order['id'] ?></td>
                <td><?php  echo    $order['create_at'] ?></td>
                <td><a href="<?php echo $order['href'] ?>" target="_blank"><?php echo $order['name'] ?></a></td>
                <td>
                    <select class="fax">
                     <?php if($order['fax'] == 0){ ?>
                    <option value="0" selected><?php echo $fax_process ?></option>
                    <?php }else{ ?>
                    <option value="0" ><?php echo $fax_process ?></option>
                    <?php } ?>
                      <?php if($order['fax'] == 1){ ?>
                    <option value="1" selected><?php echo $fax_again ?></option>
                    <?php }else{ ?>
                    <option value="1" ><?php echo $fax_again ?></option>
                    <?php } ?>
                          <?php if($order['fax'] == 2){ ?>
                    <option value="2" class="fail" selected><?php echo $fax_fail ?></option>
                    <?php }else{ ?>
                    <option class="fail" value="2"><?php echo $fax_fail ?></option>
                    <?php } ?>
                          <?php if($order['fax'] == 3){ ?>
                    <option value="3" selected><?php echo $fax_validate ?></option>
                    <?php }else{ ?>
                    <option value="3" ><?php echo $fax_validate ?></option>
                    <?php } ?>
                          <?php if($order['fax'] == 4){ ?>
                    <option value="4" selected><?php echo $fax_send ?></option>
                    <?php }else{ ?>
                    <option value="4"><?php echo $fax_send ?></option>
                    <?php } ?>
                    </select>
                </td>
                <td><?php echo $order['type'] ?></td>
                <td class="rest_col"><?php echo $order['restaurent'] ?><div class="com-open"></div><span class="comment-manager"><?php echo $order['phone'] ?></span></td>
                <td><?php echo $order['street'] . ' ' . $order['house_number'] . ',' . ' ' .  $order['city']?>
                    <?php if(!empty($order['comment'])){ ?>
                    <div class="com-open"></div><span class="comment-manager"><?php echo $order['comment'] ?></span>
                
                   <?php } ?>
                </td>
                <td>
                 <?php if(!isset($order['curier'])){ ?>    
                    <a class="staffing"><?php echo 'לחץ לציוות' ?></a>
                <?php }else{ ?>
                  <?php echo $order['curier']   ?>
                <?php } ?>
                </td>
                
                <td class="time_parent">
                <?php if($order['fax'] != 4 && $order['time_fax'] > $order['total_fax']){ ?>
                    <div class="time_bar fail">
                <?php }elseif($order['status'] != 1 && $order['time_rest'] > $order['total_rest']){ ?>
                    <div class="time_fail">
                <?php }else if($order['status'] == 1 && $order['time_curier'] > $order['total_curier']){ ?>
                
                <?php }else{ ?>
                <div class="time_bar">
                <?php } ?>
                    <?php if($order['time_fax'] > 0){ ?>
                    <div class="time_fax time_part" style="width:<?php echo $order['time_fax'] . '%' ?>"><?php echo $order['total_fax'] ?></div>
                    <?php } ?>
                    <?php if($order['time_rest'] > 0){ ?>
                    <div class="time_rest time_part" style="width:<?php echo $order['time_rest'] . '%' ?>"><?php echo $order['total_rest'] ?></div>
                    <?php } ?>
                    <?php if($order['time_curier'] > 0){ ?>
                    <div class="time_curier time_part" style="width:<?php echo $order['time_curier'] . '%' ?>"><?php echo $order['total_curier'] ?></div>
                    <?php } ?>
                 <div class="time_general"><?php echo $order['total_time'] ?></div>
                </div>
                </td>
                
                <!-- status  -->
                <td>
                    <select class="status">
                     <?php if($order['status'] == 0){ ?>
                        <option value="0" selected><?php echo $delivery_notcollected ?></option>
                    <?php }else{ ?>
                         <option value="0"><?php echo $delivery_notcollected ?></option>
                    <?php } ?>
                    <?php if($order['status'] == 1){ ?>
                        <option value="1" selected><?php echo $delivery_collected ?></option>
                    <?php }else{ ?>
                         <option value="1"><?php echo $delivery_collected ?></option>
                    <?php } ?>
                     <?php if($order['status'] == 2){ ?>
                        <option value="2" selected><?php echo $delivery_provided ?></option>
                    <?php }else{ ?>
                         <option value="2"><?php echo $delivery_provided ?></option>
                    <?php } ?>
                    <?php if($order['status'] == 3){ ?>
                        <option value="3" selected><?php echo $delivery_restaurant ?></option>
                    <?php }else{ ?>
                         <option value="3"><?php echo $delivery_restaurant ?></option>
                    <?php } ?>
                    <?php if($order['status'] == 4){ ?>
                        <option value="4" selected><?php echo $delivery_customer ?></option>
                    <?php }else{ ?>
                         <option value="4"><?php echo $delivery_customer ?></option>
                    <?php } ?>
                    
                    </select>
                </td>
                <!-- end of status -->
                
                <td><?php echo $order['final_price'] ?></td>
            </tr>
        <?php } ?>
        
        </tbody>
    </table>
    <!-- end of manager list -->
    
    <table class="list side_manager">
        <thead>
            <tr><td class="table_header"><?php echo $text_tableheader ?></td></tr>
        </thead>
        <!-- going to be available -->
        <thead>
            <tr><td><?php echo $text_going ?></td></tr>
        </thead>
       <tbody>
           <tr>
            <?php foreach($getFree AS $free){ ?>
               <td align="center"><?php echo $free['first_name'] . ' ' . $free['last_name'] ?></td>
            <?php } ?>
            </tr>  
      </tbody>
        
        <!-- end of going to be available -->
        
        <!--  available -->
        <thead>
            <tr><td><?php echo $text_available ?></td></tr>
        </thead>
       <tbody>
           <tr><td></td></tr>  
      </tbody>
        
        <!-- end of  available -->
        
        <!--  links -->
        <thead>
            <tr><td><?php echo $text_links?></td></tr>
        </thead>
       <tbody>
           <tr><td class="table_link"><a href="<?php echo $watch_link ?>"><?php echo $text_watch ?></a></td></tr>
            <tr><td class="table_link"><a href="<?php echo $rest_link ?>"><?php echo $text_rest ?></a></td></tr>
           <tr><td class="table_link"><a href="<?php echo $manager_link ?>"><?php echo $text_manager ?></a></td></tr>
             <tr><td class="table_link"><a href="<?php echo $quik_link ?>"><?php echo $text_quick ?></a></td></tr>
           <tr><td class="table_link"><a href="<?php echo $curier_collect_link ?>"><?php echo $text_collect_curier ?></a></td></tr>
      </tbody>
        
        <!-- end of  links -->
    </table>
</div>
    
</div>

<!-- give the grand-parent css -->    
<script>

 //$('option.fail').parent().parent().css('background', 'red')    
</script>    
    
<!-- get url param -->
<script>
function getUrlParP(sParam)
{
    var sPageURL = window.location.search.substring(1);
    var sURLVariables = sPageURL.split('&');
    for (var i = 0; i < sURLVariables.length; i++) 
    {
        var sParameterName = sURLVariables[i].split('=');
        if (sParameterName[0] == sParam) 
        {
            return sParameterName[1];
        }
    }
}  
    
var tokK = getUrlParP('token');     
    
</script>    

<!-- hover on tr -->
<script>
    $('table.manager_table tbody tr').mouseover(function(){
     
     $(this).children().css('background', '#A9A9A9');
    
    });
    
        $('table.list tbody tr').mouseleave(function(){
     
     $(this).children().css('background', '#FFFFFF');
    
    });
</script>

<!-- update the cart on change -->
<script>
 $('td select').on('change', function(){
 var id = $(this).parent().parent().children('.order_id').text();
 var type = $(this).attr('class');
 var status = $(this).val();
 $.ajax({
			  url: "index.php?route=sale/manager/update&token=" + tokK,
			  type: "POST",
			  dataType: "html",
			  async: "false", 
			  data: { id:id, type:type, status:status },
              success: function(response) {	
               
                location.reload();
              }
        });
 });
    
</script>
    
<script>
$('div.com-open').on('mouseover', function(){

  var status = $(this).next().css('display');
  console.log(status);
  if(status == 'none'){
       $(this).next().css('display', 'block');
  
  }else{
  
    $(this).next().css('display', 'none');
  
  }

})

$('div.com-open').on('mouseleave', function(){

    $(this).next().css('display', 'none');
});
</script>
    
<script>
$('a.staffing').on('click', function(){
 
var id = $(this).parent().parent().children('.order_id').html();
var output = '';
output += '<div class="curier-pop" id="'+ id +'">';
output += '<form method="post" action="index.php?route=sale/manager/addCurier&token=' + tokK +'">';
output += '<input type="hidden" name="id" value="' + id +'">';
output += '<select class="curier-list" name="curier">';
<?php foreach ($curiers as $curier){ ?>
var cur_id = "<?php echo $curier['id'] ?>";
var option_id = 'value="' + cur_id +'"';
output += "<option "+ option_id +"><?php echo $curier['first_name'] . ' ' . $curier['last_name'] ?></option>";
<?php } ?>
output += '</select><br><br>';
output += '<input type="submit" name="submit" value="צוות">';
output += '</form>';
output += '</div>';
$('body').append(output);

});    
</script>

<script>
    
setInterval(function(){ 
    
     $.ajax({
			  url: "index.php?route=sale/manager/updateTimes&token=" + tokK,
			  type: "POST",
			  dataType: "html",
			  async: "false", 
              success: function(response) {	
               
                   location.reload();
              }
        });
                      
                      
                      
 }, 120000);
</script>

<?php echo $footer; ?>