<?php echo $header ?>

<div id="content">
<div class="box">
 <div class="heading">
 <h1><img src="view/image/order.png" alt="" /> <?php echo $heading_title; ?></h1>
     <div class="buttons"><a onclick="$('#form').submit();" class="button"><?php echo $button_save; ?></a><a onclick="history.go(-1);" class="button"><?php echo $entery_back; ?></a></div>
</div>    
 <div class="content">

<form action="<?php echo $form_action  ?>" method="post" enctype="multipart/form-data" id="form">
<table class="form">
    <tr>
        <td><?php echo $text_curier ?></td>
        <td>
            <select name="curier">
                <?php if(isset($curier_id) && isset($name)){ ?>
                    <option value="<?php echo $curier['id'] ?>"><?php echo $name?></option>
                <?php }else{ ?>
                <?php foreach($curiers as $curier){ ?>
                <option value="<?php echo $curier['id'] ?>"><?php echo $curier['first_name'] . ' ' .  $curier['last_name']?></option>
                <?php } ?>
                <?php } ?>
            </select>
        </td>
    </tr>
    
    <tr>
     <td><?php echo $text_mirs ?></td>
     <td>
        <?php if(isset($mirs)){ ?>
         <input type="text" name="mirs" value="<?php echo $mirs ?>">
         <?php }else{ ?>
         <input type="text" name="mirs">
         <?php } ?>
        
    </td>
    </tr>
    
     <tr>
     <td><?php echo $text_motorcycle ?></td>
     <td>
          <?php if(isset($motorcycle)){ ?>
         <input type="text" name="motorcycle" value="<?php echo $motorcycle ?>">
          <?php }else{ ?>
            <input type="text" name="motorcycle">
           <?php } ?>
     </td>
    </tr>
    
         <tr>
     <td><?php echo $text_change ?></td>
     <td>
          <?php if(isset($change_money)){ ?>
         <input type="text" name="change" value="<?php echo $change_money ?>">
          <?php }else{ ?>
         <input type="text" name="change">
         <?php } ?>
     </td>
    </tr>
         <tr>
     <td><?php echo $text_storm ?></td>
     <td>
         <?php if($storm_suit == 1){ ?>
         <input type="checkbox" name="storm" value="1" checked>
          <?php }else{ ?>
          <input type="checkbox" name="storm" value="1">
         <?php } ?>
     </td>
    </tr>

</table>
</form>
    
</div>
</div>
</div>



<?php echo $footer ?>