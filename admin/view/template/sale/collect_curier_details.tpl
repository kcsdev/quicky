<?php echo $header; ?>
<div id="content">
<div class="box">
    
<div class="heading">
 <h1><img src="view/image/order.png" alt="" /> <?php echo $heading_title; ?></h1> 
</div>    
  
<div class="content">
        <table class="list">
      <thead>
         <tr>
             <td align="center"><?php echo $text_rest_name ?></td>
              <td align="center"><?php echo $text_customer_name ?></td>
              <td align="center"><?php echo $text_address ?></td>
              <td align="center"><?php echo $text_money ?></td>
             <td align="center"><?php echo $text_cash ?></td>
         </tr>
      </thead>
     <tbody>
            <?php foreach($rest_det as $det){ ?>
            <tr>
             <td align="center"><?php echo $det['restaurent'] ?></td>
             <td align="center"><?php echo $det['name'] ?></td>
             <td align="center"><?php echo $det['city'] . ' ' . $det['street'] . ' ' . $det['house_number'] ?></td>
              <td align="center"><?php echo $det['price'] ?></td>
             <td align="center"><?php echo $det['cash'] ?></td>
        </tr>
           <?php } ?>
         </tbody>
    </table>
    <table class="form">
        <tr>
            <td><?php echo $text_customer_name ?></td>
            <td><input type="text" name="curier_name" value="<?php echo $rest_det[0]['first_name'] . ' ' . $rest_det[0]['last_name'] ?>" readonly></td>
        </tr>
        <tr>
            <td><?php echo $text_cash_orders ?></td>
            <td><input type="text" name="total_cash" value="<?php echo $total_cash ?>"></td>
        </tr>
        <tr>
            <td><?php echo $text_extra_cash ?></td>
            <td><input type="text" name="extra_cash" value=""></td>
        </tr>
               <tr>
            <td><?php echo $text_claim ?></td>
            <td><input type="text" name="claim" value=""></td>
        </tr>
        <tr>
            <td><?php echo $text_full ?></td>
            <td><input type="text" name="full" value="0"></td>
        </tr>
        <tr>
            <td><?php echo $text_give ?></td>
            <td><input type="text" name="give" value="" readonly></td>
        </tr>

         <tr>
            <td><?php echo $text_take ?></td>
            <td><input type="text" name="take" value=""></td>
        </tr>
        <tr>
            <td><?php echo $text_diffrent ?></td>
            <td><input type="text" name="diffrent" value="" readonly></td>
        </tr>
        <tr>
           <td><?php echo $text_storm ?></td> 
            <td>
                <?php if($rest_det[0]['storm_suit'] == 1){ ?>
                <input type="checkbox" name="storm_suit" value="1" checked>
                <?php }else{ ?>
                <input type="checkbox" name="storm_suit" value="0">
                <?php } ?>
            </td>
        </tr>
    </table>
</div>
</div>
</div>
<script>
var val = $('input'); 
    function getUrlParameter(sParam)
{
    var sPageURL = window.location.search.substring(1);
    var sURLVariables = sPageURL.split('&');
    for (var i = 0; i < sURLVariables.length; i++) 
    {
        var sParameterName = sURLVariables[i].split('=');
        if (sParameterName[0] == sParam) 
        {
            return sParameterName[1];
        }
    }
}
var tok = getUrlParameter('token');
var curier_id = getUrlParameter('id');
var date = getUrlParameter('date');
var total_cash = $('input[name="total_cash"]').val();
var full = $('input[name="full"]').val();
if($('input[name="storm_suit"]').prop('checked') == true){
    var storm = 1;
}else{

   var storm = 0;
}

        $.ajax({
			  url: "index.php?route=sale/collect_curier/insert&token=" + tok,
			  type: "POST",
			  dataType: "html",
			  async: "false", 
			  data: {curier_id:curier_id, date:date, total_cash:total_cash, full:full, storm:storm},
			  success: function(response) {
                  var arr = JSON.parse(response);
                  $.each(arr[0],function(index,value){
                    if(index == 'storm_suit'){
                        if(value == 1){
                          $('input[name="storm_suit"]').prop('checked', true);
                        }else{
                        
                           $('input[name="storm_suit"]').prop('checked', false);
                        }
                    
                    }else{
                    $('input[name="' + index + '"]').val(value); 
                    }
                  })
              }
			});
    
    $('input[name="total_cash"], input[name="extra_cash"], input[name="full"]').on('focusout', function(){
     var new_total_cash = $('input[name="total_cash"]').val();
     var new_extra_cash = $('input[name="extra_cash"]').val();
     var new_full = $('input[name="full"]').val();
    $('input[name="give"]').val(parseFloat(new_total_cash) + parseFloat(new_extra_cash) - parseFloat(new_full));
    var give = $('input[name="give"]').val();
    var take = $(this).val();
    $('input[name="diffrent"]').val(parseFloat(give) - parseFloat(take));
    val = $('input');
    edit(val);
    })
    
    $('input[name="claim"]').on('focusout', function(){
     
       val = $('input');
       edit(val);
    
    });
    
    $('input[name="take"]').on('focusout', function(){
    
      var give = $('input[name="give"]').val();
      var take = $(this).val();
      $('input[name="diffrent"]').val(parseFloat(give) - parseFloat(take));
       val = $('input');
       edit(val);    
    
    });
    
    $('input[name="storm_suit"]').on('change', function(){
    
        val = $('input');
       edit(val); 
    
    
    })
    
    
    
//edit the data
 function edit(arr){
      var input_arr = {};
      $.each(arr, function(index, value){
          input_arr[$(value).attr('name')] = $(value).val();  
         if($(value).attr('name') == 'storm_suit'){
          if($('input[name="storm_suit"]').prop('checked') == true){
               input_arr[$(value).attr('name')] = 1; 
             }else{

                input_arr[$(value).attr('name')] = 0; 
              }
         }
      })
     // console.log(input_arr);
      var new_arr = JSON.stringify(input_arr);
     //console.log(new_arr);
      //console.log(new_arr);
              $.ajax({
			  url: "index.php?route=sale/collect_curier/edit&token=" + tok,
			  type: "POST",
			  dataType: "html",
			  async: "false", 
			  data: {input_arr:new_arr, curier_id:curier_id, date:date},
			  success: function(response) {
                      console.log(response);
                  }
              
			});
 }

</script>
<?php echo $footer; ?>