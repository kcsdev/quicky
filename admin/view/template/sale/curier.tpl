<?php echo $header; ?>

<div id="content">
<div class="box">
 <div class="heading">
 <h1><img src="view/image/order.png" alt="" /> <?php echo $heading_title; ?></h1>
     <div class="buttons"><a href="<?php echo $action_add ?>"  class="button"><?php echo $button_add; ?></a></div>
</div>       
   
<div class="content">
    
 <table class="list">
     
    <thead>
     <tr>
         <td class="left"><?php echo $entery_name ?></td>
         <td class="right"><?php echo $button_edit ?></td>
         <td class="right"><?php echo $button_delete ?></td>
        </tr>
  </thead>
     
    <tbody>
        
    <?php foreach($curiers as $key=>$value){ ?>  
        <tr>
            <td class="left"><?php echo $value['first_name'] . ' ' .  $value['last_name'] ?></td>
            <td class="right"><a href="<?php echo $action_edit . '&curier_id=' . $value['id'] ?>"><?php echo $button_edit ?></a></td>
            <td><a href="<?php echo $action_delete . '&curier_id=' . $value['id'] ?>"><?php echo $button_delete ?></a></td>
        
        </tr>
     <?php } ?> 
    </tbody>

 </table>
    
</div>
</div>
</div>


<?php echo $footer; ?>