<?php echo $header ?>

<div id="content">
<div class="box">
 <div class="heading">
  <h1><img src="view/image/order.png" alt="" /> <?php echo $heading_title; ?></h1>  
</div>
    
<div class="content"> 

    
   <!-- start of manager list -->
    <table class="list manager_table">
      <thead>
          <tr>
           <td><?php echo $text_name ?></td>
           <td><?php echo $text_neutralizing ?></td>
            <td><?php echo $text_comment ?></td>
             <td><?php echo $text_time ?></td>
          </tr>
      </thead>
    <tbody>
    <?php foreach($rest as $value){?>
        <tr>
        <td><?php echo $value['firstname'] . ' ' . $value['lastname']  ?></td>
        <td>
         <?php if($value['status'] == 1){ ?>
            <input class="neutral" type="checkbox" name="status" value="<?php echo $value['customer_id'] ?>">
        <?php }else{ ?>
            <input class="neutral" type="checkbox" name="status" value="<?php echo $value['customer_id'] ?>" checked>
        <?php } ?>
        </td>
        <td>
            <?php if(empty($value['comment'])){ ?>
            <textarea class="comment_rest" id="<?php echo $value['customer_id'] ?>"></textarea>
            <?php }else{ ?>
            <textarea class="comment_rest" id="<?php echo $value['customer_id'] ?>"><?php echo $value['comment'] ?></textarea>
            <?php } ?>
        </td>
            <td><input type="text" class="add_time" value="<?php echo $value['add_time'] ?>" id="<?php echo 'number_' . $value['customer_id'] ?>"></td>
        </tr>
    <?php }?>
    </tbody>
    </table>
    
    
      <table class="list side_manager">
        <thead>
            <tr><td class="table_header"><?php echo $text_tableheader ?></td></tr>
        </thead>
        <!-- going to be available -->
        <thead>
            <tr><td><?php echo $text_going ?></td></tr>
        </thead>
       <tbody>
           <tr><td></td></tr>  
      </tbody>
        
        <!-- end of going to be available -->
        
        <!--  available -->
        <thead>
            <tr><td><?php echo $text_available ?></td></tr>
        </thead>
       <tbody>
           <tr><td></td></tr>  
      </tbody>
        
        <!-- end of  available -->
        
        <!--  links -->
        <thead>
            <tr><td><?php echo $text_links?></td></tr>
        </thead>
       <tbody>
           <tr><td class="table_link"><a href="<?php echo $manager_link ?>"><?php echo $text_manager ?></a></td></tr>
           <tr><td class="table_link"><a href="<?php echo $watch_link ?>"><?php echo $text_watch ?></a></td></tr>
            <tr><td class="table_link"><a href="<?php echo $rest_link ?>"><?php echo $text_rest ?></a></td></tr>
              <tr><td class="table_link"><a href="<?php echo $quik_link ?>"><?php echo $text_quick ?></a></td></tr>
              <tr><td class="table_link"><a href="<?php echo $curier_collect_link ?>"><?php echo $text_collect_curier ?></a></td></tr>
      </tbody>
        
        <!-- end of  links -->
    </table>
    
</div>
</div>
</div>

<script>
function getUrlParameter(sParam)
{
    var sPageURL = window.location.search.substring(1);
    var sURLVariables = sPageURL.split('&');
    for (var i = 0; i < sURLVariables.length; i++) 
    {
        var sParameterName = sURLVariables[i].split('=');
        if (sParameterName[0] == sParam) 
        {
            return sParameterName[1];
        }
    }
} 
    
var tok = getUrlParameter('token');    
    
$('input.neutral').on('change', function(){
var id = $(this).val();
    
  if($(this).prop('checked') == true){
      
      window.location.href = 'index.php?route=sale/managerest/update&token=' + tok + '&id=' + id;
  
  }else{

     window.location.href = 'index.php?route=sale/managerest/delete&token=' + tok+ '&id=' + id;
  }

});
  $('textarea.comment_rest').on('focusout', function(){
  var comment = $(this).val();
  var id = $(this).attr('id');   
 			$.ajax({
			  url: 'index.php?route=sale/managerest/comment&token=' + tok ,
			  type: "POST",
			  dataType: "html",
			  async: "false", 
			  data: {comment:comment, id:id},
			  success: function(response) {			
			  		location.reload();		  			  					  					  					  				  				  					  	
			  }	  
			});	
  });  
    
   $('input.add_time').on('focusout', function(){
  var comment = $(this).val();
  var id = $(this).attr('id');  
  id = id.replace("number_", "");
    
 			$.ajax({
			  url: 'index.php?route=sale/managerest/addTime&token=' + tok ,
			  type: "POST",
			  dataType: "html",
			  async: "false", 
			  data: {comment:comment, id:id},
			  success: function(response) {			
			  		location.reload();  			  					  					  					  				  				  					  	
			  }	  
			});	
  }); 

</script>
<?php echo $footer ?>