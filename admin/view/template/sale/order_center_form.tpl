<?php echo $header ?>

<div id="content">
<div class="box">
 <div class="heading">
 <h1><img src="view/image/order.png" alt="" /> <?php echo $heading_title; ?></h1>
     <div class="buttons"><a onclick="$('#form').submit();" class="button"><?php echo $entery_edit; ?></a><a onclick="history.go(-1);" class="button"><?php echo $button_back; ?></a></div>
</div>       
   
<div class="content">
<form action="<?php echo $form_action . '&order_id=' . $_GET['order_id'] ?>" method="post" enctype="multipart/form-data" id="form">
<table class="form">
<tr>
    <td><?php echo $entery_name ?></td>
    <td><input type="text" name="name" value="<?php echo $order[0]['name'] ?>"></td>
</tr>    
<tr>
    <td><?php echo $entery_phone ?></td>
    <td><input type="text" name="phone" value="<?php echo $order[0]['phone'] ?>"></td>
</tr>   
<tr>
    <td><?php echo $entery_mail ?></td>
    <td><input type="text" name="mail" value="<?php echo $order[0]['mail'] ?>"></td>
</tr> 
 <tr>
    <td><?php echo $entery_company ?></td>
    <td><input type="text" name="company" value="<?php echo $order[0]['company'] ?>"></td>
</tr>    
 <tr>
    <td><?php echo $entery_companyPhone ?></td>
    <td><input type="text" name="company_phone" value="<?php echo $order[0]['company_phone'] ?>"></td>
</tr>      
<tr>
    <td><?php echo $entery_city ?></td>
   <!-- <td><input type="text" name="city" value="<?php echo $order[0]['city'] ?>"></td>-->
   <td>
    <select name="city">
    <?php foreach($cities as $city){?>
       
        <?php if($city['name'] == $order[0]['city']){ ?>
          <option value="<?php echo $city['name'] ?>" selected><?php echo $city['name'] ?></option>
        <?php }else{ ?>
        <option value="<?php echo $city['name'] ?>"><?php echo $city['name'] ?></option>
        <?php }  ?>
       
    <?php } ?>
       </select>
    </td>
</tr>  
    
<tr>
    <td><?php echo $entery_street ?></td>
    <td><input type="text" name="street" value="<?php echo $order[0]['street'] ?>"></td>
</tr> 
    
<tr>
    <td><?php echo $entery_house ?></td>
    <td><input type="text" name="house_number" value="<?php echo $order[0]['house_number'] ?>"></td>
</tr> 
    
<tr>
    <td><?php echo $entery_entrence ?></td>
    <td><input type="text" name="entrence" value="<?php echo $order[0]['entrence'] ?>"></td>
</tr> 
<tr>
    <td><?php echo $entery_appartment ?></td>
    <td><input type="text" name="appartment_number" value="<?php echo $order[0]['appartment_number'] ?>"></td>
</tr> 
<tr>
    <td><?php echo $entery_floor ?></td>
    <td><input type="text" name="floor" value="<?php echo $order[0]['floor'] ?>"></td>
</tr> 
<tr>
    <td><?php echo $entery_comments ?></td>
    <td><textarea name="comments"><?php echo $order[0]['comments'] ?></textarea></td>
</tr> 
    
<tr>
    <td><?php echo $entery_price ?></td>
    <td><input type="text" name="price" value="<?php echo $order[0]['price'] ?>"></td>
</tr>
<tr>
    <td><?php echo $entery_delivery ?></td>
    <td><input type="text" name="delivery" value="<?php echo $order[0]['delivey'] ?>" readonly></td>
</tr>
<tr>
    <td><?php echo $entery_finelPrice ?></td>
    <td><input type="text" name="finel" value="<?php echo $order[0]['final_price'] ?>" readonly></td>
</tr>
<tr>
    <td><?php echo $entery_date ?></td>
    <td><input type="text" name="date" value="<?php echo $order[0]['create_at'] ?>" readonly></td>
</tr> 
 <tr>
     
    <td><?php echo $entery_restaurent ?></td>
    <td><input type="text" name="restaurent" value="<?php echo $order[0]['restaurent'] ?>" readonly>
         <input type="hidden" name="rest_id" value="<?php echo $order[0]['rest_id'] ?>" readonly></td>
</tr> 
<tr>
    <td><?php echo $entery_time ?></td>
    <td><input type="text" name="time" value="<?php echo $order[0]['time'] ?>"></td>
</tr> 
<?php if(isset($special)){ ?>
<tr>
<td><?php echo $entery_special ?></td> 
    
<td>
<?php for($i = 0; $i<count($special); $i = $i+2){  ?>
    <?php echo $special[$i] . ' : ' . $special[$i+1] . '<br>' ?>
<?php } ?>
</td>   
</tr>
<?php } ?>    
    
<tr>
</form>
   <table class="list"> 
    <thead>
       <tr>
           <td class="left"><?php echo $entery_product ?></td>
           <td class="left"><?php echo $entery_quantity ?></td>
           <td class="left"><?php echo $entery_priceOne ?></td>
           <td class="right"><?php echo $entery_total ?></td>
       </tr>
    </thead>
    <tbody>
     <?php foreach($cart as $key=>$value){ ?>
        <tr>
            <td><?php echo $value->name ?><br>
                
               <?php foreach($value->option as $key_samll=>$value_small){?>
                <small><?php echo $value_small->name . ' : ' . $value_small->option_value ?></small><br>
               <?php } ?>
            </td>
            <td><?php echo $value->quantity ?></td>
            <td><?php echo $value->price ?></td>
            <td><?php echo $value->total ?></td>
        </tr>
    <?php } ?>
    </tbody>
    </table>
    
</tr>
    
</table>   
</div>    
 </div>
</div>


<script>
function getUrlParameters(sParam)
{
    var sPageURL = window.location.search.substring(1);
    var sURLVariables = sPageURL.split('&');
    for (var i = 0; i < sURLVariables.length; i++) 
    {
        var sParameterName = sURLVariables[i].split('=');
        if (sParameterName[0] == sParam) 
        {
            return sParameterName[1];
        }
    }
}    
    
var token = getUrlParameter('token'); 
var order = getUrlParameter('order_id');
console.log(token);  
    
$('select[name="city"]').change(function(){

var id = $('input[name="rest_id"]').val();
console.log(id);
var city = $('select[name="city"] option:selected').val();
var price = parseFloat($('input[name="price"]').val());
var new_price = 0;
			$.ajax({
			  url: "index.php?route=sale/orderCenter/ajaxPrice&token=" +  token,
			  type: "POST",
			  dataType: "html",
			  async: "false", 
			  data: { id:id, city:city},
			  success: function(response) {
                  console.log(response);
			  		var city_arr = JSON.parse(response);
                    $.each( city_arr, function( key, value ) {
                       
                        if(value.city == city){
                               
                               new_price = parseFloat(value.pay);
                               console.log(new_price);
                            }
                     });
                $('input[name="delivery"]').val(new_price);
                $('input[name="finel"]').val(new_price + price);
			  }	  
			});	

})


$('input[name="price"]').focusout(function(){
var price = parseFloat($('input[name="price"]').val());
var delivery = parseFloat($('input[name="delivery"]').val());
$('input[name="finel"]').val(delivery + price);

})
</script>
<?php echo $footer ?>