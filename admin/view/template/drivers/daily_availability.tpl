<?php echo $header; ?>
<div id="content">
  <div class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
    <?php } ?>
  </div>
  <?php if ($error_warning) { ?>
  <div class="warning"><?php echo $error_warning; ?></div>
  <?php } ?>
  <div class="box">
    <div class="heading">
      <h1><img src="view/image/feed.png" alt="" /> <?php echo $heading_title; ?></h1>
      <div class="buttons"><a onclick="$('#form').submit();" class="button"><span><?php echo $button_save; ?></span></a><a onclick="location = '<?php echo $cancel; ?>';" class="button"><span><?php echo $button_cancel; ?></span></a></div>
    </div>
    <div class="content">
      <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form">
        <table class="form">
        <tr>
          <td><?php echo $entry_status; ?></td>
          <td><select name="daily_availability_status">
              <?php if ($daily_availability_status) { ?>
              <option value="1" selected="selected"><?php echo $text_enabled; ?></option>
              <option value="0"><?php echo $text_disabled; ?></option>
              <?php } else { ?>
              <option value="1"><?php echo $text_enabled; ?></option>
              <option value="0" selected="selected"><?php echo $text_disabled; ?></option>
              <?php } ?>
            </select></td>
        </tr>
      </table>
        
      <h1><?php echo $text_def_availability; ?></h1>
          <table class="form">
          <table id="mealAvailability" class="list">
              <thead>
                <tr>
                  <td class="right"><?php echo $column_date; ?></td>
                  <td class="left"><?php echo $column_per_window; ?></td>
                  <td class="left"><?php echo $column_drivers; ?></td>
                  <td class="left"><?php echo 'הזמנות פר חלון'; ?></td>
                </tr>
              </thead>
              <?php
                setlocale(LC_ALL, 'he_IL.UTF-8');
                foreach ($def_availabilitys as $day_info ){
              ?>
              <tbody id="def-availability-row-<?php echo $day_info['day']; ?>">
              <tr>
              <td>
                <?php echo strftime("%A" , strtotime($day_info['day']) ); ?>
              </td>
              
              <td><?php echo $column_per_window; ?><input type="text" name="product_def_availability[<?php echo $day_info['day']; ?>][per_window]" value="<?php echo $day_info['per_window']; ?>" size="3" /></td>
              <td><?php echo $column_drivers; ?><input type="text" name="product_def_availability[<?php echo $day_info['day']; ?>][drivers]" value="<?php echo $day_info['drivers']; ?>" size="3" /></td>
              
              <td><?php echo ($day_info['per_window']*$day_info['drivers']); ?></td>
              
            </tr>
            </tbody>
            <?php } ?>
          </table>
          
          <h1><?php echo $text_custom_availability; ?></h1>
            <table id="availability" class="list">
            <thead>
                <tr>
                    <td class="right"><?php echo $column_date; ?></td>
                    <td class="left"><?php echo $column_per_window; ?></td>
                    <td class="left"><?php echo $column_drivers; ?></td>
                    <td></td>
                </tr>
            </thead>
            <?php $availability_row = 0; ?>
            <?php foreach ($product_availabilitys as $product_availability) { ?>
            <tbody id="availability-row<?php echo $availability_row; ?>">
              <tr>
                
                <td class="left"><input type="text" name="product_availability[<?php echo $availability_row; ?>][custom_date]" value="<?php echo $product_availability['custom_date']; ?>" class="date" /></td>
                <td class="right"><input type="text" name="product_availability[<?php echo $availability_row; ?>][per_window]" value="<?php echo $product_availability['per_window']; ?>" size="2" /></td>
                <td class="right"><input type="text" name="product_availability[<?php echo $availability_row; ?>][drivers]" value="<?php echo $product_availability['drivers']; ?>" size="2" /></td>
                
                <td class="left"><a onclick="$('#availability-row<?php echo $availability_row; ?>').remove();" class="button"><?php echo $button_remove; ?></a></td>
              </tr>
            </tbody>
            <?php $availability_row++; ?>
            <?php } ?>
            <tfoot>
              <tr>
                <td colspan="3"></td>
                <td class="left"><a onclick="addAvailability();" class="button"><?php echo $button_add_availability; ?></a></td>
              </tr>
            </tfoot>
          </table>
          </table>
    </form>
    </div>
  </div>
</div>

<script type="text/javascript"><!--

$('.date').datepicker({dateFormat: 'yy-mm-dd'});

var availability_row = <?php echo $availability_row; ?>; // homeals

function addAvailability() {
	html  = '<tbody id="availability-row' + availability_row + '">';
	html += ' <tr>';
    
        html += '<td class="left"><input type="text" name="product_availability[' + availability_row + '][custom_date]" value="" class="date" /></td>';
        html += '<td class="right"><input type="text" name="product_availability[' + availability_row + '][per_window]" value="" size="2" /></td>';
        html += '<td class="right"><input type="text" name="product_availability[' + availability_row + '][drivers]" value="" size="2" /></td>';

        html += '<td class="left"><a onclick="$(\'#availability-row'+ availability_row +'\').remove();" class="button"><?php echo $button_remove; ?></a></td>';
        
        html += ' </tr>';
        html += '</tbody>';
	
	$('#availability tfoot').before(html);
    $('.date').datepicker({dateFormat: 'yy-mm-dd'});
    
	availability_row++;
}
//--></script>

<?php echo $footer; ?>