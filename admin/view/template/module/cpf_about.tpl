<div id="tab-license">
  <table class="list">
    <thead>
	<tr>
	  <td class="left">Order ID:</td>
	  <td>Email:</td>
	  <td>Licensed Domain:</td>
	  <td>Activated Date:</td>
	</tr>
	</thead>
	<tbody>
	  <tr>
		<td class="left"><?php echo $order_id; ?></td>
		<td><?php echo $email; ?></td>
		<td><?php echo $domain; ?></td>
		<td><?php echo $activated_date; ?></td>
	  </tr>
	  <tr>
	    <td colspan="4" class="center">
		  To revoke your license for this domain, click <a href="#" id="button-revoke">here</a>.<br /><br />
		  <a href="http://license.marketinsg.com/?order_id=<?php echo $order_id; ?>&email=<?php echo $email; ?>" class="button" target="_blank">License Portal</a>
		</td>
	  </tr>
	</tbody>
  </table>
</div>
<?php if (!$order_id) { ?>
<div style="position:fixed;height:100%;width:100%;z-index:1;background:rgba(255,255,255,0.9);top:0;left:0;">
  <div class="container">
	<div class="row" style="padding:200px;">
	  <h2>Register Extension for Support &amp; Updates</h2>
	  <table class="form">
	    <tr>
		  <td>Order ID</td>
		  <td><input type="text" name="license_order_id" value="" /></td>
		</tr>
		<tr>
		  <td>Email</td>
		  <td><input type="text" name="license_email" value="" /></td>
		</tr>
		<tr>
		  <td colspan="2"><a id="button-license" class="button">License this Domain</a></td>
		</tr>
	  </table>
    </div>
  </div>
</div>
<?php } ?>
<div id="tab-about">
  <div style="float:left;margin-right:15px;width:70%;">
	<h2>Support</h2>
	<table class="form">
	  <tr>
	    <td colspan="2">Need support? Fill up the form below to open a support ticket.</td>
	  </tr>
	  <tr>
	    <td>Your Name</td>
	    <td><input type="text" name="mail_name" placeholder="Your Name" value="" /></td>
	  </tr>
	  <tr>
		<td>Email Address</td>
		<td><input type="text" name="mail_email" placeholder="Email Address" value="" /></td>
	  </tr>
	  <tr>
		<td>Order ID</td>
		<td><input type="text" name="mail_order_id" placeholder="Order ID" value="" /></td>
	  </tr>
	  <tr>
		<td>Message</td>
		<td><textarea name="mail_message" style="width:500px; height:200px;" placeholder="Describe your issues. Provide your store admin and FTP login credentials if you require technical support."></textarea></td>
	  </tr>
	  <tr>
		<td colspan="2"><a class="button" id="button-mail">Contact Support</a></td>
	  </tr>
	  <tr>
		<td><a class="button" href="http://www.opencart.com/index.php?route=extension/extension/info&extension_id=<?php echo $extension_id; ?>" target="_blank" rel="nofollow">Rate <?php echo $extension; ?></td>
		<td><a class="button" href="http://www.marketinsg.com/<?php echo $purchase_url; ?>" target="_blank">Purchase <?php echo $extension; ?></a></td>
	  </tr>
	</table>
  </div>
  <div style="float:left;">
	<h2>Follow Us</h2>
	<iframe src="//www.facebook.com/plugins/likebox.php?href=http%3A%2F%2Fwww.facebook.com%2FEquotix&amp;width=262&amp;height=558&amp;show_faces=true&amp;colorscheme=light&amp;stream=true&amp;show_border=false&amp;header=false&amp;appId=391573267589280" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:262px; height:558px;" allowTransparency="true"></iframe>
  </div>
</div>
<style type="text/css">
.text-danger {
	color: #f00;
}
</style>
<script type="text/javascript">
$('#button-license').on('click', function() {
	$.ajax({
		url: 'index.php?route=module/<?php echo $code; ?>/license&token=<?php echo $token; ?>',
		type: 'post',
		data: $('input[name=\'license_order_id\'], input[name=\'license_email\']'),
		dataType: 'json',
		beforeSend: function() {
			$('#button-license').after('<i class="fa fa-spinner fa-spin"></i>');
		},
		success: function(json) {
			$('.fa-spinner').remove();
		
			if (json['success']) {
				location.reload();
			} else if (json['error']) {
				alert(json['error']);
			}
		},
		error: function(xhr, ajaxOptions, thrownError) {
			alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
		}
	});
});

$('#button-revoke').on('click', function(e) {
	e.preventDefault();
	
	$.ajax({
		url: 'index.php?route=module/<?php echo $code; ?>/revoke&token=<?php echo $token; ?>',
		type: 'get',
		dataType: 'json',
		beforeSend: function() {
			$('#button-revoke').after('<i class="fa fa-spinner fa-spin"></i>');
		},
		success: function(json) {
			$('.fa-spinner').remove();
		
			if (json['success']) {
				location.reload();
			} else if (json['error']) {
				alert(json['error']);
			}
		},
		error: function(xhr, ajaxOptions, thrownError) {
			alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
		}
	});
});
</script>
<script type="text/javascript"><!--//
$('#button-mail').on('click', function() {
	$.ajax({
		url: 'index.php?route=module/<?php echo $code; ?>/mail&token=<?php echo $token; ?>',
		type: 'post',
		data: $('input[name=\'mail_name\'], input[name=\'mail_email\'], input[name=\'mail_order_id\'], textarea[name=\'mail_message\']'),
		dataType: 'json',
		beforeSend: function() {
			$('#button-mail').after('<i class="fa fa-spinner"></i>');
		},
		success: function(json) {
			$('.fa-spinner, .text-danger').remove();
			
			if (json['error']) {
				if (json['error']['warning']) {
					alert(json['error']['warning']);
				}
				
				if (json['error']['name']) {
					$('input[name=\'mail_name\']').after('<div class="text-danger">' + json['error']['name'] + '</span>');
				}
				
				if (json['error']['email']) {
					$('input[name=\'mail_email\']').after('<div class="text-danger">' + json['error']['email'] + '</span>');
				}
				
				if (json['error']['order_id']) {
					$('input[name=\'mail_order_id\']').after('<div class="text-danger">' + json['error']['order_id'] + '</span>');
				}
				
				if (json['error']['message']) {
					$('textarea[name=\'mail_message\']').after('<div class="text-danger">' + json['error']['message'] + '</span>');
				}
			} else {
				alert(json['success']);
				
				$('input[name=\'mail_name\']').val('');
				$('input[name=\'mail_email\']').val('');
				$('input[name=\'mail_order_id\']').val('');
				$('textarea[name=\'mail_message\']').val('');
			}
		},
		error: function(xhr, ajaxOptions, thrownError) {
			alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
		}
	});
});
//--></script>