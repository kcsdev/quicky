<?php echo $header; ?>
<div id="content">
  <div class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
    <?php } ?>
  </div>
  <?php if ($error_warning) { ?>
  <div class="warning"><?php echo $error_warning; ?></div>
  <?php } ?>
  <div class="box">
    <div class="heading">
      <h1><img src="view/image/module.png" alt="" /> <?php echo $heading_title; ?></h1>
      <div class="buttons"><a onclick="$('#form').submit();" class="button"><?php echo $button_save; ?></a><a href="<?php echo $cancel; ?>" class="button"><?php echo $button_cancel; ?></a></div>
    </div>
    <div class="content">
      <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form">
		<div>
        	<table class="form">
				<tr>
					<td><?php echo $entry_status; ?></td>
					<td><select name="lbcoupon_vibhag[status]">
							  <?php if ($modules['status']) { ?>
							  <option value="1" selected="selected"><?php echo $text_enabled; ?></option>
							  <option value="0"><?php echo $text_disabled; ?></option>
							  <?php } else { ?>
							  <option value="1"><?php echo $text_enabled; ?></option>
							  <option value="0" selected="selected"><?php echo $text_disabled; ?></option>
							  <?php } ?>
							</select></td>
				</tr>
				<tr>
					<td><?php echo $entry_discount_type; ?></td>
					<td><select name="lbcoupon_vibhag[discount_type]">
							  <?php if ($modules['discount_type'] == 'F') { ?>
							  <option value="F" selected="selected">Fixed Amount</option>
							  <option value="P">Percentage</option>
							  <?php } else { ?>
							  <option value="F">Fixed Amount</option>
							  <option value="P" selected="selected">Percentage</option>
							  <?php } ?>
							</select></td>
				</tr>
				<tr>
					<td><?php echo $entry_discount_amount; ?></td>
					<td><input type="text" name="lbcoupon_vibhag[discount_amount]" value="<?php if(isset($modules['discount_amount'])) echo $modules['discount_amount']; ?>" size="8" /></td>
				</tr>
				<tr>
					<td><?php echo $entry_discount_duration; ?></td>
					<td><input type="text" name="lbcoupon_vibhag[discount_duration]" value="<?php if(isset($modules['discount_duration'])) echo $modules['discount_duration']; ?>" size="8" /></td>
				</tr>
			</table>
		</div>
      </form>
    </div>
  </div>
</div>
<?php echo $footer; ?>