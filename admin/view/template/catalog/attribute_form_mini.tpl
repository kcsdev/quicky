<?php echo '<?xml version="1.0" encoding="UTF-8"?>' . "\n"; ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" dir="ltr" lang="en" xml:lang="en">
<head>
<title>Add Attribute</title>
<base href="<?php echo HTTPS_SERVER; ?>" />
<link rel="stylesheet" type="text/css" href="view/stylesheet/stylesheet.css" />
<script type="text/javascript" src="view/javascript/jquery/jquery-1.6.1.min.js"></script>
<script type="text/javascript" src="view/javascript/jquery/ui/jquery-ui-1.8.9.custom.min.js"></script>
<link rel="stylesheet" type="text/css" href="view/javascript/jquery/ui/themes/ui-lightness/jquery-ui-1.8.9.custom.css" />
<script type="text/javascript" src="view/javascript/jquery/ui/external/jquery.bgiframe-2.1.2.js"></script>
<script type="text/javascript" src="view/javascript/jquery/tabs.js"></script>
<script type="text/javascript" src="view/javascript/jquery/superfish/js/superfish.js"></script>
<script type="text/javascript">
//-----------------------------------------
// Confirm Actions (delete, uninstall)
//-----------------------------------------
$(document).ready(function(){
	
    // Confirm Delete
    $('#form').submit(function(){
        if ($(this).attr('action').indexOf('delete',1) != -1) {
            if (!confirm ('<?php echo $text_confirm; ?>')) {
                return false;
            }
        }
    });
    	
    // Confirm Uninstall
    $('a').click(function(){
        if ($(this).attr('href') != null && $(this).attr('href').indexOf('uninstall',1) != -1) {
            if (!confirm ('<?php echo $text_confirm; ?>')) {
                return false;
            }
        }
    });
});
</script>
</head>
<body>
<div id="container">
<div id="content" style="min-width:0px;padding-bottom:20px;padding-top:20px;">
  <div class="box" style="">
    <div class="heading">
      <h1><img src="view/image/information.png" alt="" /> <?php echo $heading_title; ?></h1>
      <div class="buttons"><a onclick="Save();" class="button"><span>Save and Close</span></a></div>
    </div>
    <div class="content" style="min-height:0px;">
      <form action="" method="post" enctype="multipart/form-data" id="form">
        <table class="form">
          <tr>
            <td><span class="required">*</span> <?php echo $entry_name; ?></td>
            <td><?php foreach ($languages as $language) { ?>
              <input type="text" name="attribute_description[<?php echo $language['language_id']; ?>][name]" value="" />
              <img src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" /><br />
              <?php } ?></td>
          </tr>
          <tr>
            <td><?php echo $entry_attribute_group; ?></td>
            <td><select name="attribute_group_id">
                <?php foreach ($attribute_groups as $attribute_group) { ?>
                <option value="<?php echo $attribute_group['attribute_group_id']; ?>"><?php echo $attribute_group['name']; ?></option>
                <?php } ?>
              </select></td>
          </tr>
          <tr>
            <td><?php echo $entry_sort_order; ?></td>
            <td><input type="text" name="sort_order" value="" size="1" /></td>
          </tr>
        </table>
      </form>
    </div>
  </div>
</div>
</div>
<script type="text/javascript">
function Save() {
	$.ajax({
		url: 'index.php?route=catalog/attribute/AJAXinsert&token=<?php echo $this->session->data['token']; ?>',
		type: 'POST',
		data: $('#form').serialize(),
		dataType: 'json',
		beforeSend: function () {
			$('.warning').remove();
		},
		success: function(json) {
			if(json['success']) {
				$('.box').before('<div class="success">' + json['success'] + '</div>');
				setTimeout(function() {
					parent.$('input[name=\'product_attribute[<?php echo $attribute_row; ?>][attribute_id]\']').attr('value', json['attribute_id']);
					parent.$('input[name=\'product_attribute[<?php echo $attribute_row; ?>][name]\']').attr('value', json['attribute_name']);
					parent.$('#dialog').dialog('close');
					parent.$('#dialog').remove();	
					return false;
				},1000);
			}
			if(json['warning']) {
				$('.box').before('<div class="warning">' + json['warning'] + '</div>');
			}
		}
	});
}
</script>
</body>
</html>
