<?php echo '<?xml version="1.0" encoding="UTF-8"?>' . "\n"; ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" dir="ltr" lang="en" xml:lang="en">
<head>
<title>Add Attribute</title>
<base href="<?php echo HTTPS_SERVER; ?>" />
<link rel="stylesheet" type="text/css" href="view/stylesheet/stylesheet.css" />
<script type="text/javascript" src="view/javascript/jquery/jquery-1.6.1.min.js"></script>
<script type="text/javascript" src="view/javascript/jquery/ui/jquery-ui-1.8.9.custom.min.js"></script>
<link rel="stylesheet" type="text/css" href="view/javascript/jquery/ui/themes/ui-lightness/jquery-ui-1.8.9.custom.css" />
<script type="text/javascript" src="view/javascript/jquery/ui/external/jquery.bgiframe-2.1.2.js"></script>
<script type="text/javascript" src="view/javascript/jquery/tabs.js"></script>
<script type="text/javascript" src="view/javascript/jquery/superfish/js/superfish.js"></script>
<script type="text/javascript">
//-----------------------------------------
// Confirm Actions (delete, uninstall)
//-----------------------------------------
$(document).ready(function(){
	
    // Confirm Delete
    $('#form').submit(function(){
        if ($(this).attr('action').indexOf('delete',1) != -1) {
            if (!confirm ('<?php echo $text_confirm; ?>')) {
                return false;
            }
        }
    });
    	
    // Confirm Uninstall
    $('a').click(function(){
        if ($(this).attr('href') != null && $(this).attr('href').indexOf('uninstall',1) != -1) {
            if (!confirm ('<?php echo $text_confirm; ?>')) {
                return false;
            }
        }
    });
});
</script>
</head>
<body>
<div id="container">
<div id="content" style="min-width:0px;padding-bottom:20px;padding-top:20px;">
  <div class="box">
    <div class="heading">
      <h1><img src="view/image/information.png" alt="" /> <?php echo $heading_title; ?></h1>
      <div class="buttons"><a onclick="Save();" class="button"><span>Save and Close</span></a></div>
    </div>
    <div class="content">
      <form action="" method="post" enctype="multipart/form-data" id="form">
        <table class="form">
          <tr>
            <td><span class="required">*</span> <?php echo $entry_name; ?></td>
            <td><?php foreach ($languages as $language) { ?>
              <input type="text" name="option_description[<?php echo $language['language_id']; ?>][name]" value="<?php echo isset($option_description[$language['language_id']]) ? $option_description[$language['language_id']]['name'] : ''; ?>" />
              <img src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" /><br />
              <?php } ?></td>
          </tr>
          <tr>
            <td><?php echo $entry_type; ?></td>
            <td><select name="type">
                <optgroup label="<?php echo $text_choose; ?>">
					<option value="select"><?php echo $text_select; ?></option>
					<option value="radio"><?php echo $text_radio; ?></option>
					<option value="checkbox"><?php echo $text_checkbox; ?></option>
                </optgroup>
                <optgroup label="<?php echo $text_input; ?>">
					<option value="text"><?php echo $text_text; ?></option>
					<option value="textarea"><?php echo $text_textarea; ?></option>
                </optgroup>
                <optgroup label="<?php echo $text_file; ?>">
					<option value="file"><?php echo $text_file; ?></option>
                </optgroup>
                <optgroup label="<?php echo $text_date; ?>">
					<option value="date"><?php echo $text_date; ?></option>
					<option value="time"><?php echo $text_time; ?></option>
					<option value="datetime"><?php echo $text_datetime; ?></option>
                </optgroup>
              </select></td>
          </tr>
          <tr>
            <td><?php echo $entry_sort_order; ?></td>
            <td><input type="text" name="sort_order" value="" size="1" /></td>
          </tr>
        </table>
        <table id="option-value" class="list">
          <thead>
            <tr>
              <td class="left"><span class="required">*</span> <?php echo $entry_value; ?></td>
              <td class="right"><?php echo $entry_sort_order; ?></td>
              <td></td>
            </tr>
          </thead>
          <tfoot>
            <tr>
              <td colspan="2"></td>
              <td class="left"><a onclick="addOptionValue();" class="button"><span><?php echo $button_add_option_value; ?></span></a></td>
            </tr>
          </tfoot>
        </table>
      </form>
    </div>
  </div>
</div>
<script type="text/javascript">
function Save() {
	$.ajax({
		url: 'index.php?route=catalog/option/AJAXinsert&token=<?php echo $this->session->data['token']; ?>',
		type: 'POST',
		data: $('#form').serialize(),
		dataType: 'json',
		beforeSend: function () {
			$('.warning').remove();
		},
		success: function(json) {
			if(json['success']) {
				$('.box').before('<div class="success">' + json['success'] + '</div>');
					
				html  = '<div id="tab-option-<?php echo $option_row; ?>" class="vtabs-content">';
				html += '	<input type="hidden" name="product_option[<?php echo $option_row; ?>][product_option_id]" value="" />';
				html += '	<input type="hidden" name="product_option[<?php echo $option_row; ?>][name]" value="' + json['name'] + '" />';
				html += '	<input type="hidden" name="product_option[<?php echo $option_row; ?>][option_id]" value="' + json['option_id'] + '" />';
				html += '	<input type="hidden" name="product_option[<?php echo $option_row; ?>][type]" value="' + json['type'] + '" />';
				html += '	<table class="form">';
				html += '	  <tr>';
				html += '		<td><?php echo $entry_required; ?></td>';
				html += '       <td><select name="product_option[<?php echo $option_row; ?>][required]">';
				html += '	      <option value="1"><?php echo $text_yes; ?></option>';
				html += '	      <option value="0"><?php echo $text_no; ?></option>';
				html += '	    </select></td>';
				html += '     </tr>';
				
				if (json['type'] == 'text') {
					html += '     <tr>';
					html += '       <td><?php echo $entry_option_value; ?></td>';
					html += '       <td><input type="text" name="product_option[<?php echo $option_row; ?>][option_value]" value="" /></td>';
					html += '     </tr>';
				}
				
				if (json['type'] == 'textarea') {
					html += '     <tr>';
					html += '       <td><?php echo $entry_option_value; ?></td>';
					html += '       <td><textarea name="product_option[<?php echo $option_row; ?>][option_value]" cols="40" rows="5"></textarea></td>';
					html += '     </tr>';						
				}
				 
				if (json['type'] == 'file') {
					html += '     <tr style="display: none;">';
					html += '       <td><?php echo $entry_option_value; ?></td>';
					html += '       <td><input type="text" name="product_option[<?php echo $option_row; ?>][option_value]" value="" /></td>';
					html += '     </tr>';			
				}
								
				if (json['type'] == 'date') {
					html += '     <tr>';
					html += '       <td><?php echo $entry_option_value; ?></td>';
					html += '       <td><input type="text" name="product_option[<?php echo $option_row; ?>][option_value]" value="" class="date" /></td>';
					html += '     </tr>';			
				}
				
				if (json['type'] == 'datetime') {
					html += '     <tr>';
					html += '       <td><?php echo $entry_option_value; ?></td>';
					html += '       <td><input type="text" name="product_option[<?php echo $option_row; ?>][option_value]" value="" class="datetime" /></td>';
					html += '     </tr>';			
				}
				
				if (json['type'] == 'time') {
					html += '     <tr>';
					html += '       <td><?php echo $entry_option_value; ?></td>';
					html += '       <td><input type="text" name="product_option[<?php echo $option_row; ?>][option_value]" value="" class="time" /></td>';
					html += '     </tr>';			
				}
				
				html += '  </table>';
					
				if (json['type'] == 'select' || json['type'] == 'radio' || json['type'] == 'checkbox') {
					html += '  <table id="option-value<?php echo $option_row; ?>" class="list">';
					html += '  	 <thead>'; 
					html += '      <tr>';
					html += '        <td class="left"><?php echo $entry_option_value; ?></td>';
					html += '        <td class="right"><?php echo $entry_quantity; ?></td>';
					html += '        <td class="left"><?php echo $entry_subtract; ?></td>';
					html += '        <td class="right"><?php echo $entry_price; ?></td>';
					html += '        <td class="right"><?php echo $entry_option_points; ?></td>';
					html += '        <td class="right"><?php echo $entry_weight; ?></td>';
					html += '        <td></td>';
					html += '      </tr>';
					html += '  	 </thead>';
					html += '    <tfoot>';
					html += '      <tr>';
					html += '        <td colspan="6"></td>';
					html += '        <td class="left"><a onclick="addOptionValue(<?php echo $option_row; ?>);" class="button"><span><?php echo $button_add_option_value; ?></span></a></td>';
					html += '      </tr>';
					html += '    </tfoot>';
					html += '  </table>';
					html += '</div>';	
				}					
	
				parent.$('#tab-option').append(html);
				
				parent.$('#option-add').before('<a href="#tab-option-<?php echo $option_row; ?>" id="option-<?php echo $option_row; ?>">' + json['name'] + '&nbsp;<img src="view/image/delete.png" alt="" onclick="$(\'#vtab-option a:first\').trigger(\'click\'); $(\'#option-<?php echo $option_row; ?>\').remove(); $(\'#tab-option-<?php echo $option_row; ?>\').remove(); return false;" /></a>');
								
				setTimeout(function() {							
					parent.$('#dialog').dialog("close");	
					parent.$('#dialog').remove();
					return false;
				},1000);
			}
			if(json['warning']) {
				$('.box').before('<div class="warning">' + json['warning'] + '</div>');
			}
		}
	});
}
</script>
<script type="text/javascript"><!--
$('select[name=\'type\']').bind('change', function() {
	if (this.value == 'select' || this.value == 'radio' || this.value == 'checkbox') {
		$('#option-value').show();
	} else {
		$('#option-value').hide();	
	}
});

var option_value_row = 0;

function addOptionValue() {
	html  = '<tbody id="option-value-row' + option_value_row + '">';
	html += '<tr>';	
    html += '<td class="left"><input type="hidden" name="option_value[' + option_value_row + '][option_value_id]" value="" />';
	<?php foreach ($languages as $language) { ?>
	html += '<input type="text" name="option_value[' + option_value_row + '][option_value_description][<?php echo $language['language_id']; ?>][name]" value="" /> <img src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" /><br />';
    <?php } ?>
	html += '</td>';
	html += '<td class="right"><input type="text" name="option_value[' + option_value_row + '][sort_order]" value="" size="1" /></td>';
	html += '<td class="left"><a onclick="$(\'#option-value-row' + option_value_row + '\').remove();" class="button"><span><?php echo $button_remove; ?></span></a></td>';
	html += '</tr>';	
    html += '</tbody>';
	
	$('#option-value tfoot').before(html);
	
	option_value_row++;
}
//--></script> 
</body>
</html>
