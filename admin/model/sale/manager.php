<?php
class ModelSaleManager extends Model {
    
public function getFree(){

 $query = $this->db->query("SELECT oc_curier.first_name, oc_curier.last_name FROM oc_new_order INNER JOIN oc_curier ON oc_new_order.curier_id=oc_curier.id WHERE oc_new_order.status <> 2 GROUP BY oc_new_order.curier_id HAVING count(oc_new_order.curier_id) = 1 ");    
 return $query->rows;
}


public function getList(){

    $query = $this->db->query("SELECT oc_new_order.*,  oc_customer.customer_id FROM `oc_new_order`   JOIN oc_customer ON oc_customer.email =  oc_new_order.mail");

   return $query->rows;


}
public function getRun(){

    $query = $this->db->query("SELECT oc_new_order.*,  oc_customer.customer_id FROM `oc_new_order`  JOIN oc_customer ON oc_customer.email =  oc_new_order.mail WHERE oc_new_order.status <> 2 AND oc_new_order.status <> 5");

   return $query->rows;


}    
 
    
public function getNoCurier(){

    $query = $this->db->query("SELECT oc_new_order.*,  oc_customer.customer_id FROM `oc_new_order`  JOIN oc_customer ON oc_customer.email =  oc_new_order.mail WHERE oc_new_order.curier_id = 0");

   return $query->rows;


}
    
public function getFaxValidate(){

    $query = $this->db->query("SELECT oc_new_order.*,  oc_customer.customer_id FROM `oc_new_order`  JOIN oc_customer ON oc_customer.email =  oc_new_order.mail WHERE oc_new_order.fax = 3");

   return $query->rows;


}
    
public function getStatusGived(){

    $query = $this->db->query("SELECT oc_new_order.*,  oc_customer.customer_id FROM `oc_new_order`  JOIN oc_customer ON oc_customer.email =  oc_new_order.mail WHERE oc_new_order.status = 2");

   return $query->rows;


}
    
 public function update($data){
 
  $this->db->query("UPDATE oc_new_order SET " . $data['type'] . " = " . (int)$data['status'] . " WHERE id = " . (int)$data['id']);
 
  }
    
public function getphone($id){
 $query = $this->db->query("SELECT telephone FROM oc_customer WHERE customer_id =" . $id);

     return $query->row;
  }
    
 public function updateCurier($data){
  $this->db->query("UPDATE  oc_new_order SET curier_id = " . (int)$data['curier'] . " WHERE id = " . (int)$data['id']);
  $curiers = $this->db->query("SELECT * FROM oc_watch LEFT OUTER JOIN oc_new_order ON (oc_watch.curier_id = oc_new_order.curier_id) WHERE oc_new_order.curier_id IS NULL AND oc_new_order.status<>2");
  
 }

 public function getCurier($id){
 
  $query = $this->db->query("SELECT CONCAT (`first_name`,' ', `last_name`) as name FROM `oc_curier` WHERE `id` =" . $id);

   return $query->rows;
 
 }
    
 public function getCrf($id){
  
    $query = $this->db->query("SELECT crf FROM oc_customer WHERE customer_id = " . $id);
    
    return $query->rows;
 
 }
    
 public function gettime($id){
 
    $query = $this->db->query("SELECT data FROM oc_customer_cities WHERE customer_id = " . $id);
     
    return $query->rows;
 
   }
    
 public function getStatus(){
     
     $query = $this->db->query("SELECT * FROM oc_new_order WHERE status <> 2");
      
     return $query->rows;
   }
    
 public function gettimebar($id){
 
      $query = $this->db->query("SELECT bar_start FROM oc_new_order WHERE id = " . $id);
      
     return $query->rows;
 
 }
    
public function updateFaxTime($id, $time){

  $this->db->query("UPDATE oc_new_order SET time_fax = " . $time . " WHERE id =" . $id);
 }
    
 public function getFaxTime($id){
     
 $query = $this->db->query("SELECT time_fax FROM oc_new_order WHERE id = " . $id);
      
     return $query->rows;
 
 }
    
public function updateRestTime($id, $time){

  $this->db->query("UPDATE oc_new_order SET time_rest = " . $time . " WHERE id =" . $id);
 }
    
 public function getRestTime($id){
     
 $query = $this->db->query("SELECT time_rest FROM oc_new_order WHERE id = " . $id);
      
     return $query->rows;
 
 }
    
public function updateCurierTime($id, $time){

  $this->db->query("UPDATE oc_new_order SET time_curier = " . $time . " WHERE id =" . $id);
 }


}



?>