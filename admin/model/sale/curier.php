<?php
class ModelSaleCurier extends Model {
    
   public function getList(){
   
   $query = $this->db->query("SELECT * FROM oc_curier");
       
    return $query->rows;
   
   
   }
    
   public function insert($data){

    $this->db->query("INSERT INTO oc_curier SET first_name = '" . $data['firstname'] . "', last_name = '" . $data['lastname'] . "', phone = '" . $data['phone'] . "', home_phone = '" . $data['homephone'] . "', personal_id = '" . $data['id'] . "', address = '" . $data['address'] . "', create_at = NOW(), status = " . (isset($data['status']) ? 1 : 0) . ", curier = " . (isset($data['curier_job']) ? 1 : 0)  . ", phone_job = " .  (isset($data['phone_job']) ? 1 : 0));


   }
    
  public function getCurier($id){
  
     $query = $this->db->query("SELECT * FROM oc_curier WHERE id = " . $id);
       
    return $query->rows;
  
  
  }
    
 public function update($data, $id){
 
 
  $this->db->query("UPDATE oc_curier SET first_name = '" . $data['firstname'] . "', last_name = '" . $data['lastname'] . "', phone = '" . $data['phone'] . "', home_phone = '" . $data['homephone'] . "', personal_id = '" . $data['id'] . "', address = '" . $data['address'] . "', status = " . (isset($data['status']) ? 1 : 0) . ", curier = " . (isset($data['curier_job']) ? 1 : 0)  . ", phone_job = " .  (isset($data['phone_job']) ? 1 : 0) .  " WHERE id = " . $id);
 
 }
    
 public function delete($id){
 
  $this->db->query("DELETE FROM oc_curier WHERE id = " . $id);
 
 }



}



?>