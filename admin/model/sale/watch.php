<?php
class ModelSaleWatch extends Model {

public function getCuriers(){

$query = $this->db->query("SELECT oc_curier.first_name, oc_curier.last_name, oc_curier.id FROM   oc_curier LEFT OUTER JOIN oc_watch ON (oc_curier.id =  oc_watch.curier_id) WHERE oc_watch.curier_id IS NULL");
    
return $query->rows;


}
    
public function get($id){
    
$query = $this->db->query("SELECT * FROM oc_curier WHERE id = " . $id);
    
return $query->rows;

}
    
public function getCurier($id){
$query = $this->db->query("SELECT oc_watch.*, oc_curier.first_name, oc_curier.last_name FROM  oc_watch INNER JOIN oc_curier ON oc_curier.id=oc_watch.curier_id WHERE oc_watch.curier_id = " . $id);
    
return $query->rows;


}
    
 public function insert($data){
     
  $this->db->query("INSERT INTO oc_watch SET curier_id = " . (int)$data['curier'] . ", mirs = '" . $data['mirs'] . "', motorcycle = '" . $data['motorcycle'] . "',  change_money = '" . $data['change'] . "', storm_suit = " . (isset($data['storm']) ? 1 : 0) . ", create_at = NOW()");
     
    $watch_id = $this->db->getLastId();
    date_default_timezone_set('Asia/Jerusalem'); 
    $date = date("d-m-Y"); 
    $time = date("H:i:s"); 
  $this->db->query("INSERT INTO  oc_watch_times SET curier_id = " . (int)$data['curier'] . ", watch_id = " . $watch_id . ", type = 1, date = '" . $date . "', start_time = '" . $time . "'" );
  
 }
    
 public function getList(){
  
  $query = $this->db->query("SELECT oc_curier.first_name, oc_curier.last_name, oc_curier.id, oc_watch.curier_id, oc_watch.break FROM   oc_curier LEFT OUTER JOIN oc_watch ON (oc_curier.id =  oc_watch.curier_id) WHERE oc_curier.status = 1");    
 return $query->rows;
 
 }
    
 public function update($data, $id){
  
    $this->db->query("UPDATE  oc_watch SET  mirs = '" . $data['mirs'] . "', motorcycle = '" . $data['motorcycle'] . "',  change_money = '" . $data['change'] . "', storm_suit = " . (isset($data['storm']) ? 1 : 0) . " WHERE curier_id = " . $id);
 
 }
    
 public function delete($id){
 
    $watch_id = $this->db->query("SELECT id FROM oc_watch WHERE curier_id = " . $id);
    $start_time = $this->db->query("SELECT start_time FROM oc_watch_times WHERE watch_id=" . $watch_id->rows[0]['id'] . " AND type=1 ORDER BY id DESC LIMIT 1");
    date_default_timezone_set('Asia/Jerusalem'); 
     date_default_timezone_set('Asia/Jerusalem'); 
      $time = date("H:i:s");
 $st_time = explode(":", $start_time->rows[0]['start_time']);
 $cr_time = explode(":", $time);
 $minuts = intval ($st_time[1]);
 $cr_time_now = intval ($cr_time[1]);
 $sum_time = $cr_time_now - $minuts + 60*(intval ($cr_time[0]) - intval ($st_time[1]));
  if($sum_time < 0 ){
       $sum_time = (60 - $minuts) + $cr_time_now + 60*(intval ($cr_time[0]) - intval ($st_time[1]) - 1);
    
   }
     
    $break_id = $this->db->query("SELECT id FROM oc_watch_times WHERE watch_id = " . $watch_id->rows[0]['id'] . " AND type=1 ORDER BY id DESC LIMIT 1");
    $this->db->query("UPDATE oc_watch_times SET end_time='" . $time  ."', time = '" . $sum_time ."' WHERE id=" .  $break_id->rows[0]['id']);
    $this->db->query("DELETE FROM oc_watch WHERE curier_id = " . $id);
 
 }
    
public function getBreak($id){
  $watch_id = $this->db->query("SELECT id FROM oc_watch WHERE curier_id = " . $id);
  $this->db->query("UPDATE oc_watch SET break = 1 WHERE curier_id = " . $id);
     date_default_timezone_set('Asia/Jerusalem'); 
     $date = date("d-m-Y"); 
    $time = date("H:i:s"); 
  $this->db->query("INSERT INTO  oc_watch_times SET curier_id = " . $id . ", watch_id = " . (int)$watch_id->rows[0]['id'] . ", type = 2, date = '" . $date . "', start_time = '" . $time . "'" );
}
    
 public function finishBreak($id){
 $watch_id = $this->db->query("SELECT id FROM oc_watch WHERE curier_id = " . $id);
 $this->db->query("UPDATE oc_watch SET break = 0 WHERE curier_id = " . $id);
 $break_id = $this->db->query("SELECT id FROM oc_watch_times WHERE watch_id = " . $watch_id->rows[0]['id'] . " AND type=2 ORDER BY id DESC LIMIT 1");
 $start_time = $this->db->query("SELECT start_time FROM oc_watch_times WHERE watch_id=" . $watch_id->rows[0]['id'] . " AND type=2 ORDER BY id DESC LIMIT 1");
 date_default_timezone_set('Asia/Jerusalem'); 
  $time = date("H:i:s");
 $st_time = explode(":", $start_time->rows[0]['start_time']);
 $cr_time = explode(":", $time);
 $minuts = intval ($st_time[1]);
$cr_time_now = intval ($cr_time[1]);
    $sum_time = $cr_time_now - $minuts + 60*(intval ($cr_time[0]) - intval ($st_time[1]));
if($sum_time < 0 ){
    $sum_time = (60 - $minuts) + $cr_time_now + 60*(intval ($cr_time[0]) - intval ($st_time[1]) - 1);
    
}
 $this->db->query("UPDATE oc_watch_times SET end_time='" . $time  ."', time = '" . $sum_time ."' WHERE id=" . $break_id->rows[0]['id']);
 }


}