<?php
// Heading
$_['heading_title']     = 'משתמש';

// Text
$_['text_success']      = 'הצלחה: המשתמשים שונו!';

// Column
$_['column_username']   = 'שם משתמש';
$_['column_status']     = 'מצב';
$_['column_date_added'] = 'תאריך הוספה';
$_['column_action']     = 'פעולה';

// Entry
$_['entry_username']   = 'שם משתמש:';
$_['entry_password']   = 'סיסמה:';
$_['entry_confirm']    = 'אשר:';
$_['entry_firstname']  = 'שם פרטי:';
$_['entry_lastname']   = 'שם משפחה:';
$_['entry_email']      = 'דואר אלקטרוני:';
$_['entry_user_group'] = 'קבוצת משתמש:';
$_['entry_status']     = 'מצב:';
$_['entry_captcha']    = 'הזן את הקוד שבתיבה שלמטה:';

// Error
$_['error_permission'] = 'אזהרה: אין לך הרשאה לשנות משתמשים!';
$_['error_account']    = 'אזהרה: אתה לא יכול למחוק את החשבון של עצמך!';
$_['error_exists']     = 'אזהרה: שם המשתמש כבר בשימוש!';
$_['error_username']   = 'שם המשתמש חייב להכין בין 3 ל-20 תווים!';
$_['error_password']   = 'הסיסמה חייבת להכיל בין 4 ל-20 תווים!';
$_['error_confirm']    = 'הסיסמה ואישור הסיסמה לא תואמים!';
$_['error_firstname']  = 'שם פרטי חייב להכיל בין 1 ל-32 תווים!';
$_['error_lastname']   = 'שם משפחה חייב להכיל בין 1 ל-32 תווים!';
$_['error_captcha']    = 'קוד האימות לא תואם את התמונה!';
?>