<?php
// Heading
$_['heading_title']    = 'גיבוי / שחזור';

// Text
$_['text_backup']      = 'הורדת גיבוי';
$_['text_success']     = 'הצלחה: מסד הנתונים יובא בהצלחה!';

// Entry
$_['entry_restore']    = 'שחזור גיבוי:';
$_['entry_backup']     = 'גיבוי:';

// Error
$_['error_permission'] = 'אזהרה: אין לך הרשאה לשנות גיבויים!';
$_['error_empty']      = 'אזהרה: הקובץ שהעלת היה ריק!';
?>