<?php
// Heading
$_['heading_title']         = 'דיווח הזמנות לקוח';

// Text
$_['text_all_status']       = 'כל המצבים';

// Column
$_['column_customer']       = 'שם לקוח';
$_['column_email']          = 'דואר אלקטרוני';
$_['column_customer_group'] = 'קבוצת לקוח';
$_['column_status']         = 'מצב';
$_['column_orders']         = 'מספר הזמנות';
$_['column_products']       = 'מספר מוצרים';
$_['column_total']          = 'סך הכל';
$_['column_action']         = 'פעולה';

// Entry
$_['entry_date_start']      = 'תאריך התחלה:';
$_['entry_date_end']        = 'תאריך סיום:';
$_['entry_status']          = 'מצב הזמנה:';
?>