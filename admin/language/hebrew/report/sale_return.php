<?php
// Heading
$_['heading_title']     = 'דיווח החזרות';

// Text
$_['text_year']         = 'שנים';
$_['text_month']        = 'חודשים';
$_['text_week']         = 'שבועות';
$_['text_day']          = 'ימים';
$_['text_all_status']   = 'כל המצבים';

// Column
$_['column_date_start'] = 'תאריך התחלה';
$_['column_date_end']   = 'תאריך סיום';
$_['column_returns']    = 'מספר החזרות';
$_['column_products']   = 'מספר מוצרים';

// Entry
$_['entry_date_start']  = 'תאריך התחלה:';
$_['entry_date_end']    = 'תאריך סיום:';
$_['entry_group']       = 'קבץ לפי:';
$_['entry_status']      = 'מצב החזרה:';
?>