<?php
// Heading
$_['heading_title']         = 'דיווח זיכוי לקוח';

// Column
$_['column_customer']       = 'שם לקוח';
$_['column_email']          = 'דואר אלקטרוני';
$_['column_customer_group'] = 'קבוצת לקוח';
$_['column_status']         = 'מצב';
$_['column_total']          = 'סך הכל';
$_['column_action']         = 'פעולה';

// Entry
$_['entry_date_start']      = 'תאריך התחלה:';
$_['entry_date_end']        = 'תאריך סיום:';
?>