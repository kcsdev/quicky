<?php
// Heading
$_['heading_title']     = 'תסקיר עמלת שותפים';

// Column
$_['column_affiliate']  = 'שם שותף';
$_['column_email']      = 'דואר אלקטרוני';
$_['column_status']     = 'מצב';
$_['column_commission'] = 'עמלה';
$_['column_orders']     = 'מספר הזמנות';
$_['column_total']      = 'סך הכל';
$_['column_action']     = 'פעולה';

// Entry
$_['entry_date_start']  = 'תאריך התחלה:';
$_['entry_date_end']    = 'תאריך סיום:';
?>