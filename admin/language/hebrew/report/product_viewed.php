<?php
// Heading
$_['heading_title']  = 'דיווח מוצרים שנצפו';

// Text
$_['text_success']   = 'הצלחה: דיווח המוצרים שנצפו אופס!';

// Column
$_['column_name']    = 'שם מוצר';
$_['column_model']   = 'דגם';
$_['column_viewed']  = 'נצפה';
$_['column_percent'] = 'אחוז';
?>