<?php
// Heading
$_['heading_title']     = 'דיווח מוצרים שנרכשו';

// Text
$_['text_all_status']   = 'כל המצבים';

// Column
$_['column_date_start'] = 'תאריך התחלה';
$_['column_date_end']   = 'תאריך סיום';
$_['column_name']       = 'שם מוצר';
$_['column_model']      = 'דגם';
$_['column_quantity']   = 'כמות';
$_['column_total']      = 'סך הכל';

// Entry
$_['entry_date_start']  = 'תאריך התחלה:';
$_['entry_date_end']    = 'תאריך סיום:';
$_['entry_status']      = 'מצב הזמנה:';
?>