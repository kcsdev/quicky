<?php
// Heading
$_['heading_title']     = 'דיווח הזמנות';

// Text
$_['text_year']         = 'שנים';
$_['text_month']        = 'חודשים';
$_['text_week']         = 'שבועות';
$_['text_day']          = 'ימים';
$_['text_all_status']   = 'כל המצבים';

// Column
$_['column_date_start'] = 'תאריך התחלה';
$_['column_date_end']   = 'תאריך סיום';
$_['column_orders']     = 'מספר הזמנות';
$_['column_products']   = 'מספר מוצרים';
$_['column_tax']        = 'מס';
$_['column_total']      = 'סך הכל';

// Entry
$_['entry_date_start']  = 'תאריך התחלה:';
$_['entry_date_end']    = 'תאריך סיום:';
$_['entry_group']       = 'קבץ לפי:';
$_['entry_status']      = 'מצב הזמנה:';
?>