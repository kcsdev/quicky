<?php
// Heading
$_['heading_title']    = 'דיווח קופונים';

// Column
$_['column_name']      = 'שם קופון';
$_['column_code']      = 'קוד';
$_['column_orders']    = 'הזמנות';
$_['column_total']     = 'סך הכל';
$_['column_action']    = 'פעולה';

// Entry
$_['entry_date_start'] = 'תאריך התחלה:';
$_['entry_date_end']   = 'תאריך סיום:';
?>