<?php 
// Heading
$_['heading_title']     = 'לכל פריט';

// Text
$_['text_shipping']    = 'משלוח';
$_['text_success']     = 'הצלחה: המשלוח שונה!';

// Entry
$_['entry_cost']       = 'עלות:';
$_['entry_tax_class']  = 'סיווג מס:';
$_['entry_geo_zone']   = 'אזור גיאוגרפי:';
$_['entry_status']     = 'מצב:';
$_['entry_sort_order'] = 'סדר המיון:';

// Error
$_['error_permission'] = 'אזהרה: אין לך הרשאה לשנות משלוח!';
?>