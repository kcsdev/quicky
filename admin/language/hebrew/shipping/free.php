<?php
// Heading
$_['heading_title']    = 'משלוח חינם';

// Text 
$_['text_shipping']    = 'משלוח';
$_['text_success']     = 'הצלחה: המשלוח שונה!';

// Entry
$_['entry_total']      = 'סך הכל:<br /><span class="help">סכום הביניים שצריך להגיע אליו לפני שמודול המשלוח חינם הופך לפעיל.</span>';
$_['entry_geo_zone']   = 'אזור גיאוגרפי:';
$_['entry_status']     = 'מצב:';
$_['entry_sort_order'] = 'סדר המיון:';

// Error
$_['error_permission'] = 'אזהרה: אין לך הרשאה לשנות משלוח!';
?>