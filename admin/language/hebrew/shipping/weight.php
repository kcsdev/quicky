<?php
// Heading
$_['heading_title']    = 'משלוח מבוסס משקל';

// Text
$_['text_shipping']    = 'משלוח';
$_['text_success']     = 'הצלחה: המשלוח שונה!';

// Entry
$_['entry_rate']       = 'תעריפים:<br /><span class="help">דוגמה: 5:10.00,7:12.00 משקל:עלות,משקל:עלות, וכן הלאה.</span>';
$_['entry_tax_class']  = 'סיווג מס:';
$_['entry_geo_zone']   = 'אזור גיאוגרפי:';
$_['entry_status']     = 'מצב:';
$_['entry_sort_order'] = 'סדר המיון:';

// Error
$_['error_permission'] = 'אזהרה: אין לך הרשאה לשנות משלוח!';
?>