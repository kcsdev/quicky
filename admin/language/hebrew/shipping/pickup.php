<?php
// Heading
$_['heading_title']    = 'איסוף עצמי';

// Text 
$_['text_shipping']    = 'משלוח';
$_['text_success']     = 'הצלחה: המשלוח שונה!';

// Entry
$_['entry_geo_zone']   = 'אזור גיאוגרפי:';
$_['entry_status']     = 'מצב:';
$_['entry_sort_order'] = 'סדר המיון:';

// Error
$_['error_permission'] = 'אזהרה: אין לך הרשאה לשנות משלוח!';
?>