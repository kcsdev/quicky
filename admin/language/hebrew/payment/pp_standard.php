<?php
// Heading
$_['heading_title']					 = 'PayPal Standard';

// Text
$_['text_payment']					 = 'תשלום';
$_['text_success']					 = 'הצלחה: פרטי חשבון התשלום שונו!';
$_['text_pp_standard']				 = '<a onclick="window.open(\'https://www.paypal.com/uk/mrb/pal=W9TBB5DTD6QJW\');"><img src="view/image/payment/paypal.png" alt="PayPal" title="PayPal" style="border: 1px solid #EEEEEE;" /></a>';
$_['text_authorization']			 = 'הרשאה';
$_['text_sale']						 = 'מכירה';

// Entry
$_['entry_email']					 = 'דואר אלקטרוני:';
$_['entry_test']					 = 'מצב ארגז חול:';
$_['entry_transaction']				 = 'שיטת העסקה:';
$_['entry_pdt_token']				 = 'PDT קוד:<br/><span class="help">השימוש בקוד תשלום העברת נתונים הוא לאבטחה ואמינות נוספים. גלה איך לאפשר את הקוד <a href="https://cms.paypal.com/us/cgi-bin/?&cmd=_render-content&content_ID=developer/howto_html_paymentdatatransfer" alt="">כאן</a></span>';
$_['entry_debug']					 = 'מצב איתור תקלות:<br/><span class="help">פולט מידע נוסף ליומן המערגת.</span>';
$_['entry_total']                    = 'סך הכל:<br /><span class="help">סך כל הסכום שההזמנה חייבת להגיע אליה לפני ששיטת התשלום הופכת לפעילה.</span>';
$_['entry_canceled_reversal_status'] = 'מצב מהופך מבוטל:';
$_['entry_completed_status']         = 'מצב הושלמו:';
$_['entry_denied_status']			 = 'מצב נדחו:';
$_['entry_expired_status']			 = 'מצב פג תוקף:';
$_['entry_failed_status']			 = 'מצב נכשלו:';
$_['entry_pending_status']			 = 'מצב המתנה:';
$_['entry_processed_status']		 = 'מצב עובדו:';
$_['entry_refunded_status']			 = 'מצב החזר כספי:';
$_['entry_reversed_status']			 = 'מצב מהופך:';
$_['entry_voided_status']		     = 'מצב בוטלו:';
$_['entry_geo_zone']				 = 'אזור גיאוגרפי:';
$_['entry_status']					 = 'מצב:';
$_['entry_sort_order']				 = 'סדר המיון:';

// Error
$_['error_permission']				 = 'אזהרה: אין לך הרשאה לשנות את שיטת התשלום!';
$_['error_email']					 = 'נדרש דואר אלקטרוני!';
?>