<?php
// Heading
$_['heading_title']      = 'PayPoint'; 

// Text 
$_['text_payment']       = 'תשלום';
$_['text_success']       = 'הצלחה: פרטי חשבון התשלום שונו!';
$_['text_paypoint']      = '<a onclick="window.open(\'https://www.paypoint.net/partners/opencart\');"><img src="view/image/payment/paypoint.png" alt="PayPoint" title="PayPoint" style="border: 1px solid #EEEEEE;" /></a>';
$_['text_live']          = 'הפקה';
$_['text_successful']    = 'תמיד מצליח';
$_['text_fail']          = 'תמיד נכשל';

// Entry
$_['entry_merchant']     = 'מזהה סוחר:';
$_['entry_test']         = 'מצב בדיקה:';
$_['entry_total']        = 'סך הכל:<br /><span class="help">סך כל הסכום שההזמנה חייבת להגיע אליה לפני ששיטת התשלום הופכת לפעילה.</span>';
$_['entry_order_status'] = 'מצב הזמנה:';
$_['entry_geo_zone']     = 'אזור גיאוגרפי:';
$_['entry_status']       = 'מצב:'; 
$_['entry_sort_order']   = 'סדר המיון:';

// Error
$_['error_permission']   = 'אזהרה: אין לך הרשאה לשנות את שיטת התשלום!';
$_['error_merchant']     = 'נדרש מזהה סוחר!';
?>