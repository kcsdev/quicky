<?php
// Heading
$_['heading_title']      = 'Authorize.Net (AIM)';

// Text 
$_['text_payment']       = 'תשלום';
$_['text_success']       = 'הצלחה: פרטי חשבון התשלום שונו!';
$_['text_test']          = 'בדיקה';
$_['text_live']          = 'אמיתי';
$_['text_authorization'] = 'הרשאה';
$_['text_capture']       = 'לכידה';

// Entry
$_['entry_login']        = 'מזהה כניסה:';
$_['entry_key']          = 'מפתח עסקה:';
$_['entry_hash']         = 'אלגוריתם MD5:';
$_['entry_server']       = 'שרת עסקה:';
$_['entry_mode']         = 'אופן עסקה:';
$_['entry_method']       = 'שיטת עסקה:';
$_['entry_total']        = 'סך הכל:<br /><span class="help">סך כל הסכום שההזמנה חייבת להגיע אליה לפני ששיטת התשלום הופכת לפעילה.</span>';
$_['entry_order_status'] = 'מצב הזמנה:';
$_['entry_geo_zone']     = 'אזור גיאוגרפי:'; 
$_['entry_status']       = 'מצב:';
$_['entry_sort_order']   = 'סדר המיון:';

// Error 
$_['error_permission']   = 'אזהרה: אין לך הרשאה לשנות את שיטת התשלום!';
$_['error_login']        = 'נדרש מזהה כניסה!';
$_['error_key']          = 'נדרש מפתח עסקה!';
?>