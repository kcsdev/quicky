<?php
// Heading					
$_['heading_title']		      = 'Moneybookers';

// Text 					
$_['text_payment']		      = 'תשלום';
$_['text_success']		      = 'הצלחה: פרטי חשבון התשלום שונו!';
$_['text_moneybookers']	      = '<a onclick="window.open(\'https://www.moneybookers.com/partners/?p=OpenCart\');"><img src="view/image/payment/moneybookers.png" alt="Moneybookers" title="Moneybookers" style="border: 1px solid #EEEEEE;" /></a>';
	
// Entry					
$_['entry_email']		      = 'דואר אלקטרוני:';
$_['entry_secret']		      = 'מילה סודית:';
$_['entry_total']             = 'סך הכל:<br /><span class="help">סך כל הסכום שההזמנה חייבת להגיע אליה לפני ששיטת התשלום הופכת לפעילה.</span>';
$_['entry_order_status']      = 'מצב הזמנה:';
$_['entry_pending_status']    = 'מצב המתנה:';
$_['entry_canceled_status']   = 'מצב מבוטל:';
$_['entry_failed_status']     = 'מצב כשל:';
$_['entry_chargeback_status'] = 'מצב החזר:';
$_['entry_geo_zone']          = 'אזור גיאוגרפי:';
$_['entry_status']            = 'מצב:';
$_['entry_sort_order']        = 'סדר המיון:';

// Error					
$_['error_permission']	      = 'אזהרה: אין לך הרשאה לשנות את שיטת התשלום!'; 
$_['error_email']		      = 'נדרש דואר אלקטרוני!';
?>