<?php
// Heading
$_['heading_title']      = '2Checkout';

// Text 
$_['text_payment']       = 'תשלום'; 
$_['text_success']       = 'הצלחה: פרטי חשבון התשלום שונו!';
$_['text_twocheckout']	 = '<a onclick="window.open(\'http://www.2checkout.com\');"><img src="view/image/payment/2checkout.png" alt="2Checkout" title="2Checkout" style="border: 1px solid #EEEEEE;" /></a>';

// Entry
$_['entry_account']      = 'מזהה חשבון:';
$_['entry_secret']       = 'מילה סודית:<br /><span class="help">המילה הסודית כדי לאשר עסקאות, חייב להיות זהה כמוגדר בדף ההגדרות בחשבון הסוחר.</span>';
$_['entry_test']         = 'מצב בדיקה:';
$_['entry_total']        = 'סך הכל:<br /><span class="help">סך כל הסכום שההזמנה חייבת להגיע אליה לפני ששיטת התשלום הופכת לפעילה.</span>';
$_['entry_order_status'] = 'מצב הזמנה:';
$_['entry_geo_zone']     = 'אזור גיאוגרפי:';
$_['entry_status']       = 'מצב:';
$_['entry_sort_order']   = 'סדר המיון:';

// Error
$_['error_permission']   = 'אזהרה: אין לך הרשאה לשנות את שיטת התשלום!';
$_['error_account']      = 'נדרש מזהה חשבון!';
$_['error_secret']       = 'נדרשת מילה סודית!';
?>