<?php
// Heading
$_['heading_title']      = 'PayPal Website Payment Pro (UK)';

// Text 
$_['text_payment']       = 'תשלום';
$_['text_success']       = 'הצלחה: פרטי חשבון התשלום שונו!';
$_['text_pp_pro_uk']     = '<a onclick="window.open(\'https://www.paypal.com/uk/mrb/pal=W9TBB5DTD6QJW\');"><img src="view/image/payment/paypal.png" alt="PayPal Website Payment Pro (UK)" title="PayPal Website Payment Pro (UK)" style="border: 1px solid #EEEEEE;" /></a>';
$_['text_authorization'] = 'הרשאה';
$_['text_sale']          = 'מכירה';

// Entry
$_['entry_vendor']       = 'מוכר:<br /><span class="help">מזהה כניסת הסוחר שלך שיצרת בזמן ההרשמה לחשבון</span>';
$_['entry_user']         = 'משתמש:<br /><span class="help">אם יצרת אחד או יותר משתמשים נוספים בחשבון, הערך הזה הוא המזהה של המשתמש עם ההרשאה לעבד עסקאות. אם, לעומת זאת, לא יצרת משתמשים נוספים בחשבון, למשתמשים יש את אותו ערך כמו למוכרים.</span>';
$_['entry_password']     = 'סיסמה:<br /><span class="help">הסיסמה אשר מכילה 6 עד 32 תווים שהגדרת בזמן ההרשמה לחשבון.</span>';
$_['entry_partner']      = 'שותף:<br /><span class="help">המזהה שניתן לך על ידי המשווק המורשה שרשם אותך ל-Payflow SDK. השתמש ב-PayPalUK אם רכשת את החשבון שלך ישירות מפייפאל.</span>';
$_['entry_test']         = 'מצב בדיקה:<br /><span class="help">השתמש בשער שרת ההפקה או הבדיקה (ארגז חול) בכדי לעבד עסקאות?</span>';
$_['entry_transaction']  = 'שיטת העסקה:';
$_['entry_total']        = 'סך הכל:<br /><span class="help">סך כל הסכום שההזמנה חייבת להגיע אליה לפני ששיטת התשלום הופכת לפעילה.</span>';
$_['entry_order_status'] = 'מצב הזמנה:';
$_['entry_geo_zone']     = 'אזור גיאוגרפי:';
$_['entry_status']       = 'מצב:';
$_['entry_sort_order']   = 'סדר המיון:';

// Error
$_['error_permission']   = 'אזהרה: אין לך הרשאה לשנות את שיטת התשלום!';
$_['error_vendor']       = 'נדרש מוכר!';
$_['error_user']         = 'נדרש משתמש!';
$_['error_password']     = 'נדרשת סיסמה!';
$_['error_partner']      = 'נדרש שותף!';
?>