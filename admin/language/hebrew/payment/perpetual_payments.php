<?php
// Heading
$_['heading_title']      = 'Perpetual Payments';

// Text 
$_['text_payment']       = 'תשלום';
$_['text_success']       = 'הצלחה: פרטי חשבון התשלום שונו!';

// Entry
$_['entry_auth_id']      = 'מזהה הרשאה:';
$_['entry_auth_pass']    = 'סיסמת הרשאה:';
$_['entry_test']         = 'מצב בדיקה:<span class="help">השתמש במודול הזה במצב בדיקה (כן) או במצב פעיל (לא)? אנא בחר</span>';
$_['entry_total']        = 'סך הכל:<br /><span class="help">סך כל הסכום שההזמנה חייבת להגיע אליה לפני ששיטת התשלום הופכת לפעילה.</span>';
$_['entry_order_status'] = 'מצב הזמנה:';
$_['entry_geo_zone']     = 'אזור גיאוגרפי:';
$_['entry_status']       = 'מצב:';
$_['entry_sort_order']   = 'סדר המיון:';

// Error
$_['error_permission']   = 'אזהרה: אין לך הרשאה לשנות את שיטת התשלום!';
$_['error_auth_id']      = 'נדרש מזהה הרשאה!';
$_['error_auth_pass']    = 'נדרשת סיסמת הרשאה!';
?>