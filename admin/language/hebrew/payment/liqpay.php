<?php
// Heading
$_['heading_title']      = 'LIQPAY';

// Text 
$_['text_payment']       = 'תשלום';
$_['text_success']       = 'הצלחה: פרטי חשבון התשלום שונו!';   
$_['text_pay']           = 'LIQPAY';
$_['text_card']          = 'כרטיס אשראי';

// Entry
$_['entry_merchant']     = 'מזהה סוחר:';
$_['entry_signature']    = 'חתימה:';
$_['entry_type']         = 'סוג:';
$_['entry_total']        = 'סך הכל:<br /><span class="help">סך כל הסכום שההזמנה חייבת להגיע אליה לפני ששיטת התשלום הופכת לפעילה.</span>';
$_['entry_order_status'] = 'מצב הזמנה:';
$_['entry_geo_zone']     = 'אזור גיאוגרפי:';
$_['entry_status']       = 'מצב:';
$_['entry_sort_order']   = 'סדר המיון:';

// Error
$_['error_permission']   = 'אזהרה: אין לך הרשאה לשנות את שיטת התשלום!';
$_['error_merchant']     = 'נדרש מזהה סוחר!';
$_['error_signature']    = 'נדרשת חתימה!';
?>