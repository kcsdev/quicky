<?php
// Heading
$_['heading_title']      = 'גוגל סיום ההזמנה';

// Text
$_['text_payment']       = 'תשלום';
$_['text_success']       = 'הצלחה: פרטי חשבון התשלום שונו!';

// Entry
$_['entry_merchant_id']  = 'מזהה סוחר:';
$_['entry_merchant_key'] = 'מפתח סוחר:';
$_['entry_currency']     = 'מטבע:';
$_['entry_test']         = 'מצב בדיקה:';
$_['entry_status']       = 'מצב:';

// Error
$_['error_permission']   = 'אזהרה: אין לך הרשאה לשנות את שיטת התשלום!';
$_['error_merchant_id']  = 'נדרש מזהה סוחר!';
$_['error_merchant_key'] = 'נדרש מפתח סוחר!';
?>