<?php
// Heading
$_['heading_title']      = 'סיום ההזמנה חינם';

// Text
$_['text_payment']       = 'תשלום';
$_['text_success']       = 'הצלחה: פרטי חשבון התשלום שונו!';

// Entry
$_['entry_order_status'] = 'מצב הזמנה:';
$_['entry_status']       = 'מצב:';
$_['entry_sort_order']   = 'סדר המיון:';

// Error
$_['error_permission']   = 'אזהרה: אין לך הרשאה לשנות את שיטת התשלום!';
?>