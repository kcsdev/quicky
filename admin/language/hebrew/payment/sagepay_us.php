<?php
// Heading
$_['heading_title']      = 'SagePay (US)';

// Text 
$_['text_payment']       = 'תשלום'; 
$_['text_success']       = 'הצלחה: פרטי חשבון התשלום שונו!';

// Entry
$_['entry_merchant_id']  = 'מזהה סוחר:';
$_['entry_merchant_key'] = 'מפתח סוחר:';
$_['entry_total']        = 'סך הכל:<br /><span class="help">סך כל הסכום שההזמנה חייבת להגיע אליה לפני ששיטת התשלום הופכת לפעילה.</span>';
$_['entry_order_status'] = 'מצב הזמנה:';
$_['entry_geo_zone']     = 'אזור גיאוגרפי:';
$_['entry_status']       = 'מצב:';
$_['entry_sort_order']   = 'סדר המיון:';

// Error
$_['error_permission']   = 'אזהרה: אין לך הרשאה לשנות את שיטת התשלום!';
$_['error_merchant_id']  = 'נדרש מזהה סוחר!';
$_['error_merchant_key'] = 'נדרש מפתח סוחר!';
?>