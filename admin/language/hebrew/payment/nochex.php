<?php
// Heading
$_['heading_title']      = 'NOCHEX';

// Text 
$_['text_payment']       = 'תשלום';
$_['text_success']       = 'הצלחה: פרטי חשבון התשלום שונו!';
$_['text_nochex']	     = '<a onclick="window.open(\'https://secure.nochex.com/apply/merchant_info.aspx?partner_id=172198798\');"><img src="view/image/payment/nochex.png" alt="NOCHEX" title="NOCHEX" style="border: 1px solid #EEEEEE;" /><br /></a>';
$_['text_seller']        = 'חשבון מוכר / אישי';
$_['text_merchant']      = 'חשבון סוחר';
      
// Entry
$_['entry_email']        = 'דואר אלקטרוני';
$_['entry_account']      = 'סוג חשבון:';
$_['entry_merchant']     = 'מזהה סוחר:';
$_['entry_template']     = 'תבנית פאס:';
$_['entry_test']         = 'בדיקה:';
$_['entry_total']        = 'סך הכל:<br /><span class="help">סך כל הסכום שההזמנה חייבת להגיע אליה לפני ששיטת התשלום הופכת לפעילה.</span>';
$_['entry_order_status'] = 'מצב הזמנה:';
$_['entry_geo_zone']     = 'אזור גיאוגרפי:';
$_['entry_status']       = 'מצב:';
$_['entry_sort_order']   = 'סדר המיון:';

// Error
$_['error_permission']   = 'אזהרה: אין לך הרשאה לשנות את שיטת התשלום!';
$_['error_email']        = 'נדרש דואר אלקטרוני!';
$_['error_merchant']     = 'נדרש מזהה סוחר!';
?>