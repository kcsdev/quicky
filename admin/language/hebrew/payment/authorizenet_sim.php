<?php
// Heading
$_['heading_title']      = 'Authorize.Net (AIM)';

// Text 
$_['text_payment']       = 'תשלום';
$_['text_success']       = 'הצלחה: פרטי חשבון התשלום שונו!';

// Entry
$_['entry_merchant']     = 'מזהה סוחר:';
$_['entry_key']          = 'מפתח עסקה:';
$_['entry_callback']     = 'כתובת אתר תגובת העברה:<br /><span class="help">אנא היכנס והגדר זאת ב <a href="https://secure.authorize.net" target="_blank" class="txtLink">https://secure.authorize.net</a>.</span>';
$_['entry_test']         = 'מצב בדיקה:';
$_['entry_total']        = 'סך הכל:<br /><span class="help">סך כל הסכום שההזמנה חייבת להגיע אליה לפני ששיטת התשלום הופכת לפעילה.</span>';
$_['entry_order_status'] = 'מצב הזמנה:';
$_['entry_geo_zone']     = 'אזור גיאוגרפי:';
$_['entry_status']       = 'מצב:'; 
$_['entry_sort_order']   = 'סדר המיון:';

// Error 
$_['error_permission']   = 'אזהרה: אין לך הרשאה לשנות את שיטת התשלום!';
$_['error_merchant']     = 'נדרש מזהה סוחר!';
$_['error_key']          = 'נדרש מפתח עסקה!';
?>