<?php
// Heading
$_['heading_title']      = 'WorldPay';

// Text 
$_['text_payment']       = 'תשלום'; 
$_['text_success']       = 'הצלחה: פרטי חשבון התשלום שונו!';
$_['text_successful']    = 'מופעל - תמיד מוצלח';
$_['text_declined']      = 'מופעל - תמיד נדחה';
$_['text_off']           = 'כבוי';
      
// Entry
$_['entry_merchant']     = 'מזהה סוחר:';
$_['entry_password']     = 'סיסמת תגובת תשלום:<br /><span class="help">חייב להגדיר את זה בלוח הבקרה בחשבון השירות.</span>';
$_['entry_callback']     = 'כתובת העברת תגובה:<br /><span class="help">חייב להגדיר את זה בלוח הבקרה בחשבון השירות. אתה גם צריך לסמן את "אפשר את תגובת הקונה".</span>';
$_['entry_test']         = 'מצב בדיקה:';
$_['entry_total']        = 'סך הכל:<br /><span class="help">סך כל הסכום שההזמנה חייבת להגיע אליה לפני ששיטת התשלום הופכת לפעילה.</span>';
$_['entry_order_status'] = 'מצב הזמנה:';
$_['entry_geo_zone']     = 'אזור גיאוגרפי:';
$_['entry_status']       = 'מצב:';
$_['entry_sort_order']   = 'סדר המיון:';

// Error
$_['error_permission']   = 'אזהרה: אין לך הרשאה לשנות את שיטת התשלום!';
$_['error_merchant']     = 'נדרש מזהה סוחר!';
$_['error_password']     = 'נדרשת סיסמה!';
?>