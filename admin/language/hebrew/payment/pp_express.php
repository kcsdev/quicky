<?php
// Heading
$_['heading_title']      = 'PayPal Express';

// Text 
$_['text_payment']       = 'תשלום';
$_['text_success']       = 'הצלחה: פרטי חשבון התשלום שונו!';
$_['text_pp_express']    = '<a onclick="window.open(\'https://www.paypal.com/uk/mrb/pal=W9TBB5DTD6QJW\');"><img src="view/image/payment/paypal.png" alt="PayPal" title="PayPal" style="border: 1px solid #EEEEEE;" /></a>';
$_['text_authorization'] = 'הרשאה';
$_['text_sale']          = 'מכירה';

// Entry
$_['entry_username']     = 'שם משתמש איי.פי.איי:';
$_['entry_password']     = 'סיסמת איי.פי.איי:';
$_['entry_signature']    = 'חתימת איי.פי.איי:';
$_['entry_test']         = 'מצב בדיקה:';
$_['entry_method']       = 'שיטת העסקה:';
$_['entry_total']        = 'סך הכל:<br /><span class="help">סך כל הסכום שההזמנה חייבת להגיע אליה לפני ששיטת התשלום הופכת לפעילה.</span>';
$_['entry_order_status'] = 'מצב הזמנה:';
$_['entry_geo_zone']     = 'אזור גיאוגרפי:';
$_['entry_status']       = 'מצב:';
$_['entry_sort_order']   = 'סדר המיון:';

// Error
$_['error_permission']   = 'אזהרה: אין לך הרשאה לשנות את שיטת התשלום!';
$_['error_username']     = 'נדרש שם משתמש איי.פי.איי!';
$_['error_password']     = 'נדרשת סיסמת איי.פי.איי!';
$_['error_signature']    = 'נדרשת חתימת איי.פי.איי!';
?>