<?php
// Heading
$_['heading_title']             = 'Web Payment Software';

// Text 
$_['text_payment']              = 'תשלום';
$_['text_success']              = 'הצלחה: פרטי חשבון התשלום שונו!';
$_['text_web_payment_software'] = '<a onclick="window.open(\'http://www.web-payment-software.com/\');"><img src="view/image/payment/wps-logo.jpg" alt="Web Payment Software" title="Web Payment Software" style="border: 1px solid #EEEEEE;" /></a>';
$_['text_test']                 = 'בדיקה';
$_['text_live']                 = 'הפקה';
$_['text_authorization']        = 'הרשאה';
$_['text_capture']              = 'לכידה';

// Entry
$_['entry_login']               = 'מזהה סוחר:';
$_['entry_key']                 = 'מפתח סוחר:';
$_['entry_mode']                = 'אופן עסקה:';
$_['entry_method']              = 'שיטת העסקה:';
$_['entry_total']               = 'סך הכל:<br /><span class="help">סך כל הסכום שההזמנה חייבת להגיע אליה לפני ששיטת התשלום הופכת לפעילה.</span>';
$_['entry_order_status']        = 'מצב הזמנה:';
$_['entry_geo_zone']            = 'אזור גיאוגרפי:';
$_['entry_status']              = 'מצב:';
$_['entry_sort_order']          = 'סדר המיון:';

// Error 
$_['error_permission']          = 'אזהרה: אין לך הרשאה לשנות את שיטת התשלום!';
$_['error_login']               = 'נדרש מזהה סוחר!';
$_['error_key']                 = 'נדרש מפתח סוחר!';
?>