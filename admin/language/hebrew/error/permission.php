<?php
// Heading
$_['heading_title'] = 'הרשאה נדחתה!'; 

// Text
$_['text_permission'] = 'אין לך הרשאה להכנס לדף זה, אנא פנה אל מנהל המערכת.';
?>
