<?php
// Heading
$_['heading_title']            = 'הגדרות';

// Text
$_['text_success']             = 'הצלחה: ההגדרות שונו!';
$_['text_image_manager']       = 'מנהל התמונות';
$_['text_browse']              = 'עיון בקבצים';
$_['text_clear']               = 'הסרת התמונה';
$_['text_shipping']            = 'כתובת למשלוח';
$_['text_payment']             = 'כתובת לתשלום';

// Column
$_['column_name']              = 'שם החנות';
$_['column_url']	           = 'כתובת אתר החנות';
$_['column_action']            = 'פעולה';

// Entry
$_['entry_url']                = 'כתובת אתר החנות:<br /><span class="help">כלול את כתובת האתר המלאה של החנות שלך. וודא שאתה מוסיף \'/\' בסוף. דוגמה: http://www.yourdomain.com/path/<br /><br /> אל תשתמש בתיקיות בכדי להוסיף חנות חדשה. אתה תמיד צריך להפנות עוד דומיין או תת-דומיין בשרת שלך.</span>';
$_['entry_ssl']                = 'כתובת אתר החנות ב-SSL:<br /><span class="help">כתובת אתר SSL. Make sure to add \'/\' at the end. Example: http://www.yourdomain.com/path/<br /><br />Don\'t use directories to create a new store. You should always point another domain or sub domain to your hosting.</span>';
$_['entry_name']               = 'שם החנות:';
$_['entry_owner']              = 'בעלי החנות:';
$_['entry_address']            = 'כתובת:';
$_['entry_email']              = 'דואר אלקטרוני:';
$_['entry_telephone']          = 'טלפון:';
$_['entry_fax']                = 'פקס:';
$_['entry_title']              = 'כותר:';
$_['entry_meta_description']   = 'תאור במטא תג:';
$_['entry_layout']             = 'פריסת ברירת המחדל:';
$_['entry_template']           = 'תבנית:';
$_['entry_country']            = 'ארץ:';
$_['entry_zone']               = 'אזור / מדינה:';
$_['entry_language']           = 'שפה:';
$_['entry_currency']           = 'מטבע:';
$_['entry_catalog_limit'] 	   = 'הגבלת פריטי ברירת מחדל לכל עמוד (קטלוג):<br /><span class="help">קובע כמה פריטי קטלוג מוצגים בכל עמוד (מוצרים, קטגוריות, וכן הלאה)</span>';
$_['entry_tax']                = 'הצג מחירים כולל מע&quot;מ:';
$_['entry_tax_default']        = 'השתמש בכתובת החנות לחישוב המס:<br /><span class="help">השתמש בכתובת החנות לחישוב מסים אם אף אחד לא מחובר. אתה יכול לבחור להשתמש בכתובת החנות בשביל כתובת המשלוח או התשלום של הלקוחות.</span>';
$_['entry_tax_customer']       = 'השתמש בכתובת הלקוח לחישוב המס:<br /><span class="help">השתמש בכתובת ברירת המחדל של הלקוחות כשהם מתחברים לחישוב מסים. אתה יכול לבחור להשתמש בכתובת ברירת המחדל בשביל כתובת המשלוח או התשלום של הלקוחות.</span>';
$_['entry_customer_group']     = 'קבוצת לקוח:<br /><span class="help">קבוצת לקוח ברירת מחדל.</span>';
$_['entry_customer_price']     = 'הצג מחירים למחוברים בלבד:<br /><span class="help">הצג מחירים רק ללקוחות שהתחברו לחשבונם.</span>';
$_['entry_customer_approval']  = 'אשר לקוחות חדשים:<br /><span class="help">אל תאפשר ללקוח חדש להתחבר, עד שתאשר לו את החשבון.</span>';
$_['entry_guest_checkout']     = 'סיום ההזמנה לאורחים:<br /><span class="help">אפשר ללקוחות לסיים הזמנה מבלי ליצור חשבון. זה לא יהיה אפשרי כשבעגלת הקניות יש מוצר להורדה.</span>';
$_['entry_account']            = 'תנאי החשבון:<br /><span class="help">מאלץ אנשים להסכים לתנאים לפני שיוכלו ליצור חשבון.</span>';
$_['entry_checkout']           = 'תנאי סיום ההזמנה:<br /><span class="help">מאלץ אנשים להסכים לתנאים לפני שלקוח יוכל לסיים את ההזמנה.</span>';
$_['entry_stock_display']      = 'הצגת מלאי:<br /><span class="help">הצג את כמות המלאי בדף המוצר.</span>';
$_['entry_stock_checkout']     = 'סיום ההזמנה כשאזל:<br /><span class="help">אפשר ללקוחות לסיים את ההזמנה אפילו כשהמוצרים שהזמינו לא במלאי.</span>';
$_['entry_order_status']       = 'מצב הזמנה:<br /><span class="help">קבע את ברירת המחדל של מצב הזמנה כשההזמנה מעובדת.</span>';
$_['entry_cart_weight']        = 'Display Weight on Cart Page:';
$_['entry_logo']               = 'לוגו החנות:';
$_['entry_icon']               = 'סמל:<br /><span class="help">הסמל צריך להיות קובץ PNG בגודל 16 על 16 פיקסלים.</span>';
$_['entry_image_category']     = 'גודל תמונת קטגוריה:';
$_['entry_image_thumb']        = 'גודל תמונת מוצר ממוזערת:';
$_['entry_image_popup']        = 'גודל תמונת מוצר בחלון קופץ:';
$_['entry_image_product']      = 'גודל תמונת מוצר ברשימה:';
$_['entry_image_additional']   = 'גודל תמונת מוצר נוסף:';
$_['entry_image_related']      = 'גודל תמונת מוצר נלווה:';
$_['entry_image_compare']      = 'גודל תמונה בהשוואה:';
$_['entry_image_wishlist']     = 'גודל תמונה ברשימת המשאלות:';
$_['entry_image_cart']         = 'גודל תמונה בעגלת הקניות:';
$_['entry_use_ssl']            = 'השתמש ב-SSL:<br /><span class="help">בכדי להשתמש ב-SSL בדוק עם מי שמאחסן לך את האתר האם תעודה כזו מותקנת.</span>';

// Error
$_['error_warning']            = 'אזהרה: נא לבדוק את הטופס בקפידה לטעויות!';
$_['error_permission']         = 'אזהרה: אין לך הרשאה לשנות חנויות!';
$_['error_name']               = 'שם החנות חייב להכיל בין 3 ל-32 תווים!';
$_['error_owner']              = 'בעלי החנות חייבים להכיל בין 3 ל-64 תווים!';
$_['error_address']            = 'כתובת החנות חייבת להכיל בין 10 ל-256 תווים!';
$_['error_email']              = 'כתובת הדואר האלקטרוני אינה נראית חוקית!';
$_['error_telephone']          = 'טלפון חייב להכיל בין 3 ל-32 מספרים!';
$_['error_url']                = 'נדרשת כתובת אתר לחנות!';
$_['error_title']              = 'כותר חייב להכיל בין 3 ל-32 תווים!';
$_['error_limit']       	   = 'נדרשת הגבלת פריטי ברירת מחדל לכל עמוד!';
$_['error_image_thumb']        = 'נדרש גודל תמונת מוצר ממוזערת!';
$_['error_image_popup']        = 'נדרש גודל תמונת מוצר בחלון קופץ!';
$_['error_image_product']      = 'נדרש גודל תמונת מוצר ברשימה!';
$_['error_image_category']     = 'נדרש גודל תמונת קטגוריה ברשימה!';
$_['error_image_manufacturer'] = 'נדרש גודל תמונת יצרן ברשימה!';
$_['error_image_additional']   = 'נדרש גודל תמונת מוצר נוסף!';
$_['error_image_related']      = 'נדרש גודל תמונת מוצר נלווה!';
$_['error_image_compare']      = 'נדרש גודל תמונה בהשוואה!';
$_['error_image_wishlist']     = 'נדרש גודל תמונה ברשימת המשאלות!';
$_['error_image_cart']         = 'נדרש גודל תמונה בעגלת הקניות!';
$_['error_default']            = 'אזהרה: אינך יכול למחוק את חנות ברירת המחדל שלך!';
$_['error_store']              = 'אזהרה: לא ניתן למחוק את החנות הזו מכיוון שהיא מוגדרת כרגע ב-%s הזמנות.';
?>