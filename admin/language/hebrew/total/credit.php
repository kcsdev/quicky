<?php
// Heading
$_['heading_title']    = 'נקודות זיכוי בחנות';

// Text
$_['text_total']       = 'סך כל ההזמנות';
$_['text_success']     = 'הצלחה: סך נקודות הזיכוי בחנות שונה!';

// Entry
$_['entry_status']     = 'מצב:';
$_['entry_sort_order'] = 'סדר המיון:';

// Error
$_['error_permission'] = 'אזהרה: אין לך הרשאה לשנות את סך נקודות הזיכוי בחנות!';
?>