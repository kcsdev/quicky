<?php
// Heading
$_['heading_title']    = 'משלוח';

// Text
$_['text_total']       = 'סך כל ההזמנות';
$_['text_success']     = 'הצלחה: סך המשלוח שונה!';

// Entry
$_['entry_estimator']  = 'הערכת משלוח:';
$_['entry_status']     = 'מצב:';
$_['entry_sort_order'] = 'סדר המיון:';

// Error
$_['error_permission'] = 'אזהרה: אין לך הרשאה לשנות את סך המשלוח!';
?>