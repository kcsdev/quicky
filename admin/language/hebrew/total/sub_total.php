<?php
// Heading
$_['heading_title']    = 'סכום ביניים';

// Text
$_['text_total']       = 'סך כל ההזמנות';
$_['text_success']     = 'הצלחה: סך סכום הביניים שונה!';

// Entry
$_['entry_status']     = 'מצב:';
$_['entry_sort_order'] = 'סדר המיון:';

// Error
$_['error_permission'] = 'אזהרה: אין לך הרשאה לשנות את סך סכום הביניים!';
?>