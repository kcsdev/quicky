<?php
// Heading
$_['heading_title']    = 'שוברי מתנה';

// Text
$_['text_total']       = 'סך כל ההזמנות';
$_['text_success']     = 'הצלחה: סך שוברי המתנה שונה!';

// Entry
$_['entry_status']     = 'מצב:';
$_['entry_sort_order'] = 'סדר המיון:';

// Error
$_['error_permission'] = 'אזהרה: אין לך הרשאה לשנות את סך שוברי המתנה!';
?>