<?php
// Heading
$_['heading_title']    = 'סך הכל';

// Text
$_['text_total']       = 'סך כל ההזמנות';
$_['text_success']     = 'הצלחה: סך הסך הכל שונה!';

// Entry
$_['entry_status']     = 'מצב:';
$_['entry_sort_order'] = 'סדר המיון:';

// Error
$_['error_permission'] = 'אזהרה: אין לך הרשאה לשנות את סך הסך הכל!';
?>