<?php
// Heading
$_['heading_title']    = 'עמלת הזמנה נמוכה';

// Text
$_['text_total']       = 'סך כל ההזמנות';
$_['text_success']     = 'הצלחה: סך עמלת הזמנה נמוכה שונה!';

// Entry
$_['entry_total']      = 'סך כל ההזמנה:';
$_['entry_fee']        = 'עמלה:';
$_['entry_tax_class']  = 'סיווג מס:';
$_['entry_status']     = 'מצב:';
$_['entry_sort_order'] = 'סדר המיון:';

// Error
$_['error_permission'] = 'אזהרה: אין לך הרשאה לשנות את סך עמלת הזמנה נמוכה!';
?>