<?php
// Heading
$_['heading_title']      = 'נושאי השוברים';

// Text
$_['text_success']       = 'הצלחה: נושאי השוברים שונו!';
$_['text_image_manager'] = 'מנהל התמונות';
$_['text_browse']        = 'עיון בקבצים';
$_['text_clear']         = 'הסרת התמונה';

// Column
$_['column_name']        = 'שם נושא השובר';
$_['column_action']      = 'פעולה';

// Entry
$_['entry_name']         = 'שם נושא השובר:';
$_['entry_description']  = 'תאור נושא השובר:';
$_['entry_image']        = 'תמונה:';

// Error
$_['error_permission']  = 'אזהרה: אין לך הרשאה לשנות נושאי שוברים!';
$_['error_name']        = 'שם נושא השובר חייב להכיל בין 3 ל-32 תווים!';
$_['error_image']       = 'נדרשת תמונה!';
$_['error_voucher']     = 'אזהרה: לא ניתן למחוק נושא שובר זה מכיוון שהוא מוגדר כרגע ב-%s שוברים!';
?>