<?php
// Heading
$_['heading_title']       = 'מוצרים מוחזרים';

// Text
$_['text_opened']         = 'נפתח';
$_['text_unopened']       = 'לא נפתח';
$_['text_success']        = 'הצלחה: מוחזרים שונו!';
$_['text_wait']           = 'אנא המתן!';

// Text
$_['text_return_id']      = 'מזהה החזרה:';
$_['text_order_id']       = 'מזהה הזמנה:';
$_['text_date_ordered']   = 'תאריך הזמנה:';
$_['text_customer']       = 'לקוח:';
$_['text_email']          = 'דואר אלקטרוני:';
$_['text_telephone']      = 'טלפון:';
$_['text_return_status']  = 'מצב החזרה:';
$_['text_comment']        = 'הערה:';
$_['text_date_added']     = 'תאריך הוספה:';
$_['text_date_modified']  = 'תאריך שינוי:';

// Column
$_['column_return_id']     = 'מזהה החזרה';
$_['column_order_id']      = 'מזהה הזמנה';
$_['column_customer']      = 'לקוח';
$_['column_quantity']      = 'כמות';
$_['column_status']        = 'מצב';
$_['column_date_added']    = 'תאריך הוספה';
$_['column_date_modified'] = 'תאריך שינוי';
$_['column_product']       = 'מוצר';
$_['column_model']         = 'דגם';
$_['column_reason']        = 'סיבת החזרה';
$_['column_opened']        = 'נפתח';
$_['column_notify']        = 'הלקוח קיבל הודעה';
$_['column_comment']       = 'הערה';
$_['column_action']        = 'פעולה';

// Entry
$_['entry_customer']      = 'לקוח:';
$_['entry_order_id']      = 'מזהה הזמנה:';
$_['entry_date_ordered']  = 'תאריך הזמנה:';
$_['entry_firstname']     = 'שם פרטי:';
$_['entry_lastname']      = 'שם משפחה:';
$_['entry_email']         = 'דואר אלקטרוני:';
$_['entry_telephone']     = 'טלפון:';
$_['entry_product']       = 'מוצר:';
$_['entry_model']         = 'דגם:';
$_['entry_quantity']      = 'כמות:';
$_['entry_reason']        = 'סיבת החזרה:';
$_['entry_opened']        = 'נפתח:';
$_['entry_comment']       = 'הערה:';
$_['entry_return_status'] = 'מצב החזרה:';
$_['entry_notify']        = 'ליידע לקוח:';
$_['entry_action']        = 'פעולת החזרה:';

// Error
$_['error_warning']       = 'אזהרה: נא לבדוק את הטופס בקפידה לטעויות!';
$_['error_permission']    = 'אזהרה: אין לך הרשאה לשנות החזרות!';
$_['error_firstname']     = 'שם פרטי חייב להכיל בין 1 ל-32 תווים!';
$_['error_lastname']      = 'שם משפחה חייב להכיל בין 1 ל-32 תווים!';
$_['error_email']         = 'כתובת הדואר האלקטרוני אינה נראית חוקית!';
$_['error_telephone']     = 'טלפון חייב להכיל בין 3 ל-32 מספרים!';
$_['error_password']      = 'הסיסמה חייבת להכיל בין 4 ל-20 תווים!';
$_['error_confirm']       = 'הסיסמה ואישור הסיסמה אינם תואמים!';
$_['error_address_1']     = 'כתובת 1 חייבת להכיל בין 3 ל-128 תווים!';
$_['error_city']          = 'עיר חייבת להכיל בין 2 ל-128 תווים!';
$_['error_postcode']      = 'מיקוד חייב להכיל בין 2 ל-10 תווים לארץ הזאת!';
$_['error_country']       = 'אנא בחרו ארץ!';
$_['error_zone']          = 'אנא בחרו אזור / מדינה!';
?>