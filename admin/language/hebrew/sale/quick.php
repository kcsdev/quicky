<?php
$_['heading_title']       = 'הזמנה מהירה';
$_['button_save'] = 'שמור';
$_['entery_back'] = 'ביטול';
$_['text_number'] = 'מספר הזמנות';
$_['text_name'] = 'שם המזמין';
$_['text_phone'] = 'מספר טלפון';
$_['text_city'] = 'עיר';
$_['text_street'] = 'רחוב';
$_['text_house_number'] = 'מספר בית';
$_['text_entrence'] = 'כניסה';
$_['text_appartment_number'] = 'מספר דירה';
$_['text_floor'] = 'קומה';
$_['text_restaurant'] = 'מסעדה';
$_['text_time_make'] = 'זמן לאיסוף';
$_['text_add_row'] = 'הוסף שורה';