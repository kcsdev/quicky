<?php

//heading title
$_['heading_title']     = 'מרכז הזמנות';


//entery
$_['entery_mail'] = 'אימייל';
$_['entery_phone'] = 'טלפון';
$_['entery_price'] = 'סה״כ';
$_['entery_restaurent'] = 'מסעדה';
$_['entery_action'] = 'פעולה';
$_['entery_edit'] = 'עריכה';
$_['entery_remove'] = 'מחיקה';
$_['entery_name'] = 'שם מלא:';
$_['entery_phone'] = 'טלפון:';
$_['entery_company'] = 'חברה:';
$_['entery_companyPhone'] = 'טלפון נוסף:';
$_['entery_city'] = 'עיר:';
$_['entery_street'] = 'רחוב';
$_['entery_house'] = 'מספר בית:';
$_['entery_entrence'] = 'כניסה:';
$_['entery_appartment'] = 'מספר דירה:';
$_['entery_floor'] = 'קומה:';
$_['entery_comments'] = 'הערות:';
$_['entery_price'] = 'מחיר:';
$_['entery_date'] = 'תאריך:';
$_['entery_product'] = 'מוצר';
$_['entery_quantity'] = 'כמות';
$_['entery_total'] = 'סה״כ';
$_['entery_priceOne'] = 'מחיר ליחידה';
$_['entery_back'] = 'ביטול';
$_['entery_delivery'] = 'דמי משלוח';
$_['entery_finelPrice'] = 'מחיר סופי';
$_['entery_special'] = 'תוספות להזמנה:';
$_['entery_time'] = 'זמן הגעה:'
?>