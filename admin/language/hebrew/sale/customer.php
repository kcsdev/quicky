<?php
// Heading
$_['heading_title']         = 'לקוח';

// Text
$_['text_success']          = 'הצלחה: לקוחות שונו!';
$_['text_default']          = 'ברירת מחדל';
$_['text_approved']         = 'אושרו %s חשבונות!';
$_['text_wait']             = 'אנא המתן!';
$_['text_balance']          = 'מאזן:';

// Column
$_['column_name']           = 'שם לקוח';
$_['column_email']          = 'דואר אלקטרוני';
$_['column_customer_group'] = 'קבוצת לקוח';
$_['column_status']         = 'מצב'; 
$_['column_login']          = 'הכנס לחנות';
$_['column_approved']       = 'מאושר';
$_['column_date_added']     = 'תאריך הוספה';
$_['column_description']    = 'תאור';
$_['column_amount']         = 'סכום';
$_['column_points']         = 'נקודות מצטברות';
$_['column_ip']             = 'כתובת איי.פי';
$_['column_total']          = 'סך הכל חשבונות';
$_['column_action']         = 'פעולה';

// Entry
$_['entry_firstname']       = 'שם פרטי:';
$_['entry_lastname']        = 'שם משפחה:';
$_['entry_email']           = 'דואר אלקטרוני:';
$_['entry_telephone']       = 'טלפון:';
$_['entry_fax']             = 'פקס:';
$_['entry_newsletter']      = 'ידיעון:';
$_['entry_customer_group']  = 'קבוצת לקוח:';
$_['entry_status']          = 'מצב:';
$_['entry_password']        = 'סיסמה:';
$_['entry_confirm']         = 'אשר:';
$_['entry_company']         = 'חברה:';
$_['entry_address_1']       = 'כתובת 1:';
$_['entry_address_2']       = 'כתובת 2:';
$_['entry_city']            = 'עיר:';
$_['entry_postcode']        = 'מיקוד:';
$_['entry_country']         = 'ארץ:';
$_['entry_zone']            = 'אזור / מדינה:';
$_['entry_default']         = 'כתובת ברירת מחדל:';
$_['entry_amount']          = 'סכום:';
$_['entry_points']          = 'נקודות מצטברות:';
$_['entry_description']     = 'תאור:';


// new for homeals
$_['entry_sex']                 = 'מין:';
$_['entry_birth_day']           = 'תאריך לידה:';
$_['entry_sp']                  = 'התמחות:';
$_['entry_image']       = 'תמונת פרופיל:';
$_['entry_user_images']         = 'תמונות:';
$_['entry_sms_aproved']         = 'אשרור נייד:';

$_['tab_image']                 = 'תמונות';



// Error
$_['error_warning']         = 'אזהרה: נא לבדוק את הטופס בקפידה לטעויות!';
$_['error_permission']      = 'אזהרה: אין לך הרשאה לשנות לקוחות!';
$_['error_exists']          = 'אזהרה: כתובת הדואר האלקטרוני כבר רשומה במערכת!';
$_['error_firstname']       = 'שם פרטי חייב להכיל בין 1 ל-32 תווים!';
$_['error_lastname']        = 'שם משפחה חייב להכיל בין 1 ל 32 תווים!';
$_['error_email']           = 'כתובת הדואר האלקטרוני אינה נראית חוקית!';
$_['error_telephone']       = 'טלפון חייב להכיל בין 3 ל-32 מספרים!';
$_['error_password']        = 'הסיסמה חייבת להכיל בין 4 ל-20 תווים!';
$_['error_confirm']         = 'הסיסמה ואישור הסיסמה אינם תואמים!';
$_['error_address_1']       = 'כתובת 1 חייבת להכיל בין 3 ל-128 תווים!';
$_['error_city']            = 'עיר חייבת להכיל בין 2 ל-128 תווים!';
$_['error_postcode']        = 'מיקוד חייב להכיל בין 2 ל-10 תווים לארץ הזאת!';
$_['error_country']         = 'אנא בחרו ארץ!';
$_['error_zone']            = 'אנא בחרו אזור / מדינה!';
?>