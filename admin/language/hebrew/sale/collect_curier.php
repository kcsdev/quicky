<?php

$_['heading_title']       = 'גביית שליח';
$_['text_date'] = 'תאריך';

$_['text_names'] = 'שם';
$_['text_details'] = 'פרטי המשמרת';

//details
$_['text_rest_name'] = 'מסעדה';
$_['text_cash'] = 'מזומן';
$_['text_customer_name'] = 'שם הלקוח';
$_['text_address'] = 'כתובת';
$_['text_money'] = 'סה״כ תשלום';
$_['text_curier_name'] = 'שם השליח';
$_['text_extra_cash'] = 'מזומן עודף';
$_['text_claim'] = 'מבצע הגבייה';
$_['text_cash_orders'] = 'סה״כ מזומן עבור הזמנות';
$_['text_full'] = 'הוצאות/דלק';
$_['text_give'] = 'כסף להחזיר';
$_['text_take'] = 'כסף שנלקח מהשליח';
$_['text_diffrent'] = 'הפרש';
$_['text_storm'] = 'חליפת סערה';