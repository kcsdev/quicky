<?php
$_['heading_title']       = 'מסך ניהול';

//tabs text
$_['all_text']       = 'כל ההזמנות';
$_['run_text']       = 'הזמנות רצות';
$_['ready_text']       = 'הזמנות מוכנות';
$_['staffing_text']       = 'ציוות';
$_['process_text']       = 'בתהליך';
$_['validate_text']       = 'פקסים לוודא';
$_['gived_text']       = 'סופקו';


//table text
$_['time_text']       = 'שעת הזמנה';
$_['customer_text']       = 'שם הלקוח';
$_['fax_text']       = 'פקס';
$_['brend_text']       = 'סוג';
$_['restaurant_text']       = 'מסעדה';
$_['address_text']       = 'כתובת';
$_['courier_text']       = 'שליח';
$_['timebar_text']       = 'בר-זמן';
$_['status_text']       = 'סטאטוס';
$_['money_text']       = 'סכום';
$_['id_text']       = 'זהות';
$_['fax_process'] = 'בתהליך שליחה';
$_['fax_again'] = 'סבב שליחה נוסף';
$_['fax_fail'] = 'שליחה נכשלה';
$_['fax_validate'] = 'לוודא';
$_['fax_send'] = 'פקס נשלח';
$_['delivery_notcollected'] = 'טרם נאסף';
$_['delivery_collected'] = 'נאסף';
$_['delivery_provided'] = 'סופק';
$_['delivery_restaurant'] = 'עיכוב במסעדה';
$_['delivery_customer'] = 'איחור ללקוח';
$_['text_tableheader'] = 'ניהול שליחים';
// side bar
$_['text_going'] = 'עומדים להתפנות';
$_['text_available'] = 'שליחים פנויים';
$_['text_links'] = 'קישורים';
$_['text_watch'] = 'שעון נוכחות';
$_['text_rest'] = 'טבלת מסעדות';
$_['text_manager'] = 'מסך בקרה';
$_['text_quick'] = 'הזמנה מהירה';
$_['text_collect_curier'] = 'גביית שליח';