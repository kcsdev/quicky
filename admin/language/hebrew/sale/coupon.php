<?php
// Heading  
$_['heading_title']       = 'קופון';

// Text
$_['text_success']        = 'הצלחה: הקופונים שונו!';
$_['text_percent']        = 'אחוזים';
$_['text_amount']         = 'סכום קבוע';

// Column
$_['column_name']         = 'שם קופון';
$_['column_code']         = 'קוד';
$_['column_discount']     = 'הנחה';
$_['column_date_start']   = 'תאריך התחלה';
$_['column_date_end']     = 'תאריך סיום';
$_['column_status']       = 'מצב';
$_['column_order_id']     = 'מזהה הזמנה';
$_['column_customer']     = 'לקוח';
$_['column_amount']       = 'סכום';
$_['column_date_added']   = 'תאריך הוספה';
$_['column_action']       = 'פעולה';

// Entry
$_['entry_name']          = 'שם קופון:';
$_['entry_code']          = 'קוד:<br /><span class="help">הקוד שהלקוח מזין בכדי לקבל את ההנחה</span>';
$_['entry_type']          = 'סוג:<br /><span class="help">אחוזים או סכום קבוע</span>';
$_['entry_discount']      = 'הנחה:';
$_['entry_logged']        = 'כניסת לקוח:<br /><span class="help">הלקוח חייב להיות מחובר בכדי להשתמש בקופון.</span>';
$_['entry_shipping']      = 'משלוח חינם:';
$_['entry_total']         = 'סך הכל הסכום:<br /><span class="help">הסכום הכולל שחייב להגיע אליו לפני שהקופון תקף.</span>';
$_['entry_category']      = 'קטגוריה:<br /><span class="help">בחר את כל המוצרים תחת הקטגוריה הנבחרת.</span>';
$_['entry_product']       = 'מוצרים:<br /><span class="help">בחר מוצרים מסוימים שהקופון יחול עליהם. אל תבחר אף מוצר בכדי שהקופון יחול על כל עגלת הקניות.</span>';
$_['entry_date_start']    = 'תאריך התחלה:';
$_['entry_date_end']      = 'תאריך סיום:';
$_['entry_uses_total']    = 'שימושים לכל קופון:<br /><span class="help">המספר המרבי לשימוש בקופון עבור כל לקוח. השאר ריק בשביל ללא הגבלה</span>';
$_['entry_uses_customer'] = 'שימושים לכל לקוח:<br /><span class="help">המספר המרבי לשימוש בקופון עבור לקוח בודד. השאר ריק בשביל ללא הגבלה</span>';
$_['entry_status']        = 'מצב:';

// Error
$_['error_permission']    = 'אזהרה: אין לך הרשאה לשנות קופונים!!';
$_['error_name']          = 'שם קופון חייב להכיל בין 3 ל-128 תווים!';
$_['error_code']          = 'קוד חייב להכיל בין 3 ל-10 תווים!';
?>