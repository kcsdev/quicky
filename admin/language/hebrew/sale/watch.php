<?php
$_['heading_title']       = 'ניהול משמרת';


$_['heading_title']       = 'ניהול משמרת';
$_['text_insert']       = 'הוספה';
$_['text_edit']       = 'עריכה';
$_['text_delete']       = 'מחיקה';
$_['entery_name']       = 'שם השליח';
$_['button_save']       = 'שמירה';
$_['entery_back']       = 'ביטול';
$_['text_curier']       = 'שליח';
$_['text_mirs']       = 'מספר מירס';
$_['text_motorcycle']       = 'מספר אופנוע';
$_['text_change']       = 'סכום עודף';
$_['text_storm']       = 'חליפת סערה';
$_['text_enter']       = 'פעילות';
$_['text_active']       = 'במשמרת';
$_['text_break']       = 'הפסקה';