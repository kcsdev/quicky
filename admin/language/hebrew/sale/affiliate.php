<?php
// Heading
$_['heading_title']             = 'שותפים';

// Text
$_['text_success']              = 'הצלחה: השותפים שונו!';
$_['text_approved']             = 'אישרת %s חשבונות!';
$_['text_wait']                 = 'אנא המתן!';
$_['text_balance']              = 'מאזן:';
$_['text_cheque']               = 'המחאה';
$_['text_paypal']               = 'פייפאל';
$_['text_bank']                 = 'העברה בנקאית';

// Column
$_['column_name']               = 'שם שותף';
$_['column_email']              = 'דואר אלקטרוני';
$_['column_code']               = 'קוד מעקב';
$_['column_balance']            = 'מאזן';
$_['column_status']             = 'מצב';
$_['column_approved']           = 'מאושר';
$_['column_date_added']         = 'תאריך הוספה';
$_['column_description']        = 'תאור';
$_['column_amount']             = 'סכום';
$_['column_action']             = 'פעולה';

// Entry
$_['entry_firstname']           = 'שם פרטי:';
$_['entry_lastname']            = 'שם משפחה:';
$_['entry_email']               = 'דואר אלקטרוני:';
$_['entry_telephone']           = 'טלפון:';
$_['entry_fax']                 = 'פקס:';
$_['entry_status']              = 'מצב:';
$_['entry_password']            = 'סיסמה:';
$_['entry_confirm']             = 'לאשר:';
$_['entry_company']             = 'חברה:';
$_['entry_address_1']           = 'כתובת 1:';
$_['entry_address_2']           = 'כתובת 2:';
$_['entry_city']                = 'עיר:';
$_['entry_postcode']            = 'מיקוד:';
$_['entry_country']             = 'ארץ:';
$_['entry_zone']                = 'אזור / מדינה:';
$_['entry_code']                = 'קוד מעקב:<span class="help">קוד המעקב אשר ישמש למעקב אחר הפניות.</span>';
$_['entry_commission']          = 'עמלה (%):<span class="help">האחוזים שהשותף מקבל על כל הזמנה.</span>';
$_['entry_tax']                 = 'מזהה מס:';
$_['entry_payment']             = 'שיטת תשלום:';
$_['entry_cheque']              = 'שם המוטב בהמחאה:';
$_['entry_paypal']              = 'דואר אלקטרוני בחשבון פייפאל:';
$_['entry_bank_name']           = 'שם הבנק:';
$_['entry_bank_branch_number']  = 'מספר ABA/BSB - מספר סניף:';
$_['entry_bank_swift_code']     = 'קוד סוויפט:';
$_['entry_bank_account_name']   = 'שם החשבון:';
$_['entry_bank_account_number'] = 'מספר החשבון:';
$_['entry_amount']              = 'סכום:';
$_['entry_description']         = 'תאור:';

// Error
$_['error_permission']          = 'אזהרה: אין לך הרשאה לשנות שותפים!';
$_['error_exists']              = 'אזהרה: כתובת הדואר האלקטרוני כבר רשום במערכת!';
$_['error_firstname']           = 'שם פרטי חייב להכיל בין 1 ל-32 תווים!';
$_['error_lastname']            = 'שם משפחה חייב להכיל בין 1 ל-32 תווים!';
$_['error_email']               = 'כתובת הדואר האלקטרוני אינה נראית חוקית!';
$_['error_telephone']           = 'טלפון חייב להכיל בין 3 ל-32 מספרים!';
$_['error_password']            = 'הסיסמה חייבת להכיל בין 4 ל-20 תווים!';
$_['error_confirm']             = 'הסיסמה ואישור הסיסמה אינם תואמים!';
$_['error_address_1']           = 'כתובת 1 חייבת להכיל בין 3 ל-128 תווים!';
$_['error_city']                = 'עיר חייבת להכיל בין 2 ל-128 תווים!';
$_['error_postcode']            = 'מיקוד חייב להכיל בין 2 ל-10 תווים לארץ הזאת!';
$_['error_country']             = 'אנא בחרו ארץ!';
$_['error_zone']                = 'אנא בחרו אזור / מדינה!';
$_['error_code']                = 'נדרש קוד מעקב!';
?>