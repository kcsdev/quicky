<?php
// Heading  
$_['heading_title']     = 'שובר מתנה';

// Text
$_['text_send']         = 'שלח';
$_['text_success']      = 'הצלחה: השוברים שונו!';
$_['text_sent']         = 'הצלחה: דואר אלקטרוני עם שובר מתנה נשלח!';
$_['text_wait']         = 'אנא המתן!';

// Column
$_['column_name']       = 'שם השובר';
$_['column_code']       = 'קוד';
$_['column_from']       = 'מאת';
$_['column_to']         = 'אל';
$_['column_amount']     = 'סכום';
$_['column_theme']      = 'נושא';
$_['column_date_added'] = 'תאריך הוספה';
$_['column_status']     = 'מצב';
$_['column_order_id']   = 'מזהה הזמנה';
$_['column_customer']   = 'לקוח';
$_['column_date_added'] = 'תאריך הוספה';
$_['column_action']     = 'פעולה';

// Entry
$_['entry_code']        = 'קוד:<br /><span class="help">הקוד שהלקוח מזין בכדי להשתמש בשובר.</span>';
$_['entry_from_name']   = 'מאת - שם:';
$_['entry_from_email']  = 'מאת - דואר אלקטרוני:';
$_['entry_to_name']     = 'אל - שם:';
$_['entry_to_email']    = 'אל - דואר אלקטרוני:';
$_['entry_message']     = 'הודעה:';
$_['entry_amount']      = 'סכום:';
$_['entry_theme']       = 'נושא:';
$_['entry_status']      = 'מצב:';

// Error
$_['error_permission']  = 'אזהרה: אין לך הרשאה לשנות שוברים!';
$_['error_code']        = 'הקוד חייב להכיל בין 3 ל-10 תווים!';
$_['error_to_name']     = 'שם הנמען חייב להכיל בין 1 ל-64 תווים!';
$_['error_from_name']   = 'שמך חייב להכיל בין 1 ל-64 תווים!';
$_['error_email']       = 'כתובת הדואר האלקטרוני אינה נראית חוקית!';
$_['error_amount']      = 'הסכום חייב להיות גדול או שווה ל-1!';
?>