<?php

$_['heading_title']       = 'ניהול עובדים';

$_['text_delete'] = 'מחיקה';
$_['text_edit'] = 'עריכה';
$_['text_save'] = 'שמירה';
$_['text_add'] = 'הוספה';
$_['entery_name'] = 'שם';
$_['entery_back'] = 'ביטול';
$_['entery_firstname'] = 'שם פרטי';
$_['entery_lastname'] = 'שם משפחה';
$_['entery_phone'] = 'טלפון נייד';
$_['entery_homephone'] = 'טלפון בבית';
$_['entery_id'] = 'תעודת זהות';
$_['entery_address'] = 'כתובת';
$_['entery_job'] = 'תפקיד';
$_['entery_status'] = 'סטאטוס';
$_['entery_curier'] = 'שליח';
$_['entery_phone_job'] = 'טלפן';
$_['entery_activate'] = 'פעיל';