<?php  
// Heading
$_['heading_title']        = 'דואר';

// Text
$_['text_success']         = 'ההודעה נשלחה בהצלחה!';
$_['text_default']         = 'ברירת מחדל';
$_['text_newsletter']      = 'כל מנויי הידיעון';
$_['text_customer_all']    = 'כל הלקוחות';
$_['text_customer_group']  = 'קבוצת לקוח';
$_['text_customer']        = 'לקוחות';
$_['text_affiliate_all']   = 'כל השותפים';
$_['text_affiliate']       = 'שותפים';
$_['text_product']         = 'מוצרים';

// Entry
$_['entry_store']          = 'מאת:';
$_['entry_to']             = 'אל:';
$_['entry_customer_group'] = 'קבוצת לקוח:';
$_['entry_customer']       = 'לקוח:<br /><span class="help">השלמה אוטומטית</span>';
$_['entry_affiliate']      = 'שותף:<br /><span class="help">השלמה אוטומטית</span>';
$_['entry_product']        = 'מוצרים:<br /><span class="help">שלח רק ללקוחות אשר הזמינו מוצרים שברשימה. (השלמה אוטומטית)</span>';
$_['entry_subject']        = 'נושא:';
$_['entry_message']        = 'הודעה:';

// Error
$_['error_permission']     = 'אזהרה: אין לך הרשאה לשלוח דואר אלקטרוני!';
$_['error_subject']        = 'נדרש נושא בדואר האלקטרוני!';
$_['error_message']        = 'נדרשת הודעה בדואר האלקטרוני!';
?>