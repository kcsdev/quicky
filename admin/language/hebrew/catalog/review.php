<?php
// Heading
$_['heading_title']       = 'סקירות';

// Text
$_['text_success']      = 'הצלחה: הסקירות שונו!';

// Column
$_['column_product']    = 'מוצר';
$_['column_author']     = 'מחבר';
$_['column_rating']     = 'דירוג';
$_['column_status']     = 'מצב';
$_['column_date_added'] = 'תאריך הוספה';
$_['column_action']     = 'פעולה';

// Entry
$_['entry_product']     = 'מוצר:<br/><span class="help">(השלמה אוטומטית)</span>';
$_['entry_author']      = 'מחבר:';
$_['entry_rating']      = 'דירוג:';
$_['entry_status']      = 'מצב:';
$_['entry_text']        = 'תמליל:';
$_['entry_good']        = 'טוב';
$_['entry_bad']         = 'לא טוב';

// Error
$_['error_permission']  = 'אזהרה: אין לך הרשאה לשנות סקירות!';
$_['error_product']     = 'נדרש מוצר!';
$_['error_author']      = 'מחבר חייב להכיל בין 3 ל-64 תווים!';
$_['error_text']        = 'תמליל סקירה חייב להכיל לפחות 1 תווים!';
$_['error_rating']      = 'נדרש דירוג סקירה!';
?>
