<?php
// Heading
$_['heading_title']      = 'יצרן';

// Text
$_['text_success']       = 'הצלחה: היצרנים שונו!';
$_['text_default']       = 'ברירת מחדל';
$_['text_image_manager'] = 'מנהל התמונות';
$_['text_browse']        = 'עיון בקבצים';
$_['text_clear']         = 'הסרת תמונה';
$_['text_percent']       = 'אחוזים';
$_['text_amount']        = 'כמות קבועה';

// Column
$_['column_name']        = 'שם יצרן';
$_['column_sort_order']  = 'סדר המיון';
$_['column_action']      = 'פעולה';

// Entry
$_['entry_name']         = 'שם יצרן:';
$_['entry_store']        = 'חנויות:';
$_['entry_keyword']      = 'מילות מפתח - אופטימיזציה למנועי חיפוש:';
$_['entry_image']        = 'תמונה:';
$_['entry_sort_order']   = 'סדר המיון:';
$_['entry_type']         = 'סוג:';

// Error
$_['error_permission']   = 'אזהרה: אין לך הרשאה לשנות יצרנים!';
$_['error_name']         = 'שם יצרן חייב להכיל בין 3 ל-64 תווים!';
$_['error_product']      = 'אזהרה: אי אפשר למחוק יצרן זה מכיוון שהוא מוגדר ב %s מוצרים!';
?>