<?php
// Heading
$_['heading_title']          = 'קטגוריה';

// Text
$_['text_success']           = 'הצלחה: הקטגוריות שונו!';
$_['text_default']           = 'ברירת מחדל';
$_['text_image_manager']     = 'מנהל התמונות';
$_['text_browse']            = 'עיון בקבצים';
$_['text_clear']             = 'הסרת התמונה';

// Column
$_['column_name']            = 'שם קטגוריה';
$_['column_sort_order']      = 'סדר המיון';
$_['column_action']          = 'פעולה';

// Entry
$_['entry_name']             = 'שם קטגוריה:';
$_['entry_meta_keyword'] 	 = 'מילות מפתח במטא תג:';
$_['entry_meta_description'] = 'תיאור במתא תג:';
$_['entry_description']      = 'תיאור:';
$_['entry_parent']           = 'תיקיית אם:';
$_['entry_store']            = 'חנויות:';
$_['entry_keyword']          = 'מילות מפתח - אופטימיזציה למנועי חיפוש:<br/><span class="help">חייב להכיל ייחודי גלובלי.</span>';
$_['entry_image']            = 'תמונה:';
$_['entry_top']              = 'למעלה:<br/><span class="help">הצג בתפריט העליון. משפיע רק על קטגוריות אם.</span>';
$_['entry_column']           = 'עמודות:<br/><span class="help">מספר עמודות ב-3 הקטגוריות התחתונות. משפיע רק על קטגוריות אם.</span>';
$_['entry_sort_order']       = 'סדר המיון:';
$_['entry_status']           = 'מצב:';
$_['entry_layout']           = 'אכיפת פריסה:';

// Error 
$_['error_warning']          = 'אזהרה: אנא בדוק את הטופס היטב לשגיאות!';
$_['error_permission']       = 'אזהרה: אין לך הרשאה לשנות קטגוריות!';
$_['error_name']             = 'שם קטגוריה חייב להכיל בין 2 ל-32 תווים!';
?>