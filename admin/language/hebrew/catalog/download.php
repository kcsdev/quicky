<?php
// Heading
$_['heading_title']    = 'הורדות';

// Text
$_['text_success']     = 'הצלחה: ההורדות שונו!';

// Column
$_['column_name']      = 'שם הורדה';
$_['column_remaining'] = 'סך הכל הורדות מורשות';
$_['column_action']    = 'פעולה';

// Entry
$_['entry_name']       = 'שם הורדה:';
$_['entry_filename']   = 'שם קובץ:';
$_['entry_remaining']  = 'סך הכל הורדות מורשות:';
$_['entry_update']     = 'העבר ללקוחות קודמים:<br /><span class="help">סמן את זה כדי לעדכן גם גרסאות שנרכשו בעבר.</span>';

// Error
$_['error_permission'] = 'אזהרה: אין לך הרשאה לשנות הורדות!';
$_['error_name']       = 'שם הורדה חייב להכיל בין 3 ל-64 תווים!';
$_['error_filename']   = 'שם הקובץ חייב להכיל בין 3 ל-128 תווים!';
$_['error_filetype']   = 'סוג הקובץ לא מורשה!';
$_['error_product']    = 'אזהרה: אי אפשר למחוק הורדה זו מכיוון שהיא מוגדרת ב %s מוצרים!';
?>