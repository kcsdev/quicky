<?php
// Heading
$_['heading_title']          = 'תכונות';

// Text
$_['text_success']           = 'הצלחה: התכונות שונו!';

// Column
$_['column_name']            = 'שם תכונה';
$_['column_attribute_group'] = 'קבוצת תכונה';
$_['column_sort_order']      = 'סדר המיון';
$_['column_action']          = 'פעולה';

// Entry
$_['entry_name']            = 'שם תכונה:';
$_['entry_attribute_group'] = 'קבוצת תכונה:';
$_['entry_sort_order']      = 'סדר המיון:';

// Error
$_['error_permission']      = 'אזהרה: אין לך הרשאה לשנות תכונות!';
$_['error_name']            = 'שם תכונה חייב להכיל בין 3 ל-64 תווים!';
$_['error_product']         = 'אזהרה: אי אפשר למחוק תכונה זו מכיוון שכרגע היא מוגדרת ב %s מוצרים!';
?>