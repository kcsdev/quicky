<?php
// Text
$_['text_approve_subject']      = '%s - חשבון השותפים שלך הופעל!';
$_['text_approve_welcome']      = 'ברוכים הבאים ותודה שנרשמת ב-%s!';
$_['text_approve_login']        = 'החשבון שלך נוצר עכשיו ואתה יכול להיכנס אליו באמצעות כתובת הדוא"ל והסיסמה שלך על ידי ביקור באתר האינטרנט שלנו או בכתובת האתר הבאה:';
$_['text_approve_services']     = 'לאחר הכניסה, תוכל ליצור קודי מעקב, לעקוב אחר תשלומי עמלה ולערוך את פרטי החשבון שלך.';
$_['text_approve_thanks']       = 'תודה,';
$_['text_transaction_subject']  = '%s - עמלת שותפים';
$_['text_transaction_received'] = 'קיבלת %s עמלה!';
$_['text_transaction_total']    = 'הסכום הכולל של העמלה שלך הוא כעת %s.';
?>