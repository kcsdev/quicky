<?php
// Heading 
$_['heading_title']    = 'פיד מוצרים';

// Text
$_['text_install']     = 'התקן';
$_['text_uninstall']   = 'הסר';

// Column
$_['column_name']      = 'שם פיד מוצר';
$_['column_status']    = 'מצב';
$_['column_action']    = 'פעולה';

// Error
$_['error_permission'] = 'אזהרה: אין לך הרשאה לשנות פידים!';
?>