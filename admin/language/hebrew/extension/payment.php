<?php
// Heading
$_['heading_title']     = 'תשלום';

// Text
$_['text_install']      = 'התקן';
$_['text_uninstall']    = 'הסר';

// Column
$_['column_name']       = 'שיטת תשלום';
$_['column_status']     = 'מצב';
$_['column_sort_order'] = 'סדר המיון';
$_['column_action']     = 'פעולה';

// Error
$_['error_permission']  = 'אזהרה: אין לך הרשאה לשנות תשלומים!';
?>