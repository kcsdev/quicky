<?php
// Heading 
$_['heading_title']    = 'ניהול שליחים';

// Text
$_['text_install']     = 'התקן';
$_['text_uninstall']   = 'הסר';

// Column
$_['column_name']      = 'שם מודל';
$_['column_status']    = 'מצב';
$_['column_action']    = 'פעולה';

// Error
$_['error_permission'] = 'אזהרה: אין לך הרשאה לשנות בשליחים!';
?>