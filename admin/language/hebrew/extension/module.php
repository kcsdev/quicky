<?php
// Heading
$_['heading_title']    = 'מודולים';

// Text
$_['text_install']     = 'התקן';
$_['text_uninstall']   = 'הסר';

// Column
$_['column_name']      = 'שם מודול';
$_['column_action']    = 'פעולה';

// Error
$_['error_permission'] = 'אזהרה: אין לך הרשאה לשנות מודולים!';
?>