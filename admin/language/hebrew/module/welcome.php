<?php
// Heading
$_['heading_title']       = 'ברוכים הבאים';

// Text
$_['text_module']         = 'מודולים';
$_['text_success']        = 'הצלחה: מודול ברוכים הבאים שונה!';
$_['text_content_top']    = 'מעל לתוכן';
$_['text_content_bottom'] = 'מתחת לתוכן';
$_['text_column_left']    = 'עמודה משמאל - הפוך כאשר מוגדר מימין לשמאל';
$_['text_column_right']   = 'עמודה מימין - הפוך כאשר מוגדר מימין לשמאל';

// Entry
$_['entry_description']   = 'הודעת ברוכים הבאים:';
$_['entry_layout']        = 'פריסה:';
$_['entry_position']      = 'מיקום:';
$_['entry_status']        = 'מצב:';
$_['entry_sort_order']    = 'סדר המיון:';

// Error
$_['error_permission']    = 'אזהרה: אין לך הרשאה לשנות מודול ברוכים הבאים!';
?>