<?php
// Heading
$_['heading_title']       = 'מצגת באנרים';

// Text
$_['text_module']         = 'מודולים';
$_['text_success']        = 'הצלחה: מודול מצגת באנרים שונה!';
$_['text_content_top']    = 'מעל לתוכן';
$_['text_content_bottom'] = 'מתחת לתוכן';
$_['text_column_left']    = 'עמודה משמאל - הפוך כאשר מוגדר מימין לשמאל';
$_['text_column_right']   = 'עמודה מימין - הפוך כאשר מוגדר מימין לשמאל';

// Entry
$_['entry_banner']        = 'באנר:';
$_['entry_dimension']     = 'מידות רוחב וגובה:';
$_['entry_layout']        = 'פריסה:';
$_['entry_position']      = 'מיקום:';
$_['entry_status']        = 'מצב:';
$_['entry_sort_order']    = 'סדר המיון:';

// Error
$_['error_permission']    = 'אזהרה: אין לך הרשאה לשנות מודול מצגת באנרים!';
$_['error_dimension']     = 'נדרשות מידות רוחב וגובה!';
?>