<?php
// Heading
$_['heading_title']       = 'חדשים';

// Text
$_['text_module']         = 'מודולים';
$_['text_success']        = 'הצלחה: מודול חדשים שונה!';
$_['text_content_top']    = 'מעל לתוכן';
$_['text_content_bottom'] = 'מתחת לתוכן';
$_['text_column_left']    = 'עמודה משמאל - הפוך כאשר מוגדר מימין לשמאל';
$_['text_column_right']   = 'עמודה מימין - הפוך כאשר מוגדר מימין לשמאל';

// Entry
$_['entry_limit']         = 'הגבלה:'; 
$_['entry_image']         = 'רוחב וגובה תמונה:';
$_['entry_layout']        = 'פריסה:';
$_['entry_position']      = 'מיקום:';
$_['entry_status']        = 'מצב:';
$_['entry_sort_order']    = 'סדר המיון:';

// Error
$_['error_permission']    = 'אזהרה: אין לך הרשאה לשנות מודול חדשים!';
$_['error_image']         = 'נדרשות מידות רוחב וגובה תמונה!';
?>