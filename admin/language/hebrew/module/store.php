<?php
// Heading
$_['heading_title']       = 'חנות';

// Text
$_['text_module']         = 'מודולים';
$_['text_success']        = 'הצלחה: מודול חנות שונה!';
$_['text_content_top']    = 'מעל לתוכן';
$_['text_content_bottom'] = 'מתחת לתוכן';
$_['text_column_left']    = 'עמודה משמאל - הפוך כאשר מוגדר מימין לשמאל';
$_['text_column_right']   = 'עמודה מימין - הפוך כאשר מוגדר מימין לשמאל';

// Entry
$_['entry_admin']         = 'משתמשי מנהל בלבד:';
$_['entry_layout']        = 'פריסה:';
$_['entry_position']      = 'מיקום:';
$_['entry_status']        = 'מצב:';
$_['entry_sort_order']    = 'סדר המיון:';

// Error
$_['error_permission']    = 'אזהרה: אין לך הרשאה לשנות מודול חנות!';
?>