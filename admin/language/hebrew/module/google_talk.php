<?php
// Heading
$_['heading_title']       = 'Google Talk - גוגל טוק';

// Text
$_['text_module']         = 'מודולים';
$_['text_success']        = 'הצלחה: מודול גוגל טוק שונה!';
$_['text_content_top']    = 'מעל לתוכן';
$_['text_content_bottom'] = 'מתחת לתוכן';
$_['text_column_left']    = 'עמודה משמאל - הפוך כאשר מוגדר מימין לשמאל';
$_['text_column_right']   = 'עמודה מימין - הפוך כאשר מוגדר מימין לשמאל';

// Entry
$_['entry_code']          = 'קוד גוגל טוק:<br /><span class="help">לך ל-<a onclick="window.open(\'http://www.google.com/talk/service/badge/New\');"><u>צור תג גוגל טוק</u></a> והעתק הדבק את הקוד שנוצר לתוך תיבת הטקסט.</span>';
$_['entry_layout']        = 'פריסה:';
$_['entry_position']      = 'מיקום:';
$_['entry_status']        = 'מצב:';
$_['entry_sort_order']    = 'סדר המיון:';

// Error
$_['error_permission']    = 'אזהרה: אין לך הרשאה לשנות מודול גוגל טוק!';
$_['error_code']          = 'נדרש קוד!';
?>