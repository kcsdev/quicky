<?php
// Heading 
$_['heading_title']    = 'זמינות לפי מספר שליחים';

// Text
$_['text_install']     = 'התקן';
$_['text_uninstall']   = 'הסר';

// Column
$_['column_name']      = 'שליחים';
$_['column_status']    = 'מצב';
$_['column_action']    = 'פעולה';

// Error
$_['error_permission'] = 'אין לך הרשאה שלנות שליחים';
?>