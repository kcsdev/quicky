<?php
// Heading
$_['heading_title']    = 'Google Sitemap - מפת אתר גוגל';

// Text 
$_['text_feed']        = 'פידים מוצר';
$_['text_success']     = 'הצלחה: שינית פיד מפת אתר גוגל!';

// Entry
$_['entry_status']     = 'מצב:';
$_['entry_data_feed']  = 'כתובת אתר של הפיד:';

// Error
$_['error_permission'] = 'אזהרה: אין לך הרשאה לשנות פיד מפת אתר גוגל!';
?>