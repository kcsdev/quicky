<?php
// Heading
$_['heading_title']    = 'Google Base - גוגל בייס';

// Text   
$_['text_feed']        = 'פידים מוצר';
$_['text_success']     = 'הצלחה: שינית פיד גוגל בייס!';

// Entry
$_['entry_status']     = 'מצב:';
$_['entry_data_feed']  = 'כתובת אתר של הפיד:';

// Error
$_['error_permission'] = 'אזהרה: אין לך הרשאה לשנות פיד גוגל בייס!';
?>