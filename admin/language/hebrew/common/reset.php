<?php
// header
$_['heading_title']  = 'אפס את סיסמתך';

// Text
$_['text_reset']     = 'אפס את סיסמתך!';
$_['text_password']  = 'הזן את הסיסמה החדשה שברצונך להשתמש.';
$_['text_success']   = 'הצלחה: הסיסמה שלך עודכנה בהצלחה!';

// Entry
$_['entry_password'] = 'סיסמה:';
$_['entry_confirm']  = 'אישור סיסמה:';

// Error
$_['error_password'] = 'סיסמה חייבת להכיל בין 5 ל-20 תווים!';
$_['error_confirm']  = 'סיסמה ואישור סיסמה לא תואמים!';
?>