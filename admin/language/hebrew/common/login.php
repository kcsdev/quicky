<?php
// header
$_['heading_title']  = 'מנהל';

// Text
$_['text_heading']   = 'מנהל';
$_['text_login']     = 'אנא הזן את פרטי הכניסה שלך.';
$_['text_forgotten'] = 'שכחתי את הסיסמה';

// Entry
$_['entry_username'] = 'שם משתמש:';
$_['entry_password'] = 'סיסמה:';

// Button
$_['button_login']   = 'כניסה';

// Error
$_['error_login']    = 'אין התאמה לשם משתמש ו/או הסיסמה.';
$_['error_token']    = 'קוד ההצפנה לכניסה זו לא תקף, אנא התחבר שוב.';
?>