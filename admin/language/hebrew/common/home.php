<?php
// Heading
$_['heading_title']                 = 'לוח ראשי';

// Text
$_['text_overview']                 = 'סקירה כללית';
$_['text_statistics']               = 'סטטיסטיקה';
$_['text_latest_10_orders']         = 'עשר הזמנות אחרונות';
$_['text_total_sale']               = 'סך הכל מכירות:';
$_['text_total_sale_year']          = 'סך הכל מכירות השנה:';
$_['text_total_order']              = 'סך הכל הזמנות:';
$_['text_total_customer']           = 'מספר הלקוחות:';
$_['text_total_customer_approval']  = 'לקוחות הממתינים להתר:';
$_['text_total_review_approval']    = 'סקירות הממתינות להתר:';
$_['text_total_affiliate']          = 'מספר השותפים:';
$_['text_total_affiliate_approval'] = 'שותפים הממתינים להתר:';
$_['text_day']                      = 'היום';
$_['text_week']                     = 'השבוע';
$_['text_month']                    = 'החודש';
$_['text_year']                     = 'השנה';
$_['text_order']                    = 'סך הכל הזמנות';
$_['text_customer']                 = 'סך הכל לקוחות';

// Column 
$_['column_order']                  = 'מזהה הזמנה';
$_['column_customer']               = 'לקוח';
$_['column_status']                 = 'מצב';
$_['column_date_added']             = 'תאריך הוספה';
$_['column_total']                  = 'סך הכל';
$_['column_firstname']              = 'שם פרטי';
$_['column_lastname']               = 'שם משפחה';
$_['column_action']                 = 'פעולה';

// Entry
$_['entry_range']                   = 'בחר טווח:';

// Error
$_['error_install']                 = 'אזהרה: תיקיית ההתקנה עדיין קיימת!';
$_['error_image']                   = 'אזהרה: תיקיית image %s לא ניתנת לכתיבה!';
$_['error_image_cache']             = 'אזהרה: תיקיית Image cache %s לא ניתנת לכתיבה!';
$_['error_cache']                   = 'אזהרה: תיקיית Cache %s לא ניתנת לכתיבה!';
$_['error_download']                = 'אזהרה: תיקיית Download %s לא ניתנת לכתיבה!';
$_['error_logs']                    = 'אזהרה: תיקיית Log %s לא ניתנת לכתיבה!';
?>