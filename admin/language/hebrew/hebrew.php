<?php
// Locale
$_['code']                    = 'he';
$_['direction']               = 'rtl';
$_['date_format_short']       = 'd.m.Y';
$_['date_format_long']        = 'l dS F Y';
$_['time_format']             = 'h:i:s A';
$_['decimal_point']           = '.';
$_['thousand_point']          = ',';

$_['column_cook']             = 'בשלן';

// Text
$_['text_yes']                = 'כן-Yes';
$_['text_no']                 = 'לא-No';

$_['text_enabled']            = 'מאופשר';
$_['text_disabled']           = 'לא מאופשר';
$_['text_none']               = ' --- אף אחד --- ';
$_['text_select']             = ' --- לבחור בבקשה --- ';
$_['text_select_all']         = 'לבחור הכל';
$_['text_unselect_all']       = 'לבחור בכלום';
$_['text_all_zones']          = 'כל האיזורים';
$_['text_default']            = ' <b>(ברירת מחדל)</b>';
$_['text_close']              = 'סגירה';
$_['text_pagination']         = 'מציג {start} עד {end} מתוך {total} ({pages} דפים)';
$_['text_no_results']         = 'אין תוצאות!';
$_['text_separator']          = ' &gt; ';
$_['text_edit']               = 'עריכה';
$_['text_view']               = 'הצגה';
$_['text_home']               = 'בית';

// homeals
$_['text_male']               = 'זכר';
$_['text_female']             = 'נקבה';
$_['text_browse']             = 'דפדף';
$_['text_clear']              = 'נקה';
$_['text_qunt']               = 'כמות: ';

// Button
$_['button_insert']           = 'הוספה';
$_['button_delete']           = 'מחיקה';
$_['button_save']             = 'שמירה';
$_['button_cancel']           = 'ביטול';
$_['button_clear']            = 'ניקוי יומן';
$_['button_close']            = 'סגירה';
$_['button_filter']           = 'סינון';
$_['button_send']             = 'שליחה';
$_['button_edit']             = 'עריכה';
$_['button_copy']             = 'העתקה';
$_['button_back']             = 'חזרה';
$_['button_remove']           = 'הסרה';
$_['button_backup']           = 'גיבוי';
$_['button_restore']          = 'שחזור';
$_['button_upload']           = 'העלאה';
$_['button_invoice']          = 'הדפסת חשבונית';
$_['button_add_address']      = 'הוספת כתובת';
$_['button_add_attribute']    = 'הוספת תכונה';
$_['button_add_banner']       = 'הוספת באנר';
$_['button_add_product']      = 'הוספת מוצר';
$_['button_add_option']       = 'הוספת אפשרות';
$_['button_add_option_value'] = 'הוספת ערך לאפשרות';
$_['button_add_discount']     = 'הוספת הנחה';
$_['button_add_special']      = 'הוספת מיוחד';
$_['button_add_image']        = 'הוספת תמונה';
$_['button_add_geo_zone']     = 'הוספת אזור גיאוגרפי';
$_['button_add_history']      = 'הוספת הסטוריה';
$_['button_add_transaction']  = 'הוספת עסקה';
$_['button_add_total']        = 'הוספת סה&quot;כ';
$_['button_add_reward']       = 'הוספת נקודות מצטברות';
$_['button_add_route']        = 'הוספת נתיב';
$_['button_add_rule' ]        = 'הוספת כלל';
$_['button_add_module']       = 'הוספת מודול';
$_['button_add_link']         = 'הוספת לינק';
$_['button_approve']          = 'אישור';
$_['button_reset']            = 'אפוס';

// coupon link only
$_['entry_link_only']            = 'שימוש דרך קישור בלבד:';


// Tab
$_['tab_address']             = 'כתובת';
$_['tab_admin']               = 'מנהל';
$_['tab_attribute']           = 'תכונה';
$_['tab_coupon_history']      = 'היסטורית קופונים';
$_['tab_data']                = 'נתונים';
$_['tab_design']              = 'עיצוב';
$_['tab_discount']            = 'הנחה';
$_['tab_general']             = 'כללי';
$_['tab_meal_avilabilaty'] = 'זמינות ארוחה'; // homeals
$_['tab_ip']                  = 'כתובות אי-פי';
$_['tab_links']               = 'קישורים';
$_['tab_image']               = 'תמונה';
$_['tab_option']              = 'אפשרויות';
$_['tab_server']              = 'שרת';
$_['tab_store']               = 'חנות';
$_['tab_special']             = 'מיוחד';
$_['tab_local']               = 'לוקליזציה';
$_['tab_mail']                = 'דואר';
$_['tab_module']              = 'מודול';
$_['tab_order']               = 'פרטי ההזמנה';
$_['tab_order_history']       = 'היסטוריית הזמנות';
$_['tab_payment']             = 'פרטי תשלום';
$_['tab_product']             = 'מוצרים';
$_['tab_return']              = 'פרטי החזרה';
$_['tab_return_history']      = 'הסטוריית החזרות';
$_['tab_reward']              = 'נקודות מצטברות';
$_['tab_shipping']            = 'פרטי משלוח';
$_['tab_total']               = 'סכומים';
$_['tab_transaction']         = 'עסקאות';
$_['tab_voucher_history']     = 'היסטוריית שוברים';

// Error
$_['error_upload_1']          = 'אזהרה: הקובץ שהועלה עובר את ההנחיה upload_max_filesize בתוך php.ini!';
$_['error_upload_2']          = 'אזהרה: הקובץ שהועלה עובר את ההנחיה MAX_FILE_SIZE שצוין בטופס ה-HTML!';
$_['error_upload_3']          = 'אזהרה: הקובץ הועלה חלקית!';
$_['error_upload_4']          = 'אזהרה: לא הועלה אף קובץ!';
$_['error_upload_6']          = 'אזהרה: חסרה תיקייה זמנית!';
$_['error_upload_7']          = 'אזהרה: לא הצליח לכתוב קובץ לדיסק!';
$_['error_upload_8']          = 'אזהרה: העלאת הקובץ הופסקה ע&quot;י תוספת!';
$_['error_upload_999']        = 'אזהרה: אין קוד שגיאה זמין!';

$_['entry_force_coupon']        = 'כפה קופון:';
$_['entry_coupon_customer']        = 'הוסף קופון באופן ישיר ללקוח:';

?>