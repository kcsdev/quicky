<?php
// Heading
$_['heading_title']      = 'באנרים';

// Text
$_['text_success']       = 'הצלחה: באנרים שונו!';
$_['text_default']       = 'ברירת מחדל';
$_['text_image_manager'] = 'מנהל התמונות';
$_['text_browse']        = 'עיון בקבצים';
$_['text_clear']         = 'הסרת תמונה';

// Column
$_['column_name']        = 'שם באנר';
$_['column_status']      = 'מצב';
$_['column_action']      = 'פעולה';

// Entry
$_['entry_name']         = 'שם באנר:';
$_['entry_title']        = 'כותר:';
$_['entry_link']         = 'קישור:';
$_['entry_image']        = 'תמונה:';
$_['entry_status']       = 'מצב:';

// Error
$_['error_permission']   = 'אזהרה: אין לך הרשאה לשנות באנרים!';
$_['error_name']         = 'שם באנר חייב להכיל בין 3 ל-64 תווים!';
$_['error_title']        = 'כותר באנר חייב להכיל בין 2 ל-64 תווים!';
$_['error_default']      = 'אזהרה: לא ניתן למחוק פריסה זו מכיוון שהיא מוגדרת כרגע כפריסת ברירת המחדל של החנות!';
$_['error_product']      = 'אזהרה: לא ניתן למחוק פריסה זו מכיוון שהיא מוגדרת כרגע ב-%s מוצרים!';
$_['error_category']     = 'אזהרה: לא ניתן למחוק פריסה זו מכיוון שהיא מוגדרת כרגע ב-%s קטגוריות!';
$_['error_information']  = 'אזהרה: לא ניתן למחוק פריסה זו מכיוון שהיא מוגדרת כרגע ב-%s דפי מידע!';
?>