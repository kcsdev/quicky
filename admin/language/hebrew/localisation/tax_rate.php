<?php
// Heading
$_['heading_title']        = 'שיעורי מס';

// Text
$_['text_percent']         = 'אחוזים';
$_['text_amount']          = 'סכום קבוע';
$_['text_success']         = 'הצלחה: שיעורי מס שונו!';

// Column
$_['column_name']          = 'שם מס';
$_['column_rate']          = 'שעור מס';
$_['column_type']          = 'סוג';
$_['column_geo_zone']      = 'אזור גיאוגרפי';
$_['column_date_added']    = 'תאריך הוספה';
$_['column_date_modified'] = 'תאריך שינוי';
$_['column_action']        = 'פעולה';

// Entry
$_['entry_name']           = 'שם מס:';
$_['entry_rate']           = 'שעור מס:';
$_['entry_type']           = 'סוג:';
$_['entry_customer_group'] = 'קבוצת לקוח:';
$_['entry_geo_zone']       = 'אזור גיאוגרפי:';

// Error
$_['error_permission']     = 'אזהרה: אין לך הרשאה לשנות שיעורי מס!';
$_['error_tax_rule']       = 'אזהרה: אי אפשר למחוק שעור מס זה מכיוון שהוא כרגע מוגדר ב-%s סיווגי מס!';
$_['error_name']           = 'שם מס חייב להכיל בין 3 ל-32 תווים!';
$_['error_rate']           = 'נדרש שעור מס!';
?>