<?php
// Heading
$_['heading_title']      = 'אזורים גיאוגרפיים';

// Text
$_['text_success']       = 'הצלחה: אזורים גיאוגרפיים שונו!';

// Column
$_['column_name']        = 'שם אזור גיאוגרפי';
$_['column_description'] = 'תאור';
$_['column_action']      = 'פעולה';

// Entry
$_['entry_name']         = 'שם אזור גיאוגרפי:';
$_['entry_description']  = 'תאור:';
$_['entry_country']      = 'ארץ:';
$_['entry_zone']         = 'אזור:';

// Error
$_['error_permission']   = 'אזהרה: אין לך הרשאה לשנות אזורים גיאוגרפיים!';
$_['error_name']         = 'שם אזור גיאוגרפי חייב להכיל בין 3 ל-32 תווים!';
$_['error_description']  = 'תאור חייב להכיל בין 3 ל-255 תווים!';
$_['error_tax_rate']     = 'אזהרה: אי אפשר למחוק אזור גיאוגרפי זה מכיוון שהוא כרגע מוגדר באחד או יותר שיעורי מס!';
?>