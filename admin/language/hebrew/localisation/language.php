<?php
// Heading
$_['heading_title']     = 'שפות';  

// Text
$_['text_success']      = 'הצלחה: שפות שונו!'; 

// Column
$_['column_name']       = 'שם שפה';
$_['column_code']       = 'קוד';
$_['column_sort_order'] = 'סדר המיון';
$_['column_action']     = 'פעולה';

// Entry
$_['entry_name']        = 'שם שפה:';
$_['entry_code']        = 'קוד:<br /><span class="help">דוגמה: he אל תשנה אם זו שפת ברירת המחדל שלך.</span>';
$_['entry_locale']      = 'לוקליזציה:<br /><span class="help">דוגמה: he_IL,UTF-8,hebrew</span>';
$_['entry_image']       = 'תמונה:<br /><span class="help">דוגמה: il.png</span>';
$_['entry_directory']   = 'תיקייה:<br /><span class="help">שם תיקיית שפה (תלוי רישיות)</span>';
$_['entry_filename']    = 'שם קובץ:<br /><span class="help">שם קובץ שפה ראשי ללא סיומת</span>';
$_['entry_status']      = 'מצב:<br /><span class="help">הסתר/הצג ברשימה הנפתחת</span>';
$_['entry_sort_order']  = 'סדר המיון:';

// Error
$_['error_permission']  = 'הזהרה: אין לך הרשאה לשנות שפות!';
$_['error_name']        = 'שם שפה חייב להכיל בין 3 ל-32 תווים!';
$_['error_code']        = 'קוד שפה חייב להכיל לפחות 2 תווים!';
$_['error_locale']      = 'לוקליזציה נדרשת!';
$_['error_image']       = 'שם תמונה חייב להכיל בין 3 ל-64 תווים!';
$_['error_directory']   = 'תיקייה נדרשת!';
$_['error_filename']    = 'שם קובץ חייב להכיל בין 3 ל-64 תווים!';
$_['error_default']     = 'אזהרה: אי אפשר למחוק שפה זו מכיוון שהיא כרגע מוגדרת כשפת ברירת המחדל של החנות!';
$_['error_admin']       = 'אזהרה: אי אפשר למחוק שפה זו מכיוון שהיא כרגע מוגדרת כשפת הניהול!';
$_['error_store']       = 'אזהרה: אי אפשר למחוק שפה זו מכיוון שהיא כרגע מוגדרת ב-%s חנויות!';
$_['error_order']       = 'אזהרה: אי אפשר למחוק שפה זו מכיוון שהיא כרגע מוגדרת ב-%s הזמנות';
?>