<?php
// Heading
$_['heading_title']    = 'סיבת החזרה';

// Text
$_['text_success']     = 'הצלחה: סיבות החזרה שונו!';

// Column
$_['column_name']      = 'שם סיבת החזרה';
$_['column_action']    = 'פעולה';

// Entry
$_['entry_name']       = 'שם סיבות החזרה:';

// Error
$_['error_permission'] = 'אזהרה: אין לך הרשאה לשנות סיבות החזרה!';
$_['error_name']       = 'שם סיבות החזרה חייב להכיל בין 3 ל-32 תווים!';
$_['error_return']     = 'אזהרה: אי אפשר למחוק סיבת החזרה זו מכיוון שהיא כרגע מוגדרת ב-%s מוצרים שהוחזרו!';
?>