<?php
// Heading
$_['heading_title']     = 'סיווג מס';

// Text
$_['text_shipping']     = 'כתובת משלוח';
$_['text_payment']      = 'כתובת תשלום';
$_['text_store']        = 'כתובת חנות';
$_['text_success']      = 'הצלחה: סיווגי מס שונו!';

// Column
$_['column_title']      = 'כותר סיווג מס';
$_['column_action']     = 'פעולה';

// Entry
$_['entry_title']       = 'כותר סיווג מס:';
$_['entry_description'] = 'תאור:';
$_['entry_rate']        = 'שעור מס:';
$_['entry_based']       = 'בהתבסס על:';
$_['entry_geo_zone']    = 'אזור גיאוגרפי:';
$_['entry_priority']    = 'עדיפות:';

// Error
$_['error_permission']  = 'אזהרה: אין לך הרשאה לשנות סיווגי מס!';
$_['error_title']       = 'כותר סיווג מס חייב להכיל בין 3 ל-32 תווים!';
$_['error_description'] = 'תאור חייב להכיל בין 3 ל-255 תווים!';
$_['error_product']     = 'אזהרה: אי אפשר למחוק סיווג מס זה מכיוון שהוא כרגע מוגדר ב-%s מוצרים!';
?>