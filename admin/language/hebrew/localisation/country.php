<?php
// Heading
$_['heading_title']           = 'ארץ';

// Text
$_['text_success']            = 'הצלחה: הארצות שונו!';

// Column
$_['column_name']             = 'שם ארץ';
$_['column_iso_code_2']       = 'ISO Code (2)';
$_['column_iso_code_3']       = 'ISO Code (3)';
$_['column_action']           = 'פעולה';

// Entry
$_['entry_name']              = 'שם ארץ:';
$_['entry_iso_code_2']        = 'ISO Code (2):';
$_['entry_iso_code_3']        = 'ISO Code (3):';
$_['entry_address_format']    = 'פורמט כתובת:<br /><span class="help">
שם פרטי = {firstname}<br />
שם משפחה = {lastname}<br />
חברה = {company}<br />
כתובת 1 = {address_1}<br />
כתובת 2 = {address_2}<br />
עיר = {city}<br />
מיקוד = {postcode}<br />
אזור = {zone}<br />
קוד אזור = {zone_code}<br />
ארץ = {country}</span>';
$_['entry_postcode_required'] = 'דרוש מיקוד:';
$_['entry_status']            = 'מצב:';

// Error
$_['error_permission']        = 'אזהרה: אין לך הרשאה לשנות ארצות!';
$_['error_name']              = 'שם ארץ חייב להכיל בין 3 ל-128 תווים!';
$_['error_default']           = 'אזהרה: אי אפשר למחוק ארץ זו מכיוון שהיא כרגע מוגדרת בחנות ברירת המחדל!';
$_['error_store']             = 'אזהרה: אי אפשר למחוק ארץ זו מכיוון שהיא כרגע מוגדרת ב-%s חנויות!';
$_['error_address']           = 'אזהרה: אי אפשר למחוק ארץ זו מכיוון שהיא כרגע מוגדרת ב-%s פנקסי כתובות!';
$_['error_affiliate']         = 'אזהרה: אי אפשר למחוק ארץ זו מכיוון שהיא כרגע מוגדרת ב-%s שותפים!';
$_['error_zone']              = 'אזהרה: אי אפשר למחוק ארץ זו מכיוון שהיא כרגע מוגדרת ב-%s אזורים!';
$_['error_zone_to_geo_zone']  = 'אזהרה: אי אפשר למחוק ארץ זו מכיוון שהיא כרגע מוגדרת ב-%s אזורים לאזורים גיאוגרפיים!';
?>