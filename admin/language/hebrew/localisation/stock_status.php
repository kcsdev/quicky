<?php
// Heading
$_['heading_title']    = 'מצב מלאי';

// Text
$_['text_success']     = 'הצלחה: מצבי מלאי שונו!';

// Column
$_['column_name']      = 'שם מצב מלאי';
$_['column_action']    = 'פעולה';

// Entry
$_['entry_name']       = 'שם מצב מלאי:';

// Error
$_['error_permission'] = 'אזהרה: אין לך הרשאה לשנות מצבי מלאי!';
$_['error_name']       = 'שם מצבי מלאי חייב להכיל בין 3 ל-32 תווים!';
$_['error_product']    = 'אזהרה: אי אפשר למחוק מצב הזמנה זה מכיוון שהוא כרגע מוגדר ב-%s מוצרים!';
?>