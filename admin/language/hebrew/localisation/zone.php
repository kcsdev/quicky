<?php
// Heading
$_['heading_title']          = 'אזורים';

// Text
$_['text_success']           = 'הצלחה: אזורים שונו!';

// Column
$_['column_name']            = 'שם אזור';
$_['column_code']            = 'קוד אזור';
$_['column_country']         = 'ארץ';
$_['column_action']          = 'פעולה';

// Entry
$_['entry_status']           = 'מצב אזור:';
$_['entry_name']             = 'שם אזור:';
$_['entry_code']             = 'קוד אזור:';
$_['entry_country']          = 'ארץ:';

// Error
$_['error_permission']       = 'אזהרה: אין לך הרשאה לשנות אזורים!';
$_['error_name']             = 'שם אזור חייב להכיל בין 3 ל-128 תווים!';
$_['error_default']          = 'אזהרה: אי אפשר למחוק אזור זה מכיוון שהוא מוגדר כרגע כאזור ברירת המחדל של החנות!';
$_['error_store']            = 'אזהרה: אי אפשר למחוק אזור זה מכיוון שהוא מוגדר כרגע ב-%s חנויות!';
$_['error_address']          = 'Warning: This zone cannot be deleted as it is currently assigned to %s address book entries!';
$_['error_affiliate']        = 'אזהרה: אי אפשר למחוק אזור זה מכיוון שהוא מוגדר כרגע ב-%s שותפים!';
$_['error_zone_to_geo_zone'] = 'אזהרה: אי אפשר למחוק אזור זה מכיוון שהוא מוגדר כרגע ב-%s אזורים לאזורים גיאוגרפיים!';
?>