<?php
// Heading
$_['heading_title']        = 'מטבע';  

// Text
$_['text_success']         = 'הצלחה: מטבעות שונו!';

// Column
$_['column_title']         = 'כותר מטבע';
$_['column_code']          = 'קוד'; 
$_['column_value']         = 'ערך';
$_['column_date_modified'] = 'העדכון האחרון';
$_['column_action']        = 'פעולה';

// Entry
$_['entry_title']          = 'כותר מטבע:';
$_['entry_code']           = 'קוד:<br /><span class="help">אל תשנה את זה אם זה מטבע ברירת המחדל שלך.</span>';
$_['entry_value']          = 'ערך:<br /><span class="help">הגדר כ-1.00000 אם זה מטבע ברירת המחדל שלך.</span>';
$_['entry_symbol_left']    = 'סימול שמאל:';
$_['entry_symbol_right']   = 'סימול ימין:';
$_['entry_decimal_place']  = 'אפסים אחרי הנקודה:';
$_['entry_status']         = 'מצב:';

// Error
$_['error_permission']     = 'אזהרה: אין לך הרשאה לשנות מטבעות!';
$_['error_title']          = 'כותר מטבע חייב להכיל בין 3 ל-32 תווים!';
$_['error_code']           = 'קוד מטבע חייב להכיל 3 תווים!';
$_['error_default']        = 'אזהרה: אי אפשר מטבע זה מכיוון שהוא כרגע מוגדר בחנות ברירת המ+חדל!';
$_['error_store']          = 'אזהרה: אי אפשר למחוק מטבע זה מכיוון שהוא כרגע מוגדר ב-%s חנויות!';
$_['error_order']          = 'אזהרה: אי אפשר למחוק מטבע זה מכיוון שהוא כרגע מוגדר ב-%s הזמנות!';
?>