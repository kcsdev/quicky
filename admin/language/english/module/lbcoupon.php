<?php
// Heading
$_['heading_title']       		= 'LB Coupon';

// Text
$_['text_module']         		= 'Modules';
$_['text_success']        		= 'Success: You have modified module LB Coupon!';
$_['text_content_top']    		= 'Content Top';
$_['text_content_bottom'] 		= 'Content Bottom';
$_['text_column_left']    		= 'Column Left';
$_['text_column_right']   		= 'Column Right';
$_['text_image_manager']   		= 'Upload';
// Entry
$_['entry_banner']        		= 'Banner:';
$_['entry_discount_type']     	= 'Discount Type:';
$_['entry_discount_amount']     = 'Discount Amount:';
$_['entry_discount_duration']   = 'Valid Days:';
$_['entry_title']        		= 'Title:';

$_['entry_status']        		= 'Status:';

// Error
$_['error_permission']    		= 'Warning: You do not have permission to modify module LB Coupon!';
?>