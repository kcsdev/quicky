<?php
// Heading
$_['heading_title']       = 'WebNet Bulk Options to products';

// Text
$_['text_module']         = 'Modules';
$_['text_success']        = 'Success: You have modified module WebNet Bulk Options to products!';


// Entry
$_['entry_status'] = 'Status:';
$_['entry_delete_empty'] = 'Delete Empty Options: <span class="help">If enabled, it will not save options which has no option value.</span>';
$_['text_enabled'] = "Yes";
$_['text_no'] = "No";
$_['text_enabled'] = 'Enabled';
$_['text_disabled'] = 'Disabled';


// tabs
$_['tab_general'] = 'General';
$_['tab_help']  = 'Help';

$_['button_remove'] = 'Remove';

// Errors
$_['error_length'] = 'Please add a full number for redemption code length!';
$_['error_chars'] = 'Please specify characters to be used in redemption generation!';
?>