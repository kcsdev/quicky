<?php
// Heading
$_['heading_title']       = 'Custom Registration Field';

// Text
$_['text_module']         = 'Modules';
$_['text_success']        = 'Success: You have modified module Custom Registration Field!';
$_['text_date']			  = 'Date';
$_['text_file']			  = 'File';
$_['text_textarea']		  = 'Textarea';
$_['text_datetime']		  = 'Date Time';
$_['text_text']			  = 'Text';
$_['text_checkbox']		  = 'Checkbox';
$_['text_select']		  = 'Select';
$_['text_radio']		  = 'Radio';
$_['text_types']		  = 'Only for radio, select & checkbox.';
$_['text_none']		  	  = '-- None --';
$_['text_display_field']  = 'Display Field';

// Entry
$_['entry_status']        = 'Status:';
$_['entry_sort_order']    = 'Sort Order:';
$_['entry_name']		  = 'Field Name (Autocomplete):';
$_['entry_type']		  = 'Field Type:<span class="help">No Image Type</span>';
$_['entry_parent']		  = 'Parent Option:<span class="help">Only select or radio type can be parent.</span>';
$_['entry_required']	  = 'Required:';
$_['entry_unique_id']	  = 'Values:';

// Button
$_['button_add_field'] 	  = 'Add Field';

// Error
$_['error_permission']    = 'Warning: You do not have permission to modify module Custom Registration Field!';
$_['error_field']	      = 'Warning: All fields are to be filled in!';
$_['error_type']		  = 'Image type not allowed!';
?>