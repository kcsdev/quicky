<?php
// HTTP
define('HTTP_SERVER', 'http://54.77.118.39/admin/');
define('HTTP_CATALOG', 'http://54.77.118.39/');

// HTTPS
define('HTTPS_SERVER', 'http://54.77.118.39/admin/');
define('HTTPS_CATALOG', 'http://54.77.118.39/');

// DIR
define('DIR_APPLICATION', '/var/www/admin/');
define('DIR_SYSTEM', '/var/www/system/');
define('DIR_DATABASE', '/var/www/system/database/');
define('DIR_LANGUAGE', '/var/www/admin/language/');
define('DIR_TEMPLATE', '/var/www/admin/view/template/');
define('DIR_CONFIG', '/var/www/system/config/');
define('DIR_IMAGE', '/var/www/image/');
define('DIR_CACHE', '/var/www/system/cache/');
define('DIR_DOWNLOAD', '/var/www/download/');
define('DIR_LOGS', '/var/www/system/logs/');
define('DIR_CATALOG', '/var/www/catalog/');

// DB
define('DB_DRIVER', 'mysql_i');
define('DB_HOSTNAME', 'localhost');
define('DB_USERNAME', 'root');
define('DB_PASSWORD', 'reloaded100');
define('DB_DATABASE', 'homeals_site');
define('DB_PREFIX', 'oc_');

?>
