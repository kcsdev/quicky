<?php
class ControllerSaleCollectCurier extends Controller {


 public function index(){
 
  //get lenguage 
  $this->language->load('sale/manager');
  $this->load->language('sale/collect_curier');
  $this->data['heading_title'] = $this->language->get('heading_title');
  $this->document->setTitle($this->language->get('heading_title'));
   $this->data['text_date'] = $this->language->get('text_date'); 
     $this->data['text_names'] = $this->language->get('text_names');
      $this->data['text_details'] = $this->language->get('text_details');
     
    //get links
     $this->data['action_details'] = $this->url->link('sale/collect_curier/details', 'token=' . $this->session->data['token']); 
     
          //side table
     $this->data['text_tableheader'] = $this->language->get('text_tableheader');
      $this->data['text_going'] = $this->language->get('text_going');
      $this->data['text_available'] = $this->language->get('text_available');
      $this->data['text_links'] = $this->language->get('text_links');
      $this->data['text_watch'] = $this->language->get('text_watch');
     $this->data['text_rest'] = $this->language->get('text_rest');
     $this->data['text_manager'] = $this->language->get('text_manager');
      $this->data['text_quick'] = $this->language->get('text_quick');
      $this->data['text_collect_curier'] = $this->language->get('text_collect_curier');
     
    //get links
     $this->data['watch_link'] = $this->url->link('sale/watch', 'token=' . $this->session->data['token']  );
     $this->data['manager_link'] = $this->url->link('sale/manager', 'token=' . $this->session->data['token']  );
     $this->data['rest_link'] = $this->url->link('sale/managerest', 'token=' . $this->session->data['token']  );
      $this->data['quik_link'] = $this->url->link('sale/quick', 'token=' . $this->session->data['token']  );
      $this->data['curier_collect_link'] = $this->url->link('sale/collect_curier', 'token=' . $this->session->data['token']  );
     
     
    //get date
     date_default_timezone_set('Asia/Jerusalem');
    $this->data['date'] = date("Y-m-d"); 
     
    //load model
     $this->load->model('sale/collect_curier');
    $this->data['curiers'] = $this->model_sale_collect_curier->getCuriers(date("d-m-Y"));
    // $this->data['curiers'] = $this->model_sale_collect_curier->getCuriers('14-10-2015');
  $this->template = 'sale/collect_curier.tpl'; 
     
    $this->children = array(
        'common/header',
        'common/footer'
    );  
 
    $this->response->setOutput($this->render()); 
     
 
 }
    
 public function details(){
 
 // get the lenguage
  $this->load->language('sale/collect_curier');
  $this->data['heading_title'] = $this->language->get('heading_title');
  $this->document->setTitle($this->language->get('heading_title'));
  $this->data['text_rest_name'] = $this->language->get('text_rest_name');
  $this->data['text_cash'] = $this->language->get('text_cash');
  $this->data['text_customer_name'] = $this->language->get('text_customer_name');
  $this->data['text_address'] = $this->language->get('text_address');
  $this->data['text_money'] = $this->language->get('text_money');
   $this->data['text_curier_name'] = $this->language->get('text_curier_name');
      $this->data['text_extra_cash'] = $this->language->get('text_extra_cash');
    $this->data['text_claim'] = $this->language->get('text_claim');
     $this->data['text_cash_orders'] = $this->language->get('text_cash_orders');
     $this->data['text_full'] = $this->language->get('text_full');
     $this->data['text_give'] = $this->language->get('text_give');
     $this->data['text_take'] = $this->language->get('text_take');
     $this->data['text_diffrent'] = $this->language->get('text_diffrent');
     $this->data['text_storm'] = $this->language->get('text_storm');
     
 //get data from model
   $this->load->model('sale/collect_curier');
     $rest_det =  $this->model_sale_collect_curier->getOrders($this->request->get['id'],$this->request->get['date']);
     $total_cash = 0;
     foreach($rest_det as $det){
      $total_cash += $det['cash'];
     }
     $this->data['total_cash'] = $total_cash;
     $this->data['rest_det'] = $rest_det ;
    $this->template = 'sale/collect_curier_details.tpl'; 
     
    $this->children = array(
        'common/header',
        'common/footer'
    );  
 
    $this->response->setOutput($this->render()); 
 
 }
    
 public function getData(){
     
     //load model
     $this->load->model('sale/collect_curier');
 
   $this->load->model('sale/collect_curier');
   $curiers = $this->model_sale_collect_curier->getCuriers($this->request->post['date']);
   echo json_encode($curiers, JSON_UNESCAPED_UNICODE );
 }
    
 public function insert(){
 
     $this->load->model('sale/collect_curier');
     $question = $this->model_sale_collect_curier->insert($this->request->post);
     if($question == 'insert'){
       echo 'insert';
     
     }else{
       echo  json_encode($question, JSON_UNESCAPED_UNICODE );
     
     }
   
 
 }
    
 public function edit(){
 
   $this->load->model('sale/collect_curier');
     $y = str_replace("&quot;",'"',$this->request->post['input_arr']);
    $y = json_decode($y);
   $this->model_sale_collect_curier->edit($y, $this->request->post['curier_id'], $this->request->post['date']);

 }    
    

 



}