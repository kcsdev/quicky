<?php    
class ControllerSaleCurier extends Controller { 
  
    public function index(){
        
    $this->language->load('sale/curier');
    $this->document->setTitle($this->language->get('heading_title'));    
      
    //get all lenguage data
     $this->data['heading_title'] = $this->language->get('heading_title');
     $this->data['button_delete'] = $this->language->get('text_delete');
    $this->data['button_edit'] = $this->language->get('text_edit'); 
    $this->data['button_add'] = $this->language->get('text_add');
    $this->data['entery_name'] = $this->language->get('entery_name');
        
        
     //load the model
     $this->load->model('sale/curier');
     
     //get all the curiers
     $this->data['curiers'] = $this->model_sale_curier->getList();  
    
     //get url for links     
        $this->data['action_edit'] = $this->url->link('sale/curier/edit', 'token=' . $this->session->data['token']);     
         $this->data['action_delete'] = $this->url->link('sale/curier/delete', 'token=' . $this->session->data['token']);
        $this->data['action_add'] = $this->url->link('sale/curier/add', 'token=' . $this->session->data['token']);
    
    
      $this->template = 'sale/curier.tpl'; 
     
    $this->children = array(
        'common/header',
        'common/footer'
    );  
 
    $this->response->setOutput($this->render()); 
    
    }
    
    
    
 public function add(){
 
   $this->language->load('sale/curier');
    $this->document->setTitle($this->language->get('heading_title')); 
     
     //get all lenguage data
     $this->data['heading_title'] = $this->language->get('heading_title');
      $this->data['button_save'] = $this->language->get('text_save');
     $this->data['entery_back'] = $this->language->get('entery_back');
     $this->data['entery_firstname'] = $this->language->get('entery_firstname');
      $this->data['entery_lastname'] = $this->language->get('entery_lastname');
     $this->data['entery_phone'] = $this->language->get('entery_phone');
     $this->data['entery_homephone'] = $this->language->get('entery_homephone');
      $this->data['entery_id'] = $this->language->get('entery_id');
      $this->data['entery_address'] = $this->language->get('entery_address');
     $this->data['entery_job'] = $this->language->get('entery_job');
     $this->data['entery_status'] = $this->language->get('entery_status');
     $this->data['entery_curier'] = $this->language->get('entery_curier');
     $this->data['entery_phone_job'] = $this->language->get('entery_phone_job');
     $this->data['entery_activate'] = $this->language->get('entery_activate');
     
     $this->data['status'] = 0;
     $this->data['curier_job'] = 0;
     $this->data['phone_job'] = 0;
     
     if($this->request->server['REQUEST_METHOD'] == 'POST'){

      //load the model
     $this->load->model('sale/curier');
     $this->model_sale_curier->insert($this->request->post); 
     $this->redirect($this->url->link('sale/curier', 'token=' . $this->session->data['token'] ));
     }
     
     //get url for links     
     $this->data['form_action'] = $this->url->link('sale/curier/add', 'token=' . $this->session->data['token'] );  
     
 
 
    $this->template = 'sale/curier_form.tpl'; 
     
    $this->children = array(
        'common/header',
        'common/footer'
    );  
 
    $this->response->setOutput($this->render()); 
 
 }
    
    
  public function edit(){
 
   $this->language->load('sale/curier');
    $this->document->setTitle($this->language->get('heading_title')); 
     
     //get all lenguage data
     $this->data['heading_title'] = $this->language->get('heading_title');
      $this->data['button_save'] = $this->language->get('text_save');
     $this->data['entery_back'] = $this->language->get('entery_back');
     $this->data['entery_firstname'] = $this->language->get('entery_firstname');
      $this->data['entery_lastname'] = $this->language->get('entery_lastname');
     $this->data['entery_phone'] = $this->language->get('entery_phone');
     $this->data['entery_homephone'] = $this->language->get('entery_homephone');
      $this->data['entery_id'] = $this->language->get('entery_id');
      $this->data['entery_address'] = $this->language->get('entery_address');
           $this->data['entery_job'] = $this->language->get('entery_job');
     $this->data['entery_status'] = $this->language->get('entery_status');
     $this->data['entery_curier'] = $this->language->get('entery_curier');
     $this->data['entery_phone_job'] = $this->language->get('entery_phone_job');
     $this->data['entery_activate'] = $this->language->get('entery_activate');
      
      
     //load the model
     $this->load->model('sale/curier');
     $curier = $this->model_sale_curier->getCurier($this->request->get['curier_id']);

     if(isset($curier[0]['first_name'])){
     
      $this->data['firstname'] = $curier[0]['first_name'];
     
     }else{
     
        $this->data['firstname'] = '';
     }
      
    if(isset($curier[0]['last_name'])){
     
      $this->data['lastname'] = $curier[0]['last_name'];
     
     }else{
     
        $this->data['lastname'] = '';
     }
      
    if(isset($curier[0]['phone'])){
     
      $this->data['phone'] = $curier[0]['phone'];
     
     }else{
     
        $this->data['phone'] = '';
     }
      
    if(isset($curier[0]['home_phone'])){
     
      $this->data['homephone'] = $curier[0]['home_phone'];
     
     }else{
     
        $this->data['homephone'] = '';
     }
      
      if(isset($curier[0]['personal_id'])){
     
      $this->data['id'] = $curier[0]['personal_id'];
     
     }else{
     
        $this->data['id'] = '';
     }
      
     if(isset($curier[0]['address'])){
     
      $this->data['address'] = $curier[0]['address'];
     
     }else{
     
        $this->data['address'] = '';
     }
      
     if(isset($curier[0]['status'])){
     
      $this->data['status'] = $curier[0]['status'];
     
     }else{
     
        $this->data['status'] = 0;
     }
      
         if(isset($curier[0]['curier'])){
     
      $this->data['curier_job'] = $curier[0]['curier'];
     
     }else{
     
        $this->data['curier_job'] = 0;
     }
      
    if(isset($curier[0]['phone_job'])){
     
      $this->data['phone_job'] = $curier[0]['phone_job'];
     
     }else{
     
        $this->data['phone_job'] = 0;
     }
      
      
      
      
     if($this->request->server['REQUEST_METHOD'] == 'POST'){
        
     
     $this->model_sale_curier->update($this->request->post, $this->request->get['curier_id']); 
     $this->redirect($this->url->link('sale/curier', 'token=' . $this->session->data['token'] ));
     }
     
     //get url for links     
     $this->data['form_action'] = $this->url->link('sale/curier/edit', 'token=' . $this->session->data['token'] .'&curier_id=' . $this->request->get['curier_id'] );  
     
 
 
    $this->template = 'sale/curier_form.tpl'; 
     
    $this->children = array(
        'common/header',
        'common/footer'
    );  
 
    $this->response->setOutput($this->render()); 
 
 }
    
 public function delete(){
 
   //load the model
     $this->load->model('sale/curier');
      $curier = $this->model_sale_curier->delete($this->request->get['curier_id']);
       $this->redirect($this->url->link('sale/curier', 'token=' . $this->session->data['token'] ));
 
 }
    
}


?>