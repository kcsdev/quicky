<?php    
class ControllerSaleManagerest extends Controller {

   public function index(){
       
    $this->language->load('sale/manager'); 
     $this->language->load('sale/managerest');
    $this->document->setTitle($this->language->get('heading_title'));
     
     //get all lenguage data
     $this->data['heading_title'] = $this->language->get('heading_title');
       
     $this->data['text_name'] = $this->language->get('text_name');
     $this->data['text_neutralizing'] = $this->language->get('text_neutralizing');
     $this->data['text_comment'] = $this->language->get('text_comment');
      $this->data['text_time'] = $this->language->get('text_time'); 
       
    //side table
     $this->data['text_tableheader'] = $this->language->get('text_tableheader');
      $this->data['text_going'] = $this->language->get('text_going');
      $this->data['text_available'] = $this->language->get('text_available');
      $this->data['text_links'] = $this->language->get('text_links');
      $this->data['text_watch'] = $this->language->get('text_watch');
     $this->data['text_rest'] = $this->language->get('text_rest');
     $this->data['text_manager'] = $this->language->get('text_manager');
      $this->data['text_quick'] = $this->language->get('text_quick');
      $this->data['text_collect_curier'] = $this->language->get('text_collect_curier');
       
     //get links
     $this->data['staffing'] = $this->url->link('sale/manager', 'token=' . $this->session->data['token'] .  '&curier=0');
     $this->data['all_link'] = $this->url->link('sale/manager', 'token=' . $this->session->data['token'] );
      $this->data['validate_link'] = $this->url->link('sale/manager', 'token=' . $this->session->data['token'] . '&fax=3' );
     $this->data['gived_link'] = $this->url->link('sale/manager', 'token=' . $this->session->data['token'] . '&status=2' );
     $this->data['watch_link'] = $this->url->link('sale/watch', 'token=' . $this->session->data['token']  );
     $this->data['manager_link'] = $this->url->link('sale/manager', 'token=' . $this->session->data['token']  );
     $this->data['rest_link'] = $this->url->link('sale/managerest', 'token=' . $this->session->data['token']  );
     $this->data['quik_link'] = $this->url->link('sale/quick', 'token=' . $this->session->data['token']  );   
    $this->data['curier_collect_link'] = $this->url->link('sale/collect_curier', 'token=' . $this->session->data['token']  );
       
    $this->load->model('sale/managerest');
    $this->data['rest'] = $this->model_sale_managerest->getRest();
       
       
    $this->template = 'sale/managerest.tpl'; 
     
    $this->children = array(
        'common/header',
        'common/footer'
    );  
 
    $this->response->setOutput($this->render()); 


   }

 public function update(){

   $this->load->model('sale/managerest');
    $this->data['rest'] = $this->model_sale_managerest->update($this->request->get['id']);
    $this->redirect($this->url->link('sale/managerest', 'token=' . $this->session->data['token'] ));
 
 }
    
 public function delete(){
   
     $this->load->model('sale/managerest');
    $this->data['rest'] = $this->model_sale_managerest->delete($this->request->get['id']);
    $this->redirect($this->url->link('sale/managerest', 'token=' . $this->session->data['token'] ));
 
 }

public function comment(){
    $this->load->model('sale/managerest');
    $this->data['rest'] = $this->model_sale_managerest->updateComment($this->request->post['id'], $this->request->post['comment']);
    $this->redirect($this->url->link('sale/managerest', 'token=' . $this->session->data['token'] ));

}
    
public function addTime(){


    $this->load->model('sale/managerest');
    $this->data['rest'] = $this->model_sale_managerest->updatetime($this->request->post['id'], $this->request->post['comment']);
}


} 