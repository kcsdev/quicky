<?php    
class ControllerSaleWatch extends Controller { 

public function index(){
    
     $this->language->load('sale/manager');
    $this->language->load('sale/watch');
    $this->document->setTitle($this->language->get('heading_title'));
     
     //get all lenguage data
     $this->data['heading_title'] = $this->language->get('heading_title');
     $this->data['text_insert'] = $this->language->get('text_insert');
    $this->data['text_edit'] = $this->language->get('text_edit');
    $this->data['text_delete'] = $this->language->get('text_delete');
    $this->data['entery_name'] = $this->language->get('entery_name');
    $this->data['text_enter'] = $this->language->get('text_enter');
    $this->data['text_active'] = $this->language->get('text_active');
    $this->data['text_break'] = $this->language->get('text_break');
    
    //get links
   $this->data['action_insert'] = $this->url->link('sale/watch/insert', 'token=' . $this->session->data['token']);
    $this->data['action_edit'] = $this->url->link('sale/watch/edit', 'token=' . $this->session->data['token']);     
     $this->data['action_delete'] = $this->url->link('sale/watch/delete', 'token=' . $this->session->data['token']);
    
         //side table
     $this->data['text_tableheader'] = $this->language->get('text_tableheader');
      $this->data['text_going'] = $this->language->get('text_going');
      $this->data['text_available'] = $this->language->get('text_available');
      $this->data['text_links'] = $this->language->get('text_links');
      $this->data['text_watch'] = $this->language->get('text_watch');
     $this->data['text_rest'] = $this->language->get('text_rest');
     $this->data['text_manager'] = $this->language->get('text_manager');
      $this->data['text_quick'] = $this->language->get('text_quick');
     $this->data['text_collect_curier'] = $this->language->get('text_collect_curier');
     //get links
     $this->data['staffing'] = $this->url->link('sale/manager', 'token=' . $this->session->data['token'] .  '&curier=0');
     $this->data['all_link'] = $this->url->link('sale/manager', 'token=' . $this->session->data['token'] );
      $this->data['validate_link'] = $this->url->link('sale/manager', 'token=' . $this->session->data['token'] . '&fax=3' );
     $this->data['gived_link'] = $this->url->link('sale/manager', 'token=' . $this->session->data['token'] . '&status=2' );
     $this->data['watch_link'] = $this->url->link('sale/watch', 'token=' . $this->session->data['token']  );
    $this->data['manager_link'] = $this->url->link('sale/manager', 'token=' . $this->session->data['token']  );
     $this->data['rest_link'] = $this->url->link('sale/managerest', 'token=' . $this->session->data['token']  );
     $this->data['quik_link'] = $this->url->link('sale/quick', 'token=' . $this->session->data['token']  );
     $this->data['curier_collect_link'] = $this->url->link('sale/collect_curier', 'token=' . $this->session->data['token']  );
    
    $this->load->model('sale/watch');
    $this->data['curiers'] = $this->model_sale_watch->getLIst();

  $this->template = 'sale/watch.tpl'; 
     
    $this->children = array(
        'common/header',
        'common/footer'
    );  
 
    $this->response->setOutput($this->render()); 

}
    
 public function insert(){
     
    $this->language->load('sale/watch');
    $this->document->setTitle($this->language->get('heading_title'));
     
     //get all lenguage data
     $this->data['heading_title'] = $this->language->get('heading_title');
      $this->data['button_save'] = $this->language->get('button_save');
      $this->data['entery_back'] = $this->language->get('entery_back');
        $this->data['text_curier'] = $this->language->get('text_curier');
     $this->data['text_mirs'] = $this->language->get('text_mirs');
      $this->data['text_motorcycle'] = $this->language->get('text_motorcycle');
      $this->data['text_change'] = $this->language->get('text_change');
      $this->data['text_storm'] = $this->language->get('text_storm');
     
     $this->data['storm_suit'] = 0;
     
      //get links
       $this->data['form_action'] = $this->url->link('sale/watch/insert', 'token=' . $this->session->data['token']);
     
     //load model
     $this->load->model('sale/watch');
     
     //get curiers
     if(isset($this->request->get['curier_id'])){
     $this->data['curiers'] = $this->model_sale_watch->get($this->request->get['curier_id']);
     }
     if ($this->request->post) {
       $this->model_sale_watch->insert($this->request->post);
       $this->redirect($this->url->link('sale/watch', 'token=' . $this->session->data['token'] ));
     }
     
    $this->template = 'sale/watch_form.tpl'; 
     
    $this->children = array(
        'common/header',
        'common/footer'
    );  
 
    $this->response->setOutput($this->render()); 
  
 
 }
    
 public function edit(){
    
       $this->language->load('sale/watch');
    $this->document->setTitle($this->language->get('heading_title'));
     
     //get all lenguage data
     $this->data['heading_title'] = $this->language->get('heading_title');
      $this->data['button_save'] = $this->language->get('button_save');
      $this->data['entery_back'] = $this->language->get('entery_back');
        $this->data['text_curier'] = $this->language->get('text_curier');
     $this->data['text_mirs'] = $this->language->get('text_mirs');
      $this->data['text_motorcycle'] = $this->language->get('text_motorcycle');
      $this->data['text_change'] = $this->language->get('text_change');
      $this->data['text_storm'] = $this->language->get('text_storm');
     
      //get links
       $this->data['form_action'] = $this->url->link('sale/watch/edit', 'token=' . $this->session->data['token'] . '&curier_id=' . $this->request->get['curier_id']);
     
     //load model
     $this->load->model('sale/watch');
     
     //get curier
     $curier =  $this->model_sale_watch->getCurier($this->request->get['curier_id']);

     if(isset($curier[0]['first_name']) && isset($curier[0]['last_name']) && isset($curier[0]['id'])){
        $this->data['curier_id'] = $curier[0]['curier_id'];
         $this->data['name'] = $curier[0]['first_name'] . ' ' .  $curier[0]['last_name'] ;
     }else{
         $this->data['curier_id'] = '';
         $this->data['name'] = '';
     }
     
    if(isset($curier[0]['mirs'])){
        $this->data['mirs'] = $curier[0]['mirs'];
     }else{
         $this->data['mirs'] = '';
     }
    if(isset($curier[0]['motorcycle'])){
        $this->data['motorcycle'] = $curier[0]['motorcycle'];
     }else{
         $this->data['motorcycle'] = '';
     }
     if(isset($curier[0]['change_money'])){
        $this->data['change_money'] = $curier[0]['change_money'];
     }else{
         $this->data['change_money'] = '';
     }
     if(isset($curier[0]['storm_suit'])){
        $this->data['storm_suit'] = $curier[0]['storm_suit'];
     }else{
         $this->data['storm_suit'] = 0;
     }
   
     if ($this->request->post) {
       $this->model_sale_watch->update($this->request->post, $this->request->get['curier_id']);
       $this->redirect($this->url->link('sale/watch', 'token=' . $this->session->data['token'] ));
     }
     
    $this->template = 'sale/watch_form.tpl'; 
     
    $this->children = array(
        'common/header',
        'common/footer'
    );  
 
    $this->response->setOutput($this->render());
 
  }
    
 public function delete(){
     
  //load model
  $this->load->model('sale/watch');
 $this->model_sale_watch->delete($this->request->get['curier_id']);
  $this->redirect($this->url->link('sale/watch', 'token=' . $this->session->data['token'] ));
 }
    
 public function updateBreak (){
   $this->load->model('sale/watch');
 $this->model_sale_watch->getBreak($this->request->get['curier_id']);
  $this->redirect($this->url->link('sale/watch', 'token=' . $this->session->data['token'] ));
   
 
 }
    
 public function endBreak(){
 $this->load->model('sale/watch');
 $this->model_sale_watch->finishBreak($this->request->get['curier_id']);
  $this->redirect($this->url->link('sale/watch', 'token=' . $this->session->data['token'] ));
 
 }


}

?>