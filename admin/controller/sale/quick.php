<?php    
class ControllerSaleQuick extends Controller { 

 public function index(){
 
  $this->load->language('sale/quick');
     
$this->document->setTitle($this->language->get('heading_title'));
 //get language    
$this->data['heading_title'] = $this->language->get('heading_title');
$this->data['button_save'] = $this->language->get('button_save');
$this->data['entery_back'] = $this->language->get('entery_back');
 $this->data['text_number'] = $this->language->get('text_number');
$this->data['text_name'] = $this->language->get('text_name'); 
$this->data['text_phone'] = $this->language->get('text_phone');
$this->data['text_city'] = $this->language->get('text_city'); 
$this->data['text_street'] = $this->language->get('text_street'); 
$this->data['text_house_number'] = $this->language->get('text_house_number'); 
$this->data['text_entrence'] = $this->language->get('text_entrence'); 
$this->data['text_appartment_number'] = $this->language->get('text_appartment_number'); 
 $this->data['text_floor'] = $this->language->get('text_floor'); 
   $this->data['text_restaurant'] = $this->language->get('text_restaurant');  
 $this->data['text_time_make'] = $this->language->get('text_time_make'); 
 $this->data['text_add_row'] = $this->language->get('text_add_row');     
//get links     
$this->data['form_action'] = $this->url->link('sale/quick/insert', 'token=' . $this->session->data['token']  );
// get data 
 $this->load->model('sale/quick');    
$this->data['restaurants'] = $this->model_sale_quick->getRest();

$this->template = 'sale/quick.tpl'; 
     
$this->children = array(
        'common/header',
        'common/footer'
    );  
 
    $this->response->setOutput($this->render()); 
 
 }
    
 public function insert(){
   print_r($this->request->post);
   $this->load->model('sale/quick'); 
   $citeis = $this->model_sale_quick->get_cities($this->request->post['rest_id']);
   $cities_arr = json_decode($citeis[0]['data']);
    date_default_timezone_set ('Asia/Jerusalem');
    $time_arr = explode(":",date('H:i'));
    $bar_start = 60*(int)$time_arr[0] + (int)$time_arr[1];
    foreach($this->request->post['order'] as $order){
      $this->model_sale_quick->insert($this->request->post, $order , $bar_start);
    }
     $this->redirect($this->url->link('sale/manager', 'token=' . $this->session->data['token'] ));
 }

public function get_id(){
 $this->load->model('sale/quick'); 
$id = $this->model_sale_quick->get_id($this->request->post['name']);
echo $id[0]['customer_id'];

}
    
public function get_city(){
 $this->load->model('sale/quick'); 
$id = $this->model_sale_quick->get_id($this->request->post['name']);
$citeis = $this->model_sale_quick->get_cities($id[0]['customer_id']);
echo $citeis[0]['data'];
}
    
public function get_time(){
$this->load->model('sale/quick'); 
$id = $this->model_sale_quick->get_id($this->request->post['name']);
$crf = $this->model_sale_quick->getCrf($id[0]['customer_id']);
$options_rest = unserialize($crf[0]['crf']);   
$time_rest = (int)$options_rest[62];
echo $time_rest;
}

}