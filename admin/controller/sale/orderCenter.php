<?php

class ControllerSaleOrderCenter extends Controller {


  public function index(){
      
  
          $this->language->load('sale/cart');
      
          $this->document->setTitle($this->language->get('heading_title'));
       
        //get lenguage data
        $this->data['heading_title'] = $this->language->get('heading_title');
        $this->data['entery_mail'] = $this->language->get('entery_mail');
       $this->data['entery_phone'] = $this->language->get('entery_phone');
      $this->data['entery_price'] = $this->language->get('entery_price');
      $this->data['entery_restaurent'] = $this->language->get('entery_restaurent');
      $this->data['entery_action'] = $this->language->get('entery_action');
      $this->data['entery_edit'] = $this->language->get('entery_edit');
      $this->data['entery_remove'] = $this->language->get('entery_remove');
      
        //get data from db
        $this->load->model('sale/order');
      
        $order = $this->model_sale_order->getNewOrders();
        if(!empty($order)){
            
        $this->data['order'] = $order;
        //get url for links     
        $this->data['action_edit'] = $this->url->link('sale/orderCenter/edit', 'token=' . $this->session->data['token']);     
         $this->data['action_delete'] = $this->url->link('sale/orderCenter/delete', 'token=' . $this->session->data['token']);    
            
        }else{
        
         $order = array();
        
        
        }
        // load template
        $this->template = 'sale/order_center.tpl';
		$this->children = array(
			'common/header',
			'common/footer'
		);
		
		$this->response->setOutput($this->render());
  
  
  
        }
    
    public function delete(){
    
      $this->load->model('sale/order');
        
      $this->model_sale_order->deleteNewOrders($_GET['order_id']);
    
     $this->redirect($this->url->link('sale/orderCenter', 'token=' . $this->session->data['token'] ));
    }
    
    public function edit(){
    
    
    $this->language->load('sale/cart');
        
    $this->document->setTitle($this->language->get('heading_title'));    
    //get lenguage data    
    $this->data['heading_title'] = $this->language->get('heading_title');
     $this->data['entery_name'] = $this->language->get('entery_name');   
    $this->data['entery_phone'] = $this->language->get('entery_phone');
     $this->data['entery_mail'] = $this->language->get('entery_mail');
    $this->data['entery_company'] = $this->language->get('entery_company');  
    $this->data['entery_companyPhone'] = $this->language->get('entery_companyPhone');     
    $this->data['entery_city'] = $this->language->get('entery_city');
    $this->data['entery_street'] = $this->language->get('entery_street');    
    $this->data['entery_house'] = $this->language->get('entery_house');     
    $this->data['entery_entrence'] = $this->language->get('entery_entrence');
     $this->data['entery_appartment'] = $this->language->get('entery_appartment');   
      $this->data['entery_floor'] = $this->language->get('entery_floor'); 
     $this->data['entery_comments'] = $this->language->get('entery_comments'); 
    $this->data['entery_price'] = $this->language->get('entery_price'); 
     $this->data['entery_date'] = $this->language->get('entery_date'); 
    $this->data['entery_product'] = $this->language->get('entery_product');
    $this->data['entery_quantity'] = $this->language->get('entery_quantity');
   $this->data['entery_quantity'] = $this->language->get('entery_quantity');
    $this->data['entery_total'] = $this->language->get('entery_total');
    $this->data['entery_priceOne'] = $this->language->get('entery_priceOne');
    $this->data['button_back'] = $this->language->get('entery_back');
    $this->data['entery_restaurent'] = $this->language->get('entery_restaurent');    
    $this->data['entery_delivery'] = $this->language->get('entery_delivery');  
    $this->data['entery_finelPrice'] = $this->language->get('entery_finelPrice');  
    $this->data['entery_edit'] = $this->language->get('entery_edit'); 
    $this->data['entery_special'] = $this->language->get('entery_special');  
    $this->data['entery_time'] = $this->language->get('entery_time');
    //send action to form
    $this->data['form_action'] = $this->url->link('sale/orderCenter/update', 'token=' . $this->session->data['token']); 
        
    //get order details
    $this->load->model('sale/order');
    $this->data['cities'] = $this->model_sale_order->getCities();
    $order = $this->model_sale_order->getNewOrdersTo($_GET['order_id']);
    $this->data['order'] = $order;  
    if(!empty($order[0]['special'])){
    $this->data['special'] = explode(",",$order[0]['special']);
    }
    $cart = json_decode($order[0]['order_cart']);
    $this->data['cart'] = $cart;
        
        
    // load template
     $this->template = 'sale/order_center_form.tpl';
    $this->children = array(
			'common/header',
			'common/footer'
		);
		
    $this->response->setOutput($this->render());   
        
        
    }


    public function update(){
    $this->load->model('sale/order');
    $this->model_sale_order->updateNewOrders($_POST, $_GET['order_id']);
        
   $this->redirect($this->url->link('sale/orderCenter', 'token=' . $this->session->data['token'] ));
      
    
    
    }

   public function ajaxPrice(){
     $new_price = 0;
     
    $this->load->model('sale/order');
    $city = $this->model_sale_order->getRestCities($_POST['id']);
   echo  $city[0]['data'];
   
   }




}





?>