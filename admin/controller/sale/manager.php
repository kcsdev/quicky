<?php    
class ControllerSaleManager extends Controller { 


 public function index(){
 
     $this->language->load('sale/manager');
    $this->document->setTitle($this->language->get('heading_title'));
     
     //get all lenguage data
     $this->data['heading_title'] = $this->language->get('heading_title');
     $this->data['all'] = $this->language->get('all_text');
     $this->data['run_text'] = $this->language->get('run_text');
     $this->data['ready_text'] = $this->language->get('ready_text');
     $this->data['staffing_text'] = $this->language->get('staffing_text');
     $this->data['process_text'] = $this->language->get('process_text');
     $this->data['validate_text'] = $this->language->get('validate_text');
     $this->data['gived_text'] = $this->language->get('gived_text');
     
     $this->data['time_text'] = $this->language->get('time_text');
     $this->data['customer_text'] = $this->language->get('customer_text');
     $this->data['fax_text'] = $this->language->get('fax_text');
     $this->data['brend_text'] = $this->language->get('brend_text');
     $this->data['restaurant_text'] = $this->language->get('restaurant_text');
     $this->data['address_text'] = $this->language->get('address_text');
     $this->data['courier_text'] = $this->language->get('courier_text');
     $this->data['timebar_text'] = $this->language->get('timebar_text');
     $this->data['status_text'] = $this->language->get('status_text');
     $this->data['money_text'] = $this->language->get('money_text');
     $this->data['id_text'] = $this->language->get('id_text');
      $this->data['fax_process'] = $this->language->get('fax_process');
      $this->data['fax_again'] = $this->language->get('fax_again');
      $this->data['fax_fail'] = $this->language->get('fax_fail');
      $this->data['fax_validate'] = $this->language->get('fax_validate');
      $this->data['fax_send'] = $this->language->get('fax_send');
     //delivery language
      $this->data['delivery_notcollected'] = $this->language->get('delivery_notcollected');
      $this->data['delivery_collected'] = $this->language->get('delivery_collected');
      $this->data['delivery_provided'] = $this->language->get('delivery_provided');
      $this->data['delivery_restaurant'] = $this->language->get('delivery_restaurant');
      $this->data['delivery_customer'] = $this->language->get('delivery_customer');
     //side table
     $this->data['text_tableheader'] = $this->language->get('text_tableheader');
      $this->data['text_going'] = $this->language->get('text_going');
      $this->data['text_available'] = $this->language->get('text_available');
      $this->data['text_links'] = $this->language->get('text_links');
      $this->data['text_watch'] = $this->language->get('text_watch');
     $this->data['text_rest'] = $this->language->get('text_rest');
     $this->data['text_manager'] = $this->language->get('text_manager');
      $this->data['text_quick'] = $this->language->get('text_quick');
      $this->data['text_collect_curier'] = $this->language->get('text_collect_curier');
     
     
     //get links
     $this->data['staffing'] = $this->url->link('sale/manager', 'token=' . $this->session->data['token'] .  '&curier=0');
     $this->data['all_link'] = $this->url->link('sale/manager', 'token=' . $this->session->data['token'] );
      $this->data['validate_link'] = $this->url->link('sale/manager', 'token=' . $this->session->data['token'] . '&fax=3' );
     $this->data['gived_link'] = $this->url->link('sale/manager', 'token=' . $this->session->data['token'] . '&status=2' );
     $this->data['running_link'] = $this->url->link('sale/manager', 'token=' . $this->session->data['token'] . '&status=not' );
     $this->data['watch_link'] = $this->url->link('sale/watch', 'token=' . $this->session->data['token']  );
     $this->data['manager_link'] = $this->url->link('sale/manager', 'token=' . $this->session->data['token']  );
     $this->data['rest_link'] = $this->url->link('sale/managerest', 'token=' . $this->session->data['token']  );
      $this->data['quik_link'] = $this->url->link('sale/quick', 'token=' . $this->session->data['token']  );
      $this->data['curier_collect_link'] = $this->url->link('sale/collect_curier', 'token=' . $this->session->data['token']  );
     
     //load the model
     $this->load->model('sale/manager');
     $this->load->model('sale/curier');
    $this->data['curiers'] = $this->model_sale_curier->getList(); 
    $this->data['getFree'] = $this->model_sale_manager->getFree(); 
     
     //get all the orders
     if(isset($_GET['curier']) && $_GET['curier'] == 0){
         
         $orders = $this->model_sale_manager->getNoCurier();
     
     }elseif (isset($_GET['fax']) && $_GET['fax'] == 3){
         
         $orders = $this->model_sale_manager->getFaxValidate();
     
     }elseif (isset($_GET['status']) && $_GET['status'] == 2){
         
         $orders = $this->model_sale_manager->getStatusGived();
     
     }
     elseif (isset($_GET['status']) && $_GET['status'] == 'not'){
         
         $orders = $this->model_sale_manager->getRun();
     
     }else{
     $orders = $this->model_sale_manager->getList();
     }
     
     //send orders
     if(!empty($orders)){
         
     foreach($orders as $order){
         
     $create = explode(" ",$order['create_at']);
     $href = $this->url->link('sale/customer/update', 'token=' . $this->session->data['token'] .  '&customer_id=' . $order['customer_id']);
     $phone = $this->model_sale_manager->getphone($order['rest_id']);
     
    //calculate the time of the order
    $options_rest = $this->model_sale_manager->getcrf($order['rest_id']);
    $rest_get = $this->model_sale_manager->gettime($order['rest_id']);
    $city_arr =  json_decode($rest_get[0]['data']); 
    $time_curier = '';    
       foreach($city_arr as $city){
         if($order['city'] == $city->city)
    
         $time_curier = (int)$city->time;
        }

    $options_rest = unserialize($options_rest[0]['crf']);   
    $time_rest = (int)$options_rest[62];
    $time_fax = 5;
    $total_time = $time_fax + $time_rest + $time_curier;
    //$time_rest = ($time_rest/$total_time);
    $fax_cal = (int)$order['time_fax']/$total_time * 100;
    $rest_cal =  (int)$order['time_rest']/$total_time * 100;   
    $curier_cal = (int)$order['time_curier']/$total_time * 100;  
    $this->updateTimes();   
    $this->data['orders'][] = array(
               'id' => $order['id'],
               'create_at' => $create[1],
               'name' => $order['name'],
               'href' => $href,
               'restaurent' => $order['restaurent'],
               'street' => $order['street'],
               'house_number' => $order['house_number'],
               'city' => $order['city'],
               'status' => $order['status'],
               'time' => date("H:i:s"),
               'final_price' => $order['final_price'],
               'fax' => $order['fax'],
               'type' => $order['type_of_order'],
               'phone' => $phone['telephone'],
               'comment' => $order['comments'],
              'time_fax' => $fax_cal,
              'time_rest' => $rest_cal,
              'time_curier' => $curier_cal,
              'total_time' => $total_time,
               'total_fax' => $time_fax,
              'total_rest' => $time_rest,
              'total_curier' => $time_curier,
        );
         
    if($order['curier_id'] != 0){
      $curier_name = $this->model_sale_manager->getCurier($order['curier_id']);
        $this->data['orders'][count ($this->data['orders'])-1]['curier'] = $curier_name[0]['name'];
     }
     
     }
     
    
     
     }else{
      $this->data['orders'] = array();
     
     }
     
     
    
    $this->template = 'sale/manager.tpl'; 
     
    $this->children = array(
        'common/header',
        'common/footer'
    );  
 
    $this->response->setOutput($this->render()); 
}
    
    public function update(){
    
     $this->load->model('sale/manager');
    $this->model_sale_manager->update($_POST);
    }
    
    public function addCurier(){
 
         $this->load->model('sale/manager');
          $this->model_sale_manager->updateCurier($this->request->post);
           $this->redirect($this->url->link('sale/manager', 'token=' . $this->session->data['token'] ));
    }
    
    public function updateTimes(){
        
    $this->load->model('sale/manager');
     $orders = $this->model_sale_manager->getStatus();
     //get the time now in miutes
     date_default_timezone_set('Asia/Jerusalem'); 
  $time = date("H:i:s");
  $time_arr = explode (':', $time);
   if($time_arr[0] == '00'){
       
       $time_arr[0] = '25';
   
   }
   $time_now = (int)$time_arr[0]*60 + (int)$time_arr[1];

     foreach($orders as $order){
         
      $time_bar = $this->model_sale_manager->gettimebar($order['id']);

       if($time_now > $time_bar[0]['bar_start']){
           
       if($order['fax'] != 4 ){
         $time = $time_now - $time_bar[0]['bar_start'];
           
         $this->model_sale_manager->updateFaxTime($order['id'], $time);
           
        }elseif ($order['fax'] ==4 && $order['status'] != 1){
           
           $time_fax = $this->model_sale_manager->getFaxTime($order['id']);
          $time = $time_now - $time_fax[0]['time_fax'] - $time_bar[0]['bar_start'];
          $this->model_sale_manager->updateRestTime($order['id'], $time);
       
         }elseif($order['status'] == 1){
           $time_fax = $this->model_sale_manager->getFaxTime($order['id']);
           $time_rest = $this->model_sale_manager->getRestTime($order['id']);
          $time = $time_now - $time_rest[0]['time_rest'] - $time_fax[0]['time_fax'] - $time_bar[0]['bar_start'];
          $this->model_sale_manager->updateCurierTime($order['id'], $time);
       
        }
     
       }
     }
    }
 
 
 }


