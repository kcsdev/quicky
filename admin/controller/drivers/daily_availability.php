<?php 
/************************************************************************************
*  Daily Availability by Menny at Homeals   
*************************************************************************************/

class ControllerDriversDailyavailability extends Controller {
	public function index() {
		$this->load->language('drivers/daily_availability');
		$this->language->load('catalog/product');
		
		$this->document->setTitle($this->language->get('heading_title'));
		
		$this->load->model('setting/setting');
			
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && ($this->validate())) {
			
			// deletes old lines
			$delete_sql = "DELETE FROM oc_driver_availability WHERE type = 'custom'";			
			$this->db->query($delete_sql);
			
			// makes new lines
			foreach($this->request->post['product_availability'] as $key => $avail){
				
				$custom_sql = "INSERT INTO oc_driver_availability (driver_availability_id, type, custom_date, per_window, drivers) VALUES (NULL, 'custom', '". $avail['custom_date'] ."', '". $avail['per_window'] ."', '". $avail['drivers'] ."')";
				
				$this->db->query($custom_sql);
			}
			
			foreach($this->request->post['product_def_availability'] as $key => $avail){
				$driver_sql = "UPDATE oc_driver_availability SET per_window = '".(int)$avail['per_window']."' , drivers = '".(int)$avail['drivers']."' WHERE day = '". $key ."'";
				$this->db->query($driver_sql);
			}
			
			$this->load->model('setting/setting');
			
			$this->model_setting_setting->editSetting('daily_availability', $this->request->post);				
			
			$this->session->data['success'] = $this->language->get('text_success');
			
			$this->redirect($this->url->link('extension/drivers', 'token=' . $this->session->data['token'], 'SSL'));
		}
			
			$language_info = array(
			'heading_title',
			'text_enabled',
			'text_disabled',
			'entry_status',
			'entry_data_daily_availability',
			'entry_headings',
			'entry_cdata',
			'button_save',
			'button_cancel',
			'tab_general',
		);
		
		foreach ($language_info as $language) {
			$this->data[$language] = $this->language->get($language);
		}
		

 		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}
		
  		$this->data['breadcrumbs'] = array();

   		$this->data['breadcrumbs'][] = array(
       		'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
       		'text'      => $this->language->get('text_home'),
      		'separator' => FALSE
   		);

   		$this->data['breadcrumbs'][] = array(
       		'href'      => $this->url->link('extension/drivers', 'token=' . $this->session->data['token'], 'SSL'),
       		'text'      => $this->language->get('text_drivers'),
      		'separator' => ' :: '
   		);

   		$this->data['breadcrumbs'][] = array(
       		'href'      => $this->url->link('drivers/daily_availability', 'token=' . $this->session->data['token'], 'SSL'),
       		'text'      => $this->language->get('heading_title'),
      		'separator' => ' :: '
   		);
				
		$this->data['action'] = $this->url->link('drivers/daily_availability', 'token=' . $this->session->data['token'], 'SSL');
		
		$this->data['cancel'] = $this->url->link('extension/drivers', 'token=' . $this->session->data['token'], 'SSL');
		
		if (isset($this->request->post['daily_availability_status'])) {
			$this->data['daily_availability_status'] = $this->request->post['daily_availability_status'];
		} else {
			$this->data['daily_availability_status'] = $this->config->get('daily_availability_status');
		}
		
		
		$this->data['data_daily_availability'] = HTTP_CATALOG . 'index.php?route=drivers/daily_availability';
		
		$this->data['column_quantity']       	 = $this->language->get('column_quantity');
		$this->data['column_date']               = $this->language->get('column_date');
		$this->data['column_last_order_time']    = $this->language->get('column_last_order_time');
		$this->data['column_day_time']    = $this->language->get('column_day_time');
		
		$this->data['text_def_availability']    = $this->language->get('text_def_availability');
		$this->data['text_custom_availability']    = $this->language->get('text_custom_availability');
		 
		$this->data['column_per_window']    = 'הזמנות פר שליח';
		$this->data['column_drivers']    = 'מספר שליחים';
		$this->data['column_by_stary_time']    = $this->language->get('column_by_stary_time');
		
		$this->data['text_start'] = $this->language->get('text_start');
		$this->data['text_end'] = $this->language->get('text_end');
		$this->data['text_morning'] = $this->language->get('text_morning');
		$this->data['text_afternoon'] = $this->language->get('text_afternoon');
		$this->data['text_night'] = $this->language->get('text_night');
		$this->data['text_late_night'] = $this->language->get('text_late_night');
		$this->data['button_remove'] = $this->language->get('button_remove');
		
		$this->data['button_add_availability'] = $this->language->get('button_add_availability');
		
		$drivers_sql = "SELECT * FROM  ". DB_PREFIX ."driver_availability WHERE type ='def'";
		$drivers = $this->db->query($drivers_sql);
		
		$drivers_custom_sql = "SELECT * FROM  ". DB_PREFIX ."driver_availability WHERE type ='custom'";
		$drivers_custom = $this->db->query($drivers_custom_sql);
		
		$this->data['def_availabilitys'] = $drivers->rows;
		$this->data['product_availabilitys'] = $drivers_custom->rows;
		
		$this->template = 'drivers/daily_availability.tpl';
		$this->children = array(
			'common/header',	
			'common/footer'	
		);
		
		$this->response->setOutput($this->render());
	} 
	
	private function validate() {
		if (!$this->user->hasPermission('modify', 'drivers/daily_availability')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		if (!$this->error) {
			return TRUE;
		} else {
			return FALSE;
		}	
	}	
}
?>