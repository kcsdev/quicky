<?php
class ControllerModuleCRF extends Controller {
	private $error = array(); 
	
	public function index() {   
		$this->language->load('module/crf');

		$this->document->setTitle($this->language->get('heading_title'));
		
		$this->load->model('setting/setting');
				
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
			$this->model_setting_setting->editSetting('crf', $this->request->post);	
			
			$this->db->query("DELETE FROM " . DB_PREFIX . "registration_option_value_to_option_value");

			if (isset($this->request->post['crf_fields'])) {
				foreach ($this->request->post['crf_fields'] as $field) { 
					if (isset($field['registration_option_value'])) {
						foreach ($field['registration_option_value'] as $registration_option_value_id => $registration_option_value) {
							foreach ($registration_option_value as $value) {
								$this->db->query("INSERT INTO " . DB_PREFIX . "registration_option_value_to_option_value SET registration_option_value_id = '" . (int)$registration_option_value_id . "', option_value_id = '" . (int)$value . "', option_id = '" . (int)$field['option_id'] . "'");
							}
						}
					}
				}
			}
			
			$this->session->data['success'] = $this->language->get('text_success');
						
			$this->redirect($this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL'));
		}
				
		$this->data['heading_title'] = $this->language->get('heading_title');

		$this->data['text_enabled'] = $this->language->get('text_enabled');
		$this->data['text_disabled'] = $this->language->get('text_disabled');
		$this->data['text_yes'] = $this->language->get('text_yes');
		$this->data['text_no'] = $this->language->get('text_no');
		$this->data['text_text'] = $this->language->get('text_text');
		$this->data['text_file'] = $this->language->get('text_file');
		$this->data['text_datetime'] = $this->language->get('text_datetime');
		$this->data['text_textarea'] = $this->language->get('text_textarea');
		$this->data['text_date'] = $this->language->get('text_date');
		$this->data['text_select'] = $this->language->get('text_select');
		$this->data['text_radio'] = $this->language->get('text_radio');
		$this->data['text_checkbox'] = $this->language->get('text_checkbox');
		$this->data['text_types'] = $this->language->get('text_types');
		$this->data['text_none'] = $this->language->get('text_none');
		$this->data['text_display_field'] = $this->language->get('text_display_field');
		
		$this->data['entry_status'] = $this->language->get('entry_status');
		$this->data['entry_sort_order'] = $this->language->get('entry_sort_order');
		$this->data['entry_type'] = $this->language->get('entry_type');
		$this->data['entry_parent'] = $this->language->get('entry_parent');
		$this->data['entry_name'] = $this->language->get('entry_name');
		$this->data['entry_required'] = $this->language->get('entry_required');
		$this->data['entry_unique_id'] = $this->language->get('entry_unique_id');
		
		$this->data['button_save'] = $this->language->get('button_save');
		$this->data['button_cancel'] = $this->language->get('button_cancel');
		$this->data['button_remove'] = $this->language->get('button_remove');
		$this->data['button_add_field'] = $this->language->get('button_add_field');
		
		$this->data['error_type'] = $this->language->get('error_type');
		
 		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}
		
  		$this->data['breadcrumbs'] = array();

   		$this->data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
      		'separator' => false
   		);

   		$this->data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('text_module'),
			'href'      => $this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL'),
      		'separator' => ' :: '
   		);
		
   		$this->data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('heading_title'),
			'href'      => $this->url->link('module/crf', 'token=' . $this->session->data['token'], 'SSL'),
      		'separator' => ' :: '
   		);
		
		$this->data['token'] = $this->session->data['token'];
		
		$this->data['action'] = $this->url->link('module/crf', 'token=' . $this->session->data['token'], 'SSL');
		
		$this->data['cancel'] = $this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL');

		$fields = array();
		
		if (isset($this->request->post['crf_fields'])) {
			$fields = $this->request->post['crf_fields'];
		} elseif ($this->config->get('crf_fields')) { 
			$fields = $this->config->get('crf_fields');
		}
		
		$this->load->model('catalog/option');
		
		$this->data['fields'] = array();
		
		foreach ($fields as $field) {
			$field['parent_id'] = isset($field['parent_id']) ? $field['parent_id'] : 0;
			
			$option = $this->model_catalog_option->getOption($field['option_id']);
			
			$parent_option_values_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "option_value ov LEFT JOIN " . DB_PREFIX . "option_value_description ovd ON ovd.option_value_id = ov.option_value_id WHERE ovd.language_id = '" . (int)$this->config->get('config_language_id') . "' AND ov.option_id = '" . (int)$field['parent_id'] . "'");
			
			if ($option) {
				if ($option['type'] == 'select' || $option['type'] == 'radio' || $option['type'] == 'checkbox') {
					$values = array();
					
					$option_values = $this->model_catalog_option->getOptionValues($field['option_id']);
					
					foreach ($option_values as $value) {
						$product_option_value_to_option_value_query = $this->db->query("SELECT option_value_id FROM " . DB_PREFIX . "registration_option_value_to_option_value WHERE registration_option_value_id = '" . (int)$value['option_value_id'] . "' AND option_id = '" . (int)$field['option_id'] . "'");
						
						$linked_option_values = array();
				
						foreach ($product_option_value_to_option_value_query->rows as $linked_option_value_id) {
							$linked_option_values[] = $linked_option_value_id['option_value_id'];
						}
						
						$values[] = array(
							'option_value_id'		=> $value['option_value_id'],
							'name'					=> $value['name'],
							'linked_option_values'	=> $linked_option_values
						);
					}
				} else {
					$values = array();
					
					$product_option_value_to_option_value_query = $this->db->query("SELECT option_value_id FROM " . DB_PREFIX . "registration_option_value_to_option_value WHERE registration_option_value_id = '1' AND option_id = '" . (int)$field['option_id'] . "'");
						
					$linked_option_values = array();
			
					foreach ($product_option_value_to_option_value_query->rows as $linked_option_value_id) {
						$linked_option_values[] = $linked_option_value_id['option_value_id'];
					}
					
					$values[] = array(
						'option_value_id'		=> 1,
						'name'					=> '',
						'linked_option_values'	=> $linked_option_values
					);
				}
				
				if ($option['type'] == 'select') {
					$type = $this->language->get('text_select');
				} elseif ($option['type'] == 'radio') {
					$type = $this->language->get('text_radio');
				} elseif ($option['type'] == 'checkbox') {
					$type = $this->language->get('text_checkbox');
				} elseif ($option['type'] == 'date') {
					$type = $this->language->get('text_date');
				} elseif ($option['type'] == 'datetime') {
					$type = $this->language->get('text_datetime');
				} elseif ($option['type'] == 'textarea') {
					$type = $this->language->get('text_textarea');
				} elseif ($option['type'] == 'text') {
					$type = $this->language->get('text_text');
				} elseif ($option['type'] == 'file') {
					$type = $this->language->get('text_file');
				} else {
					$type = '';
				}
			} else {
				$option['name'] = '';
				$type = '';
				$values = array();
			}
			
			$this->data['fields'][] = array(
				'name'					=> $option['name'],
				'type'					=> $type,
				'option_id'				=> $field['option_id'],
				'parent_id'				=> $field['parent_id'],
				'required'				=> $field['required'],
				'status'				=> $field['status'],
				'sort_order'			=> $field['sort_order'],
				'values'				=> $values,
				'parent_option_values' 	=> $parent_option_values_query->rows
			);
		}
		
		$options = $this->model_catalog_option->getOptions();
		
		$this->data['options'] = array();
		
		foreach ($options as $option) {
			if ($option['type'] == 'select' || $option['type'] == 'radio') {
				$this->data['options'][] = array(
					'option_id'			=> $option['option_id'],
					'name'				=> $option['name']
				);
			}
		}
				
		$this->template = 'module/crf.tpl';
		$this->children = array(
			'common/header',
			'common/footer'
		);
				
		$this->response->setOutput($this->render());
	}
	
	private function validate() {
		if (!$this->user->hasPermission('modify', 'module/crf')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}
		
		if (isset($this->request->post['crf_fields'])) {
			foreach ($this->request->post['crf_fields'] as $field) {
				if (empty($field['option_id'])) {
					$this->error['warning'] = $this->language->get('error_field');
				}
			}
		}
				
		if (!$this->error) {
			return true;
		} else {
			return false;
		}	
	}
	
	public function install() {
		$query = $this->db->query("SHOW COLUMNS FROM " . DB_PREFIX . "customer");
		$exists = false;
				
		foreach ($query->rows as $result) {
			if ($result['Field'] == 'crf') {
				$exists = true;
				break;
			}
		}
		
		if (!$exists) {		
			$this->db->query("ALTER TABLE " . DB_PREFIX . "customer ADD crf longtext NOT NULL AFTER lastname");
			
			$this->db->query("
				CREATE TABLE IF NOT EXISTS `" . DB_PREFIX . "registration_option_value_to_option_value` (
				  `registration_option_value_to_option_value_id` int(11) NOT NULL AUTO_INCREMENT,
				  `registration_option_value_id` int(11) NOT NULL,
				  `option_value_id` int(11) NOT NULL,
				  `option_id` int(11) NOT NULL,
				  PRIMARY KEY (`registration_option_value_to_option_value_id`)
				)
			");
		}
	}
	
	public function uninstall() {
		$query = $this->db->query("SHOW COLUMNS FROM " . DB_PREFIX . "customer");
		$exists = false;
				
		foreach ($query->rows as $result) {
			if ($result['Field'] == 'crf') {
				$exists = true;
				break;
			}
		}
		
		if ($exists) {		
			$this->db->query("ALTER TABLE " . DB_PREFIX . "customer DROP crf");
			
			$this->db->query("DROP TABLE IF EXISTS `" . DB_PREFIX . "registration_option_value_to_option_value`");
		}
	}
	
	public function dependentoptionvalues() {
		$json = array();
		
		if (isset($this->request->get['parent_id'])) {
			$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "option_value ov LEFT JOIN " . DB_PREFIX . "option_value_description ovd ON ovd.option_value_id = ov.option_value_id WHERE ovd.language_id = '" . (int)$this->config->get('config_language_id') . "' AND ov.option_id = '" . (int)$this->request->get['parent_id'] . "'");
		
			if ($query->num_rows) {
				$json['value'] = $query->rows;
			} else {
				$json['value'] = '';
			}
		}
		
		$this->response->setOutput(json_encode($json));
	}
	
	public function dependentoption() {
		if (isset($this->request->get['value'])) {
			$option_value_id = (int)$this->request->get['value'];
		} else {
			$option_value_id = 0;
		}
		
		if (isset($this->request->get['parent_id'])) {
			$option_id = (int)$this->request->get['parent_id'];
		} else {
			$option_id = 0;
		}
		
		$json = array();
		
		$fields = $this->config->get('crf_fields');
		
		$json['option'] = array();
		
		foreach ($fields as $field) {
			if ($field['parent_id'] == $option_id) {
				$query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "option` WHERE option_id = '" . (int)$field['option_id'] . "'");
				
				if ($query->row) {
					if ($query->row['type'] == 'select' || $query->row['type'] == 'radio' || $query->row['type'] == 'checkbox') {
						$query1 = $this->db->query("SELECT * FROM " . DB_PREFIX . "option_value ov LEFT JOIN " . DB_PREFIX . "registration_option_value_to_option_value rov2ov ON rov2ov.registration_option_value_id = ov.option_value_id LEFT JOIN " . DB_PREFIX . "option_value_description ovd ON ovd.option_value_id = ov.option_value_id WHERE ovd.language_id = '" . (int)$this->config->get('config_language_id') . "' AND rov2ov.option_value_id = '" . (int)$option_value_id . "' AND rov2ov.option_id = '" . (int)$field['option_id'] . "' ORDER BY ov.sort_order ASC");
					
						$option_value_data = array();
						
						if ($query1->num_rows) {
							foreach ($query1->rows as $option_value) {
								if (isset($this->session->data['crf'][$field['option_id']]) && ($query->row['type'] == 'select' || $query->row['type'] == 'radio')) {
									$selected = $this->session->data['crf'][$field['option_id']];
								} elseif (isset($this->session->data['crf'][$field['option_id']]) && $query->row['type'] == 'checkbox') {
									$selected = (is_array($this->session->data['crf'][$field['option_id']]) && in_array($option_value['option_value_id'], $this->session->data['crf'][$field['option_id']])) ? $option_value['option_value_id'] : 0;
								} else {
									$selected = '';
								}
								
								$option_value_data[] = array(
									'option_value_id'         => $option_value['option_value_id'],
									'name'                    => $option_value['name'],
									'selected'				  => $selected
								);
							}
						}
					} else {
						$query1 = $this->db->query("SELECT * FROM " . DB_PREFIX . "registration_option_value_to_option_value WHERE option_id = '" . (int)$field['option_id'] . "' AND option_value_id = '" . (int)$option_value_id . "'");
					
						if ($query1->num_rows) {
							$option_value_data = 1;
						} else {
							$option_value_data = 0;
						}
					}
					
					$json['option'][] = array(
						'option_id'         => $field['option_id'],
						'type'              => $query->row['type'],
						'option_value'		=> $option_value_data
					);
				}
			}
		}
		
		$this->response->setOutput(json_encode($json));
	}
	
	public function download() {
		if ($this->request->get['file']) {
			$file = DIR_DOWNLOAD . $this->request->get['file'];
			$mask = basename(substr($this->request->get['file'], 0, strrpos($this->request->get['file'], '.')));
			
			if (!headers_sent()) {
				if (file_exists($file)) {
					header('Content-Type: application/octet-stream');
					header('Content-Description: File Transfer');
					header('Content-Disposition: attachment; filename="' . ($mask ? $mask : basename($file)) . '"');
					header('Content-Transfer-Encoding: binary');
					header('Expires: 0');
					header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
					header('Pragma: public');
					header('Content-Length: ' . filesize($file));
					
					readfile($file, 'rb');
					
					exit;
				} else {
					exit('Error: Could not find file ' . $file . '!');
				}
			} else {
				exit('Error: Headers already sent out!');
			}
		} else {
			$this->redirect($this->url->link('sale/customer', 'token=' . $this->session->data['token'], 'SSL'));
		}
	}
	
	public function upload() {
		$this->language->load('common/filemanager');
		
		$json = array();
		
		if (!empty($this->request->files['file']['name'])) {
			$filename = basename(preg_replace('/[^a-zA-Z0-9\.\-\s+]/', '', html_entity_decode($this->request->files['file']['name'], ENT_QUOTES, 'UTF-8')));
			
			if ((utf8_strlen($filename) < 3) || (utf8_strlen($filename) > 64)) {
        		$json['error'] = $this->language->get('error_filename');
	  		}	  	
			
			$allowed = array();
			
			if (!$this->config->get('config_upload_allowed')) {
				$filetypes = explode("\n", $this->config->get('config_file_extension_allowed'));
			} else {
				$filetypes = explode(',', $this->config->get('config_upload_allowed'));
			}
			
			foreach ($filetypes as $filetype) {
				$allowed[] = trim($filetype);
			}
			
			if (!in_array(substr(strrchr($filename, '.'), 1), $allowed)) {
				$json['error'] = $this->language->get('error_file_type');
       		}	
						
			if ($this->request->files['file']['error'] != UPLOAD_ERR_OK) {
				$json['error'] = $this->language->get('error_upload_' . $this->request->files['file']['error']);
			}
		} else {
			$json['error'] = $this->language->get('error_upload');
		}
		
		if (!$json) {
			if (is_uploaded_file($this->request->files['file']['tmp_name']) && file_exists($this->request->files['file']['tmp_name'])) {
				$file = basename($filename) . '.' . md5(mt_rand());
				
				// Hide the uploaded file name so people can not link to it directly.
				$json['file'] = $file;
				
				move_uploaded_file($this->request->files['file']['tmp_name'], DIR_DOWNLOAD . $file);
			}
						
			$json['success'] = $this->language->get('text_uploaded');
		}	
		
		$this->response->setOutput(json_encode($json));		
	}
}
?>