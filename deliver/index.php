<!DOCTYPE html>

<html>
<head>
    <link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.0-beta.1/css/jquery.dataTables.css">
    <link rel="stylesheet" type="text/css" href="css/main.css">
    <link rel="stylesheet" type="text/css" href="css/chosen.css">
    <script type="text/javascript" language="javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
    <script type="text/javascript" language="javascript" src="//cdn.datatables.net/1.10.0/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" language="javascript" src="js/chosen.jquery.min.js" ></script>
    <title>ניהול משלוחים</title>
</head>

<body>
    <?php setlocale(LC_ALL, 'he_IL.UTF-8'); ?>
<div id="main-con">
    
    <div id="header">
        <div id="homeals-logo">
            <img src="images/homeals_logo.png" />
        </div>
        <div id="title">ניהול משלוחים</div>
        <div id="quicky-logo">
            <img src="images/quiky_logo.png" />
        </div>
        <div style="clear: both"></div>
    </div>
    
    <div id="change-date">
        <a href="?date=<?php echo date("Y-m-d",strtotime($_GET['date']." + 1 day")); ?>"><</a>
        <?php echo strftime("%A, %e ב%B",strtotime($_GET['date'])); ?>
        <a href="?date=<?php echo date("Y-m-d",strtotime($_GET['date']." - 1 day")); ?>">></a>
    </div>
    
    <div id="status-filter-con">
        סנן לפי סטאטוס:
        <select id="status-filter" name="status-filter" multiple="multiple" class="chosen-rtl" ></select>
    </div>
    
    <table id="delivery-table" class="display" cellspacing="0" width="100%">
        <thead>
            <tr>
                <th></th>
                <th>כתובת בשלן</th>
                <th>זמן איסוף</th>
                <th>כתובת סועד</th>
                <th>זמן הפצה</th>
                <th style="padding: 0;">ארוחות</th>
                <th>שליח איסוף</th>
                <th>שליח הפצה</th>
                <th>סטטוס</th>
                <th>סטטוס</th>
            </tr>
            
            <tr>
                <th></th>
                <th class="filter" width="140px">כתובת בשלן</th>
                <th class="filter">זמן איסוף</th>
                <th class="filter" width="140px">כתובת סועד</th>
                <th class="filter">זמן הפצה</th>
                <th class="filter" width="15px">ארוחות</th>
                <th class="filter" width="78px">שליח איסוף</th>
                <th class="filter" width="78px">שליח הפצה</th>
                <th class="filter">סטטוס</th>
                <th></th>
            </tr>
        </thead>
        
        <tfoot>
        </tfoot>
        
        
    </table>
</div>
    
<script>

var table;

/* Formatting function for row details - modify as you need */
function format ( d ) {
    
    var status_histoy = "";
    
    d.history.forEach(function(entry) {
        //console.log(entry);
        status_histoy += '<tr>'+
            '<td>'+entry.name+'</td>'+
            '<td>'+entry.date+'</td>'+
        '</tr>';

    });
    
    
    // `d` is the original data object for the row
    return '<table class="inside" cellpadding="5" cellspacing="0" border="0">'+
        '<tr>'+
            '<td colspan=2>פרטי בשלן</td>'+
        '</tr>'+
        '<tr>'+
            '<td>שם:</td>'+
            '<td>'+d.cook_name+'</td>'+
        '</tr>'+
        '<tr>'+
            '<td>כתובת:</td>'+
            '<td><a href="https://maps.google.co.il/maps?q=תל אביב '+d.cook_address_1.replace('#','')+'" target="_blank">'+d.cook_address_1.replace('#','')+'</a>​</td>'+
        '</tr>'+
        '<tr>'+
            '<td>המשך:</td>'+
            '<td>'+d.cook_address_2+'</td>'+
        '</tr>'+
        '<tr>'+
            '<td>זמן איסוף:</td>'+
            '<td>'+d.cook_order_time+'</td>'+
        '</tr>'+
        '<tr>'+
            '<td>חברה:</td>'+
            '<td>'+d.cook_company+'</td>'+
        '</tr>'+
        '<tr>'+
            '<td>איש קשר:</td>'+
            '<td>'+d.cook_name+'</td>'+
        '</tr>'+
        '<tr>'+
            '<td>טלפון:</td>'+
            '<td><a href="tel:'+d.cook_phone+'">'+d.cook_phone+'</a></td>'+
        '</tr>'+
        '<tr>'+
            '<td>הערות לשליח:</td>'+
            '<td>'+d.cook_comment+'</td>'+
        '</tr>'+
    '</table>'+
    '<table  class="inside" cellpadding="5" cellspacing="0" border="0">'+
        '<tr>'+
            '<td colspan=2>פרטי סועד</td>'+
        '</tr>'+
        '<tr>'+
            '<td>שם:</td>'+
            '<td>'+d.customer_name+'</td>'+
        '</tr>'+
        '<tr>'+
            '<td>כתובת:</td>'+
            '<td><a href="https://maps.google.co.il/maps?q=תל אביב '+d.customer_address_1.replace('#','')+'" target="_blank">'+d.customer_address_1.replace('#','')+'</a>​</td>'+
        '</tr>'+
        '<tr>'+
            '<td>המשך:</td>'+
            '<td>'+d.customer_address_2+'</td>'+
        '</tr>'+
        '<tr>'+
            '<td>זמן הפצה:</td>'+
            '<td>'+d.order_time+'</td>'+
        '</tr>'+
        '<tr>'+
            '<td>חברה:</td>'+
            '<td>'+d.customer_company+'</td>'+
        '</tr>'+
        '<tr>'+
            '<td>איש קשר:</td>'+
            '<td>'+d.customer_contact+'</td>'+
        '</tr>'+    
        '<tr>'+
            '<td>טלפון:</td>'+
            '<td><a href="tel:'+d.customer_phone+'">'+d.customer_phone+'</a></td>'+
        '</tr>'+
        '<tr>'+
            '<td>הערות לשליח:</td>'+
            '<td>'+d.order_comment+'</td>'+
        '</tr>'+
    '</table>'+
    '<table  class="inside" cellpadding="5" cellspacing="0" border="0">'+
        '<tr>'+
            '<td colspan=2>מספר הזמנה</td>'+
            '<td>'+d.order_id+'</td>'+
        '</tr>'+
    '</table>'+
    '<table  class="inside" cellpadding="5" cellspacing="0" border="0">'+
        '<tr>'+
            '<td colspan=2>היסטוריית סטטוסים</td>'+
        '</tr>'+
        status_histoy+
    '</table>';
}

// adding select for sorting
function multi_select_status() {
    $('#status-filter').on( 'change', function () {
            
            var value = "";
            
            if ($(this).val()) {
                $(this).val().forEach(function(entry) {
                    if (value == "") {
                        value += entry;
                    } else {
                        value += "|";
                        value += entry;
                    }
                });
            } else {
                value = "";
            }
            
            if (value == "") {
                table.column( 8 )
                .search("")
                    .draw();
            } else {
               table.column( 8 )
                .search( '^'+value+'$', true, false )
                    .draw();
            }
            
    } );
    
    table.column(8).data().unique().sort().each( function ( data ) {
        
        if (data == 5) {
            name = "מאושר";
        } else if (data == 17) {
            name = "נאסף מהבשלן";
        } else if (data == 18) {
            name = "נמסר לסועד";
        } else if (data == 19) {
            name = "בדרך לבשלן";
        } else if (data == 20) {
            name = "בדרך לסועד";
        } else if (data == 21) {
            name = "ממתין למשלוח";
        }
        
    });
    
    $('#status-filter').append( '<option value="5" selected>מאושר</option>' );
    $('#status-filter').append( '<option value="21" selected>ממתין למשלוח</option>' );
    $('#status-filter').append( '<option value="19" selected>בדרך לבשלן</option>' );
    $('#status-filter').append( '<option value="17" selected>נאסף מהבשלן</option>' );
    $('#status-filter').append( '<option value="20" selected>בדרך לסועד</option>' );
    $('#status-filter').append( '<option value="18" >נמסר לסועד</option>' );
    
    $('#status-filter-con').prependTo("#delivery-table_wrapper");
    
    $('#status-filter').chosen({
      placeholder_text_multiple: "בחר סטאטוס הזמנה",
      no_results_text: "אופס!, אין סטאטוסים"
    });  
}

$(document).ready(function() {
    
    var date = "<?php echo $_GET['date']; ?>";
    var driver_list = '<option>'+
                        '' +
                        '</option>'+
                        '<option>'+
                        'דני פישמן' +
                        '</option>'+
                        '<option>'+
                        'יואב שטיל' +
                        '</option>'+
                        '<option>'+
                        'טל בושרי' +
                        '</option>'+
                        '<option>'+
                        'עידן טל' +
                        '</option>'+
                        '<option>'+
                        'אלמוג והב' +
                        '</option>'+
                        '<option>'+
                        'דניאל בליטי' +
                        '</option>'+
                        '<option>'+
                        'מידד חינקיס' +
                        '</option>';
    
    $('#delivery-table thead th.filter').each( function () {
        var title = $('#delivery-table thead th').eq( $(this).index() ).text();
        $(this).html( '<input type="text" placeholder="'+title+'" />' );
    } );
    
    table = $('#delivery-table').DataTable( {
        "lengthMenu": [[-1], ["All"]],
        "bPaginate": false,
        "language": {
                "url": "//cdn.datatables.net/plug-ins/e9421181788/i18n/Hebrew.json"
            },
        "ajax": "/index.php?route=seller/catalog-seller/ajaxDelivery&date="+date,
        "columns": [
            { "render": function ( data, type, row ) {  return '<img src="images/plus.png" />'; } ,"sClass": "pressable"  , "bSortable": false},
            { "data": "cook_address_1", "sClass": "pressable" },
            { "data": "cook_order_time", "sClass": "pressable" },
            { "data": "customer_address_1", "sClass": "pressable"},
            { "data": "order_time", "sClass": "pressable" },
            { "data": "number_of_meals", "sClass": "pressable" },
            { "data": "driver_pickup" ,
              "render": function ( data, type, row ) {
                    return '<select class="driver-pickup-change" id="'+row.order_id+'">'+
                            '<option>'+
                            data +
                            '</option>'+
                            driver_list+
                            '</select>';
                }
            },
            { "data": "driver_deliver" ,
              "render": function ( data, type, row ) {
                    return '<select class="driver-deliver-change" id="'+row.order_id+'">'+
                            '<option>'+
                            data +
                            '</option>'+
                            driver_list+
                            '</select>';
                }
            },
            { "data": "status_text" , "visible": false },
            { "data": "status_text" ,
              "render": function ( data, type, row ) {
                
                    if (data == 1) {
                        name = "ממתין לאישור";
                    } else if (data == 5) {
                        name = "מאושר";
                    } else if (data == 7) {
                        name = "בוטל";
                    } else if (data == 17) {
                        name = "נאסף מהבשלן";
                    } else if (data == 18) {
                        name = "נמסר לסועד";
                    } else if (data == 19) {
                        name = "בדרך לבשלן";
                    } else if (data == 20) {
                        name = "בדרך לסועד";
                    } else if (data == 21) {
                        name = "ממתין למשלוח";
                    }
                    
                    var option = "";
                    
                    option = '<option value="'+data+'">'+
                            name +
                            '</option>'+
                            '<option value="21">'+
                            'ממתין למשלוח' +
                            '</option>'+
                            '<option value="19">'+
                            'בדרך לבשלן' +
                            '</option>'+
                            '<option value="17">'+
                            'נאסף מהבשלן' +
                            '</option>'+
                            '<option value="20">'+
                            'בדרך לסועד' +
                            '</option>'+
                            '<option value="18">'+
                            'נמסר לסועד' +
                            '</option>'+
                            '</select>';
                        
                    return '<select class="status-change" id="'+row.order_id+'">'+
                            option+
                            '</select>';
                }
            }
        ],
        "order": [[2, 'asc'],[3,'asc']],
        "initComplete": function(settings, json) {
            
            multi_select_status();
            
          }
    } );
     
    
     
    $("#delivery-table thead input").on('keyup change', function () {
        table
            .column( $(this).parent().index()+':visible' )
            .search( this.value )
            .draw();
    } );
    
    
     
    // Add event listener for opening and closing details
    $('#delivery-table tbody').on('click', 'td.pressable', function () {
        var tr = $(this).parents('tr');
        var row = table.row( tr );
 
        if ( row.child.isShown() ) {
            // This row is already open - close it
            row.child.hide();
            tr.removeClass('shown');
        }
        else {
            // Open this row
            row.child( format(row.data()) ).show();
            tr.addClass('shown');
            tr.next().addClass('showny');
        }
    } );
    
    $("#delivery-table tbody").on("change", "select.status-change" ,function() {
       
       if ($(this).val() != 5) {
            var status_data  = {
                order_id: $(this).attr("id"),
                status_id : $(this).val(),
            };
            
           $.ajax({
                type: 'POST',
                url: '/index.php?route=seller/catalog-seller/updateDeliverStatus',
                data: status_data,
                dataType: 'json',
                beforeSend: function() {
                    console.log("sending status...");
                },
                success: function(json) {
                    console.log(json);
                    //table.ajax.reload();
                }
            });
       }
       
    });
    
    $("#delivery-table tbody").on("change", "select.driver-pickup-change" ,function() {
        var driver_data  = {
            order_id: $(this).attr("id"),
            driver : $(this).val(),
            type : 1,
        };
        
       $.ajax({
            type: 'POST',
            url: '/index.php?route=seller/catalog-seller/updateDeliverStatus',
            data: driver_data,
            dataType: 'json',
            beforeSend: function() {
                console.log("sending driver...");
            },
            success: function(json) {
                console.log(json);
                //table.ajax.reload();
            }
        });
        
    });
    
    $("#delivery-table tbody").on("change", "select.driver-deliver-change" ,function() {
        var driver_data  = {
            order_id: $(this).attr("id"),
            driver : $(this).val(),
            type : 0,
        };
        
       $.ajax({
            type: 'POST',
            url: '/index.php?route=seller/catalog-seller/updateDeliverStatus',
            data: driver_data,
            dataType: 'json',
            beforeSend: function() {
                console.log("sending driver...");
            },
            success: function(json) {
                console.log(json);
                //table.ajax.reload();
            }
        });
        
    });
    
    // autorefresh
    setInterval( function () {
        table.ajax.reload();
    }, 1000*60*1 );

} );
</script>
    
</body>
</html>
