<?php setlocale(LC_ALL, 'he_IL.UTF-8');?>
<!DOCTYPE html>
<html>
<head>
    <link rel="stylesheet" type="text/css" href="css/main.css">
    <link rel="stylesheet" type="text/css" href="css/chosen.css">
    <link rel="stylesheet" type="text/css" href="http://cdn.jsdelivr.net/qtip2/2.2.1/basic/jquery.qtip.min.css">
    <script type="text/javascript" language="javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
    <script type="text/javascript" language="javascript" src="//cdnjs.cloudflare.com/ajax/libs/jQuery-slimScroll/1.3.1/jquery.slimscroll.min.js"></script>
    <script type="text/javascript" language="javascript" src="http://cdn.jsdelivr.net/qtip2/2.2.1/jquery.qtip.js" ></script>
    <script type="text/javascript" language="javascript" src="js/all.min.js" ></script>
    <title>ניהול מסלולי שליחים</title>
</head>
<body>
<div class="loading-screen" id="confirmLoader"></div>
<div id="plan-header">
    <div id="legend">
        <div class="key">
            <div id="legend-cook"><span>בשלן</span></div>
        </div>
        <div class="key">
            <div id="legend-onway-cook"><span>בדרך לבשלן</span></div>
        </div>
        <div class="key">
            <div id="legend-pickedup"><span>נאסף מבשלן</span></div>
        </div>

        <div class="key">
            <div id="legend-dinner"><span>סועד</span></div>
        </div>
        <div class="key">
            <div id="legend-onway-dinner"><span>בדרך לסועד</span></div>
        </div>
        <div class="key">
            <div id="legend-delivered"><span>נמסר לסעוד</span></div>
        </div>
    </div>
    <div id="homeals-logo-right">
        <img src="images/homeals_logo.png" />
    </div>
    <div id="plan-title">ניהול מסלולי שליחים</div>
    <div style="clear: both"></div>
</div><!-- plan-header -->
    
<div id="plan-main-con">
    
    <div class="left-side">
        <div class="inner-left-side">
            
            <div class="time-scale">
                <div class="driver-header">
                    זמנים
                </div>
                <div class='time-window'>
                    <?php for ($i=0;$i < 25;$i++) { ?>
                    <li><div class="time-slot">
                            <?php
                                $min = 0;
                                $min = $i*15;
                                $date = date("H:i",strtotime("09:30 +".$min." minutes"));
                                echo $date;
                            ?>
                    </div></li>
                    <?php } ?>
                </div>
            </div><!-- time-scale -->
            <?php for ($j=1;$j < 3;$j++) { ?>
            <div class="driver-con">
                
                <div class="vertical-line"></div>
                
                <div class="driver-header">
                    
                    <select class="driver-select" id="driver[<?php echo $j ?>][name]">
                        <option><?php echo "שליח ".$j ?></option>
                        <option>דני פישמן</option>
                        <option>יואב שטיל</option>
                        <option>טל בושרי</option>
                        <option>עידן טל</option>
                        <option>אלמוג והב</option>
                        <option>דניאל בליטי</option>
                        <option>מידד חינקיס</option>
                    </select>
                    
                </div>
                
                <?php for ($i=0;$i < 25;$i++) { ?>
                <div class="add-ol">+</div>
                <ol class="time-window multi-slot" id="driver-<?php echo $j.$i ?>"></ol>
                <div class="remove-ol">-</div>
                <?php } ?>
                
            </div><!-- driver-con -->
            <?php } ?>
            
            <div class="time-scale">
                <div class="driver-header">
                    זמנים
                </div>
                <div class='time-window'>
                    <?php for ($i=0;$i < 25;$i++) { ?>
                    <li><div class="time-slot">
                            <?php
                                $min = 0;
                                $min = $i*15;
                                $date = date("H:i",strtotime("09:30 +".$min." minutes"));
                                echo $date;
                            ?>
                    </div></li>
                    <?php } ?>
                </div>
            </div><!-- time-scale -->
            
        </div><!-- inner-left-side -->
    </div><!-- left-side -->
    
    
    
    <?php
    
    /****************************
     *
     *  right side where all the data will be set
     *
     ***************************/
    
    ?>
    
    <div class="right-side">
        <div class="inner-right-side">
            
            <div id="plan-change-date">
                <a href="?date=<?php echo date("Y-m-d",strtotime($_GET['date']." + 1 day")); ?>"><</a>
                <?php echo strftime("%A, %e ב%B",strtotime($_GET['date'])); ?>
                <a href="?date=<?php echo date("Y-m-d",strtotime($_GET['date']." - 1 day")); ?>">></a>
            </div>
            
        </div><!-- inner-right-side -->
    </div><!-- right-side -->
    
</div><!-- plan-main-con -->
    
<script>
$(".loading-screen").show();

var date = "<?php echo $_GET['date']; ?>";
var debug_data;
var changed = false;

$(document).ready(function() {
    $.ajax({
            type: 'POST',
            url: "/index.php?route=seller/catalog-seller/ajaxDelivery&date="+date,
            dataType: 'json',
            beforeSend: function() {
                //console.log("getting info...");
            },
            complete: function() {
                setInterval(function(){
                    if (changed) {
                        save();
                        changed = false;
                    }
                }, 3000);
            },
            success: function(json) {
                // updating data
                updateData(json);
            }
        });
    
    $('.inner-left-side').slimScroll({
        height: '100%'
    });
    
    $('.inner-right-side').slimScroll({
        height: '100%'
    });
    
});

function makeitdrag() {
    $("ol.time-window").sortable({
        group: 'time-window',
        placeholder: '<li class="placeholder"/>',
        pullPlaceholder: true,
        onDrop: function  (item, targetContainer, _super) {
            
            if($(targetContainer.target).hasClass("drop-window") || $(targetContainer.target).hasClass("pickup-window")){
                if (($(targetContainer.target).attr("data-id") == $(item).attr("data-id"))) {
                    $(targetContainer.target).empty();
                    $(targetContainer.target).append($(item));
                    
                    _super(item)
                } else {
                    $("li[data-id='"+item.attr("data-id")+"'].lidisable").find("div.time-slot.disabled").removeClass("disabled");
                    $("li[data-id='"+item.attr("data-id")+"'].lidisable").removeClass("lidisable");
                    
                    item.remove();
                    $("body").removeClass("dragging");
                } 
            } else {
                _super(item)
            }
            
            $("li").show();
            
            checkMultiSlot();
            
            $('.inner-left-side').slimScroll({
                height: '100%'
            });
            
            changed = true;
            
            setTimeout(function() {makeItTip();},200);
        },
        onCancel: function ($item, container, _super, event) {
            //console.log("here");
            _super($item)
        },
        onDragStart: function (item, container, _super) {
            $('div.qtip:visible').qtip('hide');
            
            var offset = item.offset(),
            pointer = container.rootGroup.pointer
            
            adjustment = {
                left: pointer.left - offset.left,
                top: pointer.top - offset.top
            }
            
            if ($(container.target).hasClass("drop-window") || $(container.target).hasClass("pickup-window")) {
                if (!$(container.target).find("li").first().find("div").hasClass("disabled")){
                    // clones items
                    var clonedItem = item.clone();
                    clonedItem.addClass("lidisable");
                    clonedItem.find(".time-slot").addClass("disabled");
                    item.before(clonedItem);
                }
            }
            
            _super(item, container)
        },
        onDrag: function (item, position) {
            item.css({
                left: position.left - adjustment.left,
                top: position.top - adjustment.top
            });
        },
        onMousedown: function (item, _super) {
            if (item.find(".disabled").length) {
                return false;
            }
            
            return true;
        },
        isValidTarget: function  (item, container) {
            
            var item_class;
            var item_address;
            
            if($(item).find('div.drop-slot').length){
                item_class = "drop-slot";
            } else {
                item_class = "pickup-slot";
            }
            
            if($(container.target).hasClass("multi-slot") && $($(container.items[0]).find('div.time-slot')[0]).hasClass(item_class)){
                if ($(item[0].children[1]).find('.address').text() == $(container.items[0].children[1]).find('.address').text()) {
                    item_address = true;
                } else{
                    item_address = false;
                }
            }
            
            if ( ( $(container.target).hasClass("drop-window") || $(container.target).hasClass("pickup-window") ) /* && ($(container.target).attr("data-id") == $(item).attr("data-id")) */ ) {
                if (($(container.target).attr("data-id") == $(item).attr("data-id"))) {
                    $(container.target).find("li.lidisable").hide();
                } else {
                   $("li").show();
                }
                
                return true
            } else if($(container.target).hasClass("single-slot") && (container.items.length < 1)){
                return true
            } else if($(container.target).hasClass("multi-slot") && (container.items.length == 0) ){
                return true
            } else if( item_address ){
                return true
            } else {
                return item.parent("ol")[0] == container.el[0]
            }
            
        },
        tolerance: 0,
        distance: 0,
        delay: 50,
    });
}

function updateData(json) {
    //console.log(json);
    var data = {};
    data.pickup = {};
    data.drop = {};
    
    // sorting data for easy use
    $.each(json.data, function( index, order ) {
        // pick up orders
        if (!(order.cook_order_time in data.pickup))
            data.pickup[order.cook_order_time] = [];
            
        data.pickup[order.cook_order_time].push(order);
        // drop orders
        if (!(order.order_time in data.drop))
            data.drop[order.order_time] = [];
            
        data.drop[order.order_time].push(order);
    });
    
    debug_data = data.pickup;
    // pick-up html
    buildPickup(sortObj(data.pickup));
    // drop html
    buildDrop(sortObj(data.drop));
    
    load(redraw_map);
}

function sortObj(obj, type, caseSensitive) {
  var temp_array = [];
  for (var key in obj) {
    if (obj.hasOwnProperty(key)) {
      if (!caseSensitive) {
        key = (key['toLowerCase'] ? key.toLowerCase() : key);
      }
      temp_array.push(key);
    }
  }
  if (typeof type === 'function') {
    temp_array.sort(type);
  } else if (type === 'value') {
    temp_array.sort(function(a,b) {
      var x = obj[a];
      var y = obj[b];
      if (!caseSensitive) {
        x = (x['toLowerCase'] ? x.toLowerCase() : x);
        y = (y['toLowerCase'] ? y.toLowerCase() : y);
      }
      return ((x < y) ? -1 : ((x > y) ? 1 : 0));
    });
  } else {
    temp_array.sort();
  }
  var temp_obj = {};
  for (var i=0; i<temp_array.length; i++) {
    temp_obj[temp_array[i]] = obj[temp_array[i]];
  }
  return temp_obj;
}

function buildPickup(data) {

    // pick-up html
    var pickup_html = '<div class="pickup-con">';
    
    $.each(data, function ( pickup_time , orders ) {
        // con and header
        pickup_html += '<div class="driver-header">';
        pickup_html += 'איסוף '+pickup_time;
        pickup_html += '</div>';
        // list items
        
        $.each(orders, function ( index , order ) {
            pickup_html += '<ol class="time-window pickup-window" data-id="pickup-' + order.order_id + '">';
            pickup_html += '<li data-id="pickup-'+ order.order_id +'" data-num="'+ order.number_of_meals +'"><div class="time-slot pickup-slot tooltip">';
            pickup_html += '<div class="street-name">';
            pickup_html += order.cook_address_1;
            //pickup_html += '<span class="time-window" style="float:left">' + order.order_id + '</span>';
            pickup_html += '</div>';
            pickup_html += '<div class="num-of-meals">';
            pickup_html += '<span class="num">' + order.number_of_meals + '</span>';
            pickup_html += '<span class="time-window">איסוף ' + pickup_time + '</span>';
            pickup_html += '</div>';
            pickup_html += '</div>';
            pickup_html += '<div class="extra-data">';
            pickup_html += '<div class="extra-data-status-con"><strong>שינוי סטאטוס הזמנה:</strong>';
            pickup_html += '<select class="status-select" id="status-' + order.order_id + '">';
            pickup_html += '<option value="5">מאושר</option>';
            pickup_html += '<option value="21">ממתין למשלוח</option>';
            pickup_html += '<option value="19">בדרך לבשלן</option>';
            pickup_html += '<option value="17">נאסף מהבשלן</option>';
            pickup_html += '<option value="20">בדרך לסועד</option>';
            pickup_html += '<option value="18" >נמסר לסועד</option>';
            pickup_html += '</select>';
            pickup_html += '</div>';
            pickup_html += '<div class="extra-data-address-con">';
            pickup_html += '<div class="title"> פרטי בשלן </div>';
            pickup_html += '<div class="phone"><strong>טלפון:</strong> ' + order.cook_phone + '</div>';
            pickup_html += '<div class="name"><strong>שם:</strong> ' + order.cook_name + '</div>';
            pickup_html += '<div class="address"><strong>כתובת:</strong></div>';
            pickup_html += '<div class="address"><a href="https://maps.google.co.il/maps?q=תל אביב '+order.cook_address_1.replace('#','')+'" target="_blank">' + order.cook_address_1 + "</a>, " + order.cook_address_2 + '</div>';
            pickup_html += '<div class="comment"><strong>הערות לשליח:</strong> ' + order.order_comment + '</div>';
            pickup_html += '</div>';
            pickup_html += '<div class="order-more">';
            pickup_html += '<div class="order-id"><strong>הזמנה:</strong> ' + order.order_id + '</div>';
            pickup_html += '<div class="order-meal-num"><strong>ארוחות:</strong> ' + order.number_of_meals + '</div>';
            pickup_html += '<div class="order-address"><strong>כתובת:</strong><a href="https://maps.google.co.il/maps?q=תל אביב '+order.customer_address_1.replace('#','')+'" target="_blank">' + order.customer_address_1 + "</a>, " + order.customer_address_2 + '</div>';
            pickup_html += '</div>'
            pickup_html += '</div></li></ol>';
        });
    });
    pickup_html += '</div><!-- pickup-con -->';
    //appending data
    $("#plan-change-date").after(pickup_html);
}

function buildDrop(data) {
    var time_index = 0;

    // drop html
    var drop_html = '<div class="drop-con">';
    
    $.each(data, function ( drop_time , orders ) {
        // con and header
        drop_html += '<div class="driver-header">';
        drop_html += 'הפצה '+drop_time;
        drop_html += '</div>';
        // list items
        
        $.each(orders, function ( index , order ) {
            drop_html += '<ol class="time-window drop-window" data-id="drop-' + order.order_id + '">';
            drop_html += '<li data-id="drop-'+ order.order_id +'" data-num="'+ order.number_of_meals +'"><div class="time-slot drop-slot tooltip">';
            drop_html += '<div class="street-name">';
            drop_html += order.customer_address_1;
            //drop_html += '<span class="time-window" style="float:left">' + order.order_id + '</span>';
            drop_html += '</div>';
            drop_html += '<div class="num-of-meals">';
            drop_html += '<span class="num">' + order.number_of_meals + '</span>';
            drop_html += '<span class="time-window">הפצה ' + drop_time + '</span>';
            drop_html += '</div>';
            drop_html += '</div>';
            drop_html += '<div class="extra-data">';
            drop_html += '<div class="extra-data-status-con"><strong>שינוי סטאטוס הזמנה:</strong>';
            drop_html += '<select class="status-select" id="status-' + order.order_id + '">';
            drop_html += '<option value="5">מאושר</option>';
            drop_html += '<option value="21">ממתין למשלוח</option>';
            drop_html += '<option value="19">בדרך לבשלן</option>';
            drop_html += '<option value="17">נאסף מהבשלן</option>';
            drop_html += '<option value="20">בדרך לסועד</option>';
            drop_html += '<option value="18" >נמסר לסועד</option>';
            drop_html += '</select>';
            drop_html += '</div>';
            drop_html += '<div class="extra-data-address-con">';
            drop_html += '<div class="title"> פרטי סועד </div>';
            drop_html += '<div class="phone"><strong>טלפון:</strong> ' + order.cook_phone + '</div>';
            drop_html += '<div class="name"><strong>שם:</strong> ' + order.customer_contact + '</div>';
            drop_html += '<div class="address"><strong>כתובת:</strong></div>';
            drop_html += '<div class="address"><a href="https://maps.google.co.il/maps?q=תל אביב '+order.customer_address_1.replace('#','')+'" target="_blank">' + order.customer_address_1 + "</a>, " + order.customer_address_2 + '</div>';
            drop_html += '<div class="comment"><strong>הערות לשליח:</strong> ' + order.order_comment + '</div>';
            drop_html += '</div>';
            drop_html += '<div class="order-more">';
            drop_html += '<div class="order-id"><strong>הזמנה:</strong> ' + order.order_id + '</div>';
            drop_html += '<div class="order-meal-num"><strong>ארוחות:</strong> ' + order.number_of_meals + '</div>';
            drop_html += '<div class="order-address"><strong>כתובת:</strong><a href="https://maps.google.co.il/maps?q=תל אביב '+order.cook_address_1.replace('#','')+'" target="_blank">' + order.cook_address_1 + "</a>, " + order.cook_address_2 + '</div>';
            drop_html += '</div>'
            drop_html += '</div></li></ol>';
        });
    });
    drop_html += '</div><!-- drop-con -->';
    //appending data
    $(".pickup-con").after(drop_html);
}

function makeItTip() {
    $('div.qtip:visible').qtip('hide');
    var content
    $('.tooltip').each(function() { // Notice the .each() loop, discussed below
        
        content = $(this).next('div').clone();
        
        $(this).qtip({
            overwrite: true,
            content: {
                text: content // Use the "div" element next to this for the content
            },
            position: {
                //target: 'mouse', // Track the mouse as the positioning target
                viewport: $(window),               
                adjust: { x: -540, y: -70 }, // Offset it slightly from under the mouse
                
            },
            show: {
                event: 'click',
                delay: 0,
                effect: function() {
                    $(this).fadeTo(250, 1);
                }
            },
            events: {
                show: function(event, api) {
                    $(this).find('.extra-data').slimScroll({
                        height: '100%'
                    });
                }
            },
            hide: {
                event: 'mouseleave',
                delay: 100,
                fixed: true,
                effect: function() {
                    $(this).fadeTo(250, 0);
                }
            }
        });
    });
}

function checkMultiSlot() {
    $(".order-more.extra").remove();
    
    $('ol.multi-slot').each(function() {
        if (this.children.length >= 1) {
            
            $(this).next().hide();
            
            var meals_num_text = '';
            var meals_extra_text = [];
            
            var len = $(this.children).length;
            $(this.children).each(function(index, element) {
                if (index <= 1) {
                    meals_num_text += $(this).attr("data-num");
                    //code
                    if (index != len - 1 && index != 1) {
                        meals_num_text += '+';
                    }
                } else if (index == 2){
                    meals_num_text +='...';
                }
                if (index){
                    meals_extra_text[index] = $(this).find(".order-more").clone().addClass("extra");
                }
            });
            
            $(this.children[0]).find(".extra-data").append(meals_extra_text);
            
            $(this.children).each(function(index, element) {
                $(this).find(".num").html(meals_num_text);
            });
        } else {
            $(this).next().show();
        }
    });
}

function addOl() {
    
    $("ol.time-window").sortable("destroy");
    
    $(this).before('<div class="add-ol">+</div><ol class="time-window multi-slot"></ol><div class="remove-ol">-</div>');
    
    makeitdrag();
    
    $('.add-ol').off();
    $('.remove-ol').off();
    
    $('.add-ol').on("click",addOl);
    $('.remove-ol').on("click",removeOl);
    
    changed = true;
}

function removeOl() {
    $(this).prev("ol").remove();
    $(this).prev("div").remove();
    $(this).remove();
    
    changed = true;
}

function save() {
    var data_to_save = {};
    var json_data = '';

    $(".driver-con").each( function ( index , data ) {
        var slots = {};
        $(data).find(".time-window.multi-slot").each(function ( index_i , data_i ) {
            var orders = [];
            $(data_i).find("li").each(function ( index_ii , data_ii ) {
                orders[index_ii] = $(data_ii).attr("data-id");
            });
           slots[index_i] = orders;
        });
        data_to_save[index] = slots;
    });
    
    // data we are going to save to database
    json_data = JSON.stringify(data_to_save);
    
    $.ajax({
        type: 'POST',
        url: '/index.php?route=seller/catalog-seller/ajaxDeliverySave',
        data: {'date' : date , 'deliver_map' : json_data , 'data_to_save' : data_to_save },
        dataType: 'json',
        beforeSend: function() {
            //console.log("saving data...");
            $('body').append('<span class="msg">saving data...</span>');
        },
        complete: function() {
            //console.log("done");
            //$('body').append('<span class="msg">done</span>');
            setTimeout(function() {
                $('.msg').fadeOut();
            },1000);
            
        },
        success: function(json) {
            if (json.data == true) {
                console.log("data saved");
                $('.msg').fadeOut();
                $('body').append('<span class="msg">data saved</span>');
            } else {
                console.log("error saving data");
                $('body').append('<span class="msg">error saving data</span>');
            }
        }
    });
}

function load(callback) {
    $.ajax({
        type: 'POST',
        url: '/index.php?route=seller/catalog-seller/ajaxDeliveryLoad',
        data: {'date' : date },
        dataType: 'json',
        beforeSend: function() {
            // do nothing
            $(".loading-screen").show();
        },
        complete: function() {            
            $(".loading-screen").fadeOut();
        },
        success: function(json) {
            // map_data = JSON.parse(json.deliver_map);
            
            // update the data
            if (json.error) {
                // do nothing now
                newInis();
            } else {
                callback(JSON.parse(json.deliver_map));
            }
            
        }
    });
}

function newInis(){
    $(".driver-select").chosen();
    $('.add-ol').on("click",addOl);
    $('.remove-ol').on("click",removeOl);
    
    makeItTip();
    checkMultiSlot();
    makeitdrag();
}

function redraw_map(map_data) {
    
    $("ol.time-window").sortable("destroy");
    // cancell all disables first
    $(".driver-con").remove();
    
    // then we redraw based on the map
    var html = '';
    
    $.each(map_data , function( driver_col_num , driver_map ) {
        
        
        html += '<div class="driver-con" id="driver-'+ driver_col_num +'">';
        
        html += '<div class="vertical-line"></div>';
        html += '<div class="driver-header">';
        html += '            <select class="driver-select" id="driver['+ (driver_col_num) +'][name]">';
        html += '                <option><?php echo "שליח "; ?>'+ (driver_col_num) +'</option>';
        html += '                <option>דני פישמן</option>';
        html += '                <option>יואב שטיל</option>';
        html += '                <option>טל בושרי</option>';
        html += '                <option>עידן טל</option>';
        html += '                <option>אלמוג והב</option>';
        html += '                <option>דניאל בליטי</option>';
        html += '                <option>מידד חינקיס</option>';
        html += '            </select>';
        html += '</div>';
        
        $.each( driver_map , function( key , order_list ) {
            html += '<div class="add-ol">+</div>';
            html += '<ol class="time-window multi-slot" id="driver-'+(driver_col_num+1)+key+'">';
            $.each( order_list , function( order_key , order_slot_id ) {
                html += $('<div>').append($("li[data-id='"+ order_slot_id +"']").first().clone()).html();
                $("li[data-id='"+ order_slot_id +"']").addClass('lidisable');
                $("li[data-id='"+ order_slot_id +"']").find('.time-slot').addClass('disabled');
            });
            html += '</ol>';
            html += '<div class="remove-ol">-</div>';
        });
        html += '</div>';
        
    });
    $(".time-scale").first().after(html);
    
    // redraw sequence
    
    $(".driver-select").chosen();
    
    $('.add-ol').on("click",addOl);
    $('.remove-ol').on("click",removeOl);
    
    makeItTip();
    checkMultiSlot();
    makeitdrag();
    
}
</script>
    
</body>
</html>
