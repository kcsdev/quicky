<?php

// vars
$site_url = "http://homeals.com";
$intro_img = "images/intro_img.png";
$cuopon_name = isset($_REQUEST['cop']) ? $_REQUEST['cop'] : "";
$cuopon_url = ($cuopon_name != "") ? "http://homeals.com/index.php?route=common/home&cpcod=".$cuopon_name : "http://homeals.com/index.php?route=common/home";
$comp_name = isset($_REQUEST['com']) ? $_REQUEST['com'] : "אגד";
$comp_code = isset($_REQUEST['cod']) ? $_REQUEST['cod'] : "12345";
$color_code = isset($_REQUEST['clr']) ? $_REQUEST['clr'] : "dc3d3d";
?>

<!DOCTYPE html>

<html>
<head>
    <meta charset="UTF-8" />
    <title><?php echo $title; ?></title>
    <meta http-equiv="Content-type" content="text/html; charset=utf-8" />
    <link rel="stylesheet" type="text/css" href="css/style.css" />
    
    <script>
      (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
      (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
      m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
      })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
        
      ga('create', 'UA-48957712-1', 'homeals.com');
      ga('send', 'pageview');
    </script>
    
</head>
<body>

    <div class="page-con">
        <div class="logo-con">
           <a href="<?php echo $site_url; ?>" target="_blank" ><img src="images/logo.png" /></a>
        </div>
        <div class="msg-con">
           
            <div class="msg-image">
                <img src="<?php echo $intro_img; ?>" />
            </div>
            
            <div class="msg-body">
                <div class="msg-left">
                    <div class="inner">
                        <h3>
                            הטבה לעובדי <?php echo $comp_name; ?> לרגל ההשקה
                        </h3>
                        <div class="text-box" style="color: #<?php echo $color_code; ?>; border-color: #<?php echo $color_code; ?>">
                            <span id="line1" >משלוח</span>
                            <span id="line2" >חינם</span>
                            <span id="line3" >למשרד</span>
                        </div>
                        <h3>
                            במהלך כל חודש יולי
                        </h3>
                        <span>
                            (ללא מינימום הזמנה)
                        </span>
                        
                        
                        
                    </div>
                    
                    <div class="code">
                        בעת ביצוע ההזמנה הקלידו <span class="codeitself" style="color: #<?php echo $color_code; ?>;"><?php echo $comp_code; ?></span> בשדה קוד קופון
                    </div>
                    
                    <div class="btn">
                        <a href="<?php echo $cuopon_url; ?>">למימוש ההטבה</a>
                    </div>
                    
                    
                    
                    <div class="shadow"></div>
                </div>
                
                <div class="msg-right">
                    <div id="intro"> 
                        <h1>
                            הגיע הזמן לאכול בריא יותר, גם במשרד
                        </h1>
                        
                        <p>
                            מהיום אתם יכולים ליהנות במשרד שלכם מארוחות איכותיות וטעימות שהוכנו על ידי שפים ובשלנים, בביתם.
                        </p>
                        
                        <p>
                            באתר שלנו תמצאו מגוון ארוחות ייחודיות המשתנות מדי יום: קציצות, תבשילי קדרה, מנות צמחוניות וטבעוניות, תוספות עשירות ועוד...
                        </p>
                        
                        <p>
                            הבשלנים והשפים שלנו מקפידים על שימוש בחומרי גלם טבעיים בלבד באיכות גבוהה, כדי שתיהנו מאוכל טרי ומזין מדי יום.
                        </p>
                        
                        <p class="red" style="color: #<?php echo $color_code; ?>" >
                            מהרו להזמין. בכל יום כמות מוגבלת מכל ארוחה.
                        </p>
                    </div>
                    
                    <div id="list"> 
                        <ul>
                            <li>ממשק פשוט ונוח להזמנה מהירה</li>
                            <li>רשימת מרכיבים מפורטת לכל מנה</li>
                            <li>הוספת הערות ובקשות אישיות מהבשלנים</li>
                            <li>אפשרות להזמין מראש ולקבל בזמן שנוח לכם</li>
                            <li>סליקה מאובטחת באתר</li>
                        </ul>
                    </div>
                    
                    <div id="find-out-more"> 
                        <h2>
                            אז מה זה בעצם Homeals?
                        </h2>
                        
                        <p>
                           Homeals היא פלטפורמה דיגיטלית ושירות המאפשר לסועדים להזמין ארוחות ייחודיות מבשלנים ושפים מוכשרים המבשלים במטבח הביתי.
באמצעות האתר סועדים יכולים ללמוד על הבשלנים, ליצור קשר ולהזמין באופן פשוט ומהיר ארוחות טעימות לצהריים ולערב ישירות לבית או למשרד.
                        </p>
                        
                        <a class="red" style="color: #<?php echo $color_code; ?>" id="read-more" href="<?php echo $site_url; ?>" target="_blank">
                            קראו עוד...
                        </a>
                    </div>
                </div>
                
                <div style="clear: both">
            </div>
            
        </div>
    </div>
        
        
    <div id="footer-main">
        <div id="footer-main-logo">
            <img src="image/data/greylogo.png">
        </div>
        <div style="clear: both"></div>
        <div id="footer-main-links">
            <ul>
                <li><a href="http://dev2.homeals.co.il/index.php?route=information/information&amp;information_id=4" target="_blank">אודותינו</a></li>
                <li><a href="http://dev2.homeals.co.il/index.php?route=information/information&amp;information_id=7" target="_blank">איך זה עובד</a></li>
                <li><a href="http://dev2.homeals.co.il/index.php?route=information/information&amp;information_id=8" target="_blank">אמון ובטיחות</a></li>
                <li><a href="http://dev2.homeals.co.il/index.php?route=information/information&amp;information_id=10" target="_blank">שאלות ותשובות</a></li>
                <li><a href="http://dev2.homeals.co.il/index.php?route=information/information&amp;information_id=9" target="_blank">הצטרפו כבשלנים</a></li>
            </ul>
            <div style="clear: both"></div>
        </div>
        <div style="clear: both"></div>
        <div id="footer-main-followus">
            <div id="followus-title">עקבו אחרינו</div>
            <div id="followus-body">
                <a href="https://www.facebook.com/pages/Homeals-Israel/608776639200255" target="blank">
                        <div id="facebook-follow"></div>
                </a>
                <a href="http://instagram.com/homealsisrael" target="blank">
                        <div id="insta-fllow"></div>
                </a>
                <a href="http://saloona.co.il/homeals/" target="blank">
                        <div id="saloona-fllow"></div>
                </a>
                <div style="clear: both"></div>
            </div>
        </div>
        <div style="clear: both"></div>
    </div>
 
</body>
</html>
