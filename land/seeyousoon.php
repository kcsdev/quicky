<?php

// save mail in list

if ($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST['email'])) {



    $mysqli = new mysqli("localhost", "root", "reloaded100", "homeals_site");

    /* check connection */
    if (mysqli_connect_errno()) {
        printf(json_encode("Connect failed: %s\n", mysqli_connect_error()));
        exit();
    }

    $query = "INSERT INTO `homeals_site`.`land_pivot` (`id`, `email`, `ip`) VALUES (NULL, '". $_POST['email'] ."', '". $_SERVER['REMOTE_ADDR'] ."')";

    $mysqli->query($query);

    printf (json_encode("New Record has id ".$mysqli->insert_id));

    /* close connection */
    $mysqli->close();

    //print_r(json_encode("success"));

} else {

?>

<!DOCTYPE html>

<html>
<head>
    <meta charset="UTF-8" />
    <title>Homeals</title>
    <meta http-equiv="Content-type" content="text/html; charset=utf-8" />
    <link rel="stylesheet" type="text/css" href="land/css/style.css" />
    <link rel="stylesheet" type="text/css" href="catalog/view/theme/OPC040088/stylesheet/colorbox.css" />

    <script type="text/javascript" src="https://code.jquery.com/jquery-1.11.1.min.js"></script>
    <script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/jquery-browser/0.0.6/jquery.browser.min.js"></script>
    <script type="text/javascript" src="catalog/view/javascript/jquery.colorbox-min.js"></script>



    <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
            (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

        ga('create', 'UA-48957712-1', 'homeals.com');
        ga('send', 'pageview');
    </script>

</head>
<body>
<div class="loading-screen" id="confirmLoader" ></div>

<div class="page-con">
    <div class="logo-con">
        <img src="land/images/logo2.png" />
    </div>
    <div class="msg-con">
        <h2>
            סועדים יקרים
        </h2>

        <p>
            אנחנו כרגע במטבח מבשלים עבורכם דברים חדשים <br>
            בכדי שתוכלו להמשיך וליהנות מאוכל טעים, בריא ואיכותי.
        </p>

        <p class="red" style="color: #<?php echo $color_code; ?>" >
            פרטים נוספים בקרוב...
        </p>



        <form name="login" action="" method="post">
            <div class="signup-input-div">
                <input type="email" id="email" name="email" class="email" placeholder="כתובת אימייל" required>
            </div>

            <div class="clear">
                <input type="submit" value="עדכנו אותי" name="subscribe" id="submit-btn" class="button">
            </div>

            <div class="no-spam">
                אנו מתחייבים לא לשולח הודעות ספאם
            </div>

        </form>


    </div>


    <div style="text-align: center; margin-top: 50px;">
        <a style="font-size: 14px;" href="mailto:info@homeals.com" >info@homeals.com</a>
    </div>

</div>

<script>//<!--
    $(document).ready(function() {

        $("#submit-btn").on("click", function(event) {
            event.preventDefault();

            //code here
            var email = $("#email").val();

            if (validateEmail(email)) {
                $.ajax({
                    type: "POST",
                    url: "land/seeyousoon.php",
                    data: { "email": email },
                    dataType: "json",
                    cache: false,
                    beforeSend : function() {
                        //showLoad();
                    },
                    complete: function() {
                        //hideLoad();
                    }
                }).done(function( msg ) {
                    console.log(msg);
                    alert("תודה על התעניותך נעדכן אותך בהמשך");
                });
            } else {
                alert("כתובת המייל אינה תקינה");
            }
        });

    });

    function validateEmail(email) {
        var filter = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
        if (filter.test(email)) {
            return true;
        } else {
            return false;
        }
    }

    //--></script>


</body>
</html>

<?php } ?>