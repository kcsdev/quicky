<?php

// vars
$site_url = "http://homeals.com";
$intro_img = "images/intro_img.png";
$coupon_name = isset($_REQUEST['cop']) ? $_REQUEST['cop'] : "";
$coupon_url = ($coupon_name != "") ? "http://homeals.com/index.php?route=common/home&cpcod=".$coupon_name : "http://homeals.com/index.php?route=common/home";
$comp_name = isset($_REQUEST['com']) ? $_REQUEST['com'] : "";
$coupon_code = isset($_REQUEST['cod']) ? $_REQUEST['cod'] : "1010";
$color_code = isset($_REQUEST['clr']) ? $_REQUEST['clr'] : "dc3d3d";
$c_group_id = isset($_REQUEST['cgrp']) ? $_REQUEST['cgrp'] : "3";


?>

<!DOCTYPE html>

<html>
<head>
    <meta charset="UTF-8" />
    <title><?php echo $title; ?></title>
    <meta http-equiv="Content-type" content="text/html; charset=utf-8" />
    <link rel="stylesheet" type="text/css" href="css/style.css" />
    <link rel="stylesheet" type="text/css" href="../catalog/view/theme/OPC040088/stylesheet/colorbox.css" />
    
    <script type="text/javascript" src="https://code.jquery.com/jquery-1.11.1.min.js"></script>
    <script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/jquery-browser/0.0.6/jquery.browser.min.js"></script>
    <script type="text/javascript" src="../catalog/view/javascript/jquery.colorbox-min.js"></script>
    
    
    
    <script>
      (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
      (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
      m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
      })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
        
      ga('create', 'UA-48957712-1', 'homeals.com');
      ga('send', 'pageview');
    </script>
    
</head>
<body>
    <div class="loading-screen" id="confirmLoader" ></div>

    <div class="page-con">
        <div class="logo-con">
           <a href="<?php echo $site_url; ?>" target="_blank" ><img src="images/logo.png" /></a>
        </div>
        <div class="msg-con">
           
            <div class="msg-image">
                <img src="<?php echo $intro_img; ?>" />
            </div>
            
            <div class="msg-body">
                <div class="msg-left">
                    <div class="inner">
                        <h3>
                            הטבה לעובדי <?php echo $comp_name; ?>
                        </h3>
                        <div class="text-box" style="color: #<?php echo $color_code; ?>; border-color: #<?php echo $color_code; ?>">
                            
                            <span id="line1" >משלוח</span>
                            <span id="line2" >חינם</span>
                            <span id="line3" >למשרד</span>
                            
                            <span class="smalltext">בכל הזמנה, ללא מינימום</span>
                            
                        </div>
                    </div>
                    
                    
                    <div id="signup-div">
                    <form name="login" action="" method="post">
                        
                        <div class="signup-input-div">
                            <label for="email" class="label">הרשמו לקבלת ההטבה</label>
                            <input type="email" id="email" name="email" class="email" placeholder="כתובת אימייל" required>
                            <input type="text" id="firstname" name="first" class="email" placeholder="שם פרטי" >
                            <input type="text" id="lastname" name="last" class="email" placeholder="שם משפחה" >
                            <input type="password" id="password" name="password" class="email" placeholder="בחרו סיסמה" >
                            
                            <input type="hidden" id="city" name="city" value="תל אביב" >  
                            <input type="hidden" id="coupon_code" name="coupon_code" value="<?php echo $coupon_code; ?>" >
                            <input type="hidden" id="customer_group_id" name="customer_group_id" value="<?php echo $c_group_id; ?>" >
                            
                        </div>
                        
                        <div class="clear">
                            <input type="submit" value="הרשמה" name="subscribe" id="submit-btn" class="button">
                        </div>
                        <div class="iagree">
                            בהרשמה לשירות, אני מסכים ל<a href="http://homeals.com/index.php?route=information/information&amp;information_id=5" target="_blank">תנאי השימוש</a> ו<a href="http://homeals.com/index.php?route=information/information&amp;information_id=3" target="_blank">מדיניות הפרטיות</a> של Homeals.
                        </div>
                        
                    </form>
                    </div>
                    
                    <div class="shadow"></div>
                </div>
                
                <div class="msg-right">
                    <div id="intro"> 
                        <h1>
                            הגיע הזמן לאכול בריא יותר, גם במשרד
                        </h1>
                        
                        <p>
                            מהיום אתם יכולים ליהנות במשרד שלכם מארוחות איכותיות וטעימות שהוכנו על ידי שפים ובשלנים, בביתם.
                        </p>
                        
                        <p>
                            באתר שלנו תמצאו מגוון ארוחות ייחודיות המשתנות מדי יום: קציצות, תבשילי קדרה, מנות צמחוניות וטבעוניות, תוספות עשירות ועוד...
                        </p>
                        
                        <p>
                            הבשלנים והשפים שלנו מקפידים על שימוש בחומרי גלם טבעיים בלבד באיכות גבוהה, כדי שתיהנו מאוכל טרי ומזין מדי יום.
                        </p>
                        
                        <p class="red" style="color: #<?php echo $color_code; ?>" >
                            מהרו להזמין. בכל יום כמות מוגבלת מכל ארוחה.
                        </p>
                    </div>
                    
                    <div id="list"> 
                        <ul>
                            <li>ממשק פשוט ונוח להזמנה מהירה</li>
                            <li>רשימת מרכיבים מפורטת לכל מנה</li>
                            <li>הוספת הערות ובקשות אישיות מהבשלנים</li>
                            <li>אפשרות להזמין מראש ולקבל בזמן שנוח לכם</li>
                            <li>סליקה מאובטחת באתר</li>
                        </ul>
                    </div>
                    
                    <div id="find-out-more"> 
                        <h2>
                            אז מה זה בעצם Homeals?
                        </h2>
                        
                        <p>
                           Homeals היא פלטפורמה דיגיטלית ושירות המאפשר לסועדים להזמין ארוחות ייחודיות מבשלנים ושפים מוכשרים המבשלים במטבח הביתי.
באמצעות האתר סועדים יכולים ללמוד על הבשלנים, ליצור קשר ולהזמין באופן פשוט ומהיר ארוחות טעימות לצהריים ולערב ישירות לבית או למשרד.
                        </p>
                        
                        <a class="red" style="color: #<?php echo $color_code; ?>" id="read-more" href="<?php echo $site_url; ?>" target="_blank">
                            קראו עוד...
                        </a>
                    </div> 
                    
                    
                </div>
                
                <div style="clear: both">
            </div>
            
        </div>
    </div>
        
        
        <div id="footer-main">
        <div id="footer-main-logo">
            <img src="../image/data/greylogo.png">
        </div>
        <div style="clear: both"></div>
        <div id="footer-main-links">
            <ul>
                <li><a href="http://dev2.homeals.co.il/index.php?route=information/information&amp;information_id=4" target="_blank">אודותינו</a></li>
                <li><a href="http://dev2.homeals.co.il/index.php?route=information/information&amp;information_id=7" target="_blank">איך זה עובד</a></li>
                <li><a href="http://dev2.homeals.co.il/index.php?route=information/information&amp;information_id=8" target="_blank">אמון ובטיחות</a></li>
                <li><a href="http://dev2.homeals.co.il/index.php?route=information/information&amp;information_id=10" target="_blank">שאלות ותשובות</a></li>
                <li><a href="http://dev2.homeals.co.il/index.php?route=information/information&amp;information_id=9" target="_blank">הצטרפו כבשלנים</a></li>
            </ul>
            <div style="clear: both"></div>
        </div>
        <div style="clear: both"></div>
        <div id="footer-main-followus">
            <div id="followus-title">עקבו אחרינו</div>
            <div id="followus-body">
                <a href="https://www.facebook.com/pages/Homeals-Israel/608776639200255" target="blank">
                        <div id="facebook-follow"></div>
                </a>
                <a href="http://instagram.com/homealsisrael" target="blank">
                        <div id="insta-fllow"></div>
                </a>
                <a href="http://saloona.co.il/homeals/" target="blank">
                        <div id="saloona-fllow"></div>
                </a>
                <div style="clear: both"></div>
            </div>
        </div>
        <div style="clear: both"></div>
    </div>

    <div class="lightbox-con" style="display: none">
        
        <div id="box-one">
            <div class="close-btn" onclick="$.colorbox.close();">X</div>
            <div class="text">
            תודה שנרשמתם לאתר Homeals. ההטבה עודכנה בחשבונכם. 
            <br>
            שיהיה בתיאבון
            </div>
            <a href="../index.php?route=product/category&path=0" class="continue-btn">המשיכו לאתר</a>
            <div style="clear: both"></div>
        </div>
        
        <div id="box-two">
            <div class="close-btn" onclick="$.colorbox.close();">X</div>
            <div class="text">
            חשבון עם כתובת המייל שהוקלדה כבר קיים במערכת של Homeals.
            <br>
            ההטבה עודכנה בחשבונכם, שיהיה בתיאבון
            </div>
            <a href="../index.php?route=product/category&path=0" class="continue-btn">המשיכו לאתר</a>
            <div style="clear: both"></div>
        </div>
        
    </div>
    
    
<script>//<!--
    
$(document).ready(function() {
    
    $("#email").on("blur", function() {
        $("#submit-btn").trigger("click");
    });
    
    $("#submit-btn").on("click", function(event) {
        event.preventDefault();
        
        //code here
        var email = $("#email").val();
        var password = $("#password").val();
        var firstname = $("#firstname").val();
        var lastname = $("#lastname").val();
        
        var customer_group_id = $("#customer_group_id").val();
        var city = $("#city").val();
        var coupon_code = $("#coupon_code").val();
        var join_list = 1;
        
        var compony = "";
        
       
        if ($.trim(email).length == 0) {
            $("#email").css("border","1px solid #F00");
        }
        
        if (validateEmail(email)) {
            $("#password").val("");
            $("#email").css("border","1px solid #DDDDDD");
                
            $.ajax({
                type: "POST",
                url: "/index.php?route=account/register/ajax_validate/",
                data: { "firstname":firstname,"lastname":lastname,"email": email, "password": password , "join_list": join_list , "customer_group_id" : customer_group_id ,"city" : city },
                dataType: "json",
                cache: false,
                beforeSend : function() {
                    //showLoad();
                },
                complete: function() {
                    //hideLoad();
                }
            }).done(function( msg ) {
                    //console.log(msg);
                    
                    // user as been signed up
                    if (msg == "singedup") {
                        
                        $("#email").val("");
                        $("#firstname").val("");
                        $("#lastname").val("");
                        
                        var data = { "email" : email , "coupon_code" : coupon_code };
                        
                        add_coupon(data,open_one);
                        
                        return;
                    }
                    
                    // user is allready signed up
                    if( msg.warning == "error_exists" ) {
                        
                        $("#email").val("");
                        $("#firstname").val("");
                        $("#lastname").val("");
                        
                        var data = { "email" : email , "coupon_code" : coupon_code };
                        
                        add_coupon(data,open_two);
                        
                        return;
                    }
                    
                    if( !(msg.warning == "error_exists") && !(msg == "singedup")) {
                        hideLoad();
                    }
                    
                    if(msg.email == "error_email" ) {
                    	$("#email").css("border","1px solid #F00");
                        //$("#email-error").show();
                    }else{
                    	$("#email").css("border","1px solid #DDDDDD");
                    	//$("#signinemail-error").hide();
                    }
                    
                    if(msg.password == "error_password") {
                    	$("#password").css("border","1px solid #F00");
                    	//$("#signinpass-error").show();
                    }else{
                    	$("#password").css("border","1px solid #DDDDDD");
                    	//$("#signinmail-error").hide();
                    }
                    
                    if(msg.firstname == "error_firstname") {
                    	$("#firstname").css("border","1px solid #F00");
                    	//$("#signinfirstname-error").show();
                    }else{
                    	$("#firstname").css("border","1px solid #DDDDDD");
                    	//$("#signinfirstname-error").hide();
                    }
                    
                    if(msg.lastname == "error_lastname") {
                    	$("#lastname").css("border","1px solid #F00");
                    	//$("#signinlastname-error").show();
                    }else{
                    	$("#lastname").css("border","1px solid #DDDDDD");
                    	//$("#signinlastname-error").hide();
                    }
            });
        }
    });
    
});

function add_coupon(data,callbackFunc) {
    
    $.ajax({
            type: "POST",
            url: "/index.php?route=account/thankyou/addThisCoupon",
            data: data,
            dataType: "json",
            cache: false,
            before: function() {
                showLoad();
            },
            complete: function() {
                hideLoad();
            }
        }).done(function( msg ) { 
            if (msg.success) {
                callbackFunc();
            } 
            
            if (msg.error){
                alert(msg.error);
            }
        });
    
}

function open_one(data) {
    $.colorbox({href:"#box-one",inline:true
        ,onOpen:function(){
            $("body").css("height",$(window).height());
            $("body").css("overflow","hidden");
        }
        ,onComplete:function(){
            
        }
        ,onClosed:function(){
            $("body").css("height","100%");
            $("body").css("overflow","visible");
        }
    });
}

function open_two(data) {
    $.colorbox({href:"#box-two",inline:true
        ,onOpen:function(){
            $("body").css("height",$(window).height());
            $("body").css("overflow","hidden");
        }
        ,onComplete:function(){
            
        }
        ,onClosed:function(){
            $("body").css("height","100%");
            $("body").css("overflow","visible");
        }
    });
}

function validateEmail(email) {
    var filter = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
    if (filter.test(email)) {
        return true;
    } else {
        return false;
    }
}

function showLoad() {
    if( !$.browser.msie ) {
        $("#confirmLoader").show();
    }
}

function hideLoad() {
    $("#confirmLoader").hide();
}

//--></script>
    
    
</body>
</html>
