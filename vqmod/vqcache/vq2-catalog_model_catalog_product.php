<?php
class ModelCatalogProduct extends Model {
	public function updateViewed($product_id) {
		$this->db->query("UPDATE " . DB_PREFIX . "product SET viewed = (viewed + 1) WHERE product_id = '" . (int)$product_id . "'");
	}
	
	public function getProduct($product_id, $date_time = 0, $show_opposite = false , $time_key = false) {
		if ($this->customer->isLogged()) {
			$customer_group_id = $this->customer->getCustomerGroupId();
		} else {
			$customer_group_id = $this->config->get('config_customer_group_id');
		}
		
		$query = $this->db->query("SELECT DISTINCT *, pd.name AS name, p.image, m.name AS manufacturer, (SELECT price FROM " . DB_PREFIX . "product_discount pd2 WHERE pd2.product_id = p.product_id AND pd2.customer_group_id = '" . (int)$customer_group_id . "' AND pd2.quantity = '1' AND ((pd2.date_start = '0000-00-00' OR pd2.date_start < NOW()) AND (pd2.date_end = '0000-00-00' OR pd2.date_end > NOW())) ORDER BY pd2.priority ASC, pd2.price ASC LIMIT 1) AS discount, (SELECT price FROM " . DB_PREFIX . "product_special ps WHERE ps.product_id = p.product_id AND ps.customer_group_id = '" . (int)$customer_group_id . "' AND ((ps.date_start = '0000-00-00' OR ps.date_start < NOW()) AND (ps.date_end = '0000-00-00' OR ps.date_end > NOW())) ORDER BY ps.priority ASC, ps.price ASC LIMIT 1) AS special, (SELECT points FROM " . DB_PREFIX . "product_reward pr WHERE pr.product_id = p.product_id AND customer_group_id = '" . (int)$customer_group_id . "') AS reward, (SELECT ss.name FROM " . DB_PREFIX . "stock_status ss WHERE ss.stock_status_id = p.stock_status_id AND ss.language_id = '" . (int)$this->config->get('config_language_id') . "') AS stock_status, (SELECT wcd.unit FROM " . DB_PREFIX . "weight_class_description wcd WHERE p.weight_class_id = wcd.weight_class_id AND wcd.language_id = '" . (int)$this->config->get('config_language_id') . "') AS weight_class, (SELECT lcd.unit FROM " . DB_PREFIX . "length_class_description lcd WHERE p.length_class_id = lcd.length_class_id AND lcd.language_id = '" . (int)$this->config->get('config_language_id') . "') AS length_class, (SELECT AVG(rating) AS total FROM " . DB_PREFIX . "review r1 WHERE r1.product_id = p.product_id AND r1.status = '1' GROUP BY r1.product_id) AS rating, (SELECT COUNT(*) AS total FROM " . DB_PREFIX . "review r2 WHERE r2.product_id = p.product_id AND r2.status = '1' GROUP BY r2.product_id) AS reviews, (SELECT COUNT(*) AS total FROM " . DB_PREFIX . "review r2 WHERE r2.product_id = p.product_id AND r2.status = '1' AND r2.text != '' AND r2.text IS NOT NULL GROUP BY r2.product_id) AS reviews_text, p.sort_order FROM " . DB_PREFIX . "product p LEFT JOIN " . DB_PREFIX . "product_description pd ON (p.product_id = pd.product_id) LEFT JOIN " . DB_PREFIX . "product_to_store p2s ON (p.product_id = p2s.product_id) LEFT JOIN " . DB_PREFIX . "manufacturer m ON (p.manufacturer_id = m.manufacturer_id) WHERE p.product_id = '" . (int)$product_id . "' AND pd.language_id = '" . (int)$this->config->get('config_language_id') . "' AND p.status = '1' AND p.date_available <= NOW() AND p2s.store_id = '" . (int)$this->config->get('config_store_id') . "' ORDER BY p.position ASC");
		
		date_default_timezone_set("Asia/Jerusalem");
		
		if(true/*$date_time == 'all'*/){
			
			$meal_curr = 0;
			$order_times = array();
			$times_of_day = array();
			$times_end = array();
			$lst_odr_tim = 0;
			
		} else {
			// Availability and Quantity
			$meal_curr = 0;			// current meals for sale
			$meal_qunt = 0;			// defualt quantity of meals for sale
			$meals_bought = 0;		// meals all ready bought
			
			// need to get the date we want is some form of request or we pick today as the date
			if($date_time){
				$date = date('Y-m-d', strtotime($date_time));
				$today_str = strtolower(date("D",strtotime($date_time)));
			} else if(isset($_REQUEST['date'])){
				$date = date('Y-m-d', strtotime($_REQUEST['date']));//print_r($_REQUEST['date']);
				$today_str = strtolower(date("D",strtotime($_REQUEST['date']))); // gettings todays day in 3 letter format (sun,mon...)
			} else if(isset($this->session->data['date'])){
				$date = date('Y-m-d', strtotime($this->session->data['date']));
				$today_str = strtolower(date("D",strtotime($this->session->data['date']))); // gettings todays day in 3 letter format (sun,mon...)
			} else {
				$date = date("Y-m-d");
				$today_str = strtolower(date("D"));
			}
			
			/**************************************************************************
			* removes cooks by id
			* ( in the future the array list will be editable from admin scrren )
			**************************************************************************/
			// hadcoded way to kill of cooks from avialability (111 = Dana, 110 = Ohad)
			$cooks_list = array(111,110,109);
			if (in_array((int)$query->row['user_id'],$cooks_list)) {
				return false;
			}
			/**************************************************************************
			 * removes cooks by id end
			 **************************************************************************/
			
			/*********************************************************
			 *	closing dates for holydays and so on.
			 ********************************************************/
			$roshhashana_days = array("2014-10-08","2014-10-09","2014-10-11","2014-10-12","2014-10-13","2014-10-14","2014-10-15","2014-10-16","2014-10-17","2014-10-18");
			if(in_array($date,$roshhashana_days)) {
				return false;
			}
			/*********************************************************
			 *	closing dates for rosh ha shana
			 ********************************************************/

			/*************************************************************
			 *	product availabilty system, checks if meal is available
			 *	for purcheas in this time, no very efficent need to symplfiy
			 *	prosses and make more efficent.
			 *************************************************************/
			$this->load->model('catalog/availability');
			
			// getting defualt avaiability settings
			$def_avaiabilitys = $this->model_catalog_availability->getDefAvailabilitys($product_id);
			// getting that date custom avaiability settings
			$avaiability = $this->model_catalog_availability->getAvailability($product_id,$date);
			//consider making only one request here and split by code
			
			// Setting meal quantity
			if(!empty($avaiability)){
				$meal_qunt = $avaiability['quantity'];
			} else if(!empty($def_avaiabilitys)) {
				
				foreach($def_avaiabilitys as $defavail) {
					
					if(strtolower($defavail['day']) == strtolower($today_str)){
						$meal_qunt = $defavail['quantity'];
						// setting def avaiability as todays main avaiability
						$avaiability = $defavail;
						
						$def_avail = true;
					}
				}
				
			} else {
				return false;
			}
			
			// getting product past orders for quantity check
			// ( maybe we can move this to the main request )
			// ( consider reusing product quantity and using cron to reload quantity every day )
			$orders_query = $this->db->query("SELECT op.quantity,op.delivery_date,op.delivery_time, o.order_status_id FROM ". DB_PREFIX ."order_product op LEFT JOIN ". DB_PREFIX ."order o ON op.order_id = o.order_id WHERE op.product_id='".(int)$product_id."' AND o.order_status_id != '0' AND op.delivery_date='".$this->db->escape($date)."'");
			
			foreach($orders_query->rows as $order){
				$meals_bought += $order['quantity'];
			}
			
			/*****************************************************************/
			$meal_curr = $meal_qunt - $meals_bought; // current meal quantity
			/*****************************************************************/
			
			$check_qunt = true;
			
			if( $meal_qunt <= 0 ) {
				return false;
			} else if( $meal_curr <= 0 ) {
				$check_qunt = false;
			}
			
			if(!$show_opposite){
				// returns false if there is not meals avilable
				if( $meal_curr <= 0 ){
					return false;
				}
			}
			
			/******************************************************************
			*
			*	order times checks
			*
			*******************************************************************/
			// getting that date custom avaiability settings
			
			$last_order_time = $avaiability['last_order_time'] + 1;
			$start = $avaiability['start_time'];
			$end = $avaiability['end_time'];
			
			//$last_order_time = $this->model_catalog_availability->getTimeAvailability($product_id,$date);
			
			$times_of_day = array();
			
			// one more layer over the time so we can swap it with custom times if needed
			$all_times = array ();
			for($i=1;$i <= 24; $i++){
				if($i == 1){
					$all_times[$i] = "01:30 - 24:30";
				} else if($i == 10){
					$all_times[$i] = "10:30 - 09:30";
				} else if($i < 10) {
					$all_times[$i] = "0".$i.":30 - 0".($i - 1).":30";
				} else {
					$all_times[$i] = $i.":30 - ".($i - 1).":30";
				}
			}
			
			// picking the right order times for this avaiability
			$order_times = array();
			
			for($i=$start+1 ;$i <= $end; $i++){
				if(isset($all_times[$i])){
					$order_times[$i] = $all_times[$i];
				}
			}
			
			// need to get all the days orders for times check
			// function get full times (maybe in the future for now we will do it with product ajax)
			
			if($avaiability['by_start_time']){
				$last_order_time_stamp = strtotime($date." ".($start - 1).":30 -".$last_order_time." minutes" );
			} else {
				$last_order_time_stamp = strtotime($date." ".($end - 1).":31 -".$last_order_time." minutes");
				
				if ( strtotime($date." ".($end - 1).":31 -".$last_order_time." minutes") > strtotime($date." 12:00") ){
					$last_order_time_stamp = strtotime($date." 12:00");
				} else {
					$last_order_time_stamp = strtotime($date." ".($end - 1).":31 -".$last_order_time." minutes");
				}
			}
			
			// last order time in hour format
			$lst_odr_tim = strftime("%H:%M" ,$last_order_time_stamp);
			
			// last order time in month day format
			$lst_odr_tim_day = strftime("%e" ,$last_order_time_stamp);
			$date_mon_num = strftime("%e" ,strtotime($date));
			
			// we out put the text for the site (nothing for today tommarow for tommarow)
			if( $lst_odr_tim_day < $date_mon_num ) {
				$lst_odr_tim = strftime("%H:%M יום לפני" ,$last_order_time_stamp);
			}
			
			$times_end = array();
			
			if($avaiability['by_start_time']){
				foreach($order_times as $key => $order_time){
					if( $last_order_time_stamp > time() ){
						$times_of_day[$key] = $order_time;
						$times_end[$key] = date("H:i",$last_order_time_stamp + 60);
					}
				}
			} else {
				foreach($order_times as $key => $order_time){
					
					if( ( strtotime($date." ".($key-1).":30") - 60*$last_order_time ) > strtotime($date." 12:00") ){
						$final_last_order_time = strtotime($date." 12:00");
					} else {
						$final_last_order_time = strtotime($date." ".($key-1).":30") - 60*$last_order_time + 60;
					}
					
					if( $final_last_order_time > time() ){
						$times_of_day[$key] = $order_time;
					}
					
					$times_end[$key] = date("H:i",$final_last_order_time);
					
				}
			}
			
			// if we have a time key we check it
			if($time_key){
				if(empty($times_of_day[$time_key])){
					return false;
				}
			}
			
			if(empty($times_of_day)){
				
				if(!$check_qunt){
					$lst_odr_tim = 0;
				}
				
				if(!$show_opposite) {
					return false;
				}
				
			} elseif ($show_opposite) {
				
				if($check_qunt){
					return false;
				}else{
					$lst_odr_tim = 0;
				}
				
			} elseif($this->cart->hasProducts()) {
				$cart_items = $this->cart->getProducts();
				$cart_item = reset($cart_items);
				
				if(empty($times_of_day[$cart_item['time']])){
					return false;
				}
			}
		}
		
		// Customers
		$this->load->model('tool/image'); 
		
		$seller = array();
		$seller['username'] = '';
		$seller['sp'] = '';
		$seller['image'] = '';
		$seller['city'] = '';
		$cook_image = 'image/no-avatar-women.png';
		
		if(isset($query->row['product_id'])){
			$pro_to_cat = $this->db->query("SELECT * FROM oc_product_to_category LEFT JOIN oc_category_description INNER JOIN oc_category ON oc_category.category_id = oc_category_description.category_id ON oc_product_to_category.category_id = oc_category_description.category_id WHERE language_id = '" . (int)$this->config->get('config_language_id') . "' AND product_id ='".$query->row['product_id']."'");
			
			if(!empty($pro_to_cat->row['name'])){
				$pro_to_cat = $pro_to_cat->rows;
			} else {
				$pro_to_cat = '';
			}
		}
		
		if ($query->num_rows) {

				if ($this->config->get('cpf_fields')) {
					$fields = $this->config->get('cpf_fields');
				} else {
					$fields = array();
				}
				
				$cpf = array();
				
				foreach ($fields as $field) {
					$cpf['cpf_' . $field['option_id']] = $query->row['cpf_' . $field['option_id']];
				}
			
			return array(
				'product_id'       => $query->row['product_id'],

				'cpf'     => $cpf,
			
				'name'             => $query->row['name'],
				'description'      => $query->row['description'],
				'meta_description' => $query->row['meta_description'],
				'meta_keyword'     => $query->row['meta_keyword'],
				'tag'              => $query->row['tag'],
				'model'            => $query->row['model'],
				'sku'              => $query->row['sku'],
				'upc'              => $query->row['upc'],
				'ean'              => $query->row['ean'],
				'jan'              => $query->row['jan'],
				'isbn'             => $query->row['isbn'],
				'mpn'              => $query->row['mpn'],
				'location'         => $query->row['location'],
				'quantity'         => $meal_curr,//was $query->row['quantity'],
				'stock_status'     => $query->row['stock_status'],
				'image'            => $query->row['image'],
				'manufacturer_id'  => $query->row['manufacturer_id'],
				'manufacturer'     => $query->row['manufacturer'],
				'price'            => ($query->row['discount'] ? $query->row['discount'] : $query->row['price']),
				'special'          => $query->row['special'],
				'reward'           => $query->row['reward'],
				'points'           => $query->row['points'],
				'tax_class_id'     => $query->row['tax_class_id'],
				'date_available'   => $query->row['date_available'],
				'weight'           => $query->row['weight'],
				'weight_class_id'  => $query->row['weight_class_id'],
				'length'           => $query->row['length'],
				'width'            => $query->row['width'],
				'height'           => $query->row['height'],
				'length_class_id'  => $query->row['length_class_id'],
				'subtract'         => $query->row['subtract'],
				'rating'           => round($query->row['rating']),
				'reviews'          => $query->row['reviews'] ? $query->row['reviews'] : 0,
				'minimum'          => $query->row['minimum'],
				'sort_order'       => $query->row['sort_order'],
				'status'           => $query->row['status'],
				'date_added'       => $query->row['date_added'],
				'date_modified'    => $query->row['date_modified'],
				'viewed'           => $query->row['viewed'],
				//Added by KCS
				'meal_type'	   => $query->row['meal_type'],
				'reviews_text'     => $query->row['reviews_text'] ? $query->row['reviews_text'] : 0,
				'number_of_sides'  => $query->row['number_of_sides'],
				'user_id'          => $query->row['user_id'],
				'username'	   => $seller['username'],
				'sp'		   => $seller['sp'],
				'user_image'       => $seller['image'],
				'cook_image'       => $cook_image,
				'pro_cat'	   => $pro_to_cat,
				'city'		   => $seller['city'],
				'all_order_times'  => $order_times,
				'order_times'	   => $times_of_day,
				'all_end_times'    => $times_end,
				'last_order_time'  => $lst_odr_tim,
				'is_verified'  	   => $query->row['is_verified']
			);
		} else {
			return false;
		}
	}
    
    //get product category
   public function getProductCat($id){
    $query = $this->db->query("SELECT data FROM oc_product_menu_cat WHERE product_id='" . $id ."'");
    return $query->row;
    
    }
	
	public function getProduct_with_images($product_id) {
		if ($this->customer->isLogged()) {
			$customer_group_id = $this->customer->getCustomerGroupId();
		} else {
			$customer_group_id = $this->config->get('config_customer_group_id');
		}
		
		$check = $this->getProduct($product_id);
		
		if(!$check){
			return false;
		}
		
		$query = $this->db->query("SELECT DISTINCT *, pd.name AS name, p.image, m.name AS manufacturer, (SELECT price FROM " . DB_PREFIX . "product_discount pd2 WHERE pd2.product_id = p.product_id AND pd2.customer_group_id = '" . (int)$customer_group_id . "' AND pd2.quantity = '1'
								  AND ((pd2.date_start = '0000-00-00' OR pd2.date_start < NOW()) AND (pd2.date_end = '0000-00-00' OR pd2.date_end > NOW())) ORDER BY pd2.priority ASC, pd2.price ASC LIMIT 1) AS discount,
								  (SELECT price FROM " . DB_PREFIX . "product_special ps WHERE ps.product_id = p.product_id AND ps.customer_group_id = '" . (int)$customer_group_id . "' AND ((ps.date_start = '0000-00-00' OR ps.date_start < NOW()) AND
								  (ps.date_end = '0000-00-00' OR ps.date_end > NOW())) ORDER BY ps.priority ASC, ps.price ASC LIMIT 1) AS special,
								  (SELECT points FROM " . DB_PREFIX . "product_reward pr WHERE pr.product_id = p.product_id AND customer_group_id = '" . (int)$customer_group_id . "') AS reward,
								  (SELECT ss.name FROM " . DB_PREFIX . "stock_status ss WHERE ss.stock_status_id = p.stock_status_id AND ss.language_id = '" . (int)$this->config->get('config_language_id') . "') AS stock_status,
								  (SELECT wcd.unit FROM " . DB_PREFIX . "weight_class_description wcd WHERE p.weight_class_id = wcd.weight_class_id AND wcd.language_id = '" . (int)$this->config->get('config_language_id') . "') AS weight_class,
								  (SELECT lcd.unit FROM " . DB_PREFIX . "length_class_description lcd WHERE p.length_class_id = lcd.length_class_id AND lcd.language_id = '" . (int)$this->config->get('config_language_id') . "') AS length_class,
								  (SELECT AVG(rating) AS total FROM " . DB_PREFIX . "review r1 WHERE r1.product_id = p.product_id AND r1.status = '1' GROUP BY r1.product_id) AS rating,
								  (SELECT COUNT(*) AS total FROM " . DB_PREFIX . "review r2 WHERE r2.product_id = p.product_id AND r2.status = '1' GROUP BY r2.product_id) AS reviews,
								  p.sort_order,
								  msp.seller_id
								  
								  FROM " . DB_PREFIX . "product p
								  LEFT JOIN " . DB_PREFIX . "ms_product msp ON msp.product_id = p.product_id
								  LEFT JOIN " . DB_PREFIX . "product_description pd ON (p.product_id = pd.product_id)
								  LEFT JOIN " . DB_PREFIX . "product_to_store p2s ON (p.product_id = p2s.product_id) LEFT JOIN " . DB_PREFIX . "manufacturer m ON (p.manufacturer_id = m.manufacturer_id)
								  WHERE p.product_id = '" . (int)$product_id . "' AND pd.language_id = '" . (int)$this->config->get('config_language_id') . "' AND p.status = '1' AND p.meal_type = '0' AND p.date_available <= NOW() AND p2s.store_id = '" . (int)$this->config->get('config_store_id') . "'AND p.meal_type = '0'");
		
		
		// Customers
		
		$this->load->model('account/customer');
		
		$seller = array();
			
		if (isset($query->row['user_id'])) {
				$customer_info = $this->model_account_customer->getCustomer((int)$query->row['user_id']);
				
				if ($customer_info) {		
					$seller['username'] = $customer_info['firstname']." ".$customer_info['lastname'];
				} else {
					$seller['username'] = '';
				}	
		} else {
			$seller['username'] = '';
		}
		
		$pro_to_cat = $this->db->query("SELECT * FROM oc_product_to_category LEFT JOIN oc_category_description ON oc_product_to_category.category_id = oc_category_description.category_id WHERE language_id = '" . (int)$this->config->get('config_language_id') . "' AND product_id ='".$query->row['product_id']."'");
		
		if ($query->num_rows) {

				if ($this->config->get('cpf_fields')) {
					$fields = $this->config->get('cpf_fields');
				} else {
					$fields = array();
				}
				
				$cpf = array();
				
				foreach ($fields as $field) {
					$cpf['cpf_' . $field['option_id']] = $query->row['cpf_' . $field['option_id']];
				}
			
			return array(
				'product_id'       => $query->row['product_id'],

				'cpf'     => $cpf,
			
				'seller_id'       => $query->row['seller_id'],
				'name'             => $query->row['name'],
				'description'      => $query->row['description'],
				'meta_description' => $query->row['meta_description'],
				'meta_keyword'     => $query->row['meta_keyword'],
				'tag'              => $query->row['tag'],
				'model'            => $query->row['model'],
				'sku'              => $query->row['sku'],
				'upc'              => $query->row['upc'],
				'ean'              => $query->row['ean'],
				'jan'              => $query->row['jan'],
				'isbn'             => $query->row['isbn'],
				'mpn'              => $query->row['mpn'],
				'location'         => $query->row['location'],
				'quantity'         => $query->row['quantity'],
				'stock_status'     => $query->row['stock_status'],
				'image'            => $query->row['image'],
				'manufacturer_id'  => $query->row['manufacturer_id'],
				'manufacturer'     => $query->row['manufacturer'],
				'price'            => ($query->row['discount'] ? $query->row['discount'] : $query->row['price']),
				'special'          => $query->row['special'],
				'reward'           => $query->row['reward'],
				'points'           => $query->row['points'],
				'tax_class_id'     => $query->row['tax_class_id'],
				'date_available'   => $query->row['date_available'],
				'weight'           => $query->row['weight'],
				'weight_class_id'  => $query->row['weight_class_id'],
				'length'           => $query->row['length'],
				'width'            => $query->row['width'],
				'height'           => $query->row['height'],
				'length_class_id'  => $query->row['length_class_id'],
				'subtract'         => $query->row['subtract'],
				'rating'           => round($query->row['rating']),
				'reviews'          => $query->row['reviews'] ? $query->row['reviews'] : 0,
				'minimum'          => $query->row['minimum'],
				'sort_order'       => $query->row['sort_order'],
				'status'           => $query->row['status'],
				'date_added'       => $query->row['date_added'],
				'date_modified'    => $query->row['date_modified'],
				'viewed'           => $query->row['viewed'],
				'images'           => $this->getProductImages($query->row['product_id']),
				//Added by Homeals
				'number_of_sides'  => $query->row['number_of_sides'],
				'user_id'          => $query->row['user_id'],
				'username'		   => $seller['username'],
				'sp'		   	   => $seller['sp'],
				'user_image'       => $seller['image'],
				'cook_image'       => $cook_image,
				'pro_cat'	   	   => $pro_to_cat,
				'city'		       => $seller['city']
				);
		} else {
			return false;
		}
	}
	
	public function getProducts_byCategory($category_id) {
		if ($this->customer->isLogged()) {
			$customer_group_id = $this->customer->getCustomerGroupId();
		} else {
			$customer_group_id = $this->config->get('config_customer_group_id');
		}	
				
		$query = $this->db->query("SELECT DISTINCT *, pd.name AS name, p.image, m.name AS manufacturer, (SELECT price FROM " . DB_PREFIX . "product_discount pd2 WHERE pd2.product_id = p.product_id AND pd2.customer_group_id = '" . (int)$customer_group_id . "' AND pd2.quantity = '1'
								  AND ((pd2.date_start = '0000-00-00' OR pd2.date_start < NOW()) AND (pd2.date_end = '0000-00-00' OR pd2.date_end > NOW())) ORDER BY pd2.priority ASC, pd2.price ASC LIMIT 1) AS discount,
								  (SELECT price FROM " . DB_PREFIX . "product_special ps WHERE ps.product_id = p.product_id AND ps.customer_group_id = '" . (int)$customer_group_id . "' AND ((ps.date_start = '0000-00-00' OR ps.date_start < NOW()) AND
								  (ps.date_end = '0000-00-00' OR ps.date_end > NOW())) ORDER BY ps.priority ASC, ps.price ASC LIMIT 1) AS special,
								  (SELECT points FROM " . DB_PREFIX . "product_reward pr WHERE pr.product_id = p.product_id AND customer_group_id = '" . (int)$customer_group_id . "') AS reward,
								  (SELECT ss.name FROM " . DB_PREFIX . "stock_status ss WHERE ss.stock_status_id = p.stock_status_id AND ss.language_id = '" . (int)$this->config->get('config_language_id') . "') AS stock_status,
								  (SELECT wcd.unit FROM " . DB_PREFIX . "weight_class_description wcd WHERE p.weight_class_id = wcd.weight_class_id AND wcd.language_id = '" . (int)$this->config->get('config_language_id') . "') AS weight_class,
								  (SELECT lcd.unit FROM " . DB_PREFIX . "length_class_description lcd WHERE p.length_class_id = lcd.length_class_id AND lcd.language_id = '" . (int)$this->config->get('config_language_id') . "') AS length_class,
								  (SELECT AVG(rating) AS total FROM " . DB_PREFIX . "review r1 WHERE r1.product_id = p.product_id AND r1.status = '1' GROUP BY r1.product_id) AS rating,
								  (SELECT COUNT(*) AS total FROM " . DB_PREFIX . "review r2 WHERE r2.product_id = p.product_id AND r2.status = '1' GROUP BY r2.product_id) AS reviews,
								  p.sort_order,
								  msp.seller_id
								  
								  FROM " . DB_PREFIX . "product p
								  INNER JOIN " .DB_PREFIX . "product_to_category ptoc ON ptoc.product_id=p.product_id
								  LEFT JOIN " . DB_PREFIX . "ms_product msp ON msp.product_id = p.product_id
								  LEFT JOIN " . DB_PREFIX . "product_description pd ON (p.product_id = pd.product_id)
								  LEFT JOIN " . DB_PREFIX . "product_to_store p2s ON (p.product_id = p2s.product_id) LEFT JOIN " . DB_PREFIX . "manufacturer m ON (p.manufacturer_id = m.manufacturer_id)
								  WHERE ptoc.category_id = '" . (int)$category_id . "' AND pd.language_id = '" . (int)$this->config->get('config_language_id') . "' AND p.status = '1' AND p.date_available <= NOW() AND p2s.store_id = '" . (int)$this->config->get('config_store_id') . "' AND p.meal_type = '0'");
		
		$product_data = array();
				
		
	
		foreach ($query->rows as $result) {
			$product_data[] = $this->getProduct_with_images($result['product_id']);
			
		}
		
		
		////KCS
		//foreach($product_data as &$v) {
		//	foreach($v as &$v1) {	
		//		$v1 = str_replace('"','&quot;',$v1);
		//	}
		//}
		////~KCS
		return $product_data;
	}
	
	
	

	public function getProducts($data = array()) {
		if ($this->customer->isLogged()) {
			$customer_group_id = $this->customer->getCustomerGroupId();
		} else {
			$customer_group_id = $this->config->get('config_customer_group_id');
		}	
		
		$sql = "SELECT p.product_id, (SELECT AVG(rating) AS total FROM " . DB_PREFIX . "review r1 WHERE r1.product_id = p.product_id AND r1.status = '1' GROUP BY r1.product_id) AS rating, (SELECT price FROM " . DB_PREFIX . "product_discount pd2 WHERE pd2.product_id = p.product_id AND pd2.customer_group_id = '" . (int)$customer_group_id . "' AND pd2.quantity = '1' AND ((pd2.date_start = '0000-00-00' OR pd2.date_start < NOW()) AND (pd2.date_end = '0000-00-00' OR pd2.date_end > NOW())) ORDER BY pd2.priority ASC, pd2.price ASC LIMIT 1) AS discount, (SELECT price FROM " . DB_PREFIX . "product_special ps WHERE ps.product_id = p.product_id AND ps.customer_group_id = '" . (int)$customer_group_id . "' AND ((ps.date_start = '0000-00-00' OR ps.date_start < NOW()) AND (ps.date_end = '0000-00-00' OR ps.date_end > NOW())) ORDER BY ps.priority ASC, ps.price ASC LIMIT 1) AND p.meal_type = '0' AS special"; 
		
		if (!empty($data['filter_category_id'])) {
			if (!empty($data['filter_sub_category'])) {
				$sql .= " FROM " . DB_PREFIX . "category_path cp LEFT JOIN " . DB_PREFIX . "product_to_category p2c ON (cp.category_id = p2c.category_id)";			
			} else {
				$sql .= " FROM " . DB_PREFIX . "product_to_category p2c";
			}
		
			if (!empty($data['filter_filter'])) {
				$sql .= " LEFT JOIN " . DB_PREFIX . "product_filter pf ON (p2c.product_id = pf.product_id) LEFT JOIN " . DB_PREFIX . "product p ON (pf.product_id = p.product_id)";
			} else {
				$sql .= " LEFT JOIN " . DB_PREFIX . "product p ON (p2c.product_id = p.product_id)";
			}
		} else {
			$sql .= " FROM " . DB_PREFIX . "product p";
		}
		
		$sql .= " LEFT JOIN " . DB_PREFIX . "product_description pd ON (p.product_id = pd.product_id) LEFT JOIN " . DB_PREFIX . "product_to_store p2s ON (p.product_id = p2s.product_id) WHERE pd.language_id = '" . (int)$this->config->get('config_language_id') . "' AND p.status = '1' AND p.meal_type = '0' AND p.date_available <= NOW() AND p2s.store_id = '" . (int)$this->config->get('config_store_id') . "'";
		
		
		if (!empty($data['filter_category_id'])) {
			$this->load->model('catalog/product');
			$total = $this->model_catalog_product->getTotalProducts(array('filter_category_id' => $data['filter_category_id']));
			
			if ($total) {
				if (!empty($data['filter_sub_category'])) {
					$sql .= " AND cp.path_id = '" . (int)$data['filter_category_id'] . "'";	
				} else {
					$sql .= " AND p2c.category_id = '" . (int)$data['filter_category_id'] . "'";			
				}
							
				if (!empty($data['filter_filter'])) {
					$implode = array();
					
					$filters = explode(',', $data['filter_filter']);
					
					foreach ($filters as $filter_id) {
						$implode[] = (int)$filter_id;
					}
					
					$sql .= " AND pf.filter_id IN (" . implode(',', $implode) . ")";				
				}
			}
                }
		
		$dont_filter = false;

		if (!empty($data['filter_name']) || !empty($data['filter_tag'])) {
			$sql .= " AND (";
			
			if (!empty($data['filter_name'])) {
				$implode = array();

				$words = explode(' ', trim(preg_replace('/\s\s+/', ' ', $data['filter_name'])));

				foreach ($words as $word) {
					$implode[] = "pd.name LIKE '%" . $this->db->escape($word) . "%'";
				}
				
				if ($implode) {
					$sql .= " " . implode(" AND ", $implode) . "";
				}

				if (!empty($data['filter_description'])) {
					$sql .= " OR pd.description LIKE '%" . $this->db->escape($data['filter_name']) . "%'";
				}
			}
			
			if (!empty($data['filter_name']) && !empty($data['filter_tag'])) {
				$sql .= " OR ";
			}
			
			if (!empty($data['filter_tag'])) {
				$sql .= "pd.tag LIKE '%" . $this->db->escape($data['filter_tag']) . "%'";
			}
			
			if (!empty($data['filter_name'])) {
				$sql .= " OR LCASE(p.model) = '" . $this->db->escape(utf8_strtolower($data['filter_name'])) . "'";

				if ($this->config->get('cpf_fields')) {
					$fields = $this->config->get('cpf_fields');
				} else {
					$fields = array();
				}
			
				foreach ($fields as $field) {
					if (!empty($field['searchable']) && $field['status']) {
						if ($field['type'] == 'radio' || $field['type'] == 'select') {
							$sql .= " OR LCASE((SELECT name FROM " . DB_PREFIX . "option_value_description WHERE language_id = '" . (int)$this->config->get('config_language_id') . "' AND option_value_id = pd.`cpf_" . $field['option_id'] . "`)) LIKE '%" . $this->db->escape(utf8_strtolower($data['filter_name'])) . "%'";
						} else {
							$sql .= " OR LCASE(pd.`cpf_" . $field['option_id'] . "`) LIKE '%" . $this->db->escape(utf8_strtolower($data['filter_name'])) . "%'";
						}
					}
				}
			
			}
			
			if (!empty($data['filter_name'])) {
				$sql .= " OR LCASE(p.sku) = '" . $this->db->escape(utf8_strtolower($data['filter_name'])) . "'";
			}	
			
			if (!empty($data['filter_name'])) {
				$sql .= " OR LCASE(p.upc) = '" . $this->db->escape(utf8_strtolower($data['filter_name'])) . "'";
			}		

			if (!empty($data['filter_name'])) {
				$sql .= " OR LCASE(p.ean) = '" . $this->db->escape(utf8_strtolower($data['filter_name'])) . "'";
			}

			if (!empty($data['filter_name'])) {
				$sql .= " OR LCASE(p.jan) = '" . $this->db->escape(utf8_strtolower($data['filter_name'])) . "'";
			}
			
			if (!empty($data['filter_name'])) {
				$sql .= " OR LCASE(p.isbn) = '" . $this->db->escape(utf8_strtolower($data['filter_name'])) . "'";
			}		
			
			if (!empty($data['filter_name'])) {
				$sql .= " OR LCASE(p.mpn) = '" . $this->db->escape(utf8_strtolower($data['filter_name'])) . "'";
			}
			
			$sql .= ")";
		}
		
		if (!empty($data['filter_manufacturer_id'])) {
			$sql .= " AND p.manufacturer_id = '" . (int)$data['filter_manufacturer_id'] . "'";
		}
		
		if (!empty($data['filter_customer_id']) && $data['filter_customer_id'] != 0) {
			$sql .= " AND p.user_id = '" . (int)$data['filter_customer_id'] . "'";
		}
		
		if (!empty($data['filter_exclude_product_id']) && $data['filter_exclude_product_id'] != 0) {
			$sql .= " AND p.product_id != '" . (int)$data['filter_exclude_product_id'] . "'";
		}
		
		if (!empty($data['filter_exclude_products']) && $data['filter_exclude_products'] != 0) {
			$sql .= " AND p.product_id NOT IN ( '" . implode($data['filter_exclude_products'], "', '") . "' )";
		}
		
		if (!empty($data['filter_homepage']) && $data['filter_homepage'] != 0) {
			$sql .= " AND p.isbn > '0'";
			$dont_filter = true;
		}
		
		$sql .= " GROUP BY p.product_id";
		
		$sort_data = array(
			'pd.name',
			'p.model',
			'p.quantity',
			'p.price',
			'rating',
			'p.sort_order',
			'p.date_added',
			'p.isbn'
		);	
		
		if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
			if ($data['sort'] == 'pd.name' || $data['sort'] == 'p.model') {
				$sql .= " ORDER BY LCASE(" . $data['sort'] . ")";
			} elseif ($data['sort'] == 'p.price') {
				$sql .= " ORDER BY (CASE WHEN special IS NOT NULL THEN special WHEN discount IS NOT NULL THEN discount ELSE p.price END)";
			} else {
				$sql .= " ORDER BY " . $data['sort'];
			}
		} else {
			$sql .= " ORDER BY p.sort_order";	
		}
		
		if (isset($data['order']) && ($data['order'] == 'DESC')) {
			$sql .= " DESC, LCASE(pd.name) DESC";
		} else {
			$sql .= " ASC, LCASE(pd.name) ASC";
		}
	
		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}				
			
			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}	
			
			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}
		
		$product_data = array();
		
		$query = $this->db->query($sql);
		
		foreach($query->rows as $result){
			$product = $this->getProduct($result['product_id']);
		
			if($product){ 
				$product_data[$result['product_id']] = $product;
			}
		}
		
		/****************************************************************************
		 *
		 *	HOMEALS SORTING
		 *		A - Rating
		 *		B - # of Star Reviews
		 *		C - # of Text Reviews
		 *		D - Manual sort
		 *		N - # of Sales
		 *
		 *	A(1*B + 2*C) + N || D
		 *
		 ***************************************************************************/
		if(!$dont_filter){
			$sorted_product_data = array();
			
			foreach($product_data as $product){
				
				//getting sales
				$product_sold_sql = "SELECT SUM(p.quantity) AS sold FROM " . DB_PREFIX . "order_product p LEFT JOIN " . DB_PREFIX . "order o ON p.order_id = o.order_id WHERE o.order_status_id != 0 AND p.product_id = '".(int)$product['product_id']."'";
				$product_sold = $this->db->query($product_sold_sql);
				
				if($product['mpn']){
					$product['sort_rating'] = 99999999 - $product['mpn'];
				} else {
					$product['sort_rating'] = $product['rating'] * (1*$product['reviews'] + 2*$product['reviews_text']) + $product_sold->row['sold'];
				}
				
				$sorted_product_data[$product['product_id']] = $product;
			}
			
			// sort by furmula and name
			usort($sorted_product_data, array("ModelCatalogProduct", "cmp"));
			
			$product_data = $sorted_product_data;
		}
		
		return $product_data;
	}
	
	static function cmp($a, $b)
	{
		if ($a['sort_rating'] == $b['sort_rating']) {
			return strcmp($a['name'],$b['name']);
		}
		
		return ($a['sort_rating'] < $b['sort_rating']) ? 1 : -1;
	}
	
	public function getProducts_api($data = array()) {
		if ($this->customer->isLogged()) {
			$customer_group_id = $this->customer->getCustomerGroupId();
		} else {
			$customer_group_id = $this->config->get('config_customer_group_id');
		}	
		
		$sql = "SELECT p.product_id, (SELECT AVG(rating) AS total FROM " . DB_PREFIX . "review r1 WHERE r1.product_id = p.product_id AND r1.status = '1' GROUP BY r1.product_id) AS rating, (SELECT price FROM " . DB_PREFIX . "product_discount pd2 WHERE pd2.product_id = p.product_id AND pd2.customer_group_id = '" . (int)$customer_group_id . "' AND pd2.quantity = '1' AND ((pd2.date_start = '0000-00-00' OR pd2.date_start < NOW()) AND (pd2.date_end = '0000-00-00' OR pd2.date_end > NOW())) ORDER BY pd2.priority ASC, pd2.price ASC LIMIT 1) AS discount, (SELECT price FROM " . DB_PREFIX . "product_special ps WHERE ps.product_id = p.product_id AND ps.customer_group_id = '" . (int)$customer_group_id . "' AND ((ps.date_start = '0000-00-00' OR ps.date_start < NOW()) AND (ps.date_end = '0000-00-00' OR ps.date_end > NOW())) ORDER BY ps.priority ASC, ps.price ASC LIMIT 1) AS special"; 
		
		if (!empty($data['filter_category_id'])) {
			if (!empty($data['filter_sub_category'])) {
				$sql .= " FROM " . DB_PREFIX . "category_path cp LEFT JOIN " . DB_PREFIX . "product_to_category p2c ON (cp.category_id = p2c.category_id)";			
			} else {
				$sql .= " FROM " . DB_PREFIX . "product_to_category p2c";
			}
		
			if (!empty($data['filter_filter'])) {
				$sql .= " LEFT JOIN " . DB_PREFIX . "product_filter pf ON (p2c.product_id = pf.product_id) LEFT JOIN " . DB_PREFIX . "product p ON (pf.product_id = p.product_id)";
			} else {
				$sql .= " LEFT JOIN " . DB_PREFIX . "product p ON (p2c.product_id = p.product_id)";
			}
		} else {
			$sql .= " FROM " . DB_PREFIX . "product p";
		}
		
		$sql .= " LEFT JOIN " . DB_PREFIX . "product_description pd ON (p.product_id = pd.product_id) LEFT JOIN " . DB_PREFIX . "product_to_store p2s ON (p.product_id = p2s.product_id) WHERE pd.language_id = '" . (int)$this->config->get('config_language_id') . "' AND p.status = '1' AND p.meal_type = '0' AND p.date_available <= NOW() AND p2s.store_id = '" . (int)$this->config->get('config_store_id') . "'";
		
		if (!empty($data['filter_category_id'])) {
			if (!empty($data['filter_sub_category'])) {
				$sql .= " AND cp.path_id = '" . (int)$data['filter_category_id'] . "'";	
			} else {
				$sql .= " AND p2c.category_id = '" . (int)$data['filter_category_id'] . "'";			
			}	
		
			if (!empty($data['filter_filter'])) {
				$implode = array();
				
				$filters = explode(',', $data['filter_filter']);
				
				foreach ($filters as $filter_id) {
					$implode[] = (int)$filter_id;
				}
				
				$sql .= " AND pf.filter_id IN (" . implode(',', $implode) . ")";				
			}
		}	

		if (!empty($data['filter_name']) || !empty($data['filter_tag'])) {
			$sql .= " AND (";
			
			if (!empty($data['filter_name'])) {
				$implode = array();

				$words = explode(' ', trim(preg_replace('/\s\s+/', ' ', $data['filter_name'])));

				foreach ($words as $word) {
					$implode[] = "pd.name LIKE '%" . $this->db->escape($word) . "%'";
				}
				
				if ($implode) {
					$sql .= " " . implode(" AND ", $implode) . "";
				}

				if (!empty($data['filter_description'])) {
					$sql .= " OR pd.description LIKE '%" . $this->db->escape($data['filter_name']) . "%'";
				}
			}
			
			if (!empty($data['filter_name']) && !empty($data['filter_tag'])) {
				$sql .= " OR ";
			}
			
			if (!empty($data['filter_tag'])) {
				$sql .= "pd.tag LIKE '%" . $this->db->escape($data['filter_tag']) . "%'";
			}
			
			if (!empty($data['filter_name'])) {
				$sql .= " OR LCASE(p.model) = '" . $this->db->escape(utf8_strtolower($data['filter_name'])) . "'";

				if ($this->config->get('cpf_fields')) {
					$fields = $this->config->get('cpf_fields');
				} else {
					$fields = array();
				}
			
				foreach ($fields as $field) {
					if (!empty($field['searchable']) && $field['status']) {
						if ($field['type'] == 'radio' || $field['type'] == 'select') {
							$sql .= " OR LCASE((SELECT name FROM " . DB_PREFIX . "option_value_description WHERE language_id = '" . (int)$this->config->get('config_language_id') . "' AND option_value_id = pd.`cpf_" . $field['option_id'] . "`)) LIKE '%" . $this->db->escape(utf8_strtolower($data['filter_name'])) . "%'";
						} else {
							$sql .= " OR LCASE(pd.`cpf_" . $field['option_id'] . "`) LIKE '%" . $this->db->escape(utf8_strtolower($data['filter_name'])) . "%'";
						}
					}
				}
			
			}
			
			if (!empty($data['filter_name'])) {
				$sql .= " OR LCASE(p.sku) = '" . $this->db->escape(utf8_strtolower($data['filter_name'])) . "'";
			}	
			
			if (!empty($data['filter_name'])) {
				$sql .= " OR LCASE(p.upc) = '" . $this->db->escape(utf8_strtolower($data['filter_name'])) . "'";
			}		

			if (!empty($data['filter_name'])) {
				$sql .= " OR LCASE(p.ean) = '" . $this->db->escape(utf8_strtolower($data['filter_name'])) . "'";
			}

			if (!empty($data['filter_name'])) {
				$sql .= " OR LCASE(p.jan) = '" . $this->db->escape(utf8_strtolower($data['filter_name'])) . "'";
			}
			
			if (!empty($data['filter_name'])) {
				$sql .= " OR LCASE(p.isbn) = '" . $this->db->escape(utf8_strtolower($data['filter_name'])) . "'";
			}		
			
			if (!empty($data['filter_name'])) {
				$sql .= " OR LCASE(p.mpn) = '" . $this->db->escape(utf8_strtolower($data['filter_name'])) . "'";
			}
			
			$sql .= ")";
		}
					
		if (!empty($data['filter_manufacturer_id'])) {
			$sql .= " AND p.manufacturer_id = '" . (int)$data['filter_manufacturer_id'] . "'";
		}
		
		$sql .= " GROUP BY p.product_id";
		
		$sort_data = array(
			'pd.name',
			'p.model',
			'p.quantity',
			'p.price',
			'rating',
			'p.sort_order',
			'p.date_added'
		);	
		
		if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
			if ($data['sort'] == 'pd.name' || $data['sort'] == 'p.model') {
				$sql .= " ORDER BY LCASE(" . $data['sort'] . ")";
			} elseif ($data['sort'] == 'p.price') {
				$sql .= " ORDER BY (CASE WHEN special IS NOT NULL THEN special WHEN discount IS NOT NULL THEN discount ELSE p.price END)";
			} else {
				$sql .= " ORDER BY " . $data['sort'];
			}
		} else {
			$sql .= " ORDER BY p.sort_order";	
		}
		
		if (isset($data['order']) && ($data['order'] == 'DESC')) {
			$sql .= " DESC, LCASE(pd.name) DESC";
		} else {
			$sql .= " ASC, LCASE(pd.name) ASC";
		}
	
		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}				

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}	
		
			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}
		
		$product_data = array();
				
		$query = $this->db->query($sql);
	
		foreach ($query->rows as $result) {
			$product_data[] = $this->getProduct_with_images($result['product_id']);
			
			
			
		}
		//KCS
		foreach($product_data as &$v) {
			foreach($v as &$v1) {	
				$v1 = str_replace('"','&quot;',$v1);
			}
		}
		//~KCS
		return $product_data;
	}
	
	public function getProductSpecials($data = array()) {
		if ($this->customer->isLogged()) {
			$customer_group_id = $this->customer->getCustomerGroupId();
		} else {
			$customer_group_id = $this->config->get('config_customer_group_id');
		}	
				
		$sql = "SELECT DISTINCT ps.product_id, (SELECT AVG(rating) FROM " . DB_PREFIX . "review r1 WHERE r1.product_id = ps.product_id AND r1.status = '1' GROUP BY r1.product_id) AS rating FROM " . DB_PREFIX . "product_special ps LEFT JOIN " . DB_PREFIX . "product p ON (ps.product_id = p.product_id) LEFT JOIN " . DB_PREFIX . "product_description pd ON (p.product_id = pd.product_id) LEFT JOIN " . DB_PREFIX . "product_to_store p2s ON (p.product_id = p2s.product_id) WHERE p.status = '1' AND p.meal_type = '0' AND p.date_available <= NOW() AND p2s.store_id = '" . (int)$this->config->get('config_store_id') . "' AND ps.customer_group_id = '" . (int)$customer_group_id . "' AND ((ps.date_start = '0000-00-00' OR ps.date_start < NOW()) AND (ps.date_end = '0000-00-00' OR ps.date_end > NOW())) GROUP BY ps.product_id";

		$sort_data = array(
			'pd.name',
			'p.model',
			'ps.price',
			'rating',
			'p.sort_order'
		);
		
		if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
			if ($data['sort'] == 'pd.name' || $data['sort'] == 'p.model') {
				$sql .= " ORDER BY LCASE(" . $data['sort'] . ")";
			} else {
				$sql .= " ORDER BY " . $data['sort'];
			}
		} else {
			$sql .= " ORDER BY p.sort_order";	
		}
		
		if (isset($data['order']) && ($data['order'] == 'DESC')) {
			$sql .= " DESC, LCASE(pd.name) DESC";
		} else {
			$sql .= " ASC, LCASE(pd.name) ASC";
		}
	
		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}				

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}	
		
			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}

		$product_data = array();
		
		$query = $this->db->query($sql);
		
		foreach ($query->rows as $result) { 		
			$product_data[$result['product_id']] = $this->getProduct($result['product_id']);
		}
		
		return $product_data;
	}
		
	public function getLatestProducts($limit) {
		if ($this->customer->isLogged()) {
			$customer_group_id = $this->customer->getCustomerGroupId();
		} else {
			$customer_group_id = $this->config->get('config_customer_group_id');
		}	
				
		$product_data = $this->cache->get('product.latest.' . (int)$this->config->get('config_language_id') . '.' . (int)$this->config->get('config_store_id') . '.' . $customer_group_id . '.' . (int)$limit);

		if (!$product_data) { 
			$query = $this->db->query("SELECT p.product_id FROM " . DB_PREFIX . "product p LEFT JOIN " . DB_PREFIX . "product_to_store p2s ON (p.product_id = p2s.product_id) WHERE p.status = '1' AND p.meal_type = '0' AND p.date_available <= NOW() AND p2s.store_id = '" . (int)$this->config->get('config_store_id') . "' ORDER BY p.date_added DESC LIMIT " . (int)$limit);
		 	 
			foreach ($query->rows as $result) {
				$product_data[$result['product_id']] = $this->getProduct($result['product_id']);
			}
			
			$this->cache->set('product.latest.' . (int)$this->config->get('config_language_id') . '.' . (int)$this->config->get('config_store_id'). '.' . $customer_group_id . '.' . (int)$limit, $product_data);
		}
		
		return $product_data;
	}
	
	public function getPopularProducts($limit) {
		$product_data = array();
		
		$query = $this->db->query("SELECT p.product_id FROM " . DB_PREFIX . "product p LEFT JOIN " . DB_PREFIX . "product_to_store p2s ON (p.product_id = p2s.product_id) WHERE p.status = '1' AND p.meal_type = '0' AND p.date_available <= NOW() AND p2s.store_id = '" . (int)$this->config->get('config_store_id') . "' ORDER BY p.viewed, p.date_added DESC LIMIT " . (int)$limit);
		
		foreach ($query->rows as $result) { 		
			$product_data[$result['product_id']] = $this->getProduct($result['product_id']);
		}
					 	 		
		return $product_data;
	}

	public function getBestSellerProducts($limit) {
		if ($this->customer->isLogged()) {
			$customer_group_id = $this->customer->getCustomerGroupId();
		} else {
			$customer_group_id = $this->config->get('config_customer_group_id');
		}	
				
		$product_data = $this->cache->get('product.bestseller.' . (int)$this->config->get('config_language_id') . '.' . (int)$this->config->get('config_store_id'). '.' . $customer_group_id . '.' . (int)$limit);

		if (!$product_data) { 
			$product_data = array();
			
			$query = $this->db->query("SELECT op.product_id, COUNT(*) AS total FROM " . DB_PREFIX . "order_product op LEFT JOIN `" . DB_PREFIX . "order` o ON (op.order_id = o.order_id) LEFT JOIN `" . DB_PREFIX . "product` p ON (op.product_id = p.product_id) LEFT JOIN " . DB_PREFIX . "product_to_store p2s ON (p.product_id = p2s.product_id) WHERE o.order_status_id > '0' AND p.status = '1' AND p.meal_type = '0' AND p.date_available <= NOW() AND p2s.store_id = '" . (int)$this->config->get('config_store_id') . "' GROUP BY op.product_id ORDER BY total DESC LIMIT " . (int)$limit);
			
			foreach ($query->rows as $result) { 		
				$product_data[$result['product_id']] = $this->getProduct($result['product_id']);
			}
			
			$this->cache->set('product.bestseller.' . (int)$this->config->get('config_language_id') . '.' . (int)$this->config->get('config_store_id'). '.' . $customer_group_id . '.' . (int)$limit, $product_data);
		}
		
		return $product_data;
	}
	
	public function getProductAttributes($product_id) {
		$product_attribute_group_data = array();
		
		$product_attribute_group_query = $this->db->query("SELECT ag.attribute_group_id, agd.name FROM " . DB_PREFIX . "product_attribute pa LEFT JOIN " . DB_PREFIX . "attribute a ON (pa.attribute_id = a.attribute_id) LEFT JOIN " . DB_PREFIX . "attribute_group ag ON (a.attribute_group_id = ag.attribute_group_id) LEFT JOIN " . DB_PREFIX . "attribute_group_description agd ON (ag.attribute_group_id = agd.attribute_group_id) WHERE pa.product_id = '" . (int)$product_id . "' AND agd.language_id = '" . (int)$this->config->get('config_language_id') . "' GROUP BY ag.attribute_group_id ORDER BY ag.sort_order, agd.name");
		
		foreach ($product_attribute_group_query->rows as $product_attribute_group) {
			$product_attribute_data = array();
			
			$product_attribute_query = $this->db->query("SELECT a.attribute_id, ad.name, pa.text FROM " . DB_PREFIX . "product_attribute pa LEFT JOIN " . DB_PREFIX . "attribute a ON (pa.attribute_id = a.attribute_id) LEFT JOIN " . DB_PREFIX . "attribute_description ad ON (a.attribute_id = ad.attribute_id) WHERE pa.product_id = '" . (int)$product_id . "' AND a.attribute_group_id = '" . (int)$product_attribute_group['attribute_group_id'] . "' AND ad.language_id = '" . (int)$this->config->get('config_language_id') . "' AND pa.language_id = '" . (int)$this->config->get('config_language_id') . "' ORDER BY a.sort_order, ad.name");
			
			foreach ($product_attribute_query->rows as $product_attribute) {
				$product_attribute_data[] = array(
					'attribute_id' => $product_attribute['attribute_id'],
					'name'         => $product_attribute['name'],
					'text'         => $product_attribute['text']		 	
				);
			}
			
			$product_attribute_group_data[] = array(
				'attribute_group_id' => $product_attribute_group['attribute_group_id'],
				'name'               => $product_attribute_group['name'],
				'attribute'          => $product_attribute_data
			);			
		}
		
		return $product_attribute_group_data;
	}
			
	public function getProductOptions($product_id) {
		$product_option_data = array();

		$product_option_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "product_option po LEFT JOIN `" . DB_PREFIX . "option` o ON (po.option_id = o.option_id) LEFT JOIN " . DB_PREFIX . "option_description od ON (o.option_id = od.option_id) WHERE po.product_id = '" . (int)$product_id . "' AND od.language_id = '" . (int)$this->config->get('config_language_id') . "' ORDER BY o.sort_order");
		
		foreach ($product_option_query->rows as $product_option) {
			//if ($product_option['type'] == 'select' || $product_option['type'] == 'radio' || $product_option['type'] == 'checkbox' ||  $product_option['type'] == 'image') {
			//Modified for option quantity ===================================================================================
			if ($product_option['type'] == 'select' || $product_option['type'] == 'radio' || $product_option['type'] == 'checkbox' || $product_option['type'] == 'checkboxQuantity' || $product_option['type'] == 'image') {
			//================================================================================================================
				$product_option_value_data = array();
			
				$product_option_value_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "product_option_value pov LEFT JOIN " . DB_PREFIX . "option_value ov ON (pov.option_value_id = ov.option_value_id) LEFT JOIN " . DB_PREFIX . "option_value_description ovd ON (ov.option_value_id = ovd.option_value_id) WHERE pov.product_id = '" . (int)$product_id . "' AND pov.product_option_id = '" . (int)$product_option['product_option_id'] . "' AND ovd.language_id = '" . (int)$this->config->get('config_language_id') . "' ORDER BY ov.sort_order");
				
				foreach ($product_option_value_query->rows as $product_option_value) {
					$product_option_value_data[] = array(
						'product_option_value_id' => $product_option_value['product_option_value_id'],
						'option_value_id'         => $product_option_value['option_value_id'],
						'name'                    => $product_option_value['name'],
						'note'                    => $product_option_value['note'],

						'image'                   => $product_option_value['image'],
						'quantity'                => $product_option_value['quantity'],
						'subtract'                => $product_option_value['subtract'],
						'price'                   => $product_option_value['price'],
						'price_prefix'            => $product_option_value['price_prefix'],
						'weight'                  => $product_option_value['weight'],
						'weight_prefix'           => $product_option_value['weight_prefix']
					);
				}
									
				$product_option_data[] = array(
					'product_option_id' => $product_option['product_option_id'],
					'option_id'         => $product_option['option_id'],
					'name'              => $product_option['name'],                    'note'              => $product_option['note'],

					'type'              => $product_option['type'],
					'option_value'      => $product_option_value_data,
					'required'          => $product_option['required']
				);
			} else {
				$product_option_data[] = array(
					'product_option_id' => $product_option['product_option_id'],
					'option_id'         => $product_option['option_id'],
					'name'              => $product_option['name'],                    'note'              => $product_option['note'],

					'type'              => $product_option['type'],
					'option_value'      => $product_option['option_value'],
					'required'          => $product_option['required']
				);				
			}
      	}
		
		return $product_option_data;
	}
	
	public function getProductDiscounts($product_id) {
		if ($this->customer->isLogged()) {
			$customer_group_id = $this->customer->getCustomerGroupId();
		} else {
			$customer_group_id = $this->config->get('config_customer_group_id');
		}	
		
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "product_discount WHERE product_id = '" . (int)$product_id . "' AND customer_group_id = '" . (int)$customer_group_id . "' AND quantity > 1 AND ((date_start = '0000-00-00' OR date_start < NOW()) AND (date_end = '0000-00-00' OR date_end > NOW())) ORDER BY quantity ASC, priority ASC, price ASC");

		return $query->rows;		
	}
		
	public function getProductImages($product_id) {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "product_image WHERE product_id = '" . (int)$product_id . "' ORDER BY sort_order ASC");

		return $query->rows;
	}
	
	public function getProductRelated($product_id) {
		$product_data = array();

		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "product_related pr LEFT JOIN " . DB_PREFIX . "product p ON (pr.related_id = p.product_id) LEFT JOIN " . DB_PREFIX . "product_to_store p2s ON (p.product_id = p2s.product_id) WHERE pr.product_id = '" . (int)$product_id . "' AND p.status = '1' AND p.meal_type = '1' AND p.date_available <= NOW() AND p2s.store_id = '" . (int)$this->config->get('config_store_id') . "'");
		
		foreach ($query->rows as $result) { 
			$product_data[$result['related_id']] = $this->getProduct($result['related_id']);
		}
		
		return $product_data;
	}
		
	public function getProductLayoutId($product_id) {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "product_to_layout WHERE product_id = '" . (int)$product_id . "' AND store_id = '" . (int)$this->config->get('config_store_id') . "'");
		
		if ($query->num_rows) {
			return $query->row['layout_id'];
		} else {
			return  $this->config->get('config_layout_product');
		}
	}
	
	public function getCategories($product_id) {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "product_to_category WHERE product_id = '" . (int)$product_id . "'");
		
		return $query->rows;
	}	
		
	public function getTotalProducts($data = array()) {
		if ($this->customer->isLogged()) {
			$customer_group_id = $this->customer->getCustomerGroupId();
		} else {
			$customer_group_id = $this->config->get('config_customer_group_id');
		}	

		$sql = "SELECT COUNT(DISTINCT p.product_id) AS total"; 
		
		if (!empty($data['filter_category_id'])) {
			if (!empty($data['filter_sub_category'])) {
				$sql .= " FROM " . DB_PREFIX . "category_path cp LEFT JOIN " . DB_PREFIX . "product_to_category p2c ON (cp.category_id = p2c.category_id)";			
			} else {
				$sql .= " FROM " . DB_PREFIX . "product_to_category p2c";
			}
		
			if (!empty($data['filter_filter'])) {
				$sql .= " LEFT JOIN " . DB_PREFIX . "product_filter pf ON (p2c.product_id = pf.product_id) LEFT JOIN " . DB_PREFIX . "product p ON (pf.product_id = p.product_id)";
			} else {
				$sql .= " LEFT JOIN " . DB_PREFIX . "product p ON (p2c.product_id = p.product_id)";
			}
		} else {
			$sql .= " FROM " . DB_PREFIX . "product p";
		}
		
		$sql .= " LEFT JOIN " . DB_PREFIX . "product_description pd ON (p.product_id = pd.product_id) LEFT JOIN " . DB_PREFIX . "product_to_store p2s ON (p.product_id = p2s.product_id) WHERE pd.language_id = '" . (int)$this->config->get('config_language_id') . "' AND p.status = '1' AND p.meal_type = '0' AND p.date_available <= NOW() AND p2s.store_id = '" . (int)$this->config->get('config_store_id') . "'";
		
		if (!empty($data['filter_category_id'])) {
			if (!empty($data['filter_sub_category'])) {
				$sql .= " AND cp.path_id = '" . (int)$data['filter_category_id'] . "'";	
			} else {
				$sql .= " AND p2c.category_id = '" . (int)$data['filter_category_id'] . "'";			
			}	
		
			if (!empty($data['filter_filter'])) {
				$implode = array();
				
				$filters = explode(',', $data['filter_filter']);
				
				foreach ($filters as $filter_id) {
					$implode[] = (int)$filter_id;
				}
				
				$sql .= " AND pf.filter_id IN (" . implode(',', $implode) . ")";				
			}
		}
		
		if (!empty($data['filter_name']) || !empty($data['filter_tag'])) {
			$sql .= " AND (";
			
			if (!empty($data['filter_name'])) {
				$implode = array();

				$words = explode(' ', trim(preg_replace('/\s\s+/', ' ', $data['filter_name'])));

				foreach ($words as $word) {
					$implode[] = "pd.name LIKE '%" . $this->db->escape($word) . "%'";
				}
				
				if ($implode) {
					$sql .= " " . implode(" AND ", $implode) . "";
				}

				if (!empty($data['filter_description'])) {
					$sql .= " OR pd.description LIKE '%" . $this->db->escape($data['filter_name']) . "%'";
				}
			}
			
			if (!empty($data['filter_name']) && !empty($data['filter_tag'])) {
				$sql .= " OR ";
			}
			
			if (!empty($data['filter_tag'])) {
				$sql .= "pd.tag LIKE '%" . $this->db->escape(utf8_strtolower($data['filter_tag'])) . "%'";
			}
		
			if (!empty($data['filter_name'])) {
				$sql .= " OR LCASE(p.model) = '" . $this->db->escape(utf8_strtolower($data['filter_name'])) . "'";

				if ($this->config->get('cpf_fields')) {
					$fields = $this->config->get('cpf_fields');
				} else {
					$fields = array();
				}
			
				foreach ($fields as $field) {
					if (!empty($field['searchable']) && $field['status']) {
						if ($field['type'] == 'radio' || $field['type'] == 'select') {
							$sql .= " OR LCASE((SELECT name FROM " . DB_PREFIX . "option_value_description WHERE language_id = '" . (int)$this->config->get('config_language_id') . "' AND option_value_id = pd.`cpf_" . $field['option_id'] . "`)) LIKE '%" . $this->db->escape(utf8_strtolower($data['filter_name'])) . "%'";
						} else {
							$sql .= " OR LCASE(pd.`cpf_" . $field['option_id'] . "`) LIKE '%" . $this->db->escape(utf8_strtolower($data['filter_name'])) . "%'";
						}
					}
				}
			
			}
			
			if (!empty($data['filter_name'])) {
				$sql .= " OR LCASE(p.sku) = '" . $this->db->escape(utf8_strtolower($data['filter_name'])) . "'";
			}	
			
			if (!empty($data['filter_name'])) {
				$sql .= " OR LCASE(p.upc) = '" . $this->db->escape(utf8_strtolower($data['filter_name'])) . "'";
			}		

			if (!empty($data['filter_name'])) {
				$sql .= " OR LCASE(p.ean) = '" . $this->db->escape(utf8_strtolower($data['filter_name'])) . "'";
			}

			if (!empty($data['filter_name'])) {
				$sql .= " OR LCASE(p.jan) = '" . $this->db->escape(utf8_strtolower($data['filter_name'])) . "'";
			}
			
			if (!empty($data['filter_name'])) {
				$sql .= " OR LCASE(p.isbn) = '" . $this->db->escape(utf8_strtolower($data['filter_name'])) . "'";
			}		
			
			if (!empty($data['filter_name'])) {
				$sql .= " OR LCASE(p.mpn) = '" . $this->db->escape(utf8_strtolower($data['filter_name'])) . "'";
			}
			
			$sql .= ")";				
		}
		
		if (!empty($data['filter_manufacturer_id'])) {
			$sql .= " AND p.manufacturer_id = '" . (int)$data['filter_manufacturer_id'] . "'";
		}
		

				$sql_disable = "SELECT p.product_id as 'product_id'";
				
				/* Filters */
				if (!empty($data['filter_category_id'])) {
					if (!empty($data['filter_sub_category'])) {
						$sql_disable .= " FROM " . DB_PREFIX . "category_path cp LEFT JOIN " . DB_PREFIX . "product_to_category p2c ON (cp.category_id = p2c.category_id)";	
					} else {
						$sql_disable .= " FROM " . DB_PREFIX . "product_to_category p2c";
					}
					if (!empty($data['filter_filter'])) {
						$sql_disable .= " LEFT JOIN " . DB_PREFIX . "product_filter pf ON (p2c.product_id = pf.product_id) LEFT JOIN " . DB_PREFIX . "product p ON (pf.product_id = p.product_id)";
					} else {
						$sql_disable .= " LEFT JOIN " . DB_PREFIX . "product p ON (p2c.product_id = p.product_id)";
					}
				} else {
					$sql_disable .= " FROM " . DB_PREFIX . "product p";
				}
				
				$sql_disable .= " LEFT JOIN `" . DB_PREFIX . "ms_product` mp ON (p.product_id = mp.product_id)";
				
				$sql_disable .= " LEFT JOIN " . DB_PREFIX . "product_description pd ON (p.product_id = pd.product_id) LEFT JOIN " . DB_PREFIX . "product_to_store p2s ON (p.product_id = p2s.product_id) WHERE pd.language_id = '" . (int)$this->config->get('config_language_id') . "' AND p.status = '1' AND p.date_available <= NOW() AND p2s.store_id = '" . (int)$this->config->get('config_store_id') . "'";
				
				if (!empty($data['filter_category_id'])) {
					if (!empty($data['filter_sub_category'])) {
						$sql_disable .= " AND cp.path_id = '" . (int)$data['filter_category_id'] . "'";
					} else {
						$sql_disable .= " AND p2c.category_id = '" . (int)$data['filter_category_id'] . "'";
					}
					
					if (!empty($data['filter_filter'])) {
						$implode = array();
						
						$filters = explode(',', $data['filter_filter']);
						
						foreach ($filters as $filter_id) {
							$implode[] = (int)$filter_id;
						}
						
						$sql_disable .= " AND pf.filter_id IN (" . implode(',', $implode) . ")";
					}
				}
				
				if (!empty($data['filter_name']) || !empty($data['filter_tag'])) {
					$sql_disable .= " AND (";
					
					if (!empty($data['filter_name'])) {
						$implode = array();
						
						$words = explode(' ', trim(preg_replace('/\s\s+/', ' ', $data['filter_name'])));
						
						foreach ($words as $word) {
							$implode[] = "pd.name LIKE '%" . $this->db->escape($word) . "%'";
						}
						
						if ($implode) {
							$sql_disable .= " " . implode(" AND ", $implode) . "";
						}

						if (!empty($data['filter_description'])) {
							$sql_disable .= " OR pd.description LIKE '%" . $this->db->escape($data['filter_name']) . "%'";
						}
					}
					
					if (!empty($data['filter_name']) && !empty($data['filter_tag'])) {
						$sql_disable .= " OR ";
					}
					
					if (!empty($data['filter_tag'])) {
						$sql_disable .= "pd.tag LIKE '%" . $this->db->escape(utf8_strtolower($data['filter_tag'])) . "%'";
					}
				
					if (!empty($data['filter_name'])) {
						$sql_disable .= " OR LCASE(p.model) = '" . $this->db->escape(utf8_strtolower($data['filter_name'])) . "'";
					}
					
					if (!empty($data['filter_name'])) {
						$sql_disable .= " OR LCASE(p.sku) = '" . $this->db->escape(utf8_strtolower($data['filter_name'])) . "'";
					}
					
					if (!empty($data['filter_name'])) {
						$sql_disable .= " OR LCASE(p.upc) = '" . $this->db->escape(utf8_strtolower($data['filter_name'])) . "'";
					}

					if (!empty($data['filter_name'])) {
						$sql_disable .= " OR LCASE(p.ean) = '" . $this->db->escape(utf8_strtolower($data['filter_name'])) . "'";
					}

					if (!empty($data['filter_name'])) {
						$sql_disable .= " OR LCASE(p.jan) = '" . $this->db->escape(utf8_strtolower($data['filter_name'])) . "'";
					}
					
					if (!empty($data['filter_name'])) {
						$sql_disable .= " OR LCASE(p.isbn) = '" . $this->db->escape(utf8_strtolower($data['filter_name'])) . "'";
					}
					
					if (!empty($data['filter_name'])) {
						$sql_disable .= " OR LCASE(p.mpn) = '" . $this->db->escape(utf8_strtolower($data['filter_name'])) . "'";
					}
					
					$sql_disable .= ")";
				}
				
				if (!empty($data['filter_manufacturer_id'])) {
					$sql_disable .= " AND p.manufacturer_id = '" . (int)$data['filter_manufacturer_id'] . "'";
				}
				
				$sql_disable .= " AND mp.list_until < NOW() AND p.status = 1";
				
				$res_disable = $this->db->query($sql_disable);
				
				if ($res_disable->num_rows) {
					foreach ($res_disable->rows as $product) {
						$this->MsLoader->MsProduct->changeStatus((int)$product['product_id'], MsProduct::STATUS_DISABLED);
						$this->MsLoader->MsProduct->disapprove((int)$product['product_id']);
					}
				}
			
		$query = $this->db->query($sql);
		
		return $query->row['total'];
	}
			
	public function getTotalProductSpecials() {
		if ($this->customer->isLogged()) {
			$customer_group_id = $this->customer->getCustomerGroupId();
		} else {
			$customer_group_id = $this->config->get('config_customer_group_id');
		}		
		
		$query = $this->db->query("SELECT COUNT(DISTINCT ps.product_id) AS total FROM " . DB_PREFIX . "product_special ps LEFT JOIN " . DB_PREFIX . "product p ON (ps.product_id = p.product_id) LEFT JOIN " . DB_PREFIX . "product_to_store p2s ON (p.product_id = p2s.product_id) WHERE p.status = '1' AND p.meal_type = '0' AND p.date_available <= NOW() AND p2s.store_id = '" . (int)$this->config->get('config_store_id') . "' AND ps.customer_group_id = '" . (int)$customer_group_id . "' AND ((ps.date_start = '0000-00-00' OR ps.date_start < NOW()) AND (ps.date_end = '0000-00-00' OR ps.date_end > NOW()))");
		
		if (isset($query->row['total'])) {
			return $query->row['total'];
		} else {
			return 0;	
		}
	}
	
}
?>