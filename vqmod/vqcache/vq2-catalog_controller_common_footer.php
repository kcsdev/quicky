<?php  
class ControllerCommonFooter extends Controller {
	protected function index() {

				$this->data = array_merge($this->data, $this->load->language('multiseller/multiseller'));
			
		$this->language->load('common/footer');
		
		$this->data['text_information'] = $this->language->get('text_information');
		$this->data['text_service'] = $this->language->get('text_service');
		$this->data['text_extra'] = $this->language->get('text_extra');
		$this->data['text_contact'] = $this->language->get('text_contact');
		$this->data['text_return'] = $this->language->get('text_return');
		$this->data['text_sitemap'] = $this->language->get('text_sitemap');
		$this->data['text_manufacturer'] = $this->language->get('text_manufacturer');
		$this->data['text_voucher'] = $this->language->get('text_voucher');
		$this->data['text_affiliate'] = $this->language->get('text_affiliate');
		$this->data['text_special'] = $this->language->get('text_special');
		$this->data['text_account'] = $this->language->get('text_account');
		$this->data['text_order'] = $this->language->get('text_order');
		$this->data['text_wishlist'] = $this->language->get('text_wishlist');
		$this->data['text_newsletter'] = $this->language->get('text_newsletter');
		
		$this->data['text_items_in_bag'] = $this->language->get('text_items_in_bag');
		$this->data['text_recent_meal_added'] = $this->language->get('text_recent_meal_added');
		$this->data['text_add_meal'] = $this->language->get('text_add_meal');
		$this->data['text_checkout'] = $this->language->get('text_checkout');
		$this->data['text_close'] = $this->language->get('text_close');
		$this->data['text_empty'] = $this->language->get('text_empty');
		$this->data['text_empty_cart'] = $this->language->get('text_empty_cart');
		$this->data['text_address_time'] = $this->language->get('text_address_time');
		
		$this->data['the_service'] = $this->language->get('the_service');
		$this->data['how_it_works'] = $this->language->get('how_it_works');
		$this->data['trust'] = $this->language->get('trust');
		$this->data['faq'] = $this->language->get('faq');
		$this->data['join_us'] = $this->language->get('join_us');
		
		$this->data['the_company'] = $this->language->get('the_company');
		$this->data['about_us'] = $this->language->get('about_us');
		$this->data['terms_of_use'] = $this->language->get('terms_of_use');
		$this->data['privacy_policy'] = $this->language->get('privacy_policy');
		
		$this->data['rights'] = sprintf($this->language->get('rights'),"Homeals");
		$this->data['rights_ex'] = $this->language->get('rights_ex');
		
		$this->data['text_total'] = $this->language->get('text_total');
		$this->data['ils'] = $this->language->get('ils');
		
		$address_type = ( isset($this->session->data['address_type']) ? $this->session->data['address_type'] : "none" );
        
        //get the resaurant list
        $this->load->model('account/customer');
        
        //get the city menu
         $city = $this->model_account_customer->getTheCity();
        if(isset($city)){
        
         $this->data['city'] = $city;
        
        }else{
        
         $this->data['city'] = '';
        
        }
        if($this->cart->getProducts()){
         $rest_id = '';
        foreach($this->cart->getProducts() as $value){
        $rest_id = $value['cook'];      
          }

        $rest_name = $this->model_account_customer->getCustomerName($rest_id);
        
        $this->data['rest'] =   $rest_name[0]['firstname'] . ' ' . $rest_name[0]['lastname'];
        
        }
		if($this->customer->isLogged() && $this->customer->getAddressId()){
			$this->load->model('account/address');
			$address_id = $this->customer->getAddressId();
			$def_address = $this->model_account_address->getAddress($address_id);
		} else {
			$def_address = false;
		}
		
		if (isset($this->request->get['filter_place'])) {
			
			$this->data['delivery_address'] = $this->request->get['filter_place'].", ".$this->language->get('text_tel_aviv');
		
		} else if(isset($this->session->data['filter_place']) && $address_type == "new"){
			
			$this->data['delivery_address'] = $this->session->data['filter_place'];
			
		} else if($address_type == "def") {
			
			if($def_address){
				if(!empty($def_address['address_1'])){
					$street = explode("#",$def_address['address_1']);
					$this->data['delivery_address']	= $street[0]." ".$street[1].", ".$def_address['city'];
				} else {
					$this->data['delivery_address']	= $def_address['city'];
				}
			} else {
				$this->data['delivery_address']	= $this->language->get('text_tel_aviv');
			}
			
		} else {
			if($def_address){
				if(!empty($def_address['address_1'])){
					$street = explode("#",$def_address['address_1']);
					$this->data['delivery_address']	= $street[0]." ".$street[1].", ".$def_address['city'];
				} else {
					$this->data['delivery_address']	= $def_address['city'];
				}
			} else {
				$this->data['delivery_address']	= $this->language->get('text_tel_aviv');
			}
			
		}
		
		
		/**********************************
		*	review popup code
		**********************************/
		
		$this->data['review_this_meal_id'] = $this->get_review_meal_id();
		
		/**********************************
		*	review popup code end
		**********************************/ 
		
		$this->data['text_address_time_demo'] = sprintf($this->language->get('text_address_time_demo'),$this->data['delivery_address']);
		
		$this->load->model('catalog/information');
		
		$this->data['informations'] = array();
		
		foreach ($this->model_catalog_information->getInformations() as $result) {
			if ($result['bottom']) {
				$this->data['informations'][] = array(
					'title' => $result['title'],
					'href'  => $this->url->link('information/information', 'information_id=' . $result['information_id'])
				);
			}
		}
		
		$this->data['contact'] = $this->url->link('information/contact');
		$this->data['return'] = $this->url->link('account/return/insert', '', 'SSL');
		$this->data['sitemap'] = $this->url->link('information/sitemap');
		$this->data['manufacturer'] = $this->url->link('product/manufacturer');
		$this->data['voucher'] = $this->url->link('account/voucher', '', 'SSL');
		$this->data['affiliate'] = $this->url->link('affiliate/account', '', 'SSL');
		$this->data['special'] = $this->url->link('product/special');
		$this->data['account'] = $this->url->link('account/account', '', 'SSL');
		$this->data['order'] = $this->url->link('account/order', '', 'SSL');
		$this->data['wishlist'] = $this->url->link('account/wishlist', '', 'SSL');
		$this->data['newsletter'] = $this->url->link('account/newsletter', '', 'SSL');		
		
		$this->data['powered'] = sprintf($this->language->get('text_powered'), $this->config->get('config_name'), date('Y', time()));
		
		// Whos Online
		if ($this->config->get('config_customer_online')) {
			$this->load->model('tool/online');
	
			if (isset($this->request->server['REMOTE_ADDR'])) {
				$ip = $this->request->server['REMOTE_ADDR'];	
			} else {
				$ip = ''; 
			}
			
			if (isset($this->request->server['HTTP_HOST']) && isset($this->request->server['REQUEST_URI'])) {
				$url = 'http://' . $this->request->server['HTTP_HOST'] . $this->request->server['REQUEST_URI'];	
			} else {
				$url = '';
			}
			
			if (isset($this->request->server['HTTP_REFERER'])) {
				$referer = $this->request->server['HTTP_REFERER'];	
			} else {
				$referer = '';
			}
			
			$this->model_tool_online->whosonline($ip, $this->customer->getId(), $url, $referer);
		}
		
		$this->children = array(
			'module/magnorcms',
			'common/content_footer',
			//'supercheckout/homealscheckout',
		);
		
		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/common/footer.tpl')) {
			$this->template = $this->config->get('config_template') . '/template/common/footer.tpl';
		} else {
			$this->template = 'default/template/common/footer.tpl';
		}
		
		$this->render();
	}
	
	private function get_review_meal_id(){
		// only for logged in users
		if($this->customer->isLogged() && !isset($this->session->data['review_flag'])){
			$customer_id = $this->customer->getId();
			
			$meals_array = array();
			
			$last_date = date("Y-m-d",strtotime("today -5 days"));
			$today = date("Y-m-d",strtotime("today"));
			$last_time = date("H:i:s");
			
			$order_id_sql ="( SELECT o.order_id
				FROM " . DB_PREFIX . "order o
				WHERE o.customer_id = '" . (int)$customer_id . "'
				AND o.order_status_id != 0
				AND o.delivery_date >=  '" . $last_date . "'
				AND o.delivery_date <=  '" . $today . "'
				AND o.delivery_time <=  '" . $last_time . "'
				ORDER BY o.date_modified DESC 
				LIMIT 1 )";
			
			// gets last order meals
			$last_order_meals = $this->db->query("SELECT *
							     FROM " . DB_PREFIX . "order_product
							     WHERE order_id = ".$order_id_sql);
			
			foreach($last_order_meals->rows as $meal_info){
				$reviews = $this->db->query("SELECT * FROM oc_review WHERE product_id = " . (int)$meal_info['product_id'] . " AND customer_id = " . (int)$customer_id . "");
				
				// number of reviews of meal by customer
				$has_review = $reviews->num_rows;
				
				// we give a score based on the last time the product was reviewd, never - 3 , in the past - 2 , lately 1
				if($has_review){
					foreach($reviews->rows as $review_info){
						$review_date = strtotime($review_info['date_added']);
						$order_date = strtotime($meal_info['delivery_date']);
						
						if($review_date >= $order_date){
							$review_customer_score = 1;
						} else {
							$review_customer_score = 2;
						}
					}
				} else {
					$review_customer_score = 3;
				}
				
				$meal_history = $this->db->query("SELECT op.* , o.customer_id FROM oc_order_product op LEFT JOIN oc_order o ON o.order_id = op.order_id WHERE product_id = " . (int)$meal_info['product_id'] . " AND o.customer_id = " . (int)$customer_id . "");
				
				// number of total times this customer bought this product
				$meal_bought_in_pass = $meal_history->num_rows;
				
				$reviews_by_meal_only = $this->db->query("SELECT * FROM oc_review WHERE product_id = " . (int)$meal_info['product_id'] . "");
				// number of total reviews that the product has
				$number_of_reviews = $reviews_by_meal_only->num_rows;
				
				if($review_customer_score != 1){
					$meals_array[] = array(
						"product_id"			=>	$meal_info['product_id'],
						"my_review_score"		=>	$review_customer_score,
						"meals_i_bought_score"		=>	$meal_bought_in_pass,
						"all_review_score"		=>	$number_of_reviews,
					);
				}
			}
			
			if(!empty($meals_array)){
				// Obtain a list of columns
				foreach ($meals_array as $key => $row) {
				    $my_reviews[$key]  = $row['my_review_score'];
				    $all_reviews[$key] = $row['all_review_score'];
				    $meals_bought[$key] = $row['meals_i_bought_score'];
				}
				// sorting array to get the best id we want
				array_multisort($my_reviews, SORT_DESC,$meals_bought, SORT_DESC, $all_reviews, SORT_ASC, $meals_array);
				$meal_id = reset($meals_array);
				// the id of the meal we need to review
				return $meal_id['product_id'];	
			} else {
				return false;
			}
		} else {
			return false;
		}
	}
	
	
	
}
?>