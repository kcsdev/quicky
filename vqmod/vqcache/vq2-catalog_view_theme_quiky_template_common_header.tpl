<!DOCTYPE html>

<!--[if lt IE 7]><html class="ie6" dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>"><![endif]-->
<!--[if IE 7]><html class="ie7" dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>"><![endif]-->
<!--[if IE 8]><html class="ie8" dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>"><![endif]-->
<!--[if (gte IE 9)|!(IE)]><!--><html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>"><!--<![endif]-->
<head>
<?php
//echo "<pre>";
//print_r($this->cart->getProducts());
//die();
?>
<meta charset="UTF-8" />
<title><?php echo $title; ?></title>

<base href="<?php echo $base; ?>" />
<!-- Palm -->
<meta name="HandheldFriendly" content="True" />
<!-- Windows -->
<meta name="MobileOptimized" content="320" />
<!-- Safari, Android, BB, Opera -->
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no">
<meta name="apple-mobile-web-app-status-bar-style" content="black-translucent"/>
<meta name="apple-mobile-web-app-capable" content="yes"/>
<meta name="mobile-web-app-capable" content="yes">
<!-- Use the .htaccess and remove these lines to avoid edge case issues. -->
<meta http-equiv="Content-type" content="text/html; charset=utf-8" />
<!--<meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1" />-->

<?php if ($icon) { ?>
<link rel="icon" sizes="32x32" href="<?php echo $icon; ?>">
<link rel="icon" sizes="60x60" href="/image/data/<?php echo $icon; ?>-60x60.png">
<link rel="icon" sizes="120x120" href="/image/data/<?php echo $icon; ?>-120x120.png">
<link rel="icon" sizes="152x152" href="/image/data/<?php echo $icon; ?>-152x152.png">
<link rel="icon" sizes="192x192" href="/image/data/<?php echo $icon; ?>-192x192.png">
<?php } ?>

<?php if ($description) { ?>
<meta name="description" content="<?php echo $description; ?>" />
<meta name="og:description" content="<?php echo $description; ?>" />
<?php } ?>
<?php if ($keywords) { ?>
<meta name="keywords" content="<?php echo $keywords; ?>" />
<?php } ?>

<?php foreach ($links as $link) { ?>
<link href="<?php echo $link['href']; ?>" rel="<?php echo $link['rel']; ?>" />
<?php } ?>
<?php
    if ($route != "information/information") {
?> 
<link rel="stylesheet" type="text/css" href="catalog/view/theme/<?php echo $this->config->get('config_template') ?>/stylesheet/stylesheet.css?v=10092014" />
<link rel="stylesheet" type="text/css" href="catalog/view/theme/<?php echo $this->config->get('config_template') ?>/stylesheet/multiseller.css" />
<?php } ?>
<?php foreach ($styles as $style) { ?>
<link rel="<?php echo $style['rel']; ?>" type="text/css" href="<?php echo $style['href']; ?>" media="<?php echo $style['media']; ?>" />
<?php } ?>
<link rel="stylesheet" type="text/css" href="catalog/view/theme/<?php echo $this->config->get('config_template') ?>/stylesheet/responsive.css" />

<!-- Megnor - Start-->
<?php if (isset($this->request->server['HTTPS']) && (($this->request->server['HTTPS'] == 'on') || ($this->request->server['HTTPS'] == '1'))) {
$http_type = "https:";} else {$http_type = "http:";} 
 ?>

<link href="//netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css" rel="stylesheet">
 
<link rel="stylesheet" type="text/css" href="catalog/view/theme/<?php echo $this->config->get('config_template') ?>/stylesheet/megnor/custom.css" />
<link rel="stylesheet" type="text/css" href="catalog/view/theme/<?php echo $this->config->get('config_template') ?>/stylesheet/colorbox.css" />
<link rel="stylesheet" type="text/css" href="catalog/view/theme/<?php echo $this->config->get('config_template') ?>/stylesheet/megnor/carousel.css" />
<link rel="stylesheet" type="text/css" href="catalog/view/theme/<?php echo $this->config->get('config_template') ?>/stylesheet/megnor/responsive.css" />
<?php if($direction=='rtl'){ ?>
<link rel="stylesheet" type="text/css" href="catalog/view/theme/<?php echo $this->config->get('config_template') ?>/stylesheet/megnor/rtl.css">
<?php }?>

<?php if(isset($_GET['print'])){ ?>
<link rel="stylesheet" type="text/css" href="catalog/view/theme/<?php echo $this->config->get('config_template') ?>/stylesheet/print.css">
<?php }?>

<!--[if lt IE 9]><script type="text/javascript" src="catalog/view/javascript/megnor/html5.js"></script><![endif]-->

<!-- jquery -->
<script type="text/javascript" src="catalog/view/javascript/jquery/jquery-1.7.1.min.js"></script>
<!-- <script type="text/javascript" src="catalog/view/javascript/jquery/jquery-1.11.2.js"></script>  -->

<!-- jquery-ui -->
<script type="text/javascript" src="catalog/view/javascript/jquery/ui/jquery-ui-1.8.16.custom.min.js"></script>
<link rel="stylesheet" type="text/css" href="catalog/view/javascript/jquery/ui/themes/ui-lightness/jquery-ui-1.8.16.custom.css" />

<script type="text/javascript" src="catalog/view/javascript/jquery.colorbox-min.js"></script>
<script type="text/javascript" src="catalog/view/javascript/common.js"></script>

<!-- Megnor - Start -->
<script type="text/javascript" src="catalog/view/javascript/megnor/carousel.min.js"></script>
<script type="text/javascript" src="catalog/view/javascript/megnor/megnor.min.js"></script>
<script type="text/javascript" src="catalog/view/javascript/megnor/custom.js"></script>
<script type="text/javascript" src="catalog/view/javascript/megnor/jquery.custom.min.js"></script>
<script type="text/javascript" src="catalog/view/javascript/megnor/scrolltop.min.js"></script>
<script type="text/javascript" src="catalog/view/javascript/megnor/jquery.formalize.min.js"></script> 
<script type="text/javascript" src="catalog/view/javascript/megnor/jstree.min.js"></script> 
<script type="text/javascript" src="catalog/view/javascript/megnor/cloudzoom.js"></script> 
<script type="text/javascript" src="catalog/view/javascript/megnor/fancybox.js"></script>
<!-- Megnor - End -->

<link rel="stylesheet" type="text/css" href="catalog/view/theme/<?php echo $this->config->get('config_template') ?>/stylesheet/homeals/jPushMenu.css" />
<script type="text/javascript" src="catalog/view/javascript/homeals/jPushMenu.js"></script>

<link rel="stylesheet" type="text/css" href="catalog/view/theme/<?php echo $this->config->get('config_template') ?>/stylesheet/homeals/idangerous.swiper.css" />
<script type="text/javascript" src="catalog/view/javascript/homeals/idangerous.swiper.js"></script>


<?php foreach ($scripts as $script) { ?>
<script type="text/javascript" src="<?php echo $script; ?>"></script>
<?php } ?>

<!--[if lt IE 7]>
<script type="text/javascript" src="catalog/view/javascript/DD_belatedPNG_0.0.8a-min.js"></script>
<script type="text/javascript">
DD_belatedPNG.fix('#logo img');
</script>
<![endif]-->

<?php if ($stores) { ?>
<script type="text/javascript"><!--
$(document).ready(function() {<?php foreach ($stores as $store) { ?>$('body').prepend('<iframe src="<?php echo $store; ?>" style="display: none;"></iframe>');<?php } ?>});//--></script>
<?php } ?>

<?php echo $google_analytics; ?>
<!-- TemplateMela www.templatemela.com - Start -->
<!--[if lt IE 9]><script type="text/javascript" src="catalog/view/javascript/megnor/respond.min.js"></script><![endif]-->
<!-- TemplateMela www.templatemela.com - End -->

<?php if ($route == 'account/thankyou' ) { ?>
<!-- Facebook Conversion Code for New Users -->
<script>(function() {
  var _fbq = window._fbq || (window._fbq = []);
  if (!_fbq.loaded) {
    var fbds = document.createElement('script');
    fbds.async = true;
    fbds.src = '//connect.facebook.net/en_US/fbds.js';
    var s = document.getElementsByTagName('script')[0];
    s.parentNode.insertBefore(fbds, s);
    _fbq.loaded = true;
  }
})();
window._fbq = window._fbq || [];
window._fbq.push(['track', '6019669271863', {'value':'0.00','currency':'ILS'}]);
</script>
<noscript><img height="1" width="1" alt="" style="display:none" src="https://www.facebook.com/tr?ev=6019669271863&amp;cd[value]=0.00&amp;cd[currency]=ILS&amp;noscript=1" /></noscript>

<?php } ?>

<?php if ($route == 'checkout/success' ) { ?>
<!-- Facebook Conversion Code for Buyers -->
<script>(function() {
  var _fbq = window._fbq || (window._fbq = []);
  if (!_fbq.loaded) {
    var fbds = document.createElement('script');
    fbds.async = true;
    fbds.src = '//connect.facebook.net/en_US/fbds.js';
    var s = document.getElementsByTagName('script')[0];
    s.parentNode.insertBefore(fbds, s);
    _fbq.loaded = true;
  }
})();
window._fbq = window._fbq || [];
window._fbq.push(['track', '6019669301263', {'value':'0.00','currency':'ILS'}]);
</script>
<noscript><img height="1" width="1" alt="" style="display:none" src="https://www.facebook.com/tr?ev=6019669301263&amp;cd[value]=0.00&amp;cd[currency]=ILS&amp;noscript=1" /></noscript>
<?php } ?>


<script>(function() {
  var _fbq = window._fbq || (window._fbq = []);
  if (!_fbq.loaded) {
    var fbds = document.createElement('script');
    fbds.async = true;
    fbds.src = '//connect.facebook.net/en_US/fbds.js';
    var s = document.getElementsByTagName('script')[0];
    s.parentNode.insertBefore(fbds, s);
    _fbq.loaded = true;
  }
  _fbq.push(['addPixelId', '1514996635400862']);
})();
window._fbq = window._fbq || [];
window._fbq.push(['track', 'PixelInitialized', {}]);
</script>
<script>
$(function(){


var availableTags = [
 <?php foreach($rest_list as $the_rest){ ?>
  
  <?php echo '"' . $the_rest . '",'  ?>


<?php } ?>
    
    
];
    
    $( "#input-search-homeals" ).autocomplete({
      source: availableTags
    });
});


</script>    
    
    
<noscript><img height="1" width="1" alt="" style="display:none" src="https://www.facebook.com/tr?id=1514996635400862&amp;ev=NoScript" /></noscript>


<?php 
if(isset($option_notes_styles) && !empty($option_notes_styles)){
?>
<style type="text/css">
<?php
    echo $option_notes_styles;
?>
</style>
<?php
}
?>


				<script type="text/javascript" src="catalog/view/javascript/common_ajaxcart.js"></script>
				<link rel="stylesheet" type="text/css" href="catalog/view/javascript/jquery/fancybox/jquery.fancybox.css" media="screen" />
				<script type="text/javascript" src="catalog/view/javascript/jquery/fancybox/jquery.fancybox.pack.js"></script>
			
</head>
<body class="<?php echo $body_class; ?>">

<!--[if lte IE 7]>

<div class="ie-con">
 
    <div class="ie-msg">
       
       <div class="ie-title">
           ברוכים הבאים ל-Quiky       </div>
       <div class="ie-text-block">
           <p>
           בכדי ליהנות מחווית גלישה אופטימלית אנחנו ממליצים על צפייה באתר באחד מהדפדפנים המתקדמים כגון גוגל כרום, פיירפוקס או אקספלורר מגרסה 10 ומעלה.
           </p>
           <p>
           הינכם משתמשים בדפדפן מגרסה ישנה וייתכן כי חלק מהאפשרויות באתר לא יעבדו בצורה מיטבית.
           </p>
           <p>
           אם אין באפשרותכם לשדרג את גרסת הדפדפן ואתם מעוניינים לבצע הזמנה מאחד הבשלנים, צוות התמיכה שלנו יישמח לסייע לכם בכל שעה בטלפון 03-3741886 או במייל support@quiky.co.il.           </p>
           <p>
           תודה ובתיאבון
           </p>
           <p>
           צוות Quiky
           </p>
       </div>
       <div class="action-buttons-con">
           <a href="https://www.google.com/intl/en/chrome/browser/" id="chrome" target="_blank">המשך</a>
       </div>
       
       <div style="clear: both"></div>
   
   </div>   
    
</div>


<![endif]-->

<!-- Left menu element-->
<nav class="cbp-spmenu cbp-spmenu-vertical cbp-spmenu-left">
	<div class="menu_head">
            <a href="<?php echo $home.(($this->customer->isLogged()) ? "&allow" : ""); ?>">
                <img src="/image/data/homeals_logo.png" title="<?php echo $name; ?>" alt="<?php echo $name; ?>" />
            </a>
        </div>
        <a href="index.php?route=product/category&path=0" >חפש ארוחות</a>
	<a class="a_menu_needhelp">צור קשר</a>
        <div class="menu-line" id="menu-needhelp">
            <div>
                <div class="image"></div>
                <span id="call-us">
                <?php echo $text_call_us; ?>
                <?php echo $support_number; ?>
                </span>
            </div>
            <div>
                <div class="image" id="letter"></div>
                <span id="write-us">
                <?php echo $text_write_us; ?>
                <a href="mailto:<?php echo $support_mail; ?>" ><?php echo $support_mail; ?></a>
                </span>
            </div>
        </div>
        
        <?php if (!$logged) { ?>
        
            <a id="nav_open_login"><?php echo $text_login; ?></a>
            <a id="nav_open_signin"><?php echo $text_signup; ?></a>
        <?php } else { ?>
            <div class="menu-line show" id="menu-user">
                <div>
                    <img class="avatar" src="<?php echo $image ?>" />
                    
                    <div id="user-name">
                        <?php echo $first_name; ?><br>
                        <?php echo $last_name; ?>
                    </div>
                    
                </div>
            </div>
            
            <div style="display: none;" id="menu-user-menu">
                <a id="go-to-inbox" href="<?php echo $href_inbox; ?>">
                    <?php echo $text_inbox; ?>
                </a>
                <?php if($seller) { ?> 
                <a id="go-to-orders" href="<?php echo $my_orders_url; ?>">
                    <?php echo $text_my_orders; ?>
                </a>
                <?php } ?>
                <a id="nav-log-me-out">
                    <?php echo $text_logout; ?>
                </a>
            </div>
        <?php } ?>
        
</nav>



<div class="loading-screen" id="confirmLoader" ></div>

<?php
    if ($route == 'product/product' || $route == 'common/home' || $route == false) {
?>
<div id="hp-load" class="loading-screen" style="width: 100%; height: 100%; opacity: 1; display: block"></div>
<?php } ?>



<div id="container">

<?php $route = (isset($_GET['route']) ? $_GET['route'] : false) ?>

<div id="header-placeholder" >

</div>
<header id="header-container" class="fixed">
<div id="header-under-con">
	<div id="header">
		<div class="header-homeals">
			<?php if ($logo) { ?>
				<div id="logo-homeals">
                    <a href="<?php echo $home.(($this->customer->isLogged()) ? "&allow" : ""); ?>">
                    <img src="<?php echo $logo; ?>" title="<?php echo $name; ?>" alt="<?php echo $name; ?>" />
                    </a>
                </div>			
			<?php } ?>

		<div id="welcome-homeals">
                    <ul class="quiky-menu">
                        <li id="about">
                            <a href="#"><?php echo $text_about; ?></a>
                        </li>
                        <li id="rest-list">
                            <a href="index.php?route=product/quickyCategory"><?php echo $text_rest_list; ?></a>
                        </li>
                        <li id="how-it-works">
                            <a href="#"><?php echo $text_how_it_works; ?></a>
                        </li>
                        <li id="policy">
                            <a href="#"><?php echo $text_policy; ?></a>
                        </li>
                        <li class="facebook">
                            <a href="#"><img alt="<?php echo $text_like_on_facebook; ?>" src="/image/data/facebook_icon.png" /></a>
                        </li>
                    </ul>
                    
                    
                    <!-- class in will be hiden from site  was in old-quiky if you know what i mean-->
                    <div class="in" style="display:none;">  
                        <div id="user-side" class="inner"> 
                        <?php if (!$logged) { ?>
                        
                        <!--    <a id="open_login" href="#loginbox" class="cboxElement"><?php echo $text_login; ?></a>
                            <a id="open_signin" href="#signinbox" class="cboxElement"><?php echo $text_signup; ?></a>-->
                            
                        <?php } else { ?>
                        
                        <div class="header-avatar">
                            <img class="avatar" src="<?php echo $image ?>" />
                        </div>
                        
                        <a id="open-logout-box">
                            <div id="user-name">
                            <?php echo $first_name; ?><br>
                            <?php echo $last_name; ?>
                            </div>
                        </a>
                        
                        <div class="logout-box">
                        <div id="spitz"></div>
                            <a id="go-to-inbox" href="<?php echo $href_inbox; ?>">
                                <div class="boxy">
                                <?php echo $text_inbox; ?>
                                </div>
                            </a>
                            <a id="go-to-my-orders" href="<?php echo $customer_my_orders_url; ?>">
                                <div class="boxy">
                                <?php echo $text_customer_my_orders; ?>
                                </div>
                            </a>
                            <?php if($seller) { ?> 
                            <a id="go-to-orders" href="<?php echo $my_orders_url; ?>">
                                <div class="boxy">
                                <?php echo $text_my_orders; ?>
                                </div>
                            </a>
                            <?php } ?>
                            <a id="change-pass" href="index.php?route=account/password" >
                                <div class="boxy">
                                <?php echo $change_my_pass; ?>
                                </div>
                            </a>
                            <a id="log-me-out">
                                <div class="boxy">
                                <?php echo $text_logout; ?>
                                </div>
                            </a>
                        </div>
                    <?php } ?>
                </div>
                
                <?php if ($logged) { ?>
                <div id="inbox-side" class="inner"> 
                    <a id="open_inbox" href="<?php echo $href_inbox; ?>" ><?php echo $text_inbox_s; ?>
                    
                    <?php if ($num_unread) { ?>
                    <div id="unread"><?php echo $num_unread; ?></div>
                    <?php } ?>
                    
                    </a>
                </div>
                <?php } ?>
                
                <div id="need-help-con"  class="inner">
                    <a id="need-help" class="need-help" ><?php echo $text_need_help; ?></a>
                    <div id="need-help-body" class="need-help-box">
                        <div id="spitz"></div>
                        <div class="image"></div>
                        <span id="call-us">
                        <?php echo $text_call_us; ?>
                        <b><?php echo $support_number; ?></b>
                        </span><br>
                        <div class="image" id="letter"></div>
                        <span id="write-us">
                        <?php echo $text_write_us; ?>
                        <b><a href="mailto:<?php echo $support_mail; ?>" ><?php echo $support_mail; ?></a></b>
                        </span>
                        
                        
                        </div>
                    </div>
                </div>
            </div>
             
            <div id="search-homeals">
                
                <?php if(false) { ?> <!-- if false start -->
                
                <div id="search-homeals-inner">
                <a id="open-address-box">
                <div id="button-address-book"></div>
                </a>
                
                <?php if(!empty($addresses)) { ?>
                
                <div class="address-box">
                    <div id="spitz"></div>
                    
                    <?php foreach($addresses as $address) { ?> 
                    <a href="index.php?route=product/category&path=0&defaddress=<?php echo $address['address_id'] ?>" id="<?php echo $address['address_id'] ?>" class="set-address" >
                        <div class="address-div">
                        <?php
                            if(empty($address['company'])){
                                echo mb_truncate($address['address_1'],35);
                            } else {
                                echo mb_truncate($address['address_1']." (".$address['company'].")",35);
                            }
                        ?>
                        
                        <i class="fa fa-chevron-left left-arrow"></i>
                        </div>
                    </a>
                    <?php } ?>
                </div>
                
                <?php } else { ?>
                
                <div class="address-box">
                    <div id="spitz"></div>
                    <div class="address-div">
                    <?php if ($logged) { ?> 
                        <?php echo $text_no_saved_addresses; ?>
                    <?php } else { ?>
                        <?php echo $text_log_in_addresses; ?>
                    <?php }?>
                    </div>
                </div>
                
                <?php } ?> <!-- if false end -->
                
        <div class="ui-widget">        
		<input id="input-search-homeals" type="text" placeholder="<?php echo !($street_name == "") ? $street_name : $text_search; ?>" onfocus="this.placeholder = ''" onblur="this.placeholder = '<?php echo !($street_name == "") ? $street_name : $text_search; ?>'" value="" />
        </div>
		<div class="button-search-homeals"><?php echo $text_log_in_addresses; ?></div>
                </div>
                
                <?php } ?>
               <?php if (!$logged) { ?>
                <a id="open_login_new" href="#loginbox" class="cboxElement">
                    <div class="login-returning">
                        <?php echo $text_login_return; ?>
                    </div>
                </a>
                <?php }else{ ?>
                        
                        
                        <div class="header-avatar">
                            <img class="avatar" src="<?php echo $image ?>" />
                        </div>
                        
                        <a id="open-logout-box">
                            <div id="user-name">
                            <?php echo $first_name; ?><br>
                            <?php echo $last_name; ?>
                            </div>
                        </a>
                        
                        <div class="logout-box" style="display:none; ">
                        <div id="spitz"></div>
                            <a id="go-to-inbox" href="<?php echo $href_inbox; ?>">
                                <div class="boxy" style="display:none">
                                <?php echo $text_inbox; ?>
                                </div>
                            </a>
                            <a id="go-to-my-orders" href="<?php echo $customer_my_orders_url; ?>">
                                <div class="boxy">
                                <?php echo $text_customer_my_orders; ?>
                                </div>
                            </a>
                            <?php if($seller) { ?> 
                            <a id="go-to-orders" href="<?php echo $my_orders_url; ?>">
                                <div class="boxy">
                                <?php echo $text_my_orders; ?>
                                </div>
                            </a>
                            <?php } ?>
                            <a id="change-pass" href="index.php?route=account/password" >
                                <div class="boxy">
                                <?php echo $change_my_pass; ?>
                                </div>
                            </a>
                            <a id="log-me-out">
                                <div class="boxy">
                                <?php echo $text_logout; ?>
                                </div>
                            </a>
                        </div>
                    <?php } ?>
                
                <div class="search-controller">
                    <input id="input-search-homeals" type="text" placeholder="<?php echo $placeholder_search ?>" onfocus="this.placeholder = ''" onblur="this.placeholder = '<?php echo $placeholder_search ?>'" value="" />
                    <div class="button-search-homeals"><?php echo $text_find_meals; ?></div>
                </div>
                <!-- input-search-homeals -->
            </div>
                        
            <div style="clear:both;"></div>
			
        </div>
		
		
		
    </div>
</div>

<?php
   
	if (isset($_GET['route'])) {
		$route = $_GET['route'];
                $cook_page =  ($curr_cook != 0) ? "true" : "false" ;
                
                // killing it softly with a false at the end
	    if ($route == "product/category" && $cook_page != "true" && false) {
            
                if (isset($this->session->data['date']))  {
                    $choosen_date = date("m/d/y",strtotime($this->session->data['date']));
                }else{
                    $choosen_date = date("m/d/y",strtotime("today"));
                }
                
                setlocale(LC_ALL, 'he_IL.UTF-8');
                
                //  strftime("%A, %d %B",strtotime($choosen_date));
?>

<div id="cat-header-con">
	<div id="cat-header">
		<div class="cat-header-right clearfix">
			<div class="search-result"><?php echo ($text_street ? $text_street : "תל אביב" ); ?></div>
			<div class="search-result-items"><?php echo $text_resualts ?> <?php echo strftime("%A, ".$date_strf,strtotime($choosen_date)); ?></div>
		</div>
		<div class="cat-header-left clearfix">
                    <?php
                        $i = 0;
                        $j = 0;
                        for($i;$i<5;$i++){
                            ?>
                            <?php $ex_text = isset($_GET['exclude']) ? "&exclude=".$_GET['exclude'] : ""; ?>
                            <a href="?route=product/category&path=0<?php echo $ex_text ?>&day=<?php echo date('m/d/y', strtotime('+'.$days_more[$i].' day')); ?>">
                            <div class="day-con <?php if (isset($choosen_date) && ($choosen_date == date('m/d/y', strtotime('+'.$days_more[$i].' day')))) echo "selected";?>">
                            <div class="weekday <?php if (isset($choosen_date) && ($choosen_date == date('m/d/y', strtotime('+'.$days_more[$i].' day')))) echo "selected";?> ">
                                    <div class="day-name"><? echo $day[date('w', strtotime('+'.$days_more[$i].' day'))]; ?></div>
                                    <div class="day-date"><? echo strftime($date_strf,strtotime('+'.$days_more[$i].' day')); ?></div>
                            </div>
                            </div>
                            </a>
                        <?php
                        }
                    ?>
                    <div class="line"></div>
		</div>
		<div style="clear: both"></div>
	</div>
</div>

<?php } else if ($route == "information/information" && $_GET['information_id'] != 5 && $_GET['information_id'] != 3) { ?>
<div id="info-header-con">
	<div id="info-header">
		<div class="info-header-left clearfix">
            
            <?php foreach ($informations as $information) { ?>                        
            <a href="<?php echo $information['href']; ?>">
            <div class="day-con <?php echo (($_GET['information_id'] == $information['id']) ? "selected" : "" ); ?>" >
            <div class="weekday">
                    <div class="info-name"><?php echo $information['title']; ?></div>
            </div>
            </div>
            </a>
            <?php } ?>
            
            <div class="line"></div>
		</div>
		<div style="clear: both"></div>
	</div>
</div>  
<?php   } else if ($route == "information/information") { ?>
        
<div id="info-header-con">
	<div id="info-header">
		<div class="info-header-left clearfix">
            
            <a href="?route=information/information&information_id=5">
            <div class="day-con <?php echo (($_GET['information_id'] == 5) ? "selected" : "" ); ?>" >
            <div class="weekday">
                    <div class="info-name"><?php echo $text_terms; ?></div>
            </div>
            </div>
            </a>
            <a href="?route=information/information&information_id=3">
            <div class="day-con <?php echo (($_GET['information_id'] == 3) ? "selected" : "" ); ?>" >
            <div class="weekday">
                    <div class="info-name"><?php echo $text_privet; ?></div>
            </div>
            </div>
            </a>
            
            <div class="line"></div>
		</div>
		<div style="clear: both"></div>
	</div>
</div>  
<?php
    }
}
?>

<div class="headershadow"></div>


</header>
<?php if ($route == "information/information") { ?>

<div style="height: 70px; width: 100%"></div>

<?php if (!$logged) { ?>
<div id="hp-header-links" style="display: none">
    <div class="hp-joinus">הצטרפו כבשלנים</div>
    <div class="hp-login">התחבר</div>
    <div class="hp-howitworks">איך זה עובד?</div>
</div>

<a style="display: none" id="open_login" href="#loginbox" class="cboxElement"><?php echo $text_login; ?></a>
<a style="display: none" id="open_signin" href="#signinbox" class="cboxElement"><?php echo $text_signup; ?></a>

<script>
$(document).ready(function() {
    $("#welcome-homeals").html($("#hp-header-links").html());
    $("#hp-header-links").remove();
    $("#welcome-homeals").attr("id","hp-header-links");
    
    $("#open_login").colorbox({inline:true});
    $("#open_signin").colorbox({inline:true});
});
</script>
<?php } ?>

<?php } ?>
 
<!-- section code start -->
<section id='content-wrapper'>

<div class="content-inner">

<?php if ($error) { ?>
<div class="warning"><?php echo $error ?><img src="catalog/view/theme/default/image/close.png" alt="" class="close" /></div>
<?php } ?>
<?php echo $facebook; ?>
    <div id="notification"></div>
<div style="display: none">
    <div id="fb-auth"></div>
    
    <div id="msg-success">
        <div class="success-text">
            ההודעה נשלחה.
            <br /><br />
            הודעות שתקבל/י יופיעו בתיבת הדואר אליה ניתן לגשת מהתפריט האישי שלך.
        </div>
        <button class="close" onclick="$.colorbox.close();">סגור</button>
        <div style="clear: both"></div>
    </div>
    
    <div id="loginbox" class="loginbox">
        <div class="logbox-top"><?php echo $text_loginboxtop ?> <span class="logbox-bottom" ><?php echo $text_loginboxbottom ?></span></div>
        <div class="fbButton"></div>
        <div class="logbox-logincon">
        
        <div class="logbox-or"><?php echo $text_or ?></div>
        <!--<div class="logbox-text"><?php echo $text_logintext ?></div>-->
        
        <form action="http://homeals.com/index.php?route=account/login" method="post" enctype="multipart/form-data">
            
        <div class="logbox-email">
            <input type="email" class="logboxinput" name="email" id="loginemail" value="" placeholder="<?php echo $text_loginemail ?>" />
        </div>
        <div class="logbox-pass">
            <input type="password" class="logboxinput" name="password" id="loginpass" value="" placeholder="<?php echo $text_loginpass ?>" />
        </div>
        
        <div class="logbox-warning"><?php echo $text_loginerror ?></div>
        <div class="logbox-remember">
            <span><input type="checkbox" name="rememberme" id="rememberme" class="not"/><?php echo $text_loginremember ?></span>
            <span><?php echo $text_loginforgot ?></span>
        </div>
        <div class="logbox-login">
            <input type="button" class="logboxinput" name="loginbutton" id="loginbutton" value="<?php echo $text_login ?>" />
        </div>
        
        </form>
        
        
        </div>
        <div class="logbox-bottom"></div>
    </div>
    
    <div id="signinbox" class="loginbox">
        <div class="logbox-top"><?php echo $text_signinboxtop ?> <span class="logbox-bottom"><?php echo $text_signinboxbottom ?></span></div>
        <div class="fbButton"></div>
        <div class="logbox-logincon">
        
            <div class="logbox-or"><?php echo $text_or ?></div>
            <!--<div class="logbox-text"><?php echo $text_signintext ?></div>-->
            <div class="logbox-firstname">
                <input type="text" class="logboxinput" name="signinfirstname" id="signinfirstname" placeholder="<?php echo $text_signinfirstname ?>" />
            </div>
            <div class="logbox-lastname">
                <input type="text" class="logboxinput" name="signinlastname" id="signinlastname" placeholder="<?php echo $text_signinlastname ?>" />
            </div>
            <div class="logbox-city">
                <input type="text" class="logboxinput" name="signincity" id="signincity" placeholder="<?php echo $text_signincity ?>" />
            </div>
            <div class="logbox-email">
                <input type="text" class="logboxinput" name="signinemail" id="signinemail" placeholder="<?php echo $text_loginemail ?>" />
            </div>
            <div class="logbox-pass">
                <input type="password" class="logboxinput" name="signinpass" id="signinpass" placeholder="<?php echo $text_loginpass ?>" />
                <input type="text" class="logboxinput" name="sfpass" id="sfpass" placeholder="<?php echo $text_loginpass ?>" style="display: none" />
            </div>
            <div class="logbox-join-list">
                <span><input type="checkbox" name="join_list" id="join_list" value="yes" checked="checked" /><?php echo $text_joinlist ?></span>
            </div>
            <div class="logbox-login">
                <input type="button" class="logboxinput" name="signinbutton" id="signinbutton" value="<?php echo $text_signin ?>" />
            </div>
            
            <div class="logbox-bottom">
                <div id="signinemail-error" style="display: none"><?php echo $text_error_mail; ?></div>
                <div id="signinemail-error-exists" style="display: none"><?php echo $text_error_mail_exists; ?></div>
                <div id="signinpass-error" style="display: none"><?php echo $text_error_pass; ?></div>
                <div id="signinfirstname-error" style="display: none"><?php echo $text_error_firstname; ?></div>
                <div id="signincity-error" style="display: none"><?php echo $text_error_city; ?></div>
                <div id="signinlastname-error" style="display: none"><?php echo $text_error_lastname; ?></div>
            </div>
            
            <div class="logbox-signinagree">
                <span><?php echo $text_signinagree ?></span>
            </div>
        
        </div>
        
    </div>

</div>
