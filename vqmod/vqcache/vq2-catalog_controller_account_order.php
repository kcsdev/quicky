<?php 
class ControllerAccountOrder extends Controller {
	private $error = array();
		

				public function renderSellerRateDialog() {
					if (isset($this->request->get['order_id'])) {
						$order_id = (int)$this->request->get['order_id'];
					} else {
						return 0;
					}
					
					$this->data = array_merge($this->data, $this->load->language('multiseller/multiseller'));
					
					$order_sellers = $this->MsLoader->MsOrderData->getOrderSellers(array('order_id' => $order_id));
					
					$this->data['sellers'] = array();
					foreach ($order_sellers['sellers'] as $order_seller) {
						$seller_id = $order_seller['seller_id'];
						$seller = $this->MsLoader->MsSeller->getSeller($seller_id);
						if (empty($seller))
							return false;
						
						$seller_record = array(
							'seller_id' 	=> $seller_id,
							'seller_thumb' 	=> (!empty($seller['ms.avatar'])) ? $this->MsLoader->MsFile->resizeImage($seller['ms.avatar'], $this->config->get('config_image_category_width'), $this->config->get('config_image_category_height')) : $this->MsLoader->MsFile->resizeImage('ms_no_image.jpg', $this->config->get('config_image_category_width'), $this->config->get('config_image_category_height')),
							'seller_href' 	=> $this->url->link('seller/catalog-seller/profile', 'seller_id=' . $seller_id),
							'seller_nick' 	=> $seller['ms.nickname'],
							'products' 		=> $this->MsLoader->MsOrderData->getOrderProducts(array('order_id' => $order_id, 'seller_id' => $seller_id)),
							'rating' 		=> $this->MsLoader->MsSeller->getRate(array('order_id' => $order_id, 'seller_id' => $seller_id))
						);
						$this->data['sellers'][] = $seller_record;
					}
					$this->data['order_id'] = $order_id;
					
					list($this->template, $this->children) = $this->MsLoader->MsHelper->loadTemplate('dialog-seller-rate');
					return $this->response->setOutput($this->render());
				}
			
	public function index() {
    	if (!$this->customer->isLogged()) {
      		$this->session->data['redirect'] = $this->url->link('account/order', '', 'SSL');
		
	  	$this->redirect($this->url->link('account/login', '', 'SSL'));
    	}
		
		$this->language->load('account/order');

				$this->language->load('multiseller/multiseller');
				$this->data['ms_rate'] = $this->language->get('ms_rate');
				//$this->data = array_merge($this->data, $this->load->language('multiseller/multiseller'));
			
		
		$this->load->model('account/order');
 		
		if (isset($this->request->get['order_id'])) {
			$order_info = $this->model_account_order->getOrder($this->request->get['order_id']);
			
			if ($order_info) {
				$order_products = $this->model_account_order->getOrderProducts($this->request->get['order_id']);
						
				foreach ($order_products as $order_product) {
					$option_data = array();
							
					$order_options = $this->model_account_order->getOrderOptions($this->request->get['order_id'], $order_product['order_product_id']);
							
					foreach ($order_options as $order_option) {
						if ($order_option['type'] == 'select' || $order_option['type'] == 'radio') {
							$option_data[$order_option['product_option_id']] = $order_option['product_option_value_id'];
						} elseif ($order_option['type'] == 'checkbox') {
							$option_data[$order_option['product_option_id']][] = $order_option['product_option_value_id'];
						} elseif ($order_option['type'] == 'text' || $order_option['type'] == 'textarea' || $order_option['type'] == 'date' || $order_option['type'] == 'datetime' || $order_option['type'] == 'time') {
							$option_data[$order_option['product_option_id']] = $order_option['value'];	
						} elseif ($order_option['type'] == 'file') {
							$option_data[$order_option['product_option_id']] = $this->encryption->encrypt($order_option['value']);
						}
					}
							
					$this->session->data['success'] = sprintf($this->language->get('text_success'), $this->request->get['order_id']);
							
					$this->cart->add($order_product['product_id'], $order_product['quantity'], $option_data);
				}
									
				$this->redirect($this->url->link('checkout/cart'));
			}
		}

    	$this->document->setTitle($this->language->get('heading_title'));

      	$this->data['breadcrumbs'] = array();

      	$this->data['breadcrumbs'][] = array(
        	'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home'),        	
        	'separator' => false
      	); 

      	$this->data['breadcrumbs'][] = array(
        	'text'      => $this->language->get('text_account'),
			'href'      => $this->url->link('account/account', '', 'SSL'),        	
        	'separator' => $this->language->get('text_separator')
      	);
		
		$url = '';
		
		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}
				
      	$this->data['breadcrumbs'][] = array(
        	'text'      => $this->language->get('heading_title'),
			'href'      => $this->url->link('account/order', $url, 'SSL'),        	
        	'separator' => $this->language->get('text_separator')
      	);
		
		
		
		$this->data['heading_title'] = $this->language->get('heading_title');
		
		$this->data['text_order_id'] = $this->language->get('text_order_id');
		$this->data['text_status'] = $this->language->get('text_status');
		$this->data['text_date_added'] = $this->language->get('text_date_added');
		$this->data['text_customer'] = $this->language->get('text_customer');
		$this->data['text_products'] = $this->language->get('text_products');
		$this->data['text_total'] = $this->language->get('text_total');
		$this->data['text_empty'] = $this->language->get('text_empty');

		$this->data['button_view'] = $this->language->get('button_view');
		$this->data['button_reorder'] = $this->language->get('button_reorder');
		$this->data['button_continue'] = $this->language->get('button_continue');
		
		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}
		
		$this->data['orders'] = array();
		
		$order_total = $this->model_account_order->getTotalOrders();
		
		$results = $this->model_account_order->getOrders(($page - 1) * 10, 10);
		
		foreach ($results as $result) {
			$product_total = $this->model_account_order->getTotalOrderProductsByOrderId($result['order_id']);
			$voucher_total = $this->model_account_order->getTotalOrderVouchersByOrderId($result['order_id']);

			
				$this->data['orders'][] = array(
					'status_order'   => $result['order_status_id'] == 5 /* Complete */ ? true : false,	
			
				'order_id'   => $result['order_id'],
				'name'       => $result['firstname'] . ' ' . $result['lastname'],
				'status'     => $result['status'],
				'date_added' => date($this->language->get('date_format_short'), strtotime($result['date_added'])),
				'products'   => ($product_total + $voucher_total),
				'total'      => $this->currency->format($result['total'], $result['currency_code'], $result['currency_value']),
				'href'       => $this->url->link('account/order/info', 'order_id=' . $result['order_id'], 'SSL'),
				'reorder'    => $this->url->link('account/order', 'order_id=' . $result['order_id'], 'SSL')
			);
		}

		$pagination = new Pagination();
		$pagination->total = $order_total;
		$pagination->page = $page;
		$pagination->limit = 10;
		$pagination->text = $this->language->get('text_pagination');
		$pagination->url = $this->url->link('account/order', 'page={page}', 'SSL');
		
		$this->data['pagination'] = $pagination->render();

		$this->data['continue'] = $this->url->link('account/account', '', 'SSL');

		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/account/order_list.tpl')) {

				$this->document->addScript('catalog/view/javascript/dialog-seller-rate.js');
			
			$this->template = $this->config->get('config_template') . '/template/account/order_list.tpl';
		} else {
			$this->template = 'default/template/account/order_list.tpl';
		}
		
		$this->children = array(
			'common/column_left',
			'common/column_right',
			'common/content_top',
			'common/content_bottom',
			'common/footer',
			'common/header'	
		);
						
		$this->response->setOutput($this->render());				
	}
	
	public function info() { 
		$this->language->load('account/order');
		
		if (isset($this->request->get['order_id'])) {
			$order_id = $this->request->get['order_id'];
		} else {
			$order_id = 0;
		}	

		if (!$this->customer->isLogged()) {
			$this->session->data['redirect'] = $this->url->link('account/order/info', 'order_id=' . $order_id, 'SSL');
			//change to something else
			$this->redirect($this->url->link('account/login', '', 'SSL'));
		}
		
			
		$this->load->model('account/order');
			
		$order_info = $this->model_account_order->getOrder($order_id);
		
		if ($order_info) {
			$this->document->setTitle($this->language->get('text_order'));
			
			$this->data['my_orders_url'] =  $this->url->link('account/my-orders', '', 'SSL');
			
			$this->data['heading_title'] = $this->language->get('text_order');
			
			$this->data['text_order_detail'] = $this->language->get('text_order_detail');
			$this->data['text_invoice_no'] = $this->language->get('text_invoice_no');
			$this->data['text_order_id'] = $this->language->get('text_order_id');
			$this->data['text_date_added'] = $this->language->get('text_date_added');
			$this->data['text_shipping_method'] = $this->language->get('text_shipping_method');
			$this->data['text_shipping_address'] = $this->language->get('text_shipping_address');
			$this->data['text_payment_method'] = $this->language->get('text_payment_method');
			$this->data['text_payment_address'] = $this->language->get('text_payment_address');
			$this->data['text_history'] = $this->language->get('text_history');
			$this->data['text_comment'] = $this->language->get('text_comment');
	
			$this->data['column_name'] = $this->language->get('column_name');
			$this->data['column_model'] = $this->language->get('column_model');
			$this->data['column_quantity'] = $this->language->get('column_quantity');
			$this->data['column_price'] = $this->language->get('column_price');
			$this->data['column_total'] = $this->language->get('column_total');
			$this->data['column_action'] = $this->language->get('column_action');
			$this->data['column_date_added'] = $this->language->get('column_date_added');
			$this->data['column_status'] = $this->language->get('column_status');
			$this->data['column_comment'] = $this->language->get('column_comment');
				
			$this->data['button_return'] = $this->language->get('button_return');
			$this->data['button_continue'] = $this->language->get('button_continue');
		
			if ($order_info['invoice_no']) {
				$this->data['invoice_no'] = $order_info['invoice_prefix'] . $order_info['invoice_no'];
			} else {
				$this->data['invoice_no'] = '';
			}
			
			$this->data['order_id'] = $this->request->get['order_id'];
			
			$this->data['date_added'] = date("d/m | H:i", strtotime($order_info['date_added']));
			
			$this->data['contact'] = $order_info['payment_firstname']." ".$order_info['payment_lastname'];
			$this->data['phone'] = $order_info['payment_postcode'];
			
			$format = '{address_1}' . "\n" . '{address_2}' . "\n" . '{city}' . "\n" . '{company}';
		
			$find = array(
	  			'{firstname}',
	  			'{lastname}',
	  			'{company}',
				'{address_1}',
				'{address_2}',
				'{city}',
				'{postcode}',
				'{zone}',
				'{zone_code}',
				'{country}'
			);
	
			$replace = array(
	  			'firstname' => $order_info['payment_firstname'],
	  			'lastname'  => $order_info['payment_lastname'],
	  			'company'   => $order_info['payment_company'],
				'address_1' =>  str_replace ( "#" , "" , $order_info['payment_address_1'] ),
				'address_2' => $order_info['payment_address_2'],
				'city'      => $order_info['payment_city'],
				'postcode'  => $order_info['payment_postcode'],
				'zone'      => $order_info['payment_zone'],
				'zone_code' => $order_info['payment_zone_code'],
				'country'   => $order_info['payment_country']  
			);
			
			$this->data['payment_address'] = str_replace(array("\r\n", "\r", "\n"), '<br />', preg_replace(array("/\s\s+/", "/\r\r+/", "/\n\n+/"), '<br />', trim(str_replace($find, $replace, $format))));

			$this->data['payment_method'] = $order_info['payment_method'];
			
			
			// we need here to get paypal time
			// if not paypal we put order added time
			
			if($order_info['payment_code'] == "pp_express"){
				
				$status_name = $this->db->query("SELECT *  FROM " . DB_PREFIX . "paypal_order WHERE order_id = '" . (int)$order_id . "'");
				
				$this->data['payment_time'] = "שולם ב".date("d/m/Y, H:i", strtotime($status_name->row['created']));
			} else {
				$this->data['payment_time'] = "שולם ב".date("d/m/Y, H:i", strtotime($order_info['date_added']));
			}
			
			$format = '{address_1}' . "\n" . '{address_2}' . "\n" . '{city}' . "\n" . '{company}';
		
			$find = array(
	  			'{firstname}',
	  			'{lastname}',
	  			'{company}',
				'{address_1}',
				'{address_2}',
				'{city}',
				'{postcode}',
				'{zone}',
				'{zone_code}',
				'{country}'
			);
	
			$replace = array(
	  			'firstname' => $order_info['shipping_firstname'],
	  			'lastname'  => $order_info['shipping_lastname'],
	  			'company'   => $order_info['shipping_company'],
				'address_1' => $order_info['shipping_address_1'],
				'address_2' => $order_info['shipping_address_2'],
				'city'      => $order_info['shipping_city'],
				'postcode'  => $order_info['shipping_postcode'],
				'zone'      => $order_info['shipping_zone'],
				'zone_code' => $order_info['shipping_zone_code'],
				'country'   => $order_info['shipping_country']  
			);

			$this->data['shipping_address'] = str_replace(array("\r\n", "\r", "\n"), '<br />', preg_replace(array("/\s\s+/", "/\r\r+/", "/\n\n+/"), '<br />', trim(str_replace($find, $replace, $format))));

			$this->data['shipping_method'] = $order_info['shipping_method'];
			
			$this->data['products'] = array();
			
			$products = $this->model_account_order->getOrderProducts($this->request->get['order_id']);

			foreach ($products as $product) {
				$option_data = array();
				
				$options = $this->model_account_order->getOrderOptions($this->request->get['order_id'], $product['order_product_id']);
				foreach ($options as $option) {
					if ($option['type'] != 'file') {
						$value = $option['value'];
					} else {
						$value = utf8_substr($option['value'], 0, utf8_strrpos($option['value'], '.'));
					}
					
					$option_data[] = array(
						'name'  => $option['name'],
						'value' => (utf8_strlen($value) > 20 ? utf8_substr($value, 0, 20) . '..' : $value)
					);					
				}
				
				$image_result = $this->db->query("SELECT image FROM " . DB_PREFIX . "product WHERE product_id = '".(int)$product['product_id']."'");
					
				$this->load->model('tool/image');
				
				if (isset($image_result->row['image'])) {
					$image = $this->model_tool_image->resize($image_result->row['image'], 320, 220);
				} else {
					$image = $this->model_tool_image->resize('no_image.jpg', 320, 220);
				}
				
				
				$sides = explode(',', $product['sides']);
				
				$sides_info = array();
				
				$sides_i = 0;
				$sides_name = "בתוספת: ";
				
				if(!(empty($sides[0]))) {
					foreach ($sides as $side){
						$side_result_query = $this->db->query("SELECT name , image FROM " . DB_PREFIX . "product p LEFT JOIN " . DB_PREFIX . "product_description  pd ON p.product_id = pd.product_id  WHERE p.product_id = ".(int)$side." AND language_id = ".(int)$this->config->get('config_language_id')."");
						
						$side_result = $side_result_query->row;
						
						$sides_name .= ($sides_i != 0) ?  "ו" : "";
						$sides_i++;
						
						$sides_name .= $side_result['name']." ";
						
						$sides_info[] = array(
								'name' => $side_result['name'],
								'image' => $side_result['image']
								);
								
					}
				}

				$this->data['products_cooks'][$product['cook_id']][] = array(
					'name'     => $product['name'],
					'comment'  => $product['comment'],
					'cook_id'  => $product['cook_id'],
					'model'    => $product['model'],
					'image'    => $image,
					'option'   => $option_data,
					'quantity' => $product['quantity'],
					'sides'    => $sides_name, 
					'price'    => $this->currency->format($product['price'] + ($this->config->get('config_tax') ? $product['tax'] : 0), $order_info['currency_code'], $order_info['currency_value']),
						'total'    => $this->currency->format($product['total'] + ($this->config->get('config_tax') ? ($product['tax'] * $product['quantity']) : 0), $order_info['currency_code'], $order_info['currency_value']),
						'return'   => $this->url->link('account/return/insert', 'order_id=' . $order_info['order_id'] . '&product_id=' . $product['product_id'], 'SSL')
				);
			}
			
			
			// Voucher
			$this->data['vouchers'] = array();
			
			$vouchers = $this->model_account_order->getOrderVouchers($this->request->get['order_id']);
			
			foreach ($vouchers as $voucher) {
				$this->data['vouchers'][] = array(
					'description' => $voucher['description'],
					'amount'      => $this->currency->format($voucher['amount'], $order_info['currency_code'], $order_info['currency_value'])
				);
			}
			
			$this->data['totals'] = $this->model_account_order->getOrderTotals($this->request->get['order_id']);
			
			$this->data['comment'] = nl2br($order_info['comment']);
			
			$order_time = date("H:i",strtotime($order_info['delivery_time']." -1 hour"))." - ".date("H:i",strtotime($order_info['delivery_time']."")) ;
			
			$this->data['pickup_time'] = date("d/m", strtotime($order_info['delivery_date']))." | ".$order_time;
			$this->data['delivery_time'] = date("d/m", strtotime($order_info['delivery_date']))." | ".date("H:i",strtotime($order_info['delivery_time']." -1 hour"))." - ".date("H:i",strtotime($order_info['delivery_time']."")) ;
			
			
			$this->data['order_when'] = strftime("%A, %e ב%B",strtotime($order_info['delivery_date'])).", ".$order_time;
			
			if(!$order_info['tools']){
				$this->data['order_forks'] = 'אין צורך בסכו"ם';
			} else {
				$this->data['order_forks'] = 'יש להוסיף סכו"ם';
			}
			
			
			$this->data['histories'] = array();
			
			// maybe we first check if all child orders are approved
			$pre_result = $this->db->query("SELECT order_id, order_status_id FROM " . DB_PREFIX . "order WHERE parent_order_id = '".(int)$this->request->get['order_id']."'");
			
			// a check to see if all sub orders are confirmed
			//print_r($pre_result);
			
			$num_approved = 0;
			
			foreach($pre_result->rows as $result){
				if($result['order_status_id'] >= 5){
					$num_approved++;
				}
			}
			
			if($num_approved == 0){
				$this->data['aproved_status'] = " מאושר ";
			} else if ($num_approved < $pre_result->num_rows){
				$this->data['aproved_status'] = "מאושר חלקית";
			} else if ($num_approved == $pre_result->num_rows){
				$this->data['aproved_status'] = "מאושר";
			}
			
			$this->data['status'] = $pre_result->row['order_status_id'];
			
			$pre_result = $this->db->query("SELECT order_id, order_status_id FROM " . DB_PREFIX . "order WHERE parent_order_id = '".(int)$this->request->get['order_id']."'");
			
			
			
			
			$status_name = $this->db->query("SELECT *  FROM " . DB_PREFIX . "order_status WHERE order_status_id = '".(int)$pre_result->row['order_status_id']."' AND language_id = '".(int)$order_info['language_id']."'");
			
			
			if($status_name->row['name'] == "מאושר"){
				$this->data['status_name'] = $this->data['aproved_status'];
			} else {
				$this->data['status_name'] = $status_name->row['name'];
			}
			
			
			// we will get history of the first child orders
			$results = $this->model_account_order->getOrderHistories($pre_result->row['order_id']);
			
			foreach ($results as $result) {
				
				
				$this->data['histories'][] = array(
					'date_added' => date("d/m | H:i", strtotime($result['date_added'])),
					'status'     => $result['status'],
					'comment'    => nl2br($result['comment'])
				);
			}
				
			$this->data['continue'] = $this->url->link('account/order', '', 'SSL');
			
			if(isset($_GET['print'])){	
				if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/account/order_info_print.tpl')) {
					$this->template = $this->config->get('config_template') . '/template/account/order_info_print.tpl';
				} else {
					$this->template = 'default/template/account/order_info_print.tpl';
				}
			} else {
				if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/account/order_info.tpl')) {
					$this->template = $this->config->get('config_template') . '/template/account/order_info.tpl';
				} else {
					$this->template = 'default/template/account/order_info.tpl';
				}
			}
			
			$this->children = array(
				'common/column_left',
				'common/column_right',
				'common/content_top',
				'common/content_bottom',
				'common/footer',
				'common/header'	
			);
								
			$this->response->setOutput($this->render());		
		} else {
			$this->document->setTitle($this->language->get('text_order'));
			
			$this->data['heading_title'] = $this->language->get('text_order');
	
			$this->data['text_error'] = $this->language->get('text_error');
	
			$this->data['button_continue'] = $this->language->get('button_continue');
			
			$this->data['breadcrumbs'] = array();

			$this->data['breadcrumbs'][] = array(
				'text'      => $this->language->get('text_home'),
				'href'      => $this->url->link('common/home'),
				'separator' => false
			);
			
			$this->data['breadcrumbs'][] = array(
				'text'      => $this->language->get('text_account'),
				'href'      => $this->url->link('account/account', '', 'SSL'),
				'separator' => $this->language->get('text_separator')
			);

			$this->data['breadcrumbs'][] = array(
				'text'      => $this->language->get('heading_title'),
				'href'      => $this->url->link('account/order', '', 'SSL'),
				'separator' => $this->language->get('text_separator')
			);
			
			$this->data['breadcrumbs'][] = array(
				'text'      => $this->language->get('text_order'),
				'href'      => $this->url->link('account/order/info', 'order_id=' . $order_id, 'SSL'),
				'separator' => $this->language->get('text_separator')
			);
												
			$this->data['continue'] = $this->url->link('account/order', '', 'SSL');
			 
			 
			 
						
			if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/error/not_found.tpl')) {
				$this->template = $this->config->get('config_template') . '/template/error/not_found.tpl';
			} else {
				$this->template = 'default/template/error/not_found.tpl';
			}
			
			
			
			$this->children = array(
				'common/column_left',
				'common/column_right',
				'common/content_top',
				'common/content_bottom',
				'common/footer',
				'common/header'	
			);
								
			$this->response->setOutput($this->render());				
		}
  	}
	
	
	
	public function info_cook() { 
		$this->language->load('account/order');
		
		if (isset($this->request->get['order_id'])) {
			$order_id = $this->request->get['order_id'];
		} else {
			$order_id = 0;
		}	

		if (!$this->customer->isLogged()) {
			$this->session->data['redirect'] = $this->url->link('account/order/info', 'order_id=' . $order_id, 'SSL');
			//change to something else
			$this->redirect($this->url->link('account/login', '', 'SSL'));
		}
		
			
		$this->load->model('account/order');
			
		$order_info = $this->model_account_order->getOrder($order_id,true);
		
		if ($order_info) {
			$this->document->setTitle($this->language->get('text_order'));
			
			
			$this->load->model('tool/image');
			
			$query = $this->db->query("SELECT *  FROM " . DB_PREFIX . "customer WHERE customer_id = '".$order_info['customer_id']."'");
			
			$customer_info = $query->row;
			
			if(!empty($customer_info['image'])){
			    if (strpos($customer_info['image'], 'http') === 0) {
				$author_pic = $customer_info['pic_square'];
			    }else{
				$author_pic = $this->model_tool_image->resize($customer_info['image'], 60, 60);
			    }
			} else {
			    $author_pic = $this->model_tool_image->resize("no-avatar-women.png", 60, 60);
			}
                        
			$this->data['customer_img'] = $author_pic;
			
			
			$this->data['my_orders_url'] =  $this->url->link('seller/account-my-orders', '', 'SSL');
			
			$this->data['heading_title'] = $this->language->get('text_order');
			
			$this->data['text_order_detail'] = $this->language->get('text_order_detail');
			$this->data['text_invoice_no'] = $this->language->get('text_invoice_no');
			$this->data['text_order_id'] = $this->language->get('text_order_id');
			$this->data['text_date_added'] = $this->language->get('text_date_added');
			$this->data['text_shipping_method'] = $this->language->get('text_shipping_method');
			$this->data['text_shipping_address'] = $this->language->get('text_shipping_address');
			$this->data['text_payment_method'] = $this->language->get('text_payment_method');
			$this->data['text_payment_address'] = $this->language->get('text_payment_address');
			$this->data['text_history'] = $this->language->get('text_history');
			$this->data['text_comment'] = $this->language->get('text_comment');
	
			$this->data['column_name'] = $this->language->get('column_name');
			$this->data['column_model'] = $this->language->get('column_model');
			$this->data['column_quantity'] = $this->language->get('column_quantity');
			$this->data['column_price'] = $this->language->get('column_price');
			$this->data['column_total'] = $this->language->get('column_total');
			$this->data['column_action'] = $this->language->get('column_action');
			$this->data['column_date_added'] = $this->language->get('column_date_added');
			$this->data['column_status'] = $this->language->get('column_status');
			$this->data['column_comment'] = $this->language->get('column_comment');
				
			$this->data['button_return'] = $this->language->get('button_return');
			$this->data['button_continue'] = $this->language->get('button_continue');
			
			
			
			$messages = $this->MsLoader->MsMessage->getMessages(
				array(
					'from' => $order_info['customer_id'],
					'to' => $this->customer->getId()
				),
				array(
					'order_by'  => 'date_created',
					'order_way' => 'DESC',
					
				)
			);
			
			
			if(empty($messages)){
				$this->data['messages'] = false;
			} else {
				$this->data['messages'] = $messages;
				$this->data['conv_link'] = $this->url->link('account/msmessage', '&conversation_id=' . $messages[0]['conversation_id'], 'SSL');
			}
			
			

			
			//print_r($this->data['conv_id']);
			//die;
			
			if ($order_info['invoice_no']) {
				$this->data['invoice_no'] = $order_info['invoice_prefix'] . $order_info['invoice_no'];
			} else {
				$this->data['invoice_no'] = '';
			}
			
			$this->data['order_id'] = $this->request->get['order_id'];
			
			$this->data['date_added'] = date("d/m | H:i", strtotime($order_info['date_added']));
			
			
			$this->data['contact'] = $order_info['payment_firstname']."<br>".$order_info['payment_lastname'];
			$this->data['phone'] = $order_info['payment_postcode'];
			
			$format = '{address_1}' . "\n" . '{address_2}' . "\n" . '{city}' . "\n" . '{company}';
		
			$find = array(
	  			'{firstname}',
	  			'{lastname}',
	  			'{company}',
				'{address_1}',
				'{address_2}',
				'{city}',
				'{postcode}',
				'{zone}',
				'{zone_code}',
				'{country}'
			);
	
			$replace = array(
	  			'firstname' => $order_info['payment_firstname'],
	  			'lastname'  => $order_info['payment_lastname'],
	  			'company'   => $order_info['payment_company'],
				'address_1' => $order_info['payment_address_1'],
				'address_2' => $order_info['payment_address_2'],
				'city'      => $order_info['payment_city'],
				'postcode'  => $order_info['payment_postcode'],
				'zone'      => $order_info['payment_zone'],
				'zone_code' => $order_info['payment_zone_code'],
				'country'   => $order_info['payment_country']  
			);
			
			$this->data['payment_address'] = str_replace(array("\r\n", "\r", "\n"), '<br />', preg_replace(array("/\s\s+/", "/\r\r+/", "/\n\n+/"), '<br />', trim(str_replace($find, $replace, $format))));

			$this->data['payment_method'] = $order_info['payment_method'];
			
			
			
			
			$format = '{address_1}' . "\n" . '{address_2}' . "\n" . '{city}' . "\n" . '{company}';
		
			$find = array(
	  			'{firstname}',
	  			'{lastname}',
	  			'{company}',
				'{address_1}',
				'{address_2}',
				'{city}',
				'{postcode}',
				'{zone}',
				'{zone_code}',
				'{country}'
			);
	
			$replace = array(
	  			'firstname' => $order_info['shipping_firstname'],
	  			'lastname'  => $order_info['shipping_lastname'],
	  			'company'   => $order_info['shipping_company'],
				'address_1' => $order_info['shipping_address_1'],
				'address_2' => $order_info['shipping_address_2'],
				'city'      => $order_info['shipping_city'],
				'postcode'  => $order_info['shipping_postcode'],
				'zone'      => $order_info['shipping_zone'],
				'zone_code' => $order_info['shipping_zone_code'],
				'country'   => $order_info['shipping_country']  
			);

			$this->data['shipping_address'] = str_replace(array("\r\n", "\r", "\n"), '<br />', preg_replace(array("/\s\s+/", "/\r\r+/", "/\n\n+/"), '<br />', trim(str_replace($find, $replace, $format))));

			$this->data['shipping_method'] = $order_info['shipping_method'];
			
			$this->data['products'] = array();
			
			// paret_id and only his cook_id pros
			// $order_id
			
			$products = $this->model_account_order->getOrderProducts($order_info['parent_order_id']);
			
			$cook_total = 0;
			$tax_total =0;
			
			foreach ($products as $product) {
				$option_data = array();
				
				$options = $this->model_account_order->getOrderOptions($this->request->get['order_id'], $product['order_product_id']);
				foreach ($options as $option) {
					if ($option['type'] != 'file') {
						$value = $option['value'];
					} else {
						$value = utf8_substr($option['value'], 0, utf8_strrpos($option['value'], '.'));
					}
					
					$option_data[] = array(
						'name'  => $option['name'],
						'value' => (utf8_strlen($value) > 20 ? utf8_substr($value, 0, 20) . '..' : $value)
					);					
				}
				
				$image_result = $this->db->query("SELECT image FROM " . DB_PREFIX . "product WHERE product_id = '".(int)$product['product_id']."'");
					
				$this->load->model('tool/image');
				
				if (isset($image_result->row['image'])) {
					$image = $this->model_tool_image->resize($image_result->row['image'], 320, 220);
				} else {
					$image = $this->model_tool_image->resize('no_image.jpg', 320, 220);
				}
				
				
				$sides = explode(',', $product['sides']);
				
				$sides_info = array();
				
				$sides_i = 0;
				$sides_name = "בתוספת: ";
				
				if(!(empty($sides[0]))) {
					foreach ($sides as $side){
						$side_result_query = $this->db->query("SELECT name , image FROM " . DB_PREFIX . "product p LEFT JOIN " . DB_PREFIX . "product_description  pd ON p.product_id = pd.product_id  WHERE p.product_id = ".(int)$side." AND language_id = ".(int)$this->config->get('config_language_id')."");
						
						$side_result = $side_result_query->row;
						
						$sides_name .= ($sides_i != 0) ?  "ו" : "";
						$sides_i++;
						
						$sides_name .= $side_result['name']." ";
						
						$sides_info[] = array(
								'name' => $side_result['name'],
								'image' => $side_result['image']
								);
								
					}
				}
				
				if($product['cook_id'] == $this->customer->getId() || $this->customer->getId() == 229){
					
					$cook_total += $product['total'];
					$tax_total += ($this->config->get('config_tax') ? ($product['tax'] * $product['quantity']) : 0);
					
					$this->data['products_cooks'][$product['cook_id']][] = array(
					'name'     => $product['name'],
					'comment'  => $product['comment'],
					'cook_id'  => $product['cook_id'],
					'model'    => $product['model'],
					'image'    => $image,
					'option'   => $option_data,
					'quantity' => $product['quantity'],
					'sides'    => $sides_name, 
					'price'    => $this->currency->format($product['price'], $order_info['currency_code'], $order_info['currency_value']),
						'total'    => $this->currency->format($product['total'], $order_info['currency_code'], $order_info['currency_value']),
						'total_tax'    => $this->currency->format($product['total'] + ($this->config->get('config_tax') ? ($product['tax'] * $product['quantity']) : 0), $order_info['currency_code'], $order_info['currency_value']),
						'return'   => $this->url->link('account/return/insert', 'order_id=' . $order_info['order_id'] . '&product_id=' . $product['product_id'], 'SSL')
				);
				}
			}
			
			
			$this->data['all_totals'] = $this->currency->format($tax_total + $cook_total, $order_info['currency_code'], $order_info['currency_value']);
			$this->data['tax_totals'] = $this->currency->format($tax_total, $order_info['currency_code'], $order_info['currency_value']);
			$this->data['cook_totals'] = $this->currency->format($cook_total, $order_info['currency_code'], $order_info['currency_value']);
			
			// Voucher
			$this->data['vouchers'] = array();
			
			$vouchers = $this->model_account_order->getOrderVouchers($this->request->get['order_id']);
			
			foreach ($vouchers as $voucher) {
				$this->data['vouchers'][] = array(
					'description' => $voucher['description'],
					'amount'      => $this->currency->format($voucher['amount'], $order_info['currency_code'], $order_info['currency_value'])
				);
			}
			
			$this->data['totals'] = $this->model_account_order->getOrderTotals($this->request->get['order_id']);
			
			$this->data['comment'] = nl2br($order_info['comment']);
			
			$order_time = date("H:i",strtotime($order_info['delivery_time']." -2 hour"))." - ".date("H:i",strtotime($order_info['delivery_time']." -1 hour")) ;
			
			$this->data['pickup_time'] = date("d/m", strtotime($order_info['delivery_date']))." | ".$order_time;
			$this->data['delivery_time'] = date("d/m", strtotime($order_info['delivery_date']))." | ".date("H:i",strtotime($order_info['delivery_time']." -1 hour"))." - ".date("H:i",strtotime($order_info['delivery_time']."")) ;

			
			$this->data['order_when'] = strftime("%A, %e ב%B",strtotime($order_info['delivery_date'])).", ".$order_time;
			
			if(!$order_info['tools']){
				$this->data['order_forks'] = 'אין צורך בסכו"ם';
			} else {
				$this->data['order_forks'] = 'יש להוסיף סכו"ם';
			}
			
			$this->data['histories'] = array();
			
			// maybe we first check if all child orders are approved
			$pre_result = $this->db->query("SELECT order_id, order_status_id FROM " . DB_PREFIX . "order WHERE parent_order_id = '".(int)$this->request->get['order_id']."'");
			
			$this->data['status'] = $order_info['order_status_id'];
			
			$status_name = $this->db->query("SELECT *  FROM " . DB_PREFIX . "order_status WHERE order_status_id = '".(int)$order_info['order_status_id']."' AND language_id = '".(int)$order_info['language_id']."'");
			
			$this->data['status_name'] = $status_name->row['name'];
			
			// we will get history of the first child orders
			
			$results = $this->model_account_order->getOrderHistories($this->request->get['order_id']);
			
			foreach ($results as $result) {
				
				
				$this->data['histories'][] = array(
					'date_added' => date("d/m | H:i", strtotime($result['date_added'])),
					'status'     => $result['status'],
					'comment'    => nl2br($result['comment'])
				);
			}
				
			$this->data['continue'] = $this->url->link('account/order', '', 'SSL');
			
			if(isset($_GET['print'])){	
				if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/account/order_info_cook_print.tpl')) {
					$this->template = $this->config->get('config_template') . '/template/account/order_info_cook_print.tpl';
				} else {
					$this->template = 'default/template/account/order_info_print.tpl';
				}
			} else {
				if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/account/order_info_cook.tpl')) {
					$this->template = $this->config->get('config_template') . '/template/account/order_info_cook.tpl';
				} else {
					$this->template = 'default/template/account/order_info.tpl';
				}
			}
			
			$this->children = array(
				'common/column_left',
				'common/column_right',
				'common/content_top',
				'common/content_bottom',
				'common/footer',
				'common/header'	
			);
								
			$this->response->setOutput($this->render());		
		} else {
			$this->document->setTitle($this->language->get('text_order'));
			
			$this->data['heading_title'] = $this->language->get('text_order');
	
			$this->data['text_error'] = $this->language->get('text_error');
	
			$this->data['button_continue'] = $this->language->get('button_continue');
			
			$this->data['breadcrumbs'] = array();

			$this->data['breadcrumbs'][] = array(
				'text'      => $this->language->get('text_home'),
				'href'      => $this->url->link('common/home'),
				'separator' => false
			);
			
			$this->data['breadcrumbs'][] = array(
				'text'      => $this->language->get('text_account'),
				'href'      => $this->url->link('account/account', '', 'SSL'),
				'separator' => $this->language->get('text_separator')
			);

			$this->data['breadcrumbs'][] = array(
				'text'      => $this->language->get('heading_title'),
				'href'      => $this->url->link('account/order', '', 'SSL'),
				'separator' => $this->language->get('text_separator')
			);
			
			$this->data['breadcrumbs'][] = array(
				'text'      => $this->language->get('text_order'),
				'href'      => $this->url->link('account/order/info', 'order_id=' . $order_id, 'SSL'),
				'separator' => $this->language->get('text_separator')
			);
												
			$this->data['continue'] = $this->url->link('account/order', '', 'SSL');
			 
			 
			 
						
			if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/error/not_found.tpl')) {
				$this->template = $this->config->get('config_template') . '/template/error/not_found.tpl';
			} else {
				$this->template = 'default/template/error/not_found.tpl';
			}
			
			
			
			$this->children = array(
				'common/column_left',
				'common/column_right',
				'common/content_top',
				'common/content_bottom',
				'common/footer',
				'common/header'	
			);
								
			$this->response->setOutput($this->render());				
		}
  	}
	
	
	public function reviewOrder() {
		$this->language->load('account/order');
		
		if (isset($this->request->get['order_id'])) {
			$order_id = $this->request->get['order_id'];
		} else {
			$order_id = 0;
		}
		
		if (!$this->customer->isLogged()) {
			$this->session->data['redirect'] = $this->url->link('account/order/reviewOrder', 'order_id=' . $order_id, 'SSL');
			//$this->redirect($this->url->link('product/category', 'path=0', 'SSL'));
		}
		
		$this->load->model('account/order');
		
		$order_info = $this->model_account_order->getOrder($order_id);
		
		
		
		if ($order_info) {
			$this->document->setTitle($this->language->get('text_review_order'));
			
			$this->load->model('account/customer');
			$this->load->model('tool/image');
			$this->load->model('account/address');
			
			$this->data['cooks'] = array();
			$cooks_ids = json_decode($order_info['cook_id']);
			
			foreach($cooks_ids as $cook_id){
				
				$cook_info = $this->model_account_customer->getCustomer($cook_id);
				
				$customer_address = $this->model_account_address->getAddressesById($cook_id);
				$customer_address = reset($customer_address);
				
				//print_r($cook_info); die;
				
				if(!empty($cook_info['image'])){
					if (strpos($cook_info['image'], 'http') === 0) {
						$author_pic = $cook_info['pic_square'];
					}else{
						$author_pic = $this->model_tool_image->resize($cook_info['image'], 100, 101);
					}
				} else {
					$author_pic = "";
				}
				
				$this->data['cooks'][$cook_id] = array(
									'cook_name'		=>	sprintf($this->language->get('cook_name'),$cook_info['firstname'],$cook_info['lastname']),
									'cook_id'		=>	$cook_id,
									'image'			=>	$author_pic,
									'firstname'		=>	$cook_info['firstname'],
									'sp'			=>	$cook_info['sp'],
									'email'			=>	$cook_info['email'],
									'customer_id'		=>	$cook_info['customer_id'],
									'cook_city'		=>	$customer_address['city'],
									'cook_info'		=>	$cook_info
								       );
				
			}
			
			
			
			
			
			
			setlocale(LC_ALL, 'he_IL.UTF-8');
			
			$this->data['text_cooked_by'] = $this->language->get('text_cooked_by');
			$this->data['cook_name'] =  sprintf($this->language->get('cook_name'),$cook_info['firstname'],$cook_info['lastname']);
			$this->data['text_delivered'] = $this->language->get('text_delivered');
			$this->data['delivery_time'] = strftime($this->language->get('date_time_format_strf'),strtotime($order_info["delivery_date"])).", ".( 1 + (int)$order_info["delivery_time"]).":30 - ".(int)$order_info["delivery_time"].":30";
			$this->data['text_sp'] = $this->language->get('text_sp');
			$this->data['text_meals'] = $this->language->get('text_meals');
			$this->data['text_delivery'] = $this->language->get('text_delivery');
			
			
			$this->data['cook_image'] = $author_pic;
			$this->data['cook'] = $cook_info;
			
			$this->data['heading_title'] = $this->language->get('text_review_order_detail');
			
			$this->data['text_order_detail'] = $this->language->get('text_review_order');
			$this->data['text_invoice_no'] = $this->language->get('text_invoice_no');
			$this->data['text_order_id'] = $this->language->get('text_order_id');
			$this->data['text_date_added'] = $this->language->get('text_date_added');
			$this->data['text_shipping_method'] = $this->language->get('text_shipping_method');
			$this->data['text_shipping_address'] = $this->language->get('text_shipping_address');
			$this->data['text_payment_method'] = $this->language->get('text_payment_method');
			$this->data['text_payment_address'] = $this->language->get('text_payment_address');
			$this->data['text_history'] = $this->language->get('text_history');
			$this->data['text_comment'] = $this->language->get('text_comment');
			
			$this->data['text_choose_side'] = $this->language->get('text_choose_side');
			$this->data['placeholder_meal'] = $this->language->get('placeholder_meal');
			$this->data['placeholder_delivery'] = $this->language->get('placeholder_delivery');
			
			$this->data['column_name'] = $this->language->get('column_name');
			$this->data['column_model'] = $this->language->get('column_model');
			$this->data['column_quantity'] = $this->language->get('column_quantity');
			$this->data['column_price'] = $this->language->get('column_price');
			$this->data['column_total'] = $this->language->get('column_total');
			$this->data['column_action'] = $this->language->get('column_action');
			$this->data['column_date_added'] = $this->language->get('column_date_added');
			$this->data['column_status'] = $this->language->get('column_status');
			$this->data['column_comment'] = $this->language->get('column_comment');
			
			$this->data['rate_word'][0] = $this->language->get('rate_word_0');
			$this->data['rate_word'][1] = $this->language->get('rate_word_1');
			$this->data['rate_word'][2] = $this->language->get('rate_word_2');
			$this->data['rate_word'][3] = $this->language->get('rate_word_3');
			$this->data['rate_word'][4] = $this->language->get('rate_word_4');
			$this->data['rate_word'][5] = $this->language->get('rate_word_5');
			
			$this->data['delivery_header'] = $this->language->get('delivery_header');
			
			$this->data['delivery_question'][1] = $this->language->get('delivery_question_1');
			$this->data['delivery_question'][2] = $this->language->get('delivery_question_2');
			$this->data['delivery_question'][3] = $this->language->get('delivery_question_3');
			
			$this->data['add_privet_review'] = $this->language->get('add_privet_review');
			
			$this->data['text_cancel'] = $this->language->get('text_cancel');
			$this->data['text_submit'] = $this->language->get('text_submit');
			
			$this->data['text_yes'] = $this->language->get('text_yes');
			$this->data['text_no'] = $this->language->get('text_no');
			
			$this->data['button_return'] = $this->language->get('button_return');
			$this->data['button_continue'] = $this->language->get('button_continue');
			
			if ($order_info['invoice_no']) {
				$this->data['invoice_no'] = $order_info['invoice_prefix'] . $order_info['invoice_no'];
			} else {
				$this->data['invoice_no'] = '';
			}
			
			$this->data['order_id'] = $this->request->get['order_id'];
			$this->data['date_added'] = date($this->language->get('date_format_short'), strtotime($order_info['date_added']));
			
			if ($order_info['payment_address_format']) {
      			$format = $order_info['payment_address_format'];
			} else {
				$format = '{firstname} {lastname}' . "\n" . '{company}' . "\n" . '{address_1}' . "\n" . '{address_2}' . "\n" . '{city} {postcode}' . "\n" . '{zone}' . "\n" . '{country}';
			}
		
			$find = array(
	  			'{firstname}',
	  			'{lastname}',
	  			'{company}',
				'{address_1}',
				'{address_2}',
				'{city}',
				'{postcode}',
				'{zone}',
				'{zone_code}',
				'{country}'
			);
			
			$replace = array(
	  			'firstname' => $order_info['payment_firstname'],
	  			'lastname'  => $order_info['payment_lastname'],
	  			'company'   => $order_info['payment_company'],
				'address_1' => $order_info['payment_address_1'],
				'address_2' => $order_info['payment_address_2'],
				'city'      => $order_info['payment_city'],
				'postcode'  => $order_info['payment_postcode'],
				'zone'      => $order_info['payment_zone'],
				'zone_code' => $order_info['payment_zone_code'],
				'country'   => $order_info['payment_country']  	
			);
			
			$this->data['payment_address'] = str_replace(array("\r\n", "\r", "\n"), '<br />', preg_replace(array("/\s\s+/", "/\r\r+/", "/\n\n+/"), '<br />', trim(str_replace($find, $replace, $format))));
			
			$this->data['payment_method'] = $order_info['payment_method'];
			
			if ($order_info['shipping_address_format']) {
				$format = $order_info['shipping_address_format'];
			} else {
				$format = '{firstname} {lastname}' . "\n" . '{company}' . "\n" . '{address_1}' . "\n" . '{address_2}' . "\n" . '{city} {postcode}' . "\n" . '{zone}' . "\n" . '{country}';
			}
			
			$find = array(
	  			'{firstname}',
	  			'{lastname}',
	  			'{company}',
				'{address_1}',
				'{address_2}',
				'{city}',
				'{postcode}',
				'{zone}',
				'{zone_code}',
				'{country}'
			);
			
			$replace = array(
	  			'firstname' => $order_info['shipping_firstname'],
	  			'lastname'  => $order_info['shipping_lastname'],
	  			'company'   => $order_info['shipping_company'],
				'address_1' => $order_info['shipping_address_1'],
				'address_2' => $order_info['shipping_address_2'],
				'city'      => $order_info['shipping_city'],
				'postcode'  => $order_info['shipping_postcode'],
				'zone'      => $order_info['shipping_zone'],
				'zone_code' => $order_info['shipping_zone_code'],
				'country'   => $order_info['shipping_country']  
			);
			
			$this->data['shipping_address'] = str_replace(array("\r\n", "\r", "\n"), '<br />', preg_replace(array("/\s\s+/", "/\r\r+/", "/\n\n+/"), '<br />', trim(str_replace($find, $replace, $format))));
			
			$this->data['shipping_method'] = $order_info['shipping_method'];
			
			$this->data['products'] = array();
			
			$products = $this->model_account_order->getOrderProducts($this->request->get['order_id']);
			
			$this->load->model('catalog/product');
			$this->load->model('tool/image');
			
			foreach ($products as $product) {
				
				$product_info = $this->model_catalog_product->getProduct($product['product_id']);
				
				if(isset($product['sides'])){
					$sides = explode(",",$product['sides']);
				}
				
				$option_data = array();
				
				$options = $this->model_account_order->getOrderOptions($this->request->get['order_id'], $product['order_product_id']);
				
				foreach ($options as $option) {
					if ($option['type'] != 'file') {
							$value = $option['value'];
						} else {
							$value = utf8_substr($option['value'], 0, utf8_strrpos($option['value'], '.'));
						}
						
						$option_data[] = array(
							'name'  => $option['name'],
							'value' => (utf8_strlen($value) > 20 ? utf8_substr($value, 0, 20) . '..' : $value)
						);					
				}
				
				if ($product_info['image']) {
					$product_image = $this->model_tool_image->resize($product_info['image'], 212, 145);
				} else {
					$product_image = '';
				}
				
				/* meal sides */
				$side_dishes = array();
				
				$results = $this->model_catalog_product->getProductRelated($product['product_id']);
				
				if($results /*&& $product_info['number_of_sides']*/){
					
					if($product_info['number_of_sides'] == 2){
						$total_sides = sprintf($this->language->get('total_sides'), $product_info['number_of_sides']);
					}elseif($product_info['number_of_sides'] == 1){
						$total_sides = sprintf($this->language->get('total_sides_one'), $product_info['number_of_sides']);
					}elseif($product_info['number_of_sides'] == 0){
						$total_sides = "";
					}
					
					foreach ($sides as $side_id) {
						
						foreach ($results as $result) {
							if($result['product_id'] == $side_id){
								if ($result['image']) {
									$image = $this->model_tool_image->resize($result['image'], 212, 145);
								} else {
									$image = false;
								}
								
								if (($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) {
									$price = $this->currency->format($this->tax->calculate($result['price'], $result['tax_class_id'], $this->config->get('config_tax')));
								} else {
									$price = false;
								}
										
								if ((float)$result['special']) {
									$special = $this->currency->format($this->tax->calculate($result['special'], $result['tax_class_id'], $this->config->get('config_tax')));
								} else {
									$special = false;
								}
								
								if ($this->config->get('config_review_status')) {
									$rating = (int)$result['rating'];
								} else {
									$rating = false;
								}
											
								$side_dishes[] = array(
									'product_id' => $result['product_id'],
									'tag'		=> $result['tag'],
									'image'   	 => $image,
									'name'    	 => $result['name'],
									'price'   	 => $price,
									'special' 	 => $special,
									'rating'     => $rating,
									'reviews'    => sprintf($this->language->get('text_reviews'), (int)$result['reviews']),
									'href'    	 => $this->url->link('product/product', 'product_id=' . $result['product_id']),
								);	
							}
						}
						
					}
				} else {
					$total_sides = "";
				}
				
				
				$this->data['productsByCook'][$product['cook_id']][] = array(
					'name'     			=> $product['name'],
					'cook_id'			=> $product['cook_id'],
					'image'   			=> $product_image,
					'product_id' 			=> (int)$product_info['product_id'],
					'rating'   			=> (int)$product_info['rating'],
					'number_of_sides' 		=> (int)$product_info['number_of_sides'],
					'total_sides' 			=> $total_sides,
					'sides' 			=> $side_dishes,
					'model'    			=> $product_info['model'],
					'option'   			=> $option_data,
					'quantity' 			=> $product['quantity'],
					'price'    			=> $this->currency->format($product['price'] + ($this->config->get('config_tax') ? $product['tax'] : 0), $order_info['currency_code'], $order_info['currency_value']),
					'total'    			=> $this->currency->format($product['total'] + ($this->config->get('config_tax') ? ($product['tax'] * $product['quantity']) : 0), $order_info['currency_code'], $order_info['currency_value']),
					'return'   			=> $this->url->link('account/return/insert', 'order_id=' . $order_info['order_id'] . '&product_id=' . $product['product_id'], 'SSL')
				);
			}
			
			// Voucher
			$this->data['vouchers'] = array();
			
			$vouchers = $this->model_account_order->getOrderVouchers($this->request->get['order_id']);
			
			foreach ($vouchers as $voucher) {
				$this->data['vouchers'][] = array(
					'description' => $voucher['description'],
					'amount'      => $this->currency->format($voucher['amount'], $order_info['currency_code'], $order_info['currency_value'])
				);
			}
			
			$this->data['totals'] = $this->model_account_order->getOrderTotals($this->request->get['order_id']);
			
			$this->data['comment'] = nl2br($order_info['comment']);
			
			$this->data['histories'] = array();
			
			$results = $this->model_account_order->getOrderHistories($this->request->get['order_id']);
			
			foreach ($results as $result) {
				$this->data['histories'][] = array(
					'date_added' => date($this->language->get('date_format_short'), strtotime($result['date_added'])),
					'status'     => $result['status'],
					'comment'    => nl2br($result['comment'])
				);
			}
			
			$this->data['continue'] = $this->url->link('account/order', '', 'SSL');
			
			if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/account/review_order.tpl')) {
				$this->template = $this->config->get('config_template') . '/template/account/review_order.tpl';
			} else {
				$this->template = 'default/template/account/review_order.tpl';
			}
			
			$this->children = array(
				'common/column_left',
				'common/column_right',
				'common/content_top',
				'common/content_bottom',
				'common/footer',
				'common/header'	
			);
			
			$this->response->setOutput($this->render());
			
		} else {
			
			if (!$this->customer->isLogged()) {
				$this->session->data['redirect'] = $this->url->link('account/order/reviewOrder', 'order_id=' . $order_id, 'SSL');
				
				$this->document->setTitle($this->language->get('text_order'));
				
				$this->data['heading_title'] = "דירוג הזמנה";
				
				$this->data['text_error'] = "יש להתחבר כדי לדרג את ההזמנה";
				
				$this->data['button_continue'] = $this->language->get('button_continue');
				
				$this->data['continue'] = $this->url->link('product/category', 'path=0', 'SSL');
					
				if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/error/not_concted.tpl')) {
					$this->template = $this->config->get('config_template') . '/template/error/not_concted.tpl';
				} else {
					$this->template = 'default/template/error/not_concted.tpl';
				}
				
				$this->children = array(
					'common/column_left',
					'common/column_right',
					'common/content_top',
					'common/content_bottom',
					'common/footer',
					'common/header'	
				);
				
				$this->response->setOutput($this->render());	
				
			} else {
				$this->document->setTitle($this->language->get('text_order'));
				
				$this->data['heading_title'] = $this->language->get('text_order');
				
				$this->data['text_error'] = $this->language->get('text_error');
				
				$this->data['button_continue'] = $this->language->get('button_continue');
				
				$this->data['continue'] = $this->url->link('account/order', '', 'SSL');
				
				if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/error/not_found.tpl')) {
					$this->template = $this->config->get('config_template') . '/template/error/not_found.tpl';
				} else {
					$this->template = 'default/template/error/not_found.tpl';
				}
				
				$this->children = array(
					'common/column_left',
					'common/column_right',
					'common/content_top',
					'common/content_bottom',
					'common/footer',
					'common/header'	
				);
				
				$this->response->setOutput($this->render());	
			}	
		}
  	}
}
?>