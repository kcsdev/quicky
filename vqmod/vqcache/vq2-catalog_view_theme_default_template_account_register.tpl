<?php echo $header; ?>
<?php if ($error_warning) { ?>
<div class="warning"><?php echo $error_warning; ?></div>
<?php } ?>
<?php echo $column_left; ?><?php echo $column_right; ?>
<div id="content"><?php echo $content_top; ?>
  <div class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
    <?php } ?>
  </div>
  <h1><?php echo $heading_title; ?></h1>
  <p><?php echo $text_account_already; ?></p>
  <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data">
    <h2><?php echo $text_your_details; ?></h2>
    <div class="content">
      <table class="form">
        <tr>
          <td><span class="required">*</span> <?php echo $entry_firstname; ?></td>
          <td><input type="text" name="firstname" value="<?php echo $firstname; ?>" />
            <?php if ($error_firstname) { ?>
            <span class="error"><?php echo $error_firstname; ?></span>
            <?php } ?></td>
        </tr>
        <tr>
          <td><span class="required">*</span> <?php echo $entry_lastname; ?></td>
          <td><input type="text" name="lastname" value="<?php echo $lastname; ?>" />
            <?php if ($error_lastname) { ?>
            <span class="error"><?php echo $error_lastname; ?></span>
            <?php } ?></td>
        </tr>
        <tr>
          <td><span class="required">*</span> <?php echo $entry_email; ?></td>
          <td><input type="text" name="email" value="<?php echo $email; ?>" />
            <?php if ($error_email) { ?>
            <span class="error"><?php echo $error_email; ?></span>
            <?php } ?></td>
        </tr>
        <tr>
          <td><span class="required">*</span> <?php echo $entry_telephone; ?></td>
          <td><input type="text" name="telephone" value="<?php echo $telephone; ?>" />
            <?php if ($error_telephone) { ?>
            <span class="error"><?php echo $error_telephone; ?></span>
            <?php } ?></td>
        </tr>
        <tr>
          <td><?php echo $entry_fax; ?></td>
          <td><input type="text" name="fax" value="<?php echo $fax; ?>" /></td>
        </tr>

				<?php foreach ($fields as $field) { ?>
				  <?php if ($field['status']) { ?>
				  <tr id="row-<?php echo $field['option_id']; ?>">
				    <td><?php if($field['required']) { ?><span class="required">*</span> <?php } ?><?php echo $field['name']; ?></td>
					<td>
					  <?php if ($field['type'] == 'date') { ?>
					    <input type="text" name="crf[<?php echo $field['option_id']; ?>]" value="<?php echo $field['value']; ?>" class="date" />
					  <?php } elseif ($field['type'] == 'file') { ?>
					    <input type="button" id="button-crf-<?php echo $field['option_id']; ?>" class="button" value="Upload" />
					    <input type="hidden" name="crf[<?php echo $field['option_id']; ?>]" value="<?php echo $field['value']; ?>" />
					  <?php } elseif ($field['type'] == 'text') { ?>
					    <input type="text" name="crf[<?php echo $field['option_id']; ?>]" value="<?php echo $field['value']; ?>" />
					  <?php } elseif ($field['type'] == 'textarea') { ?>
					    <textarea rows="7" columns="100" name="crf[<?php echo $field['option_id']; ?>]"><?php echo $field['value']; ?></textarea>
					  <?php } elseif ($field['type'] == 'datetime') { ?>
					    <input type="text" name="crf[<?php echo $field['option_id']; ?>]" class="datetime" value="<?php echo $field['value']; ?>" />
					  <?php } elseif ($field['type'] == 'select') { ?>
						<select name="crf[<?php echo $field['option_id']; ?>]">
						  <?php foreach ($field['values'] as $value) { ?>
						    <?php if ($value['selected'] == $value['option_value_id']) { ?>
						    <option value="<?php echo $value['option_value_id']; ?>" selected="selected"><?php echo $value['name']; ?></option>
							<?php } else { ?>
							<option value="<?php echo $value['option_value_id']; ?>"><?php echo $value['name']; ?></option>
							<?php } ?>
						  <?php } ?>
						</select>
					  <?php } elseif ($field['type'] == 'radio') { ?>
					    <?php foreach ($field['values'] as $value) { ?>
						  <?php if ($value['selected'] == $value['option_value_id']) { ?>
						  <label for="label-<?php echo $value['option_value_id']; ?>"><input type="radio" name="crf[<?php echo $field['option_id']; ?>]" value="<?php echo $value['option_value_id']; ?>" id="label-<?php echo $value['option_value_id']; ?>" checked /><?php echo $value['name']; ?><br /></label>
						  <?php } else { ?>
						  <label for="label-<?php echo $value['option_value_id']; ?>"><input type="radio" name="crf[<?php echo $field['option_id']; ?>]" value="<?php echo $value['option_value_id']; ?>" id="label-<?php echo $value['option_value_id']; ?>" /><?php echo $value['name']; ?><br /></label>
						  <?php } ?>
						<?php } ?>
					  <?php } elseif ($field['type'] == 'checkbox') { ?>
					    <?php foreach ($field['values'] as $value) { ?>
						  <?php if ($value['selected'] == $value['option_value_id']) { ?>
						  <label for="label-<?php echo $value['option_value_id']; ?>"><input type="checkbox" name="crf[<?php echo $field['option_id']; ?>][]" value="<?php echo $value['option_value_id']; ?>" id="label-<?php echo $value['option_value_id']; ?>" checked /><?php echo $value['name']; ?><br /></label>
						  <?php } else { ?>
						  <label for="label-<?php echo $value['option_value_id']; ?>"><input type="checkbox" name="crf[<?php echo $field['option_id']; ?>][]" value="<?php echo $value['option_value_id']; ?>" id="label-<?php echo $value['option_value_id']; ?>" /><?php echo $value['name']; ?><br /></label>
						  <?php } ?>
						<?php } ?>
					  <?php } ?>
					</td>
				  </tr>
				  <?php } ?>
				<?php } ?>
			
      </table>
    </div>
    <h2><?php echo $text_your_address; ?></h2>
    <div class="content">
      <table class="form">
        <tr>
          <td><?php echo $entry_company; ?></td>
          <td><input type="text" name="company" value="<?php echo $company; ?>" /></td>
        </tr>        
        <tr style="display: <?php echo (count($customer_groups) > 1 ? 'table-row' : 'none'); ?>;">
          <td><?php echo $entry_customer_group; ?></td>
          <td><?php foreach ($customer_groups as $customer_group) { ?>
            <?php if ($customer_group['customer_group_id'] == $customer_group_id) { ?>
            <input type="radio" name="customer_group_id" value="<?php echo $customer_group['customer_group_id']; ?>" id="customer_group_id<?php echo $customer_group['customer_group_id']; ?>" checked="checked" />
            <label for="customer_group_id<?php echo $customer_group['customer_group_id']; ?>"><?php echo $customer_group['name']; ?></label>
            <br />
            <?php } else { ?>
            <input type="radio" name="customer_group_id" value="<?php echo $customer_group['customer_group_id']; ?>" id="customer_group_id<?php echo $customer_group['customer_group_id']; ?>" />
            <label for="customer_group_id<?php echo $customer_group['customer_group_id']; ?>"><?php echo $customer_group['name']; ?></label>
            <br />
            <?php } ?>
            <?php } ?></td>
        </tr>      
        <tr id="company-id-display">
          <td><span id="company-id-required" class="required">*</span> <?php echo $entry_company_id; ?></td>
          <td><input type="text" name="company_id" value="<?php echo $company_id; ?>" />
            <?php if ($error_company_id) { ?>
            <span class="error"><?php echo $error_company_id; ?></span>
            <?php } ?></td>
        </tr>
        <tr id="tax-id-display">
          <td><span id="tax-id-required" class="required">*</span> <?php echo $entry_tax_id; ?></td>
          <td><input type="text" name="tax_id" value="<?php echo $tax_id; ?>" />
            <?php if ($error_tax_id) { ?>
            <span class="error"><?php echo $error_tax_id; ?></span>
            <?php } ?></td>
        </tr>
        <tr>
          <td><span class="required">*</span> <?php echo $entry_address_1; ?></td>
          <td><input type="text" name="address_1" value="<?php echo $address_1; ?>" />
            <?php if ($error_address_1) { ?>
            <span class="error"><?php echo $error_address_1; ?></span>
            <?php } ?></td>
        </tr>
        <tr>
          <td><?php echo $entry_address_2; ?></td>
          <td><input type="text" name="address_2" value="<?php echo $address_2; ?>" /></td>
        </tr>
        <tr>
          <td><span class="required">*</span> <?php echo $entry_city; ?></td>
          <td><input type="text" name="city" value="<?php echo $city; ?>" />
            <?php if ($error_city) { ?>
            <span class="error"><?php echo $error_city; ?></span>
            <?php } ?></td>
        </tr>
        <tr>
          <td><span id="postcode-required" class="required">*</span> <?php echo $entry_postcode; ?></td>
          <td><input type="text" name="postcode" value="<?php echo $postcode; ?>" />
            <?php if ($error_postcode) { ?>
            <span class="error"><?php echo $error_postcode; ?></span>
            <?php } ?></td>
        </tr>
        <tr>
          <td><span class="required">*</span> <?php echo $entry_country; ?></td>
          <td><select name="country_id">
              <option value=""><?php echo $text_select; ?></option>
              <?php foreach ($countries as $country) { ?>
              <?php if ($country['country_id'] == $country_id) { ?>
              <option value="<?php echo $country['country_id']; ?>" selected="selected"><?php echo $country['name']; ?></option>
              <?php } else { ?>
              <option value="<?php echo $country['country_id']; ?>"><?php echo $country['name']; ?></option>
              <?php } ?>
              <?php } ?>
            </select>
            <?php if ($error_country) { ?>
            <span class="error"><?php echo $error_country; ?></span>
            <?php } ?></td>
        </tr>
        <tr>
          <td><span class="required">*</span> <?php echo $entry_zone; ?></td>
          <td><select name="zone_id">
            </select>
            <?php if ($error_zone) { ?>
            <span class="error"><?php echo $error_zone; ?></span>
            <?php } ?></td>
        </tr>
      </table>
    </div>
    <h2><?php echo $text_your_password; ?></h2>
    <div class="content">
      <table class="form">
        <tr>
          <td><span class="required">*</span> <?php echo $entry_password; ?></td>
          <td><input type="password" name="password" value="<?php echo $password; ?>" />
            <?php if ($error_password) { ?>
            <span class="error"><?php echo $error_password; ?></span>
            <?php } ?></td>
        </tr>
        <tr>
          <td><span class="required">*</span> <?php echo $entry_confirm; ?></td>
          <td><input type="password" name="confirm" value="<?php echo $confirm; ?>" />
            <?php if ($error_confirm) { ?>
            <span class="error"><?php echo $error_confirm; ?></span>
            <?php } ?></td>
        </tr>
      </table>
    </div>
    <h2><?php echo $text_newsletter; ?></h2>
    <div class="content">
      <table class="form">
        <tr>
          <td><?php echo $entry_newsletter; ?></td>
          <td><?php if ($newsletter) { ?>
            <input type="radio" name="newsletter" value="1" checked="checked" />
            <?php echo $text_yes; ?>
            <input type="radio" name="newsletter" value="0" />
            <?php echo $text_no; ?>
            <?php } else { ?>
            <input type="radio" name="newsletter" value="1" />
            <?php echo $text_yes; ?>
            <input type="radio" name="newsletter" value="0" checked="checked" />
            <?php echo $text_no; ?>
            <?php } ?></td>
        </tr>
      </table>
    </div>
    <?php if ($text_agree) { ?>
    <div class="buttons">
      <div class="right"><?php echo $text_agree; ?>
        <?php if ($agree) { ?>
        <input type="checkbox" name="agree" value="1" checked="checked" />
        <?php } else { ?>
        <input type="checkbox" name="agree" value="1" />
        <?php } ?>
        <input type="submit" value="<?php echo $button_continue; ?>" class="button" />
      </div>
    </div>
    <?php } else { ?>
    <div class="buttons">
      <div class="right">
        <input type="submit" value="<?php echo $button_continue; ?>" class="button" />
      </div>
    </div>
    <?php } ?>
  </form>
  <?php echo $content_bottom; ?></div>

				<script type="text/javascript" src="catalog/view/javascript/jquery/ajaxupload.js"></script>
				<script type="text/javascript" src="catalog/view/javascript/jquery/ui/jquery-ui-timepicker-addon.js"></script> 
				<script type="text/javascript"><!--
					if ($.browser.msie && $.browser.version == 6) {
						$('.date, .datetime, .time').bgIframe();
					}
					
					$('.date').datepicker({dateFormat: 'yy-mm-dd'});
					$('.datetime').datetimepicker({
					dateFormat: 'yy-mm-dd',
					timeFormat: 'h:m'
					});
					$('.time').timepicker({timeFormat: 'h:m'});
				//--></script> 
				<?php foreach ($fields as $field) { ?>
				  <?php if ($field['type'] == 'file' && $field['status']) { ?>
					<script type="text/javascript"><!--
						new AjaxUpload('#button-crf-<?php echo $field['option_id']; ?>', {
							action: 'index.php?route=account/crf/upload',
							name: 'file',
							autoSubmit: true,
							responseType: 'json',
							onSubmit: function(file, extension) {
								$('#button-crf-<?php echo $field['option_id']; ?>').after('<img src="catalog/view/theme/default/image/loading.gif" class="loading" style="padding-left: 5px;" />');
								$('#button-crf-<?php echo $field['option_id']; ?>').attr('disabled', true);
							},
							onComplete: function(file, json) {
								$('#button-crf-<?php echo $field['option_id']; ?>').attr('disabled', false);
								
								if (json['success']) {
									alert(json['success']);
									
									$('input[name=\'crf[<?php echo $field['option_id']; ?>]\']').attr('value', json['file']);
								}
								
								if (json['error']) {
									alert(json['error']);
								}
								
								$('.loading').remove();	
							}
						});
					//--></script>
				  <?php } ?>
				<?php } ?>
				<script type="text/javascript"><!--//
				$(document).on('change', 'select[name^=\'crf\'], input[type=\'radio\'][name^=\'crf\']', function() {
					$(this).after('<img class="selection-wait" src="catalog/view/theme/default/image/loading.gif" alt="" />');
					
					var value = $(this).val();
					var parent_id = $(this).attr('name').replace (/[^\d.]/g, '');
				
					$.ajax({
						url: 'index.php?route=account/crf/dependentoption&parent_id=' +  parent_id + '&value=' + value,
						type: 'get',
						dataType: 'json',			
						success: function(json) {
							$('.selection-wait').remove();
							
							if (json['option']) {								
								for (i = 0; i < json['option'].length; i++) {
									if (json['option'][i]['type'] == 'select') {
										$('#row-' + json['option'][i]['option_id']).hide();
										
										var html = '';
									
										html += '<option value=""><?php echo $text_select; ?></option>';
											
										for (j = 0; j < json['option'][i]['option_value'].length; j++) {
											$('#row-' + json['option'][i]['option_id']).show();
											
											if (json['option'][i]['option_value'][j]['option_value_id'] == json['option'][i]['option_value'][j]['selected']) {
												html += '<option value="' + json['option'][i]['option_value'][j]['option_value_id'] + '" selected="selected">' + json['option'][i]['option_value'][j]['name'] + '</option>';
											} else {
												html += '<option value="' + json['option'][i]['option_value'][j]['option_value_id'] + '">' + json['option'][i]['option_value'][j]['name'] + '</option>';
											}
										}
										
										$('select[name=\'crf[' + json['option'][i]['option_id'] + ']\']').html(html);
										
										$('select[name=\'crf[' + json['option'][i]['option_id'] + ']\']').trigger('change');
									} else if (json['option'][i]['type'] == 'radio' || json['option'][i]['type'] == 'checkbox') {
										$('#row-' + json['option'][i]['option_id']).hide();
										
										$('#row-' + json['option'][i]['option_id']).children().find('label').hide();
										
										$('#row-' + json['option'][i]['option_id']).children().find('input').prop('checked', false);
									
										for (j = 0; j < json['option'][i]['option_value'].length; j++) {
											$('#row-' + json['option'][i]['option_id']).show();
											
											if (json['option'][i]['option_value'][j]['option_value_id'] == json['option'][i]['option_value'][j]['selected']) {
												$('label[for=\'label-' + json['option'][i]['option_value'][j]['option_value_id'] + '\']').find('input').prop('checked', true);
											}
										
											$('label[for=\'label-' + json['option'][i]['option_value'][j]['option_value_id'] + '\']').show();
										}
									} else {
										if (json['option'][i]['option_value']) {
											$('#row-' + json['option'][i]['option_id']).show();
										} else {
											$('#row-' + json['option'][i]['option_id']).hide();
										}
									}
								}
							}
						},
						error: function(xhr, ajaxOptions, thrownError) {
							alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
						}
					});	
				});
				
				$(document).ready(function() {
					$('select[name^=\'crf\'], input[type=\'radio\'][name^=\'crf\']:checked').trigger('change');
				});
				//--></script>
			
<script type="text/javascript"><!--
$('input[name=\'customer_group_id\']:checked').live('change', function() {
	var customer_group = [];
	
<?php foreach ($customer_groups as $customer_group) { ?>
	customer_group[<?php echo $customer_group['customer_group_id']; ?>] = [];
	customer_group[<?php echo $customer_group['customer_group_id']; ?>]['company_id_display'] = '<?php echo $customer_group['company_id_display']; ?>';
	customer_group[<?php echo $customer_group['customer_group_id']; ?>]['company_id_required'] = '<?php echo $customer_group['company_id_required']; ?>';
	customer_group[<?php echo $customer_group['customer_group_id']; ?>]['tax_id_display'] = '<?php echo $customer_group['tax_id_display']; ?>';
	customer_group[<?php echo $customer_group['customer_group_id']; ?>]['tax_id_required'] = '<?php echo $customer_group['tax_id_required']; ?>';
<?php } ?>	

	if (customer_group[this.value]) {
		if (customer_group[this.value]['company_id_display'] == '1') {
			$('#company-id-display').show();
		} else {
			$('#company-id-display').hide();
		}
		
		if (customer_group[this.value]['company_id_required'] == '1') {
			$('#company-id-required').show();
		} else {
			$('#company-id-required').hide();
		}
		
		if (customer_group[this.value]['tax_id_display'] == '1') {
			$('#tax-id-display').show();
		} else {
			$('#tax-id-display').hide();
		}
		
		if (customer_group[this.value]['tax_id_required'] == '1') {
			$('#tax-id-required').show();
		} else {
			$('#tax-id-required').hide();
		}	
	}
});

$('input[name=\'customer_group_id\']:checked').trigger('change');
//--></script> 
<script type="text/javascript"><!--
$('select[name=\'country_id\']').bind('change', function() {
	$.ajax({
		url: 'index.php?route=account/register/country&country_id=' + this.value,
		dataType: 'json',
		beforeSend: function() {
			$('select[name=\'country_id\']').after('<span class="wait">&nbsp;<img src="catalog/view/theme/default/image/loading.gif" alt="" /></span>');
		},
		complete: function() {
			$('.wait').remove();
		},			
		success: function(json) {
			if (json['postcode_required'] == '1') {
				$('#postcode-required').show();
			} else {
				$('#postcode-required').hide();
			}
			
			html = '<option value=""><?php echo $text_select; ?></option>';
			
			if (json['zone'] != '') {
				for (i = 0; i < json['zone'].length; i++) {
        			html += '<option value="' + json['zone'][i]['zone_id'] + '"';
	    			
					if (json['zone'][i]['zone_id'] == '<?php echo $zone_id; ?>') {
	      				html += ' selected="selected"';
	    			}
	
	    			html += '>' + json['zone'][i]['name'] + '</option>';
				}
			} else {
				html += '<option value="0" selected="selected"><?php echo $text_none; ?></option>';
			}
			
			$('select[name=\'zone_id\']').html(html);
		},
		error: function(xhr, ajaxOptions, thrownError) {
			alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
		}
	});
});

$('select[name=\'country_id\']').trigger('change');
//--></script> 
<script type="text/javascript"><!--
$(document).ready(function() {
	$('.colorbox').colorbox({
		width: 640,
		height: 480
	});
});
//--></script> 
<?php echo $footer; ?>