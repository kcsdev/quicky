<?php
// Heading
$_['heading_title']          = 'מוצרים'; 

// Text  
$_['text_success']           = 'הצלחה: המוצרים שונו!';
$_['text_plus']              = '+';
$_['text_minus']             = '-';
$_['text_default']           = 'ברירת מחדל';
$_['text_image_manager']     = 'מנהל התמונות';
$_['text_browse']            = 'עיון בקבצים';
$_['text_clear']             = 'הסרת תמונה';
$_['text_option']            = 'אפשרות';
$_['text_option_value']      = 'ערך אפשרות';
$_['text_percent']           = 'אחוזים';
$_['text_amount']            = 'כמות קבועה';

// Column
$_['column_name']            = 'שם ארוחה / תוספת';
$_['column_model']           = 'דגם';
$_['column_image']           = 'תמונה';
$_['column_price']           = 'מחיר';
$_['column_quantity']        = 'זמינות';
$_['column_status']          = 'מאופשר';
$_['column_action']          = 'פעולה';

$_['column_date']               = 'תאריך';
$_['column_last_order_time']    = 'שעות לפני הזמנה';
$_['column_start_time']         = 'חלון יציאה ראשון';
$_['column_end_time']           = 'חלון יציאה אחרון';
$_['column_day_time']           = 'זמינות תוך יומית';
$_['column_by_stary_time']      = 'חשב לפי חלון יציאה';

$_['button_add_availability']   = 'הוסף זמינות';

$_['text_def_availability']     = 'זמינות ברירת מחדל שבועיות';
$_['text_custom_availability']  = 'זמינות לבחירה';
$_['text_start']                = 'ראשון';
$_['text_end']                  = 'אחרון';

//

$_['text_morning']            = 'בוקר';
$_['text_afternoon']          = 'אחר צהריים';
$_['text_night']              = 'ערב';
$_['text_late_night']         = 'לילה';

//

// Entry
$_['entry_name']             = 'שם ארוחה:';
$_['entry_meta_keyword'] 	 = 'מילות מפתח במטא תג:';
$_['entry_meta_description'] = 'תיאור במתא תג:';
$_['entry_description']      = 'תיאור:';
$_['entry_store']            = 'חנויות:';
$_['entry_keyword']          = 'מילות מפתח - אופטימיזציה למנועי חיפוש:';
$_['entry_model']            = 'דגם:';
$_['entry_sku']              = 'שיטת הכנה:';
$_['entry_upc']              = 'UPC - קוד מוצר בין-לאומי:';
$_['entry_location']         = 'כמות בארוחה:';
$_['entry_manufacturer']     = 'טבח:';
$_['entry_filter']           = 'פילטר:';
$_['entry_shipping']         = 'דורש משלוח:'; 
$_['entry_date_available']   = 'תאריך זמינות:';
$_['entry_quantity']         = 'זמינות שבועית:';
$_['entry_minimum']          = 'כמות מינימום:<br/><span class="help">אכוף כמות הזמנה מינימלית</span>';
$_['entry_stock_status']     = 'מצב אזל המלאי:<br/><span class="help">המצב שיוצג כאשר המוצר חסר במלאי</span>';
$_['entry_price']            = 'מחיר:';
$_['entry_tax_class']        = 'סיווג מס:';
$_['entry_points']           = 'נקודות מצטברות:<br/><span class="help">מספר הנקודות הנדרשות לרכישת פריט זה. אם אין ברצונך שיוכלו לרכוש מוצר זה עם נקודות מצטברות השאר 0.</span>';
$_['entry_option_points']    = 'נקודות מצטברות:';
$_['entry_subtract']         = 'הפחת מלאי:';
$_['entry_weight_class']     = 'סיווג משקל:';
$_['entry_weight']           = 'משקל:';
$_['entry_length']           = 'סיווג אורך:';
$_['entry_dimension']        = 'מידות (אורך x רוחב x גובה):';
$_['entry_image']            = 'תמונה:';
$_['entry_customer_group']   = 'קבוצת לקוח:';
$_['entry_date_start']       = 'תאריך התחלה:';
$_['entry_date_end']         = 'תאריך סיום:';
$_['entry_priority']         = 'עדיפות:';
$_['entry_attribute']        = 'קטגוריה:';
$_['entry_attribute_group']  = 'קבוצת קטגוריה:';
$_['entry_text']             = 'תמליל:';
$_['entry_option']           = 'אפשרות:';
$_['entry_option_value']     = 'ערך אפשרות:';
$_['entry_required']         = 'נדרש:';
$_['entry_status']           = 'מצב:';
$_['entry_sort_order']       = 'סדר המיון:';
$_['entry_category']         = 'קטגוריה:';
$_['entry_download']         = 'הורדות:';
$_['entry_related']          = 'תוספות קשורות:<br /><span class="help">(השלמה אוטומטית)</span>';
$_['entry_tag']         	= 'מרכיבים:<br /><span class="help">מופרדים בפסיקים</span>';
$_['entry_reward']           = 'נקודות מצטברות:';
$_['entry_layout']           = 'אכיפת פריסה:';

$_['entry_sun'] = 'ראשון'; 
$_['entry_mon'] = 'שני'; 
$_['entry_tue'] = 'שלישי'; 
$_['entry_wed'] = 'רביעי'; 
$_['entry_thu'] = 'חמישי'; 
$_['entry_fri'] = 'שישי'; 
$_['entry_sat'] = 'שבת'; 

$_['entry_user'] = 'מסעדה';

$_['entry_meal_type'] = 'ארוחה / תוספת';
$_['text_meal'] = 'ארוחה';
$_['text_side'] = 'תוספת'; 
$_['entry_default'] = 'תוספת ברירת מחדל';
$_['entry_verified'] = 'ארוחה מאושרת'; 
$_['entry_default_quantity'] = 'כמות ברירת מחדל'; 


// Error
$_['error_warning']          = 'אזהרה: אנא בדוק את הטופס היטב לשגיאות!';
$_['error_permission']       = 'אזהרה: אין לך הרשאה לשנות מוצרים!';
$_['error_name']             = 'שם מוצר חייב להכיל גדול מ-3 ופחות מ-255 תווים!';
$_['error_model']            = 'דגם מוצר חייב להכיל גדול מ-3 ופחות מ-64 תווים!';

				$_['tab_cpf'] 			= 'Custom Fields';
				$_['button_upload'] 	= 'Upload';
				$_['button_download'] 	= 'Download';
				$_['button_clear'] 		= 'Clear';
				$_['error_cpf']			= 'Warning: Please ensure all required custom fields all filled up!';
			
?>