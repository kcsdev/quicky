<?php
// Heading
$_['heading_title']       = 'אפשרויות';

// Text
$_['text_success']        = 'הצלחה: האפשרויות שונו!';
$_['text_choose']         = 'לבחור';
$_['text_select']         = 'בחירה';
$_['text_radio']          = 'כפתור רדיו';
$_['text_checkbox']       = 'תיבת סימון';
//Modified for option quantity=======================================
$_['text_checkboxQuantity']       = 'תיבת סימון עם כמות';
//=============================================================
$_['text_image']          = 'תמונה';
$_['text_input']          = 'קלט';
$_['text_text']           = 'שדה תמליל';
$_['text_textarea']       = 'אזור תמליל';
$_['text_file']           = 'קובץ';
$_['text_date']           = 'תאריך';
$_['text_datetime']       = 'תאריך וזמן';
$_['text_time']           = 'זמן';
$_['text_image_manager']  = 'מנהל התמונות';
$_['text_browse']         = 'עיון בקבצים';
$_['text_clear']          = 'הסרת התמונה';

// Column
$_['column_name']         = 'שם אפשרות';
$_['column_sort_order']   = 'סדר המיון';
$_['column_action']       = 'פעולה';

// Entry

$_['entry_note']         = 'Option Note'.(version_compare(VERSION, '2', '<')?':':'');
$_['entry_option_value_note']         = 'Option Value Note'.(version_compare(VERSION, '2', '<')?':':'');

$_['entry_name']         = 'שם אפשרות:';
$_['entry_type']         = 'סוג:';
$_['entry_value']        = 'שם ערך אפשרות:';
$_['entry_image']        = 'תמונה:';
$_['entry_sort_order']   = 'סדר המיון:';

// Error
$_['error_permission']   = 'אזהרה: אין לך הרשאה לשנות אפשרויות!';
$_['error_name']         = 'שם אפשרות חייב להכיל בין 1 ל-128 תווים!';
$_['error_type']         = 'אזהרה: נדרש ערכי אפשרות!';
$_['error_option_value'] = 'שם ערך אפשרות חייב להכיל בין 1 ל-128 תווים!';
$_['error_product']      = 'אזהרה: אי אפשר למחוק אפשרות זו מכיוון שהיא מוגדרת ב %s מוצרים!';
?>