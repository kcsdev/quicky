

<?php
class ControllerCommonHeader extends Controller {
	protected function index() {

        if(isset($this->session->data['option_notes_styles'])){
            if(version_compare(VERSION, '2', '>=')){
                $data['option_notes_styles'] = $this->session->data['option_notes_styles'];
            }else{
                $this->data['option_notes_styles'] = $this->session->data['option_notes_styles'];
            }
        }


				$this->data = array_merge($this->data, $this->load->language('multiseller/multiseller'));
				$this->data['ms_total_products'] = $this->MsLoader->MsProduct->getTotalProducts(array(
					'enabled' => 1,
					//'product_status' => array(MsProduct::STATUS_ACTIVE),
				));
				
				$this->data['ms_total_sellers'] = $this->MsLoader->MsSeller->getTotalSellers(array(
					'seller_status' => array(MsSeller::STATUS_ACTIVE) 
				));
				
				$this->MsLoader->MsHelper->addStyle('multiseller');
				
				// note: renamed catalog
				$lang = "'view/javascript/multimerch/datatables/lang/" . $this->config->get('config_language') . ".txt'";
				$this->data['dt_language'] = file_exists(DIR_APPLICATION . $lang) ? "'catalog/$lang'" : "undefined";
			
	
	$this->data['dt_language'] = "view/javascript/multimerch/datatables/lang/hebrew.txt";
	
	$keywords = "הומילז,הומיילז,הומילס,הומילס,ארוחה,ארוחות,ביתי,טעים,בריא,כשר,אוכל,הזמנה,משלוחים,משלוח,בישול,תפריט,מסעדה,משלוחים,משלוחה,home,meals,homemeals,homemeal";
	$this->document->setKeywords($keywords);
	
	// time set for showing the oppsite products
	if(!isset($this->session->data['cut_off_time'])){
		$cut_off_time = strtotime("today 15:00");
		$this->session->data['cut_off_time'] = strtotime("today 15:00");
		$this->data['cut_off_time'] = $cut_off_time;
	} else {
		$cut_off_time = $this->session->data['cut_off_time'];
		$this->data['cut_off_time'] = $cut_off_time;
	}
	
	/******************************************************************
	*
	*	custom cupon code - start
	*
	******************************************************************/
	if (isset($this->request->get['cpcod'])) {
		
		if( true || !$this->customer->isLogged() ) {
			// sets the coupon code in session
			$this->session->data['coupon_code'] = urldecode(base64_decode($this->request->get['cpcod']));
			$this->session->data['coupon_new'] = true;
		}
		
	}
	
	if (isset($this->session->data['coupon_code']) && $this->customer->isLogged()) {
		
		$code = $this->session->data['coupon_code'];
		// add coustomer to coupon coustomer list
		$this->load->model('checkout/coupon');
		$coupon = $this->model_checkout_coupon->getCoupon($code);
		
		if($coupon){
			$this->model_checkout_coupon->addToCCList($coupon['coupon_id'],$this->customer->getId());
		}
		
		// unset the coupon so next user wont get it
		unset($this->session->data['coupon_code']);
		unset($this->session->data['coupon_new']);
	    
	}
	/******************************************************************
	*
	*	custom cupon code - end
	*
	******************************************************************/
	
	
	if(isset($this->request->get['kill_coupon'])){
		unset($this->session->data['coupon']);
	}
	
	/***********************************
	*
	*	Address 
	*
	**********************************/
	
	// set def address - by pressing the address line in the header
	if (isset($this->request->get['defaddress'])) {
		
		$this->customer->setDefAddress($this->request->get['defaddress']);
		unset($this->session->data['filter_place']);
		
		?>
		<script>
			window.location = "index.php?route=product/category&path=0"; 
		</script>
		<?php
		
		die();
	}
	
	if($this->customer->isLogged() && $this->customer->getAddressId()){
		$this->load->model('account/address');
		$address_id = $this->customer->getAddressId();
		$def_address = $this->model_account_address->getAddress($address_id);
	} else {
		$def_address = false;
	}
	
	$city = $this->language->get('text_tel_aviv');
	
	if (isset($this->request->get['filter_place'])) {
		
		$this->data['filter_place'] = $this->request->get['filter_place'];
		$this->session->data['filter_place'] = $this->request->get['filter_place'].", ".$city;
		$this->session->data['street_name'] = $this->request->get['filter_place'];
		$this->session->data['address_type'] = "new";
		
		$address_title = $this->request->get['filter_place'].", ".$city;
		$street_name = $this->request->get['filter_place'];
		
	} else if(isset($this->session->data['filter_place']) && $this->session->data['address_type'] = "new") {
		
		$this->data['filter_place'] = $this->session->data['filter_place'];
		
		$address_title = $this->session->data['filter_place'];
		$street_name = $this->session->data['street_name'];
		
	} else if(!empty($def_address)) {
		
		if(!empty($def_address['address_1'])){
			$street = explode("#",$def_address['address_1']);
			
			$this->data['filter_place'] = $street[0]." ".$street[1].", ".$def_address['city'];
			$this->session->data['filter_place_def'] = $street[0]." ".$street[1].", ".$def_address['city'];
			$this->session->data['address_type'] = "def";
			
			$address_title  = $street[0]." ".$street[1].", ".$def_address['city'];
			$street_name = trim($street[0]);
		} else {
			$this->data['filter_place'] = $def_address['city'];
			$this->session->data['filter_place_def'] = $def_address['city'];
			$this->session->data['address_type'] = "def";
			
			$address_title  = $def_address['city'];
			$street_name = "";
		}
		
	} else {
		
		$this->data['filter_place'] = false;
		$this->session->data['address_type'] = "none";
		
		$address_title = $city;
		$street_name = "";
	}
	
	$this->data['street_name'] = ($address_title == $city) ? "" : $address_title ;
	$this->data['def_address'] = $def_address ? true : false ;
	
	$GLOBALS["myAddress"] = $address_title;
	
	$this->data['address_type'] = $this->session->data['address_type'];
	
	if (isset($this->request->get['route']) && $this->request->get['route'] == "product/category") {
		$this->document->setTitle($address_title);
		$this->document->setDescription($address_title);
	}
	
	
	/***********************************
	 *
	 *	Address end
	 *
	 **********************************/
	
	$this->data['title'] = $this->document->getTitle();
	$this->data['og_title'] = $this->document->getOgTitle();
	$this->data['og_image'] = $this->document->getOgImage();
	$this->data['site_name'] = "Homeals.com"; 
	// start body_class code
	
	$this->data['products'] = array();
   
	$current_path = $this->request->get;
	$current_path_route = isset($current_path['route']) ? $current_path['route'] : "";
	if (empty($current_path) || $current_path_route == 'common/home') {
	  $body_class = 'home';
	}
	else {
	  $body_class = explode('/', str_replace('product/', '', $current_path_route));
	  unset($current_path['route']);
	  if (isset($current_path['_route_'])) {
	    $body_class = array_merge($body_class, explode('/', str_replace('-', '_', $current_path['_route_'])));
	    unset($current_path['_route_']);
	  }
	  foreach ($current_path as $key => $value) {
	    $body_class[] = $key . "_" . $value;
	  }
	  $body_class = 'page_' . implode(" page_", array_unique($body_class));
	}
	$body_class .= ' lang_' . $this->language->get('code');
	$this->data['body_class'] = $body_class;
	  
	// end body_class code
	
	if (isset($this->request->server['HTTPS']) && (($this->request->server['HTTPS'] == 'on') || ($this->request->server['HTTPS'] == '1'))) {
		$server = $this->config->get('config_ssl');
	} else {
		$server = $this->config->get('config_url');
	}
        
	// for more power over catagories
	if (isset($this->request->get['exclude'])) {
		$exclude = explode('_', (string)$this->request->get['exclude']);
	} else {
		$exclude = 0;
	}
	
	
        if (isset($this->session->data['error']) && !empty($this->session->data['error'])) {
            $this->data['error'] = $this->session->data['error'];
            
            unset($this->session->data['error']);
	    
        } else {
            $this->data['error'] = '';
        }
		
		$this->data['contact'] = $this->url->link('information/contact');
		
		$this->data['base'] = $server;
		$this->data['description'] = $this->document->getDescription();
		$this->data['keywords'] = $this->document->getKeywords();
		$this->data['links'] = $this->document->getLinks();	 
		$this->data['styles'] = $this->document->getStyles();
		$this->data['scripts'] = $this->document->getScripts();
		$this->data['lang'] = $this->language->get('code');
		$this->data['direction'] = $this->language->get('direction');
		$this->data['google_analytics'] = html_entity_decode($this->config->get('config_google_analytics'), ENT_QUOTES, 'UTF-8');
		$this->data['name'] = $this->config->get('config_name');
		
		if ($this->config->get('config_icon') && file_exists(DIR_IMAGE . $this->config->get('config_icon'))) {
			$this->data['icon'] = $server . 'image/' . $this->config->get('config_icon');
		} else {
			$this->data['icon'] = '';
		}
		
		if ($this->config->get('config_logo') && file_exists(DIR_IMAGE . $this->config->get('config_logo'))) {
			$this->data['logo'] = $server . 'image/' . $this->config->get('config_logo');
		} else {
			$this->data['logo'] = '';
		}
		
		$this->language->load('common/header');
		
		$this->data['text_home'] = $this->language->get('text_home');
		$this->data['text_wishlist'] = sprintf($this->language->get('text_wishlist'), (isset($this->session->data['wishlist']) ? count($this->session->data['wishlist']) : 0));
		$this->data['text_shopping_cart'] = $this->language->get('text_shopping_cart');
		$this->data['text_search'] = $this->language->get('text_search');
		
		
				if ($this->config->get('msconf_enable_one_page_seller_registration')) {
					$this->data['text_welcome'] = sprintf($this->language->get('ms_text_welcome'), $this->url->link('account/login', '', 'SSL'), $this->url->link('account/register', '', 'SSL'), $this->url->link('account/register-seller', '', 'SSL'));
				} else {
					$this->data['text_welcome'] = sprintf($this->language->get('text_welcome'), $this->url->link('account/login', '', 'SSL'), $this->url->link('account/register', '', 'SSL'));
				}
			
		$this->data['text_logged'] = sprintf($this->language->get('text_logged'), $this->url->link('account/account', '', 'SSL'), $this->customer->getFirstName(), $this->url->link('account/logout', '', 'SSL'));
		
		$this->data['first_name'] = $this->customer->getFirstName();
		$this->data['last_name'] = $this->customer->getLastName();
		
		$this->data['text_write_us'] = $this->language->get('text_write_us');
		$this->data['support_mail'] = $this->language->get('support_mail');
		
		$this->data['text_call_us'] = $this->language->get('text_call_us');
		$this->data['support_number'] = $this->language->get('support_number');
		
		$this->data['text_account'] = $this->language->get('text_account');
		$this->data['text_checkout'] = $this->language->get('text_checkout');
		
		if(isset($this->request->get['route'])){
			$this->data['route'] = $this->request->get['route'];
		} else {
			$this->data['route'] = false;
		}
		
		$this->data['text_inbox_s'] = $this->language->get('text_inbox_s');
		$this->data['text_inbox'] = $this->language->get('text_inbox');
		$this->data['href_inbox'] = $this->url->link('account/msconversation', '', 'SSL');
		
		$this->data['text_find_meals'] = $this->language->get('text_find_meals');
		$this->data['text_login_return'] = $this->language->get('text_login_return');
		$this->data['placeholder_search'] = $this->language->get('placeholder_search');
		
		$this->data['text_about'] = $this->language->get('text_about');
		$this->data['text_rest_list'] = $this->language->get('text_rest_list');
		$this->data['text_how_it_works'] = $this->language->get('text_how_it_works');
		$this->data['text_policy'] = $this->language->get('text_policy');
		$this->data['text_like_on_facebook'] = $this->language->get('text_like_on_facebook');
		
		/********************************************
		*
		*	counting number of unread messages for header
		*
		*********************************************/
		if($this->customer->isLogged()){
			
			$customer_id = $this->customer->getId();
			
			$conversations = $this->MsLoader->MsConversation->getConversations(
				array(
					'participant_id' => $customer_id,
				)
			);
			
			$num_unread = 0;
			
			foreach ($conversations as $conversation) {
				$read = "";
				if (!$this->MsLoader->MsConversation->isRead($conversation['conversation_id'], array('participant_id' => $customer_id))) {
					$num_unread++;
				}
			}
			
		} else {
			$num_unread = 0;
		}
		
		$this->data['num_unread'] = $num_unread;
		/********************************************
		*
		*	counting number of unread messages for header end
		*
		*********************************************/
		
		
		$this->data['text_logout'] = $this->language->get('text_logout');
		
		$this->data['wishlist'] = $this->url->link('account/wishlist', '', 'SSL');
		$this->data['logged'] = $this->customer->isLogged();
		$this->data['seller'] = $this->MsLoader->MsSeller->isSeller();
		$this->data['my_orders_url'] = $this->url->link('seller/account-my-orders', '', 'SSL');
		$this->data['customer_my_orders_url'] = $this->url->link('account/my-orders', '', 'SSL');
		$this->data['text_my_orders'] = $this->language->get('text_what_orders');
		$this->data['text_customer_my_orders'] = $this->language->get('text_customer_my_orders');
		$this->data['account'] = $this->url->link('account/account', '', 'SSL');
		$this->data['shopping_cart'] = $this->url->link('checkout/cart');
		$this->data['checkout'] = $this->url->link('checkout/checkout', '', 'SSL');
		
		if($this->customer->isLogged()){
			$this->load->model('account/address');
			
			$this->data['addresses'] = array();
		
			$results = $this->model_account_address->getAddresses();
			
			foreach ($results as $result) {
				$this->data['addresses'][] = array(
					'address_id' => $result['address_id'],
					'firstname' => $result['firstname'],
					'lastname'  => $result['lastname'],
					'company'   => $result['company'],
					'address_1' => $result['address_1'],
					'address_2' => $result['address_2'],
					'city'      => $result['city'],
					'postcode'  => $result['postcode'],
					'zone'      => $result['zone'],
					'zone_code' => $result['zone_code'],
					'country'   => $result['country']
				);
			}
		}
		
		$this->data['text_loginerror'] = $this->language->get('text_loginerror');
                
		$this->data['home'] = $this->url->link('common/home');
		
		$string = $address_title;
		$string = mb_truncate($string,25);
		$this->data['text_street'] = $string;
          
		$days = array();
		$days_more = array();
		
		$this->data['days_with_meals'] = $days;
		$this->data['days_more'] = $days_more;
		
		/***************************
		*
		* For sorting by day
		*
		*************************/
		
		$cart_items = $this->cart->getProducts();
			
		if (!empty($cart_items)){
			$cart_item = reset($cart_items);
			$this->session->data['cook'] = $cart_item['cook'];
		}else{
			$cart_item = array();
			$cart_item['cook'] = 0;
			$this->session->data['cook'] = 0;
		}
		
		if (isset($cart_item['date'])) {
			$week_day = $cart_item['date'];
			$this->session->data['date'] = date("m/d/y",strtotime($cart_item['date']));
		} else if (isset($this->request->get['day'])) {
			$week_day = date("m/d/y",strtotime($this->request->get['day']));
			$this->session->data['date'] = $week_day;
		} else if (isset($this->session->data['date'])) {
			$week_day = date("m/d/y",strtotime($this->session->data['date']));
			$this->session->data['date'] = $week_day;
		} else {
			$week_day = date("m/d/y");
			$this->session->data['date'] = $week_day;
		}
		
		$this->data['week_day'] = $week_day;
		
		if (isset($cart_item['time'])) {
			$delivery_time = $cart_item['time'];
			$this->session->data['time'] = $cart_item['time'];
		} else {
			$delivery_time = "";
			$this->session->data['time'] = "";
		}
		
		if (isset($this->request->get['cook']) && $this->request->get['cook'] == 'false') {
			$curr_cook = 0;
		} else {
			$curr_cook = $cart_item['cook'];
		}
		
		if (isset($this->request->get['route']) && $this->request->get['route'] == "information/information") {
			$this->load->model('catalog/information');
			
			$this->data['informations'] = array();
			
			foreach ($this->model_catalog_information->getInformations() as $result) {
				if ($result['bottom']) {
					$this->data['informations'][] = array(
						'title' => $result['title'],
						'id' => $result['information_id'],
						'href'  => $this->url->link('information/information', 'information_id=' . $result['information_id'])
					);
				}
			}
		}
		
		if (isset($this->request->get['route']) && $this->request->get['route'] == "product/category") {
			
			if (isset($this->request->get['path'])) {
				$category_id = $this->request->get['path'];
			} else {
				$category_id = 0;
			}
		   
			$this->data['products'] = array();
					
			$meal_type = 0;
			
			$data = array(
				'filter_category_id' 	=> $category_id,
				'filter_exclude' 	=> $exclude,
				'filter_meal_type'   	=> $meal_type,
				'filter_customer_id' 	=> $curr_cook,
				'filter_day_available'  => $week_day
			);
			
			$results = $this->model_catalog_product->getProducts($data);
			
			$product_number = count($results);
			
			/***********************************
			 *	DEBUGING
			 ***********************************/
			if(isset($_GET['debug_header'])){
				print_r($data);
				print_r("<br>");
				print_r($product_number."<br>");
				print_r("<br>");
			}
			/***********************************
			 *	DEBUGING END
			 ***********************************/
			
			$i = 0;
			$j = 0;
			
			$days = array();
			$days_more = array();
			
			if(!$product_number){
				$exclude = array();
				$category_id = 0;
			}
			
			while($i < 5 && $j < 40){
				
				$week_day = date("m/d/y", strtotime('+'.$j.' day'));
				
				$data = array(
					'filter_category_id' 	=> $category_id,
					'filter_exclude' 	=> $exclude,
					'filter_meal_type'   	=> $meal_type,
					'filter_customer_id' 	=> $curr_cook,
					'filter_day_available'  => $week_day
				);
				
				$results = $this->model_catalog_product->getProducts($data);
				
				$products_numbers = count($results);
				
				$op_data = array(
					'filter_category_id' 	=> $category_id,
					'filter_exclude' 	=> $exclude,
					'filter_meal_type'   	=> $meal_type,
					'show_opposite'   	=> "true",
					'filter_customer_id' 	=> $curr_cook,
					'filter_day_available'  => $week_day
				);
				
				$results_op = $this->model_catalog_product->getProducts($op_data);
				
				$op_products_numbers = count($results_op);
				
				if( ($products_numbers == 0) && ($op_products_numbers > 0) && ( strtotime("now") < $cut_off_time) ) {
					$days[0] = date('D', strtotime('today'));
					$days_more[0] = 0;
					$i++;
				}
				
				
				/***********************************
				 *	DEBUGING
				 ***********************************/
				if(isset($_GET['debug_header'])){
					print_r($data);
					print_r("<br>");
					print_r($products_numbers."<br>");
				}
				/***********************************
				 *	DEBUGING END
				 ***********************************/
				
				if($products_numbers > 0) {
					$days[$i] = date('D', strtotime('+'.$j.' day'));
					$days_more[$i] = $j;
					$i++;
				}
				

				
				$j++;
				
			}
			
			$this->data['text_resualts'] = sprintf($this->language->get('text_resualts'), $product_number ,$week_day);
			
		}
		
		if(isset($curr_cook)){
			$this->data['curr_cook'] = $curr_cook;
		} else {
			$this->data['curr_cook'] = 0;
		}
		
		$this->data['days_with_meals'] = $days;
		$this->data['days_more'] = $days_more;
		$this->data['text_welcomehome'] = $this->language->get('text_welcomehome');
		
		if (isset($this->request->get['nomealsfound'])){
			// add notifaction
			//print_r($days_more); die;
			$this->data['no_meals_found'] = $this->language->get('no_meals_found');
			
			?>
			<script>
				window.location = "index.php?route=product/category&path=0&day=+<? echo $days_more[0]; ?>%20day";
			</script>
			<?php
		}
		
		$this->load->model('account/customer');
		$this->load->model('tool/image');
        
        //get the restaurant list
        $list = array();
       $rest_list = $this->model_account_customer->getAllRestaurant();
        foreach($rest_list as $rest){
        
          $list[] = $rest['firstname'] . ' ' . $rest['lastname'];
        
        }
        $this->data['rest_list'] = $list;
		
		$customer_info = $this->model_account_customer->getCustomer($this->customer->getId());
		
		if(!empty($customer_info['image'])){
			if (strpos($customer_info['image'], 'http') === 0) {
				$this->data['image'] = $customer_info['pic_square'];
			}else{
				$this->data['image'] = $this->model_tool_image->resize($customer_info['image'], 50, 50);
			}
		} else {
			$this->data['image'] = $this->model_tool_image->resize('no-avatar-women.png', 50, 50);
		}
		
		
		$day_1 = $this->language->get('text_day_1');
		$day_2 = $this->language->get('text_day_2');
		$day_3 = $this->language->get('text_day_3');
		$day_4 = $this->language->get('text_day_4');
		$day_5 = $this->language->get('text_day_5');
		$day_6 = $this->language->get('text_day_6');
		$day_7 = $this->language->get('text_day_7');	
		
		$request = parse_url($_SERVER['REQUEST_URI']);
		$path = isset($request["query"]) ? $request["query"] : "" ;
		
		
		$this->data['text_loginboxtop'] = $this->language->get('text_loginboxtop');
		$this->data['text_loginemail'] = $this->language->get('text_loginemail');
		$this->data['text_loginpass'] = $this->language->get('text_loginpass');
		$this->data['text_loginforgot'] = sprintf($this->language->get('text_loginforgot'), $this->url->link('account/forgotten', '&goback='.base64_encode($path), 'SSL'));
		$this->data['text_loginremember'] = $this->language->get('text_loginremember');
		$this->data['text_loginboxbottom'] = sprintf($this->language->get('text_loginboxbottom'), 'signinnow');
		$this->data['text_login'] = $this->language->get('text_login');
		$this->data['text_signin'] = $this->language->get('text_signin');
		$this->data['text_joinlist'] = $this->language->get('text_joinlist');
		$this->data['text_logintext'] = $this->language->get('text_logintext');
		$this->data['text_or'] = $this->language->get('text_or');
		$this->data['text_signinboxtop'] = $this->language->get('text_signinboxtop');
		$this->data['text_signintext'] = $this->language->get('text_signintext');
		$this->data['text_signinfirstname'] = $this->language->get('text_signinfirstname');
		$this->data['text_signinlastname'] = $this->language->get('text_signinlastname');
		$this->data['text_signincity'] = $this->language->get('text_signincity');
		$this->data['text_signinagree']  = sprintf($this->language->get('text_signinagree'), $this->url->link('information/information', 'information_id=5', 'SSL'), $this->url->link('information/information', 'information_id=3', 'SSL'));
		$this->data['text_signinboxbottom'] = sprintf($this->language->get('text_signinboxbottom'), 'loginnow');
		
		$this->data['text_error_mail'] = $this->language->get('text_error_mail');
		$this->data['text_error_mail_exists'] = $this->language->get('text_error_mail_exists');
		$this->data['text_error_pass'] = $this->language->get('text_error_pass');
		$this->data['text_error_firstname'] = $this->language->get('text_error_firstname');
		$this->data['text_error_lastname'] = $this->language->get('text_error_lastname');
		$this->data['text_error_city'] = $this->language->get('text_error_city');
		
		$this->data['text_no_saved_addresses'] = $this->language->get('text_no_saved_addresses');
		$this->data['text_log_in_addresses'] = $this->language->get('text_log_in_addresses');
		$this->data['date_strf'] = $this->language->get('date_strf');
		
		$this->data['change_my_pass'] = $this->language->get('change_my_pass');
		
		$this->data['text_find_meals'] = $this->language->get('text_find_meals');
		
		$this->data['text_signup'] = $this->language->get('text_signup');
		$this->data['text_login'] = $this->language->get('text_login');
		$this->data['text_need_help'] = $this->language->get('text_need_help');
		
		$this->data['text_terms'] = $this->language->get('text_terms');
		$this->data['text_privet'] = $this->language->get('text_privet');
		
		$month = array();
		$i=1;
		for($i;$i<13;$i++){
			$month[$i] = $this->language->get('text_month_'.$i);
		}
		
		$this->data['day'] = array($day_1,$day_2,$day_3,$day_4,$day_5,$day_6,$day_7);
		$this->data['months'] = $month;
		
		// Daniel's robot detector
		$status = true;
		
		if (isset($this->request->server['HTTP_USER_AGENT'])) {
			$robots = explode("\n", trim($this->config->get('config_robots')));

			foreach ($robots as $robot) {
				if ($robot && strpos($this->request->server['HTTP_USER_AGENT'], trim($robot)) !== false) {
					$status = false;

					break;
				}
			}
		}
		
		// A dirty hack to try to set a cookie for the multi-store feature
		$this->load->model('setting/store');
		
		$this->data['stores'] = array();
		
		if ($this->config->get('config_shared') && $status) {
			$this->data['stores'][] = $server . 'catalog/view/javascript/crossdomain.php?session_id=' . $this->session->getId();
			
			$stores = $this->model_setting_store->getStores();
					
			foreach ($stores as $store) {
				$this->data['stores'][] = $store['url'] . 'catalog/view/javascript/crossdomain.php?session_id=' . $this->session->getId();
			}
		}
				
		
        
	
	$this->load->model('setting/store');
	
        // Checkout
        $products = $this->cart->getProducts();
	
	if(empty($products)){
		$seller = array();
		$seller["ms.avatar"] = "";
		$seller["ms.nickname"] = "";
	}else{
		foreach ($products as $product) {
			$seller = $this->MsLoader->MsSeller->getSeller($this->MsLoader->MsProduct->getSellerId($product['product_id']));
		}
	}
	
	
	
	//$this->load->model(checkout/checkout);
	//$this->data['checkout'] = $this->model_checkout_checkout->checkoutpage();
	
        $this->data['checkout'] = '
        <div class="container">
        </div>
        ';
        
		// Menu
		$this->load->model('catalog/category');
		
		$this->load->model('catalog/product');
		
		$this->data['categories'] = array();
					
		$categories = $this->model_catalog_category->getCategories(0);
		
		foreach ($categories as $category) {
			if ($category['top']) {
				// Level 2
				$children_data = array();
				
				
				$children = $this->model_catalog_category->getCategories($category['category_id']);
						
				///////  Megnor extra code for 3 level categories start  ///////
				
				foreach ($children as $child) {
					$data = array(
						'filter_category_id'  => $child['category_id'],
						'filter_sub_category' => true
					);					
					// Level 2
					$children_level2 = $this->model_catalog_category->getCategories($child['category_id']);
					$children_data_level2 = array();
					foreach ($children_level2 as $child_level2) {
							$data_level2 = array(
									'filter_category_id'  => $child_level2['category_id'],
									'filter_sub_category' => true
							);
							$product_total_level2 = '';
							if ($this->config->get('config_product_count')) {
									$product_total_level2 = ' (' . $this->model_catalog_product->getTotalProducts($data_level2) . ')';
							}

							$children_data_level2[] = array(
									'name'  =>  $child_level2['name'],
									'href'  => $this->url->link('product/category', 'path=' . $child['category_id'] . '_' . $child_level2['category_id']),
									'id' => $category['category_id']. '_' . $child['category_id']. '_' . $child_level2['category_id']
							);
					}
					$children_data[] = array(
							'name'  => $child['name'],
							'href'  => $this->url->link('product/category', 'path=' . $category['category_id'] . '_' . $child['category_id']),
							'id' => $category['category_id']. '_' . $child['category_id'],
							'children_level2' => $children_data_level2,
					);
					
				}
				
				///////  Megnor extra code for 3 level categories end  ///////
				
				// Level 1
				$this->data['categories'][] = array(
					'name'     => $category['name'],
					'children' => $children_data,
					'column'   => $category['column'] ? $category['column'] : 1,
					'href'     => $this->url->link('product/category', 'path=' . $category['category_id'])
				);
			}
		}
		
		$this->children = array(
			'module/language',
			'module/currency',
			'module/cart',
			'supercheckout/facebook',
		);
				
		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/common/header.tpl')) {
			$this->template = $this->config->get('config_template') . '/template/common/header.tpl';
		} else {
			$this->template = 'default/template/common/header.tpl';
		}
		
    	$this->render();
	}
	
}
?>